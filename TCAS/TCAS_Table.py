import numpy as np
from Encounter import units as units

# TA TAU Threshold in seconds
TA_TAU = np.array([20, 25, 30, 40, 45, 48, 48])

# RA TAU Threshold in seconds
RA_TAU = np.array([5, 15, 20, 25, 30, 35, 35])

# TA DMOD in internal units (-1 if N / A)
TA_DMOD = np.array(
  [units.nm_to_m(0.30), units.nm_to_m(0.33), units.nm_to_m(0.48), units.nm_to_m(0.75), units.nm_to_m(1.00),
   units.nm_to_m(1.30), units.nm_to_m(1.30)])

# RA DMOD in internal units (-1 if N / A)
RA_DMOD = np.array(
  [-1, units.nm_to_m(0.20), units.nm_to_m(0.35), units.nm_to_m(0.55), units.nm_to_m(0.80), units.nm_to_m(1.10),
   units.nm_to_m(1.10)])

# TA ZTHR in internal units
TA_ZTHR = np.array([units.ft_to_m(850), units.ft_to_m(850), units.ft_to_m(850), units.ft_to_m(850), units.ft_to_m(850),
                    units.ft_to_m(850), units.ft_to_m(1200)])

# RA ZTHR in internal units (-1 if N / A)
RA_ZTHR = np.array(
  [-1, units.ft_to_m(600), units.ft_to_m(600), units.ft_to_m(600), units.ft_to_m(600), units.ft_to_m(700),
   units.ft_to_m(800)])

# RA HMD in internal units (-1 if N / A)
RA_HMD = np.array(
  [-1, units.ft_to_m(1215), units.ft_to_m(2126), units.ft_to_m(3342), units.ft_to_m(4861), units.ft_to_m(6683),
   units.ft_to_m(6683)])



class TCAS_Table:

  def __init__(self):
    self.HMDFilter = False
    self.TAU = [0] * 7
    self.TCOA = [0] * 7
    self.DMOD= [0] * 7
    self.ZTHR = [0] * 7
    self.HMD = [0] * 7


  def setDefaultRAThresholds(self, ra):
    self.HMDFilter = ra
    for i in range(0, 7):
      if ra:
        self.TAU[i] = RA_TAU[i]
        self.TCOA[i] = RA_TAU[i]
        self.DMOD[i] = RA_DMOD[i]
        self.ZTHR[i] = RA_ZTHR[i]
        self.HMD[i] = RA_HMD[i]
      else:
        self.TAU[i] = TA_TAU[i]
        self.TCOA[i] = TA_TAU[i]
        self.DMOD[i] = TA_DMOD[i]
        self.ZTHR[i] = TA_ZTHR[i]
        self.HMD[i] = TA_DMOD[i]


  #
  # Return sensitivity level from alt, specified in internal units
  def getSensitivityLevel(self, alt):
    alt_ft = units.m_to_ft(alt)
    if alt_ft < 1000:    # 2: < 1000'
      return 2
    if alt_ft <= 2350:   # 3: 1000'-2350'
      return 3
    if alt_ft <= 5000:   # 4: 2350'-5000'
      return 4
    if alt_ft <= 10000:  # 5: 5000'-10000'
      return 5
    if alt_ft <= 20000:  # 6: 10000'-20000'
      return 6
    if alt_ft <= 42000:  # 7: 20000'-42000'
      return 7
    if alt_ft > 42000:   # 8: > 42000'
      return 8


  # Returns TAU threshold for sensitivity level sl in seconds
  def getTAU(self, sl):
    if (2 <= sl and sl <= 8):
      return self.TAU[sl-2]
    return -1

  # Returns TCOA threshold for sensitivity level sl in seconds
  def getTCOA(self, sl):
    if (2 <= sl and sl <= 8):
      return self.TCOA[sl-2]
    return -1

  # Returns DMOD for sensitivity level sl in internal units
  def getDMOD(self, sl):
    return self.DMOD[sl-2]

  # Returns Z threshold for sensitivity level sl in internal units
  def getZTHR(self, sl):
    return self.ZTHR[sl-2]

  # Returns HMD for sensitivity level sl in internal units
  def getHMD(self, sl):
    if (2 <= sl and sl <= 8):
      return self.HMD[sl-2]
    return -1

  # Returns HMD filter
  def getHMDFilter(self):
    return self.HMDFilter

