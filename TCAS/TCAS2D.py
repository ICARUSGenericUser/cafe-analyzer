import numpy as np
import math as math
from RWC import RWC_interval as itv
from RWC import RWC_horizontal as horizontal


def almostZero(v):
  return math.isclose(v[0], 0) and math.isclose(v[1], 0)


def ScalAdd(k, s, v):
  return k*s+v


# Compute modified tau
def tau_mod(DMOD, s, v):
  sdotv = np.dot(s, v)
  if math.isclose(sdotv, 0):
    return 0
  return DMOD**2 - np.dot(s, s) / sdotv


def horizontal_RA(DMOD, Tau, s, v):
  if np.dot(s, v) >= 0:
    return s.norm() <= DMOD
  else:
    return s.norm() <= DMOD or tau_mod(DMOD, s, v) <= Tau


def horizontal_RA_at(DMOD, Tau, s, v, t):
  sat = s + t * v
  return horizontal_RA(DMOD, Tau, sat, v)


def nominal_tau(B, T, s, v, rr):
  if almostZero(v):
    return B
  return max(B, min(T, -np.dot(s, v) / np.dot(v, v) - rr / 2))


def time_of_min_tau(DMOD, B, T, s, v):
  if np.dot(ScalAdd(B, v, s), v) >= 0:
    return B

  rr = 0
  d = horizontal.Delta(s, v, DMOD)
  if d < 0:
    rr = 2 * math.sqrt(-d) / np.dot(v, v)
  elif np.dot(ScalAdd(T, v, s), v) < 0:
    return T
  return nominal_tau(B, T, s, v, rr)


def min_tau_undef(DMOD, B, T, s, v):
  return horizontal.Delta(s, v, DMOD) >= 0 and np.dot(ScalAdd(B, v, s), v) < 0 and np.dot(ScalAdd(T, v, s), v) >= 0


def RA2D(DMOD, Tau, B, T, s, v):
  if min_tau_undef(DMOD, B, T, s, v):
    return True
  mt = time_of_min_tau(DMOD, B, T, s, v)
  return horizontal_RA_at(DMOD, Tau, s, v, mt)


def RA2D_interval(B, T, s, vo, vi, tcas_param, sl):

  time_in = B
  time_out = T

  v = vo - vi
  sqs = np.dot(s, s)
  sdotv = np.dot(s, v)

  sqD = tcas_param.getDMOD(sl) ** 2
  if np.allclose(vo, vi) and sqs <= sqD:
    return itv.Interval(time_in, time_out)

  sqv = np.dot(v, v)
  if sqs <= sqD:
    time_out = horizontal.root2b(sqv, sdotv, sqs-sqD, 1)
    return itv.Interval(time_in, time_out)

  b = 2 * sdotv + tcas_param.getTAU(sl) * sqv
  c = sqs + tcas_param.getTAU(sl) * sdotv - sqD
  if sdotv >= 0 or horizontal.discr(sqv, b, c) < 0:
    time_in = T + 1
    time_out = 0
    return itv.Interval(time_in, time_out)

  time_in = horizontal.root(sqv, b, c, -1)
  if horizontal.Delta(s, v, tcas_param.getDMOD(sl)) >= 0:
    time_out = horizontal.Theta_D(s, v, 1, tcas_param.getDMOD(sl))
  else:
    time_out = horizontal.root(sqv, b, c, 1)

  return itv.Interval(time_in, time_out)

