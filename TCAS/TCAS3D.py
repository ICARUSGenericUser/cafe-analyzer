import numpy as np
import math as math
from RWC import RWC_interval as itv
from RWC import RWC_horizontal as horizontal
from RWC import RWC_vertical as vertical
from TCAS import TCAS_ConflictData as cd
from TCAS import TCAS2D as TCAS2D


def vertical_RA(sz, vz, ZTHR, TCOA):
  if math.abs(sz) <= ZTHR:
    return True
  if math.isclose(vz, 0):
    return False
  tcoa = vertical.time_coalt(sz, vz)
  return 0 <= tcoa and tcoa <= TCOA


def cd2d_TCAS_after(HMD, s, vo, vi, t):
  v = vo - vi
  return (TCAS2D.almostZero(v) and np.dot(s, s) <= HMD**2) or (np.dot(v, v) > 0 and horizontal.Delta(s, v, HMD) >= 0 and horizontal.Theta_D(s, v, 1, HMD) >= t)


def cd2d_TCAS(HMD, s, vo, vi):
  return cd2d_TCAS_after(HMD, s, vo, vi, 0)


# If true, then ownship has a TCAS resolution advisory at current time
def TCASII_RA(so, vo, si, vi, TCASTable):

  so2 = so[0:1]
  si2 = si[0:1]
  s2 = so2 - si2
  vo2 = vo[0:1]
  vi2 = vi[0:1]
  v2 = vo2 - vi2
  sz = so[2] - si[2]
  vz = vo[2] - vi[2]

  sl = TCASTable.getSensitivityLevel(so[2])
  usehmdf = TCASTable.getHMDFilter()
  TAU = TCASTable.getTAU(sl)
  TCOA = TCASTable.getTCOA(sl)
  DMOD = TCASTable.getDMOD(sl)
  HMD = TCASTable.getHMD(sl)
  ZTHR = TCASTable.getZTHR(sl)

  return (not usehmdf or cd2d_TCAS(HMD, s2, vo2, vi2)) and TCAS2D.horizontal_RA(DMOD, TAU, s2, v2) and vertical_RA(sz, vz, ZTHR, TCOA)



# Linear projection through time
def linear(s, v, t):
  return s + v * t


# Cylindrical norm.
# param d Radius of cylinder
# param h Half-height of cylinder
# return the cylindrical distance of <code>this</code>. The cylindrical distance is
# 1 when <code>this</code> is at the boundaries of the cylinder.
def cyl_norm(s3, d, h):
  s2 = s3[0:1]
  return max(np.dot(s2, s2)/d**2, (s3[2]/h)**2)


# If true, within lookahead time interval [B,T], the ownship has a TCAS resolution advisory (effectively conflict detection)
# B must be non-negative and T > B
def RA3D(so, vo, si, vi, B, T):
    return RA3D_interval(so,vo,si,vi,B,T)


def RA3D_analysis(own_pos, int_pos, TCASTable):
  B = 0
  T = TCASTable.getTAU(TCASTable.getSensitivityLevel(own_pos.altitude))
  return RA3D_interval(own_pos, int_pos, B, T, TCASTable)


# Assumes 0 <= B < T
def RA3D_interval(own_pos, int_pos, B, T, TCASTable):
  time_in = T
  time_out = B
  time_mintau = -1
  dist_mintau = -1

  so2 = np.array([own_pos.x_loc, own_pos.y_loc])
  so = np.array([own_pos.x_loc, own_pos.y_loc, own_pos.altitude])
  si2 = np.array([int_pos.x_loc, int_pos.y_loc])
  si = np.array([int_pos.x_loc, int_pos.y_loc, int_pos.altitude])
  vo2 = np.array([own_pos.x_v, own_pos.y_v])
  vo = np.array([own_pos.x_v, own_pos.y_v, own_pos.v_speed])
  vi2 = np.array([int_pos.x_v, int_pos.y_v])
  vi = np.array([int_pos.x_v, int_pos.y_v, int_pos.v_speed])
  s2 = si2 - so2
  s = si - so
  v2 = vi2 - vo2
  v = vi - vo
  sz = int_pos.altitude - own_pos.altitude
  vz = int_pos.v_speed - own_pos.v_speed

  sl = TCASTable.getSensitivityLevel(own_pos.altitude)

  usehmdf = TCASTable.getHMDFilter()
  TAU = TCASTable.getTAU(sl)
  TCOA = TCASTable.getTCOA(sl)
  DMOD = TCASTable.getDMOD(sl)
  HMD = TCASTable.getHMD(sl)
  ZTHR = TCASTable.getZTHR(sl)

  if (usehmdf and not cd2d_TCAS_after(HMD, s2, vo2, vi2, B)):
    time_mintau = TCAS2D.time_of_min_tau(DMOD, B, T, s2, v2)
    dist_mintau = cyl_norm(linear(so, vo, time_mintau) - linear(si, vi, time_mintau), TCASTable.getDMOD(8), TCASTable.getZTHR(8))
    return cd.ConflictData(time_in, time_out, time_mintau, dist_mintau, s, v)

  if math.isclose(vz, 0) and abs(sz) > ZTHR:
    time_mintau = TCAS2D.time_of_min_tau(DMOD, B, T, s2, v2)
    dist_mintau = cyl_norm(linear(so, vo, time_mintau) - linear(si, vi, time_mintau), TCASTable.getDMOD(8), TCASTable.getZTHR(8))
    return cd.ConflictData(time_in, time_out, time_mintau, dist_mintau, s, v)

  tentry = B
  texit = T
  if not math.isclose(sz, 0):
    act_H = max(ZTHR, np.abs(vz) * TCOA)
    tentry = vertical.Theta_H(sz, vz, -1, act_H)
    texit = vertical.Theta_H(sz, vz, 1, ZTHR)

  ventry = s2 + v2* tentry
  exit_at_centry = np.dot(ventry, v2) >= 0
  los_at_centry = np.dot(ventry, ventry) <= HMD**2
  if texit < B or T < tentry:
    time_mintau = TCAS2D.time_of_min_tau(DMOD, B, T, s2, v2)
    dist_mintau = cyl_norm(linear(so, vo, time_mintau) - linear(si, vi, time_mintau), TCASTable.getDMOD(8), TCASTable.getZTHR(8))
    return cd.ConflictData(time_in, time_out, time_mintau, dist_mintau, s, v)

  tin = max(B, tentry)
  tout = min(T, texit)
  tcas2d = TCAS2D.RA2D_interval(tin, tout, s2, vo2, vi2, TCASTable, sl)
  RAin2D = tcas2d.low
  RAout2D = tcas2d.up
  RAin2D_lookahead = max(tin, min(tout, RAin2D))
  RAout2D_lookahead = max(tin, min(tout, RAout2D))

  if (RAin2D > RAout2D or RAout2D < tin or RAin2D > tout or
      (usehmdf and HMD < DMOD and exit_at_centry and not los_at_centry)):
    time_mintau = TCAS2D.time_of_min_tau(DMOD, B, T, s2, v2)
    dist_mintau = cyl_norm(linear(so, vo, time_mintau) - linear(si, vi, time_mintau), TCASTable.getDMOD(8), TCASTable.getZTHR(8))
    return cd.ConflictData(time_in, time_out, time_mintau, dist_mintau, s, v)

  if (usehmdf and HMD < DMOD):
    exitTheta = T
    if (np.dot(v2, v2) > 0):
      exitTheta = max(B, min(horizontal.Theta_D(s2, v2, 1, HMD), T))
      minRAoutTheta = min(RAout2D_lookahead, exitTheta)
      time_in = RAin2D_lookahead
      time_out = minRAoutTheta
      if (RAin2D_lookahead <= minRAoutTheta):
        time_mintau = TCAS2D.time_of_min_tau(DMOD, RAin2D_lookahead, minRAoutTheta, s2, v2)
        dist_mintau = cyl_norm(linear(so, vo, time_mintau) - linear(si, vi, time_mintau), TCASTable.getDMOD(8), TCASTable.getZTHR(8))
        return cd.ConflictData(time_in, time_out, time_mintau, dist_mintau, s, v)

    time_mintau = TCAS2D.time_of_min_tau(DMOD, B, T, s2, v2)
    dist_mintau = cyl_norm(linear(so, vo, time_mintau) - linear(si, vi, time_mintau), TCASTable.getDMOD(8), TCASTable.getZTHR(8))
    return cd.ConflictData(time_in, time_out, time_mintau, dist_mintau, s, v)

  time_in = RAin2D_lookahead
  time_out = RAout2D_lookahead
  time_mintau = TCAS2D.time_of_min_tau(DMOD, RAin2D_lookahead, RAout2D_lookahead, s2, v2)
  dist_mintau = cyl_norm(linear(so, vo, time_mintau) - linear(si, vi, time_mintau), TCASTable.getDMOD(8), TCASTable.getZTHR(8))

  return cd.ConflictData(time_in, time_out, time_mintau, dist_mintau, s, v)

