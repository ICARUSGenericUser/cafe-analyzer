from Encounter import units as units
from RWC import RWC_interval as itv

class ConflictData:

  def __init__(self, t_in, t_out, t_crit, d_crit, s3, v3):
    self.interval = itv.Interval(t_in, t_out)
    self.time_crit = t_crit  # Relative time to critical point
    self.dist_crit = d_crit  # Distance or severity at critical point (0 is most critical, +inf is least severe)
    self.s_ = s3 # Position
    self.v_ = v3 # Relative velocity

    # Returns true if loss
  def conflict(self):
    return self.interval.conflict()



