from Encounter import units as units
import numpy as np
import math as math
from RWC import RWC_alert_times as at
from STCA import SEP_volume as sep
from STCA import STCA_core

TIMER_HISTERESIS = 5

def squircle (x, y):
  return math.sqrt(x**2 + (1 - x**2) * y**2)


class Enc_Alert_Measuring:
  def __init__(self):
    self.active_conflict = False    # Activated in case any conflict happens

    self.prev_type = at.alertType.noAlert  # Preventive alert type
    self.prev_active = False        # Preventive threshold activated in some way
    self.prev_first_time = -1       # Time of the first preventive alert activation
    self.prev_first_tt_haz = -1     # Time to first HAZ violation
    self.prev_first_type = at.alertType.noAlert
    self.prev_time = -1             # Time of the preventive alert activation
    self.prev_tt_haz = -1           # Time to HAZ violation
    self.prev_number = 0            # Number of preventive activations
    self.prev_timer = 0             # Timer for hysteresis functions
    self.prev_first_end_time = -1
    self.prev_end_time = -1

    self.caution_type = at.alertType.noAlert  # Corrective alert type
    self.caution_active = False        # Corrective threshold activated in some way
    self.caution_first_time = -1       # Time of the first corrective alert activation
    self.caution_first_tt_haz = -1     # Time to first HAZ violation
    self.caution_first_type = at.alertType.noAlert
    self.caution_time = -1             # Time of the corrective alert activation
    self.caution_tt_haz = -1           # Time to HAZ violation
    self.caution_number = 0            # Number of corrective activations
    self.caution_timer = 0             # Timer for hysteresis functions
    self.caution_first_end_time = -1
    self.caution_end_time = -1

    self.warn_type = at.alertType.noAlert # Warning alert type
    self.warn_active = False        # Warning threshold activated in some way
    self.warn_first_time = -1       # Time of the first warning alert activation
    self.warn_first_tt_haz = -1     # Time to first HAZ violation
    self.warn_first_type = at.alertType.noAlert
    self.warn_time = -1             # Time of the warning alert activation
    self.warn_tt_haz = -1           # Time to HAZ violation
    self.warn_number = 0            # Number of warning activations
    self.warn_timer = 0             # Timer for hysteresis functions
    self.warn_first_end_time = -1
    self.warn_end_time = -1

    # Severity of LoWC parameters for aircraft
    self.haz_violated = False       # HAZ volume violated
    self.haz_first_time = 0
    self.haz_first_SLoWC  = 0
    self.haz_first_RangePen = 0
    self.haz_first_HMDPen = 0
    self.haz_first_VertPen = 0
    self.haz_first_tcpa = 0
    self.haz_first_dcpa = 0
    self.haz_SLoWC_time = 0
    self.SLoWC = 0                  # Severity of LoWC
    self.RangePen = 1               # SLoWC Horizontal Proximity
    self.HMDPen = 1                 # SLoWC Horizontal Miss Distance Projection
    self.VertPen = 1                # SLoWC Vertical Separation
    self.haz_tcpa_time = 0
    self.haz_tcpa = 0
    self.haz_dcpa = 0
    self.haz_slat_time = 0
    self.min_slat_range = 0

    self.minSlat = -1
    self.minSlatTime = 0

    self.minHDist = -1
    self.minHDistTime = 0

    # NMAC parameters
    self.nmac_violated = False      # NMAC volume violated
    self.nmac_time = 0
    self.nmac_first_time = 0
    self.HMD = 0                    # HMD at Horizontal CPA
    self.VMD = 0                    # VMD at Horizontal CPA
    self.WSR = 0                    # Minimum Weighted Slant Range

    # TCAS parameters
    self.tcas_ra = False
    self.tcas_ra_time = 0
    self.tcas_ra_time_worst = 0

    # Separation STCA parameters
    self.sep_violated = False  # Separation volume violated
    self.sep_time = 0
    self.sep_relevant = False

    self.stca_violated = False  # Separation volume violated
    self.stca_time = 0
    self.stca_relevant = False
    self.stcao = STCA_core.STCA_core(0)

    # LMV for NMAC and HAZ parameters
    self.lmv_activated = False
    self.lmv_violated = False
    self.lmv_first_time = 0
    self.lmv_worst_time = 0
    self.lmv_first_time_prediction = 0
    self.lmv_worst_distance = 0
    self.lmv_violated_time = 0

    self.haz_lmv_violated = False
    self.haz_lmv_violated_time = -1




  def toHeaderCSV(self):
    csvStr = "activeConflict" + ', ' + \
              "cpaTime" + ', ' + \
              "cpaBeta" + ', ' + \
              "cpaRange" + ', ' + \
              "cpaAzimuth" + ', ' + \
              "cpaElevation" + ', ' + \
              "cpaDz" + ', ' + \
             "minHDistTime" + ', ' + \
             "minHDistBeta" + ', ' + \
             "minHDistRange" + ', ' + \
             "minHDistAzimuth" + ', ' + \
             "minHDistElevation" + ', ' + \
             "minHDistDz" + ', ' + \
              "nmacViolated" + ', ' + \
              "nmacFirstTime" + ', ' + \
              "nmacTime" + ', ' + \
              "nmacWSR" + ', ' + \
              "nmacHMD" + ', ' + \
              "nmacVMD" + ', ' + \
              "nmacFirstTimeBeta" + ', ' + \
              "nmacFirstTimeRange" + ', ' + \
              "nmacFirstTimeAzimuth" + ', ' + \
              "nmacFirstTimeElevation" + ', ' + \
              "nmacFirstTimeDz" + ', ' + \
              "nmacTimeBeta" + ', ' + \
              "nmacTimeRange" + ', ' + \
              "nmacTimeAzimuth" + ', ' + \
              "nmacTimeElevation" + ', ' + \
              "nmacTimeDz" + ', ' + \
              "hazViolated" + ', ' + \
              "hazFirstTime" + ', ' + \
              "hazFirstTimeSLoWC" + ', ' + \
              "hazFirstTimeRangePen" + ', ' + \
              "hazFirstTimeHMDPen" + ', ' + \
              "hazFirstTimeVertPen" + ', ' + \
              "hazFirstTcpa" + ', ' + \
              "hazFirstDcpa" + ', ' + \
              "haz_SLoWC_time" + ', ' + \
              "hazSLoWC" + ', ' + \
              "hazRangePen" + ', ' + \
              "hazHMDPen" + ', ' + \
              "hazVertPen" + ', ' + \
              "hazTcpaTime" + ', ' + \
              "hazTcpa" + ', ' + \
              "hazDcpa" + ', ' + \
              "minSlatTime" + ', ' + \
              "minSlatRange" + ', ' + \
              "hazFirstTimeBeta" + ', ' + \
              "hazFirstTimeRange" + ', ' + \
              "hazFirstTimeAzimuth" + ', ' + \
              "hazFirstTimeElevation" + ', ' + \
              "hazFirstTimeDz" + ', ' + \
              "hazSLoWCTimeBeta" + ', ' + \
              "hazSLoWCTimeRange" + ', ' + \
              "hazSLoWCTimeAzimuth" + ', ' + \
              "hazSLoWCTimeElevation" + ', ' + \
              "hazSLoWCTimeDz" + ', ' + \
              "hazTcpaTimeBeta" + ', ' + \
              "hazTcpaTimeRange" + ', ' + \
              "hazTcpaTimeAzimuth" + ', ' + \
              "hazTcpaTimeElevation" + ', ' + \
              "hazTcpaTimeDz" + ', ' + \
              "hazSlatTimeBeta" + ', ' + \
              "hazSlatTimeRange" + ', ' + \
              "hazSlatTimeAzimuth" + ', ' + \
              "hazSlatTimeElevation" + ', ' + \
              "hazSlatTimeDz" + ', ' + \
              "cautionNumber" + ', ' + \
              "cautionFirstTime" + ', ' + \
              "cautionFirstTtHaz" + ', ' + \
              "cautionFirstType" + ', ' + \
              "cautionTime" + ', ' + \
              "cautionTtHaz" + ', ' + \
              "cautionType" + ', ' + \
              "cautionFirstBeta" + ', ' + \
              "cautionFirstRange" + ', ' + \
              "cautionFirstAzimuth" + ', ' + \
              "cautionFirstElevation" + ', ' + \
              "cautionFirstDz" + ', ' + \
              "cautionBeta" + ', ' + \
              "cautionRange" + ', ' + \
              "cautionAzimuth" + ', ' + \
              "cautionElevation" + ', ' + \
              "cautionDz" + ', ' + \
              "cautionFirstEndTime" + ', ' + \
              "cautionFirstEndBeta" + ', ' + \
              "cautionFirstEndRange" + ', ' + \
              "cautionFirstEndAzimuth" + ', ' + \
              "cautionFirstEndElevation" + ', ' + \
              "cautionFirstEndDz" + ', ' + \
              "cautionEndTime" + ', ' + \
              "cautionEndBeta" + ', ' + \
              "cautionEndRange" + ', ' + \
              "cautionEndAzimuth" + ', ' + \
              "cautionEndElevation" + ', ' + \
              "cautionEndDz" + ', ' + \
             "prevNumber" + ', ' + \
             "prevFirstTime" + ', ' + \
             "prevFirstTtHaz" + ', ' + \
             "prevFirstType" + ', ' + \
             "prevTime" + ', ' + \
             "prevTtHaz" + ', ' + \
             "prevType" + ', ' + \
             "prevFirstBeta" + ', ' + \
             "prevFirstRange" + ', ' + \
             "prevFirstAzimuth" + ', ' + \
             "prevFirstElevation" + ', ' + \
             "prevFirstDz" + ', ' + \
             "prevBeta" + ', ' + \
             "prevRange" + ', ' + \
             "prevAzimuth" + ', ' + \
             "prevElevation" + ', ' + \
             "prevDz" + ', ' + \
             "prevFirstEndTime" + ', ' + \
             "prevFirstEndBeta" + ', ' + \
             "prevFirstEndRange" + ', ' + \
             "prevFirstEndAzimuth" + ', ' + \
             "prevFirstEndElevation" + ', ' + \
             "prevFirstEndDz" + ', ' + \
             "prevEndTime" + ', ' + \
             "prevEndBeta" + ', ' + \
             "prevEndRange" + ', ' + \
             "prevEndAzimuth" + ', ' + \
             "prevEndElevation" + ', ' + \
             "prevEndDz" + ', ' + \
             "warnNumber" + ', ' + \
             "warnFirstTime" + ', ' + \
             "warnFirstTtHaz" + ', ' + \
             "warnFirstType" + ', ' + \
             "warnTime" + ', ' + \
             "warnTtHaz" + ', ' + \
             "warnType" + ', ' + \
             "warnFirstBeta" + ', ' + \
             "warnFirstRange" + ', ' + \
             "warnFirstAzimuth" + ', ' + \
             "warnFirstElevation" + ', ' + \
             "warnFirstDz" + ', ' + \
             "warnBeta" + ', ' + \
             "warnRange" + ', ' + \
             "warnAzimuth" + ', ' + \
             "warnElevation" + ', ' + \
             "warnDz" + ', ' + \
             "warnFirstEndTime" + ', ' + \
             "warnFirstEndBeta" + ', ' + \
             "warnFirstEndRange" + ', ' + \
             "warnFirstEndAzimuth" + ', ' + \
             "warnFirstEndElevation" + ', ' + \
             "warnFirstEndDz" + ', ' + \
             "warnEndTime" + ', ' + \
             "warnEndBeta" + ', ' + \
             "warnEndRange" + ', ' + \
             "warnEndAzimuth" + ', ' + \
             "warnEndElevation" + ', ' + \
             "warnEndDz" + ', ' + \
             "tcasRa" + ', ' + \
              "tcasRaTime" + ', ' + \
              "tcasRaTimeWorst" + ', ' + \
              "tcasRaTimeBeta" + ', ' + \
              "tcasRaTimeRange" + ', ' + \
              "tcasRaTimeAzimuth" + ', ' + \
              "tcasRaTimeElevation" + ', ' + \
              "tcasRaTimeDz" + ', ' + \
              "tcasRaWorstBeta" + ', ' + \
              "tcasRaWorstRange" + ', ' + \
              "tcasRaWorstAzimuth" + ', ' + \
              "tcasRaWorstElevation" + ', ' + \
              "tcasRaWorstDz" + ', ' + \
              "tcasRaTimeTimeCrit" + ', ' + \
              "tcasRaTimeDistCrit" + ', ' + \
              "tcasRaWorstTimeCrit" + ', ' + \
              "tcasRaWorstDistCrit" + ', ' + \
              "sepViolated" + ', ' + \
              "sepRelevant" + ', ' + \
              "sepTime" + ', ' + \
              "sepTimeBeta" + ', ' + \
              "sepTimeRange" + ', ' + \
              "sepTimeAzimuth" + ', ' + \
              "sepTimeElevation" + ', ' + \
              "sepTimeDz" + ', ' + \
              "stcaViolated" + ', ' + \
              "stcaRelevant" + ', ' + \
              "stcaTime" + ', ' + \
              "stcaTimeBeta" + ', ' + \
              "stcaTimeRange" + ', ' + \
              "stcaTimeAzimuth" + ', ' + \
              "stcaTimeElevation" + ', ' + \
              "stcaTimeDz" + ', ' + \
              "lmvActivated" + ', ' + \
              "lmvViolated" + ', ' + \
              "lmvFirstTime" + ', ' + \
              "lmvWorstTime" + ', ' + \
              "lmvFirstTimePrediction" + ', ' + \
              "lmvWorstDistance" + ', ' + \
              "lmvViolatedTime" + ', ' + \
              "lmvTimeBeta" + ', ' + \
              "lmvTimeRange" + ', ' + \
              "lmvTimeAzimuth" + ', ' + \
              "lmvTimeElevation" + ', ' + \
              "lmvTimeDz" + ', ' + \
              "hazLmvViolated" + ', ' + \
              "hazLmvViolatedTime" + ', ' + \
              "hazLmvTimeBeta" + ', ' + \
              "hazLmvTimeRange" + ', ' + \
              "hazLmvTimeAzimuth" + ', ' + \
              "hazLmvTimeElevation" + ', ' + \
              "hazLmvTimeDz"

    return csvStr


  def toCSV(self, NCSParameters, aircraftTCAS):

    csvStr = str(self.active_conflict) + ', '


    # CPA parameters
    csvStr += str(self.minSlatTime) + ', ' + str(NCSParameters[self.minSlatTime].beta) + ', ' + str(NCSParameters[self.minSlatTime].range) + ', ' + str(NCSParameters[self.minSlatTime].azimuth) + ', ' + str(NCSParameters[self.minSlatTime].elevation) + ', ' + str(NCSParameters[self.minSlatTime].dz) + ', '
    csvStr += str(self.minHDistTime) + ', ' + str(NCSParameters[self.minHDistTime].beta) + ', ' + str(NCSParameters[self.minHDistTime].range) + ', ' + str(NCSParameters[self.minHDistTime].azimuth) + ', ' + str(NCSParameters[self.minHDistTime].elevation) + ', ' + str(NCSParameters[self.minHDistTime].dz) + ', '


    # NMAC violation parameters
    csvStr += str(self.nmac_violated) + ', ' + str(self.nmac_first_time) + ', ' + str(self.nmac_time) + ', ' + str(self.WSR) + ', ' + str(self.HMD) + ', ' + str(self.VMD) + ', '
    if self.nmac_violated:
      csvStr += str(NCSParameters[self.nmac_first_time].beta) + ', ' + str(NCSParameters[self.nmac_first_time].range) + ', ' + str(NCSParameters[self.nmac_first_time].azimuth) + ', ' + str(NCSParameters[self.nmac_first_time].elevation) + ', ' + str(NCSParameters[self.nmac_first_time].dz) + ', '
      csvStr += str(NCSParameters[self.nmac_time].beta) + ', ' + str(NCSParameters[self.nmac_time].range) + ', ' + str(NCSParameters[self.nmac_time].azimuth) + ', ' + str(NCSParameters[self.nmac_time].elevation) + ', ' + str(NCSParameters[self.nmac_time].dz) + ', '
    else:
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '


    # HAZ violation parameters
    csvStr += str(self.haz_violated) + ', ' + str(self.haz_first_time) + ', ' + str(self.haz_first_SLoWC) + ', ' + str(self.haz_first_RangePen) + ', ' + str(self.haz_first_HMDPen) + ', ' + str(self.haz_first_VertPen) + ', ' + str(self.haz_first_tcpa) + ', ' + str(self.haz_first_dcpa) + ', '
    csvStr += str(self.haz_SLoWC_time) + ', ' + str(self.SLoWC) + ', ' + str(self.RangePen) + ', ' + str(self.HMDPen) + ', ' + str(self.VertPen) + ', '
    csvStr += str(self.haz_tcpa_time) + ', ' + str(self.haz_tcpa) + ', ' + str(self.haz_dcpa) + ', '
    csvStr += str(self.haz_slat_time) + ', ' + str(self.min_slat_range) + ', '

    if self.haz_violated:
      csvStr += str(NCSParameters[self.haz_first_time].beta) + ', ' + str(NCSParameters[self.haz_first_time].range) + ', ' + str(NCSParameters[self.haz_first_time].azimuth) + ', ' + str(NCSParameters[self.haz_first_time].elevation) + ', ' + str(NCSParameters[self.haz_first_time].dz) + ', '
      csvStr += str(NCSParameters[self.haz_first_time].beta) + ', ' + str(NCSParameters[self.haz_SLoWC_time].range) + ', ' + str(NCSParameters[self.haz_SLoWC_time].azimuth) + ', ' + str(NCSParameters[self.haz_SLoWC_time].elevation) + ', ' + str(NCSParameters[self.haz_SLoWC_time].dz) + ', '
      csvStr += str(NCSParameters[self.haz_first_time].beta) + ', ' + str(NCSParameters[self.haz_tcpa_time].range) + ', ' + str(NCSParameters[self.haz_tcpa_time].azimuth) + ', ' + str(NCSParameters[self.haz_tcpa_time].elevation) + ', ' + str(NCSParameters[self.haz_tcpa_time].dz) + ', '
      csvStr += str(NCSParameters[self.haz_first_time].beta) + ', ' + str(NCSParameters[self.haz_slat_time].range) + ', ' + str(NCSParameters[self.haz_slat_time].azimuth) + ', ' + str(NCSParameters[self.haz_slat_time].elevation) + ', ' + str(NCSParameters[self.haz_slat_time].dz) + ', '
    else:
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '


    # CAUTION threshold parameters
    csvStr += str(self.caution_number) + ', ' + str(self.caution_first_time) + ', ' + str(self.caution_first_tt_haz) + ', ' + str(self.caution_first_type) + ', ' + str(self.caution_time) + ', ' + str(self.caution_tt_haz) + ', ' + str(self.caution_type) + ', '
    if self.caution_number > 0:
      csvStr += str(NCSParameters[self.caution_first_time].beta) + ', ' + str(NCSParameters[self.caution_first_time].range) + ', ' + str(NCSParameters[self.caution_first_time].azimuth) + ', ' + str(NCSParameters[self.caution_first_time].elevation) + ', ' + str(NCSParameters[self.caution_first_time].dz) + ', '
      csvStr += str(NCSParameters[self.caution_time].beta) + ', ' + str(NCSParameters[self.caution_time].range) + ', ' + str(NCSParameters[self.caution_time].azimuth) + ', ' + str(NCSParameters[self.caution_time].elevation) + ', ' + str(NCSParameters[self.caution_time].dz) + ', '
    else:
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '

    if self.caution_number > 0:
      csvStr += str(self.caution_first_end_time) + ', ' + str(NCSParameters[self.caution_first_end_time].beta) + ', ' + str(NCSParameters[self.caution_first_end_time].range) + ', ' + str(NCSParameters[self.caution_first_end_time].azimuth) + ', ' + str(NCSParameters[self.caution_first_end_time].elevation) + ', ' + str(NCSParameters[self.caution_first_end_time].dz) + ', '
      csvStr += str(self.caution_end_time) + ', ' + str(NCSParameters[self.caution_end_time].beta) + ', ' + str(NCSParameters[self.caution_end_time].range) + ', ' + str(NCSParameters[self.caution_end_time].azimuth) + ', ' + str(NCSParameters[self.caution_end_time].elevation) + ', ' + str(NCSParameters[self.caution_end_time].dz) + ', '
    else:
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '


    # PREVENTIVE threshold parameters
    csvStr += str(self.prev_number) + ', ' + str(self.prev_first_time) + ', ' + str(self.prev_first_tt_haz) + ', ' + str(self.prev_first_type) + ', ' + str(self.prev_time) + ', ' + str(self.prev_tt_haz) + ', ' + str(self.prev_type) + ', '
    if self.prev_number > 0:
      csvStr += str(NCSParameters[self.prev_first_time].beta) + ', ' + str(NCSParameters[self.prev_first_time].range) + ', ' + str(NCSParameters[self.prev_first_time].azimuth) + ', ' + str(NCSParameters[self.prev_first_time].elevation) + ', ' + str(NCSParameters[self.prev_first_time].dz) + ', '
      csvStr += str(NCSParameters[self.prev_time].beta) + ', ' + str(NCSParameters[self.prev_time].range) + ', ' + str(NCSParameters[self.prev_time].azimuth) + ', ' + str(NCSParameters[self.prev_time].elevation) + ', ' + str(NCSParameters[self.prev_time].dz) + ', '
    else:
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '

    if self.prev_number > 0:
      csvStr += str(self.prev_first_end_time) + ', ' + str(NCSParameters[self.prev_first_end_time].beta) + ', ' + str(NCSParameters[self.prev_first_end_time].range) + ', ' + str(NCSParameters[self.prev_first_end_time].azimuth) + ', ' + str(NCSParameters[self.prev_first_end_time].elevation) + ', ' + str(NCSParameters[self.prev_first_end_time].dz) + ', '
      csvStr += str(self.prev_end_time) + ', ' + str(NCSParameters[self.prev_end_time].beta) + ', ' + str(NCSParameters[self.prev_end_time].range) + ', ' + str(NCSParameters[self.prev_end_time].azimuth) + ', ' + str(NCSParameters[self.prev_end_time].elevation) + ', ' + str(NCSParameters[self.prev_end_time].dz) + ', '
    else:
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '


    # WARNING threshold parameters
    csvStr += str(self.warn_number) + ', ' + str(self.warn_first_time) + ', ' + str(self.warn_first_tt_haz) + ', ' + str(self.warn_first_type) + ', ' + str(self.warn_time) + ', ' + str(self.warn_tt_haz) + ', ' + str(self.warn_type) + ', '
    if self.warn_number > 0:
      csvStr += str(NCSParameters[self.warn_first_time].beta) + ', ' + str(NCSParameters[self.warn_first_time].range) + ', ' + str(NCSParameters[self.warn_first_time].azimuth) + ', ' + str(NCSParameters[self.warn_first_time].elevation) + ', ' + str(NCSParameters[self.warn_first_time].dz) + ', '
      csvStr += str(NCSParameters[self.warn_time].beta) + ', ' + str(NCSParameters[self.warn_time].range) + ', ' + str(NCSParameters[self.warn_time].azimuth) + ', ' + str(NCSParameters[self.warn_time].elevation) + ', ' + str(NCSParameters[self.warn_time].dz) + ', '
    else:
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '

    if self.warn_number > 0:
      csvStr += str(self.warn_first_end_time) + ', ' + str(NCSParameters[self.warn_first_end_time].beta) + ', ' + str(NCSParameters[self.warn_first_end_time].range) + ', ' + str(NCSParameters[self.warn_first_end_time].azimuth) + ', ' + str(NCSParameters[self.warn_first_end_time].elevation) + ', ' + str(NCSParameters[self.warn_first_end_time].dz) + ', '
      csvStr += str(self.warn_end_time) + ', ' + str(NCSParameters[self.warn_end_time].beta) + ', ' + str(NCSParameters[self.warn_end_time].range) + ', ' + str(NCSParameters[self.warn_end_time].azimuth) + ', ' + str(NCSParameters[self.warn_end_time].elevation) + ', ' + str(NCSParameters[self.warn_end_time].dz) + ', '
    else:
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '



    # TCAS violation parameters
    csvStr += str(self.tcas_ra) + ', ' + str(self.tcas_ra_time) + ', ' + str(self.tcas_ra_time_worst) + ', '
    if self.tcas_ra:
      csvStr += str(NCSParameters[self.tcas_ra_time].beta) + ', ' + str(NCSParameters[self.tcas_ra_time].range) + ', ' + str(NCSParameters[self.tcas_ra_time].azimuth) + ', ' + str(NCSParameters[self.tcas_ra_time].elevation) + ', ' + str(NCSParameters[self.tcas_ra_time].dz) + ', '
      csvStr += str(NCSParameters[self.tcas_ra_time_worst].beta) + ', ' + str(NCSParameters[self.tcas_ra_time_worst].range) + ', ' + str(NCSParameters[self.tcas_ra_time_worst].azimuth) + ', ' + str(NCSParameters[self.tcas_ra_time_worst].elevation) + ', ' + str(NCSParameters[self.tcas_ra_time_worst].dz) + ', '
      csvStr += str(aircraftTCAS[self.tcas_ra_time].time_crit) + ', ' + str(aircraftTCAS[self.tcas_ra_time].dist_crit) + ', ' + str(aircraftTCAS[self.tcas_ra_time_worst].time_crit) + ', ' + str(aircraftTCAS[self.tcas_ra_time_worst].dist_crit) + ', '

    else:
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '


    # Sep violation parameters
    csvStr += str(self.sep_violated) + ', ' + str(self.sep_relevant) + ', ' + str(self.sep_time) + ', '
    if self.sep_violated:
      csvStr += str(NCSParameters[self.sep_time].beta) + ', ' + str(NCSParameters[self.sep_time].range) + ', ' + str(NCSParameters[self.sep_time].azimuth) + ', ' + str(NCSParameters[self.sep_time].elevation) + ', ' + str(NCSParameters[self.sep_time].dz) + ', '
    else:
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '

    csvStr += str(self.stca_violated) + ', ' + str(self.stca_relevant) + ', ' + str(self.stca_time) + ', '
    if self.stca_violated:
      csvStr += str(NCSParameters[self.stca_time].beta) + ', ' + str(NCSParameters[self.stca_time].range) + ', ' + str(NCSParameters[self.stca_time].azimuth) + ', ' + str(NCSParameters[self.stca_time].elevation) + ', ' + str(NCSParameters[self.stca_time].dz) + ', '
    else:
      csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '


    # NMAC LMV violation parameters
    csvStr += str(self.lmv_activated) + ', ' + str(self.lmv_violated) + ', ' + str(self.lmv_first_time) + ', ' + str(self.lmv_worst_time) + ', ' + str(self.lmv_first_time_prediction) + ', ' + str(self.lmv_worst_distance) + ', ' + str(self.lmv_violated_time) + ', '
    if self.lmv_violated:
        csvStr += str(NCSParameters[self.lmv_violated_time].beta) + ', ' + str(NCSParameters[self.lmv_violated_time].range) + ', ' + str(NCSParameters[self.lmv_violated_time].azimuth) + ', ' + str(
            NCSParameters[self.lmv_violated_time].elevation) + ', ' + str(NCSParameters[self.lmv_violated_time].dz) + ', '
    else:
        csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '

    # NMAC LMV violation parameters
    csvStr += str(self.haz_lmv_violated) + ', ' + str(self.haz_lmv_violated_time) + ', '
    if self.haz_lmv_violated:
        csvStr += str(NCSParameters[self.haz_lmv_violated_time].beta) + ', ' + str(NCSParameters[self.haz_lmv_violated_time].range) + ', ' + str(NCSParameters[self.haz_lmv_violated_time].azimuth) + ', ' + str(NCSParameters[self.haz_lmv_violated_time].elevation) + ', ' + str(NCSParameters[self.haz_lmv_violated_time].dz) + ', '
    else:
        csvStr += str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', ' + str(0) + ', '


    # Threshold violation parameters
    #csvStr += str(self.prev_number) + ', ' + str(self.prev_type) + ', ' + str(self.prev_time) + ', ' + str(self.prev_tt_haz) + ', ' + str(self.prev_first_type) + ', ' + str(self.prev_first_time) + ', ' + str(self.prev_first_tt_haz) + ', '
    #csvStr += str(self.corr_number) + ', ' + str(self.corr_type) + ', ' + str(self.corr_time) + ', ' + str(self.corr_tt_haz) + ', ' + str(self.corr_first_type) + ', ' + str(self.corr_first_time) + ', ' + str(self.corr_first_tt_haz) + ', '
    #csvStr += str(self.warn_number) + ', ' + str(self.warn_type) + ', ' + str(self.warn_time) + ', ' + str(self.warn_tt_haz) + ', ' + str(self.warn_first_type) + ', ' + str(self.warn_first_time) + ', ' + str(self.warn_first_tt_haz)

    return csvStr


  def report_Alert_Measuring(self):

    if self.prev_type != at.alertType.noAlert:
      print("Preventive caution first activated at time: ", self.prev_time, " Type: ", self.prev_type)

    if self.caution_type != at.alertType.noAlert:
      print("Corrective caution first activated at time: ", self.caution_time, " Type: ", self.caution_type)

    if self.warn_type != at.alertType.noAlert:
      print("Warning caution first activated at time: ", self.warn_time, " Type: ", self.warn_type)

    if self.haz_violated:
      print("HAZ has been violated: ", " First time at: ", self.haz_first_time, " TCPA: ", self.haz_first_tcpa)
      print("Worst SLoWC at: ", self.haz_tcpa_time, " TCPA: ", self.haz_tcpa,
            " SLoWC: ", self.SLoWC, " RangePen: ", self.RangePen, " HMDPen: ", self.HMDPen, " VertPen: ", self.VertPen)
      print("Worst Slat at: ", self.haz_slat_time, " Slat: ", self.min_slat_range)

    if self.nmac_violated:
      print("NMAC has been violated: ", " Time: ", self.nmac_time, " WSR: ", self.WSR, " HMD: ", self.HMD, " VMD: ", self.VMD)


  def record_NMAC_parameters(self, time, own_pos, int_pos, nmac_param):

    so = np.array([own_pos.x_loc, own_pos.y_loc])
    si = np.array([int_pos.x_loc, int_pos.y_loc])

    s = si - so
    r = np.linalg.norm(s, 2)
    dz = abs(int_pos.altitude - own_pos.altitude)

    wsr = math.sqrt(np.dot(s, s)/25 + dz**2)

    cond1 = r <= nmac_param.HNMAC
    cond2 = dz <= nmac_param.VNMAC

    if cond1 and cond2: # NMAC is violated
      self.active_conflict = True

      if not self.nmac_violated:  # NMAC violation occurs for the first time
        self.nmac_violated = True
        self.HMD = r
        self.VMD = dz
        self.WSR = wsr
        self.nmac_time = time
        self.nmac_first_time = time
      else:
        #self.nmac_time = time # TBD: mirar per si es un bug
        if wsr < self.WSR:
          self.WSR = wsr
          self.HMD = r
          self.VMD = dz
          self.nmac_time = time

        #if math.isclose(r, self.HMD):
        #  if dz < self.VMD:
        #    self.VMD = dz
        #    self.nmac_time = time
        #elif r < self.HMD:
        #  self.HMD = r
        #  self.VMD = dz
        #  self.nmac_time = time


  def record_CPA_parameters(self, time, own_pos, int_pos):
    so3 = np.array([own_pos.x_loc, own_pos.y_loc, own_pos.altitude])
    si3 = np.array([int_pos.x_loc, int_pos.y_loc, int_pos.altitude])
    s3 = si3 - so3
    slat = math.sqrt(np.dot(s3, s3))

    if self.minSlat == -1 or slat < self.minSlat:
        self.minSlatTime = time
        self.minSlat = slat

    so2 = np.array([own_pos.x_loc, own_pos.y_loc])
    si2 = np.array([int_pos.x_loc, int_pos.y_loc])
    s2 = si2 - so2
    range = math.sqrt(np.dot(s2, s2))

    if self.minHDist == -1 or range < self.minHDist:
        self.minHDistTime = time
        self.minHDist = range


  def record_SLOWC_parameters(self, time, xtcpa, own_pos, int_pos, rwc_param):
    self.active_conflict = True

    so = np.array([own_pos.x_loc, own_pos.y_loc])
    si = np.array([int_pos.x_loc, int_pos.y_loc])
    vo = np.array([own_pos.x_v, own_pos.y_v])
    vi = np.array([int_pos.x_v, int_pos.y_v])

    so3 = np.array([own_pos.x_loc, own_pos.y_loc, own_pos.altitude])
    si3 = np.array([int_pos.x_loc, int_pos.y_loc, int_pos.altitude])

    s = si - so
    v = vi - vo
    s3 = si3 - so3

    r = np.linalg.norm(s, 2)
    rdot = np.dot(s, v) / r
    rdot_factor = 0.5 * math.sqrt((rdot * rwc_param.TAUMOD) ** 2 + 4 * rwc_param.DMOD ** 2) - rdot * rwc_param.TAUMOD
    hr = max(rwc_param.DMOD, rdot_factor)
    RangePen = min(r/hr, 1)    # SLOWC Horizontal Proximity

    tcpa = -np.dot(s, v) / np.sum(np.power(v, 2))
    hmdp = math.sqrt((s[0] + v[0] * tcpa) ** 2 + (s[1] + v[1] * tcpa) ** 2)
    HMDPen = min(hmdp/rwc_param.DMOD, 1)   # HMDPen Horizontal Miss Distance Projection

    dz = abs(int_pos.altitude - own_pos.altitude)
    VertPen = min(dz / rwc_param.H, 1)     # VertPen Vertical Separation

    SLoWC = 1 - squircle(RangePen, squircle(HMDPen, VertPen))

    slat = math.sqrt(np.dot(s3, s3))

    if self.haz_violated == False: # Records the first time HAZ is violated
      self.haz_violated = True
      self.haz_first_time = time  # Time of first violation
      self.haz_first_tcpa = xtcpa  # Time of first violation
      self.haz_first_dcpa = hmdp  # Time of first violation
      self.haz_first_SLoWC = SLoWC
      self.haz_first_RangePen = RangePen
      self.haz_first_HMDPen = HMDPen
      self.haz_first_VertPen = VertPen
      self.SLoWC = SLoWC
      self.RangePen = RangePen
      self.HMDPen = HMDPen
      self.VertPen = VertPen
      self.haz_SLoWC_time = time
      self.haz_tcpa = xtcpa
      self.haz_dcpa = hmdp
      self.haz_tcpa_time = time
      self.min_slat_range = slat
      self.haz_slat_time = time

    else:
      if SLoWC > self.SLoWC: # Records the maximum SLoWC factor
        self.SLoWC = SLoWC
        self.RangePen = RangePen
        self.HMDPen = HMDPen
        self.VertPen = VertPen
        self.haz_SLoWC_time = time

      if tcpa > 0: # Records the last time in which TCPA is positive
        self.haz_tcpa_time = time
        self.haz_tcpa = xtcpa
        self.haz_dcpa = hmdp

      if slat < self.min_slat_range: # Records the minimum slat range time
        self.haz_slat_time = time
        self.min_slat_range = slat


  def record_Sep_parameters(self, time, own_pos, int_pos, sep_param):

    if not self.sep_violated and sep.check_separation_volume(own_pos, int_pos, sep_param):
      self.sep_violated = True
      self.sep_time = time

      if time >= 5:
        self.sep_relevant = True
      else:
        self.sep_relevant = False

    if not self.sep_violated and not self.stca_violated and self.stcao.computeStcaConflict(own_pos, int_pos):
      self.stca_violated = True
      self.stca_time = time

      if time >= 1:
        self.stca_relevant = True
      else:
        self.stca_relevant = False


  def record_LMV_parameters(self, time, own_pos, int_pos, timePrediction, prediction):
    if not self.lmv_activated:
        self.lmv_activated = True
        self.lmv_first_time = time
        self.lmv_first_time_prediction = timePrediction
        self.lmv_worst_time = time
        self.lmv_worst_distance = prediction.minDistance
    elif prediction.minDistance < self.lmv_worst_distance:
        self.lmv_worst_time = time
        self.lmv_worst_distance = prediction.minDistance

    if not self.lmv_violated and prediction.violates:
        self.lmv_violated = True
        self.lmv_violated_time = time


  def record_HAZ_LMV_parameters(self, time, own_pos, int_pos):
        self.haz_lmv_violated = True
        self.haz_lmv_violated_time = time


  # If HAZ occurs without a previous Caution, we need to clean up LMV
  def clean_HAZ_LMV_parameters(self):
    self.haz_lmv_violated = False
    self.haz_lmv_violated_time = -1


  def HAZ_LMV_violated(self):
    return self.haz_lmv_violated


  def HAZ_LMV_violated_time(self):
    return self.haz_lmv_violated_time


  def record_TCAS3D_parameters(self, time, conflictData, own_pos, int_pos, tcas_param):
    if not self.tcas_ra:
      self.tcas_ra = True
      self.tcas_ra_time = time
      self.tcas_ra_time_worst = time
      self.tcas_ra_worst = conflictData.dist_crit
    elif conflictData.dist_crit < self.tcas_ra_worst:
      self.tcas_ra_time_worst = time
      self.tcas_ra_worst = conflictData.dist_crit


  def set_warning(self, time, tt_haz, type):
    self.active_conflict = True
    if not self.warn_active:
      self.warn_active = True
      self.warn_time = time
      self.warn_tt_haz = tt_haz
      self.warn_type = type
      if self.warn_number == 0:
        self.warn_first_time = time
        self.warn_first_tt_haz = tt_haz
        self.warn_first_type = type
      self.warn_number += 1
      self.warn_timer = 0


  def set_caution(self, time, tt_haz, type):
    self.active_conflict = True
    if not self.caution_active:
      self.caution_active = True
      self.caution_time = time
      self.caution_tt_haz = tt_haz
      self.caution_type = type
      if self.caution_number == 0:
        self.caution_first_time = time
        self.caution_first_tt_haz = tt_haz
        self.caution_first_type = type
      self.caution_number += 1
      self.caution_timer = 0


  def set_preventive(self, time, tt_haz, type):
    self.active_conflict = True
    if not self.prev_active:
      self.prev_active = True
      self.prev_time = time
      self.prev_tt_haz = tt_haz
      self.prev_type = type
      if self.prev_number == 0:
        self.prev_first_time = time
        self.prev_first_tt_haz = tt_haz
        self.prev_first_type = type
      self.prev_number += 1
      self.prev_timer = 0


  def inTimeLimit(self, time):
      #return time <= 240 and not self.haz_violated
      return not self.haz_violated


  def end_Warning(self, time):
        if self.warn_active:
            if self.warn_first_end_time == -1:
                self.warn_first_end_time = time
            self.warn_end_time = time
            self.warn_active = False
            self.warn_timer = 0


  def record_Warning_parameters(self, time, interval, tcpa, rwc_param, debug):

    if self.warn_active:
      # Empty interval but some warning detected previously
      # We increase a timer to apply an hysteresis function
      if interval.isEmpty() and self.warn_time >= 0:
        self.warn_timer += 1
      # Non-empty interval, but lower larger than earlyTime but some warning detected previously
      # We increase a timer to apply an hysteresis function
      elif not interval.isEmpty() and interval.low > rwc_param.early:
        self.warn_timer += 1
      # Non-empty interval, and lower smaller than earlyTime, some warning detected previously
      # Hysteresis limit not reached
      # We reset the timer to apply an hysteresis function
      elif self.warn_timer < TIMER_HISTERESIS and not interval.isEmpty() and interval.low <= rwc_param.early:
        self.warn_timer = 0
      # Non-empty interval, and lower smaller than earlyTime, some warning detected previously
      # Hysteresis limit already reached
      # We assume it is a new conflict and we reset the whole analysis
      if self.warn_timer == TIMER_HISTERESIS:
        self.end_Warning(time)
        #self.warn_active = False
        #self.warn_timer = 0
        if (debug):
          print("Warning alarm deactivated at time: ", time)

    if not self.warn_active and self.inTimeLimit(time):
      # If interval is empty and no previous alert, then we can just return
      if interval.isEmpty():
        return False
      # If interval is not empty and no previous alert, but lower larger than earlyTime
      elif not interval.isEmpty() and interval.low > rwc_param.early:
        return False
      # Seems we are already in HAZ
      elif math.isclose(interval.low, 0):   # Seems we are already in HAZ
        self.set_warning(time, interval.low, at.alertType.lateIn)
      elif interval.low <= rwc_param.late:       # Late entry to HAZ
        self.set_warning(time, interval.low, at.alertType.lateAlert)
        if (debug):
          print("Warning activated at time: ", time, " Time to HAZ: ", interval.low, " Type: ", str(at.alertType.lateAlert))
      elif interval.low <= rwc_param.average:  # Minimum average entry to HAZ
        self.set_warning(time, interval.low, at.alertType.averageAlert)
        if (debug):
          print("Warning activated at time: ", time, " Time to HAZ: ", interval.low, " Type: ", str(at.alertType.averageAlert))
      elif (interval.low < rwc_param.early) or (tcpa <= rwc_param.pCPA):  # Early entry to HAZ
        self.set_warning(time, interval.low, at.alertType.earlyAlert)
        if (debug):
          print("Warning activated at time: ", time, " Time to HAZ: ", interval.low, " Type: ", str(at.alertType.earlyAlert))

    return self.warn_active


  def end_Caution(self, time):
        if self.caution_active:
            if self.caution_first_end_time == -1:
                self.caution_first_end_time = time
            self.caution_end_time = time
            self.caution_active = False
            self.caution_timer = 0


  def record_Caution_parameters(self, time, interval, tcpa, rwc_param, debug):

    if self.caution_active:
      # Empty interval but some corrective detected previously
      # We increase a timer to apply an hysteresis function
      if interval.isEmpty() and self.caution_time >= 0:
        self.caution_timer += 1
      # Non-empty interval, but lower larger than earlyTime but some corrective detected previously
      # We increase a timer to apply an hysteresis function
      elif not interval.isEmpty() and interval.low > rwc_param.early:
        self.caution_timer += 1
      # Non-empty interval, and lower smaller than earlyTime, some corrective detected previously
      # Hysteresis limit not reached
      # We reset the timer to apply an hysteresis function
      elif self.caution_timer < TIMER_HISTERESIS and not interval.isEmpty() and interval.low <= rwc_param.early:
        self.caution_timer = 0
      # Non-empty interval, and lower smaller than earlyTime, some corrective detected previously
      # Hysteresis limit already reached
      # We assume it is a new conflict and we reset the whole analysis
      if self.caution_timer == TIMER_HISTERESIS:
        self.end_Caution(time)
        #self.caution_end_time = time
        #self.caution_active = False
        #self.caution_timer = 0
        if (debug):
          print("Corrective alarm deactivated at time: ", time)

    if not self.caution_active and self.inTimeLimit(time):
      # If interval is empty and no previous alert, then we can just return
      if interval.isEmpty():
        return False
      # If interval is not empty and no previous alert, but lower larger than earlyTime
      elif not interval.isEmpty() and interval.low > rwc_param.early:
        return False
      elif interval.low <= rwc_param.late:       # Late entry to HAZ
        self.set_caution(time, interval.low, at.alertType.lateAlert)
        if (debug):
          print("Corrective activated at time: ", time, " Time to HAZ: ", interval.low, " Type: ", str(at.alertType.lateAlert))
      elif interval.low <= rwc_param.average:  # Minimum average entry to HAZ
        self.set_caution(time, interval.low, at.alertType.averageAlert)
        if (debug):
          print("Corrective activated at time: ", time, " Time to HAZ: ", interval.low, " Type: ", str(at.alertType.averageAlert))
      elif (interval.low < rwc_param.early) or (tcpa <= rwc_param.pCPA):  # Early entry to HAZ
        self.set_caution(time, interval.low, at.alertType.earlyAlert)
        if (debug):
          print("Corrective activated at time: ", time, " Time to HAZ: ", interval.low, " Type: ", str(at.alertType.earlyAlert))

      if self.caution_active:
        self.haz_lmv_violated = False

    return self.caution_active


  def end_Preventive(self, time):
        if self.prev_active:
            if self.prev_first_end_time == -1:
                self.prev_first_end_time = time
            self.prev_end_time = time
            self.prev_active = False
            self.prev_timer = 0


  def record_Preventive_parameters(self, time, interval, tcpa, rwc_param, debug):

    if self.prev_active:
      # Empty interval but some preventive detected previously
      # We increase a timer to apply an hysteresis function
      if interval.isEmpty() and self.prev_time >= 0:
        self.prev_timer += 1
      # Non-empty interval, but lower larger than earlyTime but some preventive detected previously
      # We increase a timer to apply an hysteresis function
      elif not interval.isEmpty() and interval.low > rwc_param.early:
        self.prev_timer += 1
      # Non-empty interval, and lower smaller than earlyTime, some preventive detected previously
      # Hysteresis limit not reached
      # We reset the timer to apply an hysteresis function
      elif self.prev_timer < TIMER_HISTERESIS and not interval.isEmpty() and interval.low <= rwc_param.early:
        self.prev_timer = 0
      # Non-empty interval, and lower smaller than earlyTime, some preventive detected previously
      # Hysteresis limit already reached
      # We assume it is a new conflict and we reset the whole analysis
      if self.prev_timer == TIMER_HISTERESIS:
        self.end_Preventive(time)
        #self.prev_active = False
        #self.prev_timer = 0
        if (debug):
          print("Preventive alarm deactivated at time: ", time)

    if not self.prev_active and self.inTimeLimit(time):
      # If interval is empty and no previous alert, then we can just return
      if interval.isEmpty():
        return False
      # If interval is not empty and no previous alert, but lower larger than earlyTime
      elif not interval.isEmpty() and interval.low > rwc_param.early:
        return False
      elif interval.low <= rwc_param.late:       # Late entry to HAZ
        self.set_preventive(time, interval.low, at.alertType.lateAlert)
        if (debug):
          print("Preventive activated at time: ", time, " Time to HAZ: ", interval.low, " Type: ", str(at.alertType.lateAlert))
      elif interval.low <= rwc_param.average:  # Minimum average entry to HAZ
        self.set_preventive(time, interval.low, at.alertType.averageAlert)
        if (debug):
          print("Preventive activated at time: ", time, " Time to HAZ: ", interval.low, " Type: ", str(at.alertType.averageAlert))
      elif (interval.low < rwc_param.early) or (tcpa <= rwc_param.pCPA):  # Early entry to HAZ
        self.set_preventive(time, interval.low, at.alertType.earlyAlert)
        if (debug):
          print("Preventive activated at time: ", time, " Time to HAZ: ", interval.low, " Type: ", str(at.alertType.earlyAlert))

    return self.prev_active

