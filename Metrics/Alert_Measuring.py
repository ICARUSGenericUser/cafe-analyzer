from datetime import datetime
from RWC import RWC_alert_times as at
from Encounter import encounter_ref as eref
from Encounter import aircraft_class as cls


def safeDiv(div, dividend):
  if dividend != 0:
    return "{:.3f}".format(div/dividend)
  else:
    return ("NaN")


class Alert_Measuring:
  def __init__(self):
    self.aircraft1Class = 0
    self.aircraft2Class = 0

    self.nEncounters = 0    # Total number of processed encounters
    self.nConflicts = 0     # Total number of conflict encounters
    self.nNMAC = 0          # Total number of NMAC violations
    self.nLMV = 0           # Total number of LMV violations
    self.nLMVnoNMAC = 0  # Total number of LMV violations
    self.nTLMV = 0          # Total number of LMV triggers
    self.nTLMVnoNMAC = 0          # Total number of LMV triggers
    self.nHAZ = 0           # Total number of HAZ violations
    self.nWarning = 0       # Total number of Warning threshold activations
    self.nCorrective = 0    # Total number of Corrective threshold activations
    self.nPreventive = 0    # Total number of Preventive threshold activations

    self.nOnlyPreventive = 0
    self.nEarlyPreventive = 0  # Total number of early Preventive threshold activations without Preventive
    self.nEarlyPreventiveAvTime = 0
    self.nAveragePreventive = 0  # Total number of average Preventive threshold activations without Preventive
    self.nAveragePreventiveAvTime = 0
    self.nLatePreventive = 0  # Total number of late Preventive threshold activations without Preventive
    self.nLatePreventiveAvTime = 0
    self.nEarlyCorrective = 0  # Total number of early Corrective threshold activations without Preventive
    self.nEarlyCorrectiveAvTime = 0
    self.nAverageCorrective = 0  # Total number of average Corrective threshold activations without Preventive
    self.nAverageCorrectiveAvTime = 0
    self.nWarningNCorrective = 0  # Total number of Warning threshold activations without Preventive
    self.nWarningNCorrectiveAvTime = 0
    self.nAverageWarningNCorrective = 0  # Total number of average Warning threshold activations without Preventive
    self.nAverageWarningNCorrectiveAvTime = 0
    self.nEarlyWarningNCorrective = 0  # Total number of early Warning threshold activations without Preventive
    self.nEarlyWarningNCorrectiveAvTime = 0
    self.nHAZNWarning = 0         # Total number of HAZ volume violations without Warning threshold activation
    self.nHAZNWarningAvTime = 0
    self.nSeparation = 0
    self.nRelevantSep = 0

    self.relevantEncounters = []


  def addEncounter(self, aircraft1Class, aircraft2Class):
    self.nEncounters = self.nEncounters + 1
    self.aircraft1Class = aircraft1Class
    self.aircraft2Class = aircraft2Class


  def addConflict(self, conf1, conf2):
    if (cls.isOwnshipRPAS(self.aircraft1Class) and conf1) or (cls.isOwnshipRPAS(self.aircraft2Class) and conf2):
      self.nConflicts = self.nConflicts + 1


  def addTimming(self, encounter, alertMeasure):

      # Average corrective with no preventive
      if alertMeasure.prev_type == at.alertType.earlyAlert:
        self.nEarlyPreventive += 1
        self.nEarlyPreventiveAvTime += alertMeasure.prev_tt_haz

      # Average corrective with no preventive
      if alertMeasure.prev_type == at.alertType.averageAlert:
        self.nAveragePreventive += 1
        self.nAveragePreventiveAvTime += alertMeasure.prev_tt_haz

      # Average corrective with no preventive
      if alertMeasure.prev_type == at.alertType.lateAlert:
        self.nLatePreventive += 1
        self.nLatePreventiveAvTime += alertMeasure.prev_tt_haz

      # No corrective nor warning with no preventive
      if alertMeasure.prev_type != at.alertType.noAlert and alertMeasure.caution_type == at.alertType.noAlert and alertMeasure.warn_type == at.alertType.noAlert:
        self.nOnlyPreventive += 1

      # Average corrective with no preventive
      if alertMeasure.prev_type == at.alertType.noAlert and alertMeasure.caution_type == at.alertType.averageAlert:
        self.nAverageCorrective += 1
        self.nAverageCorrectiveAvTime += alertMeasure.caution_tt_haz

      # Early corrective with no preventive
      if alertMeasure.prev_type == at.alertType.noAlert and alertMeasure.caution_type == at.alertType.earlyAlert:
        self.nEarlyCorrective += 1
        self.nEarlyCorrectiveAvTime += alertMeasure.caution_tt_haz

      # Some warning with no corrective
      if alertMeasure.caution_type == at.alertType.noAlert and alertMeasure.warn_type != at.alertType.noAlert:
        self.nWarningNCorrective += 1
        self.nWarningNCorrectiveAvTime += alertMeasure.warn_tt_haz

      # Early warning with no corrective
      if alertMeasure.caution_type == at.alertType.noAlert and alertMeasure.warn_type == at.alertType.earlyAlert:
        self.nEarlyWarningNCorrective += 1
        self.nEarlyWarningNCorrectiveAvTime += alertMeasure.warn_tt_haz

      # Average warning with no corrective
      if alertMeasure.caution_type == at.alertType.noAlert and alertMeasure.warn_type == at.alertType.averageAlert:
        self.nAverageWarningNCorrective += 1
        self.nAverageWarningNCorrectiveAvTime += alertMeasure.warn_tt_haz

      # HAZ violated with no warning
      if alertMeasure.warn_type == at.alertType.noAlert and alertMeasure.haz_violated == True:
        self.nHAZNWarning += 1
        self.nHAZNWarningAvTime += alertMeasure.haz_first_tcpa

      # Update of the relevant encounters list: select the desired IF clause
      # Warning with no preventive or corrective
      #if alertMeasure.prev_type == at.alertType.noAlert and alertMeasure.corr_type == at.alertType.noAlert and alertMeasure.warn_type != at.alertType.noAlert:

      # HAZ violated with no warning
      if alertMeasure.warn_type == at.alertType.noAlert and alertMeasure.haz_violated == True:
        ref = eref.Encounter_ref(encounter.id, encounter.layer, encounter.type, encounter.aircraft1Class, encounter.aircraft2Class)
        self.relevantEncounters.append(ref)



  def addNMAC(self, nmac1, nmac2):
    if (cls.isOwnshipRPAS(self.aircraft1Class) and nmac1) or (cls.isOwnshipRPAS(self.aircraft2Class) and nmac2):
      self.nNMAC = self.nNMAC + 1


  def addLMV(self, tlmv1, lmv1, tlmv2, lmv2):
    if (cls.isOwnshipRPAS(self.aircraft1Class) and lmv1) or (cls.isOwnshipRPAS(self.aircraft2Class) and lmv2):
      self.nLMV = self.nLMV + 1
    if (cls.isOwnshipRPAS(self.aircraft1Class) and tlmv1) or (cls.isOwnshipRPAS(self.aircraft2Class) and tlmv2):
      self.nTLMV = self.nTLMV + 1


  def addLMVnoNMAC(self, tlmv1, lmv1, tlmv2, lmv2):
    if (cls.isOwnshipRPAS(self.aircraft1Class) and lmv1) or (cls.isOwnshipRPAS(self.aircraft2Class) and lmv2):
      self.nLMVnoNMAC = self.nLMVnoNMAC + 1
    if (cls.isOwnshipRPAS(self.aircraft1Class) and tlmv1) or (cls.isOwnshipRPAS(self.aircraft2Class) and tlmv2):
      self.nTLMVnoNMAC = self.nTLMVnoNMAC + 1


  def addSeparation(self, sep1, relevant1, sep2, relevant2):
    if (cls.isOwnshipRPAS(self.aircraft1Class) and sep1) or (cls.isOwnshipRPAS(self.aircraft2Class) and sep2):
      self.nSeparation = self.nSeparation + 1
    if (cls.isOwnshipRPAS(self.aircraft1Class) and relevant1) or (cls.isOwnshipRPAS(self.aircraft2Class) and relevant2):
      self.nRelevantSep = self.nRelevantSep + 1


  def addHazard(self, haz1, haz2):
    if cls.isOwnshipRPAS(self.aircraft1Class) and haz1:
      self.nHAZ = self.nHAZ + 1
    if cls.isOwnshipRPAS(self.aircraft2Class) and haz2:
      self.nHAZ = self.nHAZ + 1


  def addWarning(self, type1, type2):
    if cls.isOwnshipRPAS(self.aircraft1Class) and (type1 != at.alertType.noAlert):
      self.nWarning = self.nWarning + 1
    if cls.isOwnshipRPAS(self.aircraft2Class) and (type2 != at.alertType.noAlert):
      self.nWarning = self.nWarning + 1


  def addCorrective(self, type1, type2):
    if cls.isOwnshipRPAS(self.aircraft1Class) and (type1 != at.alertType.noAlert):
      self.nCorrective = self.nCorrective + 1
    if cls.isOwnshipRPAS(self.aircraft2Class) and (type2 != at.alertType.noAlert):
      self.nCorrective = self.nCorrective + 1


  def addPreventive(self, type1, type2):
    if cls.isOwnshipRPAS(self.aircraft1Class) and (type1 != at.alertType.noAlert):
      self.nPreventive = self.nPreventive + 1
    if cls.isOwnshipRPAS(self.aircraft2Class) and (type2 != at.alertType.noAlert):
      self.nPreventive = self.nPreventive + 1


  def writeReport(self, filename, duration):

    now = datetime.now()  # current date and time
    date_time = now.strftime("%m/%d/%Y, %H:%M:%S")

    print("Encounter_Distribution set report at: filename")
    print("Encounter_Distribution set report at date: ", date_time, " generated in ", "{:.2f}".format(duration), " sec", "\n")
    print("Total number of encounters: ", self.nEncounters, " Total conflicts: ", self.nConflicts, " P(CONF) = ", safeDiv(self.nConflicts, self.nEncounters))
    print("HAZ Thresholds, Preventive: ", self.nPreventive, " Corrective: ", self.nCorrective, " Warning:", self.nWarning)
    print("HAZ Violations: ", self.nHAZ, " NMAC Violations: ", self.nNMAC, " P(HAZ) = ", safeDiv(self.nHAZ, self.nEncounters), " P(NMAC) = ", safeDiv(self.nNMAC, self.nEncounters))
    print("LMV Triggers: ", self.nTLMV, " LMV Violations: ", self.nLMV)
    print("LMV Triggers (no NMAC): ", self.nTLMVnoNMAC, " LMV Violations (no NMAC): ", self.nLMVnoNMAC)
    print("Number of separation violations: ", self.nSeparation, " Total relevant: ", self.nRelevantSep)
    print("")
    print("Encounter_Distribution performance report.")
    print("Total number of earlyPreventives: ", self.nEarlyPreventive, " averagePreventives: ", self.nAveragePreventive, " latePreventives: ", self.nLatePreventive, " totalPreventives: ", self.nPreventive)
    print("Total number of onlyPreventives: ", self.nOnlyPreventive)
    print("Total number of no Preventive earlyCorrectives: ", self.nEarlyCorrective, " averageCorrectives: ", self.nAverageCorrective, " totalCorrectives: ", self.nCorrective)
    print("Total number of no Corrective earlyWarning: ", self.nEarlyWarningNCorrective, " averageWarning: ", self.nAverageWarningNCorrective, " totalWarnings: ", self.nWarningNCorrective)
    print("Total HAZ violations without Warnings: ", self.nHAZNWarning)
    print("")
    print("Average timing report.")
    print("Av time for earlyPreventives: ", safeDiv(self.nEarlyPreventiveAvTime, self.nEarlyPreventive),
          " averagePreventives: ", safeDiv(self.nAveragePreventiveAvTime, self.nAveragePreventive),
          " latePreventives: ", safeDiv(self.nLatePreventiveAvTime, self.nLatePreventive))
    print("Av time for no Preventive earlyCorrectives: ", safeDiv(self.nEarlyCorrectiveAvTime, self.nEarlyCorrective), " averageCorrectives: ", safeDiv(self.nAverageCorrectiveAvTime, self.nAverageCorrective))
    print("Av time for no Corrective earlyWarning: ", safeDiv(self.nEarlyWarningNCorrectiveAvTime, self.nEarlyWarningNCorrective), " lateWarning: ",
          safeDiv(self.nAverageWarningNCorrectiveAvTime, self.nAverageWarningNCorrective), " totalWarnings: ", safeDiv(self.nWarningNCorrectiveAvTime, self.nWarningNCorrective))
    print("Av time for HAZ violations without Warnings: ", safeDiv(self.nHAZNWarningAvTime, self.nHAZNWarning))
    self.nSeparation = 0
    self.nRelevantSep = 0
    print("")

    f = open(filename, "w")
    f.write("Encounter_Distribution set report at date: " + date_time + " generated in " + "{:.2f}".format(duration) + " sec" + "\n\n")
    txtStr = "Total number of encounters: " + str(self.nEncounters) + " Total conflicts: " + str(self.nConflicts) + " P(CONF) = " + safeDiv(self.nConflicts, self.nEncounters) + "\n"
    f.write(txtStr)
    txtStr = "HAZ Thresholds. Preventive:" + str(self.nPreventive) + " Corrective: " +  str(self.nCorrective) + " Warning: " + str(self.nWarning) + "\n"
    f.write(txtStr)
    txtStr = "HAZ Violations: " +  str(self.nHAZ) + " NMAC Violations: " + str(self.nNMAC) + " P(HAZ) = " + safeDiv(self.nHAZ, self.nEncounters) + " P(NMAC) = " +  safeDiv(self.nNMAC, self.nEncounters)  + "\n"
    f.write(txtStr)
    txtStr = "LMV Triggers: " + str(self.nTLMV) + " LMV Violations: " + str(self.nLMV)
    f.write(txtStr)
    txtStr = "LMV Triggers (no NMAC): " + str(self.nTLMVnoNMAC) + " LMV Violations (no NMAC): " + str(self.nLMVnoNMAC)
    f.write(txtStr)
    f.write("" + "\n")
    f.write("Encounter_Distribution performance report." + "\n")
    txtStr = "Total number of earlyPreventives: " + str(self.nEarlyPreventive) + " averagePreventive: " + str(self.nAveragePreventive) + " latePreventive: " + str(self.nLatePreventive) + " totalPreventive: " + str(self.nPreventive) + "\n"
    f.write(txtStr)
    txtStr = "Total number of onlyPreventives: " + str(self.nOnlyPreventive) + "\n"
    f.write(txtStr)
    txtStr = "Total number of no Preventive earlyCorrectives: " + str(self.nEarlyCorrective) + " averageCorrectives: " + str(self.nAverageCorrective) + " totalCorrectives: " + str(self.nCorrective) + "\n"
    f.write(txtStr)
    txtStr = "Total number of no Corrective earlyWarning: " + str(self.nEarlyWarningNCorrective) + " averageWarning: " + str(self.nAverageWarningNCorrective) + " totalWarnings: " + str(self.nWarningNCorrective) + "\n"
    f.write(txtStr)
    txtStr = "Total HAZ violations without Warnings: " + str(self.nHAZNWarning) + "\n"
    f.write(txtStr)
    f.write("" + "\n")
    f.write("Average timing report." + "\n")
    txtStr = "Av time for earlyPreventives: " + safeDiv(self.nEarlyPreventiveAvTime, self.nEarlyPreventive) + " averagePreventives: " + safeDiv(self.nAveragePreventiveAvTime, self.nAveragePreventive) + \
          " latePreventives: " + safeDiv(self.nLatePreventiveAvTime, self.nLatePreventive) + "\n"
    f.write(txtStr)
    txtStr = "Av time for no Preventive earlyCorrectives: " + safeDiv(self.nEarlyCorrectiveAvTime, self.nEarlyCorrective) + " averageCorrectives: " + safeDiv(self.nAverageCorrectiveAvTime, self.nAverageCorrective) + "\n"
    f.write(txtStr)
    txtStr = "Av time for no Corrective earlyWarning: " + safeDiv(self.nEarlyWarningNCorrectiveAvTime, self.nEarlyWarningNCorrective) + " averageWarning: " + \
             safeDiv(self.nAverageWarningNCorrectiveAvTime, self.nAverageWarningNCorrective) + " totalWarnings: " + safeDiv(self.nWarningNCorrectiveAvTime, self.nWarningNCorrective) + "\n"
    f.write(txtStr)
    txtStr = "Av time for HAZ violations without Warnings: " + safeDiv(self.nHAZNWarningAvTime, self.nHAZNWarning) + "\n"
    f.write(txtStr)
    f.write("" + "\n")

    if len(self.relevantEncounters) > 0:
      f.write("Relevant encounters:" + "\n")
      i = 0
      while i < len(self.relevantEncounters):
        f.write("Id: " + self.relevantEncounters[i].id + ", Layer: " + self.relevantEncounters[i].layer + ", Type: " + self.relevantEncounters[i].type + "\n")
        i += 1

    f.close()

