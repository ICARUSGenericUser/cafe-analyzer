
class Encounter_ref:
  def __init__(self, id, layer, type, aircraft1Class, aircraft2Class):
    self.id = id
    self.layer = layer
    self.type = type
    self.aircraft1Class = aircraft1Class
    self.aircraft2Class = aircraft2Class

  def isVoid(self):
    return (self.aircraft1Class == -1) and (self.aircraft2Class == -1)
