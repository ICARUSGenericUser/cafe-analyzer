import os
import math
import copy
import shutil

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from Encounter import units as units, aircraft_class as cls, position as pos
from RWC import RWC_volume as rwct, RWC_thresholds as rwcthr, RWC_interval as itv, HAZ_parameters as rwc, RWC_alert_times as at, \
  NMAC_parameters as nmac, RWC_Horizontal_Contours as rwcc, LMV_violation as lmv, WCV_Trace as trc
from NCSensor import NCS_detection as ncsd, NCS_parameters as ncsp
from TCAS import TCAS3D
from geographiclib import geodesic as geo
from importlib import reload
from RWC import WCV_TAUMOD as Taumod



class Encounter:
  def __init__(self, id, layer, type, aircraft1Class, aircraft2Class, format):
    self.id = id
    self.nAircraft = 0
    self.layer = layer
    self.type = type
    self.enc_format = format
    self.aircraft1Class = aircraft1Class
    self.aircraft2Class = aircraft2Class
    self.aircraft1Callsign = None
    self.aircraft2Callsign = None
    self.aircraft1 = []
    self.aircraft2 = []
    self.aircraftSequence = []
    self.aircraft1RWC = []
    self.aircraft2RWC = []
    self.aircraft1TCAS = []
    self.aircraft2TCAS = []
    self.aircraftNMAC = []
    self.aircraftNLMV1 = []
    self.aircraftNLMV2 = []
    self.aircraft1NCS = []
    self.aircraft2NCS = []
    self.encAlertMeasuringAicraft1 = None
    self.encAlertMeasuringAicraft2 = None


  def isVoid(self):
    return (self.aircraft1Class == -1) and (self.aircraft2Class == -1)


  def addAircraft1Position(self, position):
    self.aircraft1.append(position)


  def addAircraft2Position(self, position):
    self.aircraft2.append(position)


  def addAircraftVectorPosition(self, aircraftVector):
    self.aircraftSequence.append(aircraftVector)


  def numberAircraft1Position(self):
    return len(self.aircraft1)


  def numberAircraft2Position(self):
    return len(self.aircraft2)


  def computeDynamics(self):
    for i in range(len(self.aircraft1) - 1):
      self.aircraft1[i].computeDynamics(self.aircraft1[i + 1])

    for i in range(len(self.aircraft2) - 1):
      self.aircraft2[i].computeDynamics(self.aircraft2[i + 1])

    for i in range(len(self.aircraft1) - 1):
      self.aircraft1[i].computeTurnDynamics(self.aircraft1[i + 1])

    for i in range(len(self.aircraft2) - 1):
      self.aircraft2[i].computeTurnDynamics(self.aircraft2[i + 1])

    self.aircraft1[len(self.aircraft1) - 1].duplicateDynamics(self.aircraft1[len(self.aircraft1) - 2])
    self.aircraft2[len(self.aircraft2) - 1].duplicateDynamics(self.aircraft2[len(self.aircraft2) - 2])


  def computeTurnDynamics(self):

      for i in range(len(self.aircraft1) - 1):
        self.aircraft1[i].computeTurnDynamics(self.aircraft1[i + 1])

      for i in range(len(self.aircraft2) - 1):
        self.aircraft2[i].computeTurnDynamics(self.aircraft2[i + 1])


  def computeDynamicsFactor(self):
    for i in range(len(self.aircraft1) - 1):
      self.aircraft1[i].computeDynamicsFactor(self.aircraft1[i + 1], self.aircraft1[i + 1].time - self.aircraft1[i].time)

    for i in range(len(self.aircraft2) - 1):
      self.aircraft2[i].computeDynamicsFactor(self.aircraft2[i + 1], self.aircraft2[i + 1].time - self.aircraft2[i].time)

    self.aircraft1[len(self.aircraft1) - 1].duplicateDynamics(self.aircraft1[len(self.aircraft1) - 2])
    self.aircraft2[len(self.aircraft2) - 1].duplicateDynamics(self.aircraft2[len(self.aircraft2) - 2])


  def computeDynamicsDEG(self):
    for i in range(len(self.aircraft1) - 1):
      self.aircraft1[i].computeDynamicsDEG(self.aircraft1[i + 1])

    for i in range(len(self.aircraft2) - 1):
      self.aircraft2[i].computeDynamicsDEG(self.aircraft2[i + 1])

    self.aircraft1[len(self.aircraft1) - 1].duplicateDynamics(self.aircraft1[len(self.aircraft1) - 2])
    self.aircraft2[len(self.aircraft2) - 1].duplicateDynamics(self.aircraft2[len(self.aircraft2) - 2])


  def validateDynamicsLatLon(self):
    for i in range(len(self.aircraft1) - 1):
      self.aircraft1[i].validateDynamicsLatLon(self.aircraft1[i + 1])

    for i in range(len(self.aircraft2) - 1):
      self.aircraft2[i].validateDynamicsLatLon(self.aircraft2[i + 1])


  def validateDynamicsXY(self):
    for i in range(len(self.aircraft1) - 1):
      self.aircraft1[i].validateDynamicsXY(self.aircraft1[i + 1])

    for i in range(len(self.aircraft2) - 1):
      self.aircraft2[i].validateDynamicsXY(self.aircraft2[i + 1])


  def duplicateEncounter(self):

    newEncounter = Encounter(self.id, self.layer, self.type, self.aircraft1Class, self.aircraft2Class, self.enc_format)

    newEncounter.aircraft1Callsign = self.aircraft1Callsign
    newEncounter.aircraft2Callsign = self.aircraft2Callsign

    newEncounter.nAircraft = self.nAircraft

    for i in range(len(self.aircraft1)):
      newEncounter.aircraft1.append(self.aircraft1[i].duplicatePosition())
      newEncounter.aircraft2.append(self.aircraft2[i].duplicatePosition())

    return newEncounter



  # Function to export encounter to MOPS format
  # The function will be called from a higher level call that iterates over all necessary encounters
  def writeMOPSFormat(self, root_MOPS_Folder, projection):
    intruderSubpath = "\\" + "Intruder"
    ownshipSubpath = "\\" + "Ownship"

    sub_folder = "\\" + "Layer_" + str(self.layer) + "-Type_" + self.type
    encounter_folder = "\\" + "Encs_" + str(self.id).zfill(9)

    #if not os.path.exists(root_MOPS_Folder + sub_folder):
        #os.makedirs(root_MOPS_Folder + sub_folder)

    if not os.path.exists(root_MOPS_Folder + encounter_folder):
      os.makedirs(root_MOPS_Folder + encounter_folder)
      os.makedirs(root_MOPS_Folder + encounter_folder + intruderSubpath)
      os.makedirs(root_MOPS_Folder + encounter_folder + ownshipSubpath)

    description = root_MOPS_Folder + encounter_folder + "\\" + "Encs_" + str(self.id).zfill(9) + "_Description.txt"
    intruder_csv = root_MOPS_Folder + encounter_folder + intruderSubpath + "\\" + "Encs_" + str(self.id).zfill(9) + "_Truth_TVInt1.csv"
    ownship_csv = root_MOPS_Folder + encounter_folder + ownshipSubpath + "\\" + "Encs_" + str(self.id).zfill(9) + "_Truth_TVOwn.csv"

    # Write a description of the encounter
    description_file = open(description, "w")

    if cls.isOwnshipRPAS(self.aircraft1Class):
      description_file.write("Ownship/ Aircraft1 Class: " + cls.className(self.aircraft1Class) + " (h=" + str(int(units.m_to_ft(self.aircraft1[0].altitude))) + " -- " + str(int(units.m_to_ft(self.aircraft1[len(self.aircraft1) - 1].altitude))) + ")\n")
      description_file.write("Intruder/ Intruder: " + cls.className(self.aircraft2Class) + " (h=" + str(int(units.m_to_ft(self.aircraft2[0].altitude))) + " -- " + str(int(units.m_to_ft(self.aircraft2[len(self.aircraft2)-1].altitude))) + ")\n")
    else:
      description_file.write("Ownship/ Aircraft1 Class: " + cls.className(self.aircraft2Class) + " (h=" + str(int(units.m_to_ft(self.aircraft2[0].altitude))) + " -- " + str(int(units.m_to_ft(self.aircraft2[len(self.aircraft2) - 1].altitude))) + ")\n")
      description_file.write("Intruder/ Intruder: " + cls.className(self.aircraft1Class) + " (h=" + str(int(units.m_to_ft(self.aircraft1[0].altitude))) + " -- " + str(int(units.m_to_ft(self.aircraft1[len(self.aircraft1)-1].altitude))) + ")\n")

    description_file.close()

    if cls.isOwnshipRPAS(self.aircraft1Class):
      ownship_file = open(ownship_csv, "w")
      intruder_file = open(intruder_csv, "w")
    else:
      intruder_file = open(ownship_csv, "w")
      ownship_file = open(intruder_csv, "w")

    ownship_file.write("ToA(s),ICAO,Lat(deg),Lon(deg),Alt(ft),EWV(kts),NSV(kts),VR(ft/min),HDG(deg)\n")
    intruder_file.write("ToA(s),ICAO,Lat(deg),Lon(deg),Alt(ft),EWV(kts),NSV(kts),VR(ft/min),HDG(deg)\n")

    for time in range(len(self.aircraft1)):
      arcf1 = self.aircraft1[time] # Ownship
      arcf2 = self.aircraft2[time]  # Intruder

      ownship_file.write(str(time) + ", " + self.aircraft1Callsign + ", ")
      intruder_file.write(str(time) + ", " + self.aircraft2Callsign + ", ")

      ownship = projection.inverse(self.aircraft1[time].x_loc, self.aircraft1[time].y_loc, units.m_to_ft(self.aircraft1[time].altitude))
      intruder = projection.inverse(self.aircraft2[time].x_loc, self.aircraft2[time].y_loc, units.m_to_ft(self.aircraft2[time].altitude))

      ownship_file.write(str(ownship[1]) + ", " + str(ownship[0]) + ", " + str(ownship[2]) + ", ")
      intruder_file.write(str(intruder[1]) + ", " + str(intruder[0]) + ", " + str(intruder[2]) + ", ")

      ownship_file.write(str(units.ms_to_kt(self.aircraft1[time].x_v)) + ", " + str(units.ms_to_kt(self.aircraft1[time].y_v)) + ", " + str(units.ms_to_fpm(self.aircraft1[time].v_speed)) + ", ")
      intruder_file.write(str(units.ms_to_kt(self.aircraft2[time].x_v)) + ", " + str(units.ms_to_kt(self.aircraft2[time].y_v)) + ", " + str(units.ms_to_fpm(self.aircraft2[time].v_speed)) + ", ")

      ownship_file.write(str(self.aircraft1[time].bearing) + "\n")
      intruder_file.write(str(self.aircraft1[time].bearing) + "\n")

    # Close both files
    ownship_file.close()
    intruder_file.close()



  def computeConflictsCAFE(self, full_diagram_path, ep, arcf1_eam, arcf2_eam):

    # Set time detection range
    B = 0
    T = ep.look_ahead_time
    NMAC_Upper = 30
    HAZ_Upper = 80
    NMAC_Params = rwc.HAZ_parameters(0, 0, 0, 0)
    NMAC_Params.setAsNMAC()
    # Remove Taumod factor from caution WC volume
    lmv_param = copy.deepcopy(ep.wcv_param_caution)
    lmv_param.TAUMOD = 0

    nTest = min(len(self.aircraft1), len(self.aircraft2))


    print("Start computing RWC conflicts...")

    # Compute conflicts
    self.aircraft1RWC = [None] * nTest
    self.aircraft2RWC = [None] * nTest
    self.aircraft1TCAS = [None] * nTest
    self.aircraft2TCAS = [None] * nTest
    self.aircraftNMAC = [False] * nTest
    self.aircraftNLMV1 = [None] * nTest
    self.aircraftNLMV2 = [None] * nTest
    self.aircraft1NCS = [None] * nTest
    self.aircraft2NCS = [None] * nTest

    for index in range(nTest):
      arcf1 = self.aircraft1[index]
      arcf2 = self.aircraft2[index]

      if ep.debug:
        print("Checking Aircraft1 - Aircraft2 for sequence Idx: ", index)
        #print("Aircraft1: ", str(arcf1.x_loc), " -- ", str(arcf1.y_loc), " // ", str(arcf1.x_v), " -- ", str(arcf1.y_v))
        #print("Aircraft2: ", str(arcf2.x_loc), " -- ", str(arcf2.y_loc), " // ", str(arcf2.x_v), " -- ", str(arcf2.y_v))

      if ep.plotContour and cls.isRPAS(self.aircraft1Class):
        contour = rwcc.horizontalContours(arcf1, arcf2, 0, 4 * ep.look_ahead_time, ep, ep.debug)
        rwcc.plotHorizontalContours(full_diagram_path, self.id, index, arcf1, arcf2, contour)

      elif ep.plotContour and cls.isRPAS(self.aircraft2Class):
        contour = rwcc.horizontalContours(arcf2, arcf1, 0, 4 * ep.look_ahead_time, ep, ep.debug)
        rwcc.plotHorizontalContours(full_diagram_path, self.id, index, arcf2, arcf1, contour)

      #
      # CPA computation
      #arcf1_eam.record_CPA_parameters(index, arcf1, arcf2)
      #arcf2_eam.record_CPA_parameters(index, arcf2, arcf1)

      #
      # Check active sensors detection
      self.aircraft1NCS[index] = ncsd.detect_intruder(arcf1, arcf2)
      self.aircraft2NCS[index] = ncsd.detect_intruder(arcf2, arcf1)

      #
      # Check NMAC violation
      self.aircraftNMAC[index] = rwct.check_nmac(arcf1, arcf2, ep.nmac_param)


      if self.aircraftNMAC[index]:
        arcf1_eam.record_NMAC_parameters(index, arcf1, arcf2, ep.nmac_param)
        arcf2_eam.record_NMAC_parameters(index, arcf2, arcf1, ep.nmac_param)
        print("NMAC Aircraft1 <-> Aircraft2 violation at sequence Idx: ", index, "Time: ", arcf1.time)
        print("HMD: ", arcf1_eam.HMD, "VMD:", arcf1_eam.VMD, "WSR:", arcf1_eam.WSR)
        #
        # CPA computation
        arcf1_eam.record_CPA_parameters(index, arcf1, arcf2)
        arcf2_eam.record_CPA_parameters(index, arcf2, arcf1)
        #
        # Detect end of preventive
        arcf1_eam.end_Preventive(index)
        arcf2_eam.end_Preventive(index)
        #
        # Detect end of caution
        arcf1_eam.end_Caution(index)
        arcf2_eam.end_Caution(index)
        #
        # Detect end of warning
        arcf1_eam.end_Warning(index)
        arcf2_eam.end_Warning(index)
        continue



      # Look ahead for a NMAC for aircraft1
      '''NMAC_interval = rwcthr.WCV_interval(arcf1, arcf2, 0, NMAC_Upper, NMAC_Params, False)

      if not NMAC_interval.isEmpty():
        print("Aircraft1 lookahead NMAC conflict detected in ", NMAC_interval.low, " seconds")

        prediction = lmv.violateNMAC(arcf1, arcf2, ep.nmac_param, NMAC_Upper, 30, 4, 0, 0)
        arcf1_eam.record_LMV_parameters(index, arcf1, arcf2, NMAC_interval.low, prediction)

      # Look ahead for a NMAC for aircraft2
      NMAC_interval = rwcthr.WCV_interval(arcf2, arcf1, 0, NMAC_Upper, NMAC_Params, False)

      if not NMAC_interval.isEmpty():
        print("Aircraft2 lookahead NMAC conflict detected in ", NMAC_interval.low, " seconds")

        prediction = lmv.violateNMAC(arcf2, arcf1, ep.nmac_param, NMAC_Upper, 30, 4, 0, 0)
        arcf2_eam.record_LMV_parameters(index, arcf2, arcf1, NMAC_interval.low, prediction)'''


      #
      # Check separation violation
      if ep.separation_actv:
        arcf1_eam.record_Sep_parameters(index, arcf1, arcf2, ep.sep_param)
        arcf2_eam.record_Sep_parameters(index, arcf2, arcf1, ep.sep_param)

      #
      # Check TCAS RA activation
      if ep.tcasii_actv:
        #
        # AIRCRAFT1 TCAS analysis
        #
        if cls.isACAS(self.aircraft1Class):
          self.aircraft1TCAS[index] = TCAS3D.RA3D_analysis(arcf1, arcf2, ep.TCAS_Table)

          if self.aircraft1TCAS[index].conflict():
            print("TCAS Aircraft1 -> Aircraft2 at sequence Idx: ", index)
            print("Interval Low: ", self.aircraft1TCAS[index].interval.low, " Up: ", self.aircraft1TCAS[index].interval.up)
            print("Time to cpa: ", self.aircraft1TCAS[index].time_crit, " Distance at cpa: ", self.aircraft1TCAS[index].dist_crit)
            arcf1_eam.record_TCAS3D_parameters(index, self.aircraft1TCAS[index], arcf1, arcf2, ep.TCAS_Table)

        #
        # AIRCRAFT2 TCAS analysis
        #
        if cls.isACAS(self.aircraft2Class):
          self.aircraft2TCAS[index] = TCAS3D.RA3D_analysis(arcf2, arcf1, ep.TCAS_Table)

          if self.aircraft2TCAS[index].conflict():
            print("TCAS Aircraft2 -> Aircraft1 at sequence Idx: ", index)
            print("Interval Low: ", self.aircraft2TCAS[index].interval.low, " Up: ", self.aircraft2TCAS[index].interval.up)
            print("Time to cpa: ", self.aircraft2TCAS[index].time_crit, "Distance at cpa: ", self.aircraft2TCAS[index].dist_crit)
            arcf2_eam.record_TCAS3D_parameters(index, self.aircraft2TCAS[index], arcf2, arcf1, ep.TCAS_Table)


      #
      # AIRCRAFT1 WCV analysis
      #
      # Check direct violation of RWC volumes
      self.aircraft1RWC[index] = rwct.check_wcv_zone(arcf1, arcf2, ep.wcv_param_caution)

      # Report hazard zone HAZ violation check for Aircraft1
      if self.aircraft1RWC[index].WCV:
        arcf1_eam.record_SLOWC_parameters(index, self.aircraft1RWC[index].Tcpa, arcf1, arcf2, ep.wcv_param_caution)
        #arcf1_eam.clean_HAZ_LMV_parameters()
        arcf1_eam.end_Warning(index)
        arcf2_eam.end_Warning(index)
        arcf1_eam.end_Caution(index)
        arcf2_eam.end_Caution(index)
        arcf1_eam.end_Preventive(index)
        arcf2_eam.end_Preventive(index)

        #if ep.debug:
        print("\tAircraft1 is violating the Aircraft2 Hazard zone.")
        print("\tTcpa: ", self.aircraft1RWC[index].Tcpa, " Dcpa: ", self.aircraft1RWC[index].Dcpa, " Rxy: ", self.aircraft1RWC[index].Rxy)
        print("\tRz: ", self.aircraft1RWC[index].Rz, " Tcoa: ", self.aircraft1RWC[index].Tcoa)

      # No Aircraft 1 HAZ violation, we move on to check thresholds
      elif ep.threshold_actv:

        activeConflict = False
        arcf1_int = itv.Interval(1, 0)
        self.aircraft1RWC[index].interval = itv.Interval(1, 0)

        # Check for Warning RWC Thresholds
        if ep.warning_actv:

          # Warning Alert Check for aircraft 1
          arcf1_int = rwcthr.WCV_interval(arcf1, arcf2, B, T, ep.wcv_param_warning, ep.debug)

          # Checking the warning conditions
          active = arcf1_eam.record_Warning_parameters(index, arcf1_int, self.aircraft1RWC[index].Tcpa, ep.warning_t, ep.debug)

          # If Warning active, we collect the data
          if active:
            activeConflict = True
            self.aircraft1RWC[index].interval = arcf1_int
            self.aircraft1RWC[index].warning_ct = ep.warning_t.checkConflict(arcf1_int, self.aircraft1RWC[index].Tcpa)
            arcf1_eam.end_Caution(index)
            arcf1_eam.end_Preventive(index)

            #if cls.isRPAS(self.aircraft1Class):
            #  if not arcf1_eam.HAZ_LMV_violated() and lmv.violateHAZ(arcf1, arcf2, lmv_param, HAZ_Upper, 20, 20, 4, 500, 500, 150, 200):
            #    arcf1_eam.record_HAZ_LMV_parameters(index, arcf1, arcf2)

        # Otherwise, we check for the Corrective RWC Thresholds
        if ep.caution_actv:

          if not activeConflict:
            # Corrective Alert Check for aircraft 1
            arcf1_int = rwcthr.WCV_interval(arcf1, arcf2, B, T, ep.wcv_param_caution, ep.debug)

            # Checking the corrective conditions
            active = arcf1_eam.record_Caution_parameters(index, arcf1_int, self.aircraft1RWC[index].Tcpa, ep.caution_t, ep.debug)

            # If Corrective active, we collect the data
            if active:
              activeConflict = True
              self.aircraft1RWC[index].interval = arcf1_int
              self.aircraft1RWC[index].corrective_ct = ep.caution_t.checkConflict(arcf1_int, self.aircraft1RWC[index].Tcpa)
              arcf1_eam.end_Preventive(index)

              if cls.isRPAS(self.aircraft1Class) and (not arcf1_eam.HAZ_LMV_violated() or (index - arcf1_eam.HAZ_LMV_violated_time()) <= 3):
                if lmv.violateHAZ(arcf1, arcf2, ep.wcv_param_caution, HAZ_Upper, 20, 20, 4, 500, 500, 150, 200):
                  if not arcf1_eam.HAZ_LMV_violated():
                    arcf1_eam.record_HAZ_LMV_parameters(index, arcf1, arcf2)
                else:
                  arcf1_eam.clean_HAZ_LMV_parameters()

        # Otherwise, we check for the Preventive RWC Thresholds
        if ep.preventive_actv and not activeConflict:

          # Preventive Alert Check for aircraft 1
          arcf1_int = rwcthr.WCV_interval(arcf1, arcf2, B, T, ep.wcv_param_preventive, ep.debug)

          # Checking the preventive conditions
          active = arcf1_eam.record_Preventive_parameters(index, arcf1_int, self.aircraft1RWC[index].Tcpa, ep.preventive_t, ep.debug)

          # If Preventive active, we collect the data
          if active:
            activeConflict = True
            self.aircraft1RWC[index].interval = arcf1_int
            self.aircraft1RWC[index].preventive_ct = ep.preventive_t.checkConflict(arcf1_int, self.aircraft1RWC[index].Tcpa)

        # Report analysis results
        if not arcf1_int.isEmpty():
          if ep.debug:
            print("\tAircraft1 will violate HAZ volume in the future. Low: ", arcf1_int.low, " Up: ", arcf1_int.up)
            print("\tPreventive: " + str(self.aircraft1RWC[index].preventive_ct) + " Corrective: " + str(self.aircraft1RWC[index].corrective_ct) + " Warning: " + str(self.aircraft1RWC[index].warning_ct))
            print("\tTcpa: ", self.aircraft1RWC[index].Tcpa, " Dcpa: ", self.aircraft1RWC[index].Dcpa, " Rxy: ", self.aircraft1RWC[index].Rxy)
            print("\tRz: ", self.aircraft1RWC[index].Rz, " Tcoa: ", self.aircraft1RWC[index].Tcoa)

        if self.aircraft1RWC[index].isAlert():
          if ep.debug:
            print("\tAircraft1 has some type of Alert.  Preventive: ", self.aircraft1RWC[index].preventive_ct,
                  " Corrective: ", self.aircraft1RWC[index].corrective_ct, "  Warning: ", self.aircraft1RWC[index].warning_ct)

      #
      # AIRCRAFT2 WCV analysis
      #
      # Check direct violation of RWC volumes
      self.aircraft2RWC[index] = rwct.check_wcv_zone(arcf2, arcf1, ep.wcv_param_caution)

      # Report hazard zone HAZ violation check for Aircraft2
      if self.aircraft2RWC[index].WCV:
        arcf2_eam.record_SLOWC_parameters(index, self.aircraft2RWC[index].Tcpa, arcf2, arcf1, ep.wcv_param_caution)
        #arcf2_eam.clean_HAZ_LMV_parameters()
        arcf1_eam.end_Warning(index)
        arcf2_eam.end_Warning(index)
        arcf1_eam.end_Caution(index)
        arcf2_eam.end_Caution(index)
        arcf1_eam.end_Preventive(index)
        arcf2_eam.end_Preventive(index)

        if ep.debug:
          print("\tAircraft2 is violating the Aircraft1 Hazard zone.")
          print("\tTcpa: ", self.aircraft2RWC[index].Tcpa, " Dcpa: ", self.aircraft2RWC[index].Dcpa, " Rxy: ", self.aircraft2RWC[index].Rxy)
          print("\tRz: ", self.aircraft2RWC[index].Rz, " Tcoa: ", self.aircraft2RWC[index].Tcoa)

      # No Aircraft 2 HAZ violation, we move on to check thresholds
      elif ep.threshold_actv:

        activeConflict = False
        arcf2_int = itv.Interval(1, 0)
        self.aircraft2RWC[index].interval = itv.Interval(1, 0)

        # Check for Warning RWC Thresholds
        if ep.warning_actv:

          # Warning Alert Check for aircraft 2
          arcf2_int = rwcthr.WCV_interval(arcf2, arcf1, B, T, ep.wcv_param_warning, ep.debug)

          # Checking the warning conditions
          active = arcf2_eam.record_Warning_parameters(index, arcf2_int, self.aircraft2RWC[index].Tcpa, ep.warning_t, ep.debug)

          # If Warning active, we collect the data
          if active:
            activeConflict = True
            self.aircraft2RWC[index].interval = arcf2_int
            self.aircraft2RWC[index].warning_ct = ep.warning_t.checkConflict(arcf2_int, self.aircraft2RWC[index].Tcpa)
            arcf2_eam.end_Caution(index)
            arcf2_eam.end_Preventive(index)

            #if cls.isRPAS(self.aircraft2Class):
            #  if not arcf2_eam.HAZ_LMV_violated() and lmv.violateHAZ(arcf2, arcf1, lmv_param, HAZ_Upper, 20, 20, 4, 500, 500, 150, 200):
            #    arcf2_eam.record_HAZ_LMV_parameters(index, arcf2, arcf1)

        # Otherwise, we check for the Corrective RWC Thresholds
        if ep.caution_actv:

          if not activeConflict:
            # Corrective Alert Check for aircraft 2
            arcf2_int = rwcthr.WCV_interval(arcf2, arcf1, B, T, ep.wcv_param_caution, ep.debug)

            # Checking the corrective conditions
            active = arcf2_eam.record_Caution_parameters(index, arcf2_int, self.aircraft2RWC[index].Tcpa, ep.caution_t, ep.debug)

            # If Corrective active, we collect the data
            if active:
              activeConflict = True
              self.aircraft2RWC[index].interval = arcf2_int
              self.aircraft2RWC[index].corrective_ct = ep.caution_t.checkConflict(arcf2_int, self.aircraft2RWC[index].Tcpa)
              arcf2_eam.end_Preventive(index)

              #if cls.isRPAS(self.aircraft2Class):
              #  if not arcf2_eam.HAZ_LMV_violated() and lmv.violateHAZ(arcf2, arcf1, lmv_param, HAZ_Upper, 20, 20, 4, 500, 500, 150, 200):
              #    arcf2_eam.record_HAZ_LMV_parameters(index, arcf2, arcf1)

              if cls.isRPAS(self.aircraft2Class) and (not arcf2_eam.HAZ_LMV_violated() or (index - arcf2_eam.HAZ_LMV_violated_time()) <= 3):
                if lmv.violateHAZ(arcf2, arcf1, ep.wcv_param_caution, HAZ_Upper, 20, 20, 4, 500, 500, 150, 200):
                  if not arcf2_eam.HAZ_LMV_violated():
                    arcf2_eam.record_HAZ_LMV_parameters(index, arcf2, arcf1)
                else:
                  arcf2_eam.clean_HAZ_LMV_parameters()


        # Otherwise, we check for the Preventive RWC Thresholds
        if ep.preventive_actv and not activeConflict:

          # Preventive Alert Check for aircraft 2
          arcf2_int = rwcthr.WCV_interval(arcf2, arcf1, B, T, ep.wcv_param_preventive, ep.debug)

          # Checking the preventive conditions
          active = arcf2_eam.record_Preventive_parameters(index, arcf2_int, self.aircraft2RWC[index].Tcpa, ep.preventive_t, ep.debug)

          # If Preventive active, we collect the data
          if active:
            activeConflict = True
            self.aircraft2RWC[index].interval = arcf1_int
            self.aircraft2RWC[index].preventive_ct = ep.preventive_t.checkConflict(arcf2_int, self.aircraft2RWC[index].Tcpa)

        # Report analysis results
        if not arcf2_int.isEmpty():
          if ep.debug:
            print("\tAircraft2 will violate HAZ volume in the future. Low: ", arcf2_int.low, " Up: ", arcf2_int.up)
            print("\tPreventive: " + str(self.aircraft2RWC[index].preventive_ct) + " Corrective: " + str(self.aircraft2RWC[index].corrective_ct) + " Warning: " + str(self.aircraft2RWC[index].warning_ct))
            print("\tTcpa: ", self.aircraft2RWC[index].Tcpa, " Dcpa: ", self.aircraft2RWC[index].Dcpa, " Rxy: ", self.aircraft2RWC[index].Rxy)
            print("\tRz: ", self.aircraft2RWC[index].Rz, " Tcoa: ", self.aircraft2RWC[index].Tcoa)

        if self.aircraft2RWC[index].isAlert():
          if ep.debug:
            print("\tAircraft2 has some type of Alert.  Preventive: ", self.aircraft2RWC[index].preventive_ct, " Corrective: ", self.aircraft2RWC[index].corrective_ct, "  Warning: ", self.aircraft2RWC[index].warning_ct)

      #
      # CPA computation in case of active conflict only
      if arcf1_eam.active_conflict or arcf2_eam.active_conflict:
        arcf1_eam.record_CPA_parameters(index, arcf1, arcf2)
        arcf2_eam.record_CPA_parameters(index, arcf2, arcf1)

    # Need to close activations that remain until the end
    arcf1_eam.end_Caution(nTest - 1)
    arcf2_eam.end_Caution(nTest - 1)
    arcf1_eam.end_Preventive(nTest - 1)
    arcf2_eam.end_Preventive(nTest - 1)



  def computeConflictsDEG(self, full_diagram_path, ep, arcf1_eam, arcf2_eam):

    # Set time detection range
    B = 0
    T = ep.look_ahead_time
    HAZ_Upper = 80

    nTest = min(len(self.aircraft1), len(self.aircraft2))


    print("Start computing RWC conflicts...")

    # Compute conflicts
    self.aircraft1RWC = [None] * nTest
    self.aircraft2RWC = [None] * nTest
    self.aircraft1TCAS = [None] * nTest
    self.aircraft2TCAS = [None] * nTest
    self.aircraftNMAC = [False] * nTest
    self.aircraft1NCS = [None] * nTest
    self.aircraft2NCS = [None] * nTest
    self.aircraft1WCV = [None] * nTest
    self.aircraft2WCV = [None] * nTest
    self.aircraft1WarningTrace = [None] * nTest
    self.aircraft2WarningTrace = [None] * nTest
    self.aircraft1CautionTrace  = [None] * nTest
    self.aircraft2CautionTrace = [None] * nTest
    self.aircraft1PreventiveTrace = [None] * nTest
    self.aircraft2PreventiveTrace = [None] * nTest


    for index in range(nTest):
      arcf1 = self.aircraft1[index]
      arcf2 = self.aircraft2[index]

      if ep.debug:
        print("Checking Aircraft1 - Aircraft2 for sequence Idx: ", index, "  Time: ", arcf1.time)
        #print("Aircraft1: ", str(arcf1.x_loc), " -- ", str(arcf1.y_loc), " // ", str(arcf1.x_v), " -- ", str(arcf1.y_v))
        #print("Aircraft2: ", str(arcf2.x_loc), " -- ", str(arcf2.y_loc), " // ", str(arcf2.x_v), " -- ", str(arcf2.y_v))

      if ep.plotContour:
        contour = rwcc.horizontalContours(arcf1, arcf2, 0, 4 * ep.look_ahead_time, ep, ep.debug)
        rwcc.plotHorizontalContours(full_diagram_path, self.id, index, "arcft1", arcf1, arcf2, contour)

        contour = rwcc.horizontalContours(arcf2, arcf1, 0, 4 * ep.look_ahead_time, ep, ep.debug)
        rwcc.plotHorizontalContours(full_diagram_path, self.id, index, "arcft2", arcf2, arcf1, contour)

      #
      # Check active sensors detection
      self.aircraft1NCS[index] = ncsd.detect_intruder(arcf1, arcf2)
      self.aircraft2NCS[index] = ncsd.detect_intruder(arcf2, arcf1)

      '''
      if ep.debug:
        if self.aircraft1NCS[index].row:
          print("\tAircraft 1 has ROW over Aircraft 2 at index: ",  index)
        if self.aircraft2NCS[index].row:
          print("\tAircraft 2 has ROW over Aircraft 1 at index: ", index)
      '''

      #
      # Check NMAC violation
      self.aircraftNMAC[index] = rwct.check_nmac(arcf1, arcf2, ep.nmac_param)

      if self.aircraftNMAC[index]:
        arcf1_eam.record_NMAC_parameters(index, arcf1, arcf2, ep.nmac_param)
        arcf2_eam.record_NMAC_parameters(index, arcf2, arcf1, ep.nmac_param)
        print("NMAC Aircraft1 <-> Aircraft2 violation at sequence Idx: ", index, "Time: ", arcf1.time, " / ", arcf2.time)
        print("HMD: ", arcf1_eam.HMD, "VMD:", arcf1_eam.VMD, "WSR:", arcf1_eam.WSR)

        #
        # CPA computation
        arcf1_eam.record_CPA_parameters(index, arcf1, arcf2)
        arcf2_eam.record_CPA_parameters(index, arcf2, arcf1)
        #
        # Detect end of preventive
        arcf1_eam.end_Preventive(index)
        arcf2_eam.end_Preventive(index)
        #
        # Detect end of caution
        arcf1_eam.end_Caution(index)
        arcf2_eam.end_Caution(index)
        #
        # Detect end of warning
        arcf1_eam.end_Warning(index)
        arcf2_eam.end_Warning(index)
        continue

      #
      # Check separation violation
      if ep.separation_actv:
        arcf1_eam.record_Sep_parameters(index, arcf1, arcf2, ep.sep_param)
        arcf2_eam.record_Sep_parameters(index, arcf2, arcf1, ep.sep_param)

      #
      # AIRCRAFT1 WCV analysis
      #
      if ep.debug:
        print("Checking for WCV violations...")
      # Check direct violation of RWC volumes
      self.aircraft1RWC[index] = rwct.check_wcv_zone(arcf1, arcf2, ep.wcv_param_warning)

      # Report hazard zone HAZ violation check for Aircraft1
      if self.aircraft1RWC[index].WCV:
        arcf1_eam.record_SLOWC_parameters(index, self.aircraft1RWC[index].Tcpa, arcf1, arcf2, ep.wcv_param_warning)
        #arcf1_eam.clean_HAZ_LMV_parameters()
        arcf1_eam.end_Warning(index)
        arcf2_eam.end_Warning(index)
        arcf1_eam.end_Caution(index)
        arcf2_eam.end_Caution(index)
        arcf1_eam.end_Preventive(index)
        arcf2_eam.end_Preventive(index)

        #if ep.debug:
        print("\tAircraft1 is violating the Aircraft2 WC volume at time: ", arcf1.time)
        print("\tTcpa: ", self.aircraft1RWC[index].Tcpa, " Dcpa: ", self.aircraft1RWC[index].Dcpa, " Rxy: ", self.aircraft1RWC[index].Rxy)
        print("\tRz: ", self.aircraft1RWC[index].Rz, " Tcoa: ", self.aircraft1RWC[index].Tcoa)

      # No Aircraft 1 WCV violation, we move on to check thresholds
      elif ep.threshold_actv:

        activeConflict = False
        arcf1_int = itv.Interval(1, 0)
        self.aircraft1RWC[index].interval = itv.Interval(1, 0)

        # Check for Warning RWC Thresholds
        if ep.warning_actv:

          if ep.debug:
            print("Checking for Warning violations...")

          # Warning Alert Check for aircraft 1
          self.aircraft1WarningTrace[index] = trc.WCV_Trace()

          arcf1_int = rwcthr.WCV_interval(arcf1, arcf2, B, T, ep.wcv_param_warning, ep.debug, trace=self.aircraft1WarningTrace[index])

          # Checking the warning conditions
          active = arcf1_eam.record_Warning_parameters(index, arcf1_int, self.aircraft1RWC[index].Tcpa, ep.warning_t, ep.debug)

          # If Warning active, we collect the data
          if active:
            activeConflict = True
            print("Warning conflict detected at: ", arcf1.time)
            self.aircraft1WarningTrace[index].conflict = True
            self.aircraft1RWC[index].interval = arcf1_int
            self.aircraft1RWC[index].warning_ct = ep.warning_t.checkConflict(arcf1_int, self.aircraft1RWC[index].Tcpa)
            arcf1_eam.end_Caution(index)
            arcf1_eam.end_Preventive(index)

            #if cls.isRPAS(self.aircraft1Class):
            #  if not arcf1_eam.HAZ_LMV_violated() and lmv.violateHAZ(arcf1, arcf2, lmv_param, HAZ_Upper, 20, 20, 4, 500, 500, 150, 200):
            #    arcf1_eam.record_HAZ_LMV_parameters(index, arcf1, arcf2)

        # Otherwise, we check for the Corrective RWC Thresholds
        if ep.caution_actv:

          if not activeConflict:

            if ep.debug:
              print("Checking for Caution violations...")

            # Corrective Alert Check for aircraft 1
            self.aircraft1CautionTrace[index] = trc.WCV_Trace()

            arcf1_int = rwcthr.WCV_interval(arcf1, arcf2, B, T, ep.wcv_param_caution, ep.debug, trace=self.aircraft1CautionTrace[index])

            # Checking the corrective conditions
            active = arcf1_eam.record_Caution_parameters(index, arcf1_int, self.aircraft1RWC[index].Tcpa, ep.caution_t, ep.debug)

            # If Corrective active, we collect the data
            if active:
              activeConflict = True
              print("Caution conflict detected at: ", arcf1.time)
              self.aircraft1CautionTrace[index].conflict = True
              self.aircraft1RWC[index].interval = arcf1_int
              self.aircraft1RWC[index].corrective_ct = ep.caution_t.checkConflict(arcf1_int, self.aircraft1RWC[index].Tcpa)
              arcf1_eam.end_Preventive(index)

              if cls.isRPAS(self.aircraft1Class) and (not arcf1_eam.HAZ_LMV_violated() or (index - arcf1_eam.HAZ_LMV_violated_time()) <= 3):
                if lmv.violateHAZ(arcf1, arcf2, ep.wcv_param_caution, HAZ_Upper, 5, 40, 8, 600, 400, 250, 400):
                  if not arcf1_eam.HAZ_LMV_violated():
                    arcf1_eam.record_HAZ_LMV_parameters(index, arcf1, arcf2)
                else:
                  arcf1_eam.clean_HAZ_LMV_parameters()

        # Otherwise, we check for the Preventive RWC Thresholds
        if ep.preventive_actv and not activeConflict:

          if ep.debug:
            print("Checking for Preventive violations...")

          # Preventive Alert Check for aircraft 1
          self.aircraft1PreventiveTrace[index] = trc.WCV_Trace()

          arcf1_int = rwcthr.WCV_interval(arcf1, arcf2, B, T, ep.wcv_param_preventive, ep.debug, trace=self.aircraft1PreventiveTrace[index])

          # Checking the preventive conditions
          active = arcf1_eam.record_Preventive_parameters(index, arcf1_int, self.aircraft1RWC[index].Tcpa, ep.preventive_t, ep.debug)

          # If Preventive active, we collect the data
          if active:
            activeConflict = True
            print("Preventive conflict detected at: ", arcf1.time)
            self.aircraft1PreventiveTrace[index].conflict = True
            self.aircraft1RWC[index].interval = arcf1_int
            self.aircraft1RWC[index].preventive_ct = ep.preventive_t.checkConflict(arcf1_int, self.aircraft1RWC[index].Tcpa)

        # Report analysis results
        if not arcf1_int.isEmpty():
          if ep.debug:
            print("\tAircraft1 will violate WCV volume in the future. Low: ", arcf1_int.low, " Up: ", arcf1_int.up)
            print("\tPreventive: " + str(self.aircraft1RWC[index].preventive_ct) + " Corrective: " + str(self.aircraft1RWC[index].corrective_ct) + " Warning: " + str(self.aircraft1RWC[index].warning_ct))
            print("\tTcpa: ", self.aircraft1RWC[index].Tcpa, " Dcpa: ", self.aircraft1RWC[index].Dcpa, " Rxy: ", self.aircraft1RWC[index].Rxy)
            print("\tRz: ", self.aircraft1RWC[index].Rz, " Tcoa: ", self.aircraft1RWC[index].Tcoa)

        #if self.aircraft1RWC[index].isAlert():
        #  if ep.debug:
        #    print("\tAircraft1 has some type of Alert.  Preventive: ", self.aircraft1RWC[index].preventive_ct,
        #          " Corrective: ", self.aircraft1RWC[index].corrective_ct, "  Warning: ", self.aircraft1RWC[index].warning_ct)

      #
      # AIRCRAFT2 WCV analysis
      #
      # Check direct violation of RWC volumes
      self.aircraft2RWC[index] = rwct.check_wcv_zone(arcf2, arcf1, ep.wcv_param_warning)

      # Report hazard zone HAZ violation check for Aircraft2
      if self.aircraft2RWC[index].WCV:
        arcf2_eam.record_SLOWC_parameters(index, self.aircraft2RWC[index].Tcpa, arcf2, arcf1, ep.wcv_param_warning)
        # arcf2_eam.clean_HAZ_LMV_parameters()
        arcf1_eam.end_Warning(index)
        arcf2_eam.end_Warning(index)
        arcf1_eam.end_Caution(index)
        arcf2_eam.end_Caution(index)
        arcf1_eam.end_Preventive(index)
        arcf2_eam.end_Preventive(index)

        if ep.debug:
          print("\tAircraft2 is violating the Aircraft1 WC volume at time: ", arcf1.time)
          print("\tTcpa: ", self.aircraft2RWC[index].Tcpa, " Dcpa: ", self.aircraft2RWC[index].Dcpa, " Rxy: ",
                self.aircraft2RWC[index].Rxy)
          print("\tRz: ", self.aircraft2RWC[index].Rz, " Tcoa: ", self.aircraft2RWC[index].Tcoa)

      # No Aircraft 2 HAZ violation, we move on to check thresholds
      elif ep.threshold_actv:

        activeConflict = False
        arcf2_int = itv.Interval(1, 0)
        self.aircraft2RWC[index].interval = itv.Interval(1, 0)

        # Check for Warning RWC Thresholds
        if ep.warning_actv:

          # Warning Alert Check for aircraft 2
          self.aircraft2WarningTrace[index] = trc.WCV_Trace()
          arcf2_int = rwcthr.WCV_interval(arcf2, arcf1, B, T, ep.wcv_param_warning, ep.debug, trace=self.aircraft2WarningTrace[index])

          # Checking the warning conditions
          active = arcf2_eam.record_Warning_parameters(index, arcf2_int, self.aircraft2RWC[index].Tcpa, ep.warning_t, ep.debug)

          # If Warning active, we collect the data
          if active:
            activeConflict = True
            self.aircraft2WarningTrace[index].conflict = True
            self.aircraft2RWC[index].interval = arcf2_int
            self.aircraft2RWC[index].warning_ct = ep.warning_t.checkConflict(arcf2_int, self.aircraft2RWC[index].Tcpa)
            arcf2_eam.end_Caution(index)
            arcf2_eam.end_Preventive(index)

            # if cls.isRPAS(self.aircraft2Class):
            #  if not arcf2_eam.HAZ_LMV_violated() and lmv.violateHAZ(arcf2, arcf1, lmv_param, HAZ_Upper, 20, 20, 4, 500, 500, 150, 200):
            #    arcf2_eam.record_HAZ_LMV_parameters(index, arcf2, arcf1)

        # Otherwise, we check for the Corrective RWC Thresholds
        if ep.caution_actv:

          if not activeConflict:
            # Corrective Alert Check for aircraft 2
            self.aircraft2CautionTrace[index] = trc.WCV_Trace()
            arcf2_int = rwcthr.WCV_interval(arcf2, arcf1, B, T, ep.wcv_param_caution, ep.debug, trace=self.aircraft2CautionTrace[index])

            # Checking the corrective conditions
            active = arcf2_eam.record_Caution_parameters(index, arcf2_int, self.aircraft2RWC[index].Tcpa, ep.caution_t, ep.debug)

            # If Corrective active, we collect the data
            if active:
              activeConflict = True
              self.aircraft2CautionTrace[index].conflict = True
              self.aircraft2RWC[index].interval = arcf2_int
              self.aircraft2RWC[index].corrective_ct = ep.caution_t.checkConflict(arcf2_int, self.aircraft2RWC[index].Tcpa)
              arcf2_eam.end_Preventive(index)

              # if cls.isRPAS(self.aircraft2Class):
              #  if not arcf2_eam.HAZ_LMV_violated() and lmv.violateHAZ(arcf2, arcf1, lmv_param, HAZ_Upper, 20, 20, 4, 500, 500, 150, 200):
              #    arcf2_eam.record_HAZ_LMV_parameters(index, arcf2, arcf1)

              if cls.isRPAS(self.aircraft2Class) and (
                      not arcf2_eam.HAZ_LMV_violated() or (index - arcf2_eam.HAZ_LMV_violated_time()) <= 3):
                if lmv.violateHAZ(arcf2, arcf1, ep.wcv_param_caution, HAZ_Upper, 5, 40, 8, 600, 400, 250, 400):
                  if not arcf2_eam.HAZ_LMV_violated():
                    arcf2_eam.record_HAZ_LMV_parameters(index, arcf2, arcf1)
                else:
                  arcf2_eam.clean_HAZ_LMV_parameters()

        # Otherwise, we check for the Preventive RWC Thresholds
        if ep.preventive_actv and not activeConflict:

          # Preventive Alert Check for aircraft 2
          self.aircraft2PreventiveTrace[index] = trc.WCV_Trace()
          arcf2_int = rwcthr.WCV_interval(arcf2, arcf1, B, T, ep.wcv_param_preventive, ep.debug, trace=self.aircraft2PreventiveTrace[index])

          # Checking the preventive conditions
          active = arcf2_eam.record_Preventive_parameters(index, arcf2_int, self.aircraft2RWC[index].Tcpa, ep.preventive_t, ep.debug)

          # If Preventive active, we collect the data
          if active:
            activeConflict = True
            self.aircraft2PreventiveTrace[index].conflict = True
            self.aircraft2RWC[index].interval = arcf1_int
            self.aircraft2RWC[index].preventive_ct = ep.preventive_t.checkConflict(arcf2_int, self.aircraft2RWC[index].Tcpa)

        # Report analysis results
        if not arcf2_int.isEmpty():
          if ep.debug:
            print("\tAircraft2 will violate HAZ volume in the future. Low: ", arcf2_int.low, " Up: ", arcf2_int.up)
            print("\tPreventive: " + str(self.aircraft2RWC[index].preventive_ct) + " Corrective: " + str(
              self.aircraft2RWC[index].corrective_ct) + " Warning: " + str(self.aircraft2RWC[index].warning_ct))
            print("\tTcpa: ", self.aircraft2RWC[index].Tcpa, " Dcpa: ", self.aircraft2RWC[index].Dcpa, " Rxy: ",
                  self.aircraft2RWC[index].Rxy)
            print("\tRz: ", self.aircraft2RWC[index].Rz, " Tcoa: ", self.aircraft2RWC[index].Tcoa)

        if self.aircraft2RWC[index].isAlert():
          if ep.debug:
            print("\tAircraft2 has some type of Alert.  Preventive: ", self.aircraft2RWC[index].preventive_ct,
                  " Corrective: ", self.aircraft2RWC[index].corrective_ct, "  Warning: ",
                  self.aircraft2RWC[index].warning_ct)

      #
      # CPA computation in case of active conflict only
      if arcf1_eam.active_conflict or arcf2_eam.active_conflict:
        arcf1_eam.record_CPA_parameters(index, arcf1, arcf2)
        arcf2_eam.record_CPA_parameters(index, arcf2, arcf1)

    # Need to close activations that remain until the end
    arcf1_eam.end_Caution(nTest - 1)
    arcf2_eam.end_Caution(nTest - 1)
    arcf1_eam.end_Preventive(nTest - 1)
    arcf2_eam.end_Preventive(nTest - 1)


  def plotEncounterCAFE(self, folder):

    nTest = min(len(self.aircraft1), len(self.aircraft2))
    self.aircraft1
    vx1 = [0] * (nTest)
    vy1 = [0] * (nTest)
    vz1 = [0] * (nTest)

    vx2 = [0] * (nTest)
    vy2 = [0] * (nTest)
    vz2 = [0] * (nTest)

    hsp1 = [0] * (nTest)
    hsp2 = [0] * (nTest)
    vsp1 = [0] * (nTest)
    vsp2 = [0] * (nTest)

    hd1 = [0] * (nTest)
    hd2 = [0] * (nTest)

    hdistance = [0] * (nTest)
    vdistance = [0] * (nTest)

    az1 = [0] * (nTest)
    az2 = [0] * (nTest)
    ele1 = [0] * (nTest)
    ele2 = [0] * (nTest)

    markerSize = 6
    legendSize = 8

    time = range(nTest)

    if cls.isRPAS(self.aircraft1Class):
      aircraft1Color = 'navy'
    else:
      aircraft1Color = 'dimgray'

    if cls.isRPAS(self.aircraft2Class):
      aircraft2Color = 'navy'
    else:
      aircraft2Color = 'dimgray'


    if not cls.isRPAS(self.aircraft1Class) and not cls.isRPAS(self.aircraft2Class):
      aircraft1Color = 'lightsteelblue'
      aircraft2Color = 'lightgreen'


    for i in range(nTest):
      vx1[i] = units.m_to_ft(self.aircraft1[i].x_loc)
      vy1[i] = units.m_to_ft(self.aircraft1[i].y_loc)
      vz1[i] = units.m_to_ft(self.aircraft1[i].altitude)
      vx2[i] = units.m_to_ft(self.aircraft2[i].x_loc)
      vy2[i] = units.m_to_ft(self.aircraft2[i].y_loc)
      vz2[i] = units.m_to_ft(self.aircraft2[i].altitude)

      hsp1[i] = units.ms_to_kt(self.aircraft1[i].h_speed)
      vsp1[i] = units.ms_to_fpm(self.aircraft1[i].v_speed)
      hsp2[i] = units.ms_to_kt(self.aircraft2[i].h_speed)
      vsp2[i] = units.ms_to_fpm(self.aircraft2[i].v_speed)

      hd1[i] = self.aircraft1[i].bearing
      hd2[i] = self.aircraft2[i].bearing

      hdistance[i] = units.m_to_ft(self.aircraft1NCS[i].range)/10
      vdistance[i] = units.m_to_ft(abs(self.aircraft1[i].altitude - self.aircraft2[i].altitude))

      ele1[i] = self.aircraft1NCS[i].elevation
      az1[i] = self.aircraft1NCS[i].azimuth
      ele2[i] = self.aircraft2NCS[i].elevation
      az2[i] = self.aircraft2NCS[i].azimuth


    matplotlib.use("Agg")

    #
    # Main plot: Horizontal plot
    plt.figure()
    label = "Aircraft1 Class: " + cls.className(self.aircraft1Class) + " (h=" + str(int(units.m_to_ft(self.aircraft1[0].altitude))) + " -- " + str(int(units.m_to_ft(self.aircraft1[len(self.aircraft1) - 2].altitude))) + ")"
    plt.plot(vx1, vy1, color=aircraft1Color, label = label)
    plt.plot(vx1[0], vy1[0], marker='x', color=aircraft1Color)
    plt.plot(vx1[nTest-2], vy1[nTest-2], marker='o', color=aircraft1Color)

    label = "Intruder: " + cls.className(self.aircraft2Class) + " (h=" + str(int(units.m_to_ft(self.aircraft2[0].altitude))) + " -- " + str(int(units.m_to_ft(self.aircraft2[len(self.aircraft2)-2].altitude))) + ")"
    plt.plot(vx2, vy2, color=aircraft2Color, label = label)
    plt.plot(vx2[0], vy2[0], marker='x', color=aircraft2Color)
    plt.plot(vx2[nTest-2], vy2[nTest-2], marker='o', color=aircraft2Color)


    # Add NMAC and RWC conflicts
    for i in range(nTest):
      if self.aircraftNMAC[i]:
        plt.plot(vx1[i], vy1[i], marker='o', markersize=markerSize, color='red')
        plt.plot(vx2[i], vy2[i], marker='o', markersize=markerSize, color='red')
      else:
        if self.aircraft1TCAS[i] != None and self.aircraft1TCAS[i].conflict():
          plt.plot(vx1[i], vy1[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft1RWC[i].WCV:
          plt.plot(vx1[i], vy1[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft1RWC[i].isAlert():
          if self.aircraft1RWC[i].isWarningAlert():
            plt.plot(vx1[i], vy1[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft1RWC[i].isCorrectiveAlert():
            plt.plot(vx1[i], vy1[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(vx1[i], vy1[i], marker='.', markersize=markerSize, color='green')

        if self.aircraft2TCAS[i] != None and self.aircraft2TCAS[i].conflict():
          plt.plot(vx2[i], vy2[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft2RWC[i].WCV:
          plt.plot(vx2[i], vy2[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft2RWC[i].isAlert():
          if self.aircraft2RWC[i].isWarningAlert():
            plt.plot(vx2[i], vy2[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft2RWC[i].isCorrectiveAlert():
            plt.plot(vx2[i], vy2[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft2RWC[i].isPreventiveAlert():
            plt.plot(vx2[i], vy2[i], marker='.', markersize=markerSize, color='green')

    if self.encAlertMeasuringAicraft1.minSlatTime > 0:
      plt.plot(vx1[self.encAlertMeasuringAicraft1.minSlatTime], vy1[self.encAlertMeasuringAicraft1.minSlatTime], marker='*', markersize=markerSize, color='black')
    if self.encAlertMeasuringAicraft2.minSlatTime > 0:
      plt.plot(vx2[self.encAlertMeasuringAicraft2.minSlatTime], vy2[self.encAlertMeasuringAicraft2.minSlatTime], marker='*', markersize=markerSize, color='black')

    if self.encAlertMeasuringAicraft1.haz_lmv_violated:
      plt.plot(vx1[self.encAlertMeasuringAicraft1.haz_lmv_violated_time], vy1[self.encAlertMeasuringAicraft1.haz_lmv_violated_time], marker='D', markersize=markerSize, color='black')
    if self.encAlertMeasuringAicraft2.haz_lmv_violated:
      plt.plot(vx2[self.encAlertMeasuringAicraft2.haz_lmv_violated_time], vy2[self.encAlertMeasuringAicraft2.haz_lmv_violated_time], marker='D', markersize=markerSize, color='black')


    plt.axis('equal')
    plt.legend(prop={'size': legendSize})

    plt.suptitle("Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(self.id).zfill(9) + " (x --> o) ")
    plt.title("Horizontal trajectories")
    plt.xlabel('X (ft)')
    plt.ylabel('Y (ft)')
    fig=plt.gcf()
    filepath = folder + "\\Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(9) + "_horizontal.eu1" + ".png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()
    #plt.show()


    #
    # Main plot: Vertical plot
    plt.figure()
    label = "Aircraft1 Class: " + cls.className(self.aircraft1Class) + " (h=" + str(int(units.m_to_ft(self.aircraft1[0].altitude))) + " -- " + str(int(units.m_to_ft(self.aircraft1[len(self.aircraft1) - 2].altitude))) + ")"
    plt.plot(time, vz1, color=aircraft1Color, label=label)
    plt.plot(time[0], vz1[0], marker='x', color=aircraft1Color)
    plt.plot(time[nTest-2], vz1[nTest-2], marker='o', color=aircraft1Color)

    label = "Intruder: " + cls.className(self.aircraft2Class) + " (h=" + str(int(units.m_to_ft(self.aircraft2[0].altitude))) + " -- " + str(int(units.m_to_ft(self.aircraft2[len(self.aircraft2)-2].altitude))) + ")"
    plt.plot(time, vz2, color=aircraft2Color, label=label)
    plt.plot(time[0], vz2[0], marker='x', color=aircraft2Color)
    plt.plot(time[nTest-2], vz2[nTest-2], marker='o', color=aircraft2Color)

    # Add NMAC and RWC conflicts
    for i in range(nTest):
      if self.aircraftNMAC[i]:
        plt.plot(time[i], vz1[i], marker='o', markersize=markerSize, color='red')
        plt.plot(time[i], vz2[i], marker='o', markersize=markerSize, color='red')
      else:
        if self.aircraft1TCAS[i] != None and self.aircraft1TCAS[i].conflict():
          plt.plot(time[i], vz1[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft1RWC[i].WCV:
          plt.plot(time[i], vz1[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft1RWC[i].isAlert():
          if self.aircraft1RWC[i].isWarningAlert():
            plt.plot(time[i], vz1[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft1RWC[i].isCorrectiveAlert():
            plt.plot(time[i], vz1[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(time[i], vz1[i], marker='.', markersize=markerSize, color='green')

        if self.aircraft2TCAS[i] != None and self.aircraft2TCAS[i].conflict():
          plt.plot(time[i], vz2[i], marker='o', markersize=markerSize, color='violet')
        if self.aircraft2RWC[i].WCV:
          plt.plot(time[i], vz2[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft2RWC[i].isAlert():
          if self.aircraft2RWC[i].isWarningAlert():
            plt.plot(time[i], vz2[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft2RWC[i].isCorrectiveAlert():
            plt.plot(time[i], vz2[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft2RWC[i].isPreventiveAlert():
            plt.plot(time[i], vz2[i], marker='.', markersize=markerSize, color='green')

    if self.encAlertMeasuringAicraft1.minSlatTime > 0:
      plt.plot(time[self.encAlertMeasuringAicraft1.minSlatTime], vz1[self.encAlertMeasuringAicraft1.minSlatTime], marker='*', markersize=markerSize, color='black')
    if self.encAlertMeasuringAicraft2.minSlatTime > 0:
      plt.plot(time[self.encAlertMeasuringAicraft2.minSlatTime], vz2[self.encAlertMeasuringAicraft2.minSlatTime], marker='*', markersize=markerSize, color='black')

    if self.encAlertMeasuringAicraft1.haz_lmv_violated:
      plt.plot(time[self.encAlertMeasuringAicraft1.haz_lmv_violated_time], vz1[self.encAlertMeasuringAicraft1.haz_lmv_violated_time], marker='D', markersize=markerSize, color='black')
    if self.encAlertMeasuringAicraft2.haz_lmv_violated:
      plt.plot(time[self.encAlertMeasuringAicraft2.haz_lmv_violated_time], vz2[self.encAlertMeasuringAicraft2.haz_lmv_violated_time], marker='D', markersize=markerSize, color='black')


    plt.legend(prop={'size': legendSize})

    plt.suptitle("Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(self.id).zfill(9) + " (x --> o) ")
    plt.title("Vertical trajectories")
    plt.xlabel("time (s)")
    plt.ylabel("altitude (ft)")
    fig=plt.gcf()
    filepath = folder + "\\Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(9) + "_vertical.eu1" + ".png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()


    #
    # Main plot: Horizontal speed
    plt.figure()
    label = "Aircraft1 Class: " + cls.className(self.aircraft1Class) + " (h=" + str(int(units.m_to_ft(self.aircraft1[0].altitude))) + " -- " + str(int(units.m_to_ft(self.aircraft1[len(self.aircraft1) - 2].altitude))) + ")"
    plt.plot(time, hsp1, color=aircraft1Color, label=label)
    plt.plot(time[0], hsp1[0], marker='x', color=aircraft1Color)
    plt.plot(time[nTest-2], hsp1[nTest-2], marker='o', color=aircraft1Color)

    label = "Intruder: " + cls.className(self.aircraft2Class) + " (h=" + str(int(units.m_to_ft(self.aircraft2[0].altitude))) + " -- " + str(int(units.m_to_ft(self.aircraft2[len(self.aircraft2)-2].altitude))) + ")"
    plt.plot(time, hsp2, color=aircraft2Color, label=label)
    plt.plot(time[0], hsp2[0], marker='x', color=aircraft2Color)
    plt.plot(time[nTest-2], hsp2[nTest-2], marker='o', color=aircraft2Color)

    # Add NMAC and RWC conflicts
    for i in range(nTest):
      if self.aircraftNMAC[i]:
        plt.plot(time[i], hsp1[i], marker='o', markersize=markerSize, color='red')
        plt.plot(time[i], hsp2[i], marker='o', markersize=markerSize, color='red')
      else:
        if self.aircraft1TCAS[i] != None and self.aircraft1TCAS[i].conflict():
          plt.plot(time[i], hsp1[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft1RWC[i].WCV:
          plt.plot(time[i], hsp1[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft1RWC[i].isAlert():
          if self.aircraft1RWC[i].isWarningAlert():
            plt.plot(time[i], hsp1[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft1RWC[i].isCorrectiveAlert():
            plt.plot(time[i], hsp1[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(time[i], hsp1[i], marker='.', markersize=markerSize, color='green')

        if self.aircraft2TCAS[i] != None and self.aircraft2TCAS[i].conflict():
          plt.plot(time[i], hsp2[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft2RWC[i].WCV:
          plt.plot(time[i], hsp2[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft2RWC[i].isAlert():
          if self.aircraft2RWC[i].isWarningAlert():
            plt.plot(time[i], hsp2[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft2RWC[i].isCorrectiveAlert():
            plt.plot(time[i], hsp2[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft2RWC[i].isPreventiveAlert():
            plt.plot(time[i], hsp2[i], marker='.', markersize=markerSize, color='green')

    plt.legend(prop={'size': legendSize})

    plt.suptitle("Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(self.id).zfill(9) + " (x --> o) ")
    plt.title("Horizontal speed")
    plt.xlabel('time (s)')
    plt.ylabel('speed (kt)')

    fig=plt.gcf()
    filepath = folder + "\\Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(9) + "_hspeed.eu1" + ".png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()


    #
    # Main plot: Vertical speed
    label = "Aircraft1 Class: " + cls.className(self.aircraft1Class) + " (h=" + str(
      int(units.m_to_ft(self.aircraft1[0].altitude))) + " -- " + str(
      int(units.m_to_ft(self.aircraft1[len(self.aircraft1) - 2].altitude))) + ")"
    plt.plot(time, vsp1, color=aircraft1Color, label=label)
    plt.plot(time[0], vsp1[0], marker='x', color=aircraft1Color)
    plt.plot(time[nTest - 2], vsp1[nTest - 2], marker='o', color=aircraft1Color)

    label = "Intruder: " + cls.className(self.aircraft2Class) + " (h=" + str(
      int(units.m_to_ft(self.aircraft2[0].altitude))) + " -- " + str(
      int(units.m_to_ft(self.aircraft2[len(self.aircraft2) - 1].altitude))) + ")"
    plt.plot(time, vsp2, color=aircraft2Color, label=label)
    plt.plot(time[0], vsp2[0], marker='x', color=aircraft2Color)
    plt.plot(time[nTest - 2], vsp2[nTest - 2], marker='o', color=aircraft2Color)

    # Add NMAC and RWC conflicts
    for i in range(nTest - 1):
      if self.aircraftNMAC[i]:
        plt.plot(time[i], vsp1[i], marker='o', markersize=markerSize, color='red')
        plt.plot(time[i], vsp2[i], marker='o', markersize=markerSize, color='red')
      else:
        if self.aircraft1TCAS[i] != None and self.aircraft1TCAS[i].conflict():
          plt.plot(time[i], vsp1[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft1RWC[i].WCV:
          plt.plot(time[i], vsp1[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft1RWC[i].isAlert():
          if self.aircraft1RWC[i].isWarningAlert():
            plt.plot(time[i], vsp1[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft1RWC[i].isCorrectiveAlert():
            plt.plot(time[i], vsp1[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(time[i], vsp1[i], marker='.', markersize=markerSize, color='green')

        if self.aircraft2TCAS[i] != None and self.aircraft2TCAS[i].conflict():
          plt.plot(time[i], vsp2[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft2RWC[i].WCV:
          plt.plot(time[i], vsp2[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft2RWC[i].isAlert():
          if self.aircraft2RWC[i].isWarningAlert():
            plt.plot(time[i], vsp2[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft2RWC[i].isCorrectiveAlert():
            plt.plot(time[i], vsp2[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft2RWC[i].isPreventiveAlert():
            plt.plot(time[i], vsp2[i], marker='.', markersize=markerSize, color='green')


    plt.legend(prop={'size': legendSize})

    plt.suptitle("Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(self.id).zfill(9) + " (x --> o) ")
    plt.xlabel('time (s)')
    plt.ylabel('speed (fpm)')
    plt.title("Vertical speed")
    fig = plt.gcf()
    filepath = folder + "\\Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(9) + "_vspeed.eu1" + ".png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()


    #
    # Main plot: Bearing
    label = "Aircraft1 Class: " + cls.className(self.aircraft1Class) + " (h=" + str(
      int(units.m_to_ft(self.aircraft1[0].altitude))) + " -- " + str(
      int(units.m_to_ft(self.aircraft1[len(self.aircraft1) - 2].altitude))) + ")"
    plt.plot(time, hd1, color=aircraft1Color, label=label)
    plt.plot(time[0], hd1[0], marker='x', color=aircraft1Color)
    plt.plot(time[nTest - 2], hd1[nTest - 2], marker='o', color=aircraft1Color)

    label = "Intruder: " + cls.className(self.aircraft2Class) + " (h=" + str(
      int(units.m_to_ft(self.aircraft2[0].altitude))) + " -- " + str(
      int(units.m_to_ft(self.aircraft2[len(self.aircraft2) - 2].altitude))) + ")"
    plt.plot(time, hd2, color=aircraft2Color, label=label)
    plt.plot(time[0], hd2[0], marker='x', color=aircraft2Color)
    plt.plot(time[nTest - 2], hd2[nTest - 2], marker='o', color=aircraft2Color)

    # Add NMAC and RWC conflicts
    for i in range(nTest):
      if self.aircraftNMAC[i]:
        plt.plot(time[i], hd1[i], marker='o', markersize=markerSize, color='red')
        plt.plot(time[i], hd2[i], marker='o', markersize=markerSize, color='red')
      else:
        if self.aircraft1TCAS[i] != None and self.aircraft1TCAS[i].conflict():
          plt.plot(time[i], hd1[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft1RWC[i].WCV:
          plt.plot(time[i], hd1[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft1RWC[i].isAlert():
          if self.aircraft1RWC[i].isWarningAlert():
            plt.plot(time[i], hd1[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft1RWC[i].isCorrectiveAlert():
            plt.plot(time[i], hd1[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(time[i], hd1[i], marker='.', markersize=markerSize, color='green')

        if self.aircraft2TCAS[i] != None and self.aircraft2TCAS[i].conflict():
          plt.plot(time[i], hd2[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft2RWC[i].WCV:
          plt.plot(time[i], hd2[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft2RWC[i].isAlert():
          if self.aircraft2RWC[i].isWarningAlert():
            plt.plot(time[i], hd2[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft2RWC[i].isCorrectiveAlert():
            plt.plot(time[i], hd2[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft2RWC[i].isPreventiveAlert():
            plt.plot(time[i], hd2[i], marker='.', markersize=markerSize, color='green')


    plt.legend(prop={'size': legendSize})

    plt.suptitle("Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(self.id).zfill(9) + " (x --> o) ")
    plt.xlabel('time (s)')
    #plt.ylabel('bearing (deg)')
    plt.title("Bearing")
    fig = plt.gcf()
    filepath = folder + "\\Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(9) + "_bearing.eu1" + ".png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()


    #
    # Main plot: Conflict plot
    label = "H distance/10 "
    plt.plot(time, hdistance, color='lightgreen', label=label)
    label = "V distance "
    plt.plot(time, vdistance, color='lightsteelblue', label=label)

    # Add NMAC and RWC conflicts
    for i in range(nTest):
      if self.aircraftNMAC[i]:
        plt.plot(time[i], hdistance[i], marker='o', markersize=markerSize, color='red')
        plt.plot(time[i], vdistance[i], marker='o', markersize=markerSize, color='red')
      else:
        if self.aircraft1TCAS[i] != None and self.aircraft1TCAS[i].conflict():
          plt.plot(time[i], hdistance[i], marker='o', markersize=markerSize, color='violet')
          plt.plot(time[i], vdistance[i], marker='o', markersize=markerSize, color='violet')
        if self.aircraft1RWC[i].WCV:
          plt.plot(time[i], hdistance[i], marker='.', markersize=markerSize, color='red')
          plt.plot(time[i], vdistance[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft1RWC[i].isAlert():
          if self.aircraft1RWC[i].isWarningAlert():
            plt.plot(time[i], hdistance[i], marker='.', markersize=markerSize, color='orange')
            plt.plot(time[i], vdistance[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft1RWC[i].isCorrectiveAlert():
            plt.plot(time[i], hdistance[i], marker='.', markersize=markerSize, color='yellow')
            plt.plot(time[i], vdistance[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(time[i], hdistance[i], marker='.', markersize=markerSize, color='green')
            plt.plot(time[i], vdistance[i], marker='.', markersize=markerSize, color='green')


    if self.encAlertMeasuringAicraft1.minSlatTime > 0:
      plt.plot(time[self.encAlertMeasuringAicraft1.minSlatTime], hdistance[self.encAlertMeasuringAicraft1.minSlatTime], marker='*', markersize=markerSize, color='black')

    if self.encAlertMeasuringAicraft1.haz_lmv_violated:
      plt.plot(time[self.encAlertMeasuringAicraft1.haz_lmv_violated_time], hdistance[self.encAlertMeasuringAicraft1.haz_lmv_violated_time], marker='D', markersize=markerSize, color='black')

    plt.legend(prop={'size': 6})

    label = "Aircraft1 Class: " + cls.className(self.aircraft1Class) + " (h=" + str(
      int(units.m_to_ft(self.aircraft1[0].altitude))) + " -- " + str(
      int(units.m_to_ft(self.aircraft1[len(self.aircraft1) - 2].altitude))) + ")"

    plt.suptitle("Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(self.id).zfill(9) + " (x --> o) ")
    plt.xlabel("time (s)")
    plt.ylabel("distance (ft)")
    plt.title("Encounter_Distribution data: " + label)
    fig = plt.gcf()
    filepath = folder + "\\Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(9) + "_AC1" + "_encounter.eu1" + ".png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()

    #
    # Main plot: Conflict plot AC2
    label = "H distance/10 "
    plt.plot(time, hdistance, color='lightgreen', label=label)
    label = "V distance "
    plt.plot(time, vdistance, color='lightsteelblue', label=label)

    # Add NMAC and RWC conflicts
    for i in range(nTest):
      if self.aircraftNMAC[i]:
        plt.plot(time[i], hdistance[i], marker='o', markersize=markerSize, color='red')
        plt.plot(time[i], vdistance[i], marker='o', markersize=markerSize, color='red')
      else:
        if self.aircraft2TCAS[i] != None and self.aircraft2TCAS[i].conflict():
          plt.plot(time[i], hdistance[i], marker='o', markersize=markerSize, color='violet')
          plt.plot(time[i], vdistance[i], marker='o', markersize=markerSize, color='violet')
        if self.aircraft2RWC[i].WCV:
          plt.plot(time[i], hdistance[i], marker='.', markersize=markerSize, color='red')
          plt.plot(time[i], vdistance[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft2RWC[i].isAlert():
          if self.aircraft1RWC[i].isWarningAlert():
            plt.plot(time[i], hdistance[i], marker='.', markersize=markerSize, color='orange')
            plt.plot(time[i], vdistance[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft2RWC[i].isCorrectiveAlert():
            plt.plot(time[i], hdistance[i], marker='.', markersize=markerSize, color='yellow')
            plt.plot(time[i], vdistance[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft2RWC[i].isPreventiveAlert():
            plt.plot(time[i], hdistance[i], marker='.', markersize=markerSize, color='green')
            plt.plot(time[i], vdistance[i], marker='.', markersize=markerSize, color='green')


    if self.encAlertMeasuringAicraft2.minSlatTime > 0:
      plt.plot(time[self.encAlertMeasuringAicraft2.minSlatTime], vdistance[self.encAlertMeasuringAicraft2.minSlatTime], marker='*', markersize=markerSize, color='black')

    if self.encAlertMeasuringAicraft2.haz_lmv_violated:
      plt.plot(time[self.encAlertMeasuringAicraft2.haz_lmv_violated_time], vdistance[self.encAlertMeasuringAicraft2.haz_lmv_violated_time], marker='D', markersize=markerSize, color='black')


    plt.legend(prop={'size': 6})

    label = "Intruder: " + cls.className(self.aircraft2Class) + " (h=" + str(
      int(units.m_to_ft(self.aircraft2[0].altitude))) + " -- " + str(
      int(units.m_to_ft(self.aircraft2[len(self.aircraft2) - 2].altitude))) + ")"

    plt.suptitle("Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(self.id).zfill(9) + " (x --> o) ")
    plt.xlabel("time (s)")
    plt.ylabel("distance (ft)")
    plt.title("Encounter_Distribution data: " + label)
    fig = plt.gcf()
    filepath = folder + "\\Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(9) + "_AC2" + "_encounter.eu1" + ".png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()


    #
    # Main plot: Azimuth and Elevation
    label = "Aircraft1 Class: " + cls.className(self.aircraft1Class) + " (h=" + str(
      int(units.m_to_ft(self.aircraft1[0].altitude))) + " -- " + str(
      int(units.m_to_ft(self.aircraft1[len(self.aircraft1) - 2].altitude))) + ")"
    plt.plot(az1, ele1, color=aircraft1Color, label=label)
    plt.plot(az1[0], ele1[0], marker='x', color=aircraft1Color)
    plt.plot(az1[nTest - 2], ele1[nTest - 2], marker='o', color=aircraft1Color)

    label = "Intruder: " + cls.className(self.aircraft2Class) + " (h=" + str(
      int(units.m_to_ft(self.aircraft2[0].altitude))) + " -- " + str(
      int(units.m_to_ft(self.aircraft2[len(self.aircraft2) - 2].altitude))) + ")"
    plt.plot(az2, ele2, color=aircraft2Color, label=label)
    plt.plot(az2[0], ele2[0], marker='x', color=aircraft2Color)
    plt.plot(az2[nTest - 2], ele2[nTest - 2], marker='o', color=aircraft2Color)

    # Add NMAC and RWC conflicts
    for i in range(nTest):
      if self.aircraftNMAC[i]:
        plt.plot(az1[i], hd1[i], marker='o', markersize=markerSize, color='red')
        plt.plot(az2[i], hd2[i], marker='o', markersize=markerSize, color='red')
      else:
        if self.aircraft1TCAS[i] != None and self.aircraft1TCAS[i].conflict():
          plt.plot(az1[i], ele1[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft1RWC[i].WCV:
          plt.plot(az1[i], ele1[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft1RWC[i].isAlert():
          if self.aircraft1RWC[i].isWarningAlert():
            plt.plot(az1[i], ele1[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft1RWC[i].isCorrectiveAlert():
            plt.plot(az1[i], ele1[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(az1[i], ele1[i], marker='.', markersize=markerSize, color='green')

        if self.aircraft2TCAS[i] != None and self.aircraft2TCAS[i].conflict():
          plt.plot(az2[i], ele2[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft2RWC[i].WCV:
          plt.plot(az2[i], ele2[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft2RWC[i].isAlert():
          if self.aircraft2RWC[i].isWarningAlert():
            plt.plot(az2[i], ele2[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft2RWC[i].isCorrectiveAlert():
            plt.plot(az2[i], ele2[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft2RWC[i].isPreventiveAlert():
            plt.plot(az2[i], ele2[i], marker='.', markersize=markerSize, color='green')

    plt.legend(prop={'size': legendSize})

    plt.suptitle(
      "Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(self.id).zfill(9) + " (x --> o) ")
    plt.xlabel('Azimuth (deg)')
    plt.ylabel('Elevation (deg)')
    plt.title("Azimuth / Elevation")
    fig = plt.gcf()
    filepath = folder + "\\Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(9) + "_azimuth-elevation.eu1" + ".png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()


    #########################3

    #
    # Main plot: TCPA
    label = "Aircraft1 Class: " + cls.className(self.aircraft1Class) + " (h=" + str(
      int(units.m_to_ft(self.aircraft1[0].altitude))) + " -- " + str(
      int(units.m_to_ft(self.aircraft1[len(self.aircraft1) - 2].altitude))) + ")"
    plt.plot(time, vsp1, color=aircraft1Color, label=label)
    plt.plot(time[0], vsp1[0], marker='x', color=aircraft1Color)
    plt.plot(time[nTest - 2], vsp1[nTest - 2], marker='o', color=aircraft1Color)

    label = "Intruder: " + cls.className(self.aircraft2Class) + " (h=" + str(
      int(units.m_to_ft(self.aircraft2[0].altitude))) + " -- " + str(
      int(units.m_to_ft(self.aircraft2[len(self.aircraft2) - 1].altitude))) + ")"
    plt.plot(time, vsp2, color=aircraft2Color, label=label)
    plt.plot(time[0], vsp2[0], marker='x', color=aircraft2Color)
    plt.plot(time[nTest - 2], vsp2[nTest - 2], marker='o', color=aircraft2Color)

    # Add NMAC and RWC conflicts
    for i in range(nTest - 1):
      if self.aircraftNMAC[i]:
        plt.plot(time[i], vsp1[i], marker='o', markersize=markerSize, color='red')
        plt.plot(time[i], vsp2[i], marker='o', markersize=markerSize, color='red')
      else:
        if self.aircraft1TCAS[i] != None and self.aircraft1TCAS[i].conflict():
          plt.plot(time[i], vsp1[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft1RWC[i].WCV:
          plt.plot(time[i], vsp1[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft1RWC[i].isAlert():
          if self.aircraft1RWC[i].isWarningAlert():
            plt.plot(time[i], vsp1[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft1RWC[i].isCorrectiveAlert():
            plt.plot(time[i], vsp1[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(time[i], vsp1[i], marker='.', markersize=markerSize, color='green')

        if self.aircraft2TCAS[i] != None and self.aircraft2TCAS[i].conflict():
          plt.plot(time[i], vsp2[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft2RWC[i].WCV:
          plt.plot(time[i], vsp2[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft2RWC[i].isAlert():
          if self.aircraft2RWC[i].isWarningAlert():
            plt.plot(time[i], vsp2[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft2RWC[i].isCorrectiveAlert():
            plt.plot(time[i], vsp2[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft2RWC[i].isPreventiveAlert():
            plt.plot(time[i], vsp2[i], marker='.', markersize=markerSize, color='green')


    plt.legend(prop={'size': legendSize})

    plt.suptitle("Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(self.id).zfill(9) + " (x --> o) ")
    plt.xlabel('time (s)')
    plt.ylabel('speed (fpm)')
    plt.title("Vertical speed")
    fig = plt.gcf()
    filepath = folder + "\\Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(9) + "_vspeed.eu1" + ".png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()




  def plotEncounterDEG(self, root_folder):

    textSize = 20
    axisSize = 24
    titleSize = 28
    lineSize = 4
    markerSize = 12
    legendSize = 12

    nTest = min(len(self.aircraft1), len(self.aircraft2))

    time = [0] * (nTest - 1)

    vx1 = [0] * (nTest - 1)
    vy1 = [0] * (nTest - 1)
    vz1 = [0] * (nTest - 1)

    vx2 = [0] * (nTest - 1)
    vy2 = [0] * (nTest - 1)
    vz2 = [0] * (nTest - 1)

    hsp1 = [0] * (nTest - 1)
    hsp2 = [0] * (nTest - 1)
    vsp1 = [0] * (nTest - 1)
    vsp2 = [0] * (nTest - 1)

    hd1 = [0] * (nTest - 1)
    hd2 = [0] * (nTest - 1)

    hdistance = [0] * (nTest - 1)
    vdistance = [0] * (nTest - 1)

    az1 = [0] * (nTest - 1)
    az2 = [0] * (nTest - 1)
    ele1 = [0] * (nTest - 1)
    ele2 = [0] * (nTest - 1)

    taumod = [0] * (nTest - 1)
    taumod_Caution_Warning=[0] * (nTest - 1)


    if cls.isRPAS(self.aircraft1Class):
      aircraft1Color = 'blue'
    else:
      aircraft1Color = 'dimgray'

    if cls.isRPAS(self.aircraft2Class):
      aircraft2Color = 'navy'
    else:
      aircraft2Color = 'dimgray'


    if not cls.isRPAS(self.aircraft1Class) and not cls.isRPAS(self.aircraft2Class):
      aircraft1Color = 'lightsteelblue'
      aircraft2Color = 'lightgreen'

    encounter_folder = "Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(9)
    folder = os.path.join(root_folder, encounter_folder)

    if os.path.exists(folder):
      print("Removing folder...\n")
      shutil.rmtree(folder)

    os.mkdir(folder)

    #################################
    rwc_param = rwc.HAZ_parameters(0, 0, 0, 0)
    rwc_param.DMOD = 60
    rwc_param.TAUMOD = 35
    rwc_param_Caution_Warning=rwc.HAZ_parameters(0, 0, 0, 0)
    rwc_param_Caution_Warning.DMOD = 40
    rwc_param_Caution_Warning.TAUMOD = 20


    for i in range(nTest - 1):
      time[i] = self.aircraft1[i].time

      vx1[i] = (self.aircraft1[i].x_loc)
      vy1[i] = (self.aircraft1[i].y_loc)
      vz1[i] = (self.aircraft1[i].altitude)
      vx2[i] = (self.aircraft2[i].x_loc)
      vy2[i] = (self.aircraft2[i].y_loc)
      vz2[i] = (self.aircraft2[i].altitude)

      hsp1[i] = (self.aircraft1[i].h_speed)
      vsp1[i] = (self.aircraft1[i].v_speed)
      hsp2[i] = (self.aircraft2[i].h_speed)
      vsp2[i] = (self.aircraft2[i].v_speed)

      hd1[i] = self.aircraft1[i].bearing
      hd2[i] = self.aircraft2[i].bearing

      hdistance[i] = (self.aircraft1NCS[i].range)/10
      vdistance[i] = (abs(self.aircraft1[i].altitude - self.aircraft2[i].altitude))

      ele1[i] = self.aircraft1NCS[i].elevation
      az1[i] = self.aircraft1NCS[i].azimuth
      ele2[i] = self.aircraft2NCS[i].elevation
      az2[i] = self.aircraft2NCS[i].azimuth


      s1 = np.array([vx1[i], vy1[i]])
      v1 = np.array([hsp1[i], vsp1[i]])
      s2 = np.array([vx2[i], vy2[i]])
      v2 = np.array([hsp2[i], vsp2[i]])
      s = s2 - s1
      v = v2 - v1

      #######################################
      taumod[i] = Taumod.horizontal_tvar(s, v, rwc_param)
      taumod_Caution_Warning[i] = Taumod.horizontal_tvar(s, v, rwc_param_Caution_Warning)



    #
    # Main plot: Horizontal plot
    plt.figure()
    #label = "Ownship: " + cls.className(self.aircraft1Class) + " (h=" + str(int((self.aircraft1[0].altitude))) + " -- " + str(int((self.aircraft1[len(self.aircraft1) - 2].altitude))) + ")"
    labelO = "Ownship: " + cls.className(self.aircraft1Class)
    plt.plot(vx1, vy1, color=aircraft1Color, label=labelO, linewidth=lineSize)
    plt.plot(vx1[0], vy1[0], marker='x', color=aircraft1Color, linewidth=lineSize)
    plt.plot(vx1[nTest-2], vy1[nTest-2], marker='o', color=aircraft1Color, linewidth=lineSize)

    #label = "Intruder: " + cls.className(self.aircraft2Class) + " (h=" + str(int((self.aircraft2[0].altitude))) + " -- " + str(int((self.aircraft2[len(self.aircraft2)-2].altitude))) + ")"
    labelI = "Intruder: " + cls.className(self.aircraft2Class)
    plt.plot(vx2, vy2, color=aircraft2Color, label=labelI, linewidth=lineSize)
    plt.plot(vx2[0], vy2[0], marker='x', color=aircraft2Color, linewidth=lineSize)
    plt.plot(vx2[nTest-2], vy2[nTest-2], marker='o', color=aircraft2Color, linewidth=lineSize)

    plt.legend([labelO,labelI], loc='best', fontsize=legendSize)


    # Add NMAC and RWC conflicts
    for i in range(nTest - 1):
      if self.aircraftNMAC[i]:
        plt.plot(vx1[i], vy1[i], marker='o', markersize=markerSize, color='red')
        plt.plot(vx2[i], vy2[i], marker='o', markersize=markerSize, color='red')
      else:
        if self.aircraft1TCAS[i] != None and self.aircraft1TCAS[i].conflict():
          plt.plot(vx1[i], vy1[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft1RWC[i].WCV:
          plt.plot(vx1[i], vy1[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft1RWC[i].isAlert():
          if self.aircraft1RWC[i].isWarningAlert():
            plt.plot(vx1[i], vy1[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft1RWC[i].isCorrectiveAlert():
            plt.plot(vx1[i], vy1[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(vx1[i], vy1[i], marker='.', markersize=markerSize, color='green')

        if self.aircraft2TCAS[i] != None and self.aircraft2TCAS[i].conflict():
          plt.plot(vx2[i], vy2[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft2RWC[i].WCV:
          plt.plot(vx2[i], vy2[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft2RWC[i].isAlert():
          if self.aircraft2RWC[i].isWarningAlert():
            plt.plot(vx2[i], vy2[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft2RWC[i].isCorrectiveAlert():
            plt.plot(vx2[i], vy2[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(vx2[i], vy2[i], marker='.', markersize=markerSize, color='green')


    plt.axis('equal')
    #plt.legend(prop={'size': legendSize})

    plt.suptitle("Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(self.id).zfill(9) + " (x --> o) ")
    plt.title("Horizontal trajectories", fontsize=titleSize)
    plt.xlabel('X (m)', fontsize=axisSize)
    plt.ylabel('Y (m)', fontsize=axisSize)
    plt.tick_params(axis='both', which='major', labelsize=textSize)
    plt.tick_params(axis='both', which='minor', labelsize=textSize-2)

    fig=plt.gcf()
    filepath = os.path.join(folder, "Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(9) + "_horizontal.eu1" + ".png")
    print("Generating figure: " + filepath)
    fig.set_size_inches(14.0, 14.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()
    #plt.show()


    #
    # Main plot: Vertical plot
    plt.figure()
    #label = "Ownship: " + cls.className(self.aircraft1Class) + " (h=" + str(int((self.aircraft1[0].altitude))) + " -- " + str(int((self.aircraft1[len(self.aircraft1) - 2].altitude))) + ")"
    labelO = "Ownship: " + cls.className(self.aircraft1Class)
    plt.plot(time, vz1, color=aircraft1Color, label=labelO, linewidth=lineSize)
    plt.plot(time[0], vz1[0], marker='x', color=aircraft1Color, linewidth=lineSize)
    plt.plot(time[nTest-2], vz1[nTest-2], marker='o', color=aircraft1Color, linewidth=lineSize)

    #label = "Intruder: " + cls.className(self.aircraft2Class) + " (h=" + str(int((self.aircraft2[0].altitude))) + " -- " + str(int((self.aircraft2[len(self.aircraft2)-2].altitude))) + ")"
    labelI = "Intruder: " + cls.className(self.aircraft2Class)
    plt.plot(time, vz2, color=aircraft2Color, label=labelI, linewidth=lineSize)
    plt.plot(time[0], vz2[0], marker='x', color=aircraft2Color, linewidth=lineSize)
    plt.plot(time[nTest-2], vz2[nTest-2], marker='o', color=aircraft2Color, linewidth=lineSize)

    plt.legend([labelO, labelI], loc='best', fontsize=legendSize)

    # Add NMAC and RWC conflicts
    for i in range(nTest - 1):
      if self.aircraftNMAC[i]:
        plt.plot(time[i], vz1[i], marker='o', markersize=markerSize, color='red')
        plt.plot(time[i], vz2[i], marker='o', markersize=markerSize, color='red')
      else:
        if self.aircraft1TCAS[i] != None and self.aircraft1TCAS[i].conflict():
          plt.plot(time[i], vz1[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft1RWC[i].WCV:
          plt.plot(time[i], vz1[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft1RWC[i].isAlert():
          if self.aircraft1RWC[i].isWarningAlert():
            plt.plot(time[i], vz1[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft1RWC[i].isCorrectiveAlert():
            plt.plot(time[i], vz1[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(time[i], vz1[i], marker='.', markersize=markerSize, color='green')

        if self.aircraft2TCAS[i] != None and self.aircraft2TCAS[i].conflict():
          plt.plot(time[i], vz2[i], marker='o', markersize=markerSize, color='violet')
        if self.aircraft2RWC[i].WCV:
          plt.plot(time[i], vz2[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft2RWC[i].isAlert():
          if self.aircraft2RWC[i].isWarningAlert():
            plt.plot(time[i], vz2[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft2RWC[i].isCorrectiveAlert():
            plt.plot(time[i], vz2[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(time[i], vz2[i], marker='.', markersize=markerSize, color='green')

    plt.suptitle("Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(self.id).zfill(9) + " (x --> o) ")
    plt.title("Vertical trajectories", fontsize=titleSize)
    plt.xlabel("time (s)", fontsize=axisSize)
    plt.ylabel("altitude (m)", fontsize=axisSize)
    plt.tick_params(axis='both', which='major', labelsize=textSize)
    plt.tick_params(axis='both', which='minor', labelsize=textSize-2)

    fig=plt.gcf()
    filepath = os.path.join(folder, "Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(9) + "_vertical.eu1" + ".png")
    print("Generating figure: " + filepath)
    fig.set_size_inches(14.0, 14.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()


    #
    # Main plot: Horizontal speed
    plt.figure()
    #label = "Ownship: " + cls.className(self.aircraft1Class) + " (h=" + str(int((self.aircraft1[0].altitude))) + " -- " + str(int((self.aircraft1[len(self.aircraft1) - 2].altitude))) + ")"
    labelO = "Ownship: " + cls.className(self.aircraft1Class)
    plt.plot(time, hsp1, color=aircraft1Color, label=labelO, linewidth=lineSize)
    plt.plot(time[0], hsp1[0], marker='x', color=aircraft1Color, linewidth=lineSize)
    plt.plot(time[nTest-2], hsp1[nTest-2], marker='o', color=aircraft1Color, linewidth=lineSize)

    #label = "Intruder: " + cls.className(self.aircraft2Class) + " (h=" + str(int((self.aircraft2[0].altitude))) + " -- " + str(int((self.aircraft2[len(self.aircraft2)-2].altitude))) + ")"
    labelI = "Intruder: " + cls.className(self.aircraft2Class)
    plt.plot(time, hsp2, color=aircraft2Color, label=labelI, linewidth=lineSize)
    plt.plot(time[0], hsp2[0], marker='x', color=aircraft2Color, linewidth=lineSize)
    plt.plot(time[nTest-2], hsp2[nTest-2], marker='o', color=aircraft2Color, linewidth=lineSize)

    plt.legend([labelO, labelI], loc='best', fontsize=legendSize)

    # Add NMAC and RWC conflicts
    for i in range(nTest - 1):
      if self.aircraftNMAC[i]:
        plt.plot(time[i], hsp1[i], marker='o', markersize=markerSize, color='red')
        plt.plot(time[i], hsp2[i], marker='o', markersize=markerSize, color='red')
      else:
        if self.aircraft1TCAS[i] != None and self.aircraft1TCAS[i].conflict():
          plt.plot(time[i], hsp1[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft1RWC[i].WCV:
          plt.plot(time[i], hsp1[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft1RWC[i].isAlert():
          if self.aircraft1RWC[i].isWarningAlert():
            plt.plot(time[i], hsp1[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft1RWC[i].isCorrectiveAlert():
            plt.plot(time[i], hsp1[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(time[i], hsp1[i], marker='.', markersize=markerSize, color='green')

        if self.aircraft2TCAS[i] != None and self.aircraft2TCAS[i].conflict():
          plt.plot(time[i], hsp2[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft2RWC[i].WCV:
          plt.plot(time[i], hsp2[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft2RWC[i].isAlert():
          if self.aircraft2RWC[i].isWarningAlert():
            plt.plot(time[i], hsp2[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft2RWC[i].isCorrectiveAlert():
            plt.plot(time[i], hsp2[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft2RWC[i].isPreventiveAlert():
            plt.plot(time[i], hsp2[i], marker='.', markersize=markerSize, color='green')


    plt.suptitle("Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(self.id).zfill(9) + " (x --> o) ")
    plt.title("Horizontal speed", fontsize=titleSize)
    plt.xlabel('time (s)', fontsize=axisSize)
    plt.ylabel('speed (m/s)', fontsize=axisSize)
    plt.tick_params(axis='both', which='major', labelsize=textSize)
    plt.tick_params(axis='both', which='minor', labelsize=textSize-2)

    fig=plt.gcf()
    filepath = os.path.join(folder, "Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(9) + "_hspeed.eu1" + ".png")
    print("Generating figure: " + filepath)
    fig.set_size_inches(14.0, 14.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()


    #
    # Main plot: Vertical speed
    #label = "Ownship: " + cls.className(self.aircraft1Class) + " (h=" + str(int((self.aircraft1[0].altitude))) + " -- " + str(int((self.aircraft1[len(self.aircraft1) - 2].altitude))) + ")"
    labelO = "Ownship: " + cls.className(self.aircraft1Class)
    plt.plot(time, vsp1, color=aircraft1Color, label=labelO, linewidth=lineSize)
    plt.plot(time[0], vsp1[0], marker='x', color=aircraft1Color, linewidth=lineSize)
    plt.plot(time[nTest - 2], vsp1[nTest - 2], marker='o', color=aircraft1Color, linewidth=lineSize)

    #label = "Intruder: " + cls.className(self.aircraft2Class) + " (h=" + str(int((self.aircraft2[0].altitude))) + " -- " + str(int((self.aircraft2[len(self.aircraft2) - 1].altitude))) + ")"
    labelI = "Intruder: " + cls.className(self.aircraft2Class)
    plt.plot(time, vsp2, color=aircraft2Color, label=labelI, linewidth=lineSize)
    plt.plot(time[0], vsp2[0], marker='x', color=aircraft2Color, linewidth=lineSize)
    plt.plot(time[nTest - 2], vsp2[nTest - 2], marker='o', color=aircraft2Color, linewidth=lineSize)

    plt.legend([labelO, labelI], loc='best', fontsize=legendSize)

    # Add NMAC and RWC conflicts
    for i in range(nTest - 1):
      if self.aircraftNMAC[i]:
        plt.plot(time[i], vsp1[i], marker='o', markersize=markerSize, color='red')
        plt.plot(time[i], vsp2[i], marker='o', markersize=markerSize, color='red')
      else:
        if self.aircraft1TCAS[i] != None and self.aircraft1TCAS[i].conflict():
          plt.plot(time[i], vsp1[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft1RWC[i].WCV:
          plt.plot(time[i], vsp1[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft1RWC[i].isAlert():
          if self.aircraft1RWC[i].isWarningAlert():
            plt.plot(time[i], vsp1[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft1RWC[i].isCorrectiveAlert():
            plt.plot(time[i], vsp1[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(time[i], vsp1[i], marker='.', markersize=markerSize, color='green')

        if self.aircraft2TCAS[i] != None and self.aircraft2TCAS[i].conflict():
          plt.plot(time[i], vsp2[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft2RWC[i].WCV:
          plt.plot(time[i], vsp2[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft2RWC[i].isAlert():
          if self.aircraft2RWC[i].isWarningAlert():
            plt.plot(time[i], vsp2[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft2RWC[i].isCorrectiveAlert():
            plt.plot(time[i], vsp2[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft2RWC[i].isPreventiveAlert():
            plt.plot(time[i], vsp2[i], marker='.', markersize=markerSize, color='green')


    #plt.legend(prop={'size': legendSize})

    plt.suptitle("Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(self.id).zfill(9) + " (x --> o) ")
    plt.xlabel('time (s)', fontsize=axisSize)
    plt.ylabel('speed (m/s)', fontsize=axisSize)
    plt.tick_params(axis='both', which='major', labelsize=textSize)
    plt.tick_params(axis='both', which='minor', labelsize=textSize-2)

    plt.title("Vertical speed", fontsize=titleSize)
    fig = plt.gcf()
    filepath = os.path.join(folder, "Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(9) + "_vspeed.eu1" + ".png")
    print("Generating figure: " + filepath)
    fig.set_size_inches(14.0, 14.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()


    #
    # Main plot: Bearing
    #label = "Ownship: " + cls.className(self.aircraft1Class) + " (h=" + str(int((self.aircraft1[0].altitude))) + " -- " + str(int((self.aircraft1[len(self.aircraft1) - 2].altitude))) + ")"
    labelO = "Ownship: " + cls.className(self.aircraft1Class)
    plt.plot(time, hd1, color=aircraft1Color, label=labelO, linewidth=lineSize)
    plt.plot(time[0], hd1[0], marker='x', color=aircraft1Color, linewidth=lineSize)
    plt.plot(time[nTest - 2], hd1[nTest - 2], marker='o', color=aircraft1Color, linewidth=lineSize)

    #label = "Intruder: " + cls.className(self.aircraft2Class) + " (h=" + str(int((self.aircraft2[0].altitude))) + " -- " + str(int((self.aircraft2[len(self.aircraft2) - 2].altitude))) + ")"
    labelI = "Intruder: " + cls.className(self.aircraft2Class)
    plt.plot(time, hd2, color=aircraft2Color, label=labelI, linewidth=lineSize)
    plt.plot(time[0], hd2[0], marker='x', color=aircraft2Color, linewidth=lineSize)
    plt.plot(time[nTest - 2], hd2[nTest - 2], marker='o', color=aircraft2Color, linewidth=lineSize)

    plt.legend([labelO, labelI], loc='best', fontsize=legendSize)

    # Add NMAC and RWC conflicts
    for i in range(nTest - 1):
      if self.aircraftNMAC[i]:
        plt.plot(time[i], hd1[i], marker='o', markersize=markerSize, color='red')
        plt.plot(time[i], hd2[i], marker='o', markersize=markerSize, color='red')
      else:
        if self.aircraft1TCAS[i] != None and self.aircraft1TCAS[i].conflict():
          plt.plot(time[i], hd1[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft1RWC[i].WCV:
          plt.plot(time[i], hd1[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft1RWC[i].isAlert():
          if self.aircraft1RWC[i].isWarningAlert():
            plt.plot(time[i], hd1[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft1RWC[i].isCorrectiveAlert():
            plt.plot(time[i], hd1[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(time[i], hd1[i], marker='.', markersize=markerSize, color='green')

        if self.aircraft2TCAS[i] != None and self.aircraft2TCAS[i].conflict():
          plt.plot(time[i], hd2[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft2RWC[i].WCV:
          plt.plot(time[i], hd2[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft2RWC[i].isAlert():
          if self.aircraft2RWC[i].isWarningAlert():
            plt.plot(time[i], hd2[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft2RWC[i].isCorrectiveAlert():
            plt.plot(time[i], hd2[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft2RWC[i].isPreventiveAlert():
            plt.plot(time[i], hd2[i], marker='.', markersize=markerSize, color='green')


    #plt.legend(prop={'size': legendSize})

    plt.suptitle("Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(self.id).zfill(9) + " (x --> o) ")
    plt.xlabel('time (s)', fontsize=axisSize)
    #plt.ylabel('bearing (deg)')
    plt.title("Bearing", fontsize=titleSize)
    plt.tick_params(axis='both', which='major', labelsize=textSize)
    plt.tick_params(axis='both', which='minor', labelsize=textSize-2)

    fig = plt.gcf()
    filepath = os.path.join(folder, "Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(9) + "_bearing.eu1" + ".png")
    print("Generating figure: " + filepath)
    fig.set_size_inches(14.0, 14.0)
    fig.savefig(filepath)
    plt.close(fig)


    #############################

    # Main plot: Caution and Warning Taumod plot
    label = "Caution and Warning Taumod "
    plt.plot(time, taumod_Caution_Warning, color='lightgreen', label=label)
    plt.hlines(rwc_param_Caution_Warning.TAUMOD, time[0], time[-1], color="red", label="TAUMOD= 20s")

    plt.legend(prop={'size': 6})

    plt.suptitle("Caution and Warning WCV Taumod_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(self.id).zfill(9) + " (x --> o) ")
    plt.title("Taumod_Distribution data")
    fig = plt.gcf()
    filepath = os.path.join(folder, "Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(9) + "_caut_warn_taumod.eu1" + ".png")
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)

    # Main plot: Preventive Taumod plot
    label = "Preventive Taumod "
    plt.plot(time, taumod, color='lightgreen', label=label)
    plt.hlines(rwc_param.TAUMOD, time[0], time[-1], color="red", label="TAUMOD= 35s")

    # Add NMAC and RWC conflicts
    # for i in range(nTest - 1):
    #   if self.aircraftNMAC[i]:
    #     plt.plot(time[i], hdistance[i], marker='o', markersize=markerSize, color='red')
    #     plt.plot(time[i], vdistance[i], marker='o', markersize=markerSize, color='red')
    #   else:
    #     if self.aircraft1TCAS[i] != None and self.aircraft1TCAS[i].conflict():
    #       plt.plot(time[i], hdistance[i], marker='o', markersize=markerSize, color='violet')
    #       plt.plot(time[i], vdistance[i], marker='o', markersize=markerSize, color='violet')
    #     if self.aircraft1RWC[i].WCV:
    #       plt.plot(time[i], hdistance[i], marker='.', markersize=markerSize, color='red')
    #       plt.plot(time[i], vdistance[i], marker='.', markersize=markerSize, color='red')
    #     elif self.aircraft1RWC[i].isAlert():
    #       if self.aircraft1RWC[i].isWarningAlert():
    #         plt.plot(time[i], hdistance[i], marker='.', markersize=markerSize, color='orange')
    #         plt.plot(time[i], vdistance[i], marker='.', markersize=markerSize, color='orange')
    #       elif self.aircraft1RWC[i].isCorrectiveAlert():
    #         plt.plot(time[i], hdistance[i], marker='.', markersize=markerSize, color='yellow')
    #         plt.plot(time[i], vdistance[i], marker='.', markersize=markerSize, color='yellow')
    #       elif self.aircraft1RWC[i].isPreventiveAlert():
    #         plt.plot(time[i], hdistance[i], marker='.', markersize=markerSize, color='green')
    #         plt.plot(time[i], vdistance[i], marker='.', markersize=markerSize, color='green')

    plt.legend(prop={'size': 6})

    plt.suptitle("Preventive WCV Taumod_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(self.id).zfill(9) + " (x --> o) ")
    plt.title("Taumod_Distribution data")
    fig = plt.gcf()
    filepath = os.path.join(folder, "Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(9) + "_prev_taumod.eu1" + ".png")
    print("Generating figure: " + filepath)
    fig.set_size_inches(14.0, 14.0)
    fig.savefig(filepath)
    plt.close(fig)

    # Main plot: TAUMOD subplots
    label = "Taumod distribution comparison"
    fig, (ax1, ax2) = plt.subplots(2)
    ax1.plot(time, taumod_Caution_Warning)
    ax1.set_title('Caution-Warning TAUMOD Distribution data')

    ax2.plot(time,taumod )
    ax2.set_title('Preventive TAUMOD Distribution data')

    plt.suptitle("Taumod_Distribution Comparison")
    fig = plt.gcf()
    filepath = os.path.join(folder, "Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(9) + "taumod_comparison.eu1" + ".png")
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)

    #
    # Main plot: Interval plot

    # Aircraft 1
    for i in range(nTest - 1):
      gotInterval = False
      if self.aircraft1WarningTrace[i] is not None and self.aircraft1WarningTrace[i].HInterval is not None and self.aircraft1WarningTrace[i].conflict:
        imin = self.aircraft1WarningTrace[i].HInterval.low
        imax = self.aircraft1WarningTrace[i].HInterval.up
        icolor = 'orange'
        gotInterval = True

      elif self.aircraft1CautionTrace[i] is not None and self.aircraft1CautionTrace[i].HInterval is not None and self.aircraft1CautionTrace[i].conflict:
        imin = self.aircraft1CautionTrace[i].HInterval.low
        imax = self.aircraft1CautionTrace[i].HInterval.up
        icolor = 'yellow'
        gotInterval = True

      elif self.aircraft1PreventiveTrace[i] is not None and self.aircraft1PreventiveTrace[i].HInterval is not None and self.aircraft1PreventiveTrace[i].conflict:
        imin = self.aircraft1PreventiveTrace[i].HInterval.low
        imax = self.aircraft1PreventiveTrace[i].HInterval.up
        icolor = 'green'
        gotInterval = True

      if gotInterval:
        plt.vlines(x=time[i], ymin=imin , ymax=imax, colors=icolor)

    plt.legend(prop={'size': 6})

    plt.suptitle(
      "Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(self.id).zfill(
        9) + " (x --> o) ")
    plt.title("Encounter horizontal interval data")
    fig = plt.gcf()
    filepath = os.path.join(folder, "Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(9) + "_hInterval.eu1" + ".png")
    print("Generating figure: " + filepath)
    fig.set_size_inches(14.0, 14.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()


    #
    # Main plot: Conflict plot
    labelH = "H distance/10"
    plt.plot(time, hdistance, color='lightgreen', label=labelH, linewidth=lineSize)
    labelI = "V distance"
    plt.plot(time, vdistance, color='lightsteelblue', label=labelI, linewidth=lineSize)

    plt.legend([labelH, labelI], loc='best', fontsize=legendSize)


    # Add NMAC and RWC conflicts
    for i in range(nTest - 1):
      if self.aircraftNMAC[i]:
        plt.plot(time[i], hdistance[i], marker='o', markersize=markerSize, color='red')
        plt.plot(time[i], vdistance[i], marker='o', markersize=markerSize, color='red')
      else:
        if self.aircraft1TCAS[i] != None and self.aircraft1TCAS[i].conflict():
          plt.plot(time[i], hdistance[i], marker='o', markersize=markerSize, color='violet')
          plt.plot(time[i], vdistance[i], marker='o', markersize=markerSize, color='violet')
        if self.aircraft1RWC[i].WCV:
          plt.plot(time[i], hdistance[i], marker='.', markersize=markerSize, color='red')
          plt.plot(time[i], vdistance[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft1RWC[i].isAlert():
          if self.aircraft1RWC[i].isWarningAlert():
            plt.plot(time[i], hdistance[i], marker='.', markersize=markerSize, color='orange')
            plt.plot(time[i], vdistance[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft1RWC[i].isCorrectiveAlert():
            plt.plot(time[i], hdistance[i], marker='.', markersize=markerSize, color='yellow')
            plt.plot(time[i], vdistance[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(time[i], hdistance[i], marker='.', markersize=markerSize, color='green')
            plt.plot(time[i], vdistance[i], marker='.', markersize=markerSize, color='green')

    #plt.legend(prop={'size': 6})

    plt.suptitle("Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(self.id).zfill(9) + " (x --> o) ")
    plt.xlabel('time (s)', fontsize=axisSize)
    plt.ylabel('distance (m)', fontsize=axisSize)
    plt.tick_params(axis='both', which='major', labelsize=textSize)
    plt.tick_params(axis='both', which='minor', labelsize=textSize-2)

    plt.title("Encounter Distance Data", fontsize=titleSize)
    fig = plt.gcf()
    filepath = os.path.join(folder, "Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(9) + "_encounter.eu1" + ".png")
    print("Generating figure: " + filepath)
    fig.set_size_inches(14.0, 14.0)
    fig.savefig(filepath)
    plt.close(fig)



  def writeBasicInformation12(self, row):
    ap = row["Approach Angle"]
    ac1Mode = row["AC1 Mode A"]
    ac1Alt = row["AC1 Altitude"]
    ac1VR = row["AC1 Vertical"]
    ac1Speed = row["AC1 Speed"]
    ac2Mode = row["AC2 Mode A"]
    ac2Alt = row["AC2 Altitude"]
    ac2VR = row["AC2 Vertical"]
    ac2Speed = row["AC2 Speed"]

    return str(self.layer) + ', ' + str(ap) + ', ' + str(ac1Mode) + ', ' + str(ac1Alt) + ', ' + str(ac1VR) + ', ' + str(ac1Speed) + ', ' +str(ac2Mode) + ', ' + str(ac2Alt) + ', ' + str(ac2VR) + ', ' + str(ac2Speed)


  def writeBasicInformation21(self, row):
    ap = row["Approach Angle"]
    ac1Mode = row["AC1 Mode A"]
    ac1Alt = row["AC1 Altitude"]
    ac1VR = row["AC1 Vertical"]
    ac1Speed = row["AC1 Speed"]
    ac2Mode = row["AC2 Mode A"]
    ac2Alt = row["AC2 Altitude"]
    ac2VR = row["AC2 Vertical"]
    ac2Speed = row["AC2 Speed"]

    return str(self.layer) + ', ' + str(ap) + ', ' + str(ac2Mode) + ', ' + str(ac2Alt) + ', ' + str(ac2VR) + ', ' + str(ac2Speed) + ', ' + str(ac1Mode) + ', ' + str(ac1Alt) + ', ' + str(ac1VR) + ', ' + str(ac1Speed)


  def writeBasicInformationHeader(self):
    return "layer" + ', ' + "Approach Angle" + ', ' + "AC1 Mode A" + ', ' + "AC1 Altitude" + ', ' + "AC1 Vertical" + ', ' + "AC1 Speed" + ', ' + "AC2 Mode A" + ', ' + "AC2 Altitude" + ', ' + "AC2 Vertical" + ', ' + "AC2 Speed"


  def writeBasicDEGInformation12(self, row):
    ap = row["Approach Angle"]
    ac1ModeH = row["AC1 Horizontal Mode"]
    ac1ModeV = row["AC1 Vertical Mode"]
    ac1ModeS = row["AC1 Speed Mode"]
    ac1Alt = row["AC1 Altitude"]
    ac1Speed = row["AC1 Speed"]
    ac1VR = row["AC1 Vertical Speed"]
    ac2ModeH = row["AC2 Horizontal Mode"]
    ac2ModeV = row["AC2 Vertical Mode"]
    ac2ModeS = row["AC2 Speed Mode"]
    ac2Alt = row["AC2 Altitude"]
    ac2Speed = row["AC2 Speed"]
    ac2VR = row["AC2 Vertical Speed"]

    return str(ap) + ', ' + str(ac1ModeH) + ', ' + str(ac1ModeV) + ', ' + str(ac1ModeS) + ', ' + str(ac1Speed) + ', ' + str(ac1Alt) + ', ' + str(ac1VR) + ', ' + str(ac2ModeH) + ', ' + str(ac2ModeV) + ', ' + str(ac2ModeS) + ', ' + str(ac2Speed) + ', ' + str(ac2Alt) + ', ' + str(ac2VR)

  def writeBasicDEGInformation21(self, row):
    ap = row["Approach Angle"]
    ac1ModeH = row["AC1 Horizontal Mode"]
    ac1ModeV = row["AC1 Vertical Mode"]
    ac1ModeS = row["AC1 Speed Mode"]
    ac1Alt = row["AC1 Altitude"]
    ac1Speed = row["AC1 Speed"]
    ac1VR = row["AC1 Vertical Speed"]
    ac2ModeH = row["AC2 Horizontal Mode"]
    ac2ModeV = row["AC2 Vertical Mode"]
    ac2ModeS = row["AC2 Speed Mode"]
    ac2Alt = row["AC2 Altitude"]
    ac2Speed = row["AC2 Speed"]
    ac2VR = row["AC2 Vertical Speed"]

    return str(ap) + ', ' + str(ac2ModeH) + ', ' + str(ac2ModeV) + ', ' + str(ac2ModeS) + ', ' + str(ac2Speed) + ', ' + str(ac2Alt) + ', ' + str(ac2VR) + ', ' + str(ac1ModeH) + ', ' + str(ac1ModeV) + ', ' + str(ac1ModeS) + ', ' + str(ac1Speed) + ', ' + str(ac1Alt) + ', ' + str(ac1VR)


  def writeBasicDEGInformationHeader(self):
    return "Approach Angle" + ', ' + "AC1 Horizontal Mode" + ', ' + "AC1 Vertical Mode" + ', ' + "AC1 Speed Mode" + ', ' + "AC1 Altitude" + ', ' + "AC1 Speed" + ', ' + "AC1 Vertical Speed" + ', ' + "AC2 Horizontal Mode" + ', ' + "AC2 Vertical Mode" + ', ' + "AC2 Speed Mode" + ', ' + "AC2 Altitude" + ', ' + "AC2 Speed" + ', ' + "AC2 Vertical Speed"


  def writeDEGEncounterResult(self, row, result_enc_file):

    result_enc_file.write(str("1") + ', ') # Fake layer
    result_enc_file.write(str(self.id) + ', ' + self.writeBasicDEGInformation12(row) + ', ' + str(self.aircraft1Class) + ', ' + str(self.aircraft2Class) + ', ' + self.encAlertMeasuringAicraft1.toCSV(self.aircraft1NCS, self.aircraft1TCAS) + "\n")
    result_enc_file.write(str("1") + ', ') # Fake layer
    result_enc_file.write(str(self.id) + ', ' + self.writeBasicDEGInformation21(row) + ', ' + str(self.aircraft2Class) + ', ' + str(self.aircraft1Class) + ', ' + self.encAlertMeasuringAicraft2.toCSV(self.aircraft2NCS, self.aircraft2TCAS) + "\n")
    result_enc_file.flush()

  def writeEncounterResult(self, row, result_enc_file):

    '''if 'Time before CPA' in row.axes:
      cpaIdx = int(row['Time before CPA'])
    else:'''
    cpaIdx = 0

    if cls.isOwnshipRPAS(self.aircraft1Class):
      result_enc_file.write(str(self.id) + ', ' + self.writeBasicInformation12(row) + ', ' + str(self.aircraft1Class) + ', ' + str(self.aircraft2Class) + ', ' + self.encAlertMeasuringAicraft1.toCSV(self.aircraft1NCS, self.aircraft1TCAS) + "\n")

    if cls.isOwnshipRPAS(self.aircraft2Class):
      result_enc_file.write(str(self.id) + ', ' + self.writeBasicInformation21(row) + ', ' + str(self.aircraft2Class) + ', ' + str(self.aircraft1Class) + ', ' + self.encAlertMeasuringAicraft2.toCSV(self.aircraft2NCS, self.aircraft2TCAS) + "\n")

    result_enc_file.flush()


  def writeACASEncounterResult(self, row, result_enc_file, acas_enc_file):

    '''if 'Time before CPA' in row.columns:
      cpaIdx = int(row['Time before CPA'])
    else:'''
    cpaIdx = 0

    if cls.isACAS(self.aircraft1Class) and cls.isOwnshipRPAS(self.aircraft2Class):
      result_enc_file.write(str(self.id) + ', ' + self.writeBasicInformation21(row) + ', ' + str(self.aircraft2Class) + ', ' + str(self.aircraft1Class) + ', ' + self.encAlertMeasuringAicraft2.toCSV(cpaIdx, self.aircraft2NCS, self.aircraft2TCAS) + "\n")
      acas_enc_file.write(str(self.id) + ', ' + self.writeBasicInformation12(row) + ', ' + str(self.aircraft1Class) + ', ' + str(self.aircraft2Class) + ', ' + self.encAlertMeasuringAicraft1.toCSV(cpaIdx, self.aircraft1NCS, self.aircraft1TCAS) + "\n")
    elif cls.isOwnshipRPAS(self.aircraft1Class) and cls.isACAS(self.aircraft2Class):
      result_enc_file.write(str(self.id) + ', ' + self.writeBasicInformation12(row) + ', ' + str(self.aircraft1Class) + ', ' + str(self.aircraft2Class) + ', ' + self.encAlertMeasuringAicraft1.toCSV(cpaIdx, self.aircraft1NCS, self.aircraft1TCAS) + "\n")
      acas_enc_file.write(str(self.id) + ', ' + self.writeBasicInformation21(row) + ', ' + str(self.aircraft2Class) + ', ' + str(self.aircraft1Class) + ', ' + self.encAlertMeasuringAicraft2.toCSV(cpaIdx, self.aircraft2NCS, self.aircraft2TCAS) + "\n")

    result_enc_file.flush()


  def writeEncounterHeader(self, result_enc_file):

    result_enc_file.write("EncounterId" + ', ' + self.writeBasicInformationHeader() + ', ' + "aircraft1Class" + ', ' + "aircraft2Class" + ', ' + self.encAlertMeasuringAicraft1.toHeaderCSV() + "\n")
    result_enc_file.flush()

  def writeDEGEncounterHeader(self, result_enc_file):
    result_enc_file.write("layer" + ', ' + "EncounterId" + ', ' + self.writeBasicDEGInformationHeader() + ', ' + "aircraft1Class" + ', ' + "aircraft2Class" + ', ' + self.encAlertMeasuringAicraft1.toHeaderCSV() + "\n")
    result_enc_file.flush()


  def testDistances(self):
    print("Testing horizontal distance: ")
    for i in range(len(self.aircraft1) - 1):
      d = geo.Geodesic.WGS84.Inverse(self.aircraft1[i].y_loc, self.aircraft1[i].x_loc, self.aircraft2[i].y_loc, self.aircraft2[i].x_loc)
      print("Current distance: ", d['s12'])


  def plotEncounterBasic(self, folder):

    nTest = min(len(self.aircraft1), len(self.aircraft2))

    time = [0] * (nTest - 1)

    vx1 = [0] * (nTest - 1)
    vy1 = [0] * (nTest - 1)
    vz1 = [0] * (nTest - 1)

    vx2 = [0] * (nTest - 1)
    vy2 = [0] * (nTest - 1)
    vz2 = [0] * (nTest - 1)

    hsp1 = [0] * (nTest - 1)
    hsp2 = [0] * (nTest - 1)
    vsp1 = [0] * (nTest - 1)
    vsp2 = [0] * (nTest - 1)

    hd1 = [0] * (nTest - 1)
    hd2 = [0] * (nTest - 1)

    hdistance = [0] * (nTest - 1)
    vdistance = [0] * (nTest - 1)

    az1 = [0] * (nTest - 1)
    az2 = [0] * (nTest - 1)
    ele1 = [0] * (nTest - 1)
    ele2 = [0] * (nTest - 1)

    aircraft1Color = 'lightsteelblue'
    aircraft2Color = 'lightgreen'
    legendSize = 6


    for i in range(nTest - 1):
      time[i] = self.aircraft1[i].violates

      vx1[i] = (self.aircraft1[i].x_loc)
      vy1[i] = (self.aircraft1[i].y_loc)
      vz1[i] = (self.aircraft1[i].altitude)
      vx2[i] = (self.aircraft2[i].x_loc)
      vy2[i] = (self.aircraft2[i].y_loc)
      vz2[i] = (self.aircraft2[i].altitude)

      hsp1[i] = (self.aircraft1[i].h_speed)
      vsp1[i] = (self.aircraft1[i].v_speed)
      hsp2[i] = (self.aircraft2[i].h_speed)
      vsp2[i] = (self.aircraft2[i].v_speed)

      hd1[i] = self.aircraft1[i].bearing
      hd2[i] = self.aircraft2[i].bearing


    #
    # Main plot: Horizontal plot
    plt.figure()
    label = "Ownship: " + cls.className(self.aircraft1Class) + " (h=" + str(
      int((self.aircraft1[0].altitude))) + " -- " + str(
      int((self.aircraft1[len(self.aircraft1) - 2].altitude))) + ")"
    plt.plot(vx1, vy1, color=aircraft1Color, label=label)
    plt.plot(vx1[0], vy1[0], marker='x', color=aircraft1Color)
    plt.plot(vx1[nTest - 2], vy1[nTest - 2], marker='o', color=aircraft1Color)

    label = "Intruder: " + cls.className(self.aircraft2Class) + " (h=" + str(
      int((self.aircraft2[0].altitude))) + " -- " + str(
      int((self.aircraft2[len(self.aircraft2) - 2].altitude))) + ")"
    plt.plot(vx2, vy2, color=aircraft2Color, label=label)
    plt.plot(vx2[0], vy2[0], marker='x', color=aircraft2Color)
    plt.plot(vx2[nTest - 2], vy2[nTest - 2], marker='o', color=aircraft2Color)

    plt.axis('equal')
    plt.legend(prop={'size': legendSize})

    plt.suptitle("Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(
      self.id).zfill(9) + " (x --> o) ")
    plt.title("Horizontal trajectories")
    plt.xlabel('X (m)')
    plt.ylabel('Y (m)')
    fig = plt.gcf()
    filepath = folder + "\\Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(
      9) + "_horizontal.eu1" + ".png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()
    # plt.show()

    #
    # Main plot: Vertical plot
    plt.figure()
    label = "Ownship: " + cls.className(self.aircraft1Class) + " (h=" + str(
      int((self.aircraft1[0].altitude))) + " -- " + str(
      int((self.aircraft1[len(self.aircraft1) - 2].altitude))) + ")"
    plt.plot(time, vz1, color=aircraft1Color, label=label)
    plt.plot(time[0], vz1[0], marker='x', color=aircraft1Color)
    plt.plot(time[nTest - 2], vz1[nTest - 2], marker='o', color=aircraft1Color)

    label = "Intruder: " + cls.className(self.aircraft2Class) + " (h=" + str(
      int((self.aircraft2[0].altitude))) + " -- " + str(
      int((self.aircraft2[len(self.aircraft2) - 2].altitude))) + ")"
    plt.plot(time, vz2, color=aircraft2Color, label=label)
    plt.plot(time[0], vz2[0], marker='x', color=aircraft2Color)
    plt.plot(time[nTest - 2], vz2[nTest - 2], marker='o', color=aircraft2Color)

    # Add NMAC and RWC conflicts
    for i in range(nTest - 1):
      if self.aircraftNMAC[i]:
        plt.plot(time[i], vz1[i], marker='o', markersize=markerSize, color='red')
        plt.plot(time[i], vz2[i], marker='o', markersize=markerSize, color='red')
      else:
        if self.aircraft1TCAS[i] != None and self.aircraft1TCAS[i].conflict():
          plt.plot(time[i], vz1[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft1RWC[i].WCV:
          plt.plot(time[i], vz1[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft1RWC[i].isAlert():
          if self.aircraft1RWC[i].isWarningAlert():
            plt.plot(time[i], vz1[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft1RWC[i].isCorrectiveAlert():
            plt.plot(time[i], vz1[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(time[i], vz1[i], marker='.', markersize=markerSize, color='green')

        if self.aircraft2TCAS[i] != None and self.aircraft2TCAS[i].conflict():
          plt.plot(time[i], vz2[i], marker='o', markersize=markerSize, color='violet')
        if self.aircraft2RWC[i].WCV:
          plt.plot(time[i], vz2[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft2RWC[i].isAlert():
          if self.aircraft2RWC[i].isWarningAlert():
            plt.plot(time[i], vz2[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft2RWC[i].isCorrectiveAlert():
            plt.plot(time[i], vz2[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(time[i], vz2[i], marker='.', markersize=markerSize, color='green')

    plt.legend(prop={'size': legendSize})

    plt.suptitle("Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(
      self.id).zfill(9) + " (x --> o) ")
    plt.title("Vertical trajectories")
    plt.xlabel("time (s)")
    plt.ylabel("altitude (m)")
    fig = plt.gcf()
    filepath = folder + "\\Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(
      9) + "_vertical.eu1" + ".png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()

    #
    # Main plot: Horizontal speed
    plt.figure()
    label = "Ownship: " + cls.className(self.aircraft1Class) + " (h=" + str(
      int((self.aircraft1[0].altitude))) + " -- " + str(
      int((self.aircraft1[len(self.aircraft1) - 2].altitude))) + ")"
    plt.plot(time, hsp1, color=aircraft1Color, label=label)
    plt.plot(time[0], hsp1[0], marker='x', color=aircraft1Color)
    plt.plot(time[nTest - 2], hsp1[nTest - 2], marker='o', color=aircraft1Color)

    label = "Intruder: " + cls.className(self.aircraft2Class) + " (h=" + str(
      int((self.aircraft2[0].altitude))) + " -- " + str(
      int((self.aircraft2[len(self.aircraft2) - 2].altitude))) + ")"
    plt.plot(time, hsp2, color=aircraft2Color, label=label)
    plt.plot(time[0], hsp2[0], marker='x', color=aircraft2Color)
    plt.plot(time[nTest - 2], hsp2[nTest - 2], marker='o', color=aircraft2Color)

    # Add NMAC and RWC conflicts
    for i in range(nTest - 1):
      if self.aircraftNMAC[i]:
        plt.plot(time[i], hsp1[i], marker='o', markersize=markerSize, color='red')
        plt.plot(time[i], hsp2[i], marker='o', markersize=markerSize, color='red')
      else:
        if self.aircraft1TCAS[i] != None and self.aircraft1TCAS[i].conflict():
          plt.plot(time[i], hsp1[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft1RWC[i].WCV:
          plt.plot(time[i], hsp1[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft1RWC[i].isAlert():
          if self.aircraft1RWC[i].isWarningAlert():
            plt.plot(time[i], hsp1[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft1RWC[i].isCorrectiveAlert():
            plt.plot(time[i], hsp1[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(time[i], hsp1[i], marker='.', markersize=markerSize, color='green')

        if self.aircraft2TCAS[i] != None and self.aircraft2TCAS[i].conflict():
          plt.plot(time[i], hsp2[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft2RWC[i].WCV:
          plt.plot(time[i], hsp2[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft2RWC[i].isAlert():
          if self.aircraft2RWC[i].isWarningAlert():
            plt.plot(time[i], hsp2[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft2RWC[i].isCorrectiveAlert():
            plt.plot(time[i], hsp2[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft2RWC[i].isPreventiveAlert():
            plt.plot(time[i], hsp1[i], marker='.', markersize=markerSize, color='green')

    plt.legend(prop={'size': legendSize})

    plt.suptitle("Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(
      self.id).zfill(9) + " (x --> o) ")
    plt.title("Horizontal speed")
    plt.xlabel('time (s)')
    plt.ylabel('speed (m/s)')

    fig = plt.gcf()
    filepath = folder + "\\Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(
      9) + "_hspeed.eu1" + ".png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()

    #
    # Main plot: Vertical speed
    label = "Ownship: " + cls.className(self.aircraft1Class) + " (h=" + str(
      int((self.aircraft1[0].altitude))) + " -- " + str(
      int((self.aircraft1[len(self.aircraft1) - 2].altitude))) + ")"
    plt.plot(time, vsp1, color=aircraft1Color, label=label)
    plt.plot(time[0], vsp1[0], marker='x', color=aircraft1Color)
    plt.plot(time[nTest - 2], vsp1[nTest - 2], marker='o', color=aircraft1Color)

    label = "Intruder: " + cls.className(self.aircraft2Class) + " (h=" + str(
      int((self.aircraft2[0].altitude))) + " -- " + str(
      int((self.aircraft2[len(self.aircraft2) - 1].altitude))) + ")"
    plt.plot(time, vsp2, color=aircraft2Color, label=label)
    plt.plot(time[0], vsp2[0], marker='x', color=aircraft2Color)
    plt.plot(time[nTest - 2], vsp2[nTest - 2], marker='o', color=aircraft2Color)

    # Add NMAC and RWC conflicts
    for i in range(nTest - 1):
      if self.aircraftNMAC[i]:
        plt.plot(time[i], vsp1[i], marker='o', markersize=markerSize, color='red')
        plt.plot(time[i], vsp2[i], marker='o', markersize=markerSize, color='red')
      else:
        if self.aircraft1TCAS[i] != None and self.aircraft1TCAS[i].conflict():
          plt.plot(time[i], vsp1[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft1RWC[i].WCV:
          plt.plot(time[i], vsp1[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft1RWC[i].isAlert():
          if self.aircraft1RWC[i].isWarningAlert():
            plt.plot(time[i], vsp1[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft1RWC[i].isCorrectiveAlert():
            plt.plot(time[i], vsp1[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(time[i], vsp1[i], marker='.', markersize=markerSize, color='green')

        if self.aircraft2TCAS[i] != None and self.aircraft2TCAS[i].conflict():
          plt.plot(time[i], vsp2[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft2RWC[i].WCV:
          plt.plot(time[i], vsp2[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft2RWC[i].isAlert():
          if self.aircraft2RWC[i].isWarningAlert():
            plt.plot(time[i], vsp2[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft2RWC[i].isCorrectiveAlert():
            plt.plot(time[i], vsp2[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft2RWC[i].isPreventiveAlert():
            plt.plot(time[i], vsp2[i], marker='.', markersize=markerSize, color='green')

    plt.legend(prop={'size': legendSize})

    plt.suptitle("Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(
      self.id).zfill(9) + " (x --> o) ")
    plt.xlabel('time (s)')
    plt.ylabel('speed (m/s)')
    plt.title("Vertical speed")
    fig = plt.gcf()
    filepath = folder + "\\Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(
      9) + "_vspeed.eu1" + ".png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()

    #
    # Main plot: Bearing
    label = "Ownship: " + cls.className(self.aircraft1Class) + " (h=" + str(
      int((self.aircraft1[0].altitude))) + " -- " + str(
      int((self.aircraft1[len(self.aircraft1) - 2].altitude))) + ")"
    plt.plot(time, hd1, color=aircraft1Color, label=label)
    plt.plot(time[0], hd1[0], marker='x', color=aircraft1Color)
    plt.plot(time[nTest - 2], hd1[nTest - 2], marker='o', color=aircraft1Color)

    label = "Intruder: " + cls.className(self.aircraft2Class) + " (h=" + str(
      int((self.aircraft2[0].altitude))) + " -- " + str(
      int((self.aircraft2[len(self.aircraft2) - 2].altitude))) + ")"
    plt.plot(time, hd2, color=aircraft2Color, label=label)
    plt.plot(time[0], hd2[0], marker='x', color=aircraft2Color)
    plt.plot(time[nTest - 2], hd2[nTest - 2], marker='o', color=aircraft2Color)

    # Add NMAC and RWC conflicts
    for i in range(nTest - 1):
      if self.aircraftNMAC[i]:
        plt.plot(time[i], hd1[i], marker='o', markersize=markerSize, color='red')
        plt.plot(time[i], hd2[i], marker='o', markersize=markerSize, color='red')
      else:
        if self.aircraft1TCAS[i] != None and self.aircraft1TCAS[i].conflict():
          plt.plot(time[i], hd1[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft1RWC[i].WCV:
          plt.plot(time[i], hd1[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft1RWC[i].isAlert():
          if self.aircraft1RWC[i].isWarningAlert():
            plt.plot(time[i], hd1[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft1RWC[i].isCorrectiveAlert():
            plt.plot(time[i], hd1[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(time[i], hd1[i], marker='.', markersize=markerSize, color='green')

        if self.aircraft2TCAS[i] != None and self.aircraft2TCAS[i].conflict():
          plt.plot(time[i], hd2[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft2RWC[i].WCV:
          plt.plot(time[i], hd2[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft2RWC[i].isAlert():
          if self.aircraft2RWC[i].isWarningAlert():
            plt.plot(time[i], hd2[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft2RWC[i].isCorrectiveAlert():
            plt.plot(time[i], hd2[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft2RWC[i].isPreventiveAlert():
            plt.plot(time[i], hd2[i], marker='.', markersize=markerSize, color='green')

    plt.legend(prop={'size': legendSize})

    plt.suptitle("Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(
      self.id).zfill(9) + " (x --> o) ")
    plt.xlabel('time (s)')
    # plt.ylabel('bearing (deg)')
    plt.title("Bearing")
    fig = plt.gcf()
    filepath = folder + "\\Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(
      9) + "_bearing.eu1" + ".png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)

    #
    # Main plot: Conflict plot
    label = "H distance/10 "
    plt.plot(time, hdistance, color='lightgreen', label=label)
    label = "V distance "
    plt.plot(time, vdistance, color='lightsteelblue', label=label)

    # Add NMAC and RWC conflicts
    for i in range(nTest - 1):
      if self.aircraftNMAC[i]:
        plt.plot(time[i], hdistance[i], marker='o', markersize=markerSize, color='red')
        plt.plot(time[i], vdistance[i], marker='o', markersize=markerSize, color='red')
      else:
        if self.aircraft1TCAS[i] != None and self.aircraft1TCAS[i].conflict():
          plt.plot(time[i], hdistance[i], marker='o', markersize=markerSize, color='violet')
          plt.plot(time[i], vdistance[i], marker='o', markersize=markerSize, color='violet')
        if self.aircraft1RWC[i].WCV:
          plt.plot(time[i], hdistance[i], marker='.', markersize=markerSize, color='red')
          plt.plot(time[i], vdistance[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft1RWC[i].isAlert():
          if self.aircraft1RWC[i].isWarningAlert():
            plt.plot(time[i], hdistance[i], marker='.', markersize=markerSize, color='orange')
            plt.plot(time[i], vdistance[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft1RWC[i].isCorrectiveAlert():
            plt.plot(time[i], hdistance[i], marker='.', markersize=markerSize, color='yellow')
            plt.plot(time[i], vdistance[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(time[i], hdistance[i], marker='.', markersize=markerSize, color='green')
            plt.plot(time[i], vdistance[i], marker='.', markersize=markerSize, color='green')

    plt.legend(prop={'size': 6})

    plt.suptitle("Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(
      self.id).zfill(9) + " (x --> o) ")
    plt.title("Encounter_Distribution data")
    fig = plt.gcf()
    filepath = folder + "\\Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(
      9) + "_encounter.eu1" + ".png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)

    #
    # Main plot: Azimuth and Elevation
    label = "Ownship: " + cls.className(self.aircraft1Class) + " (h=" + str(
      int((self.aircraft1[0].altitude))) + " -- " + str(
      int((self.aircraft1[len(self.aircraft1) - 2].altitude))) + ")"
    plt.plot(az1, ele1, color=aircraft1Color, label=label)
    plt.plot(az1[0], ele1[0], marker='x', color=aircraft1Color)
    plt.plot(az1[nTest - 2], ele1[nTest - 2], marker='o', color=aircraft1Color)

    label = "Intruder: " + cls.className(self.aircraft2Class) + " (h=" + str(
      int((self.aircraft2[0].altitude))) + " -- " + str(
      int((self.aircraft2[len(self.aircraft2) - 2].altitude))) + ")"
    plt.plot(az2, ele2, color=aircraft2Color, label=label)
    plt.plot(az2[0], ele2[0], marker='x', color=aircraft2Color)
    plt.plot(az2[nTest - 2], ele2[nTest - 2], marker='o', color=aircraft2Color)

    # Add NMAC and RWC conflicts
    for i in range(nTest - 1):
      if self.aircraftNMAC[i]:
        plt.plot(az1[i], hd1[i], marker='o', markersize=markerSize, color='red')
        plt.plot(az2[i], hd2[i], marker='o', markersize=markerSize, color='red')
      else:
        if self.aircraft1TCAS[i] != None and self.aircraft1TCAS[i].conflict():
          plt.plot(az1[i], ele1[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft1RWC[i].WCV:
          plt.plot(az1[i], ele1[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft1RWC[i].isAlert():
          if self.aircraft1RWC[i].isWarningAlert():
            plt.plot(az1[i], ele1[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft1RWC[i].isCorrectiveAlert():
            plt.plot(az1[i], ele1[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft1RWC[i].isPreventiveAlert():
            plt.plot(az1[i], ele1[i], marker='.', markersize=markerSize, color='green')

        if self.aircraft2TCAS[i] != None and self.aircraft2TCAS[i].conflict():
          plt.plot(az2[i], ele2[i], marker='o', markersize=markerSize, color='violet')
        elif self.aircraft2RWC[i].WCV:
          plt.plot(az2[i], ele2[i], marker='.', markersize=markerSize, color='red')
        elif self.aircraft2RWC[i].isAlert():
          if self.aircraft2RWC[i].isWarningAlert():
            plt.plot(az2[i], ele2[i], marker='.', markersize=markerSize, color='orange')
          elif self.aircraft2RWC[i].isCorrectiveAlert():
            plt.plot(az2[i], ele2[i], marker='.', markersize=markerSize, color='yellow')
          elif self.aircraft2RWC[i].isPreventiveAlert():
            plt.plot(az2[i], ele2[i], marker='.', markersize=markerSize, color='green')

    plt.legend(prop={'size': legendSize})

    plt.suptitle(
      "Encounter_Distribution " + "Layer: " + str(self.layer) + " Type: " + self.type + " Id: " + str(
        self.id).zfill(9) + " (x --> o) ")
    plt.xlabel('Azimuth (deg)')
    plt.ylabel('Elevation (deg)')
    plt.title("Azimuth / Elevation")
    fig = plt.gcf()
    filepath = folder + "\\Encs_" + "L" + str(self.layer) + "_" + self.type + "_" + str(self.id).zfill(
      9) + "_azimuth-elevation.eu1" + ".png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)





  def RoWDetermination(self, full_diagram_path,ep, arcf1_eam, arcf2_eam,vdistlim,AreaRoW):
    # Set time detection range

    tinicial = None
    x=0

    nTest = min(len(self.aircraft1), len(self.aircraft2))

    print("Start computing RoW_computation.py conflicts...")
    # Compute conflicts
    self.aircraft1ROW = [None] * nTest
    self.aircraft2ROW = [None] * nTest

    modulecritic=1000 #Only necessary for it to be higher than the module found on the condition in the next 15 lines

    for index in range(nTest):
      arcf1 = self.aircraft1[index]
      arcf2 = self.aircraft2[index]

      if ep.debug:
        print("Checking Aircraft1 - Aircraft2 for sequence Idx: ", index)

             #De moment mirem right of way en aquest precís moment

      vectorentreells = np.array([arcf2.x_loc - arcf1.x_loc,arcf2.y_loc - arcf1.y_loc])
      module = math.sqrt(vectorentreells[0] ** 2 + vectorentreells[1] ** 2)
      distanciavertical=arcf1.altitude-arcf2.altitude


      if (module < AreaRoW and module<modulecritic and -vdistlim<distanciavertical<vdistlim):  # Being the given value the one which defines from what area value we start taking RoW into account
        modulecritic=module #to check at what point the aircrafts are diverging
        dot = arcf1.x_v * vectorentreells[0] + arcf1.y_v *vectorentreells[1]
        det = arcf1.x_v * vectorentreells[1] - arcf1.y_v *vectorentreells[0]
        # Angle between LOS & velocity vector of ownship
        theta1 = (180 * math.atan2(det, dot) / math.pi)

        dot2 = arcf2.x_v * vectorentreells[0] + arcf2.y_v *vectorentreells[1]
        det2 = arcf2.x_v * vectorentreells[1] - arcf2.y_v *vectorentreells[0]
        # Angle between LOS & velocity vector of intruder
        theta2 = (180 * math.atan2(det2, dot2) / math.pi)

        if theta1 < 0:  # forcing the angle to be positive anticlockwise
          theta1 = 360 + theta1
        if theta2 < 0:
          theta2 = 360 + theta2

        # Defining phi=0 as the ownship has RoW and phi=1 if the intruder has RoW

        if 270 <= theta1 < 360:
          phi = 1
          print('Aircraft2 has Right of Way' +' at' + str(arcf1.violates) + 's')
          if (x==0):
            x = arcf1.violates

        elif 0 <= theta1 <= 90 and 0 <= theta2 <= 90:
          phi = 1
          print('Aircraft2 has Right of Way' +' at ' + str(arcf1.violates) + 's')
          if (x==0):
            x = arcf1.violates

        elif 0 <= theta1 <= 90 and 180 <= theta2 < 360:
          phi = 1
          print('Aircraft2 has Right of Way' +' at ' + str(arcf1.violates) + 's')
          if (x==0):
            x = arcf1.violates

        else:
          phi = 0
          print('Aircraft1 has Right of Way' + ' at ' + str(arcf1.violates) + 's')
          if (x==0):
            x = arcf1.violates

      else:
        print('No current Right of Way conflict')

      index=index+1
    print('First detection of RoW_computation.py violation will be at ' + str(x) + ' s and it will informing us of a violation of the ' + str(AreaRoW) + ' unit area at the time ' + str(m))
  # Now if phi=0 the ownship must maintain heading and speed
  # If phi=1 the intruder must maintain heading and speed so the evasive manouvre of the ownship can be evaluated

  def RoWFutur(self, full_diagram_path, ep, arcf1_eam, arcf2_eam,AreaRoW,FutureTime,vdistlim):
    # Set time detection range

    tinicial = None
    n=0
    modulecritic=1000


    nTest = min(len(self.aircraft1), len(self.aircraft2))

    print("Start computing RoW_computation.py conflicts...")
    # Compute conflicts
    self.aircraft1ROW = [None] * nTest
    self.aircraft2ROW = [None] * nTest

    tcpa=0

    for index in range(nTest):
      arcf1 = self.aircraft1[index]
      arcf2 = self.aircraft2[index]
      time = arcf1.violates
      i = index

      if ep.debug:
        print("Checking Aircraft1 - Aircraft2 for sequence Idx: ", index)


      while (i <= ((time + FutureTime) / 0.02)): #0.02 value varies depending on the amount of indices per second provided in the trajectories

        arcf1b = self.aircraft1[i]
        arcf2b = self.aircraft2[i]


        vectorentreells = np.array([arcf2b.x_loc - arcf1b.x_loc,arcf2b.y_loc - arcf1b.y_loc])
        module = math.sqrt(vectorentreells[0] ** 2 + vectorentreells[1] ** 2)
        distanciavertical = arcf1b.altitude - arcf2b.altitude

        if (module < AreaRoW and -vdistlim<distanciavertical<vdistlim):  # Being AreaRoW the area at which RoW must be evaluated

          dot = arcf1b.x_v * vectorentreells[0] + arcf1b.y_v * vectorentreells[1]
          det = arcf1b.x_v * vectorentreells[1] - arcf1b.y_v * vectorentreells[0]
          # Angle between LOS & velocity vector of ownship
          theta1 = (180 * math.atan2(det, dot) / math.pi)

          if(module<modulecritic):
            modulecritic=module
            tcpa=arcf1b.time

          dot2 = arcf2b.x_v * vectorentreells[0] + arcf2b.y_v * vectorentreells[1]
          det2 = arcf2b.x_v * vectorentreells[1] - arcf2b.y_v * vectorentreells[0]
          # Angle between LOS & velocity vector of intruder
          theta2 = (180 * math.atan2(det2, dot2) / math.pi)

          if theta1 < 0:  # forcing the angle to be positive anticlockwise
            theta1 = 360 + theta1
          if theta2 < 0:
            theta2 = 360 + theta2

          # Defining phi=0 as the ownship has RoW and phi=1 if the intruder has RoW

          if 270 <= theta1 < 360:
            phi = 1
            print('Aircraft2 will have Right of Way' + ' in ' + str(round(arcf1b.time - arcf1.violates, 3)) + 's' + ' from ' + str(arcf1.violates) + ' waypoint ')
            if n==0:
              n=arcf1.violates
              m = arcf1b.time



          elif 0 <= theta1 <= 90 and 0 <= theta2 <= 90:
            phi = 1
            print('Aircraft2 will have Right of Way' + ' in ' + str(round(arcf1b.time - arcf1.violates, 3)) + 's' + ' from ' + str(arcf1.violates) + ' waypoint ')
            if n==0:
              n=arcf1.violates
              m=arcf1b.time


          elif 0 <= theta1 <= 90 and 180 <= theta2 < 360:
            phi = 1
            print('Aircraft2 will have Right of Way' + ' in ' + str(round(arcf1b.time - arcf1.violates, 3)) + 's' + ' from ' + str(arcf1.violates) + ' waypoint ')
            if n==0:
              n=arcf1.violates
              m = arcf1b.time


          else:
            phi = 0
            print('Aircraft1 will have Right of Way' + ' in ' + str(round(arcf1b.time - arcf1.violates, 3)) + 's' + ' from ' + str(arcf1.violates) + ' waypoint ')
            if n==0:
              n=arcf1.violates
              m = arcf1b.time


        else:
          print('No future Right of Way conflict on index '+str(i))

        if (nTest> i + 1):
          i=i+1
        else:
          break

    print('Closest point of approach is at '+str(tcpa)+ ' s')
    print('First detection of RoW_computation.py violation will be at '+str(n)+' s and it will informing us of a violation of the '+str(AreaRoW)+' unit distance at the time '+str(m))
