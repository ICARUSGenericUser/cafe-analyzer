import math

def kt_to_ms(kt):
    ms = kt * 1852 / 3600
    return ms

def ms_to_kt(ms):
    kt = ms * 3600 / 1852
    return kt

def ms_to_fpm(ms):
    fpm = ms * 196.85
    return fpm

def fpm_to_ms(fpm):
    ms = fpm / 196.85
    return ms

def nm_to_m(nm):
    m = nm * 1852
    return m

def nm_to_ft(nm):
    ft = nm * 6076.12
    return ft

def m_to_nm(m):
    nm = m / 1852
    return nm

def deg_to_rad(deg):
    rad = deg * math.pi / 180
    return rad

def rad_to_deg(rad):
    deg = rad * 180 / math.pi
    return deg

def ft_to_m(ft):
    m = ft * 0.3048
    return m

def ft_to_nm(ft):
    nm = ft / 6076.12
    return nm

def m_to_ft(m):
    ft = m / 0.3048
    return ft

def min_to_sec(min):
    sec = min * 60.0
    return sec