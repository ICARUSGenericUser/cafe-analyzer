

class encounter_task:

  def __init__(self, task_enc_folder, diagram_folder, enc_layer, enc_type, enc_number, aircraft1Class, aircraft2Class, format):
    self.task_enc_folder = task_enc_folder
    self.subfolder = None
    self.diagram_folder = diagram_folder
    self.enc_layer = enc_layer
    self.enc_type = enc_type
    self.enc_number = enc_number
    self.aircraft1Class = aircraft1Class
    self.aircraft2Class = aircraft2Class
    self.enc_format = format
    self.aircraft1Callsign = None
    self.aircraft2Callsign = None
    self.aircraft1ModeA = None
    self.aircraft2ModeA = None
    self.aircraft1Prefix = None
    self.aircraft2Prefix = None

