from Encounter import units as units
import numpy as np
import math as math
from scipy.spatial import distance
from Util import angles as ang
from geographiclib import geodesic as geo


# Utilities

# Return the x component of velocity given the track and ground
# speed.  The track angle is assumed to use the radians from true
# North-clockwise convention.
# @param trk
# @param gs
# @return x component of velocity

def trkgs2vx(trk, gs):
  return gs * math.sin(trk)

# Return the y component of velocity given the track and ground
#	speed.  The track angle is assumed to use the radians from
#	true North-clockwise convention.
# @param trk
# @param gs
# @return y component of velocity

def trkgs2vy(trk, gs):
  return gs * math.cos(trk)

# Return the 2 - dimensional Euclidean vector for velocity given the track and ground
# speed.The track angle is assumed to use the radians from
# true North - clockwise convention.
# @ param trk
# @ param gs
# @ return 2 - D velocity

def trkgs2v(trk, gs):
  return np.array([trkgs2vx(trk, gs), trkgs2vy(trk, gs)])


class Position:
  def __init__(self, id, time, x_nm, y_nm, alt_ft):
    self.id = id
    self.time = time
    self.x_loc = units.nm_to_m(x_nm)
    self.y_loc = units.nm_to_m(y_nm)
    self.altitude = units.ft_to_m(alt_ft)
    self.x_v = 0
    self.y_v = 0
    self.h_speed = 0
    self.v_speed = 0
    self.bearing = 0
    self.bankAngle = 0
    self.turnRate = 0
    self.toa_offset = 0


  def duplicatePosition(self):
    newPosition = Position(self.id, self.time, units.m_to_nm(self.x_loc), units.m_to_nm(self.y_loc), units.m_to_ft(self.altitude))
    newPosition.x_v = self.x_v
    newPosition.y_v = self.y_v
    newPosition.h_speed = self.h_speed
    newPosition.v_speed = self.v_speed
    newPosition.bearing = self.bearing
    newPosition.bankAngle = self.bankAngle
    newPosition.turnRate = self.turnRate
    return newPosition


  def setTime(self, time):
    self.time = time


  def setLocation(self, lon, lat, alt_ft):
    self.x_loc = lon
    self.y_loc = lat
    self.altitude = alt_ft


  def setDynamics(self, h_speed, v_speed, heading):
    self.h_speed = h_speed
    self.v_speed = v_speed
    self.bearing = heading


  def setToaOffset(self, offset):
    self.toa_offset = offset


  def computeDynamics(self, next_position):
    pCurrent = np.array([self.x_loc, self.y_loc])
    pNext = np.array([next_position.x_loc, next_position.y_loc])
    v = pNext - pCurrent
    self.x_v = v[0]
    self.y_v = v[1]
    self.h_speed = np.abs(distance.euclidean(pCurrent, pNext))
    #self.h_speed = np.linalg.norm(pNext - pCurrent)
    self.v_speed = next_position.altitude - self.altitude
    self.bearing =  ang.normalize(90 - (180 / math.pi) * math.atan2(next_position.y_loc - self.y_loc, next_position.x_loc - self.x_loc))


  def computeTurnDynamics(self, next_position):
    self.turnRate = ang.normalizeToAcute(next_position.bearing - self.bearing)
    ba = units.rad_to_deg(math.atan(units.deg_to_rad(abs(self.turnRate)) * self.h_speed / 9.8))

    if self.turnRate >= 0:
      self.bankAngle = ba
    else:
      self.bankAngle = -ba



  def computeDynamicsFactor(self, next_position, factor):
    pCurrent = np.array([self.x_loc, self.y_loc])
    pNext = np.array([next_position.x_loc, next_position.y_loc])
    v = (pNext - pCurrent)/factor
    self.x_v = v[0]
    self.y_v = v[1]
    self.h_speed = np.abs(distance.euclidean(pCurrent, pNext))/factor
    #self.h_speed = np.linalg.norm(pNext - pCurrent)
    self.v_speed = (next_position.altitude - self.altitude)/factor
    self.bearing = ang.normalize(90 - (180 / math.pi) * math.atan2(next_position.y_loc - self.y_loc, next_position.x_loc - self.x_loc))


  def computeDynamicsDEG(self, next_position):
    tsep = next_position.violates - self.time
    pCurrent = np.array([self.x_loc, self.y_loc])
    pNext = np.array([next_position.x_loc, next_position.y_loc])
    v = (pNext - pCurrent)/tsep
    self.x_v = v[0]
    self.y_v = v[1]
    self.h_speed = np.abs(distance.euclidean(pCurrent, pNext))/tsep
    #self.h_speed = np.linalg.norm(pNext - pCurrent)
    self.v_speed = (next_position.altitude - self.altitude)/tsep
    self.bearing =  ang.normalize(90 - (180 / math.pi) * math.atan2(next_position.y_loc - self.y_loc, next_position.x_loc - self.x_loc))


  def validateDynamicsLatLon(self, next_position):
      distance = geo.Geodesic.WGS84.Inverse(self.y_loc, self.x_loc, next_position.y_loc, next_position.x_loc)
      v = distance['s12']/0.1
      print("Test: Horizontal speed: " + str(self.h_speed) + " Computed hSpeed: " + str(v))


  def validateDynamicsXY(self, next_position):
      pCurrent = np.array([self.x_loc, self.y_loc])
      pNext = np.array([next_position.x_loc, next_position.y_loc])
      v = np.abs(distance.euclidean(pCurrent, pNext))/0.1
      print("Test: Horizontal speed: " + str(self.h_speed) + " Computed hSpeed: " + str(v))


  def computeHeading(self):
    return ang.normalize(90 - (180 / math.pi) * math.atan2(self.y_v, self.x_v))


  def assignDynamics(self, Vx, Vy, Vv, Hdg):
    self.x_v = units.kt_to_ms(Vx)
    self.y_v = units.kt_to_ms(Vy)
    self.h_speed = math.sqrt(Vx**2 + Vy**2)
    self.v_speed = units.fpm_to_ms(Vv)
    self.bearing =  Hdg


  def assignDynamicsRaw(self, Vx, Vy, Vv):
    self.x_v = Vx
    self.y_v = Vy
    self.h_speed = math.sqrt(Vx ** 2 + Vy ** 2)
    self.v_speed = Vv
    self.bearing = self.computeHeading()


  def updateCoordinateSystem(self, projection):
    v2 = projection.project(self.x_loc, self.y_loc)
    self.x_loc = v2[0]
    self.y_loc = v2[1]


  def duplicateDynamics(self, prev_position):
    self.h_speed = prev_position.h_speed
    self.x_v = prev_position.x_v
    self.y_v = prev_position.y_v
    # self.h_speed = np.linalg.norm(pNext - pCurrent)
    self.v_speed = prev_position.v_speed
    self.bearing = prev_position.bearing

  def checkDynamics(self, X, Y, Z):
    radians = math.atan2((self.y_loc - Y)/0.02, (self.x_loc - X)/0.02)
    bearing = ang.normalize(90 - (180 / math.pi) * radians)
    print("Diferential Vx: ", "{:.5f}".format((self.x_loc - X)/0.02), " orig:", "{:.5f}".format(self.x_v))
    print("Diferential Vy: ", "{:.5f}".format((self.y_loc - Y) / 0.02), " orig:", "{:.5f}".format(self.y_v))
    print("Diferential Vz: ", "{:.5f}".format((self.altitude - Z) / 0.02), " orig:", "{:.5f}".format(self.altitude))
    print("Heading: ", "{:.5f}".format(bearing), " orig:", "{:.5f}".format(self.bearing))



  def getTrack(self):
    return units.deg_to_rad(self.bearing)

  # New velocity from existing velocity by adding the given track angle to this
  # vector's track angle.  Essentially, this rotates the vector, a positive
  # angle means a clockwise rotation.
  # @param trk track angle [rad]
  # @return new velocity


  def mkAddTrk(self, trk):
    s = math.sin(trk)
    c = math.cos(trk)
    return np.array([self.x_v * c + self.y_v * s, -self.x_v * s + self.y_v * c])

def getDynamics(position, prev_position):
  pCurrent = np.array([position.x_loc, position.y_loc])
  pPrev = np.array([prev_position.x_loc, prev_position.y_loc])
  v = (pCurrent - pPrev)/(position.time - prev_position.time)
  h_speed = np.abs(distance.euclidean(pCurrent, pPrev))/(position.time - prev_position.time)
  v_speed = (position.altitude - prev_position.altitude)/(position.time - prev_position.time)
  radians = math.atan2(position.y_loc - prev_position.y_loc, position.x_loc - prev_position.x_loc)
  bearing = ang.normalize(90 - (180 / math.pi) * radians)
  sinRadian = math.sin(radians)
  cosRadian = math.cos(radians)

  return np.array([v[0], v[1], h_speed, v_speed, bearing, sinRadian, cosRadian])


def rebuildAngle(sinAverage, cosAverage):
  radians = math.atan2(sinAverage, cosAverage)
  bearing = ang.normalize(90 - (180 / math.pi) * radians)
  return bearing


