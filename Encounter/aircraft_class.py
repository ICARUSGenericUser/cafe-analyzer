

def className(arg):
    switcher = {
        0 : "Unknown",
        1 : "Piston Engine",
        2 : "Turboprop, MTOM < 5,700 kg",
        3 : "Turboprop, MTOM 5,700 - 15,000 kg",
        4 : "Turboprop, MTOM > 15,000 kg",
        5 : "Military Fast Jet",
        6 : "Turbojet, MTOM 5,700 - 15,000 kg",
        7 : "Turbojet, MTOM 15,000 - 100,000 kg",
        8 : "Turbojet, MTOM > 100,000 kg",
        9 : "Turbojet, MTOM < 5,700 kg",
        10 : "RPAS Piston < 18000ft",
        11 : "RPAS Piston < 25000ft",
        12 : "RPAS Piston < 30000ft",
        13 : "RPAS Turboprop",
        14 : "RPAS Jet",
        15 : "HELI TH",
        16 : "HELI P / UAM",
        17 : "TILTROTOR",
        18 : "RPAS HELI TACTICAL",
        19 : "Unknown Turbojet",
        20 : "Unknown Piston",
        100 : "smallInspectDrone",
        101 : "smallDeliveryDrone",
        102 : "largeInspectDrone",
        103 : "largeDeliveryDrone"
    }
    return switcher.get(int(arg), "Unknown class")


def isACAS(arg):
    switcher = {
        0 : False,
        1 : False,
        2 : False,
        3 : True,
        4 : True,
        5 : False,
        6 : True,
        7 : True,
        8 : True,
        9 : False,
        10 : False,
        11 : False,
        12 : False,
        13 : False,
        14 : False,
        15 : True,
        16 : False,
        17 : False,
        18 : False,
        19 : True,
        20 : False,
        100: False,
        101: False,
        102: False,
        103: False
    }
    return switcher.get(int(arg), False)


def isRPAS(arg):
    switcher = {
        0 : False,
        1 : False,
        2 : False,
        3 : False,
        4 : False,
        5 : False,
        6 : False,
        7 : False,
        8 : False,
        9 : False,
        10 : True,
        11 : True,
        12 : True,
        13 : True,
        14 : True,
        15 : False,
        16 : False,
        17 : False,
        18 : False,
        19 : False,
        20 : False,
        100: True,
        101: True,
        102: True,
        103: True
    }
    return switcher.get(int(arg), False)


def isOwnshipRPAS(arg):
    switcher = {
        0 : False,
        1 : False,
        2 : False,
        3 : False,
        4 : False,
        5 : False,
        6 : False,
        7 : False,
        8 : False,
        9 : False,
        10 : True,
        11 : True,
        12 : True,
        13 : False,
        14 : False,
        15 : False,
        16 : False,
        17 : False,
        18 : False,
        19 : False,
        20 : False,
        100: True,
        101: True,
        102: True,
        103: True
    }
    return switcher.get(int(arg), False)






def nameToCode(name):
    if name == "smallInspectDrone":
        return 100
    elif name == "smallDeliveryDrone":
        return 101
    elif name == "largeInspectDrone":
        return 102
    elif name == "largeDeliveryDrone":
        return 103
