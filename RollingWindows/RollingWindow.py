import math as math
import numpy as np
from Encounter import position as pos
import copy
from Encounter import units as units
from Encounter.position import rebuildAngle


class RollingWindow:
    def __init__(self, capacity, windowWeights):
        self.capacity = capacity
        self.size = 0
        self.windowWeights = windowWeights
        self.windowWeight = np.sum(windowWeights)
        # Initializing queue with none
        self.queue = [None for i in range(capacity)]
        self.front = self.rear = -1
        self.nSampled = 0


    def enqueue(self, data):

        # Condition if queue is full
        if ((self.rear + 1) % self.capacity == self.front):
            self.dequeue()

        # Condition for empty queue
        if (self.front == -1):
            self.front = 0
            self.rear = 0
            self.queue[self.rear] = data
            self.size += 1
            self.nSampled += 1
        else:
            # Next position of rear
            self.rear = (self.rear + 1) % self.capacity
            self.queue[self.rear] = data
            self.size += 1
            self.nSampled += 1


    def dequeue(self):
        if (self.front == -1):  # Condition for empty queue
            print("Queue is Empty\n")

        # Condition for only one element
        elif (self.front == self.rear):
            temp = self.queue[self.front]
            self.front = -1
            self.rear = -1
            self.size -= 1
            return temp
        else:
            temp = self.queue[self.front]
            self.front = (self.front + 1) % self.capacity
            self.size -= 1
            return temp


    def peek(self):
        if (self.front == -1):  # Condition for empty queue
            return None
        else:
            self.nSampled = 0
            return self.queue[self.rear]


    def combine(self, new):
        if (self.front == -1):  # Condition for empty queue
            return None
        else:
            index = self.rear
            current = self.queue[index]
            vv = pos.getDynamics(new, current) * self.windowWeights[0]

            windowWeights = self.windowWeights[0]
            for i in range(self.size - 1):
                index = (index - 1) % self.capacity
                prev = self.queue[index]

                # Compute whatever necessary
                vv += pos.getDynamics(current, prev) * self.windowWeights[i+1]
                windowWeights += self.windowWeights[i+1]

                # Shift the current/prev pair
                current = prev

            # [v[0], v[1], h_speed, v_speed, bearing]
            vv = vv/windowWeights
            self.nSampled = 0

            combined = copy.deepcopy(new)
            combined.x_v = vv[0]
            combined.y_v = vv[1]
            combined.h_speed = vv[2]
            combined.v_speed = vv[3]
            combined.bearing = vv[4]
            return combined

    def combine2(self, new):
        if (self.front == -1):  # Condition for empty queue
            return None
        else:
            index = self.rear
            current = self.queue[index]
            v1 = pos.getDynamics(new, current)
            vv = v1 * self.windowWeights[0]

            windowWeights = self.windowWeights[0]
            for i in range(self.size - 1):
                index = (index - 1) % self.capacity
                prev = self.queue[index]

                # Compute whatever necessary
                v1 = pos.getDynamics(current, prev)
                vv += v1 * self.windowWeights[i+1]
                windowWeights += self.windowWeights[i+1]

                # Shift the current/prev pair
                current = prev

            # [v[0], v[1], h_speed, v_speed, bearing]
            vv = vv/windowWeights
            self.nSampled = 0

            combined = copy.deepcopy(new)
            combined.x_v = vv[0]
            combined.y_v = vv[1]
            combined.h_speed = vv[2]
            combined.v_speed = vv[3]
            combined.bearing = rebuildAngle(vv[5], vv[6])
            return combined

