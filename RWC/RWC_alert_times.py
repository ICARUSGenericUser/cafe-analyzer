import enum
# Using enum class create enumerations

class alertType(enum.Enum):
   noAlert = 1
   earlyAlert = 2
   lateAlert = 3
   averageAlert = 4
   lateIn = 5

   def __str__(self):
       if self == alertType.noAlert:
           return "noAlert"
       elif self == alertType.earlyAlert:
           return "earlyAlert"
       elif self == alertType.lateAlert:
           return "lateAlert"
       elif self == alertType.averageAlert:
           return "averageAlert"
       elif self == alertType.lateIn:
           return "lateIn"

   def isAlert(self):
     return self != alertType.noAlert



class RWC_alert_times:

    def __init__(self):
        self.average = 0
        self.early = 0
        self.late = 0
        self.pCPA = 0


    def setCautionDO365A(self):
        self.average = 55
        self.early = 75
        self.late = 20
        self.pCPA = 110


    def setWarningDO365A(self):
        self.average = 25
        self.early = 55
        self.late = 15
        self.pCPA = 90


    def setPreventiveDEG(self):
        self.average = 20
        self.early = 55 # Increased from 45
        self.late = 10
        self.pCPA = 60


    def setCautionDEG(self):
          self.average = 20
          self.early = 45
          self.late = 10
          self.pCPA = 60


    def setWarningDEG(self):
        self.average = 12
        self.early = 20
        self.late = 5
        self.pCPA = 50

    def setCautionStd(self):
        self.average = 55
        self.early = 75
        self.late = 20
        self.pCPA = 110

    def setCautionShort(self):
        self.average = 40
        self.early = 55
        self.late = 20
        self.pCPA = 110

    def setPreventiveStd(self):
        self.average = 55
        self.early = 75
        self.late = 20
        self.pCPA = 110

    def setPreventiveShort(self):
        self.average = 40
        self.early = 55
        self.late = 20
        self.pCPA = 110

    def setWarningStd(self):
        self.average = 15
        self.early = 25
        self.late = 15
        self.pCPA = 90

    def setWarningShort(self):
        self.average = 15
        self.early = 15
        self.late = 15
        self.pCPA = 90




    def checkConflict(self, interval, tcpa):

        if interval.isEmpty():
            return alertType.noAlert
        elif interval.low == 0:           # Seems we are already in HAZ
            return alertType.lateIn
        elif interval.low < self.late:    # Late entry to HAZ
            return alertType.lateAlert
        elif interval.low < self.average: # Minimum average entry to HAZ
            return alertType.averageAlert
        elif (interval.low < self.early) or (tcpa <= self.pCPA):   # Early entry to HAZ
            return alertType.earlyAlert
        else:
            return alertType.noAlert

