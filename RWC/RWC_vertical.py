import numpy as np
import math as math


# Vertical plane functions:

def sign(x):
  if (x >= 0):
    return 1
  return -1


# Computes times when sz, vz intersects rectangle of half height H
# eps = -1: Entry
# eps = 1: Exit
def Theta_H(sz, vz, eps, H):
  if (math.isclose(vz, 0)):
    return np.nan
  return (eps * sign(vz) * H - sz) / vz


def time_coalt(sz, vz):
  if (math.isclose(vz, 0)):
    return np.nan
  return -sz / vz


