import numpy as np
from Encounter import units as units


class WCV_Trace:

  def __init__(self):
    self.time = 0
    self.emptyVInterval = True
    self.VInterval = None
    self.emptyHInterval = True
    self.HInterval = None
    self.dueDMOD = False
    self.DMOD = 0   # Distance value in case conflict caused by DMOD
    self.dueTAUMOD = False
    self.TAUMOD = 0
    self.DCPA = 0   # DCPA in case of conflict
    self.TCPA = 0   # TCPA in case of conflict
    self.conflict = False # Up level function decides that exists conflict

