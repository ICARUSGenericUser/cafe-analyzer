import numpy as np
import math as math
from RWC import RWC_interval as itv, RWC_horizontal as horizontal


# Val per HZ i per NHZ
# HZ : TMOD + TCOA amb TCOA = 0
# NHZ : TMOD + VMOD

def horizontal_tvar(s, v, rwc_param):
# Time variable is Modified Tau

  taumod = -1;
  sdotv = s.dot(v);
  if (sdotv < 0):
    return (rwc_param.DMOD ** 2 - np.dot(s, s)) / sdotv
  return taumod


def horizontal_WCV_interval(T, s, v, rwc_param, trace=None):

  time_in = T;
  time_out = 0;

  sqs = np.dot(s, s)
  sdotv = np.dot(s, v)

  sqD = rwc_param.DMOD ** 2
  a = np.dot(v, v)
  b = 2 * sdotv + rwc_param.TAUMOD * np.dot(v, v)
  c = sqs + rwc_param.TAUMOD * sdotv - sqD

  if (math.isclose(a, 0) and sqs <= sqD):
    time_in = 0
    time_out = T
    if trace != None:
      trace.dueDMOD = True
      trace.DMOD = math.sqrt(sqs)
    return itv.Interval(time_in, time_out)

  if (sqs <= sqD):
    time_in = 0;
    time_out = min(T, horizontal.Theta_D(s, v, 1, rwc_param.DMOD));
    if trace != None:
      trace.dueDMOD = True
      trace.DMOD = math.sqrt(sqs)
    return itv.Interval(time_in, time_out)

  discr = b ** 2 - 4 * a * c
  if (sdotv >= 0 or discr < 0):
    return itv.Interval(time_in, time_out)

  ii = itv.Interval(time_in, time_out)

  t = (-b - math.sqrt(discr)) / (2 * a)
  if (horizontal.Delta(s, v, rwc_param.DMOD) >= 0 and t <= T):
    time_in = max(0, t)
    time_out = min(T, horizontal.Theta_D(s, v, 1, rwc_param.DMOD))
    ii = itv.Interval(time_in, time_out)

    if not ii.isEmpty() and trace != None:
      trace.dueTMOD = True
      trace.TMOD = t

  return ii
