import numpy as np
from Encounter import units as units

# Inputs:
#  turnRate     : Turn rate in degrees per second
#  bankAngle    : Bank angle in degrees
#  climbRate    : Climb rate in meter per second
#  descentRate  : Descent rate in meter per second


class ManoeuvreLimitations:

  def __init__(self, turnRate, bankAngle, climbRate, descentRate):
    self.turnRate = turnRate
    self.bankAngle = bankAngle
    self.climbRate = units.fpm_to_ms(climbRate)
    self.descentRate = units.fpm_to_ms(descentRate)

