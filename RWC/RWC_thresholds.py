import numpy as np
import math as math
from RWC import RWC_interval as interval, RWC_interval as itv, WCV_TAUMOD as wcv_horizontal, WCV_TCOA as wcv_vertical, \
  RWC_horizontal as horizontal


#import WCV_VMOD as wcv_vertical


# Position vector: s
# Velocity vector: v
# RWC parameters: rwc_param
def horizontal_WCV(s, v, rwc_param, trace=None):
  if (np.linalg.norm(s, 2) <= rwc_param.DMOD):
    trace.dueDMOD = True
    trace.DMOD = np.linalg.norm(s, 2)
    return True

  if (horizontal.dcpa(s, v) <= rwc_param.DMOD):
    tvar = wcv_horizontal.horizontal_tvar(s, v, rwc_param)

    if (0 <= tvar) and (tvar <= rwc_param.TAUMOD):
      trace.dueTMOD = True
      trace.TAUMOD = tvar
      return True

  return False


# Works for HZ Hazard and for NHZ Non-Hazard
# HZ : TMOD + TCOA with TCOA = 0
# NHZ : TMOD + VMOD
# Assumes 0 <= B < T
def WCV_interval(own_pos, int_pos, B, T, rwc_param, debug, trace=None):

  si2 = np.array([int_pos.x_loc, int_pos.y_loc])
  vi2 = np.array([int_pos.x_v, int_pos.y_v])
  so2 = np.array([own_pos.x_loc, own_pos.y_loc])
  vo2 = np.array([own_pos.x_v, own_pos.y_v])
  sz = int_pos.altitude - own_pos.altitude
  vz = int_pos.v_speed - own_pos.v_speed

  return WCV_interval_int(si2, vi2, so2, vo2, sz, vz, B, T, rwc_param, debug, trace=trace)


def WCV_interval_int(si2, vi2, so2, vo2, sz, vz, B, T, rwc_param, debug, trace=None):
  time_in = T
  time_out = B

  s2 = si2 - so2
  v2 = vi2 - vo2

  # Note that rwc_param.TCOA is set be 0 and rwc_param.ZTHR is rwc_param.H
  ii = wcv_vertical.vertical_WCV_interval(B, T, sz, vz, rwc_param)

  if (ii.isEmpty()):
    return ii
  elif debug:
    print("\tRWC Z will be violated in interval: ", ii.low, " -- ", ii.up, "Current VZ: ", vz)

  if trace != None:
    trace.emptyVInterval = False
    trace.VInterval = ii

  step = s2 + ii.low * v2

  if (math.isclose(ii.low, ii.up)):
    if debug:
      print("\tRWC H low and up are almost equal: ", ii.low, " -- ", ii.up)
    if (horizontal_WCV(step, v2, rwc_param, trace)):
      time_in = ii.low
      time_out = ii.up
      if debug:
        print("\tRWC H we keep the low and up: ", ii.low, " -- ", ii.up)

      if trace != None:
        trace.emptyHInterval = False
        trace.HInterval = ii

    return itv.Interval(time_in, time_out)

  ld = wcv_horizontal.horizontal_WCV_interval(ii.up-ii.low, step, v2, rwc_param, trace)
  time_in = ld.low + ii.low
  time_out = ld.up + ii.low

  ii = itv.Interval(time_in, time_out)

  if debug and not ii.isEmpty():
    print("\tRWC H low and up updated by horizontal: ", time_in, " -- ", time_out, " S2: ",  s2, " V2: ", v2)

  if not ii.isEmpty() and trace != None:
    trace.emptyHInterval = False
    trace.HInterval = ii

  return ii

