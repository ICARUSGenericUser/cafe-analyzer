from RWC import RWC_alert_times as at

# Outputs:
#  Rxy    : Horiztonal range
#  Dcpa   : Distance at closest point of approach
#  Taumod : TCAS II modified tau
#  Rz     : Relative altitude
#  Tcpa   : Time to CPA
#  Tcoa   : Time to co-altitude
#  WCVxy  : Horizontal well-clear violation
#  WCVz   : Vertical well-clear violation
#  WCV    : Well-clear violation


class RWC_output:

  def __init__(self):
    self.Rxy = 0
    self.Dcpa = 0
    self.Taumod = 0
    self.Rz = 0
    self.Tcpa = 0
    self.Tcoa = 0
    self.WCVxy = 0
    self.WCVz = 0
    self.WCV = 0

    self.interval = None
    self.preventive_ct = at.alertType.noAlert
    self.corrective_ct = at.alertType.noAlert
    self.warning_ct = at.alertType.noAlert

  def isAlert(self):
    return self.warning_ct.isAlert() or self.corrective_ct.isAlert() or self.preventive_ct.isAlert()

  def isPreventiveAlert(self):
    return self.preventive_ct.isAlert()

  def isCorrectiveAlert(self):
    return self.corrective_ct.isAlert()

  def isWarningAlert(self):
    return self.warning_ct.isAlert()
