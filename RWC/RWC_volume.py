import numpy as np
import math as math
from RWC import RWC_output as rout


# Inputs:
#  DMOD         : Distance Modification of Modified Tau
#  HMD          : Horizontal miss distance threshold
#  TAUMOD       : Modified tau threshold
#  H            : Vertical separation threshold

# Outputs in RWC_out:
#  Rxy    : Horiztonal range
#  Dcpa   : Distance at closest point of approach
#  Taumod : TCAS II modified tau
#  Rz     : Relative altitude
#  Tcpa   : Time to CPA
#  Tcoa   : Time to co-altitude
#  WCVxy  : Horizontal well-clear violation
#  WCVz   : Vertical well-clear violation
#  WCV    : Well-clear violation


def check_wcv_zone(own_pos, int_pos, rwc_param):
  RWC_out = rout.RWC_output()

  if np.size(rwc_param.WCV_TAUMOD):
    rwc_param.setTaumod(int_pos.altitude)

  so = np.array([own_pos.x_loc, own_pos.y_loc])
  si = np.array([int_pos.x_loc, int_pos.y_loc])
  vo = np.array([own_pos.x_v, own_pos.y_v])
  vi = np.array([int_pos.x_v, int_pos.y_v])

  s = si - so
  v = vi - vo
  r = np.linalg.norm(s, 2)
  rdot = np.dot(s, v) / r
  rdot_factor = 0.5 * math.sqrt(((rdot * rwc_param.TAUMOD) ** 2 + 4 * rwc_param.DMOD ** 2) - rdot * rwc_param.TAUMOD)
  cond1 = r <= max(rwc_param.DMOD, rdot_factor)

  if np.sum(np.power(v, 2)) != 0:
    tcpa = -np.dot(s, v) / np.sum(np.power(v, 2))
    ftcpa = max(0, tcpa)
    hmdp = (s[0] + v[0] * tcpa) ** 2 + (s[1] + v[1] * tcpa) ** 2
    hmdp = math.sqrt(hmdp)
    cond2 = hmdp <= rwc_param.HMD
  else:
    #print("Speeds seem to be 0")
    cond2 = False

  # Horizontal dimension outputs
  if np.dot(v, v) != 0:
    RWC_out.Tcpa = -np.dot(s, v) / np.dot(v, v)       # Tcpa: Time to closest point of approach
    s_tcpa = s + RWC_out.Tcpa * v                     # Relative position at closest point of approach
    RWC_out.Dcpa = math.sqrt(np.dot(s_tcpa, s_tcpa))  #  Dcpa   : Distance at closest point of approach
  else:
    RWC_out.Tcpa = -1
    RWC_out.Dcpa = -1

  RWC_out.Rxy = math.sqrt(np.dot(s, s))             #  Rxy    : Horizontal range

  cxy = np.dot(s, v)     # Cxy < 0 iff aircraft are converging in horizontal plane
  if cxy < 0:
    RWC_out.Taumod = (rwc_param.DMOD ** 2 - np.dot(s, s)) / np.dot(s, v)    # Modified tau
  else:
    RWC_out.Taumod = -1

  # Vertical dimension outputs
  s_z = int_pos.altitude - own_pos.altitude
  v_z = int_pos.v_speed - own_pos.v_speed
  RWC_out.Rz = abs(s_z)

  cz = s_z * v_z      # Cz < 0  iff aircraft are converging in vertical plane
  if cz < 0:
    RWC_out.Tcoa = -s_z / v_z    # Time to co - altitude
  else:
    RWC_out.Tcoa = -1

  RWC_out.WCVxy = cond1 and cond2               #  WCVxy  : Horizontal well-clear violation
  RWC_out.WCVz = RWC_out.Rz <= rwc_param.H      #  WCVz   : Vertical well-clear violation
  RWC_out.WCV = RWC_out.WCVxy and RWC_out.WCVz  #  WCV    : Well-clear violation

  return RWC_out



def check_hazard_zone_3D(own_pos, own_pos_vv, int_pos, int_pos_vv, rwc_param):
  RWC_out = rout.RWC_output()

  so = np.array([own_pos[0], own_pos[1]])
  si = np.array([int_pos[0], int_pos[1]])
  vo = np.array([own_pos_vv[0], own_pos_vv[1]])
  vi = np.array([int_pos_vv[0], int_pos_vv[1]])

  s = si - so
  v = vi - vo
  r = np.linalg.norm(s, 2)
  rdot = np.dot(s, v) / r
  rdot_factor = 0.5 * math.sqrt((rdot * rwc_param.TAUMOD) ** 2 + 4 * rwc_param.DMOD ** 2) - rdot * rwc_param.TAUMOD
  cond1 = r <= max(rwc_param.DMOD, rdot_factor)

  if np.sum(np.power(v, 2)) != 0:
    tcpa = -np.dot(s, v) / np.sum(np.power(v, 2))
    ftcpa = max(0, tcpa)
    hmdp = (s[0] + v[0] * tcpa) ** 2 + (s[1] + v[1] * tcpa) ** 2
    hmdp = math.sqrt(hmdp)
    cond2 = hmdp <= rwc_param.HMD
  else:
    #print("Speeds seem to be 0")
    cond2 = False

  # Horizontal dimension outputs
  if np.dot(v, v) != 0:
    RWC_out.Tcpa = -np.dot(s, v) / np.dot(v, v)       # Tcpa: Time to closest point of approach
    s_tcpa = s + RWC_out.Tcpa * v                     # Relative position at closest point of approach
    RWC_out.Dcpa = math.sqrt(np.dot(s_tcpa, s_tcpa))  #  Dcpa   : Distance at closest point of approach
  else:
    RWC_out.Tcpa = -1
    RWC_out.Dcpa = -1

  RWC_out.Rxy = math.sqrt(np.dot(s, s))             #  Rxy    : Horizontal range

  cxy = np.dot(s, v)     # Cxy < 0 iff aircraft are converging in horizontal plane
  if cxy < 0:
    RWC_out.Taumod = (rwc_param.DMOD ** 2 - np.dot(s, s)) / np.dot(s, v)    # Modified tau
  else:
    RWC_out.Taumod = -1

  # Vertical dimension outputs
  s_z = int_pos[2] - own_pos[2]
  v_z = int_pos_vv[2] - own_pos_vv[2]
  RWC_out.Rz = abs(s_z)

  cz = s_z * v_z      # Cz < 0  iff aircraft are converging in vertical plane
  if cz < 0:
    RWC_out.Tcoa = -s_z / v_z    # Time to co - altitude
  else:
    RWC_out.Tcoa = -1

  RWC_out.WCVxy = cond1 and cond2               #  WCVxy  : Horizontal well-clear violation
  RWC_out.WCVz = RWC_out.Rz <= rwc_param.H      #  WCVz   : Vertical well-clear violation
  RWC_out.WCV = RWC_out.WCVxy and RWC_out.WCVz  #  WCV    : Well-clear violation

  return RWC_out



def check_nmac_3D(own_pos, int_pos, nmac_param):

  so = np.array([own_pos[0], own_pos[1]])
  si = np.array([int_pos[0], int_pos[1]])

  s = si - so
  r = np.linalg.norm(s, 2)
  cond1 = r <= nmac_param.HNMAC

  dh = abs(int_pos[2] - own_pos[2])
  cond2 = dh <= nmac_param.VNMAC

  #print("Distance H: ", str(r), "  V: ", dh)

  return cond1 and cond2


def check_nmac(own_pos, int_pos, nmac_param):

  so = np.array([own_pos.x_loc, own_pos.y_loc])
  si = np.array([int_pos.x_loc, int_pos.y_loc])

  s = si - so
  r = np.linalg.norm(s, 2)
  cond1 = r <= nmac_param.HNMAC

  dh = abs(int_pos.altitude - own_pos.altitude)
  cond2 = dh <= nmac_param.VNMAC

  #print("Distance H: ", str(r), "  V: ", dh)

  return cond1 and cond2


def check_vmdhmd(vmd, hmd, nmac_param):

  cond1 = hmd <= nmac_param.HNMAC
  cond2 = vmd <= nmac_param.VNMAC

  return cond1 and cond2


def get_slat_distance(so, si):
  s = si - so
  r = np.linalg.norm(s, 2)
  return r