import math as math
from RWC import RWC_interval as itv, RWC_vertical as vertical


# Vertical Well Clear Volume concept

# Vertical Not Well Clear Violation
# ZTHR and TCOA are altitude and time thresholds
def vertical_WCV(ZTHR, TCOA, sz, vz):
  return math.abs(sz) <= ZTHR or (not math.isclose(vz, 0) and sz * vz <= 0 and vertical.time_coalt(sz, vz) <= TCOA)


# Implementation of Hazard Zone
# ZTHR and TCOA are altitude and time thresholds
# TCOA "equivalent to TauMod in vertical" and should be 0 in the DO365A
# ZTHR equivalent to H*
def vertical_WCV_interval(B, T, sz, vz, rwc_param):
  time_in = B
  time_out = T

  if (math.isclose(vz, 0) and abs(sz) <= rwc_param.ZTHR):
    return itv.Interval(time_in, time_out)

  if (math.isclose(vz, 0)):
    time_in = T
    time_out = B
    return itv.Interval(time_in, time_out)

  act_H = max(rwc_param.ZTHR, abs(vz) * rwc_param.TCOA)
  tentry = vertical.Theta_H(sz, vz, -1, act_H)
  texit = vertical.Theta_H(sz, vz, 1, rwc_param.ZTHR)

  if (T < tentry or texit < B):
    time_in = T
    time_out = B
    return itv.Interval(time_in, time_out)

  time_in = max(B, tentry)
  time_out = min(T, texit)
  return itv.Interval(time_in, time_out)
