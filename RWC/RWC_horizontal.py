import numpy as np
import math as math


# Horizontal plane functions:

# Time of closest point of approach.
# Return the time of horizontal closest point of approach
# between this point and the line defined by the vector v.
def tcpa(s, v):
  if not math.isclose(np.dot(v, v), 0):
    return -np.dot(s, v) / np.dot(v, v)
  return 0


# Distance closest point of approach.
# Return the horizontal distance at closest point of approach
# between the point and the line defined by the vector v.
def dcpa(s, v):
  s_tcpa = s + tcpa(s, v) * v
  return math.sqrt(np.dot(s_tcpa, s_tcpa))


def sqrt_safe(x):
  return math.sqrt(max(x, 0))


# root2b(a,b,c,eps) = root(a,2*b,c,eps) , eps = -1 or +1
#
# @param a a coefficient of quadratic
# @param b b coefficient of quadratic
# @param c c coefficient of quadratic
# @param eps
# @return root of quadratic

def root2b(a, b, c,eps):
  if math.isclose(a, 0) and math.isclose(b, 0):
    return np.nan
  elif math.isclose(a, 0):
    return -c/(2*b)
  else:
    sqb = b**2
    ac  = a*c
    if (math.isclose(sqb, ac) or sqb > ac):
      return (-b + eps * sqrt_safe(sqb-ac))/a
    return np.nan


# Determinant.
# Return the determinant of <code>this</code> vector and (<code>x</code>,<code>y</code>).
def det(v1, v2):
    return v1[0]*v2[1] - v1[1]*v2[0]


# Discriminant
def discr(a, b, c):
  return b**2 - 4 * a * c


# Root of quadratic equation, eps = -1 or +1
def root(a, b, c, eps):
  if math.isclose(a, 0) and math.isclose(b, 0):
      return np.nan
  elif math.isclose(a, 0):
      return -c / b
  else:
    sqb = b**2
    ac  = 4*a*c
    if math.isclose(sqb,ac) or sqb > ac:
        return (-b + eps * sqrt_safe(sqb-ac)) / (2 * a)
    return np.nan


# Returns the time t such that the parametric line s</code>+<i>t</i><code>v</code>
# intersects the circle of radius D. If eps == -1 the returned time is
# the entry time; if eps == 1 the returned time is the exit time.
def Theta_D(s, v, eps, D):
  a = np.dot(v, v)
  b = np.dot(s, v)
  c = np.dot(s, s) - D**2
  return root2b(a,b,c,eps)


# return the discriminant of the intersection between the parametric line
# <code>s</code>+<i>t</i><code>v</code> and the circle of radius <code>D</code>.
# If the discriminant is less than <code>0</code> the intersection doesn't exist;
# if it is <code>0</code> the line is tangent to the circle; otherwise, the line
# intersects the circle in two different points.
def Delta(s, v, D):
    return D**2 * np.dot(v, v) - det(s, v)**2
