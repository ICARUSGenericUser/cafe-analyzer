from Encounter import units as units


# Inputs:
#  DMOD         : Distance Modification of Modified Tau
#  HMD          : Horizontal miss distance threshold
#  TAUMOD       : Modified tau threshold
#  H            : Vertical separation threshold


class NMAC_parameters:

  def __init__(self, HNMAC, VNMAC):
    self.HNMAC = units.ft_to_m(HNMAC)
    self.VNMAC = units.ft_to_m(VNMAC)


  # Cessna horizontal size: 11 m - 500ft = 152m  Factor of 15
  #         vertical size: 2,7m - 100 ft = 30,5m  Factor of 11
  def setStandard(self):
    self.HNMAC = units.ft_to_m(500)
    self.VNMAC = units.ft_to_m(100)


  def setDEG(self):
    self.HNMAC = 15
    self.VNMAC = 5

