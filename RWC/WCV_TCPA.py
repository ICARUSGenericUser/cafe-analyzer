import numpy as np
import math as math
from RWC import RWC_interval as interval, RWC_horizontal as horizontal


# NO USAR

def horizontal_tvar(s, v, rwc_param):
# Time variable is Modified Tau

  return horizontal.tcpa(s, v)


def horizontal_WCV_interval(T, s, v, rwc_param):

  time_in = T
  time_out = 0

  sqs = np.dot(s, s)

  sqv = v.sqv()
  sdotv = np.dot(s, v)

  sqD = rwc_param.DTHR ** 2

  if (math.isclose(sqv, 0) and sqs <= sqD):
    time_in = 0
    time_out = T
    return interval(time_in, time_out)

  if (math.isclose(sqv, 0)):
    return interval(time_in, time_out)

  if (sqs <= sqD):
    time_in = 0
    time_out = min(T, horizontal.Theta_D(s, v, 1, rwc_param.DTHR))
    return interval(time_in, time_out)

  if (sdotv > 0):
    return interval(time_in, time_out)

  tcpa = horizontal.tcpa(s, v)
  if (v.ScalAdd(tcpa, s).norm() > rwc_param.DTHR):
    return interval(time_in, time_out)

  Delta = horizontal.Delta(s, v, rwc_param.DTHR)
  if (Delta < 0 and tcpa - rwc_param.TTHR > T):
    return interval(time_in, time_out)

  if (Delta < 0):
    time_in = max(0, tcpa - rwc_param.TTHR)
    time_out = min(T, tcpa)
    return interval(time_in, time_out)

  tmin = min(horizontal.Theta_D(s, v, -1, rwc_param.DTHR), tcpa - rwc_param.TTHR)
  if (tmin > T):
    return interval(time_in, time_out)

  time_in = max(0, tmin)
  time_out = min(T, horizontal.Theta_D(s, v, 1, rwc_param.DTHR))
  return interval(time_in, time_out)

