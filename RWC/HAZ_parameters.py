import numpy as np
from Encounter import units as units


# Inputs:
#  DMOD         : Distance Modification of Modified Tau
#  HMD          : Horizontal miss distance threshold
#  TAUMOD       : Modified tau threshold
#  H            : Vertical separation threshold
#  H            : Vertical separation threshold


class HAZ_parameters:

  def __init__(self, DMOD, HMD, TAUMOD, H):
    self.DMOD = units.ft_to_m(DMOD)
    self.HMD = units.ft_to_m(HMD)
    self.TAUMOD = TAUMOD
    self.H = units.ft_to_m(H)
    self.ZTHR = self.H
    self.TCOA = 0
    self.WCV_TAUMOD = np.array([])
    self.WCV_ALTRANGE = np.array([])


  def setTaumod(self, altitude):
    for i in range(0, len(self.WCV_TAUMOD)):
      if altitude <= self.WCV_ALTRANGE[i]:
        self.TAUMOD = self.WCV_TAUMOD[i]
        return
    self.TAUMOD = 0


  def setAsNMAC(self):
    self.DMOD = units.ft_to_m(500)
    self.HMD = units.ft_to_m(500)
    self.TAUMOD = 0
    self.H = units.ft_to_m(100)
    self.ZTHR = self.H
    self.TCOA = 0


  def setPreventiveDO365A(self):
    self.DMOD = units.ft_to_m(4000)
    self.HMD = units.ft_to_m(4000)
    self.TAUMOD = 35
    self.H = units.ft_to_m(700)
    self.ZTHR = self.H
    self.TCOA = 0


  def setCorrectiveDO365A(self):
    self.DMOD = units.ft_to_m(4000)
    self.HMD = units.ft_to_m(4000)
    self.TAUMOD = 35
    self.H = units.ft_to_m(450)
    self.ZTHR = self.H
    self.TCOA = 0



  def setPreventive_05NM(self):
    self.DMOD = units.nm_to_m(0.5)
    self.HMD = units.nm_to_m(0.5)
    self.TAUMOD = 0
    self.H = units.ft_to_m(700)
    self.ZTHR = self.H
    self.TCOA = 0


  def setWCV_05NM(self):
    self.DMOD = units.nm_to_m(0.5)
    self.HMD = units.nm_to_m(0.5)
    self.TAUMOD = 0
    self.H = units.ft_to_m(450)
    self.ZTHR = self.H
    self.TCOA = 0


  def setPreventive_22KFT(self):
    self.DMOD = units.ft_to_m(2200)
    self.HMD = units.ft_to_m(2200)
    self.TAUMOD = 0
    self.H = units.ft_to_m(700)
    self.ZTHR = self.H
    self.TCOA = 0


  def setWCV_22KFT(self):
    self.DMOD = units.ft_to_m(2200)
    self.HMD = units.ft_to_m(2200)
    self.TAUMOD = 0
    self.H = units.ft_to_m(450)
    self.ZTHR = self.H
    self.TCOA = 0


  def setPreventive_URCV(self):
    self.DMOD = units.ft_to_m(2200)
    self.HMD = units.ft_to_m(2200)
    self.TAUMOD = 25
    self.H = units.ft_to_m(700)
    self.ZTHR = self.H
    self.TCOA = 0


  def setPreventive_URCV_Short(self):
      self.DMOD = units.nm_to_m(0.5)
      self.HMD = units.nm_to_m(0.5)
      self.TAUMOD = 15
      self.H = units.ft_to_m(700)
      self.ZTHR = self.H
      self.TCOA = 0


  def setWCV_URCV(self):
    self.DMOD = units.ft_to_m(2200)
    self.HMD = units.ft_to_m(2200)
    self.TAUMOD = 25
    self.H = units.ft_to_m(450)
    self.ZTHR = self.H
    self.TCOA = 0


  def setWCV_URCV_Short(self):
      self.DMOD = units.nm_to_m(0.5)
      self.HMD = units.nm_to_m(0.5)
      self.TAUMOD = 15
      self.H = units.ft_to_m(450)
      self.ZTHR = self.H
      self.TCOA = 0


  def setPreventive_SESAR(self):
    self.DMOD = units.nm_to_m(0.5)
    self.HMD = units.nm_to_m(0.5)
    self.TAUMOD = 35
    self.H = units.ft_to_m(700)
    self.ZTHR = self.H
    self.TCOA = 0
    # TA TAU Threshold in seconds per altitude
    self.WCV_TAUMOD = np.array([15, 20, 25, 30, 35])
    self.WCV_ALTRANGE = np.array([units.ft_to_m(3000), units.ft_to_m(5000), units.ft_to_m(10000), units.ft_to_m(30000), units.ft_to_m(45000)])


  def setWCV_SESAR(self):
    self.DMOD = units.nm_to_m(0.5)
    self.HMD = units.nm_to_m(0.5)
    self.TAUMOD = 35
    self.H = units.ft_to_m(450)
    self.ZTHR = self.H
    self.TCOA = 0
    # TA TAU Threshold in seconds per altitude
    self.WCV_TAUMOD = np.array([15, 20, 25, 30, 35])
    self.WCV_ALTRANGE = np.array([units.ft_to_m(3000), units.ft_to_m(5000), units.ft_to_m(10000), units.ft_to_m(30000), units.ft_to_m(45000)])


  def setPreventive_DO365(self):
    self.DMOD = units.ft_to_m(4000)
    self.HMD = units.ft_to_m(4000)
    self.TAUMOD = 35
    self.H = units.ft_to_m(700)
    self.ZTHR = self.H
    self.TCOA = 0


  def setWCV_DO365(self):
    self.DMOD = units.ft_to_m(4000)
    self.HMD = units.ft_to_m(4000)
    self.TAUMOD = 35
    self.H = units.ft_to_m(450)
    self.ZTHR = self.H
    self.TCOA = 0


  def setWarningDO365A(self):
    self.DMOD = units.ft_to_m(4000)
    self.HMD = units.ft_to_m(4000)
    self.TAUMOD = 35
    self.H = units.ft_to_m(450)
    self.ZTHR = self.H
    self.TCOA = 0


  def setWarningURCLEARED(self):
    self.DMOD = units.ft_to_m(3000)
    self.HMD = units.ft_to_m(3000)
    self.TAUMOD = 20
    self.H = units.ft_to_m(450)
    self.ZTHR = self.H
    self.TCOA = 0

    # HNMAC = 15 m
    # VNMAC = 5 m

  def setWarningDEG(self):
    self.DMOD = 60
    self.HMD = 40
    self.TAUMOD = 35
    self.H = 40
    self.ZTHR = self.H
    self.TCOA = 0


  def setCorrectiveDEG(self):
    self.DMOD = 60
    self.HMD = 40
    self.TAUMOD = 35
    self.H = 40
    self.ZTHR = self.H
    self.TCOA = 0


  def setPreventiveDEG(self):
    self.DMOD = 60
    self.HMD = 60
    self.TAUMOD = 35
    self.H = 60
    self.ZTHR = self.H
    self.TCOA = 0