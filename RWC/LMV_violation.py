import numpy as np
import math as math
import matplotlib
import matplotlib.pyplot as plt
from Util import angles as ang
from Encounter import units as units
from RWC import RWC_volume as rwct, RWC_thresholds as rwcthr, RWC_interval as itv, HAZ_parameters as rwc


class LMZ_Prediction:

  def __init__(self):
    self.violates = False
    self.lookaheadTime = 0
    self.minDistance = 0


def mysign(x):
    if x >= 0:
        return 1
    else:
        return -1

# NMAC Last Manoeuvre Violation
# Conceptual volume around NMAC that identifies the last point at which NMAC violation can be avoided
# given some manoeuvre limitations.

# Given the postion of ownship and intruder
# and a disc volume defined by HMD/VMD around the intruder position
# Determine if ownship will violate that volume even if it maneuvers by
# turning, climbing or descending at the max rates specified by the parameters
# If ownship already in the disc, return 0.
# Return the time at which this violation will happen in the future
# If no violation will occur, return -1

def violateDiscHorizontal(own_pos, int_pos, nmac_param, upperBound, maxBankAngle, bankRate, climbRate, descentRate):


    # Initial positions
    own_pos_init = np.array([own_pos.x_loc, own_pos.y_loc, own_pos.altitude])
    int_pos_init = np.array([int_pos.x_loc, int_pos.y_loc, int_pos.altitude])
    # Initial headings
    own_heading = own_pos.bearing * np.pi / 180.0
    int_heading = int_pos.bearing * np.pi / 180.0
    # Initial bank angles
    own_max_bank = units.deg_to_rad(maxBankAngle)
    int_max_bank = units.deg_to_rad(maxBankAngle)
    # Initial speeds
    own_speed = own_pos.h_speed
    int_speed = int_pos.h_speed
    # Initial turn rates
    own_bank_rate = bankRate * np.pi / 180
    int_bank_rate = bankRate * np.pi / 180


    # Maximum simulation time [s]
    T_MAX= upperBound

    # Is the Ownship intended to turn?
    own_turns = True
    # Is the Intruder intended to turn?
    int_turns = False

    idxOwnRight = 0
    idxOwnLeft = 0

    # Calculate the minimum turn radius of the uav
    own_min_turn_rad = own_speed * own_speed / (9.81 * np.tan(own_max_bank))

    # Calculate the maximum angular speed of the uav
    own_omega_max = own_speed / own_min_turn_rad

    # Calculate the minimum turn radius of the intruder
    int_turn_rad = int_speed * int_speed / (9.81 * np.tan(int_max_bank))

    # Calculate the maximum angular speed of the intruder
    int_omega_max = int_speed / int_turn_rad

    # Time discriminant
    dt = 0.1

    # Allocate storing matrices
    cols_count = 3
    rows_count = int(np.floor(T_MAX / dt))
    own_bank_angle_right = np.zeros((rows_count, 1))
    own_bank_angle_left = np.zeros((rows_count, 1))
    int_bank_angle = np.zeros((rows_count, 1))
    own_pos_left = np.zeros((rows_count, cols_count))
    own_pos_right = np.zeros((rows_count, cols_count))
    int_pos_left = np.zeros((rows_count, cols_count))
    int_pos_right = np.zeros((rows_count, cols_count))
    tVector = np.zeros(rows_count)

    # initialize rotation matrices
    R_int_heading = np.pi / 2 - int_heading
    R_own_heading = np.pi / 2 - own_heading
    R_int = np.array([[np.cos(R_int_heading), -np.sin(R_int_heading), 0], [np.sin(R_int_heading), np.cos(R_int_heading), 0], [0, 0, 1]])
    R_own = np.array([[np.cos(R_own_heading), -np.sin(R_own_heading), 0], [np.sin(R_own_heading), np.cos(R_own_heading), 0], [0, 0, 1]])


    # Loop to calculate aircraft positions
    t = 0
    #own_omega = units.deg_to_rad(own_pos.turnRate)
    int_omega = 0
    rightDone = False
    leftDone= False
    while t * dt < T_MAX:
        # If ownship is intended to turn
        if own_turns:
            if t == 0:
                own_heading_left = units.deg_to_rad(own_pos.bearing)
                own_heading_right = units.deg_to_rad(own_pos.bearing)
                own_omega_right = units.deg_to_rad(own_pos.turnRate)
                own_omega_left = units.deg_to_rad(own_pos.turnRate)
                own_bank_angle_right[t] = units.deg_to_rad(own_pos.bankAngle)
                own_bank_angle_left[t] = units.deg_to_rad(own_pos.bankAngle)
                own_turn_rad_right = own_speed * own_speed / (9.81 * np.tan(abs(own_bank_angle_right[t])))
                own_turn_rad_left = own_speed * own_speed / (9.81 * np.tan(abs(own_bank_angle_left[t])))

                own_pos_left[t, 0] = 0
                own_pos_left[t, 1] = 0
                own_pos_left[t, 2] = 0
                own_pos_right[t, 0] = 0
                own_pos_right[t, 1] = 0
                own_pos_right[t, 2] = 0
                tVector[t] = 0

            if t > 0:
                if not rightDone and (abs(own_omega_right) < own_omega_max and own_turn_rad_right > own_min_turn_rad):
                    own_bank_angle_right[t] = own_bank_angle_right[t - 1] + own_bank_rate * dt
                    own_turn_rad_right = own_speed * own_speed / (9.81 * np.tan(abs(own_bank_angle_right[t])))
                    own_omega_right = mysign(own_bank_angle_right[t]) * own_speed / own_turn_rad_right
                    if abs(own_omega_right) > own_omega_max or own_turn_rad_right < own_min_turn_rad:
                        #own_omega_right = own_omega_max
                        #own_turn_rad_right = own_min_turn_rad
                        idxOwnRight = t
                        rightDone = True
                #else:
                    #rightDone = True
                    #own_omega_right = own_omega_max
                    #own_turn_rad_right = own_min_turn_rad

                if abs(own_omega_left) < own_omega_max and own_turn_rad_left > own_min_turn_rad:
                    own_bank_angle_left[t] = own_bank_angle_left[t - 1] - own_bank_rate * dt
                    own_turn_rad_left = own_speed * own_speed / (9.81 * np.tan(abs(own_bank_angle_left[t])))
                    own_omega_left = mysign(own_bank_angle_left[t]) * own_speed / own_turn_rad_left
                    if abs(own_omega_left) > own_omega_max or own_turn_rad_left < own_min_turn_rad:
                        own_omega_left = - own_omega_max
                        own_turn_rad_left = own_min_turn_rad
                        idxOwnLeft = t
                else:
                    own_omega_left = - own_omega_max
                    own_turn_rad_left = own_min_turn_rad

                own_heading_left = own_heading_left + own_omega_left * dt
                own_heading_right = own_heading_right + own_omega_right * dt

                vvector_left = ang.getVelocityVector2D(own_pos.h_speed, units.rad_to_deg(own_heading_left))
                vvector_right = ang.getVelocityVector2D(own_pos.h_speed, units.rad_to_deg(own_heading_right))

                own_pos_left[t, 0] = own_pos_left[t-1, 0] + vvector_left[0] * dt
                own_pos_left[t, 1] = own_pos_left[t-1, 1] + vvector_left[1] * dt
                own_pos_left[t, 2] = own_pos.v_speed * t * dt
                own_pos_right[t, 0] = own_pos_right[t-1, 0] + vvector_right[0] * dt
                own_pos_right[t, 1] = own_pos_right[t-1, 1] + vvector_right[1] * dt
                own_pos_right[t, 2] = own_pos_left[t, 2]
                tVector[t] = t * dt
        # Otherwise
        else:
            own_pos_left[t, 0] = own_speed * t * dt
            own_pos_left[t, 1] = 0
            own_pos_left[t, 2] = own_pos.v_speed * t * dt
            own_pos_right[t, 0] = own_speed * t * dt
            own_pos_right[t, 1] = 0
            own_pos_right[t, 2] = own_pos.v_speed * t * dt

        # If intruder is intended to turn
        if int_turns:
            if t > 0:
                int_bank_angle[t] = int_bank_rate * dt;
            if t > 0 and int_omega < int_omega_max:
                int_bank_angle[t] = int_bank_angle[t - 1] + int_bank_rate * dt;
                int_turn_rad = int_speed * int_speed / (9.81 * np.tan(int_bank_angle[t]))
                int_omega = int_speed / int_turn_rad
            else:
                int_omega = int_omega_max

            int_pos_left[t, 0] = int_turn_rad * np.cos(3.0 / 2.0 * np.pi + int_omega * t * dt)
            int_pos_left[t, 1] = int_turn_rad + int_turn_rad * np.sin(3.0 / 2.0 * np.pi + int_omega * t * dt)  # check
            int_pos_left[t, 2] = int_pos.v_speed * t * dt
            int_pos_right[t, 0] = int_pos_left[t, 0]
            int_pos_right[t, 1] = -int_pos_left[t, 1]
            int_pos_right[t, 2] = int_pos_left[t, 2]
        # Otherwise
        else:
            int_pos_left[t, 0] = int_speed * t * dt
            int_pos_left[t, 1] = 0
            int_pos_left[t, 2] = int_pos.v_speed * t * dt
            int_pos_right[t, 0] = int_speed * t * dt
            int_pos_right[t, 1] = 0
            int_pos_right[t, 2] = int_pos.v_speed * t * dt
        t = t + 1


    #plt.plot(own_pos_left[:,0],own_pos_left[:,1])
    #plt.plot(own_pos_right[:, 0], own_pos_right[:, 1])
    #plt.show()
    # plt.plot(int_pos_left[:,0],int_pos_left[:,1])
    # plt.show()
    # rotate
    # print(int_pos_left)

    # Position synthetic trajectories at correct position and orientation
    # First rotate
    #own_pos_left = R_own.dot(own_pos_left.T).T
    #own_pos_right = R_own.dot(own_pos_right.T).T
    int_pos_left = R_int.dot(int_pos_left.T).T
    int_pos_right = R_int.dot(int_pos_right.T).T

    # then translate
    own_pos_left = own_pos_left + own_pos_init
    own_pos_right = own_pos_right + own_pos_init
    int_pos_left = int_pos_left + int_pos_init
    int_pos_right = int_pos_right + int_pos_init

    # plot intermediate results

    plt.axis('equal')
    plt.plot(own_pos_left[:, 0], own_pos_left[:, 1])
    plt.plot(int_pos_left[:, 0], int_pos_left[:, 1])
    plt.plot(own_pos_left[0, 0], own_pos_left[0, 1], marker='o')
    plt.plot(own_pos_right[:, 0], own_pos_right[:, 1])
    plt.plot(int_pos_right[:, 0], int_pos_right[:, 1])
    plt.plot(own_pos_left[0, 0], own_pos_left[0, 1], marker='o')
    plt.plot(int_pos_left[0, 0], int_pos_left[0, 1], marker='o')

    plt.plot(own_pos_right[idxOwnRight, 0], own_pos_right[idxOwnRight, 1], 'red', marker='*')
    plt.plot(own_pos_left[idxOwnLeft, 0], own_pos_left[idxOwnLeft, 1], 'orange', marker='*')

    plt.show()
    fig=plt.gcf()
    plt.close(fig)
    plt.clf()

    plt.plot(tVector, own_pos_left[:, 2])
    plt.plot(tVector, own_pos_right[:, 2])
    plt.plot(tVector, int_pos_left[:, 2])
    plt.plot(tVector, int_pos_right[:, 2])
    plt.plot(tVector[0], own_pos_left[0, 2], marker='o')
    plt.plot(tVector[0], int_pos_left[0, 2], marker='o')

    plt.plot(tVector[idxOwnRight], own_pos_right[idxOwnRight, 2], 'red', marker='*')
    plt.plot(tVector[idxOwnLeft], own_pos_left[idxOwnLeft, 2], 'orange', marker='*')

    plt.show()
    fig=plt.gcf()
    plt.close(fig)
    plt.clf()


    # Calculate maximum minimum distances for the given trajectories
    t = 0
    while t*dt < T_MAX:
        if rwct.check_nmac_3D(own_pos_left[t], int_pos_left[t], nmac_param) or \
            rwct.check_nmac_3D(own_pos_right[t], int_pos_left[t], nmac_param):
            prediction = LMZ_Prediction()
            prediction.minDistance = min(rwct.get_slat_distance(own_pos_left[t], int_pos_left[t]), rwct.get_slat_distance(own_pos_right[t], int_pos_left[t]))
            prediction.violates = True
            return prediction
        t = t + 1

    prediction = LMZ_Prediction()
    prediction.minDistance = 0
    prediction.violates = False

    return prediction



def violateHAZHorizontal(own_pos, int_pos, haz_param, upperBound, iniLat, maxBankAngle, bankRate):

    # Initial positions
    own_pos_init = np.array([own_pos.x_loc, own_pos.y_loc, own_pos.altitude])
    int_pos_init = np.array([int_pos.x_loc, int_pos.y_loc, int_pos.altitude])
    # Initial headings
    own_heading = own_pos.bearing * np.pi / 180.0
    int_heading = int_pos.bearing * np.pi / 180.0
    # Initial bank angles
    own_max_bank = units.deg_to_rad(maxBankAngle)
    int_max_bank = units.deg_to_rad(maxBankAngle)
    # Initial speeds
    own_speed = own_pos.h_speed
    int_speed = int_pos.h_speed
    # Initial turn rates
    own_bank_rate = bankRate * np.pi / 180
    int_bank_rate = bankRate * np.pi / 180


    # Maximum simulation time [s]
    T_MAX= upperBound

    # Is the Ownship intended to turn?
    own_turns_right = False
    own_turns_left = False
    # Is the Intruder intended to turn?
    int_turns = False

    idxOwnRight = 0
    idxOwnLeft = 0

    # Calculate the minimum turn radius of the uav
    own_min_turn_rad = own_speed * own_speed / (9.81 * np.tan(own_max_bank))

    # Calculate the maximum angular speed of the uav
    own_omega_max = own_speed / own_min_turn_rad

    # Calculate the minimum turn radius of the intruder
    int_turn_rad = int_speed * int_speed / (9.81 * np.tan(int_max_bank))

    # Calculate the maximum angular speed of the intruder
    int_omega_max = int_speed / int_turn_rad

    # Time discriminant
    dt = 0.5

    # Allocate storing matrices
    cols_count = 3
    rows_count = int(np.floor(T_MAX / dt))
    own_bank_angle_right = np.zeros((rows_count, 1))
    own_bank_angle_left = np.zeros((rows_count, 1))
    int_bank_angle = np.zeros((rows_count, 1))
    own_pos_left = np.zeros((rows_count, cols_count))
    own_pos_right = np.zeros((rows_count, cols_count))
    int_pos_left = np.zeros((rows_count, cols_count))
    int_pos_right = np.zeros((rows_count, cols_count))
    own_pos_left_vv = np.zeros((rows_count, cols_count))
    own_pos_right_vv = np.zeros((rows_count, cols_count))
    int_pos_left_vv = np.zeros((rows_count, cols_count))
    int_pos_right_vv = np.zeros((rows_count, cols_count))
    tVector = np.zeros(rows_count)

    # initialize rotation matrices
    R_int_heading = np.pi / 2 - int_heading
    R_own_heading = np.pi / 2 - own_heading
    R_int = np.array([[np.cos(R_int_heading), -np.sin(R_int_heading), 0], [np.sin(R_int_heading), np.cos(R_int_heading), 0], [0, 0, 1]])
    R_own = np.array([[np.cos(R_own_heading), -np.sin(R_own_heading), 0], [np.sin(R_own_heading), np.cos(R_own_heading), 0], [0, 0, 1]])

    vv = ang.getVelocityVector2D(int_pos.h_speed, int_pos.bearing)
    int_vvector = np.array([vv[0], vv[1], int_pos.v_speed])


    # Loop to calculate aircraft positions
    t = 0
    #own_omega = units.deg_to_rad(own_pos.turnRate)
    int_omega = 0
    rightDone = False
    leftDone= False
    while t * dt < T_MAX:
        # If ownship is intended to turn

        if t > 0:

            if own_turns_right:
                if not rightDone and (abs(own_omega_right) < own_omega_max and own_turn_rad_right > own_min_turn_rad):
                    own_bank_angle_right[t] = own_bank_angle_right[t - 1] + own_bank_rate * dt
                    own_turn_rad_right = own_speed * own_speed / (9.81 * np.tan(abs(own_bank_angle_right[t])))
                    own_omega_right = mysign(own_bank_angle_right[t]) * own_speed / own_turn_rad_right
                    if abs(own_omega_right) > own_omega_max or own_turn_rad_right < own_min_turn_rad:
                        own_omega_right = own_omega_max
                        own_turn_rad_right = own_min_turn_rad
                        idxOwnRight = t
                        rightDone = True

                own_heading_right = own_heading_right + own_omega_right * dt

                totalTurn = ang.normalizeToAcute(units.rad_to_deg(own_heading_right - start_heading_right))
                if rightDone and totalTurn >= 90 and own_omega_right > 0:
                    own_omega_right = 0

            if own_turns_left:
                if not leftDone and (abs(own_omega_left) < own_omega_max and own_turn_rad_left > own_min_turn_rad):
                    own_bank_angle_left[t] = own_bank_angle_left[t - 1] - own_bank_rate * dt
                    own_turn_rad_left = own_speed * own_speed / (9.81 * np.tan(abs(own_bank_angle_left[t])))
                    own_omega_left = mysign(own_bank_angle_left[t]) * own_speed / own_turn_rad_left
                    if abs(own_omega_left) > own_omega_max or own_turn_rad_left < own_min_turn_rad:
                        own_omega_left = - own_omega_max
                        own_turn_rad_left = own_min_turn_rad
                        idxOwnLeft = t
                        leftDone = True

                own_heading_left = own_heading_left + own_omega_left * dt

                totalTurn = ang.normalizeToAcute(units.rad_to_deg(own_heading_left - start_heading_left))
                if leftDone and abs(totalTurn) >= 90 and abs(own_omega_left) > 0:
                    own_omega_left = 0


            if not own_turns_right:
                own_bank_angle_right[t] = own_bank_angle_right[t - 1]
                own_turn_rad_right = own_speed * own_speed / (9.81 * np.tan(abs(own_bank_angle_right[t])))
                own_omega_right = mysign(own_bank_angle_right[t]) * own_speed / own_turn_rad_right

                own_heading_right = own_heading_right + own_omega_right * dt

                if t * dt > iniLat and not own_turns_right:
                    own_turns_right = True
                    start_heading_right = own_heading_right

            if not own_turns_left:
                own_bank_angle_left[t] = own_bank_angle_left[t - 1]
                own_turn_rad_left = own_speed * own_speed / (9.81 * np.tan(abs(own_bank_angle_left[t])))
                own_omega_left = mysign(own_bank_angle_left[t]) * own_speed / own_turn_rad_left

                own_heading_left = own_heading_left + own_omega_left * dt

                if t * dt > iniLat and not own_turns_left:
                    own_turns_left = True
                    start_heading_left = own_heading_left


            vvector_left = ang.getVelocityVector2D(own_pos.h_speed, units.rad_to_deg(own_heading_left))
            vvector_right = ang.getVelocityVector2D(own_pos.h_speed, units.rad_to_deg(own_heading_right))

            own_pos_left[t, 0] = own_pos_left[t-1, 0] + vvector_left[0] * dt
            own_pos_left[t, 1] = own_pos_left[t-1, 1] + vvector_left[1] * dt
            own_pos_left[t, 2] = own_pos.v_speed * t * dt
            own_pos_right[t, 0] = own_pos_right[t-1, 0] + vvector_right[0] * dt
            own_pos_right[t, 1] = own_pos_right[t-1, 1] + vvector_right[1] * dt
            own_pos_right[t, 2] = own_pos_left[t, 2]

            vv = ang.getVelocityVector2D(own_pos.h_speed, units.rad_to_deg(own_heading_left))
            own_pos_left_vv[t, 0] = vv[0]
            own_pos_left_vv[t, 1] = vv[1]
            own_pos_left_vv[t, 2] = own_pos.v_speed

            vv = ang.getVelocityVector2D(own_pos.h_speed, units.rad_to_deg(own_heading_right))
            own_pos_right_vv[t, 0] = vv[0]
            own_pos_right_vv[t, 1] = vv[1]
            own_pos_right_vv[t, 2] = own_pos.v_speed

        if t == 0:
            own_heading_left = units.deg_to_rad(own_pos.bearing)
            own_heading_right = units.deg_to_rad(own_pos.bearing)
            own_omega_right = units.deg_to_rad(own_pos.turnRate)
            own_omega_left = units.deg_to_rad(own_pos.turnRate)
            own_bank_angle_right[t] = units.deg_to_rad(own_pos.bankAngle)
            own_bank_angle_left[t] = units.deg_to_rad(own_pos.bankAngle)
            own_turn_rad_right = own_speed * own_speed / (9.81 * np.tan(abs(own_bank_angle_right[t])))
            own_turn_rad_left = own_speed * own_speed / (9.81 * np.tan(abs(own_bank_angle_left[t])))

            vv = ang.getVelocityVector2D(own_pos.h_speed, own_pos.bearing)
            own_pos_left[t, 0] = 0
            own_pos_left[t, 1] = 0
            own_pos_left[t, 2] = 0
            own_pos_right[t, 0] = 0
            own_pos_right[t, 1] = 0
            own_pos_right[t, 2] = 0

            own_pos_left_vv[t, 0] = vv[0]
            own_pos_left_vv[t, 1] = vv[1]
            own_pos_left_vv[t, 2] = own_pos.v_speed
            own_pos_right_vv[t, 0] = vv[0]
            own_pos_right_vv[t, 1] = vv[1]
            own_pos_right_vv[t, 2] = own_pos.v_speed

        tVector[t] = t * dt


        # If intruder is intended to turn
        if int_turns:
            if t > 0:
                int_bank_angle[t] = int_bank_rate * dt;
            if t > 0 and int_omega < int_omega_max:
                int_bank_angle[t] = int_bank_angle[t - 1] + int_bank_rate * dt;
                int_turn_rad = int_speed * int_speed / (9.81 * np.tan(int_bank_angle[t]))
                int_omega = int_speed / int_turn_rad
            else:
                int_omega = int_omega_max

            int_pos_left[t, 0] = int_turn_rad * np.cos(3.0 / 2.0 * np.pi + int_omega * t * dt)
            int_pos_left[t, 1] = int_turn_rad + int_turn_rad * np.sin(3.0 / 2.0 * np.pi + int_omega * t * dt)  # check
            int_pos_left[t, 2] = int_pos.v_speed * t * dt
            int_pos_right[t, 0] = int_pos_left[t, 0]
            int_pos_right[t, 1] = -int_pos_left[t, 1]
            int_pos_right[t, 2] = int_pos_left[t, 2]
        # Otherwise
        else:
            int_pos_left[t, 0] = int_speed * t * dt
            int_pos_left[t, 1] = 0
            int_pos_left[t, 2] = int_pos.v_speed * t * dt
            int_pos_right[t, 0] = int_speed * t * dt
            int_pos_right[t, 1] = 0
            int_pos_right[t, 2] = int_pos.v_speed * t * dt
        t = t + 1


    #plt.plot(own_pos_left[:,0],own_pos_left[:,1])
    #plt.plot(own_pos_right[:, 0], own_pos_right[:, 1])
    #plt.show()
    # plt.plot(int_pos_left[:,0],int_pos_left[:,1])
    # plt.show()
    # rotate
    # print(int_pos_left)

    # Position synthetic trajectories at correct position and orientation
    # First rotate
    #own_pos_left = R_own.dot(own_pos_left.T).T
    #own_pos_right = R_own.dot(own_pos_right.T).T
    int_pos_left = R_int.dot(int_pos_left.T).T
    int_pos_right = R_int.dot(int_pos_right.T).T

    # then translate
    own_pos_left = own_pos_left + own_pos_init
    own_pos_right = own_pos_right + own_pos_init
    int_pos_left = int_pos_left + int_pos_init
    int_pos_right = int_pos_right + int_pos_init

    # plot intermediate results

    '''plt.axis('equal')
    plt.plot(own_pos_left[:, 0], own_pos_left[:, 1])
    plt.plot(int_pos_left[:, 0], int_pos_left[:, 1])
    plt.plot(own_pos_left[0, 0], own_pos_left[0, 1], marker='o')
    plt.plot(own_pos_right[:, 0], own_pos_right[:, 1])
    plt.plot(int_pos_right[:, 0], int_pos_right[:, 1])
    plt.plot(own_pos_left[0, 0], own_pos_left[0, 1], marker='o')
    plt.plot(int_pos_left[0, 0], int_pos_left[0, 1], marker='o')

    plt.plot(own_pos_right[idxOwnRight, 0], own_pos_right[idxOwnRight, 1], 'red', marker='*')
    plt.plot(own_pos_left[idxOwnLeft, 0], own_pos_left[idxOwnLeft, 1], 'orange', marker='*')

    plt.show()
    fig=plt.gcf()
    plt.close(fig)
    plt.clf()

    plt.plot(tVector, own_pos_left[:, 2])
    plt.plot(tVector, own_pos_right[:, 2])
    plt.plot(tVector, int_pos_left[:, 2])
    plt.plot(tVector, int_pos_right[:, 2])
    plt.plot(tVector[0], own_pos_left[0, 2], marker='o')
    plt.plot(tVector[0], int_pos_left[0, 2], marker='o')

    plt.plot(tVector[idxOwnRight], own_pos_right[idxOwnRight, 2], 'red', marker='*')
    plt.plot(tVector[idxOwnLeft], own_pos_left[idxOwnLeft, 2], 'orange', marker='*')

    plt.show()
    fig=plt.gcf()
    plt.close(fig)
    plt.clf()'''


    # Calculate maximum minimum distances for the given trajectories
    Left_Idx = 0
    Right_Idx = 0

    t = 0
    while t*dt < T_MAX:
        RWC_out_left = rwct.check_hazard_zone_3D(own_pos_left[t], own_pos_left_vv[t], int_pos_left[t], int_vvector, haz_param)

        if RWC_out_left.WCV:
            Left_Idx = t
            break

        t = t + 1

    t = 0
    while t * dt < T_MAX:
        RWC_out_right = rwct.check_hazard_zone_3D(own_pos_right[t], own_pos_right_vv[t], int_pos_left[t], int_vvector, haz_param)

        if RWC_out_right.WCV:
            Right_Idx = t
            break

        t = t + 1

    if RWC_out_left.WCV and RWC_out_right.WCV:
        '''plt.axis('equal')
        plt.plot(own_pos_left[:, 0], own_pos_left[:, 1])
        plt.plot(int_pos_left[:, 0], int_pos_left[:, 1])
        plt.plot(own_pos_left[0, 0], own_pos_left[0, 1], marker='o')
        plt.plot(own_pos_right[:, 0], own_pos_right[:, 1])
        plt.plot(int_pos_right[:, 0], int_pos_right[:, 1])
        plt.plot(own_pos_left[0, 0], own_pos_left[0, 1], marker='o')
        plt.plot(int_pos_left[0, 0], int_pos_left[0, 1], marker='o')

        plt.plot(own_pos_right[idxOwnRight, 0], own_pos_right[idxOwnRight, 1], 'yellow', marker='*')
        plt.plot(own_pos_left[idxOwnLeft, 0], own_pos_left[idxOwnLeft, 1], 'orange', marker='*')
        plt.plot(own_pos_right[Right_Idx, 0], own_pos_right[Right_Idx, 1], 'red', marker='*')
        plt.plot(own_pos_left[Left_Idx, 0], own_pos_left[Left_Idx, 1], 'red', marker='*')

        plt.show()
        fig=plt.gcf()
        plt.close(fig)
        plt.clf()

        plt.plot(tVector, own_pos_left[:, 2])
        plt.plot(tVector, own_pos_right[:, 2])
        plt.plot(tVector, int_pos_left[:, 2])
        plt.plot(tVector, int_pos_right[:, 2])
        plt.plot(tVector[0], own_pos_left[0, 2], marker='o')
        plt.plot(tVector[0], int_pos_left[0, 2], marker='o')

        plt.plot(tVector[idxOwnRight], own_pos_right[idxOwnRight, 2], 'yellow', marker='*')
        plt.plot(tVector[idxOwnLeft], own_pos_left[idxOwnLeft, 2], 'orange', marker='*')
        plt.plot(tVector[Right_Idx], own_pos_right[Right_Idx, 2], 'red', marker='*')
        plt.plot(tVector[Left_Idx], own_pos_left[Left_Idx, 2], 'red', marker='*')

        plt.show()
        fig=plt.gcf()
        plt.close(fig)
        plt.clf()'''

        prediction = LMZ_Prediction()
        prediction.minDistance = 0
        prediction.violates = True
        return prediction



    prediction = LMZ_Prediction()
    prediction.minDistance = 0
    prediction.violates = False

    return prediction




def violateHAZVertical(own_pos, int_pos, haz_param, upperBound, iniLat, maxClimbRate, maxDescentRate, climbRotation, descentRotation):

    # Initial positions
    own_pos_init = np.array([own_pos.x_loc, own_pos.y_loc, own_pos.altitude])
    int_pos_init = np.array([int_pos.x_loc, int_pos.y_loc, int_pos.altitude])
    # Initial headings
    own_heading = own_pos.bearing * np.pi / 180.0
    int_heading = int_pos.bearing * np.pi / 180.0
    # Initial speeds
    own_speed = own_pos.h_speed
    int_speed = int_pos.h_speed


    # Maximum simulation time [s]
    T_MAX= upperBound

    # Is the Ownship intended to turn?
    own_climbs = False
    own_descends = False
    # Is the Intruder intended to turn?
    int_turns = False

    idxOwnClimb = 0
    idxOwnDescent = 0


    # Time discriminant
    dt = 0.5

    # Allocate storing matrices
    cols_count = 3
    rows_count = int(np.floor(T_MAX / dt))
    own_pos_climb = np.zeros((rows_count, cols_count))
    own_pos_descent = np.zeros((rows_count, cols_count))
    int_pos_left = np.zeros((rows_count, cols_count))
    int_pos_right = np.zeros((rows_count, cols_count))
    own_pos_climb_vv = np.zeros((rows_count, cols_count))
    own_pos_descent_vv = np.zeros((rows_count, cols_count))
    tVector = np.zeros(rows_count)

    # initialize rotation matrices
    R_int_heading = np.pi / 2 - int_heading
    R_own_heading = np.pi / 2 - own_heading
    R_int = np.array([[np.cos(R_int_heading), -np.sin(R_int_heading), 0], [np.sin(R_int_heading), np.cos(R_int_heading), 0], [0, 0, 1]])
    R_own = np.array([[np.cos(R_own_heading), -np.sin(R_own_heading), 0], [np.sin(R_own_heading), np.cos(R_own_heading), 0], [0, 0, 1]])

    vv = ang.getVelocityVector2D(int_pos.h_speed, int_pos.bearing)
    int_vvector = np.array([vv[0], vv[1], int_pos.v_speed])


    # Loop to calculate aircraft positions
    t = 0
    #own_omega = units.deg_to_rad(own_pos.turnRate)
    int_omega = 0
    climbDone = False
    descentDone= False
    while t * dt < T_MAX:
        # If ownship is intended to turn

        if t > 0:

            if own_climbs:
                if not climbDone and abs(own_pos_climb[t-1, 2]) < maxClimbRate:
                    own_climb = own_pos_climb_vv[t-1, 2] + units.fpm_to_ms(climbRotation * dt)

                    if abs(own_climb) > units.fpm_to_ms(maxClimbRate):
                        own_climb = units.fpm_to_ms(maxClimbRate)
                        idxOwnClimb = t
                        climbDone = True

            if own_descends:
                if not descentDone and abs(own_pos_descent[t-1, 2]) < maxDescentRate:
                    own_descent = own_pos_descent_vv[t-1, 2] - units.fpm_to_ms(descentRotation * dt)

                    if abs(own_descent) > units.fpm_to_ms(maxDescentRate):
                        own_descent = -units.fpm_to_ms(maxDescentRate)
                        idxOwnDescent = t
                        descentDone = True


            if not own_climbs:
                own_climb = own_pos_climb_vv[t-1, 2]

                if t * dt > iniLat and not own_climbs:
                    own_climbs = True

            if not own_descends:
                own_descent = own_pos_descent_vv[t-1, 2]

                if t * dt > iniLat and not own_descends:
                    own_descends = True


            vv = ang.getVelocityVector2D(own_pos.h_speed, units.rad_to_deg(own_heading))

            own_pos_climb[t, 0] = own_pos_climb[t-1, 0] + vv[0] * dt
            own_pos_climb[t, 1] = own_pos_climb[t-1, 1] + vv[1] * dt
            own_pos_climb[t, 2] = own_pos_climb[t-1, 2] + own_climb * dt
            own_pos_descent[t, 0] = own_pos_descent[t-1, 0] + vv[0] * dt
            own_pos_descent[t, 1] = own_pos_descent[t-1, 1] + vv[1] * dt
            own_pos_descent[t, 2] = own_pos_descent[t-1, 2] + own_descent * dt

            vv = ang.getVelocityVector2D(own_pos.h_speed, units.rad_to_deg(own_heading))
            own_pos_climb_vv[t, 0] = vv[0]
            own_pos_climb_vv[t, 1] = vv[1]
            own_pos_climb_vv[t, 2] = own_climb

            own_pos_descent_vv[t, 0] = vv[0]
            own_pos_descent_vv[t, 1] = vv[1]
            own_pos_descent_vv[t, 2] = own_descent

        if t == 0:
            own_heading = units.deg_to_rad(own_pos.bearing)
            own_omega = units.deg_to_rad(own_pos.turnRate)
            own_bank_angle = units.deg_to_rad(own_pos.bankAngle)
            own_turn_rad = own_speed * own_speed / (9.81 * np.tan(abs(own_bank_angle)))

            vv = ang.getVelocityVector2D(own_pos.h_speed, own_pos.bearing)
            own_pos_climb[t, 0] = 0
            own_pos_climb[t, 1] = 0
            own_pos_climb[t, 2] = 0
            own_pos_descent[t, 0] = 0
            own_pos_descent[t, 1] = 0
            own_pos_descent[t, 2] = 0

            own_pos_climb_vv[t, 0] = vv[0]
            own_pos_climb_vv[t, 1] = vv[1]
            own_pos_climb_vv[t, 2] = own_pos.v_speed
            own_pos_descent_vv[t, 0] = vv[0]
            own_pos_descent_vv[t, 1] = vv[1]
            own_pos_descent_vv[t, 2] = own_pos.v_speed

        tVector[t] = t * dt


        # If intruder is intended to turn
        int_pos_left[t, 0] = int_speed * t * dt
        int_pos_left[t, 1] = 0
        int_pos_left[t, 2] = int_pos.v_speed * t * dt
        int_pos_right[t, 0] = int_speed * t * dt
        int_pos_right[t, 1] = 0
        int_pos_right[t, 2] = int_pos.v_speed * t * dt
        t = t + 1


    #plt.plot(own_pos_left[:,0],own_pos_left[:,1])
    #plt.plot(own_pos_right[:, 0], own_pos_right[:, 1])
    #plt.show()
    # plt.plot(int_pos_left[:,0],int_pos_left[:,1])
    # plt.show()
    # rotate
    # print(int_pos_left)

    # Position synthetic trajectories at correct position and orientation
    # First rotate
    #own_pos_left = R_own.dot(own_pos_left.T).T
    #own_pos_right = R_own.dot(own_pos_right.T).T
    int_pos_left = R_int.dot(int_pos_left.T).T
    int_pos_right = R_int.dot(int_pos_right.T).T

    # then translate
    own_pos_climb = own_pos_climb + own_pos_init
    own_pos_descent = own_pos_descent + own_pos_init
    int_pos_left = int_pos_left + int_pos_init
    int_pos_right = int_pos_right + int_pos_init

    # plot intermediate results

    '''plt.axis('equal')
    plt.plot(own_pos_climb[:, 0], own_pos_climb[:, 1])
    plt.plot(int_pos_left[:, 0], int_pos_left[:, 1])
    plt.plot(own_pos_climb[0, 0], own_pos_climb[0, 1], marker='o')
    plt.plot(own_pos_descent[:, 0], own_pos_descent[:, 1])
    plt.plot(int_pos_right[:, 0], int_pos_right[:, 1])
    plt.plot(own_pos_descent[0, 0], own_pos_descent[0, 1], marker='o')
    plt.plot(int_pos_left[0, 0], int_pos_left[0, 1], marker='o')

    plt.plot(own_pos_climb[idxOwnClimb, 0], own_pos_climb[idxOwnClimb, 1], 'red', marker='*')
    plt.plot(own_pos_descent[idxOwnDescent, 0], own_pos_descent[idxOwnDescent, 1], 'orange', marker='*')

    plt.show()
    fig=plt.gcf()
    plt.close(fig)
    plt.clf()

    plt.plot(tVector, own_pos_climb[:, 2])
    plt.plot(tVector, own_pos_descent[:, 2])
    plt.plot(tVector, int_pos_left[:, 2])
    plt.plot(tVector, int_pos_right[:, 2])
    plt.plot(tVector[0], own_pos_climb[0, 2], marker='o')
    plt.plot(tVector[0], int_pos_left[0, 2], marker='o')

    plt.plot(tVector[idxOwnClimb], own_pos_climb[idxOwnClimb, 2], 'red', marker='*')
    plt.plot(tVector[idxOwnDescent], own_pos_descent[idxOwnDescent, 2], 'orange', marker='*')

    plt.show()
    fig=plt.gcf()
    plt.close(fig)
    plt.clf()'''


    # Calculate maximum minimum distances for the given trajectories
    Climb_Idx = 0
    Descent_Idx = 0
    t = 0
    while t*dt < T_MAX:
        RWC_out_climb = rwct.check_hazard_zone_3D(own_pos_climb[t], own_pos_climb_vv[t], int_pos_left[t], int_vvector, haz_param)

        if RWC_out_climb.WCV:
            Climb_Idx = t
            break

        t = t + 1

    t = 0
    while t * dt < T_MAX:
        RWC_out_descent = rwct.check_hazard_zone_3D(own_pos_descent[t], own_pos_descent_vv[t], int_pos_left[t], int_vvector, haz_param)

        if RWC_out_descent.WCV:
            Descent_Idx = t
            break

        t = t + 1

    if RWC_out_climb.WCV and RWC_out_descent.WCV:
        prediction = LMZ_Prediction()
        prediction.minDistance = 0
        prediction.violates = True

        '''plt.axis('equal')
        plt.plot(own_pos_climb[:, 0], own_pos_climb[:, 1])
        plt.plot(int_pos_left[:, 0], int_pos_left[:, 1])
        plt.plot(own_pos_climb[0, 0], own_pos_climb[0, 1], marker='o')
        plt.plot(own_pos_descent[:, 0], own_pos_descent[:, 1])
        plt.plot(int_pos_right[:, 0], int_pos_right[:, 1])
        plt.plot(own_pos_descent[0, 0], own_pos_descent[0, 1], marker='o')
        plt.plot(int_pos_left[0, 0], int_pos_left[0, 1], marker='o')

        plt.plot(own_pos_climb[idxOwnClimb, 0], own_pos_climb[idxOwnClimb, 1], 'yellow', marker='*')
        plt.plot(own_pos_descent[idxOwnDescent, 0], own_pos_descent[idxOwnDescent, 1], 'orange', marker='*')
        plt.plot(own_pos_climb[Climb_Idx, 0], own_pos_climb[Climb_Idx, 1], 'red', marker='*')
        plt.plot(own_pos_descent[Descent_Idx, 0], own_pos_descent[Descent_Idx, 1], 'red', marker='*')

        plt.show()
        fig=plt.gcf()
        plt.close(fig)
        plt.clf()

        plt.plot(tVector, own_pos_climb[:, 2])
        plt.plot(tVector, own_pos_descent[:, 2])
        plt.plot(tVector, int_pos_left[:, 2])
        plt.plot(tVector, int_pos_right[:, 2])
        plt.plot(tVector[0], own_pos_climb[0, 2], marker='o')
        plt.plot(tVector[0], int_pos_left[0, 2], marker='o')

        plt.plot(tVector[idxOwnClimb], own_pos_climb[idxOwnClimb, 2], 'yellow', marker='*')
        plt.plot(tVector[idxOwnDescent], own_pos_descent[idxOwnDescent, 2], 'orange', marker='*')
        plt.plot(tVector[Climb_Idx], own_pos_climb[Climb_Idx, 2], 'red', marker='*')
        plt.plot(tVector[Descent_Idx], own_pos_descent[Descent_Idx, 2], 'red', marker='*')

        plt.show()
        fig=plt.gcf()
        plt.close(fig)
        plt.clf()'''

        return prediction


    prediction = LMZ_Prediction()
    prediction.minDistance = 0
    prediction.violates = False

    return prediction




def violateDiscVert(own_pos, int_pos, HMD, VMD, roc, rod):
    NMAC_hor = HMD
    NMAC_ver = VMD

    # Initial positions
    own_pos_init = np.array([own_pos.x_loc, own_pos.y_loc])
    int_pos_init = np.array([int_pos.x_loc, int_pos.y_loc])
    own_pos_init_z = own_pos.z_loc
    int_pos_init_z = int_pos.z_loc
    # Initial headings
    own_heading = own_pos.bearing * np.pi / 180.0
    int_heading = int_pos.bearing * np.pi / 180.0
    # Initial bank angles
    own_bank = 0 * np.pi / 180.0
    int_bank = 0 * np.pi / 180.0
    # Initial speeds
    own_speed = own_pos.h_speed * 1852 / 3600.0
    int_speed = int_pos.h_speed * 1852 / 3600.0
    # Initial turn rates
    own_turn_rate = 0 * np.pi / 180
    int_turn_rate = 0 * np.pi / 180


    # Is the Ownship intended to climb/descend? 0:No 1:Yes
    own_maneuvers = True

    # Is the intruder intended to climb/descend? 0:No 1:Yes
    int_maneuvers = False

    # Maximum simulation time [s]
    T_MAX= 30

    # Increment of time
    dt = 0.5
    # Allocate storing matrices
    cols_count = 2
    rows_count = int(np.floor(T_MAX/dt))
    own_pos = np.zeros((rows_count, cols_count))
    int_pos = np.zeros((rows_count, cols_count))
    own_pos_vert = np.zeros((rows_count, 2))
    int_pos_vert = np.zeros((rows_count, 2))

    # Initialize rotation matrices
    R_int_heading = int_heading + np.pi / 2
    R_own_heading = own_heading + np.pi / 2
    R_int = np.array([[np.cos(R_int_heading), -np.sin(R_int_heading)], [np.sin(R_int_heading), np.cos(R_int_heading)]])
    R_own = np.array([[np.cos(R_own_heading), -np.sin(R_own_heading)], [np.sin(R_own_heading), np.cos(R_own_heading)]])

    # Loop to calculate aircraft positions
    t = 0
    while t*dt < T_MAX:

    # Calculate horizontal trajectory
        own_pos[t] = own_speed * t * dt
        int_pos[t] = int_speed * t * dt

    # If ownship is intended to vertically maneuver
        if own_maneuvers:
            own_pos_vert[t,0] = roc * t * dt
            own_pos_vert[t,1] = rod * t * dt
    # If intruder is intended to vertically maneuver
        if int_maneuvers:
            int_pos_vert[t,0] = roc * t * dt
            int_pos_vert[t,1] = rod * t * dt
        t = t + 1

    # Position synthetic trajectories at correct position and orientation
    # First rotate
    own_pos = R_own.dot(own_pos.T).T
    int_pos = R_int.dot(int_pos.T).T

    # Then translate
    own_pos = own_pos + own_pos_init
    int_pos = int_pos + int_pos_init
    own_pos_vert = own_pos_vert + own_pos_init_z
    int_pos_vert = int_pos_vert + int_pos_init_z

    # Calculate maximum minimum distances for the given trajectories
    t = 0
    while t*dt < T_MAX:
        dist_hor = np.sqrt(np.power(own_pos[t,0] - int_pos[t,0], 2) + np.power(own_pos[t,1] - int_pos[t,1], 2))
        if dist_hor < NMAC_hor:
            dist_vert1 = own_pos_vert[t,0] - int_pos_vert[t,0]
            dist_vert2 = own_pos_vert[t,0] - int_pos_vert[t,1]
            dist_vert3 = own_pos_vert[t,1] - int_pos_vert[t,0]
            dist_vert4 = own_pos_vert[t,1] - int_pos_vert[t,1]
            if dist_vert1 > NMAC_ver or dist_vert2 > NMAC_ver or dist_vert3 > NMAC_ver or dist_vert4 > NMAC_ver:
                print('OK')
            else:
                print('FAILED')
            break
        t = t + 1


def violateNMAC(own, intr, NMAC_param, upperBound, maxBankAngle, bankRate, climbRate, descentRate):
    return violateDiscHorizontal(own, intr, NMAC_param, upperBound, maxBankAngle, bankRate, climbRate, descentRate)


def violateHAZ(own, intr, haz_param_caution, upperBound, iniLat, maxBankAngle, bankRate, maxClimbRate, maxDescentRate, climbRotation, descentRotation):
    LMZ_Hor_Violation = violateHAZHorizontal(own, intr, haz_param_caution, upperBound, iniLat, maxBankAngle, bankRate)

    if LMZ_Hor_Violation.violates:
        LMZ_Ver_Violation = violateHAZVertical(own, intr, haz_param_caution, upperBound, iniLat, maxClimbRate, maxDescentRate, climbRotation, descentRotation)

        if LMZ_Hor_Violation.violates and LMZ_Ver_Violation.violates:
            return True

    return False



