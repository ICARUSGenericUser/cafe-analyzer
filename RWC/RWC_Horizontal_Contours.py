import math as math
import numpy as np
import os
import matplotlib.pyplot as plt
from Encounter import units as units
from RWC import RWC_volume as rwct, RWC_thresholds as rwcthr

# Calculates position after t time units from s in direction and magnitude of velocity v
# @ param s position
# @ param v velocity
# @ param t time
# @ return the new position

def linear_projection(s, v, t):
  return np.array([s[0] + v[0] * t, s[1] + v[1] * t, s[2] + v[2] * t])

def linear_projection_2D(s, v, t):
  return np.array([s[0] + v[0] * t, s[1] + v[1] * t])


def horizontalContours(own_pos, int_pos, B, T, rwc_param, debug):
  so2 = np.array([own_pos.x_loc, own_pos.y_loc])
  so3 = np.array([own_pos.x_loc, own_pos.y_loc, own_pos.altitude])
  vo2 = np.array([own_pos.x_v, own_pos.y_v])
  si2 = np.array([int_pos.x_loc, int_pos.y_loc])
  vi2 = np.array([int_pos.x_v, int_pos.y_v])
  sz = int_pos.altitude - own_pos.altitude
  vz = int_pos.v_speed - own_pos.v_speed

  arcf_int_out = []

  current_trk = own_pos.getTrack()

  # First step: Computes conflict contour(contour in the current path of the aircraft).
  # Get contour portion to the right. If los.getTimeIn() == 0, a 360 degree
  # contour will be computed.Otherwise, stops at the first non - conflict degree.

  right = 0 # Contour conflict limit to the right relative to current track[0 - 2pi rad]
  two_pi = 2 * math.pi

  while right < two_pi:
    vo2_mod = own_pos.mkAddTrk(right)
    vo3 = np.append(vo2_mod, own_pos.v_speed)

    arcf_int = rwcthr.WCV_interval_int(si2, vi2, so2, vo2_mod, sz, vz, B, T, rwc_param.wcv_param_caution, debug)

    if not arcf_int.conflict():
      break

    right += rwc_param.track_step

    if arcf_int.low == 0:
      arcf_int.addPosition(so3, linear_projection(so3, vo3, arcf_int.up))
    else:
      arcf_int.addPosition(linear_projection(so3, vo3, arcf_int.low), linear_projection(so3, vo3, arcf_int.up))

    arcf_int_out.append(arcf_int)

  # Second step: Compute conflict contour to the left

  left = rwc_param.track_step # Contour conflict limit to the left relative to current track[0 - 2pi rad]

  if 0 < right and right < two_pi:

    while left < two_pi:
      vo2_mod = own_pos.mkAddTrk(-left)
      vo3 = np.append(vo2_mod, own_pos.v_speed)

      arcf_int = rwcthr.WCV_interval_int(si2, vi2, so2, vo2_mod, sz, vz, B, T, rwc_param.wcv_param_caution, debug)

      if not arcf_int.conflict():
        break

      left += rwc_param.track_step

      if arcf_int.low == 0:
        arcf_int.addPosition(so3, linear_projection(so3, vo3, arcf_int.up))
      else:
        arcf_int.addPosition(linear_projection(so3, vo3, arcf_int.low), linear_projection(so3, vo3, arcf_int.up))

      arcf_int_out.insert(0, arcf_int)

  return arcf_int_out


def plotHorizontalContours(folder, enc_number, time, ft, own_pos, int_pos, arcf_int_cont):

  aircraft1Color = 'navy'
  aircraft2Color = 'dimgray'

  vx_in = [0] * len(arcf_int_cont)
  vy_in = [0] * len(arcf_int_cont)
  vx_out = [0] * len(arcf_int_cont)
  vy_out = [0] * len(arcf_int_cont)

  so2 = np.array([own_pos.x_loc, own_pos.y_loc])
  si2 = np.array([int_pos.x_loc, int_pos.y_loc])
  vo2 = np.array([own_pos.x_v, own_pos.y_v])
  vi2 = np.array([int_pos.x_v, int_pos.y_v])

  so2_est = linear_projection_2D(so2, vo2, 15)
  si2_est = linear_projection_2D(si2, vi2, 15)

  #
  # Location of aircraft1
  plt.plot(units.m_to_ft(so2[0]), units.m_to_ft(so2[1]), marker='o', color=aircraft1Color)
  plt.plot(units.m_to_ft(so2_est[0]), units.m_to_ft(so2_est[1]), marker='.', color=aircraft1Color)

  X = np.array([units.m_to_ft(so2[0]), units.m_to_ft(so2_est[0])])
  Y = np.array([units.m_to_ft(so2[1]), units.m_to_ft(so2_est[1])])
  plt.plot(X, Y, marker='.', color=aircraft1Color)

  #
  # Location of aircraft2
  plt.plot(units.m_to_ft(si2[0]), units.m_to_ft(si2[1]), marker='o', color=aircraft2Color)
  plt.plot(units.m_to_ft(si2_est[0]), units.m_to_ft(si2_est[1]), marker='.', color=aircraft2Color)

  X = np.array([units.m_to_ft(si2[0]), units.m_to_ft(si2_est[0])])
  Y = np.array([units.m_to_ft(si2[1]), units.m_to_ft(si2_est[1])])
  plt.plot(X, Y, marker='.', color=aircraft2Color)

  for i in range(len(arcf_int_cont)):
    vx_in[i] = units.m_to_ft(arcf_int_cont[i].s3_low[0])
    vy_in[i] = units.m_to_ft(arcf_int_cont[i].s3_low[1])
    vx_out[i] = units.m_to_ft(arcf_int_cont[i].s3_up[0])
    vy_out[i] = units.m_to_ft(arcf_int_cont[i].s3_up[1])

  plt.plot(vx_in, vy_in, color="red", label="Entry conflict")
  plt.plot(vx_out, vy_out, color="green", label="Exit conflict")

  plt.axis('equal')
  plt.legend(prop={'size': 6})

  if not os.path.exists(folder + "\\Encs_Countours_" + str(enc_number).zfill(9)):
    os.mkdir(folder + "\\Encs_Countours_" + str(enc_number).zfill(9))

  plt.suptitle("Encounter_Distribution Id: " + str(enc_number).zfill(9) + " Time: " + str(time).zfill(3))
  plt.title("Horizontal trajectories")
  fig = plt.gcf()
  filepath = folder + "\\Encs_Countours_" + str(enc_number).zfill(9) + "\\Encs_Countours_" + str(enc_number).zfill(9) + "_" + ft +"_Time_" + str(time).zfill(3) + ".png"
  print("Generating figure: " + filepath)
  fig.savefig(filepath)
  plt.close(fig)

