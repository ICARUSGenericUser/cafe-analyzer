import math as math
from RWC import RWC_interval as interval, RWC_vertical as vertical


# Vertical Well Clear Volume concept

# Vertical Not Well Clear Violation
# ZTHR and T_star are altitude and time thresholds
def vertical_WCV(ZTHR, T_star, sz, vz):
  return math.abs(sz) <= ZTHR or (not math.isclose(vz, 0) and sz * vz <= 0 and math.abs(sz) <= ZTHR + math.abs(vz) * T_star)


# Implementation of non-hazard zone
# ZTHR and T_star are altitude and time thresholds
# ZTHR = H*
# T_star Vmod for NHZ
def vertical_WCV_interval(ZTHR, T_star, B, T, sz, vz):
  time_in = B
  time_out = T

  if (math.isclose(vz, 0) and math.abs(sz) <= ZTHR):
    return interval(time_in, time_out)
  if (math.isclose(vz, 0)):
    time_in = T
    time_out = B
    return interval(time_in, time_out);

  act_H = max(ZTHR, ZTHR - vertical.sign(sz * vz) * math.abs(vz) * T_star)
  tentry = vertical.Theta_H(sz, vz, -1, act_H)
  texit = vertical.Theta_H(sz, vz, 1, ZTHR)

  if (T < tentry or texit < B):
    time_in = T
    time_out = B
    return interval(time_in, time_out)

  time_in = max(B, tentry)
  time_out = min(T, texit)
  return interval(time_in, time_out)
