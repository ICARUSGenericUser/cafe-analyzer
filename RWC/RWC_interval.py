import math as math

class Interval:

  def __init__(self, low, up):
    self.low = low
    self.up = up
    self.s3_low = None
    self.s3_up = None

  def addPosition(self, s3_low, s3_up):
    self.s3_low = s3_low
    self.s3_up = s3_up

  def isEmpty(self):
    return self.up < self.low

  # Returns true if loss
  def conflict(self):
    return self.low < self.up and not math.isclose(self.low, self.up)


