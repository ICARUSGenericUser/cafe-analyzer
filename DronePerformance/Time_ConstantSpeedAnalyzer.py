import os, shutil
import pathlib

import pandas as pd
import plotly.graph_objects as go
from plotly.offline import plot

from os.path import isfile, join
from os import listdir
from DronePerformance import ParserController as pc
from DronePerformance import TimeAnalysis as ta

from geopy.distance import geodesic


def GetFiles(traj_folder):
    file_list = []
    onlyfilesList = []
    onlyfolders = listdir(traj_folder)

    for folder in onlyfolders:
        oppath = join(traj_folder, folder)
        onlyfiles = [f for f in listdir(oppath) if isfile(join(oppath, f))]

        for file in onlyfiles:
            pack = [oppath, file]
            onlyfilesList.append(pack)

    for pack in onlyfilesList:
        splitter = pack[1].split("-")
        if ((splitter[0] == 'FP')):
            file_list.append(pack)

    return file_list

def calculate_centroid_from_series(xs: pd.Series, ys: pd.Series):
    """
    Calculate the centroid of a set of 2D points provided as two pandas Series.

    Parameters:
    xs (pd.Series): A pandas Series representing the x-coordinates.
    ys (pd.Series): A pandas Series representing the y-coordinates.

    Returns:
    tuple: The (x, y) coordinates of the centroid.
    """
    if len(xs) != len(ys):
        raise ValueError("The length of the x and y series must be the same.")

    # Calculate the centroid
    centroid_x = xs.mean()
    centroid_y = ys.mean()

    return centroid_x, centroid_y

def generateAccelerationTrajectoryPlotComplete(latitude, longitude, Title):
    figure = go.Figure()
    figure.add_trace(
        go.Scattermap(
            lat=latitude,
            lon=longitude,
            mode='markers+lines',
            marker=go.scattermap.Marker(
                size=14,
                color='rgb(0,0,255)',
                opacity=0.6
            )
        )
    )

    centroid = calculate_centroid_from_series(latitude,longitude)

    figure.update_layout(
        autosize=False,
        xaxis=dict(
            title="Latitude (ª)",
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        yaxis=dict(
            title="Longitude (ª)",
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        height=1000,
        width=1500,
        bargap=0.1,
        showlegend=True,
        title=Title,
        map=dict(
            center=go.layout.map.Center(
                lat=centroid[0],
                lon=centroid[1],
            ),
            zoom=15,
            style="satellite",
        )
    )

    return figure

if __name__ == '__main__':

    speed = 10

    # Line with folder in which measures are saved:
    analysis_repo_folder = "D:\\DronePerformance\\Data\\FPAlguaireEstiu2024"
    # traj_repo_folder = "D:\\U-ELCOME\\AlguaireEstiu2024\\17-08-2024-20240905T110402Z-001\\17-08-2024"
    operationId = "FPs"
    result_folder = "Results"

    analysis_folder = analysis_repo_folder + "\\" + operationId

    fps_list = GetFiles(analysis_folder)
    print("Number of DJI files to analyze: ", len(fps_list), "\n")
    print("Analyzing trajectories at folder: \n\t\t", analysis_folder, "\n")

    TimeAnalysis = ta.TimeAnalysis()

    TimeAnalysis.fpFolder = analysis_folder
    TimeAnalysis.fpList = fps_list

    if os.path.exists(analysis_repo_folder + "\\" + result_folder):
        shutil.rmtree(analysis_repo_folder + "\\" + result_folder)
    os.mkdir(analysis_repo_folder + "\\" + result_folder)

    for fp_pack in TimeAnalysis.fpList:
        print("Analyzing trajectory:\t", fp_pack[1], "\n")

        dfWaypoints = pc.readFPFile(fp_pack[0] + "\\" + fp_pack[1])

        fig = generateAccelerationTrajectoryPlotComplete(dfWaypoints['latitude'],
                                                         dfWaypoints['longitude'],
                                                         "Acceleration Coordinates Plot")

        file_suffix = pathlib.Path(fp_pack[1]).stem

        filename = analysis_repo_folder + "\\" + result_folder + "\\" + file_suffix + "-Waypoints.html"
        print("Generating figure: " + filename)
        plot(fig, filename=filename, auto_open=False)

        totalDistance = 0

        for index, row in dfWaypoints.iterrows():
            if index<len(dfWaypoints)-1:
                coord1 = (row['latitude'], row['longitude'])
                coord2 = (dfWaypoints.iloc[index+1]['latitude'], dfWaypoints.iloc[index+1]['longitude'])
                distance = geodesic(coord1, coord2).meters
                totalDistance = totalDistance + distance
        time = totalDistance/speed

        print("====================== Summary FP " + file_suffix + " ======================")
        print("Total FP distance: " + str(totalDistance) + " meters.")
        print("Time spent: " + str(time) + " seconds at " + str(speed) + " m/s.")
        print("Done.")


