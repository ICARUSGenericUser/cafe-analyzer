import xml.etree.ElementTree as ET
import pandas as pd

def readFPFile(filePath):
    print("Reading FP coordinates...")

    kml_file = filePath  # replace with your KML file path
    tree = ET.parse(kml_file)
    root = tree.getroot()

    # Extract coordinates from <Placemark> elements inside <Folder>
    coordinates_list = []

    # Find all Placemark elements inside Folder
    placemarks = root.findall('.//Folder/Placemark')

    # Prepare a list to store the coordinates
    coordinates_list = []

    # Extract coordinates from each Placemark
    for placemark in placemarks:
        coordinates = placemark.find('.//coordinates').text

        # Split the coordinates (longitude, latitude, altitude)
        lon, lat, alt = coordinates.split(',')

        speed = 0
        speed_data = placemark.find('.//{www.dji.com}speed')

        if speed_data is not None:
            speed = speed_data.text

        radius = 0
        radius_data = placemark.find('.//{www.dji.com}cornerRadius')

        if radius_data is not None:
            radius = radius_data.text

        # Append the coordinates as a dictionary (or tuple)
        coordinates_list.append({'longitude': float(lon), 'latitude': float(lat), 'altitude': float(alt), 'speed': int(speed), 'radius': float(radius)})

    # Convert the list of dictionaries to a DataFrame
    df = pd.DataFrame(coordinates_list)

    return df