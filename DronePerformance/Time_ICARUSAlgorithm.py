import math
import pathlib

import pandas as pd

from os.path import isfile, join
from os import listdir
from DronePerformance import ParserController as pc
from DronePerformance import TimeAnalysis as ta

from geopy.distance import geodesic

dronePerformanceCols = [
    "Drone",
    "acceleration",
    "deceleration"
]


def GetFiles(traj_folder):
    file_list = []
    onlyfilesList = []
    onlyfolders = listdir(traj_folder)

    for folder in onlyfolders:
        oppath = join(traj_folder, folder)
        onlyfiles = [f for f in listdir(oppath) if isfile(join(oppath, f))]

        for file in onlyfiles:
            pack = [oppath, file]
            onlyfilesList.append(pack)

    for pack in onlyfilesList:
        splitter = pack[1].split("-")
        if ((splitter[0] == 'FP')):
            file_list.append(pack)
        if ((splitter[0] == 'DJI') and (splitter[1] == 'FLOG')):
            file_list.append(pack)

    return file_list


def segmentTime(coordinate1, speed1, coordinate2, speed2, acceleration, deceleration, typeOfFlight):

    if typeOfFlight == 1:
        delta_x1 = abs((speed2 * speed2)-(speed1 * speed1)) / (2 * acceleration)
        delta_t1 = (2 * delta_x1) / (speed2+speed1)
        delta_x2 = abs(((speed2/2.5) * (speed2/2.5))-(speed2 * speed2)) / (2 * deceleration)
        delta_t2 = (2 * delta_x2) / ((speed2/2.5) + speed2)
    else:
        delta_x1 = (speed2 * speed2) / (2 * acceleration)
        delta_t1 = (2 * delta_x1) / (speed2)
        delta_x2 = (speed2 * speed2) / (2 * deceleration)
        delta_t2 = (2 * delta_x2) / (speed2)

    d_min = delta_x1 + delta_x2

    totalDistance = geodesic(coordinate1, coordinate2).meters

    totalTime = 0

    if totalDistance <= d_min:
        print("Not enough distance")
        v_max = math.sqrt((2 * acceleration * totalDistance) / (acceleration + deceleration))
        delta_t1 = v_max / acceleration
        delta_t2 = v_max / deceleration

        totalTime = delta_t1 + delta_t2
    else:
        print("Enough distance")
        d_ct = totalDistance - d_min
        t_ct = d_ct / speed2

        totalTime = delta_t1 + delta_t2 + t_ct

    return totalTime


if __name__ == '__main__':
    # Line with folder in which measures are saved:
    analysis_repo_folder = "D:\\DronePerformance\\Data\\FPAlguaireEstiu2024"
    # traj_repo_folder = "D:\\U-ELCOME\\AlguaireEstiu2024\\17-08-2024-20240905T110402Z-001\\17-08-2024"
    operationId = "FPs"
    result_folder = "ResultsFromDJILogs"

    analysis_folder = analysis_repo_folder + "\\" + operationId

    fps_list = GetFiles(analysis_folder)
    print("Number of DJI files to analyze: ", len(fps_list), "\n")
    print("Analyzing trajectories at folder: \n\t\t", analysis_folder, "\n")

    TimeAnalysis = ta.TimeAnalysis()

    TimeAnalysis.fpFolder = analysis_folder
    TimeAnalysis.fpList = fps_list

    for fp_pack in TimeAnalysis.fpList:
        dfWaypoints = pc.readFPFile(fp_pack[0] + "\\" + fp_pack[1])
        dfDronePerformance = pd.read_csv(
            "C:\\Users\\alber\\OneDrive\\Desktop\\Sources\\cafe-analyzer\\DronePerformance\\performanceParameters.csv",
            delimiter=';', index_col=False, header=None, names=dronePerformanceCols)
        for indexDrone, rowDrone in dfDronePerformance.iterrows():

            totalTime = 0
            for index, row in dfWaypoints.iterrows():
                if index < len(dfWaypoints) - 1:
                    coord1 = (row['latitude'], row['longitude'])
                    coord2 = (dfWaypoints.iloc[index + 1]['latitude'], dfWaypoints.iloc[index + 1]['longitude'])
                    typeOfFlight = 2
                    if(rowDrone['Drone']=="M300"):
                        typeOfFlight = 1
                    else:
                        typeOfFlight = 2
                    time = segmentTime(coord1, row['speed'], coord2, dfWaypoints.iloc[index + 1]['speed'],
                                       float(rowDrone['acceleration']), float(rowDrone['deceleration']), typeOfFlight)
                    totalTime = totalTime + time

            file_suffix = pathlib.Path(fp_pack[1]).stem
            print("====================== Summary FP " + file_suffix + " for drone " + rowDrone['Drone'] + "======================")
            print("Time spent: " + str(totalTime) + " seconds.")
            print("Done.")
