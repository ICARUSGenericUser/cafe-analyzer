import re
import os, shutil
import pathlib

import pandas as pd
import plotly.graph_objects as go
from plotly.offline import plot

from os.path import isfile, join
from os import listdir
import Mocca_Analyzer.Mocca.Mocca_Object as mo
from DronePerformance import ParserController as pc
from DronePerformance import TimeAnalysis as ta
from Mocca_Analyzer.Performance_Analyzer import PerformanceAnalysis as pa
from Mocca_Analyzer.Mocca import TelemetryReader as tr

from geopy.distance import geodesic


def GetFiles(traj_folder):
    file_list = []
    onlyfilesList = []
    onlyfolders = listdir(traj_folder)

    for folder in onlyfolders:
        oppath = join(traj_folder, folder)
        onlyfiles = [f for f in listdir(oppath) if isfile(join(oppath, f))]

        for file in onlyfiles:
            pack = [oppath, file]
            onlyfilesList.append(pack)

    for pack in onlyfilesList:
        splitter = pack[1].split("-")
        if ((splitter[0] == 'FP')):
            file_list.append(pack)
        if ((splitter[0] == 'DJI') and (splitter[1] == 'FLOG')):
            file_list.append(pack)

    return file_list


def calculate_centroid_from_series(xs: pd.Series, ys: pd.Series):
    """
    Calculate the centroid of a set of 2D points provided as two pandas Series.

    Parameters:
    xs (pd.Series): A pandas Series representing the x-coordinates.
    ys (pd.Series): A pandas Series representing the y-coordinates.

    Returns:
    tuple: The (x, y) coordinates of the centroid.
    """
    if len(xs) != len(ys):
        raise ValueError("The length of the x and y series must be the same.")

    # Calculate the centroid
    centroid_x = xs.mean()
    centroid_y = ys.mean()

    return centroid_x, centroid_y


def generateAccelerationTrajectoryPlotComplete(latitude, longitude, fileLatitude, fileLongitude, timestamp, Title):
    figure = go.Figure()

    figure.add_trace(
        go.Scattermap(
            lat=fileLatitude,
            lon=fileLongitude,
            text=timestamp,
            textfont=dict(size=18, color="white", weight=500),
            mode='markers+lines',
            marker=go.scattermap.Marker(
                size=14,
                color='rgb(0,0,255)',
                opacity=0.6
            )
        )
    )
    figure.add_trace(
        go.Scattermap(
            lat=latitude,
            lon=longitude,
            mode='markers+lines',

            marker=go.scattermap.Marker(
                size=14,
                color='rgb(255,0,0)',
                opacity=1
            )
        )
    )

    centroid = calculate_centroid_from_series(latitude, longitude)

    figure.update_layout(
        autosize=False,
        xaxis=dict(
            title="Latitude (ª)",
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        yaxis=dict(
            title="Longitude (ª)",
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        height=1000,
        width=1500,
        bargap=0.1,
        showlegend=True,
        title=Title,
        map=dict(
            center=go.layout.map.Center(
                lat=centroid[0],
                lon=centroid[1],
            ),
            zoom=15,
            style="satellite",
        )
    )

    return figure


if __name__ == '__main__':

    traj_repo_folder = "D:\\DronePerformance\\Data\\16-09-2024"
    # traj_repo_folder = "D:\\U-ELCOME\\AlguaireEstiu2024\\17-08-2024-20240905T110402Z-001\\17-08-2024"
    operationId = "Telemetry"

    traj_folder = traj_repo_folder + "\\" + operationId

    dji_trajectory_file_list = GetFiles(traj_folder)
    print("Number of DJI files to analyze: ", len(dji_trajectory_file_list), "\n")
    print("Analyzing trajectories at folder: \n\t\t", traj_folder, "\n")

    TrajAnalysis = pa.PerformanceAnalysis()

    TrajAnalysis.trajectoryFolder = traj_folder
    TrajAnalysis.trajectoryList = dji_trajectory_file_list

    # Line with folder in which measures are saved:
    analysis_repo_folder = "D:\\DronePerformance\\Data\\FPAlguaireEstiu2024"
    # traj_repo_folder = "D:\\U-ELCOME\\AlguaireEstiu2024\\17-08-2024-20240905T110402Z-001\\17-08-2024"
    operationId = "FPs"
    result_folder = "ResultsFromDJILogs"

    analysis_folder = analysis_repo_folder + "\\" + operationId

    fps_list = GetFiles(analysis_folder)
    print("Number of DJI files to analyze: ", len(fps_list), "\n")
    print("Analyzing trajectories at folder: \n\t\t", analysis_folder, "\n")

    TimeAnalysis = ta.TimeAnalysis()

    TimeAnalysis.fpFolder = analysis_folder
    TimeAnalysis.fpList = fps_list

    if os.path.exists(analysis_repo_folder + "\\" + result_folder):
        shutil.rmtree(analysis_repo_folder + "\\" + result_folder)
    os.mkdir(analysis_repo_folder + "\\" + result_folder)

    for trajectory_pack in TrajAnalysis.trajectoryList:

        print("Analyzing trajectory:\t", trajectory_pack[1], "\n")

        telSequence = tr.readDJITelemetryFile(trajectory_pack[0] + "\\" + trajectory_pack[1])

        LatitudeArray, LongitudeArray, TimestampArray = [], [], []

        for sample in telSequence:
            if sample.type == mo.Object_Type.djiGNSSIMUNavigation:
                latitude = sample.latitude
                LatitudeArray.append(latitude)
                longitude = sample.longitude
                LongitudeArray.append(longitude)
                timestamp = sample.timestamp / 1000
                TimestampArray.append(timestamp)

        dfFileWaypoints = pd.DataFrame(
            {'Latitude': LatitudeArray, 'Longitude': LongitudeArray, 'Timestamp': TimestampArray})

        cleanFPList = [item[1].replace("FP-", "").replace(".kml", "") for item in TimeAnalysis.fpList]

        pattern = r"-DAS-\w+"
        pattern2 = r"^SIM-|-M[\w\d]+$"

        if re.sub(pattern2,"",re.sub(pattern, "", trajectory_pack[1].replace("DJI-FLOG-", "").replace(".dat", ""))) in cleanFPList:
            index = cleanFPList.index(
                re.sub(pattern2,"",re.sub(pattern, "", trajectory_pack[1].replace("DJI-FLOG-", "").replace(".dat", ""))))
            fp_pack = TimeAnalysis.fpList[index]
            print("Analyzing trajectory:\t", fp_pack[1], "\n")

            dfWaypoints = pc.readFPFile(fp_pack[0] + "\\" + fp_pack[1])

            fig = generateAccelerationTrajectoryPlotComplete(dfWaypoints['latitude'],
                                                             dfWaypoints['longitude'],
                                                             dfFileWaypoints['Latitude'],
                                                             dfFileWaypoints['Longitude'],
                                                             dfFileWaypoints['Timestamp'],
                                                             "Acceleration Coordinates Plot")

            file_suffix = pathlib.Path(trajectory_pack[1]).stem

            filename = analysis_repo_folder + "\\" + result_folder + "\\" + file_suffix + "-Waypoints.html"
            print("Generating figure: " + filename)
            plot(fig, filename=filename, auto_open=False)

            firstWP = dfWaypoints.iloc[0]
            secondWP = dfWaypoints.iloc[1]
            beforeLastWP = dfWaypoints.iloc[-2]
            lastWP = dfWaypoints.iloc[-1]


            dfFileWaypoints['distanceToSecondWP'] = dfFileWaypoints.apply(
                lambda row: geodesic((secondWP['latitude'], secondWP['longitude']),
                                     (row['Latitude'], row['Longitude'])).meters, axis=1)
            dfFileWaypoints['distanceToBeforeLastWP'] = dfFileWaypoints.apply(
                lambda row: geodesic((beforeLastWP['latitude'], beforeLastWP['longitude']),
                                     (row['Latitude'], row['Longitude'])).meters, axis=1)

            secondFileWP = dfFileWaypoints.loc[dfFileWaypoints['distanceToSecondWP'].idxmin()]
            beforeLastFileWP = dfFileWaypoints.loc[dfFileWaypoints['distanceToBeforeLastWP'].idxmin()]

            dfFileWaypoints['distanceToFirstWP'] = dfFileWaypoints.apply(
                lambda row: geodesic((firstWP['latitude'], firstWP['longitude']),
                                     (row['Latitude'], row['Longitude'])).meters, axis=1)
            dfFileWaypoints['distanceToLastWP'] = dfFileWaypoints.apply(
                lambda row: geodesic((lastWP['latitude'], lastWP['longitude']),
                                     (row['Latitude'], row['Longitude'])).meters, axis=1)
            dfFileWaypoints['timeDifference2ndWP'] = (dfFileWaypoints['Timestamp'] - secondFileWP['Timestamp']).abs()
            dfFileWaypoints['timeDifferenceBeforeLastWP'] = (
                        dfFileWaypoints['Timestamp'] - beforeLastFileWP['Timestamp']).abs()
            df_filtered = dfFileWaypoints[dfFileWaypoints['timeDifference2ndWP']>0]
            df_filtered = df_filtered[dfFileWaypoints['timeDifferenceBeforeLastWP'] > 0]

            minTimeWP2 = df_filtered.loc[df_filtered['timeDifference2ndWP'].idxmin()]
            minDistanceWP1 = df_filtered.loc[df_filtered['distanceToFirstWP'].idxmin()]
            minTimeWP_2 = df_filtered.loc[df_filtered['timeDifferenceBeforeLastWP'].idxmin()]
            minDistanceWP_1 = df_filtered.loc[df_filtered['distanceToLastWP'].idxmin()]

            df_filteredDistance = df_filtered[df_filtered['distanceToFirstWP'] <= 2]
            df_sorted = df_filteredDistance.sort_values(by=['timeDifference2ndWP'])
            firstFileWP = df_sorted.iloc[0]
            df_filteredDistance = df_filtered[df_filtered['distanceToLastWP'] <= 2]
            df_sorted = df_filteredDistance.sort_values(by=['timeDifferenceBeforeLastWP'])
            lastFileWP = df_sorted.iloc[0]

            time = lastFileWP['Timestamp'] - firstFileWP['Timestamp']
            print("====================== Summary FP " + file_suffix + " ======================")
            print("Time spent: " + str(time) + " seconds.")
            print("Done.")
