import math
import numpy as np
import pathlib

import pandas as pd

from os.path import isfile, join
from os import listdir
from DronePerformance import ParserController as pc
from DronePerformance import TimeAnalysis as ta

from geopy.distance import geodesic

dronePerformanceCols = [
    "Drone",
    "acceleration",
    "deceleration"
]


def GetFiles(traj_folder):
    file_list = []
    onlyfilesList = []
    onlyfolders = listdir(traj_folder)

    for folder in onlyfolders:
        oppath = join(traj_folder, folder)
        onlyfiles = [f for f in listdir(oppath) if isfile(join(oppath, f))]

        for file in onlyfiles:
            pack = [oppath, file]
            onlyfilesList.append(pack)

    for pack in onlyfilesList:
        splitter = pack[1].split("-")
        if ((splitter[0] == 'FP')):
            file_list.append(pack)
        if ((splitter[0] == 'DJI') and (splitter[1] == 'FLOG')):
            file_list.append(pack)

    return file_list


def latlon_to_cartesian(lat, lon):
    """Convert latitude and longitude to Cartesian coordinates."""
    lat_rad = math.radians(lat)
    lon_rad = math.radians(lon)

    x = math.cos(lat_rad) * math.cos(lon_rad)
    y = math.cos(lat_rad) * math.sin(lon_rad)
    z = math.sin(lat_rad)

    return np.array([x, y, z])


def calculate_smaller_angle(coord1, coord2, coord3):
    a = geodesic(coord1, coord2).meters
    b = geodesic(coord2, coord3).meters
    c = geodesic(coord3, coord1).meters

    # Calculate the cosine of the angle
    cos_C = (a ** 2 + b ** 2 - c ** 2) / (2 * a * b)

    # Clamp the value to avoid floating-point errors
    cos_C = np.clip(cos_C, -1.0, 1.0)

    # Calculate the angle in radians and convert to degrees
    angle_radians = np.arccos(cos_C)
    angle_degrees = np.degrees(angle_radians)

    return angle_radians


if __name__ == '__main__':
    # Line with folder in which measures are saved:
    analysis_repo_folder = "D:\\DronePerformance\\Data\\FPAlguaireEstiu2024"
    # traj_repo_folder = "D:\\U-ELCOME\\AlguaireEstiu2024\\17-08-2024-20240905T110402Z-001\\17-08-2024"
    operationId = "FPs"
    result_folder = "ResultsFromDJILogs"

    analysis_folder = analysis_repo_folder + "\\" + operationId

    fps_list = GetFiles(analysis_folder)
    print("Number of DJI files to analyze: ", len(fps_list), "\n")
    print("Analyzing trajectories at folder: \n\t\t", analysis_folder, "\n")

    TimeAnalysis = ta.TimeAnalysis()

    TimeAnalysis.fpFolder = analysis_folder
    TimeAnalysis.fpList = fps_list

    for fp_pack in TimeAnalysis.fpList:
        dfWaypoints = pc.readFPFile(fp_pack[0] + "\\" + fp_pack[1])
        dfDronePerformance = pd.read_csv(
            "C:\\Users\\alber\\OneDrive\\Desktop\\Sources\\cafe-analyzer\\DronePerformance\\performanceParameters.csv",
            delimiter=';', index_col=False, header=None, names=dronePerformanceCols)
        for indexDrone, rowDrone in dfDronePerformance.iterrows():

            totalTime = 0
            t_diff = 0
            for index, row in dfWaypoints.iterrows():
                if index < len(dfWaypoints) - 2:
                    coord1 = (row['latitude'], row['longitude'])
                    coord2 = (dfWaypoints.iloc[index + 1]['latitude'], dfWaypoints.iloc[index + 1]['longitude'])
                    coord3 = (dfWaypoints.iloc[index + 2]['latitude'], dfWaypoints.iloc[index + 2]['longitude'])
                    angle = calculate_smaller_angle(coord1, coord2, coord3)
                    d_direct = geodesic(coord1, coord2).meters
                    d_rect = math.sqrt(d_direct**2 - 2*d_direct*row['radius']*(1-math.cos(angle))+row['radius']**2)
                    t_rect = d_rect/dfWaypoints.iloc[index + 1]['speed'] - t_diff
                    d_turn = row['radius']*angle
                    t_turn = d_turn/(dfWaypoints.iloc[index + 1]['speed']-5)
                    totalTime = totalTime + t_rect + t_turn
                    d_diff = d_direct-d_rect
                    t_diff = d_diff/dfWaypoints.iloc[index + 1]['speed']

            file_suffix = pathlib.Path(fp_pack[1]).stem
            print("====================== Summary FP " + file_suffix + " for drone " + rowDrone[
                'Drone'] + "======================")
            print("Time spent: " + str(totalTime) + " seconds.")
            print("Done.")
