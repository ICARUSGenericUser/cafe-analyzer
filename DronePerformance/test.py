import math
import numpy as np


def latlon_to_cartesian(lat, lon):
    """Convert latitude and longitude to Cartesian coordinates."""
    lat_rad = math.radians(lat)
    lon_rad = math.radians(lon)

    x = math.cos(lat_rad) * math.cos(lon_rad)
    y = math.cos(lat_rad) * math.sin(lon_rad)
    z = math.sin(lat_rad)

    return np.array([x, y, z])


def calculate_internal_angle(latA, lonA, latB, lonB, latC, lonC):
    """Calculate the internal angle between two segments AB and BC defined by latitude and longitude points."""
    # Convert lat/lon to Cartesian coordinates
    A = latlon_to_cartesian(latA, lonA)
    B = latlon_to_cartesian(latB, lonB)
    C = latlon_to_cartesian(latC, lonC)

    # Calculate vectors AB and BC
    AB = B - A
    BC = C - B

    # Calculate the dot product and magnitudes
    dot_product = np.dot(AB, BC)
    magnitude_AB = np.linalg.norm(AB)
    magnitude_BC = np.linalg.norm(BC)

    # Calculate the cosine of the angle
    cos_theta = dot_product / (magnitude_AB * magnitude_BC)

    # Clamp the value to avoid floating-point errors
    cos_theta = np.clip(cos_theta, -1.0, 1.0)

    # Calculate the angle in radians and convert to degrees
    angle_radians = np.arccos(cos_theta)
    angle_degrees = np.degrees(angle_radians)

    return angle_degrees


# Given coordinates (latitude, longitude)
latA, lonA = 41.72794419027444, 0.5466769993310417  # Point A
latB, lonB = 41.72940971822269, 0.5486451456552067  # Point B
latC, lonC = 41.73298906159976, 0.5490962602499544  # Point C

# Calculate the internal angle
internal_angle = calculate_internal_angle(latA, lonA, latB, lonB, latC, lonC)
print(f"The internal angle between segments AB and BC is: {internal_angle:.2f} degrees")
