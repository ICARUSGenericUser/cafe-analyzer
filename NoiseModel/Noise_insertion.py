from Encounter import units as units
from NCSensor import NCS_detection as ncd
from scipy.stats import nct as nctpd

''' Parameters
nu: float Degrees of freedom, also known as normality parameter (nu > 0).
mu: float Location parameter.
sigma: float Scale parameter (sigma > 0).Converges to the standard deviation as nu increases.(only required if lam is not specified)
lam: float Scale parameter (lam > 0).Converges to the precision as nu increases.(only required if sigma is not specified)
'''
class AltimeterJitter:
    def __init__(self, p1, p2, p3):
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3
        self.rv = nctpd.nct(self.p1, self.p2)


    def getSample(self):
        self.rv.rsv()


def insertGnssAltimetryError(aircraft, noiseParameters):

    #
    # Sets up a new random seed specific for inserting noise to this trajectory
    noiseParameters.setSeedForSequenceAnalysis()

    # Iterates over all positions and adds the corresponding noise
    for i in range(len(aircraft)):
        aircraft[i].toa_offset += 1
        #result.append((aircraft[i].time, aircraft[i].id, round(units.m_to_nm(aircraft[i].x_loc), 7), round(units.m_to_nm(aircraft[i].y_loc), 7), round(units.m_to_ft(aircraft[i].altitude), 7)), aircraft[i].toa_offset)


#
# Add Atar error to a single set of range, azimuth and elevation
def addAtarError(detection, noiseParameters):
    detectionError = ncd.NCS_detection()

    return detectionError

#
# Insert atar error to an intruder trajectory based on the ownship true position
def insertAtarError(aircraft1, aircraft2, noiseParameters):

    #
    # Sets up a new random seed specific for inserting noise to this trajectory
    noiseParameters.setSeedForSequenceAnalysis()

    # Iterates over all positions and adds the corresponding noise
    for i in range(len(aircraft1)):
        detection = ncd.detect_intruder(aircraft1[i], aircraft2[i])

        # Add error to detection (range, azimuth, elevation)
        detectionError = addAtarError(detection, noiseParameters)

        detection = ncd.update_intruder(aircraft1[i], aircraft2[i], detection, detectionError)



