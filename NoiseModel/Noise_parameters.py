import json
import numpy as np
import time



class Noise_parameters:

    def __init__(self):
        self.name = None
        self.version = None
        self.description = None
        self.parameter1 = 0
        self.parameter2 = 0
        self.initialSeed = 0
        self.currentSeed = 0


    # Sets up the initial seed
    def setInitialSeed(self, seed):
        self.initialSeed = seed
        self.currentSeed = seed
        self.updateSeedForNextEncounter()


    # An individual seed is used for each encounter
    # This seed is used to generate further "local" seed for each noise injection sequence
    # This function recover the encounter seed, generates a new one by sampling, and stores it
    def updateSeedForNextEncounter(self):
        np.random.seed(self.currentSeed)
        np.random.random()
        self.currentSeed = np.random.get_state()[1][0]


    # Sets a seed for the noise injection sequence based on the encounter seed and an additional randomizing factor
    def setSeedForSequenceAnalysis(self):
        tmpTime = int(time.time() * 10000.0) & 0xffffff
        tmpSeed = (self.currentSeed + tmpTime).astype(np.uint32)
        np.random.seed(tmpSeed)


    def parseFromJson(self, filename):
        complete = True

        print("Loading noise parameters from file: ", filename)
        f = open(filename, "r")
        parametersTable = json.load(f)

        if 'name' in parametersTable:
            self.name = parametersTable['name']

        if 'version' in parametersTable:
            self.version = parametersTable['version']

        if 'description' in parametersTable:
            self.description = parametersTable['description']

        if 'parameter1' in parametersTable:
            self.parameter1 = parametersTable['parameter1']

        if 'parameter2' in parametersTable:
            self.parameter2 = parametersTable['parameter2']

        return complete