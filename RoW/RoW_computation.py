import numpy as np
import math as math
from Util import angles as ang

def RoWDetermination(own_pos, int_pos):

    relVector = np.array([int_pos.x_loc - own_pos.x_loc, int_pos.y_loc - own_pos.y_loc])
    module = math.sqrt(relVector[0] ** 2 + relVector[1] ** 2)

    dot = own_pos.x_v * relVector[0] + own_pos.y_v * relVector[1]
    det = own_pos.x_v * relVector[1] - own_pos.y_v * relVector[0]
    #Angle between LOS & velocity vector of ownship
    theta1 = ang.normalize((180 * math.atan2(det, dot) / math.pi))

    dot2 = int_pos.x_v * relVector[0] + int_pos.y_v * relVector[1]
    det2 = int_pos.x_v * relVector[1] - int_pos.y_v * relVector[0]
    #Angle between LOS & velocity vector of intruder
    theta2 = ang.normalize((180 * math.atan2(det2, dot2) / math.pi))

    # Defining phi=0 as the ownship has RoW and phi=1 if the intruder has RoW

    if 270 <= theta1 < 360:
        return False

    elif 0 <= theta1 <= 90 and 0 <= theta2 <= 90:
        return False

    elif 0 <= theta1 <= 90 and 180 <= theta2 < 360:
        return False

    else:
        return True

