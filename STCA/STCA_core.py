import numpy as np
import math



# RVSM control variables
Vsep = np.array([1000, 2000])
# ATC vertical separation rule boundary (lower) [ft]
LowerSeparationFlightLevel = 29300
# ATC vertical separation rule boundary (upper) [ft]
UpperSeparationFlightLevel = 41300

# Coarse filter prediction (or look ahead) time [s]
CoarseFilterPredictionTime = 120
# Coarse filter horizontal separation threshold [m]
CoarseFilterHorizontalSeparation = 5 * 1852
# Coarse filter vertical separation threshold [ft]
CoarseFilterVerticalSeparation = np.array([1000*0.3048, 2000*0.3048])

# fast diverging conditions
HorizontalFastDivergingVelocity = np.array([200*0.5144, 200*0.5144])
HorizontalFastDivergingSeparation = np.array([2.5*1852, 1.5*1852])
VerticalFastDivergingVelocity = np.array([1000*0.00508, 1000*0.00508])
VerticalFastDivergingSeparation = np.array([[650*0.3048, 1600*0.3048], [650*0.3048, 0]])

LinearPredictionHorizontalSeparation = np.array([4.5*1852, 2.75*1852])
LinearPredictionVerticalSeparation = np.array([[750*0.3048, 1750*0.3048], [750*0.3048,0]])
LinearPredictionTime = np.array([120, 120])
LinearPredictionImminentTime = np.array([50,50])
LinearPredictionWarningTime = np.array([90,80])

LinearPredictionConflictCount = np.array([2, 2])
LinearPredictionCycleCount = np.array([4, 4])



# Different separation may be applied to tracks after they are predicted to be horizontally diverging,
# to reflect the reduced risk of conflict for these tracks.
# TODO take into account diverging particularities
LinearPredictionHorizontalSeparationDiverging = np.array([0])

def sign(x):
    if (x >= 0):
        return 1
    return -1

# Computes times when sz, vz intersects rectangle of half height H
# eps = -1: Entry
# eps = 1: Exit
def Theta_H(sz, vz, eps, H):
    if (math.isclose(vz, 0)):
        return np.nan
    return (eps * sign(vz) * H - sz) / vz


def time_coalt(sz, vz):
    if (math.isclose(vz, 0)):
        return np.nan
    return -sz / vz

class STCA_core:

    # filters
    def __init__(self, region):
        self.region = region
        self.TurningPredictionFilter = True
        self.ProximityFilter = True
        self.LinearPredictionFilter = True
        self.tau_pca = 0
        self.tau_ths = 0
        self.tau_the = 0
        self.tau_vert_cpa = 0
        self.tau_vert_entry = 0
        self.tau_vert_exit = 0
        self.tau_conf_entry = 0
        self.tau_conf_exit = 0
        self.lastLinearPredictionConflictIdx = 0
        self.lastLinearPredictionConflict = np.full(LinearPredictionCycleCount[region],0)
        self.so_hor_pca = np.array([0, 0, 0])
        self.si_hor_pca = np.array([0, 0, 0])
        self.so_hor_ths = np.array([0, 0, 0])
        self.si_hor_ths = np.array([0, 0, 0])
        self.so_hor_the = np.array([0, 0, 0])
        self.si_hor_the = np.array([0, 0, 0])
        self.so_vert_pca = np.array([0, 0, 0])
        self.si_vert_pca = np.array([0, 0, 0])
        self.so_vert_ths = np.array([0, 0, 0])
        self.si_vert_ths = np.array([0, 0, 0])
        self.so_vert_the = np.array([0, 0, 0])
        self.si_vert_the = np.array([0, 0, 0])
        self.so_conf_entry = np.array([0, 0, 0])
        self.si_conf_entry = np.array([0, 0, 0])
        self.so_conf_exit = np.array([0, 0, 0])
        self.si_conf_exit = np.array([0, 0, 0])


    def clean_object(self):
        self.tau_pca = 0
        self.tau_ths = 0
        self.tau_the = 0
        self.tau_vert_cpa = 0
        self.tau_vert_entry = 0
        self.tau_vert_exit = 0
        self.tau_conf_entry = 0
        self.tau_conf_exit = 0
        self.so_hor_pca = np.array([0, 0, 0])
        self.si_hor_pca = np.array([0, 0, 0])
        self.so_hor_ths = np.array([0, 0, 0])
        self.si_hor_ths = np.array([0, 0, 0])
        self.so_hor_the = np.array([0, 0, 0])
        self.si_hor_the = np.array([0, 0, 0])
        self.so_vert_pca = np.array([0, 0, 0])
        self.si_vert_pca = np.array([0, 0, 0])
        self.so_vert_ths = np.array([0, 0, 0])
        self.si_vert_ths = np.array([0, 0, 0])
        self.so_vert_the = np.array([0, 0, 0])
        self.si_vert_the = np.array([0, 0, 0])
        self.so_conf_entry = np.array([0, 0, 0])
        self.si_conf_entry = np.array([0, 0, 0])
        self.so_conf_exit = np.array([0, 0, 0])
        self.si_conf_exit = np.array([0, 0, 0])


    def incLastLinearPredictionConflict(self, val):
        self.lastLinearPredictionConflictIdx = (self.lastLinearPredictionConflictIdx + 1) % len(self.lastLinearPredictionConflict)
        self.lastLinearPredictionConflict[self.lastLinearPredictionConflictIdx] = val



    def compute_horizontalPrediction(self, so, si, vo, vi):
        # relative lateral position
        r = si[0:2]-so[0:2]
        # relative position module
        rMod = np.linalg.norm(r)
        # relative horizontal speed
        c = vi[0:2]-vo[0:2]
        cMod = np.linalg.norm(c)
        # relative horizontal speed module
        if np.isclose(cMod,0):
            # aircraft are moving at the same velocity. Check relative position nd exit
            if rMod < LinearPredictionHorizontalSeparation[self.region]:
                self.tau_pca = 0
                self.tau_ths = 0 # -np.inf
                self.tau_the = np.inf
                return True
            else:
                return False
        cNorm = c/cMod
        # calculate miss distance
        rm = np.cross(r.T, cNorm.T)
        #rm = np.cross(cNorm, np.cross(r, cNorm))
        md = np.linalg.norm(rm)
        dz = si[2]-so[2]
        # calculate time to closest point of approach
        self.tau_pca = -np.dot(r,c)/np.dot(c,c)
        # check time horizon
        timeHorizon = rMod/cMod
        if timeHorizon > LinearPredictionTime[self.region]:
            return False
        # check whether the conflict is in the past or in the future
        if self.tau_pca < 0:
            return False
        # check horizontal distance threshold
        #if md > CoarseFilterHorizontalSeparation:
        if md > LinearPredictionHorizontalSeparation[self.region]:
            return False

        self.tau_ths = self.tau_pca - np.sqrt(LinearPredictionHorizontalSeparation[self.region]**2 - md**2)/cMod
        #self.tau_ths = self.tau_pca - LinearPredictionHorizontalSeparation[self.region] / cMod
        # TODO check if this is correct!!!!
        self.tau_the = self.tau_pca + np.sqrt(LinearPredictionHorizontalSeparation[self.region]**2 - md**2)/cMod
        #self.tau_the = self.tau_pca + LinearPredictionHorizontalSeparation[self.region] / cMod

        self.so_hor_pca = [so[0] + vo[0] * self.tau_pca, so[1] + vo[1] * self.tau_pca, so[2] + vo[2] * self.tau_pca]
        self.si_hor_pca = [si[0] + vi[0] * self.tau_pca, si[1] + vi[1] * self.tau_pca, si[2] + vi[2] * self.tau_pca]
        self.so_hor_ths = [so[0] + vo[0] * self.tau_ths, so[1] + vo[1] * self.tau_ths, so[2] + vo[2] * self.tau_ths]
        self.si_hor_ths = [si[0] + vi[0] * self.tau_ths, si[1] + vi[1] * self.tau_ths, si[2] + vi[2] * self.tau_ths]
        self.so_hor_the = [so[0] + vo[0] * self.tau_the, so[1] + vo[1] * self.tau_the, so[2] + vo[2] * self.tau_the]
        self.si_hor_the = [si[0] + vi[0] * self.tau_the, si[1] + vi[1] * self.tau_the, si[2] + vi[2] * self.tau_the]

        return True


    def compute_verticalPrediction(self, so, si, vo, vi, vsep):
        sz = np.abs(si[2] - so[2])
        vz = vi[2] - vo[2]
        self.tau_vert = time_coalt(sz, vz)
        if np.isnan(self.tau_vert):
            if sz < LinearPredictionVerticalSeparation[self.region, vsep]:
                #permanently in conflict
                self.tau_vert_entry = -np.inf
                self.tau_vert_exit = np.inf
                return True
            else:
                #permanently NOT in conflict
                return False
        else:
            self.tau_vert_entry = Theta_H(sz, vz, -1, LinearPredictionVerticalSeparation[self.region,vsep])
            self.tau_vert_exit = Theta_H(sz, vz, 1, LinearPredictionVerticalSeparation[self.region,vsep])

            self.so_vert_pca = [so[0] + vo[0] * self.tau_vert, so[1] + vo[1] * self.tau_vert, so[2] + vo[2] * self.tau_vert]
            self.si_vert_pca = [si[0] + vi[0] * self.tau_vert, si[1] + vi[1] * self.tau_vert, si[2] + vi[2] * self.tau_vert]
            self.so_vert_ths = [so[0] + vo[0] * self.tau_vert_entry, so[1] + vo[1] * self.tau_vert_entry, so[2] + vo[2] * self.tau_vert_entry]
            self.si_vert_ths = [si[0] + vi[0] * self.tau_vert_entry, si[1] + vi[1] * self.tau_vert_entry, si[2] + vi[2] * self.tau_vert_entry]
            self.so_vert_the = [so[0] + vo[0] * self.tau_vert_exit, so[1] + vo[1] * self.tau_vert_exit, so[2] + vo[2] * self.tau_vert_exit]
            self.si_vert_the = [si[0] + vi[0] * self.tau_vert_exit, si[1] + vi[1] * self.tau_vert_exit, si[2] + vi[2] * self.tau_vert_exit]
            return True


    def compute_violationOverlap(self, so, si, vo, vi):

        interval1 = [self.tau_ths, self.tau_the]
        interval2 = [self.tau_vert_entry, self.tau_vert_exit]

        if interval2[0] <= interval1[0] <= interval2[1]:
            start = interval1[0]
        elif interval1[0] <= interval2[0] <= interval1[1]:
            start = interval2[0]
        else:
            return False

        if interval2[0] <= interval1[1] <= interval2[1]:
            end = interval1[1]
        elif interval1[0] <= interval2[1] <= interval1[1]:
            end = interval2[1]
        else:
            return False

        self.tau_conf_entry = start
        self.tau_conf_exit = end

        if self.tau_conf_entry != -np.inf:
            self.so_conf_entry = [so[0] + vo[0] * self.tau_conf_entry, so[1] + vo[1] * self.tau_conf_entry, so[2] + vo[2] * self.tau_conf_entry]
            self.si_conf_entry = [si[0] + vi[0] * self.tau_conf_entry, si[1] + vi[1] * self.tau_conf_entry, si[2] + vi[2] * self.tau_conf_entry]
        if self.tau_conf_exit != np.inf:
            self.so_conf_exit = [so[0] + vo[0] * self.tau_conf_exit, so[1] + vo[1] * self.tau_conf_exit, so[2] + vo[2] * self.tau_conf_exit]
            self.si_conf_exit = [si[0] + vi[0] * self.tau_conf_exit, si[1] + vi[1] * self.tau_conf_exit, si[2] + vi[2] * self.tau_conf_exit]
        return True


    def fastDivergingConditions(self, so, si, vo, vi,  vsep):
        c = np.linalg.norm(vi[0:1]-vo[0:1])
        s = np.linalg.norm (si[0:1]-so[0:1])
        if c > HorizontalFastDivergingVelocity[self.region] and s > HorizontalFastDivergingSeparation[self.region]:
            self.ProximityFilter = False
            self.LinearPredictionFilter = False
        vert = vi[2] - vo[2]
        s_vert = np.abs(si[2] - so[2])
        if vert > VerticalFastDivergingVelocity[self.region] and s_vert > VerticalFastDivergingSeparation[self.region, vsep]:
            self.ProximityFilter = False
            self.TurningPredictionFilter = False
            self.LinearPredictionFilter = False


    def compute_linearPredictionAlertConfirmation(self):
        # imminent conflict?
        if self.tau_conf_entry < LinearPredictionImminentTime[self.region]:
            return True
        # conflict hit count sufficient?
        m = self.lastLinearPredictionConflict.sum()
        if m < LinearPredictionConflictCount[self.region]:
            return False
        # conflict within alert time
        if self.tau_conf_entry < LinearPredictionWarningTime[self.region]:
            return True
        return False


    def computeStcaConflict(self, own_pos, int_pos):

        self.clean_object()

        so = np.array([own_pos.x_loc, own_pos.y_loc, own_pos.altitude])
        si = np.array([int_pos.x_loc, int_pos.y_loc, int_pos.altitude])
        vo = np.array([own_pos.x_v, own_pos.y_v, own_pos.v_speed])
        vi = np.array([int_pos.x_v, int_pos.y_v, int_pos.v_speed])

        isHorizontalConflict = self.compute_horizontalPrediction(so, si, vo, vi)

        if not isHorizontalConflict:
            self.incLastLinearPredictionConflict(0)
            return False

        isVerticalConflict = self.compute_verticalPrediction(so, si, vo, vi, 0)

        if not isVerticalConflict:
            self.incLastLinearPredictionConflict(0)
            return False

        if self.compute_violationOverlap(so, si, vo, vi):
            self.incLastLinearPredictionConflict(1)
            return self.compute_linearPredictionAlertConfirmation()
        else:
            return False

