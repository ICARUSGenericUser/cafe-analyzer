from Encounter import units as units


# Inputs:
#  HMD          : Horizontal miss distance threshold
#  H            : Vertical separation threshold


class SEP_parameters:

  def __init__(self, H, HMD):
    self.H = units.ft_to_m(H)
    self.HMD = units.nm_to_m(HMD)

  def setIMSParameters(self, H, HMD):
    self.H = H
    self.HMD = HMD