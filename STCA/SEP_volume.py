import numpy as np
import math as math
from STCA import SEP_output as sout


# Inputs:
#  DMOD         : Distance Modification of Modified Tau
#  HMD          : Horizontal miss distance threshold
#  TAUMOD       : Modified tau threshold
#  H            : Vertical separation threshold

# Outputs in RWC_out:
#  Rxy    : Horiztonal range
#  Dcpa   : Distance at closest point of approach
#  Taumod : TCAS II modified tau
#  Rz     : Relative altitude
#  Tcpa   : Time to CPA
#  Tcoa   : Time to co-altitude
#  WCVxy  : Horizontal well-clear violation
#  WCVz   : Vertical well-clear violation
#  WCV    : Well-clear violation

def check_separation_volume(own_pos, int_pos, sep_param):
  SEP_out = sout.SEP_output()

  so = np.array([own_pos.x_loc, own_pos.y_loc])
  si = np.array([int_pos.x_loc, int_pos.y_loc])
  s = si - so
  SEP_out.Rxy = np.linalg.norm(s, 2)
  cond1 = SEP_out.Rxy <= sep_param.HMD

  # Vertical dimension outputs
  s_z = int_pos.altitude - own_pos.altitude
  SEP_out.Rz = abs(s_z)
  cond2 = SEP_out.Rz <= sep_param.H

  SEP_out.SEPV = cond1 and cond2

  return SEP_out.SEPV


