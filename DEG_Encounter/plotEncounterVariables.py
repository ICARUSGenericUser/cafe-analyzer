import matplotlib.pyplot as plt

''' Plot all state variables to check their continuity over time '''


def plotSegmentVariables(encounterlist):
    # Check if the segment variables are correct: continuity during time, no sudden or abrupt changes etc.
    list = encounterlist
    inittimearr = []
    targettimearr = []
    initspeedarr = []
    targetspeedarr = []
    speedvararr = []
    initaltarr = []
    targetaltarr = []
    altvararr = []
    initheadarr = []
    targetheadarr = []
    headvararr = []
    initposx = []
    targetposx = []
    initposy = []
    targetposy = []
    initposz = []
    targetposz = []
    inittime = []
    targettime = []

    for i in range(len(list)):
        #timearr.append((list[i].initialTime,list[i].targetTime))
        inittimearr.append(list[i].initialTime)
        targettimearr.append(list[i].targetTime)
        initspeedarr.append(list[i].initialSpeed)
        targetspeedarr.append(list[i].targetSpeed)
        speedvararr.append(list[i].speedVariation)
        initaltarr.append(list[i].initialAltitude)
        targetaltarr.append(list[i].targetAltitude)
        altvararr.append(list[i].altitudeVariation)
        initheadarr.append(list[i].initialHeading)
        targetheadarr.append(list[i].targetHeading)
        headvararr.append(list[i].headingVariation)
        initposx.append(list[i].initialPosition[0])
        targetposx.append(list[i].targetPosition[0])
        initposy.append(list[i].initialPosition[1])
        targetposy.append(list[i].targetPosition[1])
        initposz.append(list[i].initialPosition[2])
        targetposz.append(list[i].targetPosition[2])
        inittime.append(list[i].initialPosition[3])
        targettime.append(list[i].targetPosition[3])

    plt.show()
    # Check speed variation from segments (still no waypoints data). Lines must match to be correct
    fig, axs = plt.subplots(2)
    fig.suptitle('Initial and Target Speed & Speed Variation Over Time')
    axs[0].plot(inittimearr, initspeedarr, targettimearr, targetspeedarr,marker = 'o')
    axs[1].plot(inittimearr, speedvararr,marker = 'o')
    plt.grid(True)
    axs[0].legend(['V_Initial', 'V_Target'])
    axs[0].set(ylabel='Velocity(m/s)')
    axs[1].set(xlabel='Time (s)', ylabel='Velocity(m/s)')
    plt.draw()


    # Check altitude variation from segments (still no waypoints data). Lines must match to be correct
    fig, axs = plt.subplots(2)
    fig.suptitle('Initial and Target Altitude & Altitude Variation Over Time')
    axs[0].plot(inittimearr, initaltarr, targettimearr, targetaltarr,marker = 'o')
    axs[1].plot(inittimearr, altvararr,marker = 'o')
    plt.grid(True)
    axs[0].legend(['h_Initial', 'h_Target'])
    axs[0].set(ylabel='Altitude(m)')
    axs[1].set(xlabel='Time (s)', ylabel='Altitude(m)')
    plt.draw()


    # Check heading variation from segments (still no waypoints data). Lines must match to be correct
    fig, axs = plt.subplots(2)
    fig.suptitle('Initial and Target Heading & Heading Variation Over Time')
    axs[0].plot(inittimearr, initheadarr, targettimearr, targetheadarr,marker = 'o')
    axs[1].plot(inittimearr, headvararr,marker = 'o')
    plt.grid(True)
    axs[0].legend(['Heading_Initial', 'Heading_Target'])
    axs[0].set(ylabel='Heading(deg)')
    axs[1].set(xlabel='Time (s)', ylabel='Heading(deg)')
    plt.draw()

    # Check Initial and Target variation of position from segments (still no waypoints data). Lines must match to be correct
    fig, axs = plt.subplots(2)
    fig.suptitle('X vs Y position & Altitude variation Over Time')
    axs[0].plot(initposy, initposx, targetposy, targetposx,marker = 'o')
    axs[1].plot(inittime, initposz, targettime, targetposz,marker = 'o')
    plt.grid(True)
    axs[0].legend(['Initial', 'Target'])
    axs[1].legend(['Z_Initial', 'Z_Target'])
    axs[0].set(xlabel = 'X position', ylabel='Y position')
    axs[1].set(xlabel='Time (s)', ylabel='Altitude (m)')
    plt.draw()


