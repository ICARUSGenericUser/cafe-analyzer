import os
from Encounter import units as units
from DEG_Encounter import SegmentEnum as enc


class Segment:
  def __init__(self):
    self.isNMAC = False
    self.extraSegment = False
    self.NMACFactor = 0.0
    self.estimatedDuration = 0

    self.segmentDuration = 0

    self.initialPosition = None
    self.intermediatePosition = []
    self.targetPosition = None

    self.initialTime = None
    self.targetTime = None

    self.headingVariation = 0
    self.initialHeading = 0
    self.targetHeading = 0
    self.horizontalMode = enc.horizontalSegment.none

    self.speedVariation = 0
    self.initialSpeed = 0
    self.targetSpeed = 0
    self.speedMode = enc.speedSegment.none

    self.altitudeVariation = 0
    self.initialAltitude = 0
    self.targetAltitude = 0
    self.verticalMode = enc.verticalSegment.none
