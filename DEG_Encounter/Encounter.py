import numpy as np
import math as math
import os, shutil
from DEG_Encounter import Angles as ang
from ProbabilityDist import Encounter_Distribution as ed
from DEG_Encounter import NMACValues as nv, Trajectory as tr, Segment as sg
from DEG_Encounter import SegmentEnum as se, NMACValues as nm

G = 9.8
TURN_STEP = 45
ACCELERATION_FACTOR = 0.5
DECELERATION_FACTOR = 0.4

# Vf = 0 = Vi + Df * g * t
# d = Vi * t + (Df * g t^2 / 2)
def getDecelerationTimeDistance(Vi):
  t = Vi /(G * DECELERATION_FACTOR)
  d = Vi * t + (G * DECELERATION_FACTOR) * t**2 / 2
  return t, d

def getAccelerationTimeDistance(Vi):
  t = Vi /(G * ACCELERATION_FACTOR)
  d = Vi * t + (G * ACCELERATION_FACTOR) * t**2 / 2
  return t, d


class Encounter:
  def __init__(self):
    self.number = 0
    self.currentSeed = 0
    self.rootFolder = ""
    self.NMACValues = None
    self.ownshipTrajectory = None
    self.intrTrajectory = None


  def createEncounterFolder(self, generationParameters):

    rootFolder = generationParameters.destinationPath + generationParameters.encounterSetName + "\\"

    if not os.path.exists(rootFolder):
      print("Root folder does not exist: ", rootFolder)
      return

    self.rootFolder = rootFolder
    encounterFolder = "Encs_" + str(self.number).zfill(8)
    os.mkdir(rootFolder + encounterFolder)

    os.mkdir(rootFolder + encounterFolder + "\\Ownship")
    os.mkdir(rootFolder + encounterFolder + "\\Intruder")


  def getEncounterPath(self):
    return self.rootFolder + "\\" + "Encs_" + str(self.number).zfill(8) + "\\"


  def getOwnshipFigurePath(self):
    return self.rootFolder + "\\" + "Encs_" + str(self.number).zfill(8) + "\\Ownship\\"


  def getIntruderFigurePath(self):
    return self.rootFolder + "\\" + "Encs_" + str(self.number).zfill(8) + "\\Intruder\\"


  def getOwnshipHPath(self):
    return self.rootFolder + "\\" + "Encs_" + str(self.number).zfill(8) + "\\Ownship\\" + "HPath_" + str(self.number).zfill(8) + ".png"


  def getIntruderHPath(self):
    return self.rootFolder + "\\" + "Encs_" + str(self.number).zfill(8) + "\\Intruder\\" + "HPath_" + str(self.number).zfill(8) + ".png"


  def getOwnshipPlotWPHPath(self):
    return self.rootFolder + "\\" + "Encs_" + str(self.number).zfill(8) + "\\Ownship\\" + "WP_HPath_" + str(self.number).zfill(8) + ".png"


  def getIntruderPlotWPHPath(self):
    return self.rootFolder + "\\" + "Encs_" + str(self.number).zfill(8) + "\\Intruder\\" + "WP_HPath_" + str(self.number).zfill(8) + ".png"


  def getOwnshipPlotHPath(self):
    return self.rootFolder + "\\" + "Encs_" + str(self.number).zfill(8) + "\\Ownship\\" + "Traj_HPath_" + str(self.number).zfill(8) + ".png"


  def getIntruderPlotHPath(self):
    return self.rootFolder + "\\" + "Encs_" + str(self.number).zfill(8) + "\\Intruder\\" + "Traj_HPath" + str(self.number).zfill(8) + ".png"


  def getPathStoreData(self):
    return self.rootFolder + "\\" +  "Encs_" + str(self.number).zfill(8) + "\\" + "Encs_" + str(self.number).zfill(8) + ".eu1.txt"


  def getIntruderDataPath(self):
    return self.rootFolder + "\\" + "Encs_" + str(self.number).zfill(8) + "\\Intruder\\"


  def getOwnshipDataPath(self):
    return self.rootFolder + "\\" + "Encs_" + str(self.number).zfill(8) + "\\Ownship\\"


  def getCombinedDataPath(self):
    return self.rootFolder + "\\" + "Encs_" + str(self.number).zfill(8) + "\\"


  def getPathStoreSummaryOutput(self):
    return self.rootFolder + "\\"  + "Encounter_Summary_Output.DEG.csv"

  def getEncounterPathSaveLog(self):
    return self.rootFolder + "\\" + "Encs_" + str(self.number).zfill(8) + "\\" + "Log_" + str(self.number).zfill(8) + ".txt"

  #
  # Sampling the NMAC parameters
  ##############################

  def sampleNMAC(self, encDist):
    self.NMACValues = nv.NMACValues()

    print("\nSampling NMAC: ")

    # Step-1 VMD and HMD sampling
    self.NMACValues.VMD = encDist.Initial_Distribution.vmdDistribution.getSampleUnits()
    self.NMACValues.HMD = encDist.Initial_Distribution.hmdDistribution.getSampleUnits()

    print("VMD: ", self.NMACValues.VMD, " HMD: ", self.NMACValues.HMD)

    # Step-2 AircraftClass sampling
    self.NMACValues.ClassOwnshipIdx = encDist.Initial_Distribution.ownshipClassDistribution.getSample()
    self.NMACValues.ClassOwnship = encDist.getAircraftClass(self.NMACValues.ClassOwnshipIdx)

    print("Ownship. Vehicle index:", self.NMACValues.ClassOwnshipIdx, " Type: ", self.NMACValues.ClassOwnship.className)

    self.NMACValues.ClassIntrIdx = encDist.Initial_Distribution.intruderClassDistribution.getSample()
    self.NMACValues.ClassIntr = encDist.getAircraftClass(self.NMACValues.ClassIntrIdx)

    print("Intruder. Vehicle index:", self.NMACValues.ClassIntrIdx, " Type: ", self.NMACValues.ClassIntr.className)

    # Step-3 Altitude sampling. Two options.
    # 1- Sample altitude at NMAC distribution and apply filters according to aircraftClass characteristics
    # 2- Sample altitude according to aircraft class, apply VMD and filter according to intruder aircraft class
    # Option 1:
    self.NMACValues.AltitudeOwnship = encDist.Initial_Distribution.altitudeDistribution.getSampleUnits()
    self.NMACValues.AltitudeIntr = self.NMACValues.AltitudeOwnship + self.NMACValues.VMD
    # Option 2: TBI

    print("Ownship altitude: ", self.NMACValues.AltitudeOwnship, " Intruder altitude: ", self.NMACValues.AltitudeIntr)

    # Step-4 Speed sampling. Two options:
    # 1- Sample speed at NMAC distribution and filter according to aircraft class
    # 2- Sample speeds according to aircraft class
    # Option 1:
    self.NMACValues.SpeedOwnship = encDist.Initial_Distribution.speedDistribution.getSampleUnits()
    self.NMACValues.SpeedOwnship = self.NMACValues.ClassOwnship.speedDistribution.capToLimits(self.NMACValues.SpeedOwnship)

    self.NMACValues.SpeedIntr = encDist.Initial_Distribution.speedDistribution.getSampleUnits()
    self.NMACValues.SpeedIntr = self.NMACValues.ClassIntr.speedDistribution.capToLimits(self.NMACValues.SpeedIntr)

    # Option 2: TBI

    print("Ownship speed: ", self.NMACValues.SpeedOwnship, " Intruder speed: ", self.NMACValues.SpeedIntr)

    # Step-5 Approach angle sampling
    self.NMACValues.AppAngle = encDist.Initial_Distribution.appAngleDistribution.getSampleUnits()

    print("Approach angle: ", self.NMACValues.AppAngle)
    #
    # Step-6 Create trajectories for ownship
    self.ownshipTrajectory = tr.Trajectory()
    self.ownshipTrajectory.isOwnship = True

    # Step-6.1 Duplicate selected NMAC parameters
    self.ownshipTrajectory.NMACValues = nm.NMACSubValues()
    self.ownshipTrajectory.NMACValues.Altitude = self.NMACValues.AltitudeOwnship
    self.ownshipTrajectory.NMACValues.Speed = self.NMACValues.SpeedOwnship
    self.ownshipTrajectory.NMACValues.Class = self.NMACValues.ClassOwnship

    #
    # Step-7 Create trajectories for intruder
    self.intrTrajectory = tr.Trajectory()
    self.intrTrajectory.isOwnship = False

    # Step-7.1 Duplicate selected NMAC parameters
    self.intrTrajectory.NMACValues = nm.NMACSubValues()
    self.intrTrajectory.NMACValues.Altitude = self.NMACValues.AltitudeIntr
    self.intrTrajectory.NMACValues.Speed = self.NMACValues.SpeedIntr
    self.intrTrajectory.NMACValues.Class = self.NMACValues.ClassIntr


  #
  # Sampling the NMAC segment based on the NMAC information for the encounter
  ###########################################################################

  def sampleNMACSegment(self, trajectory, initialDist, encDist):
    segment = sg.Segment()
    segment.isNMAC = True

    #
    # Step 0 Determine relative position for NMAC point
    segment.NMACFactor = ed.getUnaryUniformFactor()

    #
    # Step 1 Determine horizontal mode
    segment.horizontalMode = se.horizontalSegment(encDist.Transition_Distribution.horizontalCmdProb.getSampleUnits())
    print("\nSampling NMAC horizontal mode: ", segment.horizontalMode)

    #
    # Fill the rest of horizontal parameters

    #
    # Assign speed variation and segment if not a hover
    if segment.horizontalMode != se.horizontalSegment.hover:
      #
      # Segment duration just in straight segments
      if segment.horizontalMode != se.horizontalSegment.turn:
        segment.segmentDuration = encDist.Transition_Distribution.segmentDurationDistribution.getSampleUnits()
        print("Straight forward duration: ", segment.segmentDuration)

      segment.speedMode = se.speedSegment(encDist.Transition_Distribution.speedCmdProb.getSampleUnits())
      print("Sampling NMAC speed mode: ", segment.speedMode)

      if segment.speedMode != se.speedSegment.constant:
        speedVariation = encDist.Transition_Distribution.speedVariationDistribution.getSampleUnits()
        print("Speed variation: ", speedVariation)

        if segment.speedMode == se.speedSegment.accelerate:
          segment.speedVariation = speedVariation

        if segment.speedMode == se.speedSegment.decelerate:
          segment.speedVariation = -speedVariation

        speedRange = ed.scaleToUnaryFactor(segment.speedVariation, trajectory.NMACValues.Speed, segment.NMACFactor)

        speedRange[1] = encDist.speedDistribution.capToLimits(speedRange[1])
        speedRange[0] = encDist.speedDistribution.capToLimits(speedRange[0])
        segment.speedVariation = speedRange[1] - speedRange[0]

        if segment.speedVariation > 0:
          segment.NMACFactor = ed.newUnaryFactor(speedRange, trajectory.NMACValues.Speed)

          #
          # WARNING OVERRIDE: Very special case.
          # We set a minimum speed of 0.5m/s for a segment
          # If not satisfied, we reassign to a constant speed segment
          if speedRange[0] >= 0.5 and speedRange[1] >= 0.5:
            segment.initialSpeed = speedRange[0]
            segment.targetSpeed = speedRange[1]
          else:
            segment.speedMode = se.speedSegment.constant
            segment.speedVariation = 0
            segment.initialSpeed = trajectory.NMACValues.Speed
            segment.targetSpeed = trajectory.NMACValues.Speed
          # END OVERRIDE.
        else:
          # We revert to constant speed
          segment.speedMode = se.speedSegment.constant

      else:
        # No acceleration, so constant speed
        segment.initialSpeed = trajectory.NMACValues.Speed
        segment.targetSpeed = trajectory.NMACValues.Speed

      print("Initial speed: ", segment.initialSpeed, " Target speed: ", segment.targetSpeed)
    else:
      # Hovering, so speed is 0
      segment.initialSpeed = 0
      segment.targetSpeed = 0

    # Assign heading variation if in a turn
    if segment.horizontalMode == se.horizontalSegment.turn:
      segment.headingVariation = encDist.Transition_Distribution.headingVariationDistribution.getSampleUnits()
      segment.initialHeading = 0
      segment.targetHeading = ang.normalizeAngle(segment.headingVariation)
      trate = trajectory.NMACValues.Class.trateDistribution.getSampleUnits()
      segment.segmentDuration = abs(segment.headingVariation / trate)
      print("Turn, heading variation: ", segment.headingVariation, " target heading: ", segment.targetHeading, " duration: ", segment.segmentDuration, " trate: ", trate)
    else:
      # No turn so heading is 0
      segment.initialHeading = 0
      segment.targetHeading = 0

    #
    # Step 2 Determine vertical mode
    segment.verticalMode = se.verticalSegment(encDist.Transition_Distribution.verticalCmdProb.getSampleUnits())
    print("Sampling vertical mode: ", segment.verticalMode)

    #
    # Fill the rest of vertical parameters

    if segment.verticalMode != se.verticalSegment.level:
      altitudeVariation = encDist.Transition_Distribution.altitudeVariationDistribution.getSampleUnits()
      print("Altitude variation: ", altitudeVariation)
      altitudeRange = ed.scaleToUnaryFactor(altitudeVariation, trajectory.NMACValues.Altitude, segment.NMACFactor)

      altitudeRange[1] = initialDist.altitudeDistribution.capToLimits(altitudeRange[1])
      altitudeRange[0] = initialDist.altitudeDistribution.capToLimits(altitudeRange[0])
      altitudeVariation = altitudeRange[1] - altitudeRange[0]

      if altitudeVariation > 0:
        segment.NMACFactor = ed.newUnaryFactor(altitudeRange, trajectory.NMACValues.Altitude)

        if segment.verticalMode == se.verticalSegment.climb:
          segment.altitudeVariation = altitudeVariation
          segment.initialAltitude = altitudeRange[0]
          segment.targetAltitude = altitudeRange[1]
          print("Climb extent: ", segment.altitudeVariation)

        if segment.verticalMode == se.verticalSegment.descent:
          segment.altitudeVariation = -altitudeVariation
          segment.initialAltitude = altitudeRange[1]
          segment.targetAltitude = altitudeRange[0]
          print("Descent extent: ", segment.altitudeVariation)

      else:
        # We revert to a level segment
        segment.altitudeVariation = 0
        segment.initialAltitude = altitudeRange[0]
        segment.targetAltitude = altitudeRange[1]
        segment.verticalMode = se.verticalSegment.level

      print("Initial altitude: ", segment.initialAltitude, " Target altitude: ", segment.targetAltitude)

      # If hover, descent time needs to be determined
      if segment.horizontalMode == se.horizontalSegment.hover:
        if segment.verticalMode == se.verticalSegment.climb:
          segment.segmentDuration = altitudeVariation / encDist.climbRateDistribution.getSampleUnits()

        if segment.verticalMode == se.verticalSegment.descent:
          segment.segmentDuration = altitudeVariation / encDist.descentRateDistribution.getSampleUnits()

        print("Hover duration: ", segment.segmentDuration)
    else:
      # No altitude variation
      segment.initialAltitude = trajectory.NMACValues.Altitude
      segment.targetAltitude = trajectory.NMACValues.Altitude
      print("Level altitude: ", segment.initialAltitude)

    # If hover, stationary time needs to be determined
    if segment.horizontalMode == se.horizontalSegment.hover and segment.verticalMode == se.verticalSegment.level:
      segment.segmentDuration = encDist.Transition_Distribution.holdDurationDistribution.getSampleUnits()
      print("Stationary hover duration: ", segment.segmentDuration)

    #
    # Add to the segment list
    trajectory.segmentList.append(segment)
    trajectory.NMACSegment = segment

    #
    # Update trajectory duration
    trajectory.timeBeforeNMAC = segment.segmentDuration * segment.NMACFactor
    trajectory.timeAfterNMAC = segment.segmentDuration * (1 - segment.NMACFactor)
    print("NMAC segment. ", " Before NMAC: ", trajectory.timeBeforeNMAC, " After NMAC: ", trajectory.timeAfterNMAC)


  #
  # Sampling a segment forward based on the previous segment information
  ######################################################################

  def sampleForwardSegment(self, trajectory, initialDist, vehicleEncDist):
    segment = sg.Segment()
    segment.isNMAC = False

    prevSegment = trajectory.getLastSegment()

    #
    # Step 0 Transpose basic factors
    segment.initialAltitude = prevSegment.targetAltitude
    segment.initialSpeed = prevSegment.targetSpeed
    segment.initialHeading = prevSegment.targetHeading

    #
    # Step 1 Determine horizontal mode
    segment.horizontalMode = se.horizontalSegment(vehicleEncDist.Transition_Distribution.horizontalCmdProb.getSampleUnits())
    print("\nSampling forward horizontal mode: ", segment.horizontalMode)
    #
    # Fill the rest of horizontal parameters

    #
    # Assign speed variation and segment duration if not a hover
    if segment.horizontalMode != se.horizontalSegment.hover:
      #
      # Segment duration just in straight segments
      if segment.horizontalMode != se.horizontalSegment.turn:
        segment.segmentDuration = vehicleEncDist.Transition_Distribution.segmentDurationDistribution.getSampleUnits()
        print("Straight forward duration: ", segment.segmentDuration)

      segment.speedMode = se.speedSegment(vehicleEncDist.Transition_Distribution.speedCmdProb.getSampleUnits())
      print("Sampling speed mode: ", segment.speedMode)

      #
      # WARNING OVERRIDE: Very special case.
      # Previous segment speed is 0 and we sampled something different than a hover, thus:
      # the only reasonable option is to accelerate (constant or decelerations are inconsistent)
      if math.isclose(segment.initialSpeed, 0.0) and segment.speedMode != se.speedSegment.accelerate:
        segment.speedMode = se.speedSegment.accelerate
        print("No way, speed mode should be: ", segment.speedMode)
      # End of OVERRIDE

      if segment.speedMode != se.speedSegment.constant:
        speedVariation = vehicleEncDist.Transition_Distribution.speedVariationDistribution.getSampleUnits()
        print("Speed variation: ", speedVariation)

        if segment.speedMode == se.speedSegment.accelerate:
          segment.speedVariation = speedVariation

        if segment.speedMode == se.speedSegment.decelerate:
          segment.speedVariation = -speedVariation

        segment.targetSpeed = segment.initialSpeed + segment.speedVariation

        # Cap speed to limits according to vehicle performance
        segment.targetSpeed = vehicleEncDist.speedDistribution.capToLimits(segment.targetSpeed)
        # Recalculate Variation
        segment.speedVariation = segment.targetSpeed - segment.initialSpeed

        #
        # WARNING OVERRIDE: Very special case.
        # If target speed becomes close to zero or negative we revert to a constant speed segment
        # We set a minimum speed of 0.5m/s for a segment
        #if segment.targetSpeed <= 0.4:
        #  segment.speedVariation = 0
        #  segment.targetSpeed = segment.initialSpeed
        #  segment.speedMode = se.speedSegment.constant
        # END WARNING.

        print("Initial speed: ", segment.initialSpeed, " Target speed: ", segment.targetSpeed)
      else:
        # No speed variation
        segment.targetSpeed = segment.initialSpeed
    else:
      # Hover mode, speed is 0
      # Initial speed could be greather than 0, so a deceleration will be needed.
      segment.targetSpeed = 0
      t, d = getDecelerationTimeDistance(segment.initialSpeed)
      segment.segmentDuration = t
      print("Decelerating hover duration: ", segment.segmentDuration)

    # Assign heading variation if in a turn
    if segment.horizontalMode == se.horizontalSegment.turn:
      segment.headingVariation = vehicleEncDist.Transition_Distribution.headingVariationDistribution.getSampleUnits()
      segment.targetHeading = ang.normalizeAngle(segment.initialHeading + segment.headingVariation)
      trate = vehicleEncDist.trateDistribution.getSampleUnits()
      segment.segmentDuration = abs(segment.headingVariation) / trate
      print("Turn, heading variation: ", segment.headingVariation, " initial heading: ", segment.initialHeading, " target heading: ", segment.targetHeading, " duration: ", segment.segmentDuration, " trate: ", trate)
    else:
      # No heading variation
      segment.targetHeading = segment.initialHeading

    #
    # Step 2 Determine vertical mode
    segment.verticalMode = se.verticalSegment(vehicleEncDist.Transition_Distribution.verticalCmdProb.getSampleUnits())
    print("Sampling vertical mode: ", segment.verticalMode)

    #
    # Fill the rest of vertical parameters

    if segment.verticalMode != se.verticalSegment.level:
      altitudeVariation = vehicleEncDist.Transition_Distribution.altitudeVariationDistribution.getSampleUnits()
      print("Altitude variation: ", altitudeVariation)

      # Climb segment
      if segment.verticalMode == se.verticalSegment.climb:
        segment.altitudeVariation = altitudeVariation

        # Cap altitude to limits according to vehicle performance
        segment.targetAltitude = initialDist.altitudeDistribution.capToLimits(segment.initialAltitude + segment.altitudeVariation)
        # Recalculate Variation
        segment.altitudeVariation = segment.targetAltitude - segment.initialAltitude

        # If hover, climb time needs to be determined
        if segment.horizontalMode == se.horizontalSegment.hover:
          segment.segmentDuration += altitudeVariation / vehicleEncDist.climbRateDistribution.getSampleUnits()
          print("Hover climb duration: ", segment.segmentDuration)

      if segment.verticalMode == se.verticalSegment.descent:
        segment.altitudeVariation = -altitudeVariation

        # Cap altitude to limits according to vehicle performance
        segment.targetAltitude = initialDist.altitudeDistribution.capToLimits(segment.initialAltitude + segment.altitudeVariation)
        # Recalculate Variation
        segment.altitudeVariation = segment.targetAltitude - segment.initialAltitude

        # If hover, descent time needs to be determined
        if segment.horizontalMode == se.horizontalSegment.hover:
          segment.segmentDuration += altitudeVariation / vehicleEncDist.descentRateDistribution.getSampleUnits()
          print("Hover descent duration: ", segment.segmentDuration)

      # Segment vertical mode set to level
      if math.isclose(segment.segmentDuration, 0.0):
        segment.verticalMode = se.verticalSegment.level
        segment.segmentDuration = 0
        segment.altitudeVariation = 0

      #segment.targetAltitude = segment.initialAltitude + segment.altitudeVariation
      print("Initial altitude: ", segment.initialAltitude, " Target altitude: ", segment.targetAltitude)
    else:
      # No altitude variation
      segment.targetAltitude = segment.initialAltitude
      print("Level altitude: ", segment.initialAltitude)

    # If hover, stationary time needs to be determined
    if segment.horizontalMode == se.horizontalSegment.hover and segment.verticalMode == se.verticalSegment.level:
      segment.segmentDuration += vehicleEncDist.Transition_Distribution.holdDurationDistribution.getSampleUnits()
      print("Stationary hover duration: ", segment.segmentDuration)

    #
    # Add to the segment list
    trajectory.segmentList.append(segment)

    #
    # Update trajectory duration
    trajectory.timeAfterNMAC += segment.segmentDuration
    print("Total time after NMAC: ", trajectory.timeAfterNMAC)


  #
  # Sampling a segment backward based on the next segment information
  ######################################################################

  def sampleBackwardSegment(self, trajectory, initialDist, vehicleEncDist):
    segment = sg.Segment()
    segment.isNMAC = False

    nextSegment = trajectory.getFirstSegment()

    #
    # Step 0 Transpose basic factors
    segment.targetAltitude = nextSegment.initialAltitude
    segment.targetSpeed = nextSegment.initialSpeed
    segment.targetHeading = nextSegment.initialHeading

    #
    # Step 1 Determine horizontal mode
    segment.horizontalMode = se.horizontalSegment(vehicleEncDist.Transition_Distribution.horizontalCmdProb.getSampleUnits())
    print("\nSampling backward horizontal mode: ", segment.horizontalMode)
    #
    # Fill the rest of horizontal parameters

    #
    # Assign speed variation if not a hover
    if segment.horizontalMode != se.horizontalSegment.hover:
      #
      # Segment duration just in straight segments
      if segment.horizontalMode != se.horizontalSegment.turn:
        segment.segmentDuration = vehicleEncDist.Transition_Distribution.segmentDurationDistribution.getSampleUnits()
        print("Straight backward duration: ", segment.segmentDuration)

      segment.speedMode = se.speedSegment(vehicleEncDist.Transition_Distribution.speedCmdProb.getSampleUnits())
      print("Sampling speed mode: ", segment.speedMode)

      #
      # WARNING OVERRIDE: Very special case.
      # Next segment speed is 0 and we sampled something different than a hover, thus:
      # the only reasonable option is to decelerate (constant or accelerations are inconsistent)
      if math.isclose(segment.targetSpeed, 0.0) and segment.speedMode != se.speedSegment.decelerate:
        segment.speedMode = se.speedSegment.decelerate
        print("No way, speed mode should be: ", segment.speedMode)
      # End of OVERRIDE

      if segment.speedMode != se.speedSegment.constant:
        speedVariation = vehicleEncDist.Transition_Distribution.speedVariationDistribution.getSampleUnits()
        print("Speed variation: ", speedVariation)

        if segment.speedMode == se.speedSegment.accelerate:
          segment.speedVariation = speedVariation

        if segment.speedMode == se.speedSegment.decelerate:
          segment.speedVariation = -speedVariation

        segment.initialSpeed = segment.targetSpeed - segment.speedVariation

        # Cap speed to limits according to vehicle performance
        segment.initialSpeed = vehicleEncDist.speedDistribution.capToLimits(segment.initialSpeed)
        # Recalculate Variation
        segment.speedVariation = segment.targetSpeed - segment.initialSpeed

        #
        # WARNING OVERRIDE: Very special case.
        # If initial speed becomes close to zero or negative we revert to a constant speed segment
        # We set a minimum speed of 0.5m/s for a segment
        #if segment.initialSpeed <= 0.4:
        #  segment.speedVariation = 0
        #  segment.initialSpeed = segment.targetSpeed
        #  segment.speedMode = se.speedSegment.constant
        # END WARNING.
        print("Initial speed: ", segment.initialSpeed, " Target speed: ", segment.targetSpeed)
      else:
        # No speed variation
        segment.initialSpeed = segment.targetSpeed
    else:
      # Hover mode, speed is 0
      # Target speed could be greather than 0, so an acceleration will be needed.
      segment.initialSpeed = 0
      t, d = getAccelerationTimeDistance(segment.targetSpeed)
      segment.segmentDuration = t
      print("Accelerating hover duration: ", segment.segmentDuration)

    # Assign heading variation if in a turn
    if segment.horizontalMode == se.horizontalSegment.turn:
      segment.headingVariation = vehicleEncDist.Transition_Distribution.headingVariationDistribution.getSampleUnits()
      segment.initialHeading = ang.normalizeAngle(segment.targetHeading - segment.headingVariation)
      trate = vehicleEncDist.trateDistribution.getSampleUnits()
      #trate = trajectory.NMACValues.Class.trateDistribution.getSampleUnits()
      segment.segmentDuration = abs(segment.headingVariation) / trate
      print("Turn, heading variation: ", segment.headingVariation, " initial heading: ", segment.initialHeading, " target heading: ", segment.targetHeading, " duration: ", segment.segmentDuration, " trate: ", trate)
    else:
      # No heading variation
      segment.initialHeading = segment.targetHeading

    #
    # Step 2 Determine vertical mode
    segment.verticalMode = se.verticalSegment(vehicleEncDist.Transition_Distribution.verticalCmdProb.getSampleUnits())
    print("Sampling vertical mode: ", segment.verticalMode)

    #
    # Fill the rest of vertical parameters

    if segment.verticalMode != se.verticalSegment.level:
      altitudeVariation = vehicleEncDist.Transition_Distribution.altitudeVariationDistribution.getSampleUnits()
      print("Altitude variation: ", altitudeVariation)

      # Climb segment
      if segment.verticalMode == se.verticalSegment.climb:
        segment.altitudeVariation = altitudeVariation

        # Cap altitude to limits according to vehicle performance
        segment.initialAltitude = initialDist.altitudeDistribution.capToLimits(segment.targetAltitude - segment.altitudeVariation)
        # Recalculate Variation
        segment.altitudeVariation = segment.targetAltitude - segment.initialAltitude

        # If hover, climb time needs to be determined
        if segment.horizontalMode == se.horizontalSegment.hover:
          segment.segmentDuration += altitudeVariation / vehicleEncDist.climbRateDistribution.getSampleUnits()
          print("Hover climb duration: ", segment.segmentDuration)

      # Descent segment
      if segment.verticalMode == se.verticalSegment.descent:
        segment.altitudeVariation = -altitudeVariation

        # Cap altitude to limits according to vehicle performance
        segment.initialAltitude = initialDist.altitudeDistribution.capToLimits(segment.targetAltitude - segment.altitudeVariation)
        # Recalculate Variation
        segment.altitudeVariation = segment.targetAltitude - segment.initialAltitude

        # If hover, descent time needs to be determined
        if segment.horizontalMode == se.horizontalSegment.hover:
          segment.segmentDuration += altitudeVariation / vehicleEncDist.descentRateDistribution.getSampleUnits()
          print("Hover descent duration: ", segment.segmentDuration)

      # Segment vertical mode set to level
      if math.isclose(segment.segmentDuration, 0.0):
        segment.verticalMode = se.verticalSegment.level
        segment.segmentDuration = 0
        segment.altitudeVariation = 0

      #segment.initialAltitude = segment.targetAltitude - segment.altitudeVariation
      print("Initial altitude: ", segment.initialAltitude, " Target altitude: ", segment.targetAltitude)
    else:
      # No altitude variation
      segment.initialAltitude = segment.targetAltitude
      print("Level altitude: ", segment.initialAltitude)

    # If hover, stationary time needs to be determined
    if segment.horizontalMode == se.horizontalSegment.hover and segment.verticalMode == se.verticalSegment.level:
      segment.segmentDuration += vehicleEncDist.Transition_Distribution.holdDurationDistribution.getSampleUnits()
      print("Stationary hover duration: ", segment.segmentDuration)

    #
    # Add to the segment list
    trajectory.segmentList.insert(0, segment)

    #
    # Update trajectory duration
    trajectory.timeBeforeNMAC += segment.segmentDuration
    print("Total time before NMAC: ", trajectory.timeBeforeNMAC)


  #
  # Sampling a last segment backward to ensure 0 initial speed
  ######################################################################

  def addLastBackwardSegment(self, trajectory):

    nextSegment = trajectory.getFirstSegment()

    if nextSegment.initialSpeed > 0:
      segment = sg.Segment()

      print("Adding initial acceleration level trajectory to speed-up to: ", nextSegment.initialSpeed, " m/s")
      #
      # Step 0 Transpose basic factors
      segment.targetAltitude = nextSegment.initialAltitude
      segment.targetSpeed = nextSegment.initialSpeed
      segment.targetHeading = nextSegment.initialHeading

      segment.initialAltitude = segment.targetAltitude
      segment.initialSpeed = 0
      segment.initialHeading = segment.targetHeading
      #
      # Step 1 Determine horizontal mode
      segment.horizontalMode = se.horizontalSegment.straight
      segment.verticalMode = se.verticalSegment.level
      segment.speedMode = se.speedSegment.accelerate
      #
      # Step 2 Fill the rest of horizontal parameters
      segment.speedVariation = segment.targetSpeed
      segment.segmentDuration = 5 # Hardwired 5 sec TODO: make it proportional to the necessary acceleration
      segment.altitudeVariation = 0
      segment.headingVariation = 0
      #
      # Add to the segment list
      segment.extraSegment = True
      trajectory.extraSegment = segment
      trajectory.segmentList.insert(0, segment)

      #
      # Update trajectory duration
      trajectory.timeBeforeNMAC += segment.segmentDuration
      print("Total time before NMAC: ", trajectory.timeBeforeNMAC)


  #
  # Position the NMAC segment based on the NMAC information for the segment
  ###########################################################################

  def positionNMACSegment(self, trajectory, segment):
    #
    # Take aircraft performance data
    perfClass = trajectory.NMACValues.Class

    #
    # Segment duration
    segment.initialTime = -1 * segment.NMACFactor * segment.segmentDuration
    segment.targetTime = (1- segment.NMACFactor) * segment.segmentDuration

    print("\nPosition NMAC Segment that starts at: ", segment.initialTime, " ends at: ", segment.targetTime)
    #
    # Straight segment
    if segment.horizontalMode == se.horizontalSegment.straight:

      v2 = ang.getVelocityVector2D(segment.targetSpeed, segment.initialHeading)
      segment.initialPosition = ang.createVector4D(np.array([0, 0]), segment.initialAltitude, segment.initialTime)

      s4 =  ang.linearTimeProjection4D(segment.initialPosition, v2, segment.segmentDuration)
      s4 = ang.assignTime4D(s4, segment.targetTime)
      s4 = ang.assignAltitude4D(s4, segment.targetAltitude)
      segment.targetPosition = s4

      print("-> Straight segment. Vertical mode: ", segment.verticalMode, " Speed mode: ", segment.speedMode)
      print("\tInitial time: ", segment.initialTime, " Target time: ", segment.targetTime)
      print("\tInitial speed: ", segment.initialSpeed, " Target speed: ", segment.targetSpeed)
      print("\tInitial altitude: ", segment.initialAltitude, " Target altitude: ", segment.targetAltitude)
      print("\tInitial heading: ", segment.initialHeading, " Target heading: ", segment.targetHeading)
      print("\tInitial position at  X: ", segment.initialPosition[0], " Y: ", segment.initialPosition[1], " Z: ", segment.initialPosition[2], " T: ", segment.initialPosition[3])
      print("\tTarget position at  X: ", segment.targetPosition[0], " Y: ", segment.targetPosition[1], " Z: ", segment.targetPosition[2], " T: ", segment.targetPosition[3])


    #
    # Turn segment
    # Turns need to be divided into sub segments depending on the total heading change.
    # This current algorithm will subdivide in blocks of maximum 90 degrees each,
    # with time/distance divided proportionally.
    # Note that for that reason, intermediatePosition should become a list of waypoints.
    if segment.horizontalMode == se.horizontalSegment.turn:

      segment.intermediatePosition = []
      segment.initialPosition = ang.createVector4D(np.array([0, 0]), segment.initialAltitude, segment.initialTime)
      currentPosition = segment.initialPosition
      currentHeading = segment.initialHeading
      remainingHeading = segment.headingVariation
      headingStep = np.sign(segment.headingVariation) * TURN_STEP
      nSteps = abs(remainingHeading) // abs(headingStep)
      stepTime = segment.segmentDuration * TURN_STEP / abs(segment.headingVariation)

      if abs(remainingHeading) % abs(headingStep) < TURN_STEP * 0.25:
        LOCAL_TURN_STEP = 0.9 * TURN_STEP
        headingStep = np.sign(segment.headingVariation) * LOCAL_TURN_STEP
        stepTime = segment.segmentDuration * LOCAL_TURN_STEP / abs(segment.headingVariation)

      print("-> Turn segment. Vertical mode: ", segment.verticalMode, " Speed mode: ", segment.speedMode)
      print("\tInitial time: ", segment.initialTime, " Target time: ", segment.targetTime)
      print("\tInitial speed: ", segment.initialSpeed, " Target speed: ", segment.targetSpeed)
      print("\tInitial altitude: ", segment.initialAltitude, " Target altitude: ", segment.targetAltitude)
      print("\tInitial heading: ", segment.initialHeading, " Target heading: ", segment.targetHeading)
      print("\tInitial position at  X: ", segment.initialPosition[0], " Y: ", segment.initialPosition[1], " Z: ", segment.initialPosition[2], " T: ", segment.initialPosition[3])

      cumulativeTime = 0

      idx = 0
      while idx < nSteps:
        currentHeading = ang.normalizeAngle(currentHeading + headingStep)
        v2 = ang.getVelocityVector2D(segment.targetSpeed, currentHeading)

        cumulativeTime += segment.segmentDuration * headingStep/ segment.headingVariation
        s4 = ang.linearTimeProjection4D(currentPosition, v2, stepTime)
        s4 = ang.assignTime4D(s4, segment.initialTime + cumulativeTime)
        s4 = ang.assignAltitude4D(s4, segment.targetAltitude)
        segment.intermediatePosition.append(s4)

        print("\tIntermediate turn point:")
        print("\t\tIntermediate time: ", s4[3])
        print("\t\tIntermediate altitude: ", s4[2])
        print("\t\tIntermediate heading: ", currentHeading)
        print("\t\tIntermediate position at  X: ", s4[0], " Y: ", s4[1], " Z: ", s4[2], " T: ", s4[3])

        currentPosition = s4
        idx += 1

      cumulativeTime = segment.segmentDuration - cumulativeTime
      v2 = ang.getVelocityVector2D(segment.targetSpeed, segment.targetHeading)
      s4 = ang.linearTimeProjection4D(currentPosition, v2, cumulativeTime)
      s4 = ang.assignTime4D(s4, segment.targetTime)
      s4 = ang.assignAltitude4D(s4, segment.targetAltitude)
      segment.targetPosition = s4

      print("\tTarget position at  X: ", segment.targetPosition[0], " Y: ", segment.targetPosition[1], " Z: ", segment.targetPosition[2], " T: ", segment.targetPosition[3])

    #
    # Hover segment
    if segment.horizontalMode == se.horizontalSegment.hover:

      # Vertical translation, determined by vehicle performance
      if segment.verticalMode == se.verticalSegment.climb:
        segment.initialPosition = ang.createVector4D(np.array([0, 0]), segment.initialAltitude, segment.initialTime)
        segment.targetPosition = ang.createVector4D(np.array([0, 0]), segment.targetAltitude, segment.targetTime)
      if segment.verticalMode == se.verticalSegment.descent:
        segment.initialPosition = ang.createVector4D(np.array([0, 0]), segment.initialAltitude, segment.initialTime)
        segment.targetPosition = ang.createVector4D(np.array([0, 0]), segment.targetAltitude, segment.targetTime)

      # Level hover, need time sampling
      if segment.verticalMode == se.verticalSegment.level:
        segment.initialPosition = ang.createVector4D(np.array([0, 0]), segment.initialAltitude, segment.initialTime)
        segment.targetPosition = ang.createVector4D(np.array([0, 0]), segment.targetAltitude, segment.targetTime)

      print("-> Hover segment. Vertical mode: ", segment.verticalMode)
      print("\tInitial time: ", segment.initialTime, " Target time: ", segment.targetTime)
      print("\tInitial speed: ", segment.initialSpeed, " Target speed: ", segment.targetSpeed)
      print("\tInitial altitude: ", segment.initialAltitude, " Target altitude: ", segment.targetAltitude)
      print("\tInitial heading: ", segment.initialHeading, " Target heading: ", segment.targetHeading)
      print("\tInitial position at  X: ", segment.initialPosition[0], " Y: ", segment.initialPosition[1], " Z: ", segment.initialPosition[2], " T: ", segment.initialPosition[3])
      print("\tTarget position at  X: ", segment.targetPosition[0], " Y: ", segment.targetPosition[1], " Z: ", segment.targetPosition[2], " T: ", segment.targetPosition[3])



  #
  # Position a forward segment based on the information for the segment
  ###########################################################################

  def positionForwardSegment(self, trajectory, segment, prevSegment):
    #
    # Take aircraft performance data
    perfClass = trajectory.NMACValues.Class

    #
    # Segment extrapolation
    segment.initialPosition = prevSegment.targetPosition
    #
    # Segment duration
    segment.initialTime = prevSegment.targetTime
    segment.targetTime = segment.initialTime + segment.segmentDuration

    #
    # Straight segment
    if segment.horizontalMode == se.horizontalSegment.straight:

      v2 = ang.getVelocityVector2D(segment.targetSpeed, segment.initialHeading)

      s4 =  ang.linearTimeProjection4D(segment.initialPosition, v2, segment.segmentDuration)
      s4 = ang.assignTime4D(s4, segment.initialTime + segment.segmentDuration)
      s4 = ang.assignAltitude4D(s4, segment.targetAltitude)
      segment.targetPosition = s4

      print("-> Straight segment. Vertical mode: ", segment.verticalMode, " Speed mode: ", segment.speedMode)
      print("\tInitial time: ", segment.initialTime, " Target time: ", segment.targetTime)
      print("\tInitial speed: ", segment.initialSpeed, " Target speed: ", segment.targetSpeed)
      print("\tInitial altitude: ", segment.initialAltitude, " Target altitude: ", segment.targetAltitude)
      print("\tInitial heading: ", segment.initialHeading, " Target heading: ", segment.targetHeading)
      print("\tInitial position at  X: ", segment.initialPosition[0], " Y: ", segment.initialPosition[1], " Z: ", segment.initialPosition[2], " T: ", segment.initialPosition[3])
      print("\tTarget position at  X: ", segment.targetPosition[0], " Y: ", segment.targetPosition[1], " Z: ", segment.targetPosition[2], " T: ", segment.targetPosition[3])

    #
    # Turn segment
    # Turns need to be divided into sub segments depending on the total heading change.
    # This current algorithm will subdivide in blocks of maximum 90 degrees each,
    # with time/distance divided proportionally.
    # Note that for that reason, intermediatePosition should become a list of waypoints.
    if segment.horizontalMode == se.horizontalSegment.turn:

      segment.intermediatePosition = []
      currentPosition = segment.initialPosition
      currentHeading = segment.initialHeading
      remainingHeading = segment.headingVariation
      headingStep = np.sign(segment.headingVariation) * TURN_STEP
      nSteps = abs(remainingHeading) // abs(headingStep)
      stepTime = segment.segmentDuration * TURN_STEP / abs(segment.headingVariation)

      if abs(remainingHeading) % abs(headingStep) < TURN_STEP * 0.25:
        LOCAL_TURN_STEP = 0.9 * TURN_STEP
        headingStep = np.sign(segment.headingVariation) * LOCAL_TURN_STEP
        stepTime = segment.segmentDuration * LOCAL_TURN_STEP / abs(segment.headingVariation)

      print("-> Turn segment. Vertical mode: ", segment.verticalMode, " Speed mode: ", segment.speedMode)
      print("\tInitial time: ", segment.initialTime, " Target time: ", segment.targetTime)
      print("\tInitial speed: ", segment.initialSpeed, " Target speed: ", segment.targetSpeed)
      print("\tInitial altitude: ", segment.initialAltitude, " Target altitude: ", segment.targetAltitude)
      print("\tInitial heading: ", segment.initialHeading, " Target heading: ", segment.targetHeading)
      print("\tInitial position at  X: ", segment.initialPosition[0], " Y: ", segment.initialPosition[1], " Z: ", segment.initialPosition[2], " T: ", segment.initialPosition[3])

      cumulativeTime = 0

      idx = 0
      while idx < nSteps:
        currentHeading = ang.normalizeAngle(currentHeading + headingStep)
        v2 = ang.getVelocityVector2D(segment.targetSpeed, currentHeading)

        cumulativeTime += segment.segmentDuration * headingStep/ segment.headingVariation
        s4 = ang.linearTimeProjection4D(currentPosition, v2, stepTime)
        s4 = ang.assignTime4D(s4, segment.initialTime + cumulativeTime)
        s4 = ang.assignAltitude4D(s4, segment.targetAltitude)
        segment.intermediatePosition.append(s4)

        print("\tIntermediate turn point forward:")
        print("\t\tIntermediate time: ", s4[3])
        print("\t\tIntermediate altitude: ", s4[2])
        print("\t\tIntermediate heading: ", currentHeading)
        print("\t\tIntermediate position at  X: ", s4[0], " Y: ", s4[1], " Z: ", s4[2], " T: ", s4[3])

        currentPosition = s4
        idx += 1

      cumulativeTime = segment.segmentDuration - cumulativeTime
      v2 = ang.getVelocityVector2D(segment.targetSpeed, segment.targetHeading)
      s4 = ang.linearTimeProjection4D(currentPosition, v2, cumulativeTime)
      s4 = ang.assignTime4D(s4, segment.initialTime + segment.segmentDuration)
      s4 = ang.assignAltitude4D(s4, segment.targetAltitude)

      segment.targetPosition = s4
      print("\tTarget position at  X: ", segment.targetPosition[0], " Y: ", segment.targetPosition[1], " Z: ", segment.targetPosition[2], " T: ", segment.targetPosition[3])

    #
    # Hover segment
    if segment.horizontalMode == se.horizontalSegment.hover:

      print("-> Hover segment. Vertical mode: ", segment.verticalMode)
      print("\tInitial time: ", segment.initialTime, " Target time: ", segment.targetTime)
      print("\tInitial speed: ", segment.initialSpeed, " Target speed: ", segment.targetSpeed)
      print("\tInitial altitude: ", segment.initialAltitude, " Target altitude: ", segment.targetAltitude)
      print("\tInitial heading: ", segment.initialHeading, " Target heading: ", segment.targetHeading)
      print("\tInitial position at  X: ", segment.initialPosition[0], " Y: ", segment.initialPosition[1], " Z: ", segment.initialPosition[2], " T: ", segment.initialPosition[3])

      if segment.initialSpeed > 0:
        # Hover mode, speed is 0
        # Initial speed could be greather than 0, so a deceleration will be needed.
        t, d = getDecelerationTimeDistance(segment.initialSpeed)
        print("Decelerating hover segment of duration: ", t, " out of total:", segment.segmentDuration)

        v2 = ang.getUnitaryVector2D(segment.initialHeading)
        s4 = ang.linearDistanceProjection4D(segment.initialPosition, v2, d)
        s4 = ang.assignTime4D(s4, segment.initialTime + t)
        s4 = ang.assignAltitude4D(s4, segment.initialAltitude)
        segment.intermediatePosition.append(s4)
        initialHoverPoint = s4
        print("\tIntermediate position at  X: ", initialHoverPoint[0], " Y: ", initialHoverPoint[1], " Z: ", initialHoverPoint[2], " T: ", initialHoverPoint[3])
      else:
        initialHoverPoint = segment.initialPosition

      # Vertical translation, determined by vehicle performance
      if segment.verticalMode == se.verticalSegment.climb:
        segment.targetPosition = ang.duplicateVector4D(initialHoverPoint, segment.targetAltitude, segment.initialTime + segment.segmentDuration)

      if segment.verticalMode == se.verticalSegment.descent:
        segment.targetPosition = ang.duplicateVector4D(initialHoverPoint, segment.targetAltitude, segment.initialTime + segment.segmentDuration)

      # Level hover, need time sampling
      if segment.verticalMode == se.verticalSegment.level:
        segment.targetPosition = ang.duplicateVector4D(initialHoverPoint, segment.targetAltitude, segment.initialTime + segment.segmentDuration)
      print("\tTarget position at  X: ", segment.targetPosition[0], " Y: ", segment.targetPosition[1], " Z: ", segment.targetPosition[2], " T: ", segment.targetPosition[3])

  #
  # Position a backward segment based on the information for the segment
  ###########################################################################

  def positionBackwardSegment(self, trajectory, segment, nextSegment):
    #
    # Take aircraft performance data
    perfClass = trajectory.NMACValues.Class

    #
    # Segment extrapolation
    segment.targetPosition = nextSegment.initialPosition
    #
    # Segment duration
    segment.targetTime = nextSegment.initialTime
    segment.initialTime = nextSegment.initialTime - segment.segmentDuration

    #
    # Straight segment
    if segment.horizontalMode == se.horizontalSegment.straight:

      v2 = ang.getVelocityVector2D(segment.targetSpeed, ang.normalizeAngle(segment.initialHeading + 180))

      s4 =  ang.linearTimeProjection4D(segment.targetPosition, v2, segment.segmentDuration)
      s4 = ang.assignTime4D(s4, segment.targetTime - segment.segmentDuration)
      s4 = ang.assignAltitude4D(s4, segment.initialAltitude)
      segment.initialPosition = s4

      print("-> Straight segment. Vertical mode: ", segment.verticalMode, " Speed mode: ", segment.speedMode)
      print("\tInitial time: ", segment.initialTime, " Target time: ", segment.targetTime)
      print("\tInitial speed: ", segment.initialSpeed, " Target speed: ", segment.targetSpeed)
      print("\tInitial altitude: ", segment.initialAltitude, " Target altitude: ", segment.targetAltitude)
      print("\tInitial heading: ", segment.initialHeading, " Target heading: ", segment.targetHeading)
      print("\tInitial position at  X: ", segment.initialPosition[0], " Y: ", segment.initialPosition[1], " Z: ", segment.initialPosition[2], " T: ", segment.initialPosition[3])
      print("\tTarget position at  X: ", segment.targetPosition[0], " Y: ", segment.targetPosition[1], " Z: ", segment.targetPosition[2], " T: ", segment.targetPosition[3])

    #
    # Turn segment
    # Turns need to be divided into sub segments depending on the total heading change.
    # This current algorithm will subdivide in blocks of maximum 90 degrees each,
    # with time/distance divided proportionally.
    # Note that for that reason, intermediatePosition should become a list of waypoints.
    if segment.horizontalMode == se.horizontalSegment.turn:

      segment.intermediatePosition = []
      currentPosition = segment.targetPosition
      currentHeading = segment.targetHeading
      remainingHeading = segment.headingVariation
      headingStep = np.sign(segment.headingVariation) * TURN_STEP  # Inverse orientation of the turns
      nSteps = abs(remainingHeading) // abs(headingStep)
      stepTime = segment.segmentDuration * TURN_STEP / abs(segment.headingVariation)

      if abs(remainingHeading) % abs(headingStep) < TURN_STEP * 0.25:
        print("-> Turn segment. Small discriminant detected: ", abs(remainingHeading) % abs(headingStep))
        LOCAL_TURN_STEP = 0.9 * TURN_STEP
        print("-> Turn segment. Turn step reduced to: ", LOCAL_TURN_STEP)
        headingStep = np.sign(segment.headingVariation) * LOCAL_TURN_STEP
        stepTime = segment.segmentDuration * LOCAL_TURN_STEP / abs(segment.headingVariation)

      print("-> Turn segment. Vertical mode: ", segment.verticalMode, " Speed mode: ", segment.speedMode)
      print("\tInitial time: ", segment.initialTime, " Target time: ", segment.targetTime)
      print("\tInitial speed: ", segment.initialSpeed, " Target speed: ", segment.targetSpeed)
      print("\tInitial altitude: ", segment.initialAltitude, " Target altitude: ", segment.targetAltitude)
      print("\tInitial heading: ", segment.initialHeading, " Target heading: ", segment.targetHeading)
      print("\tTarget position at  X: ", segment.targetPosition[0], " Y: ", segment.targetPosition[1], " Z: ", segment.targetPosition[2], " T: ", segment.targetPosition[3])

      initialTime  = segment.segmentDuration - stepTime * nSteps
      initialHeading = segment.headingVariation - headingStep * nSteps
      cumulativeTime = initialTime

      speed = (segment.targetSpeed + segment.initialSpeed)/2

      v2 = ang.getVelocityVector2D(speed, ang.normalizeAngle(currentHeading + 180))

      s4 = ang.linearTimeProjection4D(currentPosition, v2, initialTime)
      s4 = ang.assignTime4D(s4, segment.targetTime - cumulativeTime)
      s4 = ang.assignAltitude4D(s4, segment.targetAltitude)
      currentPosition = s4

      remainingHeading = remainingHeading - initialHeading
      currentHeading = ang.normalizeAngle(segment.targetHeading - initialHeading)

      if nSteps > 0:
        segment.intermediatePosition.insert(0, s4)
        print("\tIntermediate turn point backwards:")
        print("\t\tIntermediate time: ", s4[3])
        print("\t\tIntermediate altitude: ", s4[2])
        print("\t\tIntermediate heading: ", currentHeading)
        print("\t\tIntermediate position at  X: ", s4[0], " Y: ", s4[1], " Z: ", s4[2], " T: ", s4[3])

      idx = 0
      while idx < nSteps:
        v2 = ang.getVelocityVector2D(speed, ang.normalizeAngle(currentHeading + 180))
        cumulativeTime += stepTime
        s4 = ang.linearTimeProjection4D(currentPosition, v2, stepTime)
        s4 = ang.assignTime4D(s4, segment.targetTime - cumulativeTime)
        s4 = ang.assignAltitude4D(s4, segment.targetAltitude)

        currentPosition = s4
        currentHeading = ang.normalizeAngle(currentHeading - headingStep)

        if idx < nSteps -1:
          segment.intermediatePosition.insert(0, s4)
          print("\tIntermediate turn point backwards:")
          print("\t\tIntermediate time: ", s4[3])
          print("\t\tIntermediate altitude: ", s4[2])
          print("\t\tIntermediate heading: ", currentHeading)
          print("\t\tIntermediate position at  X: ", s4[0], " Y: ", s4[1], " Z: ", s4[2], " T: ", s4[3])

        idx += 1

      s4 = ang.assignAltitude4D(s4, segment.initialAltitude)

      segment.initialPosition = s4
      print("\tInitial position at  X: ", segment.initialPosition[0], " Y: ", segment.initialPosition[1], " Z: ", segment.initialPosition[2], " T: ", segment.initialPosition[3])

    #
    # Hover segment
    if segment.horizontalMode == se.horizontalSegment.hover:

      # Vertical translation, determined by vehicle performance
      if segment.verticalMode == se.verticalSegment.climb:
        segment.initialPosition = ang.duplicateVector4D(segment.targetPosition, segment.initialAltitude, segment.targetTime - segment.segmentDuration)

      if segment.verticalMode == se.verticalSegment.descent:
        segment.initialPosition = ang.duplicateVector4D(segment.targetPosition, segment.initialAltitude, segment.targetTime - segment.segmentDuration)

      # Level hover, need time sampling
      if segment.verticalMode == se.verticalSegment.level:
        segment.initialPosition = ang.duplicateVector4D(segment.targetPosition, segment.initialAltitude, segment.targetTime - segment.segmentDuration)

      print("-> Hover segment. Vertical mode: ", segment.verticalMode)
      print("\tInitial time: ", segment.initialTime, " Target time: ", segment.targetTime)
      print("\tInitial speed: ", segment.initialSpeed, " Target speed: ", segment.targetSpeed)
      print("\tInitial altitude: ", segment.initialAltitude, " Target altitude: ", segment.targetAltitude)
      print("\tInitial heading: ", segment.initialHeading, " Target heading: ", segment.targetHeading)
      print("\tInitial position at  X: ", segment.initialPosition[0], " Y: ", segment.initialPosition[1], " Z: ", segment.initialPosition[2], " T: ", segment.initialPosition[3])
      print("\tTarget position at  X: ", segment.targetPosition[0], " Y: ", segment.targetPosition[1], " Z: ", segment.targetPosition[2], " T: ", segment.targetPosition[3])

