import math as math
import Util.angles as agl
import numpy as np
from DEG_Encounter import storeTrajectory as st


#MARC class name should be the same as file name
class NMACRotationParameters:
  def __init__(self):
    # Reference to the encounter
    self.encounter = None
    # VMD as for the encounter
    self.VMD = 0.0
    # HMD as for the encounter
    self.HMD = 0.0
    # Theta as for the encounter
    self.Theta = 0.0
    # Index of the reference point at the ownship trajectory
    self.ownshipIdx = 0
    # Point for the ownship trajectory [time, X, Y, Z, VX, VY, VZ]
    self.ownshipPoint = None
    # Computed heading at the NMAC ownship reference point
    self.ownshipHeading = 0
    # Computed speed at the NMAC ownship reference point
    self.ownshipSpeed = 0
    # Computed altitude at the NMAC ownship reference point
    self.ownshipAltitude = 0
    # Computed vertical speed at the NMAC ownship reference point
    self.ownshipVerticalSpeed = 0
    # Index of the reference point at the intruder trajectory
    self.intrIdx = 0
    # Computed heading at the NMAC intruder reference point
    self.intrHeading = 0
    # Computed speed at the NMAC intruder reference point
    self.intrSpeed = 0
    # Computed altitude at the NMAC intruder reference point
    self.intrAltitude = 0
    # Computed vertical speed at the NMAC intruder reference point
    self.intrVerticalSpeed = 0
    # Point for the ownship trajectory [time, X, Y, Z, VX, VY, VZ]
    self.intrPoint = None


  def setParametersFromEncounter(self, encounter):
    self.VMD = encounter.NMACValues.VMD
    self.HMD = encounter.NMACValues.HMD
    self.Theta = encounter.NMACValues.AppAngle
    self.encounter = encounter


  def fillOwnshipFromIdx(self):
    extraIdx = 0

    if self.encounter.ownshipTrajectory.extraSegment != None:
      print("Trying to Identify Extra Segment point at time: ", self.encounter.ownshipTrajectory.extraSegment.targetPosition[3])
      extraIdx = self.encounter.ownshipTrajectory.identifyReferenceIdx(
        self.encounter.ownshipTrajectory.extraSegment.targetPosition, 0)

    print("Trying to Identify Initial NMAC point at time: ", self.encounter.ownshipTrajectory.NMACSegment.initialPosition[3])
    initialIdx = self.encounter.ownshipTrajectory.identifyReferenceIdx(
      self.encounter.ownshipTrajectory.NMACSegment.initialPosition, extraIdx)
    print("Trying to Identify Final NMAC point at time: ", self.encounter.ownshipTrajectory.NMACSegment.targetPosition[3])
    targetIdx = self.encounter.ownshipTrajectory.identifyReferenceIdx(
      self.encounter.ownshipTrajectory.NMACSegment.targetPosition, initialIdx)

    selIdx = int((targetIdx - initialIdx) * self.encounter.ownshipTrajectory.NMACSegment.NMACFactor + initialIdx)
    #MARC: millor arodonir, no?

    current = self.encounter.ownshipTrajectory.trajectorySequence[selIdx]
    next = self.encounter.ownshipTrajectory.trajectorySequence[selIdx + 1]

    self.ownshipPoint = current
    self.ownshipIdx = selIdx
    self.ownshipHeading = agl.normalize(90 - (180 / math.pi) * math.atan2(next[2] - current[2], next[1] - current[1]))

    self.ownshipSpeed = math.sqrt(self.ownshipPoint[4]**2 + self.ownshipPoint[5]**2)
    self.ownshipVerticalSpeed = self.ownshipPoint[6]
    self.ownshipAltitude = self.ownshipPoint[3]

    if math.isclose(self.ownshipSpeed, 0):

      idx = selIdx + 1
      while math.isclose(self.encounter.ownshipTrajectory.trajectorySequence[idx][4], 0) and math.isclose(self.encounter.ownshipTrajectory.trajectorySequence[idx][5], 0):
        idx = idx + 1

      current = self.encounter.ownshipTrajectory.trajectorySequence[idx]
      next = self.encounter.ownshipTrajectory.trajectorySequence[idx + 1]
      fwdHeading = agl.normalize(90 - (180 / math.pi) * math.atan2(next[2] - current[2], next[1] - current[1]))

      idx = selIdx - 1
      while math.isclose(self.encounter.ownshipTrajectory.trajectorySequence[idx][4], 0) and math.isclose(self.encounter.ownshipTrajectory.trajectorySequence[idx][5], 0):
        idx = idx - 1

      next = self.encounter.ownshipTrajectory.trajectorySequence[idx]
      current = self.encounter.ownshipTrajectory.trajectorySequence[idx - 1]
      bwdHeading = agl.normalize(90 - (180 / math.pi) * math.atan2(next[2] - current[2], next[1] - current[1]))

      self.ownHeading = agl.normalize((fwdHeading + bwdHeading) / 2)

    print("Identified NMAC point with time: ", self.ownshipPoint[0], " and heading: ", self.ownshipHeading)


  def fillIntruderFromIdx(self):
    extraIdx = 0

    if self.encounter.intrTrajectory.extraSegment != None:
      extraIdx = self.encounter.intrTrajectory.identifyReferenceIdx(self.encounter.intrTrajectory.extraSegment.targetPosition, 0)

    initialIdx = self.encounter.intrTrajectory.identifyReferenceIdx(
      self.encounter.intrTrajectory.NMACSegment.initialPosition, extraIdx)
    targetIdx = self.encounter.intrTrajectory.identifyReferenceIdx(
      self.encounter.intrTrajectory.NMACSegment.targetPosition, initialIdx)

    # MARC millor arrodonir, no?
    selIdx = int((targetIdx - initialIdx) * self.encounter.intrTrajectory.NMACSegment.NMACFactor + initialIdx)

    current = self.encounter.intrTrajectory.trajectorySequence[selIdx]
    next = self.encounter.intrTrajectory.trajectorySequence[selIdx + 1]

    self.intrPoint = current
    self.intrIdx = selIdx
    self.intrHeading = agl.normalize(90 - (180 / math.pi) * math.atan2(next[2] - current[2], next[1] - current[1]))

    self.intrSpeed = math.sqrt(self.intrPoint[4]**2 + self.intrPoint[5]**2)
    self.intrVerticalSpeed = self.intrPoint[6]
    self.intrAltitude = self.intrPoint[3]

    if math.isclose(self.intrSpeed, 0):

      idx = selIdx + 1
      while math.isclose(self.encounter.intrTrajectory.trajectorySequence[idx][4], 0) and math.isclose(self.encounter.intrTrajectory.trajectorySequence[idx][5], 0):
        idx = idx + 1

      current = self.encounter.intrTrajectory.trajectorySequence[idx]
      next = self.encounter.intrTrajectory.trajectorySequence[idx + 1]
      fwdHeading = agl.normalize(90 - (180 / math.pi) * math.atan2(next[2] - current[2], next[1] - current[1]))

      idx = selIdx - 1
      while math.isclose(self.encounter.intrTrajectory.trajectorySequence[idx][4], 0) and math.isclose(self.encounter.intrTrajectory.trajectorySequence[idx][5], 0):
        idx = idx - 1

      next = self.encounter.intrTrajectory.trajectorySequence[idx]
      current = self.encounter.intrTrajectory.trajectorySequence[idx - 1]
      bwdHeading = agl.normalize(90 - (180 / math.pi) * math.atan2(next[2] - current[2], next[1] - current[1]))

      self.intrHeading = agl.normalize((fwdHeading + bwdHeading) / 2)

    print("Identified NMAC point with time: ", self.intrPoint[0], " and heading: ", self.intrHeading)


  def findFirstPositiveTimeIdx(self, trajectorySequence, idx):

    for i in range(len(trajectorySequence)):
      if trajectorySequence[i][idx] >= 0:
        return i

    return 0


  def alignTimeIndex(self):

    # Determine time for each trajectory
    ownshipTime = self.encounter.ownshipTrajectory.trajectorySequence[self.ownshipIdx][0]
    intruderTime = self.encounter.intrTrajectory.trajectorySequence[self.intrIdx][0]

    print("Time at NMAC, for Ownship: ", ownshipTime, " Intruder: ", intruderTime)

    if self.encounter.ownshipTrajectory.extraSegment != None:
      ownshipTime -= 5

    if self.encounter.intrTrajectory.extraSegment != None:
      intruderTime -= 5


    # The longest trajectory needs to be readjusted to aling both of them

    if ownshipTime > intruderTime:
      delta = round(ownshipTime - intruderTime, 2)
      self.encounter.ownshipTrajectory.trajectorySequence = self.encounter.ownshipTrajectory.trajectorySequence - np.array([delta, 0, 0, 0, 0, 0, 0])
      self.encounter.ownshipTrajectory.flownSequence = self.encounter.ownshipTrajectory.flownSequence - np.array([0, delta, 0, 0, 0])
      #self.encounter.ownshipTrajectory.timeAfterNMAC -= delta
      #self.encounter.ownshipTrajectory.timeBeforeNMAC += delta
    else:
      delta = round(intruderTime - ownshipTime, 2)
      self.encounter.intrTrajectory.trajectorySequence = self.encounter.intrTrajectory.trajectorySequence - np.array([delta, 0, 0, 0, 0, 0, 0])
      self.encounter.intrTrajectory.flownSequence = self.encounter.intrTrajectory.flownSequence - np.array([0, delta, 0, 0, 0])
      #self.encounter.intrTrajectory.timeAfterNMAC -= delta
      #self.encounter.intrTrajectory.timeBeforeNMAC += delta


    #self.encounter.ownshipTrajectory.timeAfterNMAC -= 5
    #self.encounter.intrTrajectory.timeAfterNMAC -= 5

    if self.encounter.ownshipTrajectory.extraSegment != None:
      #self.encounter.ownshipTrajectory.timeBeforeNMAC -= 5
      self.encounter.ownshipTrajectory.trajectorySequence = self.encounter.ownshipTrajectory.trajectorySequence - np.array([5.0, 0, 0, 0, 0, 0, 0])
      self.encounter.ownshipTrajectory.flownSequence = self.encounter.ownshipTrajectory.flownSequence - np.array([0, 5.0, 0, 0, 0])

    if self.encounter.intrTrajectory.extraSegment != None:
      #self.encounter.intrTrajectory.timeBeforeNMAC -= 5
      self.encounter.intrTrajectory.trajectorySequence = self.encounter.intrTrajectory.trajectorySequence - np.array([5.0, 0, 0, 0, 0, 0, 0])
      self.encounter.intrTrajectory.flownSequence = self.encounter.intrTrajectory.flownSequence - np.array([0, 5.0, 0, 0, 0])


    ownshipTime = self.encounter.ownshipTrajectory.trajectorySequence[self.ownshipIdx][0]
    intruderTime = self.encounter.intrTrajectory.trajectorySequence[self.intrIdx][0]

    print("Now, time at NMAC, for Ownship: ", ownshipTime, " Intruder: ", intruderTime)

    startIdxOwnship = self.findFirstPositiveTimeIdx(self.encounter.ownshipTrajectory.trajectorySequence, 0)
    startIdxIntruder = self.findFirstPositiveTimeIdx(self.encounter.intrTrajectory.trajectorySequence, 0)

    print("Start indexes for Ownship: ", startIdxOwnship, " Intruder: ", startIdxIntruder)

    mask = np.ones(len(self.encounter.ownshipTrajectory.trajectorySequence), dtype=bool)
    mask[0:startIdxOwnship] = False
    self.encounter.ownshipTrajectory.trajectorySequence = self.encounter.ownshipTrajectory.trajectorySequence[mask]
    self.encounter.ownshipTrajectory.flownSequence = self.encounter.ownshipTrajectory.flownSequence - np.array([startIdxOwnship, 0, 0, 0, 0])
    self.ownshipIdx -= startIdxOwnship

    mask = np.ones(len(self.encounter.intrTrajectory.trajectorySequence), dtype=bool)
    mask[0:startIdxIntruder] = False
    self.encounter.intrTrajectory.trajectorySequence = self.encounter.intrTrajectory.trajectorySequence[mask]
    self.encounter.intrTrajectory.flownSequence = self.encounter.intrTrajectory.flownSequence - np.array([startIdxIntruder, 0, 0, 0, 0])
    self.intrIdx -= startIdxIntruder


    ownshipTime = self.encounter.ownshipTrajectory.trajectorySequence[self.ownshipIdx][0]
    intruderTime = self.encounter.intrTrajectory.trajectorySequence[self.intrIdx][0]


    totalOwnshipTime = self.encounter.ownshipTrajectory.trajectorySequence[-1][0]
    totalIntruderTime = self.encounter.intrTrajectory.trajectorySequence[-1][0]

    totalOwnshipLen = len(self.encounter.ownshipTrajectory.trajectorySequence)
    totalIntruderLen = len(self.encounter.intrTrajectory.trajectorySequence)

    #
    # Find the longest trajectory and cut it to make both equal
    if totalOwnshipLen > totalIntruderLen:
      mask = np.ones(len(self.encounter.ownshipTrajectory.trajectorySequence), dtype=bool)
      mask[0:totalIntruderLen] = True
      mask[totalIntruderLen:totalOwnshipLen] = False
      self.encounter.ownshipTrajectory.trajectorySequence = self.encounter.ownshipTrajectory.trajectorySequence[mask]

      mask = np.ones(len(self.encounter.ownshipTrajectory.flownSequence), dtype=bool)
      idx = 0
      while idx < len(self.encounter.ownshipTrajectory.flownSequence):
        if self.encounter.ownshipTrajectory.flownSequence[idx][0] >= totalIntruderLen:
          mask[idx] = False
        idx = idx + 1

      self.encounter.ownshipTrajectory.flownSequence = self.encounter.ownshipTrajectory.flownSequence[mask]

    elif totalOwnshipLen < totalIntruderLen:
      mask = np.ones(len(self.encounter.intrTrajectory.trajectorySequence), dtype=bool)
      mask[0:totalOwnshipLen] = True
      mask[totalOwnshipLen:totalIntruderLen] = False
      self.encounter.intrTrajectory.trajectorySequence = self.encounter.intrTrajectory.trajectorySequence[mask]

      mask = np.ones(len(self.encounter.intrTrajectory.flownSequence), dtype=bool)
      idx = 0
      while idx < len(self.encounter.intrTrajectory.flownSequence):
        if self.encounter.intrTrajectory.flownSequence[idx][0] >= totalOwnshipLen:
          mask[idx] = False
        idx = idx + 1

      self.encounter.intrTrajectory.flownSequence = self.encounter.intrTrajectory.flownSequence[mask]


    totalOwnshipTime = self.encounter.ownshipTrajectory.trajectorySequence[-1][0]
    totalIntruderTime = self.encounter.intrTrajectory.trajectorySequence[-1][0]

    self.encounter.ownshipTrajectory.timeBeforeNMAC = ownshipTime
    self.encounter.intrTrajectory.timeBeforeNMAC = intruderTime
    self.encounter.ownshipTrajectory.timeAfterNMAC = totalOwnshipTime - ownshipTime
    self.encounter.intrTrajectory.timeAfterNMAC = totalIntruderTime - intruderTime

    print("Time at NMAC, for Ownship: ", ownshipTime, " Intruder: ", intruderTime)
    print("Time before NMAC, for Ownship: ", self.encounter.ownshipTrajectory.timeBeforeNMAC)
    print("Time before NMAC, for Intruder: ", self.encounter.intrTrajectory.timeBeforeNMAC)


    startIdxOwnship = self.findFirstPositiveTimeIdx(self.encounter.ownshipTrajectory.flownSequence, 1)

    mask = np.ones(len(self.encounter.ownshipTrajectory.flownSequence), dtype=bool)
    mask[0:startIdxOwnship] = False
    self.encounter.ownshipTrajectory.flownSequence = self.encounter.ownshipTrajectory.flownSequence[mask]

    startIdxIntruder = self.findFirstPositiveTimeIdx(self.encounter.intrTrajectory.flownSequence, 1)

    mask = np.ones(len(self.encounter.intrTrajectory.flownSequence), dtype=bool)
    mask[0:startIdxIntruder] = False
    self.encounter.intrTrajectory.flownSequence = self.encounter.intrTrajectory.flownSequence[mask]



  # def rotate2 (self):
  #   ownshipTrj_subset = np.transpose(np.array([self.encounter.ownshipTrajectory.trajectorySequence[:,1],
  #                                              self.encounter.ownshipTrajectory.trajectorySequence[:,2]]))
  #
  #   intruderTrj_subset = np.transpose(np.array([self.encounter.intrTrajectory.trajectorySequence[:,1],
  #                                               self.encounter.intrTrajectory.trajectorySequence[:,2]]))
  #
  #   beta = np.deg2rad(self.intrHeading - self.ownshipHeading)



  def updateIntruderFromIdx(self):

    current = self.encounter.intrTrajectory.trajectorySequence[self.intrIdx]
    next = self.encounter.intrTrajectory.trajectorySequence[self.intrIdx + 1]

    self.intrPoint = current
    self.intrHeading = agl.normalize(90 - (180 / math.pi) * math.atan2(next[2] - current[2], next[1] - current[1]))

    #self.intrSpeed = math.sqrt(self.intrPoint[4]**2 + self.intrPoint[5]**2)
    #self.intrVerticalSpeed = self.intrPoint[6]
    self.intrAltitude = self.intrPoint[3]


  def rotate2(self, hmd, vmd, deltaH):
    # Step 0: get subsets of trajectories (x,y coordinates)
    ownshipTrj_subset_pos = np.transpose(np.array([self.encounter.ownshipTrajectory.trajectorySequence[:, 1],
                                               self.encounter.ownshipTrajectory.trajectorySequence[:,2]]))

    intruderTrj_subset_pos = np.transpose(np.array([self.encounter.intrTrajectory.trajectorySequence[:, 1],
                                                self.encounter.intrTrajectory.trajectorySequence[:,2]]))

    ownshipTrj_subset_vel = np.transpose(np.array([self.encounter.ownshipTrajectory.trajectorySequence[:,4],
                                                  self.encounter.ownshipTrajectory.trajectorySequence[:,5]]))

    intruderTrj_subset_vel = np.transpose(np.array([self.encounter.intrTrajectory.trajectorySequence[:,4],
                                                   self.encounter.intrTrajectory.trajectorySequence[:,5]]))

    # Step 1: ownship: eliminate offset on (x,y coordinates)
    ownshipTrj_subset_pos = ownshipTrj_subset_pos - np.array([self.ownshipPoint[1], self.ownshipPoint[2]])
    intruderTrj_subset_pos = intruderTrj_subset_pos - np.array([self.intrPoint[1], self.intrPoint[2]])

    # Step 3: eliminate offset heading for both intruder and ownship trajectories
    theta = np.radians(self.ownshipHeading)
    print("theta ownship")
    print(self.ownshipHeading)
    # rotate positions and speeds (ownship)
    c, s = np.cos(theta), np.sin(theta)
    R = np.array([[c, -s], [s, c]])
    ownshipTrj_subset_pos = np.transpose(np.dot(R, np.transpose(ownshipTrj_subset_pos)))
    ownshipTrj_subset_vel = np.transpose(np.dot(R, np.transpose(ownshipTrj_subset_vel)))

    # rotate positions and speeds (intruder)
    theta = np.radians(self.intrHeading)
    print("theta intruder")
    print(self.intrHeading)
    c, s = np.cos(theta), np.sin(theta)
    R = np.array([[c, -s], [s, c]])
    intruderTrj_subset_pos = np.transpose(np.dot(R, np.transpose(intruderTrj_subset_pos)))
    intruderTrj_subset_vel = np.transpose(np.dot(R, np.transpose(intruderTrj_subset_vel)))

    # Propagate to the rest of the intruder's Z trajectory
    deltaZintr = self.ownshipPoint[3] - self.intrPoint[3] + vmd
    self.encounter.intrTrajectory.trajectorySequence = self.encounter.intrTrajectory.trajectorySequence + np.array([0, 0, 0, deltaZintr, 0, 0, 0])

    # Up to here both trajectories are set to 0 degrees and correct altitudes
    # Trajectories are re-stored into their objects

    self.encounter.ownshipTrajectory.trajectorySequence[:,1] = ownshipTrj_subset_pos[:, 0]
    self.encounter.ownshipTrajectory.trajectorySequence[:,2] = ownshipTrj_subset_pos[:, 1]
    self.encounter.ownshipTrajectory.trajectorySequence[:,4] = ownshipTrj_subset_vel[:, 0]
    self.encounter.ownshipTrajectory.trajectorySequence[:,5] = ownshipTrj_subset_vel[:, 1]

    self.encounter.intrTrajectory.trajectorySequence[:,1] = intruderTrj_subset_pos[:, 0]
    self.encounter.intrTrajectory.trajectorySequence[:,2] = intruderTrj_subset_pos[:, 1]
    self.encounter.intrTrajectory.trajectorySequence[:,4] = intruderTrj_subset_vel[:, 0]
    self.encounter.intrTrajectory.trajectorySequence[:,5] = intruderTrj_subset_vel[:, 1]

    st.plotEncounterResults("INT_ENC_", self.encounter.ownshipTrajectory.trajectorySequence,
                            self.encounter.ownshipTrajectory.flownSequence,
                            self.ownshipIdx, self.encounter.intrTrajectory.trajectorySequence,
                            self.encounter.intrTrajectory.flownSequence, self.intrIdx, self.encounter.getOwnshipFigurePath(),
                            self.encounter, True)


    #Encounter:: encounter geometry at CPA (CAFE_WP2_GEOMETRY.PDF)
    beta = np.deg2rad(deltaH)
    vown = math.sqrt(self.ownshipPoint[4] ** 2 + self.ownshipPoint[5] ** 2)
    vint = math.sqrt(self.intrPoint[4] ** 2 + self.intrPoint[5] ** 2)

    #vown = np.linalg.norm(self.ownshipPoint[4], self.ownshipPoint[5])
    #vint = np.linalg.norm( self.intrPoint[4], self.intrPoint[5])

    cot_beta = 1 / np.tan(beta)
    csc_beta = 1 / np.sin(beta)

    #Encounter:: encounter geometry definition (2.8)
    if np.isclose(vint, 0):
      chi = np.pi/2
    else:
      chi = np.arctan(vown/vint*csc_beta-cot_beta) #+ np.deg2rad(180) Alternative point 180o appart

    chi_deg = np.rad2deg(chi)
    print("chi degrees: " + str(chi_deg))

    deltax = hmd * np.sin(chi)
    deltay = hmd * np.cos(chi)

    # Step 4: rotate intruder's trajectory
    # rotate trajectory points
    theta = -np.deg2rad(deltaH)
    c, s = np.cos(theta), np.sin(theta)
    R = np.array([[c, -s], [s, c]])
    intruderTrj_subset_pos = np.transpose(np.dot(R, np.transpose(intruderTrj_subset_pos)))
    intruderTrj_subset_vel = np.transpose(np.dot(R, np.transpose(intruderTrj_subset_vel)))

    intruderTrj_subset_pos = intruderTrj_subset_pos + np.array([deltax, deltay])

    self.encounter.ownshipTrajectory.trajectorySequence[:,1] = ownshipTrj_subset_pos[:, 0]
    self.encounter.ownshipTrajectory.trajectorySequence[:,2] = ownshipTrj_subset_pos[:, 1]
    self.encounter.ownshipTrajectory.trajectorySequence[:,4] = ownshipTrj_subset_vel[:, 0]
    self.encounter.ownshipTrajectory.trajectorySequence[:,5] = ownshipTrj_subset_vel[:, 1]

    self.encounter.intrTrajectory.trajectorySequence[:,1] = intruderTrj_subset_pos[:, 0]
    self.encounter.intrTrajectory.trajectorySequence[:,2] = intruderTrj_subset_pos[:, 1]
    self.encounter.intrTrajectory.trajectorySequence[:,4] = intruderTrj_subset_vel[:, 0]
    self.encounter.intrTrajectory.trajectorySequence[:,5] = intruderTrj_subset_vel[:, 1]


  def rotate(self, deltaX, deltaY, deltaZ, deltaH):
    # Step 0: get subsets of trajectories (x,y coordinates)
    ownshipTrj_subset = np.transpose(np.array([self.encounter.ownshipTrajectory.trajectorySequence[:,1],
                                   self.encounter.ownshipTrajectory.trajectorySequence[:,2]]))

    intruderTrj_subset = np.transpose(np.array([self.encounter.intrTrajectory.trajectorySequence[:,1],
                                   self.encounter.intrTrajectory.trajectorySequence[:,2]]))

    # Step 1: ownship: eliminate offset on (x,y coordinates)
    ownshipTrj_subset = ownshipTrj_subset - np.array([self.ownshipPoint[1], self.ownshipPoint[2]])


    # Step 2: intruder: eliminate offset
    # Store deltaIntr (maybe not necessary)
    deltaXintr = self.intrPoint[1] - deltaX
    deltaYintr = self.intrPoint[2] - deltaY
    deltaZintr = self.ownshipPoint[3] - self.intrPoint[3] + deltaZ

        # Propagate to the rest of the intruder's X, Y trajectory
    intruderTrj_subset = intruderTrj_subset - np.array([self.intrPoint[1], self.intrPoint[2]])

    # Propagate to the rest of the intruder's Z trajectory
    self.encounter.intrTrajectory.trajectorySequence = self.encounter.intrTrajectory.trajectorySequence + np.array([0, 0, 0, deltaZintr, 0, 0, 0])

    # Step 3: eliminate offset heading for both intruder and ownship trajectories
    theta = np.radians(self.ownshipHeading)
    print("theta ownship")
    print(self.ownshipHeading)
    c, s = np.cos(theta), np.sin(theta)
    R = np.array([[c, -s], [s, c]])
    ownshipTrj_subset = np.transpose(np.dot(R, np.transpose(ownshipTrj_subset)))

    theta = np.radians(self.intrHeading)
    print("theta intruder")
    print(self.intrHeading)
    c, s = np.cos(theta), np.sin(theta)
    R = np.array([[c, -s], [s, c]])
    intruderTrj_subset = np.transpose(np.dot(R, np.transpose(intruderTrj_subset)))

    # Step 4: rotate intruder's trajectory
    # rotate trajectory points
    theta = np.radians(deltaH)
    c, s = np.cos(theta), np.sin(theta)
    R = np.array([[c, -s], [s, c]])
    intruderTrj_subset = np.transpose(np.dot(R, np.transpose(intruderTrj_subset)))

    intruderTrj_subset = intruderTrj_subset + np.array([deltaX, deltaY])

    self.encounter.ownshipTrajectory.trajectorySequence[:,1] = ownshipTrj_subset[:,0]
    self.encounter.ownshipTrajectory.trajectorySequence[:,2] = ownshipTrj_subset[:,1]
    self.encounter.intrTrajectory.trajectorySequence[:,1] = intruderTrj_subset[:,0]
    self.encounter.intrTrajectory.trajectorySequence[:,2] = intruderTrj_subset[:,1]


  def adjustTrajectories(self, plotDiagrams):
    #self.rotate(5.0, 10.0, 15.0, 45.0)
    # self.rotate(0.0, 0.0, self.VMD, 90.0)
    #self.rotate(100.0, self.VMD, 90.0)
    self.rotate2(self.HMD, self.VMD, self.Theta)
    self.updateIntruderFromIdx()

    encounter = self.encounter

    if plotDiagrams >= 2:
      st.plotEncounterResults("RENC_", encounter.ownshipTrajectory.trajectorySequence, encounter.ownshipTrajectory.flownSequence,
                            self.ownshipIdx, encounter.intrTrajectory.trajectorySequence,
                            encounter.intrTrajectory.flownSequence, self.intrIdx, encounter.getOwnshipFigurePath(),
                            encounter, True)

    self.alignTimeIndex()

    if plotDiagrams >= 1:
      st.plotEncounterResults("TRENC_", encounter.ownshipTrajectory.trajectorySequence, encounter.ownshipTrajectory.flownSequence,
                            self.ownshipIdx, encounter.intrTrajectory.trajectorySequence,
                            encounter.intrTrajectory.flownSequence, self.intrIdx, encounter.getOwnshipFigurePath(),
                            encounter, True)
