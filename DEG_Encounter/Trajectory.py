import numpy as np



class Trajectory:
  def __init__(self):
    self.isOwnship = True
    self.NMACValues = None
    self.NMACFactor = 0.0
    self.timeAfterNMAC = 0.0
    self.timeBeforeNMAC = 0.0
    self.indexNMAC = -1
    self.NMACSegment = None
    self.extraSegment = None
    self.segmentList = []
    self.wpSequence = []
    self.trajectorySequence = []
    self.flownSequence = []

  def getFirstSegment(self):
    return self.segmentList[0]

  def getLastSegment(self):
    return self.segmentList[-1]


  def getNMACSegmentIdx(self):
    i = 0
    while i < len(self.segmentList):
      if self.segmentList[i].isNMAC:
        return i
      i += 1
    return -1


  def printTrajectory(self):
    i = 0
    while i < len(self.segmentList):
      segment = self.segmentList[i]

      print("Segment type: ", segment.horizontalMode, segment.verticalMode, segment.speedMode, " Initial time: ", segment.initialTime, " Final time: ", segment.targetTime)
      i += 1
    print("\n")


  def generateWaypoints(self):

    wpSequence = []
    initialTime = abs(self.segmentList[0].initialTime)

    idx = 0
    while idx < len(self.segmentList):
      wp = self.segmentList[idx].initialPosition
      wp[3] += initialTime
      wpSequence.append(wp)

      if len(self.segmentList[idx].intermediatePosition) > 0:
        idy = 0
        while idy < len(self.segmentList[idx].intermediatePosition):
          wp = self.segmentList[idx].intermediatePosition[idy]
          wp[3] += initialTime
          wpSequence.append(wp)
          idy += 1

      idx += 1

    wp = self.segmentList[-1].targetPosition
    wp[3] += initialTime
    wpSequence.append(wp)

    self.wpSequence = np.array(wpSequence)

    return self.wpSequence


  def identifyNMACIdx(self):
    # Retrieve NMAC segment within Waypoint array
    for i in range(len(self.wpSequence)):
      if (self.wpSequence[i][0] == 0 and self.wpSequence[i][1] == 0):
        self.indexNMAC = i
        return self.indexNMAC
    return -1


  def identifyReferenceIdx(self, ip, idx):
    s3_ref = np.array([ip[0], ip[1], ip[2]] )

    s3_traj = self.trajectorySequence[:, [1, 2, 3]]
    t_traj = self.trajectorySequence[:, [0]]

    bestSlat = 50.0
    bestIdx = 0

    print("Trajectory lenght: ", len(s3_traj), " Staring from: ", idx)
    while idx < len(s3_traj):
      s3_pos = s3_traj[idx]
      currentSlat = np.linalg.norm(s3_ref - s3_pos)

      if currentSlat < bestSlat:
          bestSlat = currentSlat
          bestIdx = idx
          #print("Best idx: ", bestIdx, " Best slat range: ", bestSlat, " at time: ", t_traj[bestIdx])

      idx += 1

    print("Closest point idx: ", bestIdx, " Best slat range: ", bestSlat, " at time: ", t_traj[bestIdx])
    if bestSlat > 2.0:
      print("WARNING: ", "Seems that a waypoint CANNOT be attained by due to drone performance.")

    return bestIdx
