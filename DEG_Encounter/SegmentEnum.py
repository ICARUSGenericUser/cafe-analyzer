import enum

class horizontalSegment(enum.Enum):
   none = 0
   straight = 1
   turn = 2
   hover = 3

   def __str__(self):
       if self == horizontalSegment.none:
           return "None"
       elif self == horizontalSegment.straight:
           return "Straight"
       elif self == horizontalSegment.turn:
           return "Turn"
       elif self == horizontalSegment.hover:
           return "Hover"

   def isAssigned(self):
     return self != horizontalSegment.none


class verticalSegment(enum.Enum):
   none = 0
   level = 1
   climb = 2
   descent = 3

   def __str__(self):
       if self == verticalSegment.none:
           return "None"
       elif self == verticalSegment.level:
           return "Level"
       elif self == verticalSegment.climb:
           return "Climb"
       elif self == verticalSegment.descent:
           return "Descent"

   def isAssigned(self):
       return self != verticalSegment.none


class speedSegment(enum.Enum):
    none = 0
    constant = 1
    accelerate = 2
    decelerate = 3

    def __str__(self):
        if self == speedSegment.none:
            return "None"
        elif self == speedSegment.constant:
            return "Constant"
        elif self == speedSegment.accelerate:
            return "Accelerate"
        elif self == speedSegment.decelerate:
            return "Decelerate"

    def isAssigned(self):
        return self != speedSegment.none
