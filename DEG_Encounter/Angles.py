import numpy as np
import math as math
import Encounter.units as units

def normalizeAngle(angle):
    while angle < 0:
        angle += 360
    while angle > 360:
        angle -= 360
    return angle

def getVelocityVector2D(speed, angle):
    radians = units.deg_to_rad(angle)
    return np.array([speed * math.sin(radians), speed * math.cos(radians)])

def getUnitaryVector2D(angle):
    radians = units.deg_to_rad(angle)
    return np.array([math.sin(radians), math.cos(radians)])

def createVector4D(s, alt, t):
    return np.array([s[0], s[1], alt, t])

def duplicateVector4D(s4, alt, t):
    return np.array([s4[0], s4[1], alt, t])

def linearTimeProjection4D(s, v, t):
    return np.array([s[0] + v[0] * t, s[1] + v[1] * t, s[2], s[3]])

def linearDistanceProjection4D(s, v, t):
    return np.array([s[0] + v[0] * t, s[1] + v[1] * t, s[2], s[3]])

def assignTime4D(s, t):
    s[3] = t
    return s

def assignAltitude4D(s, alt):
    s[2] = alt
    return s