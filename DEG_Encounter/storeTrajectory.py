# Store position, velocity and heading in a .csv file
import math as math
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
import matplotlib.gridspec as gridspec
import time
import csv



# --------------------------------------------------

    # Trajectory reference
    # Column 1: index
    # Column 2: time (sec)
    # Column 3: x-position (m)
    # Column 4: y-position (m)
    # Column 5: z-position, altitude (m)
    # Column 6: x-velocity (m/s)
    # Column 7: y-velocity (m/s)
    # Column 8: z-velocity, climb rate (m/s)

# --------------------------------------------------
def storeFlowSequence(flowSequence, encounterFilepath, number):

    print("Storing flow sequence: " + encounterFilepath + "RFS_" + str(number).zfill(8) + ".csv")
    filepath = encounterFilepath + "RFS_" + str(number).zfill(8) + ".csv"

    with open(filepath, "a", newline="") as f:
        result = []

        for i in range(len(flowSequence)):
            wp = flowSequence[i]
            result.append((wp[0], round(wp[1], 2), wp[2], wp[3], wp[4]))

        writer = csv.writer(f)
        writer.writerows(result)
        f.close()


# --------------------------------------------------

    # Trajectory reference
    # Column 1: index
    # Column 2: time (sec)
    # Column 3: x-position (m)
    # Column 4: y-position (m)
    # Column 5: z-position, altitude (m)
    # Column 6: x-velocity (m/s)
    # Column 7: y-velocity (m/s)
    # Column 8: z-velocity, climb rate (m/s)

# --------------------------------------------------
def storeTrajectory(trajectory, encounterFilepath, number, priorRotation):
    t_wp = trajectory[:, 0]
    x_wp = trajectory[:, 1]
    y_wp = trajectory[:, 2]
    z_wp = trajectory[:, 3]
    vx_wp = trajectory[:, 4]
    vy_wp = trajectory[:, 5]
    vz_wp = trajectory[:, 6]

    if priorRotation:
        print("Storing trajectory: " + encounterFilepath + "TRJ_" + str(number).zfill(8) + ".csv")
        filepath = encounterFilepath + "TRJ_" + str(number).zfill(8) + ".csv"
    else:
        # Rotated trajectory
        print("Storing Rotated trajectory: " + encounterFilepath + "RTRJ_" + str(number).zfill(8) + ".csv")
        filepath = encounterFilepath + "RTRJ_" + str(number).zfill(8) + ".csv"

    with open(filepath, "a", newline="") as f:
        result = []

        for i in range(len(t_wp)):
            result.append((i, round(t_wp[i], 2), x_wp[i], y_wp[i], z_wp[i], vx_wp[i], vy_wp[i], vz_wp[i]))

            if i < len(t_wp) - 1:
                vx = (x_wp[i+1]-x_wp[i]) / (t_wp[i+1] - t_wp[i])
                vy = (y_wp[i+1]-y_wp[i]) / (t_wp[i+1] - t_wp[i])
                vz = (z_wp[i + 1] - z_wp[i]) / (t_wp[i+1] - t_wp[i])
                vxx = vx_wp[i]
                vyy = vy_wp[i]
                vzz = vz_wp[i]
                #print("VX: " + str(vx) + " VY: " + str(vy) + " VZ: " + str(vz))
                #print("RAW DATA: VX: " + str(vxx) + " VY: " + str(vyy) + " VZ: " + str(vzz) + "\n")

        writer = csv.writer(f)
        writer.writerows(result)
        f.close()


def storeCombinedTrajectory(ownTrajectory, intTrajectory, encounterFilepath, number, mainStep):
    own_t_wp = ownTrajectory[:, 0]
    own_x_wp = ownTrajectory[:, 1]
    own_y_wp = ownTrajectory[:, 2]
    own_z_wp = ownTrajectory[:, 3]
    own_vx_wp = ownTrajectory[:, 4]
    own_vy_wp = ownTrajectory[:, 5]
    own_vz_wp = ownTrajectory[:, 6]
    int_t_wp = intTrajectory[:, 0]
    int_x_wp = intTrajectory[:, 1]
    int_y_wp = intTrajectory[:, 2]
    int_z_wp = intTrajectory[:, 3]
    int_vx_wp = intTrajectory[:, 4]
    int_vy_wp = intTrajectory[:, 5]
    int_vz_wp = intTrajectory[:, 6]

    print("Storing trajectory: " + encounterFilepath + "DEG_TRJ_" + str(number).zfill(8) + "_" + str(mainStep).zfill(2) + ".csv")

    filepath = encounterFilepath + "DEG_TRJ_" + str(number).zfill(8) + "_" + str(mainStep).zfill(2) + ".csv"

    with open(filepath, "a", newline="") as f:
        result = []
        maxLen = max(len(own_t_wp), len(int_t_wp))

        step = 0
        stepIdx = 0
        for i in range(maxLen):
            if (mainStep > 0) and (step > 0):
                if (step == mainStep):
                    step = 0
                else:
                    step += 1

                continue
            step += 1

            if i < len(own_t_wp):
                result.append((stepIdx, 1, round(own_t_wp[i], 2), own_x_wp[i], own_y_wp[i], own_z_wp[i], own_vx_wp[i], own_vy_wp[i], own_vz_wp[i]))

            if i < len(int_t_wp):
                result.append((stepIdx, 2, round(int_t_wp[i], 2), int_x_wp[i], int_y_wp[i], int_z_wp[i], int_vx_wp[i], int_vy_wp[i], int_vz_wp[i]))

            stepIdx += 1

        writer = csv.writer(f)
        writer.writerows(result)
        f.close()


def safeOwnshipData(t,u, isOwnship):

    if isOwnship:
        res=[]
        tm = 0.0
        s = 0
        for i in range(0, len(t)):
            if tm == np.round(t[i], 4) or i == len(t):
                ts = time.strftime('%H:%M:%S', time.gmtime(tm))
                res.append((str(ts),str(1),"N/A",str(m_to_NM(u[i][0])),str(m_to_NM(u[i][1])),str(m_to_ft(u[i][2])),"ABCCZ"))
                tm = tm + 1
                s +=1

        return res


# --------------------------------------------------
# ENCOUNTER SUMMARY OUTPUT:
# ------ The encounter output text file does not contain any header ------

# All variables are defined in the International System of Units (SI Units)

# Column 1: Encounter_Distribution No. – This is the number of the generated encounter within a batch.
# Column 2: Seed – This is the seed used to generate the string of pseudo-random numbers
# Column 3: VMD – This is the Vertical Miss Distance, in feet, between the two aircraft at CPA.
# Column 4: HMD – This is the Horizontal Miss Distance, in nautical miles, between the two aircraft at CPA.
# Column 5: Approach Angle – This is the relative angle, in degrees, between the two aircraft at CPA.
# Column 6: AC1 Altitude – This is the altitude, in feet, of aircraft 1 at CPA.
# Column 7: AC1 Vertical Rate – This is the vertical rate, in feet per minute, of aircraft 1 at CPA.
# Column 8: AC1 Speed – This is the speed, in knots, of aircraft 1 at CPA.
# Column 9: AC1 Heading – This is the heading in degrees of aircraft 1 (measured clockwise from North) at CPA.
# Column 10: AC2 Altitude – This is the altitude, in feet, of aircraft 2 at CPA.
# Column 11: AC2 Vertical Rate – This is the vertical rate, in feet per minute, of aircraft 2 at CPA.
# Column 12: AC2 Speed – This is the speed, in knots, of aircraft 2 at CPA.
# Column 13: AC2 Heading – This is the heading, in degrees, of aircraft 2 (measured clockwise from North) at CPA.


# --------------------------------------------------

def storeEncounterSummaryOutput(filepath, enc, rotation, CPAconds):


    with open(filepath, "a", newline="") as f:
        result = []
        result.append((enc.number, enc.currentSeed, enc.NMACValues.VMD, enc.NMACValues.HMD, enc.NMACValues.AppAngle,
                        enc.NMACValues.ClassOwnship.className, enc.NMACValues.ClassOwnship.classIdx,
                        str(enc.ownshipTrajectory.NMACSegment.horizontalMode), str(enc.ownshipTrajectory.NMACSegment.verticalMode), str(enc.ownshipTrajectory.NMACSegment.speedMode),
                        rotation.ownshipIdx, enc.ownshipTrajectory.timeBeforeNMAC, enc.ownshipTrajectory.timeAfterNMAC,
                        rotation.ownshipHeading, rotation.ownshipSpeed, rotation.ownshipAltitude, rotation.ownshipVerticalSpeed,
                        enc.NMACValues.ClassIntr.className, enc.NMACValues.ClassIntr.classIdx,
                        str(enc.intrTrajectory.NMACSegment.horizontalMode), str(enc.intrTrajectory.NMACSegment.verticalMode), str(enc.intrTrajectory.NMACSegment.speedMode),
                        rotation.intrIdx, enc.intrTrajectory.timeBeforeNMAC, enc.intrTrajectory.timeAfterNMAC,
                        rotation.intrHeading, rotation.intrSpeed, rotation.intrAltitude, rotation.intrVerticalSpeed,
                        CPAconds.actualHMD, CPAconds.lowestCPAIdx, CPAconds.lowestCPAtime))

        writer = csv.writer(f)
        writer.writerows(result)
        f.close()



def plotTrajectoryResults(prefix, vehicleTrajectory, figureFilepath, number, droneName):
    textSize = 20
    axisSize = 22
    titleSize = 24
    lineSize = 4

    trajectory = vehicleTrajectory.trajectorySequence
    flowSequence = vehicleTrajectory.flownSequence
    waypoints = vehicleTrajectory.wpSequence

    # Points corresponding to trajectory
    t_trj = trajectory[:, 0]
    x_trj = trajectory[:, 1]
    y_trj = trajectory[:, 2]
    z_trj = trajectory[:, 3]
    d_trj = [0] * len(t_trj)
    v_trj = [0] * len(t_trj)

    for i in range(len(t_trj)):
        if i > 0:
            d_trj[i] = math.sqrt((x_trj[i] - x_trj[i-1])**2 + (y_trj[i] - y_trj[i-1])**2) + d_trj[i-1]
            v_trj[i] = (d_trj[i] - d_trj[i-1]) / (t_trj[i] - t_trj[i-1])

    # Points corresponding to waypoints
    x_wp = waypoints[:,0]
    y_wp = waypoints[:,1]
    z_wp = waypoints[:,2]
    t_wp = waypoints[:, 3]
    d_wp = [0] * len(t_wp)
    v_wp = [0] * len(t_wp)

    for i in range(len(t_wp)):
        if i > 0:
            d_wp[i] = math.sqrt((x_wp[i] - x_wp[i-1])**2 + (y_wp[i] - y_wp[i-1])**2) + d_wp[i-1]
            v_wp[i] = (d_wp[i] - d_wp[i-1]) / (t_wp[i] - t_wp[i-1])


    ##############################################
    # Vertical Trajectory Sequence versus Time
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('T (s)', fontsize=axisSize)
    ax.set_ylabel('Altitude (m)', fontsize=axisSize)
    ax.tick_params(axis='both', which='major', labelsize=textSize)
    ax.tick_params(axis='both', which='minor', labelsize=textSize-2)
    plt.title("Altitude Sequence versus Time for " + droneName, fontsize=titleSize)

    ax.plot(t_wp, z_wp, 'gray', linewidth=lineSize)
    ax.scatter(t_wp, z_wp, color='gray')
    lastTime = 0
    for i in range(len(t_wp)):  # plot each point + it's index as text above
        if (t_wp[i] - lastTime) < 50:
            continue
        lastTime = t_wp[i]
        ax.text(t_wp[i], z_wp[i], '%s' % ('[' + "{:d}: ".format(i) + "{:.1f}]".format(t_wp[i])), size=textSize, zorder=1, color='gray', rotation=25)

    ax.plot(t_trj, z_trj, 'orange', linewidth=lineSize)

    lastTime = 0
    for i in range(len(flowSequence)):
        wp = flowSequence[i]
        idx = int(wp[0])
        ax.scatter(t_trj[idx], z_trj[idx], color='b', linewidth=lineSize)
        if (wp[1] - lastTime) < 50:
            continue
        lastTime = wp[1]
        ax.text(t_trj[idx], z_trj[idx], '%s' % ('[' + "{:d}: ".format(i) + "{:.1f}]".format(wp[1])), size=textSize, zorder=1, color='k', rotation=65)

    fig.set_size_inches(14.0, 14.0)
    print("Generating figure: " + figureFilepath + prefix + "ZT_Path_" + str(number).zfill(8) + ".png")
    fig.savefig(figureFilepath + prefix + "ZT_Path_" + str(number).zfill(8) + ".png")
    fig.clf()
    plt.close(fig)


    ##############################################
    # Speed Trajectory Sequence versus Time
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('T (s)', fontsize=axisSize)
    ax.set_ylabel('Speed (m/s)', fontsize=axisSize)
    ax.tick_params(axis='both', which='major', labelsize=textSize)
    ax.tick_params(axis='both', which='minor', labelsize=textSize-2)
    plt.title("Speed Sequence versus Time for " + droneName, fontsize=titleSize)

    ax.plot(t_wp, v_wp, 'gray', linewidth=lineSize)
    ax.scatter(t_wp, v_wp, color='gray', linewidth=lineSize)
    lastTime = 0
    for i in range(len(t_wp)):  # plot each point + it's index as text above
        if (t_wp[i] - lastTime) < 20:
            continue
        lastTime = t_wp[i]
        ax.text(t_wp[i], v_wp[i], '%s' % ('[' + "{:d}: ".format(i) + "{:.1f}]".format(t_wp[i])), size=textSize, zorder=1, color='gray', rotation=25)

    ax.plot(t_trj, v_trj, 'orange', linewidth=lineSize)

    lastTime = 0
    for i in range(len(flowSequence)):
        wp = flowSequence[i]
        idx = int(wp[0])
        ax.scatter(t_trj[idx], v_trj[idx], color='b', linewidth=lineSize)
        if (wp[1] - lastTime) < 20:
            continue
        lastTime = wp[1]
        ax.text(t_trj[idx], v_trj[idx], '%s' % ('[' + "{:d}: ".format(i) + "{:.1f}]".format(wp[1])), size=textSize, zorder=1, color='k', rotation=65)

    fig.set_size_inches(14.0, 14.0)
    print("Generating figure: " + figureFilepath + prefix + "VT_Path_" + str(number).zfill(8) + ".png")
    fig.savefig(figureFilepath + prefix + "VT_Path_" + str(number).zfill(8) + ".png")
    fig.clf()
    plt.close(fig)


    ##############################################
    # Vertical Trajectory Sequence versus Distance
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('D (m)', fontsize=axisSize)
    ax.set_ylabel('Altitude (m)', fontsize=axisSize)
    ax.tick_params(axis='both', which='major', labelsize=textSize)
    ax.tick_params(axis='both', which='minor', labelsize=textSize-2)
    plt.title("Altitude Sequence versus Distance for " + droneName, fontsize=titleSize)

    ax.plot(d_wp, z_wp, 'gray', linewidth=lineSize)
    ax.scatter(d_wp, z_wp, color='gray', linewidth=lineSize)
    lastTime = 0
    for i in range(len(t_wp)):  # plot each point + it's index as text above
        if (t_wp[i] - lastTime) < 20:
            continue
        lastTime = t_wp[i]
        ax.text(d_wp[i], z_wp[i], '%s' % ('t='+"{:.1f}".format(t_wp[i])), size=textSize, zorder=1, color='gray', rotation=25)

    ax.plot(d_trj, z_trj, 'orange', linewidth=lineSize)

    lastTime = 0
    for i in range(len(flowSequence)):
        wp = flowSequence[i]
        idx = int(wp[0])
        ax.scatter(d_trj[idx], z_trj[idx], color='b', linewidth=lineSize)
        if (wp[1] - lastTime) < 20:
            continue
        lastTime = wp[1]
        ax.text(d_trj[idx], z_trj[idx], '%s' % ('t=' + "{:.1f}".format(wp[1])), size=textSize, zorder=1, color='k', rotation=65)

    fig.set_size_inches(14.0, 14.0)
    print("Generating figure: " + figureFilepath + prefix + "DZ_Path_" + str(number).zfill(8) + ".png")
    fig.savefig(figureFilepath + prefix + "DZ_Path_" + str(number).zfill(8) + ".png")
    fig.clf()
    plt.close(fig)


    ##############################################
    # Horizontal Trajectory
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('X (m)', fontsize=axisSize)
    ax.set_ylabel('Y (m)', fontsize=axisSize)
    ax.tick_params(axis='both', which='major', labelsize=textSize)
    ax.tick_params(axis='both', which='minor', labelsize=textSize-2)
    plt.axis('equal')
    plt.title("Horizontal Trajectory for " + droneName, fontsize=titleSize)

    ax.plot(x_wp, y_wp, 'gray', linewidth=lineSize)
    ax.scatter(x_wp, y_wp, color='gray', linewidth=lineSize)
    lastTime = 0
    for i in range(len(t_wp)):  # plot each point + it's index as text above
        if (t_wp[i] - lastTime) < 20:
            continue
        lastTime = t_wp[i]
        ax.text(x_wp[i], y_wp[i], '%s' % ('t='+"{:.1f}".format(t_wp[i])), size=textSize, zorder=1, color='gray', rotation=25)

    ax.plot(x_trj, y_trj, 'orange', linewidth=lineSize)

    lastTime = 0
    for i in range(len(flowSequence)):
        wp = flowSequence[i]
        idx = int(wp[0])
        ax.scatter(x_trj[idx], y_trj[idx], color='b', linewidth=lineSize)
        if (wp[1] - lastTime) < 20:
            continue
        lastTime = wp[1]
        ax.text(x_trj[idx], y_trj[idx], '%s' % ('t=' + "{:.1f}".format(wp[1])), size=textSize, zorder=1, color='k', rotation=65)

    fig.set_size_inches(14.0, 14.0)
    print("Generating figure: " + figureFilepath + prefix + "H_Path_" + str(number).zfill(8) + ".png")
    fig.savefig(figureFilepath + prefix + "H_Path_" + str(number).zfill(8) + ".png")
    fig.clf()
    plt.close(fig)




def plotEncounterResults(prefix, trajOwnship, flowOwnship, nmacIdxOwnship, trajIntruder, flowIntruder, nmacIdxIntruder, figureFilepath, encounter, plotFlown):
    textSize = 20
    axisSize = 22
    titleSize = 24
    lineSize = 3

    number = encounter.number
    t_wp_own = trajOwnship[:, 0]
    x_wp_own = trajOwnship[:, 1]
    y_wp_own = trajOwnship[:, 2]
    z_wp_own = trajOwnship[:, 3]
    d_wp_own = [0] * len(t_wp_own)
    v_wp_own = [0] * len(t_wp_own)

    for i in range(len(t_wp_own)):
        if i > 0:
            d_wp_own[i] = math.sqrt((x_wp_own[i] - x_wp_own[i-1])**2 + (y_wp_own[i] - y_wp_own[i-1])**2) + d_wp_own[i-1]
            v_wp_own[i] = (d_wp_own[i] - d_wp_own[i-1]) / (t_wp_own[i] - t_wp_own[i-1])

    t_wp_int = trajIntruder[:, 0]
    x_wp_int = trajIntruder[:, 1]
    y_wp_int = trajIntruder[:, 2]
    z_wp_int = trajIntruder[:, 3]
    d_wp_int = [0] * len(t_wp_int)
    v_wp_int = [0] * len(t_wp_int)

    for i in range(len(t_wp_int)):
        if i > 0:
            d_wp_int[i] = math.sqrt((x_wp_int[i] - x_wp_int[i-1])**2 + (y_wp_int[i] - y_wp_int[i-1])**2) + d_wp_int[i-1]
            v_wp_int[i] = (d_wp_int[i] - d_wp_int[i-1]) / (t_wp_int[i] - t_wp_int[i-1])

    matplotlib.use("Agg")

    ##############################################
    # Altitude versus time
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('T (s)', fontsize=axisSize)
    ax.set_ylabel('Altitude (m)', fontsize=axisSize)
    ax.tick_params(axis='both', which='major', labelsize=textSize)
    ax.tick_params(axis='both', which='minor', labelsize=textSize-2)
    plt.title("Vertical Trajectory Sequence versus Time", fontsize=titleSize)
    plt.suptitle("HMD: " + "{:.1f}".format(encounter.NMACValues.HMD) + " VMD: " + "{:.1f}".format(encounter.NMACValues.VMD) + " Beta: " + "{:.1f}".format(encounter.NMACValues.AppAngle))

    ax.plot(t_wp_own, z_wp_own, 'green', linewidth=lineSize)
    ax.plot(t_wp_int, z_wp_int, 'blue', linewidth=lineSize)

    ax.plot(t_wp_own[nmacIdxOwnship], z_wp_own[nmacIdxOwnship], 'red', marker='D', linewidth=lineSize)
    ax.plot(t_wp_int[nmacIdxIntruder], z_wp_int[nmacIdxIntruder], 'red', marker='D', linewidth=lineSize)

    if plotFlown:
        for i in range(len(flowOwnship)):
            wp = flowOwnship[i]
            idx = int(wp[0])
            ax.scatter(t_wp_own[idx], z_wp_own[idx], color='g')
            #ax.text(t_wp_own[idx], z_wp_own[idx], '%s' % ('i=' + "{:d}".format(i) + ' t=' + "{:.1f}".format(wp[1])), size=textSize, zorder=1, color='k', rotation=45)
            ax.text(t_wp_own[idx], z_wp_own[idx], '%s' % ('t=' + "{:.1f}".format(wp[1])), size=textSize, zorder=1, color='k', rotation=45)

    if plotFlown:
        for i in range(len(flowIntruder)):
            wp = flowIntruder[i]
            idx = int(wp[0])
            ax.scatter(t_wp_int[idx], z_wp_int[idx], color='b')
            #ax.text(t_wp_int[idx], z_wp_int[idx], '%s' % ('i=' + "{:d}".format(i) + ' t=' + "{:.1f}".format(wp[1])), size=textSize, zorder=1, color='k', rotation=45)
            ax.text(t_wp_int[idx], z_wp_int[idx], '%s' % ('t=' + "{:.1f}".format(wp[1])), size=textSize, zorder=1, color='k', rotation=45)

    fig.set_size_inches(14.0, 14.0)
    print("Generating figure: " + figureFilepath + prefix + "ZT_Path_" + str(number).zfill(8) + ".png")
    fig.savefig(figureFilepath + prefix + "ZT_Path_" + str(number).zfill(8) + ".png")
    plt.close(fig)
    fig.clf()


    ##############################################
    # Speed versus time
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('T (s)', fontsize=axisSize)
    ax.set_ylabel('Speed (m/s)', fontsize=axisSize)
    plt.title("Speed versus Time", fontsize=titleSize)
    ax.tick_params(axis='both', which='major', labelsize=textSize)
    ax.tick_params(axis='both', which='minor', labelsize=textSize-2)
    plt.suptitle("HMD: " + "{:.1f}".format(encounter.NMACValues.HMD) + " VMD: " + "{:.1f}".format(encounter.NMACValues.VMD) + " Beta: " + "{:.1f}".format(encounter.NMACValues.AppAngle))


    ax.plot(t_wp_own, v_wp_own, 'green', linewidth=lineSize)
    ax.plot(t_wp_int, v_wp_int, 'blue', linewidth=lineSize)

    ax.plot(t_wp_own[nmacIdxOwnship], v_wp_own[nmacIdxOwnship], 'red', marker='D', linewidth=lineSize)
    ax.plot(t_wp_int[nmacIdxIntruder], v_wp_int[nmacIdxIntruder], 'red', marker='D', linewidth=lineSize)

    if plotFlown:
        for i in range(len(flowOwnship)):
            wp = flowOwnship[i]
            idx = int(wp[0])
            ax.scatter(t_wp_own[idx], v_wp_own[idx], color='g')
            #ax.text(t_wp_own[idx], v_wp_own[idx], '%s' % ('i=' + "{:d}".format(i) + ' t=' + "{:.1f}".format(wp[1])), size=textSize, zorder=1, color='k', rotation=45)
            ax.text(t_wp_own[idx], v_wp_own[idx], '%s' % ('t=' + "{:.1f}".format(wp[1])), size=textSize, zorder=1, color='k', rotation=45)

    if plotFlown:
        for i in range(len(flowIntruder)):
            wp = flowIntruder[i]
            idx = int(wp[0])
            ax.scatter(t_wp_int[idx], v_wp_int[idx], color='b')
            #ax.text(t_wp_int[idx], v_wp_int[idx], '%s' % ('i=' + "{:d}".format(i) + ' t=' + "{:.1f}".format(wp[1])), size=textSize, zorder=1, color='k', rotation=45)
            ax.text(t_wp_int[idx], v_wp_int[idx], '%s' % ('t=' + "{:.1f}".format(wp[1])), size=textSize, zorder=1, color='k', rotation=45)

    fig.set_size_inches(14.0, 14.0)
    print("Generating figure: " + figureFilepath + prefix + "VT_Path_" + str(number).zfill(8) + ".png")
    fig.savefig(figureFilepath + prefix + "VT_Path_" + str(number).zfill(8) + ".png")
    plt.close(fig)
    fig.clf()


    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('D (m)', fontsize=axisSize)
    ax.set_ylabel('Altitude (m)', fontsize=axisSize)
    ax.tick_params(axis='both', which='major', labelsize=textSize)
    ax.tick_params(axis='both', which='minor', labelsize=textSize-2)
    plt.title("Vertical WP Sequence versus Distance", fontsize=titleSize)
    plt.suptitle("HMD: " + "{:.1f}".format(encounter.NMACValues.HMD) + " VMD: " + "{:.1f}".format(encounter.NMACValues.VMD) + " Beta: " + "{:.1f}".format(encounter.NMACValues.AppAngle))

    ax.plot(d_wp_own, z_wp_own, 'green', linewidth=lineSize)
    ax.plot(d_wp_int, z_wp_int, 'blue', linewidth=lineSize)

    ax.plot(d_wp_own[nmacIdxOwnship], z_wp_own[nmacIdxOwnship], 'red', marker='D', linewidth=lineSize)
    ax.plot(d_wp_int[nmacIdxIntruder], z_wp_int[nmacIdxIntruder], 'red', marker='D', linewidth=lineSize)

    fig.set_size_inches(14.0, 14.0)
    print("Generating figure: " + figureFilepath + prefix + "DZ_Path_" + str(number).zfill(8) + ".png")
    fig.savefig(figureFilepath + prefix + "DZ_Path_" + str(number).zfill(8) + ".png")
    plt.close(fig)
    fig.clf()


    ##############################################
    # Horizontal Trajectory
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('X (m)', fontsize=axisSize)
    ax.set_ylabel('Y (m)', fontsize=axisSize)
    ax.tick_params(axis='both', which='major', labelsize=textSize)
    ax.tick_params(axis='both', which='minor', labelsize=textSize-2)
    plt.axis('equal')
    plt.title("Horizontal Trajectory", fontsize=titleSize)
    plt.suptitle("HMD: " + "{:.1f}".format(encounter.NMACValues.HMD) + " VMD: " + "{:.1f}".format(encounter.NMACValues.VMD) + " Beta: " + "{:.1f}".format(encounter.NMACValues.AppAngle))

    ax.plot(x_wp_own, y_wp_own, 'green', linewidth=lineSize)
    ax.plot(x_wp_int, y_wp_int, 'blue', linewidth=lineSize)

    ax.plot(x_wp_own[nmacIdxOwnship], y_wp_own[nmacIdxOwnship], 'red', marker='D', linewidth=lineSize)
    ax.plot(x_wp_int[nmacIdxIntruder], y_wp_int[nmacIdxIntruder], 'red', marker='D', linewidth=lineSize)

    if plotFlown:
        for i in range(len(flowOwnship)):
            wp = flowOwnship[i]
            idx = int(wp[0])
            ax.scatter(x_wp_own[idx], y_wp_own[idx], color='g')
            #ax.text(x_wp_own[idx], y_wp_own[idx], '%s' % ('i=' + "{:d}".format(i) + ' t=' + "{:.1f}".format(wp[1])), size=textSize, zorder=1, color='k', rotation=45)
            ax.text(x_wp_own[idx], y_wp_own[idx], '%s' % ('t=' + "{:.1f}".format(wp[1])), size=textSize, zorder=1, color='k', rotation=45)

    if plotFlown:
        for i in range(len(flowIntruder)):
            wp = flowIntruder[i]
            idx = int(wp[0])
            ax.scatter(x_wp_int[idx], y_wp_int[idx], color='b')
            #ax.text(x_wp_int[idx], y_wp_int[idx], '%s' % ('i=' + "{:d}".format(i) + ' t=' + "{:.1f}".format(wp[1])), size=textSize, zorder=1, color='k', rotation=45)
            ax.text(x_wp_int[idx], y_wp_int[idx], '%s' % ('t=' + "{:.1f}".format(wp[1])), size=textSize, zorder=1, color='k', rotation=45)

    fig.set_size_inches(14.0, 14.0)
    print("Generating figure: " + figureFilepath + prefix + "H_Path_" + str(number).zfill(8) + ".png")
    fig.savefig(figureFilepath + prefix + "H_Path_" + str(number).zfill(8) + ".png")
    plt.close(fig)
    fig.clf()



def m_to_ft(number):
    return number*3.28084
def m_to_NM(number):
    return number/1852
def ms_to_kts(number):
    return number*1.94384
def rad_to_deg(number):
    return number * 180 / math.pi
def ms_to_fpm(number):
    return number * 196.85