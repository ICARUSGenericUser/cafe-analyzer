

class NMACSubValues:
  def __init__(self):
    self.Altitude = 0
    self.Speed = 0
    self.Class = None


class NMACValues:
  def __init__(self):
    self.VMD = 0
    self.HMD = 0
    self.AppAngle = 0
    self.AltitudeOwnship = 0
    self.AltitudeIntr = 0
    self.SpeedOwnship = 0
    self.SpeedIntr = 0
    self.ClassOwnshipIdx = 0
    self.ClassOwnship = None
    self.ClassIntrIdx = 0
    self.ClassIntr = None

