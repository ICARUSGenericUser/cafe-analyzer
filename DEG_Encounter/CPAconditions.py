
import math



# Compare Actual HMD vs HMD at CPA.


class CPAconditions:
    def __init__(self):
        self.actualHMD = 0 # This shows the lowest horizontal separation (in nautical miles) throughout the whole encounter between the two aircraft.
        self.actualTCAoffset = 0 # If HMD is lower in Actual HMD compared to HMD at CPA then this shows the offset in seconds from CPA to the actual HMD.
        self.actualCPAdifferent = 0 # This indicates if TCA is different from what was initially sampled, i.e. Actual TCA Offset is non-zero (‘1’ – CPA is different from sampled,	 ‘0’ – CPA is not different).
        self.lowestCPAIdx = 0 # Index of trajectory sequence array of the lowest horizontal separation
        self.lowestCPAtime = 0

    def lowestHMD(self ,enc ,rotation):

        # Index within trajectory sequence of CPA point for ownship
        CPAownIdx = rotation.ownshipIdx

        # todo: check what column is stored x and y within trajectory sequence array
        self.actualHMD = math.sqrt((enc.ownshipTrajectory.trajectorySequence[0][1] -
                                               enc.intrTrajectory.trajectorySequence[0][1]) ** 2 + (
                                                          (enc.ownshipTrajectory.trajectorySequence[0][1]) -
                                                          enc.intrTrajectory.trajectorySequence[0][2]) ** 2)  # lowest horizontal separation throughout the entire path

        maxLen = min(len(enc.ownshipTrajectory.trajectorySequence), len(enc.intrTrajectory.trajectorySequence))
        for i in range(0,maxLen):
            # Compare Actual HMD vs HMD at CPA.
            # Distance between two points (x1,y1), (x2,y2) => math.sqrt((x1-x2)**2+(y1-y2)**2)
            nextHMD = math.sqrt((enc.ownshipTrajectory.trajectorySequence[i][1]-enc.intrTrajectory.trajectorySequence[i][1])**2 + ((enc.ownshipTrajectory.trajectorySequence[i][2])-enc.intrTrajectory.trajectorySequence[i][2])**2)
            if nextHMD < self.actualHMD:
                self.actualHMD = nextHMD
                self.lowestCPAIdx = i
                self.lowestCPAtime = enc.ownshipTrajectory.trajectorySequence[i][0]


    def TCAoffset(self, enc,CPAownIdx):
        # If HMD is lower in Actual HMD compared to HMD at CPA then this shows the offset in seconds from CPA to the actual HMD
        # todo: check what column is stored time within trajectory sequence array
        self.actualTCAoffset = enc.ownshipTrajectory.trajectorySequence[CPAownIdx][5] - enc.ownshipTrajectory.trajectorySequence[self.lowestCPAidx][5]
        if self.actualTCAoffset != 0:
            self.actualCPAdifferent = 1