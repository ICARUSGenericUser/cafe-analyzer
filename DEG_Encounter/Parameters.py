import os, shutil

class Parameters:
  def __init__(self):
    self.generatedEncounters = 0
    self.totalNumberEncounters = 0
    self.probDistributions = None
    self.destinationPath = None
    self.encounterSetName = None
    self.timeAfterNMAC = 0
    self.timeBeforeNMAC = 0


  def createEncounterSetFolder(self):

    rootFolder = self.destinationPath + self.encounterSetName + "\\"

    if os.path.exists(rootFolder):
      print("Root folder does already exists, removing: ", rootFolder)
      shutil.rmtree(rootFolder)

    os.mkdir(rootFolder)

