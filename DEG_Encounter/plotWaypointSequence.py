import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import math as math
import numpy as np
from DEG_Encounter import SegmentEnum as se

''' Plot waypoints vector. Show in a 3D plot the waypoints and its corresponding time in a label'''




def getDynamicPlot(name):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('X (m)')
    ax.set_ylabel('Y (m)')
    plt.axis('equal')
    plt.title(name + " Encounter_Distribution Horizontal WP Sequence")
    plt.show()
    return ax, fig


def addSegmentDynamicPlot(ax, segment):

    if segment.horizontalMode == se.horizontalSegment.turn or segment.horizontalMode == se.horizontalSegment.hover:
        V = []
        V.append(segment.initialPosition)

        for p in segment.intermediatePosition:
            V.append(p)

        V.append(segment.targetPosition)
        V = np.array(V)
        ax.plot(V[:,0], V[:,1], 'green')
        ax.scatter(V[:,0], V[:,1], color='b')

        for p in segment.intermediatePosition:
            ax.text(p[0], p[1], '%s' % ('t=' + "{:.1f}".format(p[3])), size=10, zorder=1, color='k')
        ax.text(segment.targetPosition[0], segment.targetPosition[1], '%s' % ('t=' + "{:.1f}".format(segment.targetPosition[3])), size=12, zorder=1, color='k')
        ax.text(segment.initialPosition[0], segment.initialPosition[1], '%s' % ('t=' + "{:.1f}".format(segment.initialPosition[3])), size=12, zorder=1, color='k')
    else:
        V = np.array([segment.initialPosition, segment.targetPosition])
        ax.plot(V[:,0], V[:,1], 'gray')
        ax.scatter(V[:,0], V[:,1], color='b')
        ax.text(segment.targetPosition[0], segment.targetPosition[1], '%s' % ('t=' + "{:.1f}".format(segment.targetPosition[3])), size=12, zorder=1, color='k')
        ax.text(segment.initialPosition[0], segment.initialPosition[1], '%s' % ('t=' + "{:.1f}".format(segment.initialPosition[3])), size=12, zorder=1, color='k')


def closeDynamicPlot(fig, filepath):
    print("Generating figure: " + filepath)
    fig.set_size_inches(14.0, 14.0)
    fig.savefig(filepath)
    plt.close(fig)


def plotWaypoints3D(waypoints, filepath, idx_NMAC, TEST_DEBUG):

    x_wp = waypoints[:,0]
    y_wp = waypoints[:,1]
    z_wp = waypoints[:,2]
    t_wp = waypoints[:, 3]

    # NMAC wapoints coordinates. Useful to print NMAC segment in a different colour
    NMAC_x_values = [x_wp[idx_NMAC],x_wp[idx_NMAC+1]]
    NMAC_y_values = [y_wp[idx_NMAC],y_wp[idx_NMAC+1]]
    NMAC_z_values = [z_wp[idx_NMAC],z_wp[idx_NMAC+1]]

    fig = plt.figure()
    ax = p3.Axes3D(fig)

    ax.set_xlabel('X (m)')
    ax.set_ylabel('Y (m)')
    ax.set_zlabel('Altitude (m)')

    ax.plot3D(x_wp, y_wp, z_wp, 'gray')
    ax.scatter(x_wp, y_wp, z_wp, color='b')
    for i in range(len(x_wp)):  # plot each point + it's index as text above
        ax.text(x_wp[i], y_wp[i], z_wp[i], '%s' % ('t='+"{:.1f}".format(t_wp[i])), size=12, zorder=1, color='k')
        # Plot NMAC waypoints in a different colour
        if i == idx_NMAC or i == idx_NMAC + 1:
            ax.scatter(x_wp[i], y_wp[i], z_wp[i], color='y')

    # Plot NMAC segment in a different colour

    ax.plot3D(NMAC_x_values, NMAC_y_values, NMAC_z_values, color='y')
    plt.title("Encounter_Distribution WP Sequence")
    print("Generating figure: " + filepath)
    fig.savefig(filepath)
    if TEST_DEBUG:
        plt.show()
    plt.close(fig)


def plotWaypoints(waypoints, filepath, idx_NMAC, number, TEST_DEBUG):

    textSize = 20
    axisSize = 24
    titleSize = 28
    lineSize = 4
    markerSize = 12
    legendSize = 12

    x_wp = waypoints[:, 0]
    y_wp = waypoints[:, 1]
    z_wp = waypoints[:, 2]
    t_wp = waypoints[:, 3]
    d_wp = [0] * len(t_wp)
    v_wp = [0] * len(t_wp)

    for i in range(len(t_wp)):
        if i > 0:
            d_wp[i] = math.sqrt((x_wp[i] - x_wp[i-1])**2 + (y_wp[i] - y_wp[i-1])**2) + d_wp[i-1]
            v_wp[i] = (d_wp[i] - d_wp[i-1]) / (t_wp[i] - t_wp[i-1])

    # NMAC wapoints coordinates. Useful to print NMAC segment in a different colour
    NMAC_x_values = [x_wp[idx_NMAC], x_wp[idx_NMAC+1]]
    NMAC_y_values = [y_wp[idx_NMAC], y_wp[idx_NMAC+1]]
    NMAC_z_values = [z_wp[idx_NMAC], z_wp[idx_NMAC+1]]
    NMAC_t_values = [t_wp[idx_NMAC], t_wp[idx_NMAC + 1]]
    NMAC_d_values = [d_wp[idx_NMAC], d_wp[idx_NMAC + 1]]
    NMAC_v_values = [v_wp[idx_NMAC], v_wp[idx_NMAC + 1]]

    ############################################
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('T (s)', fontsize=axisSize)
    ax.set_ylabel('Altitude (m)', fontsize=axisSize)
    ax.tick_params(axis='both', which='major', labelsize=textSize)
    ax.tick_params(axis='both', which='minor', labelsize=textSize-2)
    plt.title("Vertical WP Sequence versus Time", fontsize=titleSize)

    ax.plot(t_wp, z_wp, 'gray')
    ax.scatter(t_wp, z_wp, color='b')
    for i in range(len(t_wp)):  # plot each point + it's index as text above
        ax.text(t_wp[i], z_wp[i], '%s' % ('t='+"{:.1f}".format(t_wp[i])), size=textSize, zorder=1, color='k', rotation=45)
        # Plot NMAC waypoints in a different colour
        if i == idx_NMAC or i == idx_NMAC + 1:
            ax.scatter(t_wp[i], z_wp[i], color='y')

    # Plot NMAC segment in a different colour
    ax.plot(NMAC_t_values, NMAC_z_values, color='y', linewidth=lineSize)
    fig.set_size_inches(14.0, 14.0)
    print("Generating figure: " + filepath + "WP_ZT_Path_" + str(number).zfill(8) + ".png")
    fig.savefig(filepath + "WP_ZT_Path_" + str(number).zfill(8) + ".png")
    fig.clf()
    plt.close(fig)


    ############################################
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('T (s)', fontsize=axisSize)
    ax.set_ylabel('Speed (m/s)', fontsize=axisSize)
    ax.tick_params(axis='both', which='major', labelsize=textSize)
    ax.tick_params(axis='both', which='minor', labelsize=textSize-2)
    plt.title("Speed WP Sequence versus Time", fontsize=titleSize)

    ax.plot(t_wp, v_wp, 'gray')
    ax.scatter(t_wp, v_wp, color='b')
    for i in range(len(t_wp)):  # plot each point + it's index as text above
        ax.text(t_wp[i], v_wp[i], '%s' % ('t='+"{:.1f}".format(t_wp[i])), size=textSize, zorder=1, color='k', rotation=45)
        # Plot NMAC waypoints in a different colour
        if i == idx_NMAC or i == idx_NMAC + 1:
            ax.scatter(t_wp[i], v_wp[i], color='y')

    # Plot NMAC segment in a different colour
    ax.plot(NMAC_t_values, NMAC_v_values, color='y', linewidth=lineSize)
    fig.set_size_inches(14.0, 14.0)
    print("Generating figure: " + filepath + "WP_VT_Path_" + str(number).zfill(8) + ".png")
    fig.savefig(filepath + "WP_VT_Path_" + str(number).zfill(8) + ".png")
    fig.clf()
    plt.close(fig)


    ############################################
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('D (m)', fontsize=axisSize)
    ax.set_ylabel('Altitude (m)', fontsize=axisSize)
    ax.tick_params(axis='both', which='major', labelsize=textSize)
    ax.tick_params(axis='both', which='minor', labelsize=textSize-2)
    plt.title("Vertical WP Sequence versus Distance", fontsize=titleSize)

    ax.plot(d_wp, z_wp, 'gray')
    ax.scatter(d_wp, z_wp, color='b')
    for i in range(len(d_wp)):  # plot each point + it's index as text above
        ax.text(d_wp[i], z_wp[i], '%s' % ('t='+"{:.1f}".format(t_wp[i])), size=textSize, zorder=1, color='k', rotation=45)
        # Plot NMAC waypoints in a different colour
        if i == idx_NMAC or i == idx_NMAC + 1:
            ax.scatter(d_wp[i], z_wp[i], color='y')

    # Plot NMAC segment in a different colour
    ax.plot(NMAC_d_values, NMAC_z_values, color='y', linewidth=lineSize)
    fig.set_size_inches(14.0, 14.0)
    print("Generating figure: " + filepath + "WP_DZ_Path_" + str(number).zfill(8) + ".png")
    fig.savefig(filepath + "WP_DZ_Path_" + str(number).zfill(8) + ".png")
    fig.clf()
    plt.close(fig)


    ############################################
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('X (m)', fontsize=axisSize)
    ax.set_ylabel('Y (m)', fontsize=axisSize)
    ax.tick_params(axis='both', which='major', labelsize=textSize)
    ax.tick_params(axis='both', which='minor', labelsize=textSize-2)
    plt.axis('equal')
    plt.title("Horizontal WP Sequence", fontsize=titleSize)

    ax.plot(x_wp, y_wp, 'gray')
    ax.scatter(x_wp, y_wp, color='b')
    for i in range(len(x_wp)):  # plot each point + it's index as text above
        ax.text(x_wp[i], y_wp[i], '%s' % ('t='+"{:.1f}".format(t_wp[i])), size=textSize, zorder=1, color='k', rotation=45)
        # Plot NMAC waypoints in a different colour
        if i == idx_NMAC or i == idx_NMAC + 1:
            ax.scatter(x_wp[i], y_wp[i], color='y')

    # Plot NMAC segment in a different colour

    ax.plot(NMAC_x_values, NMAC_y_values, color='y', linewidth=lineSize)
    fig.set_size_inches(14.0, 14.0)
    print("Generating figure: " + filepath + "WP_H_Path_" + str(number).zfill(8) + ".png")
    fig.savefig(filepath + "WP_H_Path_" + str(number).zfill(8) + ".png")
    fig.clf()
    plt.close(fig)

