import plotly as pl
import plotly.graph_objects as go
import pandas as pd
import numpy as np
import docx as docx
import scipy
from Encounter import units as units
from plotly.offline import plot
import plotly.express as px
import plotly.figure_factory as ff
from plotly.subplots import make_subplots

color1 = '#EB89B5'
color2 = '#330C73'
color3 = '#350C03'

# Global variables for filtering analysis

minIniCautionInterval = 5
minCautionInterval = 20
minCautionDiscontinuity = 10

minIniPrevInterval = 5
minPrevInterval = 20
minPrevDiscontinuity = 10
longPrevInterval = 30

maxPrevCautionInterval = 5
cdfDistanceNM = 8 # NM
cdfDistanceM = 1000 # M

useNM = True
rangeNM = 5
rangeM = 500
cautionTimeRange = 120 # Seconds
spuriousCautionTimeRange = 80 # Seconds
acasTimeRange = 60 # Seconds


# Evaluate timming separation between key encounter events
# Baseline taken at CPA: "cpaTime"
# NMAC as: "nmacTime"
# LMV as: "lmvTime"
# WCV Violation as: "wcvTime"
# prev activation as: "cautionTime"
# End Caution activation as: "endcautionTime"
# Separation violation as: "separationTime"

def evaluateGeneralCautionTimming(fResults, df, mycsv, mycsvh):

    # Overall Caution Alerts
    ###########################

    totalEncounters = len(df.index)

    fResults.write("General Caution Timing Evaluation:" + "\n")

    fResults.write("\tTotal number of encounters: " + str(len(df.index)) + "\n")
    mycsv.write(str(len(df.index)) + ",")
    mycsvh.write("TotalEncounters" + ",")

    caution_Set_Filter = ((df['cautionTime'] > 0) | (df['hazFirstTime'] > 0) | (df['nmacFirstTime'] > 0))

    caution_Set = df.loc[caution_Set_Filter]

    fResults.write("\tTotal number of Caution activations or LoWC / NMAC violations: " + str(len(caution_Set.index)) + "  "  + str(100 * len(caution_Set.index)/totalEncounters) + "% \n")
    mycsv.write(str(len(caution_Set.index)) + "," + str(100 * len(caution_Set.index)/totalEncounters) + ",")
    mycsvh.write("TotalConflicts" + "," + "TotalConflicts%" + ",")

    caution_Set_Filter = ((df['cautionTime'] > 0))

    caution_Set = df.loc[caution_Set_Filter]

    fResults.write("\tTotal number of Caution activations: " + str(len(caution_Set.index)) + "  " + str(100 * len(caution_Set.index)/totalEncounters) + "% \n")
    mycsv.write(str(len(caution_Set.index)) + "," + str(100 * len(caution_Set.index) / totalEncounters) + ",")
    mycsvh.write("TotalCaution" + "," + "TotalCaution%" + ",")

    # Overall Preventive Alerts
    ###########################

    prev_Set_Filter = ((df['prevTime'] > 0))

    prev_Set = df.loc[prev_Set_Filter]

    fResults.write("\tTotal number of Preventive activations: " + str(len(prev_Set.index)) + "  " + str(100 * len(prev_Set.index)/totalEncounters) + "% \n")
    mycsv.write(str(len(prev_Set.index)) + "," + str(100 * len(prev_Set.index) / totalEncounters) + ",")
    mycsvh.write("TotalPreventive" + "," + "TotalPreventive%" + ",")

    prev_Set_Filter = ((df['prevTime'] > 0) & (df['cautionTime'] <= 0))

    prev_Set = df.loc[prev_Set_Filter]

    fResults.write("\tTotal number of non-Caution Preventive activations: " + str(len(prev_Set.index)) + "  " + str(100 * len(prev_Set.index)/totalEncounters) + "% \n")
    mycsv.write(str(len(prev_Set.index)) + "," + str(100 * len(prev_Set.index) / totalEncounters) + ",")
    mycsvh.write("TotalNoCauPreventive" + "," + "TotalNoCauPreventive%" + ",")

    prev_Set_Filter = ((df['prevTime'] > 0) & (df['cautionTime'] > 0) & (df['hazFirstTime'] > 0))

    prev_Set = df.loc[prev_Set_Filter]

    fResults.write("\tTotal number of Caution + Preventive + LoWC activations: " + str(len(prev_Set.index)) + "  " + str(
        100 * len(prev_Set.index) / totalEncounters) + "% \n")
    mycsv.write(str(len(prev_Set.index)) + "," + str(100 * len(prev_Set.index) / totalEncounters) + ",")
    mycsvh.write("TotalLoWCCauPreventive" + "," + "TotalLoWCCauPreventive%" + ",")

    prev_Set_Filter = ((df['prevTime'] > 0) & (df['cautionTime'] <= 0) & (df['hazFirstTime'] > 0))

    prev_Set = df.loc[prev_Set_Filter]

    fResults.write("\tTotal number of non-Caution, Preventive + LoWC activations: " + str(len(prev_Set.index)) + "  " + str(
        100 * len(prev_Set.index) / totalEncounters) + "% \n")
    mycsv.write(str(len(prev_Set.index)) + "," + str(100 * len(prev_Set.index) / totalEncounters) + ",")
    mycsvh.write("TotalNoCauLoWCPreventive" + "," + "TotalNoCauLoWCPreventive%" + ",")


    # Overall Spurious Cautions
    ###########################

    caution_Set_Filter = ((((df['cautionEndTime'] - df['cautionTime'] < minCautionInterval) & (df['cautionNumber'] == 1)) |
                            ((((df['cautionTime'] - df['cautionFirstEndTime'] > minCautionDiscontinuity) & (df['cautionEndTime'] - df['cautionTime'] < minCautionInterval)) |
                                ((df['cautionTime'] - df['cautionFirstEndTime'] <= minCautionDiscontinuity) & (df['cautionFirstEndTime'] - df['cautionFirstTime'] < minIniCautionInterval) & (df['cautionEndTime'] - df['cautionTime'] < minCautionInterval)) |
                                ((df['cautionTime'] - df['cautionFirstEndTime'] <= minCautionDiscontinuity) & (df['cautionFirstEndTime'] - df['cautionFirstTime'] >= minIniCautionInterval) & (df['cautionEndTime'] - df['cautionFirstTime'] < minCautionInterval))
                            ) & (df['cautionNumber'] > 1))
                            ) & (df['hazFirstTime'] == 0))

    caution_Set = df.loc[caution_Set_Filter]

    fResults.write("\tTotal number of spurious Caution activations: " + str(len(caution_Set.index)) + "  " + str(100 * len(caution_Set.index)/totalEncounters) + "% \n")
    mycsv.write(str(len(caution_Set.index)) + "," + str(100 * len(caution_Set.index) / totalEncounters) + ",")
    mycsvh.write("TotalSpuriousCaution" + "," + "TotalSpuriousCaution%" + ",")


    caution_Set_Filter = (~((((df['cautionEndTime'] - df['cautionTime'] < minCautionInterval) & (df['cautionNumber'] == 1)) |
                            ((((df['cautionTime'] - df['cautionFirstEndTime'] > minCautionDiscontinuity) & (df['cautionEndTime'] - df['cautionTime'] < minCautionInterval)) |
                                ((df['cautionTime'] - df['cautionFirstEndTime'] <= minCautionDiscontinuity) & (df['cautionFirstEndTime'] - df['cautionFirstTime'] < minIniCautionInterval) & (df['cautionEndTime'] - df['cautionTime'] < minCautionInterval)) |
                                ((df['cautionTime'] - df['cautionFirstEndTime'] <= minCautionDiscontinuity) & (df['cautionFirstEndTime'] - df['cautionFirstTime'] >= minIniCautionInterval) & (df['cautionEndTime'] - df['cautionFirstTime'] < minCautionInterval))
                            ) & (df['cautionNumber'] > 1))
                            ) & (df['hazFirstTime'] == 0)) & (df['cautionNumber'] >= 1))


    caution_Set = df.loc[caution_Set_Filter]

    fResults.write("\tTotal active caution: " + str(len(caution_Set.index)) + "  " + str(100 * len(caution_Set.index)/totalEncounters) + "% \n")
    mycsv.write(str(len(caution_Set.index)) + "," + str(100 * len(caution_Set.index) / totalEncounters) + ",")
    mycsvh.write("TotalActiveCaution" + "," + "TotalActiveCaution%" + ",")


    caution_Set_Filter = ((df['hazFirstTime'] > 0))

    caution_Set = df.loc[caution_Set_Filter]

    fResults.write("\tTotal number of LoWC: " + str(len(caution_Set.index)) + "  " + str(100 * len(caution_Set.index)/totalEncounters) + "% \n")
    mycsv.write(str(len(caution_Set.index)) + "," + str(100 * len(caution_Set.index) / totalEncounters) + ",")
    mycsvh.write("TotalLoWC" + "," + "TotalLoWC%" + ",")


    caution_Set_Filter = ((df['cautionTime'] <= 0) & (df['hazFirstTime'] > 0))

    caution_Set = df.loc[caution_Set_Filter]

    fResults.write("\tTotal number of LoWC (with no Caution): " + str(len(caution_Set.index)) + "  " + str(100 * len(caution_Set.index)/totalEncounters) + "% \n")
    mycsv.write(str(len(caution_Set.index)) + "," + str(100 * len(caution_Set.index) / totalEncounters) + ",")
    mycsvh.write("TotalNoCautionLoWC" + "," + "TotalNoCautionLoWC%" + ",")


    caution_Set_Filter = ((df['nmacFirstTime'] > 0))

    caution_Set = df.loc[caution_Set_Filter]

    fResults.write("\tTotal number of NMAC activations: " + str(len(caution_Set.index)) + "  " + str(100 * len(caution_Set.index)/totalEncounters) + "% \n")
    mycsv.write(str(len(caution_Set.index)) + "," + str(100 * len(caution_Set.index) / totalEncounters) + ",")
    mycsvh.write("TotalNMAC" + "," + "TotalNMAC%" + ",")


    caution_Set_Filter = ((df['cautionTime'] <= 0) & (df['hazFirstTime'] <= 0) & (df['nmacFirstTime'] > 0))

    caution_Set = df.loc[caution_Set_Filter]

    fResults.write("\tTotal number of NMAC activations (no LoWC): " + str(len(caution_Set.index)) + "  " + str(100 * len(caution_Set.index)/totalEncounters) + "% \n")
    mycsv.write(str(len(caution_Set.index)) + "," + str(100 * len(caution_Set.index) / totalEncounters) + ",")
    mycsvh.write("TotalNMACnoLoWC" + "," + "TotalNMACnoLoWC%" + ",")

    fResults.write("\n")



def evaluateSpuriousCautionTimming(fResults, mydoc, mycsv, mycsvh, df, caption, folderPath):

    totalEncounters = len(df.index)

    ########################################
    # TIME RANGE FOR SPURIOUS CAUTION ALERTS

    fResults.write("\n\n")
    fResults.write("Spurious Caution Timing Evaluation:" + "\n")

    mydoc.add_heading("Spurious Caution Analysis", 2)


    caution_Set_Filter = ((((df['cautionEndTime'] - df['cautionTime'] < minCautionInterval) & (df['cautionNumber'] == 1)) |
                            ((((df['cautionTime'] - df['cautionFirstEndTime'] > minCautionDiscontinuity) & (df['cautionEndTime'] - df['cautionTime'] < minCautionInterval)) |
                                ((df['cautionTime'] - df['cautionFirstEndTime'] <= minCautionDiscontinuity) & (df['cautionFirstEndTime'] - df['cautionFirstTime'] < minIniCautionInterval) & (df['cautionEndTime'] - df['cautionTime'] < minCautionInterval)) |
                                ((df['cautionTime'] - df['cautionFirstEndTime'] <= minCautionDiscontinuity) & (df['cautionFirstEndTime'] - df['cautionFirstTime'] >= minIniCautionInterval) & (df['cautionEndTime'] - df['cautionFirstTime'] < minCautionInterval))
                            ) & (df['cautionNumber'] > 1))
                            ) & (df['hazFirstTime'] == 0))

    caution_Set = df.loc[caution_Set_Filter]

    fResults.write("\tTotal number of spurious Caution activations: " + str(len(caution_Set.index)) + "  " + str(100 * len(caution_Set.index)/totalEncounters) + "% \n")
    mycsv.write(str(len(caution_Set.index)) + "," + str(100 * len(caution_Set.index) / totalEncounters) + ",")
    mycsvh.write("TotalSpurious" + "," + "TotalSpurious%" + ",")

    mydoc.add_paragraph("Total number of spurious Caution activations: " + str(len(caution_Set.index)) + "  \t" + str(100 * len(caution_Set.index)/totalEncounters) + "%")

    ########## TIMING DOUBLE CHECK

    caution_Set_Filter = (caution_Set['cautionEndTime'] - caution_Set['cautionTime'] < 0)

    caution_Set_Error = caution_Set.loc[caution_Set_Filter]


    fResults.write("\tIterating erroneous spurious caution activations:\n")
    for index, row in caution_Set_Error.iterrows():
        fResults.write("\t( Layer: " + str(int(row["layer"])) + " - EncounterId: " + str(int(row["EncounterId"])) + " : " + str(int(row["cautionFirstTime"])) + " -> " + str(int(row["cautionEndTime"])) + " )\n")
    fResults.write("\tDone.\n")

    caution_Set_Filter = (caution_Set['cpaTime'] - caution_Set['cautionTime'] < 0)

    caution_Set_Error = caution_Set.loc[caution_Set_Filter]


    fResults.write("\tIterating erroneous spurious caution activations:\n")
    for index, row in caution_Set_Error.iterrows():
        fResults.write("\t( Layer: " + str(int(row["layer"])) + " - EncounterId: " + str(int(row["EncounterId"])) + " : " + str(int(row["cautionFirstTime"])) + " -> " + str(int(row["cautionEndTime"])) + " )\n")
    fResults.write("\tDone.\n")

    ##########


    hist_data = [caution_Set['cpaTime'] - caution_Set['cautionFirstTime'], \
                 caution_Set['cautionEndTime'] - caution_Set['cautionFirstTime'], \
                 caution_Set['cautionFirstEndTime'] - caution_Set['cautionFirstTime'], \
                 caution_Set['cautionTtHaz'], \
                 caution_Set['cautionFirstTtHaz'], \
                 ]
    group_labels = ['Time to CPA', 'Spurious Caution duration', 'First Caution duration', "Predicted Time to LoWC", "First Predicted Time to LoWC"]
    df3 = pd.concat(hist_data, axis=1, keys=group_labels)


    fResults.write("\tTime to CPA. Mean: " + str(df3.iloc[:, 0].mean()) + " Stddev: " + str(df3.iloc[:, 0].std()) + "\n")
    fResults.write("\tSpurious Caution duration. Mean: " + str(df3.iloc[:, 1].mean()) + " Stddev: " + str(df3.iloc[:, 1].std()) + "\n")
    fResults.write("\tFirst Caution duration. Mean: " + str(df3.iloc[:, 2].mean()) + " Stddev: " + str(df3.iloc[:, 2].std()) + "\n")
    fResults.write("\tPredicted Time to LoWC. Mean: " + str(df3.iloc[:, 3].mean()) + " Stddev: " + str(df3.iloc[:, 3].std()) + "\n")
    fResults.write("\tFirst Predicted Time to LoWC. Mean: " + str(df3.iloc[:, 4].mean()) + " Stddev: " + str(df3.iloc[:, 4].std()) + "\n")

    mycsv.write(str(df3.iloc[:, 0].mean()) + "," + str(df3.iloc[:, 0].std()) + ",")
    mycsv.write(str(df3.iloc[:, 1].mean()) + "," + str(df3.iloc[:, 1].std()) + ",")
    mycsv.write(str(df3.iloc[:, 2].mean()) + "," + str(df3.iloc[:, 2].std()) + ",")
    mycsv.write(str(df3.iloc[:, 3].mean()) + "," + str(df3.iloc[:, 3].std()) + ",")
    mycsv.write(str(df3.iloc[:, 4].mean()) + "," + str(df3.iloc[:, 4].std()) + ",")
    mycsvh.write("Time to CPA Mean" + "," + "Time to CPA STD" + ",")
    mycsvh.write("Caution Duration Mean" + "," + "Caution Duration STD" + ",")
    mycsvh.write("First Caution Duration Mean" + "," + "First Caution Duration STD" + ",")
    mycsvh.write("Predicted Time LoWC Mean" + "," + "Predicted Time LoWC STD" + ",")
    mycsvh.write("First Predicted Time LoWC Mean" + "," + "First Predicted Time LoWC STD" + ",")


    # SHOW FIGURE
    fig = ff.create_distplot(hist_data, group_labels, bin_size=[5, 5, 5, 5, 5], show_hist= False)
    fig.update_layout(margin_b= 0, margin_l= 0, margin_r= 0, width= 1000)
    fig.update_xaxes(title_text='Time(s)', range=[0, cautionTimeRange])
    fig.update_layout(title_text='Spurious Caution Alert: Time (sec)')
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="right",
        x=0.99
    ))

    filename = folderPath + "SpuriousCaution_Time_Comparison_Histogram-A.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "SpuriousCaution_Time_Comparison_Histogram-A.png"
    fig.write_image(filename)

    mydoc.add_picture(folderPath + "SpuriousCaution_Time_Comparison_Histogram-A.png", width=docx.shared.Inches(7.5))

    print(df3.to_numpy())

    print(df3.to_records())

    fig = px.ecdf(df3, group_labels, ecdfmode="reversed", marginal="box")
    fig.update_layout(margin_b= 0, margin_l= 0, margin_r= 0, width= 1000)
    fig.update_xaxes(title_text='Time(s)', range=[0, spuriousCautionTimeRange])
    fig.update_yaxes(title_text='CDF')
    fig.update_layout(title_text='Spurious Caution Alert')
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="right",
        x=0.99
    ))

    filename = folderPath + "SpuriousCaution_Time_Comparison_Histogram-CDF.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "SpuriousCaution_Time_Comparison_Histogram-CDF.png"
    fig.write_image(filename)

    mydoc.add_picture(folderPath + "SpuriousCaution_Time_Comparison_Histogram-CDF.png", width=docx.shared.Inches(7.5))


    fig = px.ecdf(df3, group_labels, marginal="box")
    fig.update_layout(margin_b= 0, margin_l= 0, margin_r= 0, width= 1000)
    fig.update_xaxes(title_text='Time(s)', range=[0, spuriousCautionTimeRange])
    fig.update_yaxes(title_text='CDF')
    fig.update_layout(title_text='Spurious Caution Alert')
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="right",
        x=0.99
    ))

    filename = folderPath + "non-Rev-SpuriousCaution_Time_Comparison_Histogram-CDF.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "non-Rev-SpuriousCaution_Time_Comparison_Histogram-CDF.png"
    fig.write_image(filename)



    hist_data = [caution_Set['cautionFirstRange'], \
                 caution_Set['cautionRange'], \
                 caution_Set['cautionEndRange'], \
                 ]
    group_labels = ['Range at First Caution', 'Range at Caution', 'Range at End of Caution']
    df3 = pd.concat(hist_data, axis=1, keys=group_labels)


    if useNM:
        fResults.write("\tRange at First Caution: " + str(units.m_to_nm(df3.iloc[:, 0].mean())) + " Stddev: " + str(units.m_to_nm(df3.iloc[:, 0].std())) + "\n")
        fResults.write("\tRange at Caution: " + str(units.m_to_nm(df3.iloc[:, 1].mean())) + " Stddev: " + str(units.m_to_nm(df3.iloc[:, 1].std())) + "\n")
        fResults.write("\tRange at End of Caution: " + str(units.m_to_nm(df3.iloc[:, 2].mean())) + " Stddev: " + str(units.m_to_nm(df3.iloc[:, 2].std())) + "\n")

        mycsv.write(str(units.m_to_nm(df3.iloc[:, 0].mean())) + "," + str(units.m_to_nm(df3.iloc[:, 0].std())) + ",")
        mycsv.write(str(units.m_to_nm(df3.iloc[:, 1].mean())) + "," + str(units.m_to_nm(df3.iloc[:, 1].std())) + ",")
        mycsv.write(str(units.m_to_nm(df3.iloc[:, 2].mean())) + "," + str(units.m_to_nm(df3.iloc[:, 2].std())) + ",")
    else:
        fResults.write("\tRange at First Caution: " + str(df3.iloc[:, 0].mean()) + " Stddev: " + str(df3.iloc[:, 0].std()) + "\n")
        fResults.write("\tRange at Caution: " + str(df3.iloc[:, 1].mean()) + " Stddev: " + str(df3.iloc[:, 1].std()) + "\n")
        fResults.write("\tRange at End of Caution: " + str(df3.iloc[:, 2].mean()) + " Stddev: " + str(df3.iloc[:, 2].std()) + "\n")

        mycsv.write(str(df3.iloc[:, 0].mean()) + "," + str(df3.iloc[:, 0].std()) + ",")
        mycsv.write(str(df3.iloc[:, 1].mean()) + "," + str(df3.iloc[:, 1].std()) + ",")
        mycsv.write(str(df3.iloc[:, 2].mean()) + "," + str(df3.iloc[:, 2].std()) + ",")

    mycsvh.write("Range First Caution Mean" + "," + "Range First Caution STD" + ",")
    mycsvh.write("Range Caution Mean" + "," + "Range Caution STD" + ",")
    mycsvh.write("Range End Caution Mean" + "," + "Range End Caution STD" + ",")


    hist1 = np.histogram(caution_Set['cautionFirstRange'], bins=30)
    dist_1 = scipy.stats.rv_histogram(hist1)
    if useNM:
        p1 = dist_1.cdf(units.nm_to_m(cdfDistanceNM))
    else:
        p1 = dist_1.cdf(cdfDistanceM)



    hist2 = np.histogram(caution_Set['cautionEndRange'], bins=30)
    dist_2 = scipy.stats.rv_histogram(hist2)
    if useNM:
        p2 = dist_2.cdf(units.nm_to_m(cdfDistanceNM))
    else:
        p2 = dist_2.cdf(cdfDistanceM)

    fResults.write("\tRange Caution. At caution activation: " + str(p1*100) + " At caution end: " + str(p2*100) + "\n")
    mycsv.write(str(p1*100) + "," + str(p2*100) + ",")
    mycsvh.write("Range Begin Caution%" + "," + "Range End Caution%" + ",")


    hist1 = np.histogram(caution_Set['cautionFirstElevation'], bins=30)
    dist_1 = scipy.stats.rv_histogram(hist1)
    p1 = dist_1.cdf(15) - dist_1.cdf(-15)


    hist2 = np.histogram(caution_Set['cautionEndElevation'], bins=80)
    dist_2 = scipy.stats.rv_histogram(hist2)
    p2 = dist_2.cdf(15) - dist_2.cdf(-15)

    fResults.write("\tElevation Caution. At caution activation: " + str(p1*100) + " At caution end: " + str(p2*100) + "\n")
    mycsv.write(str(p1*100) + "," + str(p2*100) + ",")
    mycsvh.write("Elevation Begin Caution%" + "," + "Elevation End Caution%" + ",")


    hist1 = np.histogram(caution_Set['cautionFirstAzimuth'], bins=180)
    dist_1 = scipy.stats.rv_histogram(hist1)
    p1 = dist_1.cdf(110) - dist_1.cdf(-110)


    hist2 = np.histogram(caution_Set['cautionEndAzimuth'], bins=90)
    dist_2 = scipy.stats.rv_histogram(hist2)
    p2 = dist_2.cdf(110) - dist_2.cdf(-110)

    fResults.write("\tAzimuth Caution. At caution activation: " + str(p1*100) + " At caution end: " + str(p2*100) + "\n")
    mycsv.write(str(p1*100) + "," + str(p2*100) + ",")
    mycsvh.write("Azimuth Begin Caution%" + "," + "Azimuth End Caution%" + ",")


    if useNM:
        inCoverageVector1 = (caution_Set['cautionFirstRange'] <= units.nm_to_m(rangeNM))
    else:
        inCoverageVector1 = (caution_Set['cautionFirstRange'] <= units.nm_to_m(rangeM))

    inCoverageVector2 = (caution_Set['cautionFirstElevation'] <= 15) & (caution_Set['cautionFirstElevation'] >= -15)
    inCoverageVector3 = (caution_Set['cautionFirstAzimuth'] <= 110) & (caution_Set['cautionFirstAzimuth'] >= -110)

    inCoverage1 = np.count_nonzero(inCoverageVector1 & inCoverageVector2 & inCoverageVector3)

    inCoverageVector1 = (caution_Set['cautionEndRange'] <= units.nm_to_m(8.0))
    inCoverageVector2 = (caution_Set['cautionEndElevation'] <= 15) & (caution_Set['cautionEndElevation'] >= -15)
    inCoverageVector3 = (caution_Set['cautionEndAzimuth'] <= 110) & (caution_Set['cautionEndAzimuth'] >= -110)

    inCoverage2 = np.count_nonzero(inCoverageVector1 & inCoverageVector2 & inCoverageVector3)

    fResults.write("\tIn coverage. At caution activation: " + str(100 * inCoverage1/len(caution_Set.index)) + " At caution end: " + str(100 * inCoverage2/len(caution_Set.index)) + "\n")
    fResults.write("\n\n")

    mycsv.write(str(100 * inCoverage1/len(caution_Set.index)) + "," + str(100 * inCoverage2/len(caution_Set.index)) + ",")
    mycsvh.write("Coverage Begin Caution%" + "," + "Coverage End Caution%" + ",")


    hist_data = [units.m_to_nm(caution_Set['cautionFirstRange']), \
                 units.m_to_nm(caution_Set['cautionRange']), \
                 units.m_to_nm(caution_Set['cautionEndRange']), \
                 ]
    df3 = pd.concat(hist_data, axis=1, keys=group_labels)

    # SHOW FIGURE
    fig = ff.create_distplot(hist_data, group_labels, bin_size=[0.5, 0.5, 0.5], histnorm='probability', show_hist= False)
    fig.update_layout(margin_b= 0, margin_l= 0, margin_r= 0, width= 1000)
    fig.update_xaxes(title_text='Horizontal Range (NM)', range=[0, 15])
    fig.update_layout(title_text='Spurious Caution Alert: Horizontal Range')
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="right",
        x=0.99
    ))

    filename = folderPath + "SpuriousCaution_Range_Comparison_Histogram-A.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "SpuriousCaution_Range_Comparison_Histogram-A.png"
    fig.write_image(filename)

    mydoc.add_picture(folderPath + "SpuriousCaution_Range_Comparison_Histogram-A.png", width=docx.shared.Inches(7.5))


    fig = px.ecdf(df3, group_labels, ecdfmode="reversed", marginal="box")
    fig.update_layout(margin_b= 0, margin_l= 0, margin_r= 0, width= 1000)
    fig.update_xaxes(title_text='Horizontal Range (NM)', range=[0, 15])
    fig.update_yaxes(title_text='CDF')
    fig.update_layout(title_text='Spurious Caution Alert: Horizontal Range')
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="right",
        x=0.99
    ))

    filename = folderPath + "SpuriousCaution_Range_Comparison_Histogram-CDF.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "SpuriousCaution_Range_Comparison_Histogram-CDF.png"
    fig.write_image(filename)

    mydoc.add_picture(folderPath + "SpuriousCaution_Range_Comparison_Histogram-CDF.png", width=docx.shared.Inches(7.5))



    hist_data = [abs(caution_Set['cautionFirstBeta'] - caution_Set['cautionFirstEndBeta']), \
                 abs(caution_Set['cautionBeta'] - caution_Set['cautionEndBeta']), \
                 abs(caution_Set['cautionFirstDz'] - caution_Set['cautionFirstEndDz'])/10, \
                 abs(caution_Set['cautionDz'] - caution_Set['cautionEndDz'])/10 \
                 ]
    group_labels = ['First Delta Beta', 'Delta Beta', 'First Delta Dz', 'Delta Dz']
    df3 = pd.concat(hist_data, axis=1, keys=group_labels)

    fig = ff.create_distplot(hist_data, group_labels, bin_size=[0.5, 0.5, 5, 5], histnorm='probability', show_hist= False)
    fig.update_layout(margin_b= 0, margin_l= 0, margin_r= 0, width= 1000)
    fig.update_xaxes(title_text='Beta variation (deg), Dz variation (10ft)') #, range=[0, 25]
    fig.update_layout(title_text='Spurious Caution Alert: Beta / Dz variation')
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="right",
        x=0.99
    ))

    filename = folderPath + "SpuriousCaution_Delta_Comparison_Histogram-A.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "SpuriousCaution_Delta_Comparison_Histogram-A.png"
    fig.write_image(filename)

    mydoc.add_picture(folderPath + "SpuriousCaution_Delta_Comparison_Histogram-A.png", width=docx.shared.Inches(7.5))


    #df3_plot = pd.concat(df3, axis=1, keys=group_labels)

    fig = px.ecdf(df3, group_labels, marginal="box")
    fig.update_layout(margin_b= 0, margin_l= 0, margin_r= 0, width= 1000)
    fig.update_xaxes(title_text='Beta variation (deg), Dz variation (10ft)') #, range=[0, 120]
    fig.update_yaxes(title_text='CDF')
    fig.update_layout(title_text='Spurious Caution Alert: Beta / Dz variation')
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="right",
        x=0.99
    ))

    filename = folderPath + "SpuriousCaution_Delta_Comparison_Histogram-CDF.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "SpuriousCaution_Delta_Comparison_Histogram-CDF.png"
    fig.write_image(filename)

    mydoc.add_picture(folderPath + "SpuriousCaution_Delta_Comparison_Histogram-CDF.png", width=docx.shared.Inches(7.5))



    #fig = generate_comparison_histogram3(caution_Set['cpaTime'] - caution_Set['cautionTime'], caution_Set['cautionEndTime'] - caution_Set['cautionTime'], caution_Set['cautionTtHaz'], 0, 0, 0,
    #                                     color1, color2, color3, 10, 'Time to CPA', 'Caution duration',  'Time to WCV Violation', caption)

    #filename = folderPath + "SpuriousCaution_TimeRange_Comparison_Histogram-B.html"
    #fResults.write("\tGenerating figure: " + filename + "\n")
    #fig.write_html(filename)
    #filename = folderPath + "SpuriousCaution_TimeRange_Comparison_Histogram-B.png"
    #fig.write_image(filename)
    #plot(fig, filename=filename)


    caution_Set_Filter = ((((((df['cautionTime'] - df['cautionFirstEndTime'] > minCautionDiscontinuity) & (df['cautionEndTime'] - df['cautionTime'] < minCautionInterval)) |
                                ((df['cautionTime'] - df['cautionFirstEndTime'] <= minCautionDiscontinuity) & (df['cautionFirstEndTime'] - df['cautionFirstEndTime'] < minIniCautionInterval) & (df['cautionEndTime'] - df['cautionTime'] < minCautionInterval)) |
                                ((df['cautionTime'] - df['cautionFirstEndTime'] <= minCautionDiscontinuity) & (df['cautionFirstEndTime'] - df['cautionFirstEndTime'] >= minIniCautionInterval) & (df['cautionEndTime'] - df['cautionFirstTime'] < minCautionInterval))
                            ) & (df['cautionNumber'] > 1))
                            ) & (df['hazFirstTime'] == 0))

    caution_Set = df.loc[caution_Set_Filter]


    fResults.write("\tIterating complex spurious caution activations:\n")
    for index, row in caution_Set.iterrows():
        fResults.write("\t( Layer: " + str(int(row["layer"])) + " - EncounterId: " + str(int(row["EncounterId"])) + " : " + str(int(row["cautionFirstTime"])) + " -> " + str(int(row["cautionEndTime"])) + " )\n")
    fResults.write("\tDone.\n")



    ##############################################################
    # TIME RANGE FOR SPURIOUS CAUTION ALERTS AND SEPARATION ALERTS

    caution_Set_Filter = ((((df['cautionEndTime'] - df['cautionTime'] < minCautionInterval) & (df['cautionNumber'] == 1)) |
                           ((((df['cautionTime'] - df['cautionFirstEndTime'] > minCautionDiscontinuity) & (df['cautionEndTime'] - df['cautionTime'] < minCautionInterval)) |
                             ((df['cautionTime'] - df['cautionFirstEndTime'] <= minCautionDiscontinuity) & (df['cautionFirstEndTime'] - df['cautionFirstTime'] < minIniCautionInterval) & (df['cautionEndTime'] - df['cautionTime'] < minCautionInterval)) |
                             ((df['cautionTime'] - df['cautionFirstEndTime'] <= minCautionDiscontinuity) & (df['cautionFirstEndTime'] - df['cautionFirstTime'] >= minIniCautionInterval) & (df['cautionEndTime'] - df['cautionFirstTime'] < minCautionInterval))
                           ) & (df['cautionNumber'] > 1))
                           ) & (df['hazFirstTime'] == 0))

    caution_Set = df.loc[caution_Set_Filter]

    caution_Set_Filter = ((caution_Set['sepTime'] > 0) & (caution_Set['sepRelevant'] == True) & (caution_Set['stcaTime'] > 0) & (caution_Set['stcaRelevant'] == True))
    caution_Set = caution_Set.loc[caution_Set_Filter]

    fResults.write("\tTotal number of spurious Caution activations with associated separation/STCA violation: " + str(len(caution_Set.index)) + "\n")

    hist_data = [caution_Set['cautionFirstRange'],
                 caution_Set['stcaTimeRange'],
                 caution_Set['sepTimeRange'],
                 abs(caution_Set['cautionFirstDz']),
                 abs(caution_Set['stcaTimeDz']),
                 abs(caution_Set['sepTimeDz']),
                 ]
    group_labels = ['Range at Caution activation', 'Range at STCA violation', 'Range at Separation violation', 'Dz at Caution activation', 'Dz at STCA activation', 'Dz at Separation violation']

    df3 = pd.concat(hist_data, axis=1, keys=group_labels)


    fResults.write("\tRange at Caution activation. Mean: " + str(units.m_to_nm(df3.iloc[:, 0].mean())) + " Stddev: " + str(units.m_to_nm(df3.iloc[:, 0].std())) + "\n")
    fResults.write("\tRange at STCA violation. Mean: " + str(units.m_to_nm(df3.iloc[:, 1].mean())) + " Stddev: " + str(units.m_to_nm(df3.iloc[:, 1].std())) + "\n")
    fResults.write("\tRange at Separation violation. Mean: " + str(units.m_to_nm(df3.iloc[:, 2].mean())) + " Stddev: " + str(units.m_to_nm(df3.iloc[:, 2].std())) + "\n")
    fResults.write("\tDz at Caution activation. Mean: " + str(units.m_to_ft(df3.iloc[:, 3].mean())) + " Stddev: " + str(units.m_to_ft(df3.iloc[:, 3].std())) + "\n")
    fResults.write("\tDz at STCA activation. Mean: " + str(units.m_to_ft(df3.iloc[:, 4].mean())) + " Stddev: " + str(units.m_to_ft(df3.iloc[:, 4].std())) + "\n")
    fResults.write("\tDz at Separation violation. Mean: " + str(units.m_to_ft(df3.iloc[:, 5].mean())) + " Stddev: " + str(units.m_to_ft(df3.iloc[:, 5].std())) + "\n")

    if useNM:
        mycsv.write(str(units.m_to_nm(df3.iloc[:, 0].mean())) + "," + str(units.m_to_nm(df3.iloc[:, 0].std())) + ",")
        mycsv.write(str(units.m_to_nm(df3.iloc[:, 1].mean())) + "," + str(units.m_to_nm(df3.iloc[:, 1].std())) + ",")
        mycsv.write(str(units.m_to_ft(df3.iloc[:, 2].mean())) + "," + str(units.m_to_ft(df3.iloc[:, 2].std())) + ",")
        mycsv.write(str(units.m_to_ft(df3.iloc[:, 3].mean())) + "," + str(units.m_to_ft(df3.iloc[:, 3].std())) + ",")
        mycsv.write(str(units.m_to_ft(df3.iloc[:, 4].mean())) + "," + str(units.m_to_ft(df3.iloc[:, 4].std())) + ",")
        mycsv.write(str(units.m_to_ft(df3.iloc[:, 5].mean())) + "," + str(units.m_to_ft(df3.iloc[:, 5].std())) + ",")
    else:
        mycsv.write(str(df3.iloc[:, 0].mean()) + "," + str(df3.iloc[:, 0].std()) + ",")
        mycsv.write(str(df3.iloc[:, 1].mean()) + "," + str(df3.iloc[:, 1].std()) + ",")
        mycsv.write(str(df3.iloc[:, 2].mean()) + "," + str(df3.iloc[:, 2].std()) + ",")
        mycsv.write(str(df3.iloc[:, 3].mean()) + "," + str(df3.iloc[:, 3].std()) + ",")
        mycsv.write(str(df3.iloc[:, 4].mean()) + "," + str(df3.iloc[:, 4].std()) + ",")
        mycsv.write(str(df3.iloc[:, 5].mean()) + "," + str(df3.iloc[:, 5].std()) + ",")


    mycsvh.write("Range Begin Caution Mean" + "," + "Range Begin Caution STD" + ",")
    mycsvh.write("Range STCA Mean" + "," + "Range STCA STD" + ",")
    mycsvh.write("Range Sep Mean" + "," + "Range Sep STD" + ",")
    mycsvh.write("Dz Begin Caution Mean" + "," + "Dz Begin Caution STD" + ",")
    mycsvh.write("Dz Begin STCA Mean" + "," + "Dz Begin STCA STD" + ",")
    mycsvh.write("Dz Begin Sep Mean" + "," + "Dz Begin Sep STD" + ",")

    # Create distplot with custom bin_size
    #fig = ff.create_distplot(hist_data, group_labels, bin_size=[15, 15, 15, 15], histnorm='probability')

    #filename = folderPath + "SpuriousCautionSeparation_TimeRange_Comparison_Histogram-A.html"
    #fResults.write("\tGenerating figure: " + filename)
    #fig.write_html(filename)
    #filename = folderPath + "SpuriousCautionSeparation_TimeRange_Comparison_Histogram-A.png"
    #fig.write_image(filename)
    #plot(fig, filename=filename)


    #fig = generate_comparison_histogram1(caution_Set['cautionFirstTime'] - caution_Set['sepTime'], -100, color1, 15, 'Separation_SpuriousCaution', caption, 'TimeRange (sec)')

    #filename = folderPath + "SpuriousCautionSeparation_TimeRange_Comparison_Histogram-B.html"
    #fResults.write("\tGenerating figure: " + filename + "\n")
    #fig.write_html(filename)
    #filename = folderPath + "SpuriousCautionSeparation_TimeRange_Comparison_Histogram-B.png"
    #fig.write_image(filename)
    #plot(fig, filename=filename)




def evaluateACASTimming(fResults, mydoc, mycsv, mycsvh, df_own, df_acas, caption, folderPath):

    totalEncounters = len(df_own.index)

    # TIME RANGE FOR ACTIVE CAUTION ALERTS

    fResults.write("\n\n")
    fResults.write("ACAS Timming Evaluation:" + "\n")

    mydoc.add_page_break()
    mydoc.add_heading("ACAS Resume", 2)

    # for (index_1, row_1), (index_2, row_2) in zip(df_own.iterrows(), df_acas.iterrows()):
    #    print(str(row_1['EncounterId']) + "-" + str(row_1['layer']) + " " + str(row_2['EncounterId']) + "-" + str(row_2['layer']) + " " + str(index_1) + " " + str(index_2) + ": " + str(row_1['cpaTime']) + " -- " + str(row_2['cpaTime']) + " " + str(row_2['tcasRaTime']))

    #    if row_1['EncounterId'] != row_2['EncounterId']:
    #        print ("WARNING!!!!")
    #        break


    # STEP 0 ####################################################

    fResults.write("\n")
    fResults.write("Basic active Caution Timming Evaluation:" + "\n")

    # Non-active caution condition force with caution >= 1
    # caution_Set_Filter = ((df['aircraft2Class'] != 3) & (df['aircraft2Class'] != 5) & (df['aircraft2Class'] != 15))
    # df_own = df.loc[caution_Set_Filter]
    # df_own.reset_index(inplace=True)

    # Non-active caution condition force with caution >= 1
    caution_Set_Filter = (~((((df_own['cautionEndTime'] - df_own['cautionTime'] < minCautionInterval) & (df_own['cautionNumber'] == 1)) |
                            ((((df_own['cautionTime'] - df_own['cautionFirstEndTime'] > minCautionDiscontinuity) & (df_own['cautionEndTime'] - df_own['cautionTime'] < minCautionInterval)) |
                                ((df_own['cautionTime'] - df_own['cautionFirstEndTime'] <= minCautionDiscontinuity) & (df_own['cautionFirstEndTime'] - df_own['cautionFirstTime'] < minIniCautionInterval) & (df_own['cautionEndTime'] - df_own['cautionTime'] < minCautionInterval)) |
                                ((df_own['cautionTime'] - df_own['cautionFirstEndTime'] <= minCautionDiscontinuity) & (df_own['cautionFirstEndTime'] - df_own['cautionFirstTime'] >= minIniCautionInterval) & (df_own['cautionEndTime'] - df_own['cautionFirstTime'] < minCautionInterval))
                            ) & (df_own['cautionNumber'] > 1))
                            ) & (df_own['hazFirstTime'] == 0)) & (df_own['cautionNumber'] >= 1) & (df_own['hazFirstTime'] >= 1) & (df_own['hazLmvViolatedTime'] > 0))


    bcs = df_own.loc[caution_Set_Filter]
    bcs_acas = df_acas.loc[caution_Set_Filter]

    caution_Set_Filter = ((bcs_acas['tcasRaTime'] > 0))

    bcs_acas = bcs_acas.loc[caution_Set_Filter]
    bcs = bcs.loc[caution_Set_Filter]


    totalActive = len(bcs.index)

    fResults.write("\tTotal active caution with ACAS: " + str(len(bcs.index)) + "\n")
    mycsv.write(str(len(bcs.index)) + ",")
    mycsvh.write("TotalActiveCautionACAS" + ",")

    mydoc.add_paragraph("Total active caution with ACAS: " + str(len(bcs.index)) + "\t out of Total: " + str(totalEncounters))


    # Single caution with caution duration >= minCautionInterval
    caution_Set_Filter = ((bcs['cautionNumber'] == 1) & (bcs['cautionEndTime'] - bcs['cautionTime'] >= minCautionInterval))

    caution_Set1 = bcs.loc[caution_Set_Filter]
    caution_Set1_acas = bcs_acas.loc[caution_Set_Filter]

    hist_data_set1 = [caution_Set1['haz_SLoWC_time'] - caution_Set1_acas['tcasRaTime'], \
                      caution_Set1_acas['tcasRaTime'] - caution_Set1['cautionTime'], \
                      caution_Set1_acas['tcasRaTime'] - caution_Set1['hazLmvViolatedTime'], \
                      caution_Set1_acas['tcasRaTime'] - caution_Set1['hazFirstTime'], \
                      ]

    fResults.write("\tSingle caution activation >= minCautionInterval: " + str(len(caution_Set1.index)) + "  " + str(100 * len(caution_Set1.index)/totalActive) + "% \n")
    mycsv.write(str(len(caution_Set1.index)) + "," + str(100 * len(caution_Set1.index)/totalActive) + ",")
    mycsvh.write("SingleActiveCaution>minCautionInterval ACAS" + "," + "SingleActiveCaution% ACAS" + ",")

    mydoc.add_paragraph("Single caution activation >= minCautionInterval ACAS: " + str(len(caution_Set1.index)) + "  \t" + str(100 * len(caution_Set1.index)/totalActive) + "%")

    #for (index_1, row_1), (index_2, row_2) in zip(caution_Set1.iterrows(), caution_Set1_acas.iterrows()):
    #    print(str(row_1['EncounterId']) + " " + str(index_1) + " " + str(index_2) + ": " + str(row_1['cpaTime']) + " -- " + str(row_2['cpaTime']) + " " + str(row_2['tcasRaTime']))

    #caution_Set_Filter = ((bcs['hazFirstTime'] > 0) & (bcs['cautionNumber'] == 1) & (bcs['cautionEndTime'] - bcs['cautionTime'] < 20))

    #caution_Set1_1 = bcs.loc[caution_Set_Filter]

    #fResults.write("\tSingle caution activation < 20 & LoWC: " + str(len(caution_Set1_1.index)) + "  " + str(100 * len(caution_Set1_1.index)/totalActive) + "% \n")
    #mycsv.write(str(len(caution_Set1_1.index)) + "," + str(100 * len(caution_Set1_1.index)/totalActive) + ",")
    #mycsvh.write("SingleActiveCaution<20 LoWC" + "," + "SingleActiveCaution<20 LoWC%" + ",")

    #mydoc.add_paragraph("Single caution activation < 20 & LoWC: " + str(len(caution_Set1_1.index)) + "  \t" + str(100 * len(caution_Set1_1.index)/totalActive) + "%")


    caution_Set_Filter = (((bcs['cautionFirstEndTime'] - bcs['cautionFirstTime'] < minIniCautionInterval) | (bcs['cautionTime'] - bcs['cautionFirstEndTime'] > minCautionDiscontinuity)) & (bcs['cautionEndTime'] - bcs['cautionTime'] >= minCautionInterval) & (bcs['cautionNumber'] > 1))

    caution_Set2 = bcs.loc[caution_Set_Filter]
    caution_Set2_acas = bcs_acas.loc[caution_Set_Filter]

    hist_data_set2 = [caution_Set2['haz_SLoWC_time'] - caution_Set2_acas['tcasRaTime'], \
                      caution_Set2_acas['tcasRaTime'] - caution_Set2['cautionFirstTime'], \
                      caution_Set2_acas['tcasRaTime'] - caution_Set2['hazLmvViolatedTime'], \
                      caution_Set2_acas['tcasRaTime'] - caution_Set2['hazFirstTime']]


    fResults.write("\tMultiple caution, unique activation ACAS: " + str(len(caution_Set2.index)) + "  " + str(100 * len(caution_Set2.index)/totalActive) + "% \n")
    mycsv.write(str(len(caution_Set2.index)) + "," + str(100 * len(caution_Set2.index)/totalActive) + ",")
    mycsvh.write("MultipleCautionUniqueActACAS" + "," + "MultipleCautionUniqueAct% ACAS" + ",")

    mydoc.add_paragraph("Multiple caution, unique activation ACAS: " + str(len(caution_Set2.index)) + "  \t" + str(100 * len(caution_Set2.index)/totalActive) + "%")

    #caution_Set_Filter = (((bcs['cautionFirstEndTime'] - bcs['cautionFirstTime'] < 5) | (bcs['cautionTime'] - bcs['cautionFirstEndTime'] > 10)) & (bcs['cautionEndTime'] - bcs['cautionTime'] < 20) & (bcs['cautionNumber'] > 1))

    #caution_Set2_1 = bcs.loc[caution_Set_Filter]

    #fResults.write("\tMultiple caution, unique activation and LoWC: " + str(len(caution_Set2_1.index)) + "  " + str(100 * len(caution_Set2_1.index)/totalActive) + "% \n")

    #mydoc.add_paragraph("Multiple caution, unique activation and LoWC: " + str(len(caution_Set2_1.index)) + "  \t" + str(100 * len(caution_Set2_1.index)/totalActive) + "%")

    caution_Set_Filter = (((bcs['cautionFirstEndTime'] - bcs['cautionFirstTime'] >= minIniCautionInterval) & (bcs['cautionTime'] - bcs['cautionFirstEndTime'] <= minCautionDiscontinuity)) & (bcs['cautionEndTime'] - bcs['cautionFirstTime'] >= minCautionInterval) & (bcs['cautionNumber'] > 1))

    caution_Set3 = bcs.loc[caution_Set_Filter]
    caution_Set3_acas = bcs_acas.loc[caution_Set_Filter]

    hist_data_set3 = [caution_Set3['haz_SLoWC_time'] - caution_Set3_acas['tcasRaTime'], \
                      caution_Set3_acas['tcasRaTime'] - caution_Set3['cautionFirstTime'], \
                      caution_Set3_acas['tcasRaTime'] - caution_Set3['hazLmvViolatedTime'], \
                      caution_Set3_acas['tcasRaTime'] - caution_Set3['hazFirstTime']]

    fResults.write("\tMultiple active caution ACAS: " + str(len(caution_Set3.index)) + "  " + str(100 * len(caution_Set3.index)/totalActive) + "% \n")
    mycsv.write(str(len(caution_Set3.index)) + "," + str(100 * len(caution_Set3.index)/totalActive) + ",")
    mycsvh.write("MultipleActiveCautionACAS" + "," + "MultipleActiveCaution% ACAS" + ",")

    mydoc.add_paragraph("Multiple active caution ACAS: " + str(len(caution_Set3.index)) + "  \t" + str(100 * len(caution_Set3.index)/totalActive) + "%")

    #caution_Set_Filter = (((bcs['cautionFirstEndTime'] - bcs['cautionFirstTime'] >= 5) & (bcs['cautionTime'] - bcs['cautionFirstEndTime'] <= 10)) & (bcs['cautionEndTime'] - bcs['cautionFirstTime'] < 20) & (bcs['cautionNumber'] > 1))

    #caution_Set3_1 = bcs.loc[caution_Set_Filter]



    group_labels = ['TCAS RA to WSLoWC', 'Active Caution to TCAS', 'LMV to TCAS', 'LoWC to TCAS']

    df3_1 = pd.concat(hist_data_set1, axis=1, keys=group_labels)
    df3_2 = pd.concat(hist_data_set2, axis=1, keys=group_labels)
    df3_3 = pd.concat(hist_data_set3, axis=1, keys=group_labels)

    df3 = pd.concat([df3_1, df3_2, df3_3], axis=0)


    fResults.write("\tTCAS RA to WSLoWC \tMean: " + str(df3.iloc[:, 0].mean()) + " Stddev: " + str(df3.iloc[:, 0].std()) + "\n")
    fResults.write("\tActive Caution to TCAS. \tMean: " + str(df3.iloc[:, 1].mean()) + " Stddev: " + str(df3.iloc[:, 1].std()) + "\n")
    fResults.write("\tLMV to TCAS \tMean: " + str(df3.iloc[:, 0].mean()) + " Stddev: " + str(df3.iloc[:, 0].std()) + "\n")
    fResults.write("\tLoWC to TCAS. \tMean: " + str(df3.iloc[:, 1].mean()) + " Stddev: " + str(df3.iloc[:, 1].std()) + "\n")

    mycsv.write(str(df3.iloc[:, 0].mean()) + "," + str(df3.iloc[:, 0].std()) + ",")
    mycsvh.write("TCAS RA to WSLoWC Mean" + "," + "TCAS RA to WSLoWC STD" + ",")
    mycsv.write(str(df3.iloc[:, 1].mean()) + "," + str(df3.iloc[:, 1].std()) + ",")
    mycsvh.write("Active Caution to TCAS Mean" + "," + "Active Caution to RA STD" + ",")
    mycsv.write(str(df3.iloc[:, 2].mean()) + "," + str(df3.iloc[:, 2].std()) + ",")
    mycsvh.write("LMV to TCAS Mean" + "," + "LMV to TCAS STD" + ",")
    mycsv.write(str(df3.iloc[:, 3].mean()) + "," + str(df3.iloc[:, 3].std()) + ",")
    mycsvh.write("LoWC to TCAS Mean" + "," + "LoWC to RA STD" + ",")


    # SHOW FIGURE
    df3_plot = [df3['TCAS RA to WSLoWC'], \
                df3['Active Caution to TCAS'], \
                df3['LMV to TCAS'], \
                df3['LoWC to TCAS'] \
                ]


    fig = ff.create_distplot(df3_plot, group_labels, bin_size=[5, 5, 5, 5], show_hist=False)
    fig.update_layout(margin_b=0, margin_l=0, margin_r=0, width=1000)
    fig.update_xaxes(title_text='Time(s)', range=[0, acasTimeRange])
    fig.update_layout(title_text='Active Caution / ACAS: Time (sec)')
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="right",
        x=0.99
    ))

    filename = folderPath + "ActiveCautionACAS_Time_Comparison_Histogram-A.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "ActiveCautionACAS_Time_Comparison_Histogram-A.png"
    fig.write_image(filename)

    mydoc.add_picture(folderPath + "ActiveCautionACAS_Time_Comparison_Histogram-A.png", width=docx.shared.Inches(7.5))


    fig = px.ecdf(df3, group_labels, ecdfmode="reversed", marginal="box")
    fig.update_layout(margin_b= 0, margin_l= 0, margin_r= 0, width= 1000)
    fig.update_xaxes(title_text='Time(s)', range=[0, 60])
    fig.update_yaxes(title_text='CDF')
    fig.update_layout(title_text='Active Caution / ACAS')
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="right",
        x=0.99
    ))

    filename = folderPath + "ActiveCautionACAS_Time_Comparison_Histogram-CDF.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "ActiveCautionACAS_Time_Comparison_Histogram-CDF.png"
    fig.write_image(filename)

    mydoc.add_picture(folderPath + "ActiveCautionACAS_Time_Comparison_Histogram-CDF.png", width=docx.shared.Inches(7.5))




def evaluateActiveCautionTimming(fResults, mydoc, mycsv, mycsvh, df, caption, folderPath):

    totalEncounters = len(df.index)

    # TIME RANGE FOR ACTIVE CAUTION ALERTS

    fResults.write("\n\n")
    fResults.write("Active Caution Timming Evaluation:" + "\n")

    mydoc.add_page_break()
    mydoc.add_heading("Active Caution Resume", 2)


    # STEP 0 ####################################################

    fResults.write("\n")
    fResults.write("Basic active Caution Timming Evaluation:" + "\n")

    # Non-active caution condition force with caution >= 1
    caution_Set_Filter = (~((((df['cautionEndTime'] - df['cautionTime'] < minCautionInterval) & (df['cautionNumber'] == 1)) |
                            ((((df['cautionTime'] - df['cautionFirstEndTime'] > minCautionDiscontinuity) & (df['cautionEndTime'] - df['cautionTime'] < minCautionInterval)) |
                                ((df['cautionTime'] - df['cautionFirstEndTime'] <= minCautionDiscontinuity) & (df['cautionFirstEndTime'] - df['cautionFirstTime'] < minIniCautionInterval) & (df['cautionEndTime'] - df['cautionTime'] < minCautionInterval)) |
                                ((df['cautionTime'] - df['cautionFirstEndTime'] <= minCautionDiscontinuity) & (df['cautionFirstEndTime'] - df['cautionFirstTime'] >= minIniCautionInterval) & (df['cautionEndTime'] - df['cautionFirstTime'] < minCautionInterval))
                            ) & (df['cautionNumber'] > 1))
                            ) & (df['hazFirstTime'] == 0)) & (df['cautionNumber'] >= 1))


    bcs = df.loc[caution_Set_Filter]

    totalActive = len(bcs.index)

    fResults.write("\tTotal active caution: " + str(len(bcs.index)) + "\n")
    mycsv.write(str(len(bcs.index)) + ",")
    mycsvh.write("TotalActiveCaution" + ",")

    mydoc.add_paragraph("Total active caution: " + str(len(bcs.index)) + "\t out of Total: " + str(totalEncounters))


    # Single caution with caution duration >= minCautionInterval
    caution_Set_Filter = ((bcs['cautionNumber'] == 1) & (bcs['cautionEndTime'] - bcs['cautionTime'] >= minCautionInterval))

    caution_Set1 = bcs.loc[caution_Set_Filter]

    fResults.write("\tSingle caution activation >= minCautionInterval: " + str(len(caution_Set1.index)) + "  " + str(100 * len(caution_Set1.index)/totalActive) + "% \n")
    mycsv.write(str(len(caution_Set1.index)) + "," + str(100 * len(caution_Set1.index)/totalActive) + ",")
    mycsvh.write("SingleActiveCaution>minCautionInterval" + "," + "SingleActiveCaution%" + ",")

    mydoc.add_paragraph("Single caution activation >= minCautionInterval: " + str(len(caution_Set1.index)) + "  \t" + str(100 * len(caution_Set1.index)/totalActive) + "%")

    caution_Set_Filter = ((bcs['hazFirstTime'] > 0) & (bcs['cautionNumber'] == 1) & (bcs['cautionEndTime'] - bcs['cautionTime'] < minCautionInterval))

    caution_Set1_1 = bcs.loc[caution_Set_Filter]

    fResults.write("\tSingle caution activation < minCautionInterval & LoWC: " + str(len(caution_Set1_1.index)) + "  " + str(100 * len(caution_Set1_1.index)/totalActive) + "% \n")
    mycsv.write(str(len(caution_Set1_1.index)) + "," + str(100 * len(caution_Set1_1.index)/totalActive) + ",")
    mycsvh.write("SingleActiveCaution<minCautionInterval LoWC" + "," + "SingleActiveCaution<minCautionInterval LoWC%" + ",")

    mydoc.add_paragraph("Single caution activation < minCautionInterval & LoWC: " + str(len(caution_Set1_1.index)) + "  \t" + str(100 * len(caution_Set1_1.index)/totalActive) + "%")


    caution_Set_Filter = (((bcs['cautionFirstEndTime'] - bcs['cautionFirstTime'] < minIniCautionInterval) | (bcs['cautionTime'] - bcs['cautionFirstEndTime'] > minCautionDiscontinuity)) & (bcs['cautionEndTime'] - bcs['cautionTime'] >= minCautionInterval) & (bcs['cautionNumber'] > 1))

    caution_Set2 = bcs.loc[caution_Set_Filter]

    fResults.write("\tMultiple caution, unique activation: " + str(len(caution_Set2.index)) + "  " + str(100 * len(caution_Set2.index)/totalActive) + "% \n")
    mycsv.write(str(len(caution_Set2.index)) + "," + str(100 * len(caution_Set2.index)/totalActive) + ",")
    mycsvh.write("MultipleCautionUniqueAct" + "," + "MultipleCautionUniqueAct%" + ",")

    mydoc.add_paragraph("Multiple caution, unique activation: " + str(len(caution_Set2.index)) + "  \t" + str(100 * len(caution_Set2.index)/totalActive) + "%")

    caution_Set_Filter = (((bcs['cautionFirstEndTime'] - bcs['cautionFirstTime'] < minIniCautionInterval) | (bcs['cautionTime'] - bcs['cautionFirstEndTime'] > minCautionDiscontinuity)) & (bcs['cautionEndTime'] - bcs['cautionTime'] < minCautionInterval) & (bcs['cautionNumber'] > 1))

    caution_Set2_1 = bcs.loc[caution_Set_Filter]

    fResults.write("\tMultiple caution, unique activation and LoWC: " + str(len(caution_Set2_1.index)) + "  " + str(100 * len(caution_Set2_1.index)/totalActive) + "% \n")

    mydoc.add_paragraph("Multiple caution, unique activation and LoWC: " + str(len(caution_Set2_1.index)) + "  \t" + str(100 * len(caution_Set2_1.index)/totalActive) + "%")

    caution_Set_Filter = (((bcs['cautionFirstEndTime'] - bcs['cautionFirstTime'] >= minIniCautionInterval) & (bcs['cautionTime'] - bcs['cautionFirstEndTime'] <= minCautionDiscontinuity)) & (bcs['cautionEndTime'] - bcs['cautionFirstTime'] >= minCautionInterval) & (bcs['cautionNumber'] > 1))

    caution_Set3 = bcs.loc[caution_Set_Filter]

    fResults.write("\tMultiple active caution : " + str(len(caution_Set3.index)) + "  " + str(100 * len(caution_Set3.index)/totalActive) + "% \n")
    mycsv.write(str(len(caution_Set3.index)) + "," + str(100 * len(caution_Set3.index)/totalActive) + ",")
    mycsvh.write("MultipleActiveCaution" + "," + "MultipleActiveCaution%" + ",")

    mydoc.add_paragraph("Multiple active caution : " + str(len(caution_Set3.index)) + "  \t" + str(100 * len(caution_Set3.index)/totalActive) + "%")

    caution_Set_Filter = (((bcs['cautionFirstEndTime'] - bcs['cautionFirstTime'] >= minIniCautionInterval) & (bcs['cautionTime'] - bcs['cautionFirstEndTime'] <= minCautionDiscontinuity)) & (bcs['cautionEndTime'] - bcs['cautionFirstTime'] < minCautionInterval) & (bcs['cautionNumber'] > 1))

    caution_Set3_1 = bcs.loc[caution_Set_Filter]

    fResults.write("\tMultiple spurious caution activation and LoWC: " + str(len(caution_Set3_1.index)) + "  " + str(100 * len(caution_Set3_1.index)/totalActive) + "% \n")
    mycsv.write(str(len(caution_Set3_1.index)) + "," + str(100 * len(caution_Set3_1.index)/totalActive) + ",")
    mycsvh.write("MultipleSpuriousCaution and LoWC" + "," + "MultipleSpuriousCaution and LoWC%" + ",")

    mydoc.add_paragraph("Multiple spurious caution activation and LoWC: " + str(len(caution_Set3_1.index)) + "  \t" + str(100 * len(caution_Set3_1.index)/totalActive) + "%")

    totalActive = len(caution_Set1.index) + len(caution_Set1_1.index) + len(caution_Set2.index) + len(caution_Set2_1.index) + len(caution_Set3.index) + len(caution_Set3_1.index)

    fResults.write("\tTotal active caution: " + str(totalActive) + " " + str(totalActive/totalEncounters) + "\n")
    mycsv.write(str(totalActive) + "," + str(totalActive/totalEncounters) + ",")
    mycsvh.write("TotalActiveCaution" + "," + "TotalActiveCaution%" + ",")

    mydoc.add_paragraph("Total active caution: " + str(totalActive) + " \t" + str(totalActive/totalEncounters))

    caution_Set_Filter = ((caution_Set3['hazFirstTime'] == 0))
    caution_Set3_nolowc = caution_Set3.loc[caution_Set_Filter]

    fResults.write("\n\tIterating complex caution activations with no LoWC:\n")
    for index, row in caution_Set3_nolowc.iterrows():
        fResults.write("\t( Layer: " + str(int(row["layer"])) + " - EncounterId: " + str(int(row["EncounterId"])) + " : " + str(int(row["cautionFirstTime"])) + " -> " + str(int(row["cautionFirstEndTime"])) + " -> " + str(int(row["cautionTime"])) + " -> " + str(int(row["cautionEndTime"])) + " )\n")
    fResults.write("\tDone.\n")

    fResults.write("\n\tIterating spurious caution activations due to LoWC:\n")
    for index, row in pd.concat([caution_Set2_1, caution_Set3_1], axis=0).iterrows():
        fResults.write("\t( Layer: " + str(int(row["layer"])) + " - EncounterId: " + str(int(row["EncounterId"])) + " : " + str(int(row["cautionFirstTime"])) + " -> " + str(int(row["cautionFirstEndTime"])) + " -> " + str(int(row["cautionTime"])) + " -> " + str(int(row["cautionEndTime"])) + " )\n")
    fResults.write("\tDone.\n")


    caution_Set_Filter = ((df['hazFirstTime'] > 0))

    caution_Set = df.loc[caution_Set_Filter]

    fResults.write("\tTotal number of LoWC: " + str(len(caution_Set.index)) + " " + str(len(caution_Set.index)/totalEncounters) + "\n")
    mycsv.write(str(len(caution_Set.index)) + "," + str(len(caution_Set.index)/totalEncounters) + ",")
    mycsvh.write("TotalLoWC" + "," + "TotalLoWC%" + ",")

    mydoc.add_paragraph("Total number of LoWC: " + str(len(caution_Set.index)) + " \t" + str(len(caution_Set.index)/totalEncounters))

    caution_Set_Filter = ((df['cautionNumber'] == 0) & (df['hazFirstTime'] > 0))

    caution_Set = df.loc[caution_Set_Filter]

    fResults.write("\tLoWC without Caution activation: " + str(len(caution_Set.index)) + " " + str(len(caution_Set.index)/totalEncounters) + "\n")
    mycsv.write(str(len(caution_Set.index)) + "," + str(len(caution_Set.index)/totalEncounters) + ",")
    mycsvh.write("TotalLoWCWithoutLoWC" + "," + "TotalLoWCWithoutLoWC%" + ",")

    mydoc.add_paragraph("LoWC without Caution activation: " + str(len(caution_Set.index)) + " \t" + str(len(caution_Set.index)/totalEncounters))

    caution_Set_Filter = ((df['nmacFirstTime'] > 0))

    caution_Set = df.loc[caution_Set_Filter]

    fResults.write("\tNMAC activation: " + str(len(caution_Set.index)) + " " + str(len(caution_Set.index)/totalEncounters) + "\n")
    mycsv.write(str(len(caution_Set.index)) + "," + str(len(caution_Set.index)/totalEncounters) + ",")
    mycsvh.write("NMAC" + "," + "NMAC%" + ",")


    fResults.write("\n\tIterating NMAC:\n")
    for index, row in caution_Set.iterrows():
        fResults.write("\t( Layer: " + str(int(row["layer"])) + " - EncounterId: " + str(int(row["EncounterId"])) + " )\n")
    fResults.write("\tDone.\n")


    caution_Set_Filter = ((df['nmacFirstTime'] > 0) & (df['hazFirstTime'] == 0))

    caution_Set = df.loc[caution_Set_Filter]

    fResults.write("\tNMAC violations without LoWC: " + str(len(caution_Set.index)) + "\n")
    mycsv.write(str(len(caution_Set.index)) + ",")
    mycsvh.write("NMACWithoutLoWC" + ",")

    fResults.write("\n")



    fResults.write("\n\n")
    fResults.write("Basic event Timming Order Evaluation:" + "\n")

    #df.info(verbose=True)

    caution_Set_Filter = ((df['hazFirstTime'] > 0) & (df['hazFirstTime'] > df['cpaTime']))
    xx_caution_Set = df.loc[caution_Set_Filter]
    fResults.write("\tSome CPA occurs before WCV Violation: " + str(len(xx_caution_Set.index)) + "\n")

    caution_Set_Filter = ((df['hazFirstTime'] > 0) & (df['hazFirstTime'] > df['minHDistTime']))
    xx_caution_Set = df.loc[caution_Set_Filter]
    fResults.write("\tSome minHDist occurs before WCV Violation: " + str(len(xx_caution_Set.index)) + "\n")

    fResults.write("\tReverse CPA encounters: \n\t")
    for index, row in xx_caution_Set.iterrows():
        fResults.write("(" + str(int(row["layer"])) + " - " + str(int(row["EncounterId"])) + ")")

    fResults.write("\n")

    caution_Set_Filter = ((df['hazFirstTime'] > 0) & (df['hazFirstTime'] > df['minSlatTime']))
    xx_caution_Set = df.loc[caution_Set_Filter]
    fResults.write("\tSome minSlatTime occurs before WCV Violation: " + str(len(xx_caution_Set.index)) + "\n")

    fResults.write("\tReverse CPA encounters: \n\t")
    for index, row in xx_caution_Set.iterrows():
        fResults.write("(" + str(int(row["layer"])) + " - " + str(int(row["EncounterId"])) + ")")

    fResults.write("\n")



    caution_Set_Filter = ((df['hazFirstTime'] > 0) & (df['hazFirstTime'] > df['haz_SLoWC_time']))
    xx_caution_Set = df.loc[caution_Set_Filter]
    fResults.write("\tSome haz_SLoWC_time occurs before WCV Violation: " + str(len(xx_caution_Set.index)) + "\n")

    caution_Set_Filter = ((df['hazFirstTime'] > 0) & (df['hazFirstTime'] > df['hazTcpaTime']))
    xx_caution_Set = df.loc[caution_Set_Filter]
    fResults.write("\tSome hazTcpaTime occurs before WCV Violation: " + str(len(xx_caution_Set.index)) + "\n")

    caution_Set_Filter = ((df['hazFirstTime'] > 0) & (df['hazFirstTime'] > df['hazTcpaTime']))
    xx_caution_Set = df.loc[caution_Set_Filter]
    fResults.write("\tSome hazTcpaTime occurs before WCV Violation: " + str(len(xx_caution_Set.index)) + "\n")

    caution_Set_Filter = ((df['hazFirstTime'] > 0) & (df['hazFirstTime'] > df['minSlatTime']))
    xx_caution_Set = df.loc[caution_Set_Filter]
    fResults.write("\tSome minSlatTime occurs before WCV Violation: " + str(len(xx_caution_Set.index)) + "\n")



    # STEP 1 ####################################################
    fResults.write("\n\n")
    fResults.write("Active Caution - General Timing Evaluation:" + "\n")

    caution_Set_Filter = ((df['cautionNumber'] == 1) & (((df['cautionEndTime'] - df['cautionTime'] >= minCautionInterval)) | (df['hazFirstTime'] > 0)))

    caution_Set_1 = df.loc[caution_Set_Filter]

    fResults.write("\tSingle caution activation >= minCautionInterval: " + str(len(caution_Set_1.index)) + "\n")
    mycsv.write(str(len(caution_Set_1.index)) + ",")
    mycsvh.write("SingleCautionActivation>minCautionInterval" + ",")

    mydoc.add_page_break()
    mydoc.add_heading("Active Caution Analysis", 2)
    mydoc.add_paragraph("Single caution activation >= minCautionInterval: " + str(len(caution_Set_1.index)))


    hist_data_set1 = [caution_Set_1['cpaTime'] - caution_Set_1['cautionTime'], \
                      caution_Set_1['cautionEndTime'] - caution_Set_1['cautionTime'], \
                      caution_Set_1['cautionTtHaz']]

    hist_dist_data_set1 = [caution_Set_1['cautionRange'], \
                           caution_Set_1['cautionEndRange'], \
                           caution_Set_1['cautionElevation'], \
                           caution_Set_1['cautionEndElevation'], \
                           caution_Set_1['cautionAzimuth'], \
                           caution_Set_1['cautionEndAzimuth']]


    caution_Set_Filter = ((~(((df['cautionTime'] - df['cautionFirstEndTime'] > minCautionDiscontinuity) & (df['cautionEndTime'] - df['cautionTime'] < minCautionInterval)) |
                                ((df['cautionTime'] - df['cautionFirstEndTime'] <= minCautionDiscontinuity) & (df['cautionFirstEndTime'] - df['cautionFirstTime'] < minIniCautionInterval) & (df['cautionEndTime'] - df['cautionTime'] < minCautionInterval)) |
                                ((df['cautionTime'] - df['cautionFirstEndTime'] <= minCautionDiscontinuity) & (df['cautionFirstEndTime'] - df['cautionFirstTime'] >= minIniCautionInterval) & (df['cautionEndTime'] - df['cautionFirstTime'] < minCautionInterval))
                            ) | (df['hazFirstTime'] > 0)) & (df['cautionNumber'] > 1))

    non_sp_caution_Set = df.loc[caution_Set_Filter]


    fResults.write("\tMultiple caution activation >= minCautionInterval with <minIniCautionInterval deactivation: " + str(len(non_sp_caution_Set.index)) + "\n")
    fResults.write("\tTotal active caution: Unique: " + str(len(caution_Set_1.index)) + " Multiple: " + str(len(non_sp_caution_Set.index)) + "\n")
    mycsv.write(str(len(non_sp_caution_Set.index)) + ",")
    mycsvh.write("MultipleCaution>minCautionInterval with <minIniCautionInterval deactivation" + ",")
    mycsv.write(str(len(caution_Set_1.index)) + "," + str(len(non_sp_caution_Set.index)) + ",")
    mycsvh.write("ActiveCautionUnique" + "," + "ActiveCautionMultiple" + ",")

    mydoc.add_paragraph("Multiple caution activation >= minCautionInterval with <minIniCautionInterval deactivation: " + str(len(non_sp_caution_Set.index)))
    mydoc.add_paragraph("Total active caution: Unique: " + str(len(caution_Set_1.index)) + "\t Multiple: " + str(len(non_sp_caution_Set.index)))


    caution_Set_Filter = ((non_sp_caution_Set['cautionTime'] - non_sp_caution_Set['cautionFirstEndTime'] < minCautionDiscontinuity) &
                          (non_sp_caution_Set['cautionFirstEndTime'] - non_sp_caution_Set['cautionFirstTime'] >= minIniCautionInterval))

    caution_Set_2 = non_sp_caution_Set.loc[caution_Set_Filter]

    caution_Set_Filter = (~((non_sp_caution_Set['cautionTime'] - non_sp_caution_Set['cautionFirstEndTime'] < minCautionDiscontinuity) &
                          (non_sp_caution_Set['cautionFirstEndTime'] - non_sp_caution_Set['cautionFirstTime'] >= minIniCautionInterval)))

    caution_Set_3 = non_sp_caution_Set.loc[caution_Set_Filter]

    fResults.write("\tMultiple active caution divided in: Combined: " + str(len(caution_Set_2.index)) + " Single: " + str(len(caution_Set_3.index)) + "\n")
    mycsv.write(str(len(caution_Set_2.index)) + "," + str(len(caution_Set_3.index)) + ",")
    mycsvh.write("MultipleActiveCombined" + "," + "MultipleActiveSingle" + ",")


    hist_data_set2 = [caution_Set_2['cpaTime'] - caution_Set_2['cautionTime'], \
                      caution_Set_2['cautionEndTime'] - caution_Set_2['cautionFirstTime'], \
                      caution_Set_2['cautionFirstTtHaz']]

    hist_dist_data_set2 = [caution_Set_2['cautionFirstRange'], \
                           caution_Set_2['cautionEndRange'], \
                           caution_Set_2['cautionFirstElevation'], \
                           caution_Set_2['cautionEndElevation'], \
                           caution_Set_2['cautionFirstAzimuth'], \
                           caution_Set_2['cautionEndAzimuth']]

    hist_data_set3 = [caution_Set_3['cpaTime'] - caution_Set_3['cautionTime'], \
                      caution_Set_3['cautionEndTime'] - caution_Set_3['cautionTime'], \
                      caution_Set_3['cautionTtHaz']]

    hist_dist_data_set3 = [caution_Set_3['cautionRange'], \
                           caution_Set_3['cautionEndRange'], \
                           caution_Set_3['cautionElevation'], \
                           caution_Set_3['cautionEndElevation'], \
                           caution_Set_3['cautionAzimuth'], \
                           caution_Set_3['cautionEndAzimuth']]

    group_labels = ['Caution to CPA', 'Active Caution Duration', 'Predicted Time LoWC']

    df3_1 = pd.concat(hist_data_set1, axis=1, keys=group_labels)
    df3_2 = pd.concat(hist_data_set2, axis=1, keys=group_labels)
    df3_3 = pd.concat(hist_data_set3, axis=1, keys=group_labels)

    df3 = pd.concat([df3_1, df3_2, df3_3], axis=0)

    # SHOW FIGURE
    df3_plot = [df3['Caution to CPA'], \
                 df3['Active Caution Duration'], \
                 df3['Predicted Time LoWC'], \
                 ]


    fig = ff.create_distplot(df3_plot, group_labels, bin_size=[5, 5, 5], show_hist=False)
    fig.update_layout(margin_b=0, margin_l=0, margin_r=0, width=1000)
    fig.update_xaxes(title_text='Time(s)', range=[0, 120])
    fig.update_layout(title_text='Active Caution Alert: Time (sec)')
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="right",
        x=0.99
    ))

    filename = folderPath + "ActiveCaution_Time_Comparison_Histogram-A.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "ActiveCaution_Time_Comparison_Histogram-A.png"
    fig.write_image(filename)

    mydoc.add_picture(folderPath + "ActiveCaution_Time_Comparison_Histogram-A.png", width=docx.shared.Inches(7.5))


    fig = px.ecdf(df3, group_labels, ecdfmode="reversed", marginal="box")
    fig.update_layout(margin_b= 0, margin_l= 0, margin_r= 0, width= 1000)
    fig.update_xaxes(title_text='Time(s)', range=[0, 120])
    fig.update_yaxes(title_text='CDF')
    fig.update_layout(title_text='Active Caution Alert')
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="right",
        x=0.99
    ))

    filename = folderPath + "ActiveCaution_Time_Comparison_Histogram-CDF.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "ActiveCaution_Time_Comparison_Histogram-CDF.png"
    fig.write_image(filename)

    mydoc.add_picture(folderPath + "ActiveCaution_Time_Comparison_Histogram-CDF.png", width=docx.shared.Inches(7.5))


    caution_Set = pd.concat([caution_Set_1, caution_Set_2, caution_Set_3], axis=0)

    group_labels = ['Range at Caution', 'Range at End of Caution', 'Elevation at Caution',
                    'Elevation at End of Caution', 'Azimuth at Caution', 'Azimuth at End of Caution']

    df3_1 = pd.concat(hist_dist_data_set1, axis=1, keys=group_labels)
    df3_2 = pd.concat(hist_dist_data_set2, axis=1, keys=group_labels)
    df3_3 = pd.concat(hist_dist_data_set3, axis=1, keys=group_labels)

    df3_dist = pd.concat([df3_1, df3_2, df3_3], axis=0)


    # SHOW FIGURE

    df3_dist_plot = [units.m_to_nm(df3_dist['Range at Caution']), \
                 units.m_to_nm(df3_dist['Range at End of Caution']), \
                 ]
    group_labels = ['Range at Caution', 'Range at End of Caution']

    fig = ff.create_distplot(df3_dist_plot, group_labels, bin_size=[0.5, 0.5], histnorm='probability', show_hist= False)
    fig.update_layout(margin_b= 0, margin_l= 0, margin_r= 0, width= 1000)
    fig.update_layout(title_text='Active Caution Alert: Horizontal Range (NM)')
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="right",
        x=0.99
    ))

    filename = folderPath + "ActiveCaution_Range_Comparison_Histogram-A.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "ActiveCaution_Range_Comparison_Histogram-A.png"
    fig.write_image(filename)

    #mydoc.add_picture(folderPath + "ActiveCaution_Range_Comparison_Histogram-A.png", width=docx.shared.Inches(7.5))



    fResults.write("\tAt Caution, Time to CPA. \tMean: " + str(df3.iloc[:, 0].mean()) + " Stddev: " + str(df3.iloc[:, 0].std()) + "\n")
    fResults.write("\tCaution duration. \tMean: " + str(df3.iloc[:, 1].mean()) + " Stddev: " + str(df3.iloc[:, 1].std()) + "\n")
    fResults.write("\tAt Caution, Predicted time LoWC. \tMean: " + str(df3.iloc[:, 2].mean()) + " Stddev: " + str(df3.iloc[:, 2].std()) + "\n")

    fResults.write("\tRange at Caution: " + str(units.m_to_nm(df3_dist.iloc[:, 0].mean())) + " Stddev: " + str(units.m_to_nm(df3_dist.iloc[:, 0].std())) + "\n")
    fResults.write("\tRange at End of Caution: " + str(units.m_to_nm(df3_dist.iloc[:, 1].mean())) + " Stddev: " + str(
        units.m_to_nm(df3_dist.iloc[:, 1].std())) + "\n")


    mycsv.write(str(df3.iloc[:, 0].mean()) + "," + str(df3.iloc[:, 0].std()) + ",")
    mycsvh.write("Caution T CPA Mean" + "," + "Caution T CPA STD" + ",")
    mycsv.write(str(df3.iloc[:, 1].mean()) + "," + str(df3.iloc[:, 1].std()) + ",")
    mycsvh.write("Caution Duration Mean" + "," + "Caution Duration STD" + ",")
    mycsv.write(str(df3.iloc[:, 2].mean()) + "," + str(df3.iloc[:, 2].std()) + ",")
    mycsvh.write("PredTimeWCV Mean" + "," + "PredTimeWCV STD" + ",")
    mycsv.write(str(units.m_to_nm(df3_dist.iloc[:, 0].mean())) + "," + str(units.m_to_nm(df3_dist.iloc[:, 0].std())) + ",")
    mycsvh.write("RangeCaution Mean" + "," + "RangeCaution STD" + ",")
    mycsv.write(str(units.m_to_nm(df3_dist.iloc[:, 1].mean())) + "," + str(units.m_to_nm(df3_dist.iloc[:, 1].std())) + ",")
    mycsvh.write("RangeEndCaution Mean" + "," + "RangeEndCaution STD" + ",")


    hist1 = np.histogram(df3_dist.iloc[:, 0], bins=30)
    dist_1 = scipy.stats.rv_histogram(hist1)
    p1 = dist_1.cdf(units.nm_to_m(8.0))

    hist2 = np.histogram(df3_dist.iloc[:, 1], bins=30)
    dist_2 = scipy.stats.rv_histogram(hist2)
    p2 = dist_2.cdf(units.nm_to_m(8.0))

    fResults.write("\tRange Caution. At caution activation: " + str(p1 * 100) + " At caution end: " + str(p2 * 100) + "\n")
    mycsv.write(str(p1 * 100) + "," + str(p2 * 100) + ",")
    mycsvh.write("RangeCautionBegin" + "," + "RangeEndCaution" + ",")

    hist1 = np.histogram(df3_dist.iloc[:, 2], bins=30)
    dist_1 = scipy.stats.rv_histogram(hist1)
    p1 = dist_1.cdf(15) - dist_1.cdf(-15)

    hist2 = np.histogram(df3_dist.iloc[:, 3], bins=80)
    dist_2 = scipy.stats.rv_histogram(hist2)
    p2 = dist_2.cdf(15) - dist_2.cdf(-15)

    fResults.write("\tElevation Caution. At caution activation: " + str(p1 * 100) + " At caution end: " + str(p2 * 100) + "\n")
    mycsv.write(str(p1 * 100) + "," + str(p2 * 100) + ",")
    mycsvh.write("ElevationCautionBegin" + "," + "ElevationEndCaution" + ",")

    hist1 = np.histogram(df3_dist.iloc[:, 4], bins=180)
    dist_1 = scipy.stats.rv_histogram(hist1)
    p1 = dist_1.cdf(110) - dist_1.cdf(-110)

    hist2 = np.histogram(df3_dist.iloc[:, 5], bins=90)
    dist_2 = scipy.stats.rv_histogram(hist2)
    p2 = dist_2.cdf(110) - dist_2.cdf(-110)

    fResults.write("\tAzimuth Caution. At caution activation: " + str(p1 * 100) + " At caution end: " + str(p2 * 100) + "\n")
    mycsv.write(str(p1 * 100) + "," + str(p2 * 100) + ",")
    mycsvh.write("AzimuthCautionBegin" + "," + "AzimuthEndCaution" + ",")

    inCoverageVector1 = (caution_Set['cautionFirstRange'] <= units.nm_to_m(8.0))
    inCoverageVector2 = (caution_Set['cautionFirstElevation'] <= 15) & (caution_Set['cautionFirstElevation'] >= -15)
    inCoverageVector3 = (caution_Set['cautionFirstAzimuth'] <= 110) & (caution_Set['cautionFirstAzimuth'] >= -110)

    inCoverage1 = np.count_nonzero(inCoverageVector1 & inCoverageVector2 & inCoverageVector3)

    inCoverageVector1 = (caution_Set['cautionEndRange'] <= units.nm_to_m(8.0))
    inCoverageVector2 = (caution_Set['cautionEndElevation'] <= 15) & (caution_Set['cautionEndElevation'] >= -15)
    inCoverageVector3 = (caution_Set['cautionEndAzimuth'] <= 110) & (caution_Set['cautionEndAzimuth'] >= -110)

    inCoverage2 = np.count_nonzero(inCoverageVector1 & inCoverageVector2 & inCoverageVector3)

    fResults.write("\tIn covegare. At caution activation: " + str(
            100 * inCoverage1 / len(caution_Set.index)) + " At caution end: " + str(
            100 * inCoverage2 / len(caution_Set.index)) + "\n")
    fResults.write("\n\n")

    mycsv.write(str(100 * inCoverage1 / len(caution_Set.index)) + "," + str(100 * inCoverage2 / len(caution_Set.index)) + ",")
    mycsvh.write("InCoverageCautionBegin" + "," + "InCoverageEndCaution" + ",")




    caution_Set_Filter = ((caution_Set['sepTime'] > 0) & (caution_Set['sepRelevant'] == True) & (caution_Set['stcaTime'] > 0) & (caution_Set['stcaRelevant'] == True))
    caution_Set = caution_Set.loc[caution_Set_Filter]

    fResults.write("\tTotal number of spurious Caution activations with associated separation/STCA violation: " + str(len(caution_Set.index)) + "\n")

    hist_data = [caution_Set['cautionFirstTime'] - caution_Set['stcaTime'],
                 caution_Set['cautionFirstTime'] - caution_Set['sepTime'],
                 caution_Set['cautionFirstRange'],
                 caution_Set['stcaTimeRange'],
                 caution_Set['sepTimeRange'],
                 abs(caution_Set['cautionFirstDz']),
                 abs(caution_Set['stcaTimeDz']),
                 abs(caution_Set['sepTimeDz']),
                 ]
    group_labels = ['Stca to Caution', 'Sep to Caution', 'Range at Caution activation', 'Range at Stca violation', 'Range at Separation violation', 'Dz at Caution activation', 'Dz at Stca violation', 'Dz at Separation violation']

    df3 = pd.concat(hist_data, axis=1, keys=group_labels)

    fResults.write("\tTime Stca to Caution. Mean: " + str(df3.iloc[:, 0].mean()) + " Stddev: " + str(df3.iloc[:, 0].std()) + "\n")
    fResults.write("\tTime Sep to Caution. Mean: " + str(df3.iloc[:, 1].mean()) + " Stddev: " + str(units.m_to_nm(df3.iloc[:, 1].std())) + "\n")
    fResults.write("\tRange at Caution activation. Mean: " + str(units.m_to_nm(df3.iloc[:, 2].mean())) + " Stddev: " + str(units.m_to_nm(df3.iloc[:, 2].std())) + "\n")
    fResults.write("\tRange at Stca violation. Mean: " + str(units.m_to_nm(df3.iloc[:, 3].mean())) + " Stddev: " + str(units.m_to_nm(df3.iloc[:, 3].std())) + "\n")
    fResults.write("\tRange at Separation violation. Mean: " + str(units.m_to_nm(df3.iloc[:, 4].mean())) + " Stddev: " + str(units.m_to_nm(df3.iloc[:, 4].std())) + "\n")
    fResults.write("\tDz at Caution activation. Mean: " + str(units.m_to_ft(df3.iloc[:, 5].mean())) + " Stddev: " + str(units.m_to_ft(df3.iloc[:, 5].std())) + "\n")
    fResults.write("\tDz at Stca activation. Mean: " + str(units.m_to_ft(df3.iloc[:, 6].mean())) + " Stddev: " + str(units.m_to_ft(df3.iloc[:, 6].std())) + "\n")
    fResults.write("\tDz at Separation violation. Mean: " + str(units.m_to_ft(df3.iloc[:, 7].mean())) + " Stddev: " + str(units.m_to_ft(df3.iloc[:, 7].std())) + "\n")

    mycsv.write(str(df3.iloc[:, 0].mean()) + "," + str(df3.iloc[:, 0].std()) + ",")
    mycsv.write(str(df3.iloc[:, 1].mean()) + "," + str(df3.iloc[:, 1].std()) + ",")
    mycsv.write(str(units.m_to_nm(df3.iloc[:, 2].mean())) + "," + str(units.m_to_nm(df3.iloc[:, 2].std())) + ",")
    mycsv.write(str(units.m_to_nm(df3.iloc[:, 3].mean())) + "," + str(units.m_to_nm(df3.iloc[:, 3].std())) + ",")
    mycsv.write(str(units.m_to_nm(df3.iloc[:, 4].mean())) + "," + str(units.m_to_nm(df3.iloc[:, 4].std())) + ",")
    mycsv.write(str(units.m_to_ft(df3.iloc[:, 5].mean())) + "," + str(units.m_to_ft(df3.iloc[:, 5].std())) + ",")
    mycsv.write(str(units.m_to_ft(df3.iloc[:, 6].mean())) + "," + str(units.m_to_ft(df3.iloc[:, 6].std())) + ",")
    mycsv.write(str(units.m_to_ft(df3.iloc[:, 7].mean())) + "," + str(units.m_to_ft(df3.iloc[:, 7].std())) + ",")
    mycsvh.write("Stca to Caution Mean" + "," + "Stca to Caution STD" + ",")
    mycsvh.write("Sep to Caution Mean" + "," + "Sep to Caution STD" + ",")
    mycsvh.write("Range Caution Activation Mean" + "," + "Range Caution Activation STD" + ",")
    mycsvh.write("Range Stca Activation Mean" + "," + "Range Stca Activation STD" + ",")
    mycsvh.write("Range Sep Activation Mean" + "," + "Range Sep Activation STD" + ",")
    mycsvh.write("Dz Caution Activation Mean" + "," + "Dz Caution Activation STD" + ",")
    mycsvh.write("Dz Stca Activation Mean" + "," + "Dz Stca Activation STD" + ",")
    mycsvh.write("Dz Sep Activation Mean" + "," + "Dz Sep Activation STD" + ",")


    # STEP 2 ####################################################
    fResults.write("\n\n")
    fResults.write("Active Caution with LoWC - General Timing Evaluation:" + "\n")


    caution_Set_Filter = ((df['hazFirstTime'] > 0) & (df['cautionNumber'] == 1) & (((df['cautionEndTime'] - df['cautionTime'] >= minCautionInterval)) | (df['hazFirstTime'] > 0)))

    caution_Set_1 = df.loc[caution_Set_Filter]

    fResults.write("\tSingle caution activation with LoWC: " + str(len(caution_Set_1.index)) + "\n")

    hist_data_set1 = [caution_Set_1['haz_SLoWC_time'] - caution_Set_1['cautionTime'], \
                 caution_Set_1['hazFirstTime'] - caution_Set_1['cautionTime'], \
                 caution_Set_1['cpaTime'] - caution_Set_1['cautionTime'], \
                 caution_Set_1['haz_SLoWC_time'] - caution_Set_1['hazFirstTime'], \
                 caution_Set_1['cpaTime'] - caution_Set_1['hazFirstTime'], \
                 caution_Set_1['hazFirstTcpa'], \
                 caution_Set_1['cautionTtHaz']]


    hist_dist_data_set1 = [caution_Set_1['cautionRange'], \
                           caution_Set_1['cpaRange'], \
                           caution_Set_1['hazFirstTimeRange'], \
                           caution_Set_1['hazSLoWCTimeRange'], \
                           caution_Set_1['cautionElevation'], \
                           caution_Set_1['cpaElevation'], \
                           caution_Set_1['hazFirstTimeElevation'], \
                           caution_Set_1['hazSLoWCTimeElevation'], \
                           caution_Set_1['cautionAzimuth'], \
                           caution_Set_1['cpaAzimuth'], \
                           caution_Set_1['hazFirstTimeAzimuth'], \
                           caution_Set_1['hazSLoWCTimeAzimuth']]


    caution_Set_Filter = ((~(((df['cautionTime'] - df['cautionFirstEndTime'] > minCautionDiscontinuity) & (df['cautionEndTime'] - df['cautionTime'] < minCautionInterval)) |
                                ((df['cautionTime'] - df['cautionFirstEndTime'] <= minCautionDiscontinuity) & (df['cautionFirstEndTime'] - df['cautionFirstTime'] < minIniCautionInterval) & (df['cautionEndTime'] - df['cautionTime'] < minCautionInterval)) |
                                ((df['cautionTime'] - df['cautionFirstEndTime'] <= minCautionDiscontinuity) & (df['cautionFirstEndTime'] - df['cautionFirstTime'] >= minIniCautionInterval) & (df['cautionEndTime'] - df['cautionFirstTime'] < minCautionInterval))
                            ) | (df['hazFirstTime'] > 0)) & (df['cautionNumber'] > 1) & (df['hazFirstTime'] > 0))

    non_sp_caution_Set = df.loc[caution_Set_Filter]

    fResults.write("\tMultiple caution activation with LoWC: " + str(len(non_sp_caution_Set.index)) + "\n")
    fResults.write("\tTotal active caution with LoWC: Unique: " + str(len(caution_Set_1.index)) + " Multiple: " + str(len(non_sp_caution_Set.index)) + "\n")

    mydoc.add_page_break()
    mydoc.add_heading("Active Caution with LoWC Analysis", 2)
    mydoc.add_paragraph("Multiple caution activation with LoWC: " + str(len(non_sp_caution_Set.index)))
    mydoc.add_paragraph("Total active caution with LoWC: Unique: " + str(len(caution_Set_1.index)) + " Multiple: " + str(len(non_sp_caution_Set.index)))

    mycsv.write(str(len(non_sp_caution_Set.index)) + ",")
    mycsvh.write("Multiple Caution Activation with LoWC" + ",")
    mycsv.write(str(len(caution_Set_1.index)) + "," + str(len(non_sp_caution_Set.index)) + ",")
    mycsvh.write("Caution with LoWC: Unique" + "," + "Caution with LoWC: Multiple" + ",")


    caution_Set_Filter = ((non_sp_caution_Set['cautionTime'] - non_sp_caution_Set['cautionFirstEndTime'] < minCautionDiscontinuity) &
                          (non_sp_caution_Set['cautionFirstEndTime'] - non_sp_caution_Set['cautionFirstTime'] >= minIniCautionInterval))

    caution_Set_2 = non_sp_caution_Set.loc[caution_Set_Filter]

    caution_Set_Filter = (~((non_sp_caution_Set['cautionTime'] - non_sp_caution_Set['cautionFirstEndTime'] < minCautionDiscontinuity) &
                          (non_sp_caution_Set['cautionFirstEndTime'] - non_sp_caution_Set['cautionFirstTime'] >= minIniCautionInterval)))

    caution_Set_3 = non_sp_caution_Set.loc[caution_Set_Filter]

    fResults.write("\tMultiple active caution divided in: Combined: " + str(len(caution_Set_2.index)) + " Single: " + str(len(caution_Set_3.index)) + "\n")
    mycsv.write(str(len(caution_Set_2.index)) + "," + str(len(caution_Set_3.index)) + ",")
    mycsvh.write("Multiple active with LoWC: Combined" + "," + "Multiple active with LoWC: Single" + ",")


    hist_data_set2 = [caution_Set_2['haz_SLoWC_time'] - caution_Set_2['cautionFirstTime'], \
                 caution_Set_2['hazFirstTime'] - caution_Set_2['cautionFirstTime'], \
                 caution_Set_2['cpaTime'] - caution_Set_2['cautionFirstTime'], \
                 caution_Set_2['haz_SLoWC_time'] - caution_Set_2['hazFirstTime'], \
                 caution_Set_2['cpaTime'] - caution_Set_2['hazFirstTime'], \
                 caution_Set_2['hazFirstTcpa'], \
                 caution_Set_2['cautionFirstTtHaz']]


    hist_dist_data_set2 = [caution_Set_2['cautionFirstRange'], \
                           caution_Set_2['cpaRange'], \
                           caution_Set_2['hazFirstTimeRange'], \
                           caution_Set_2['hazSLoWCTimeRange'], \
                           caution_Set_2['cautionFirstElevation'], \
                           caution_Set_2['cpaElevation'], \
                           caution_Set_2['hazFirstTimeElevation'], \
                           caution_Set_2['hazSLoWCTimeElevation'], \
                           caution_Set_2['cautionFirstAzimuth'], \
                           caution_Set_2['cpaAzimuth'], \
                           caution_Set_2['hazFirstTimeAzimuth'], \
                           caution_Set_2['hazSLoWCTimeAzimuth']]

    hist_data_set3 = [caution_Set_3['haz_SLoWC_time'] - caution_Set_3['cautionTime'], \
                 caution_Set_3['hazFirstTime'] - caution_Set_3['cautionTime'], \
                 caution_Set_3['cpaTime'] - caution_Set_3['cautionTime'], \
                 caution_Set_3['haz_SLoWC_time'] - caution_Set_3['hazFirstTime'], \
                 caution_Set_3['cpaTime'] - caution_Set_3['hazFirstTime'], \
                 caution_Set_3['hazFirstTcpa'], \
                 caution_Set_3['cautionTtHaz']]


    hist_dist_data_set3 = [caution_Set_3['cautionRange'], \
                           caution_Set_3['cpaRange'], \
                           caution_Set_3['hazFirstTimeRange'], \
                           caution_Set_3['hazSLoWCTimeRange'], \
                           caution_Set_3['cautionElevation'], \
                           caution_Set_3['cpaElevation'], \
                           caution_Set_3['hazFirstTimeElevation'], \
                           caution_Set_3['hazSLoWCTimeElevation'], \
                           caution_Set_3['cautionAzimuth'], \
                           caution_Set_3['cpaAzimuth'], \
                           caution_Set_3['hazFirstTimeAzimuth'], \
                           caution_Set_3['hazSLoWCTimeAzimuth']]


    group_labels = ['Caution to WSLOWC', 'Caution to LoWC', 'Caution to CPA', 'LoWC to WSLOWC', 'LoWC to CPA', 'Predicted time to CPA', 'Predicted time to LoWC']

    df3_1 = pd.concat(hist_data_set1, axis=1, keys=group_labels)
    df3_2 = pd.concat(hist_data_set2, axis=1, keys=group_labels)
    df3_3 = pd.concat(hist_data_set3, axis=1, keys=group_labels)

    df3 = pd.concat([df3_1, df3_2, df3_3], axis=0)
    caution_Set = pd.concat([caution_Set_1, caution_Set_2, caution_Set_3], axis=0)


    # Time check for CPA
    caution_Set_Filter = ((caution_Set['cpaTime'] - caution_Set['hazFirstTime'] < 0))
    xx_caution_Set = caution_Set.loc[caution_Set_Filter]
    fResults.write("\tSome incorrectly ordered LoWC occurs: " + str(len(xx_caution_Set.index)) + "\n")

    fResults.write("\tReverse CPA / LoWC encounters: \n\t")
    for index, row in xx_caution_Set.iterrows():
        fResults.write("(" + str(int(row["layer"])) + " - " + str(int(row["EncounterId"])) + "  LoWC: " +  str(row["hazFirstTime"]) + " CPA: " + str(row["cpaTime"]) + " )"  + "\n")

    fResults.write("\n")

    # Time check for CPA
    caution_Set_Filter = ((caution_Set['hazFirstTime'] - caution_Set['cautionTime'] < 0))
    xx_caution_Set = caution_Set.loc[caution_Set_Filter]
    fResults.write("\tSome incorrectly ordered caution - LoWC occurs: " + str(len(xx_caution_Set.index)) + "\n")

    fResults.write("\tReverse caution / LoWC encounters: \n\t")
    for index, row in xx_caution_Set.iterrows():
        fResults.write("(" + str(int(row["layer"])) + " - " + str(int(row["EncounterId"])) + "  caution: " +  str(row["cautionTime"]) + " LoWC: " + str(row["hazFirstTime"]) + " )"  + "\n")

    fResults.write("\n")


    
    group_labels = ['Range at Caution', 'Range at CPA', 'Range at LoWC', 'Range at WSLOWC', 'Elevation at Caution', 'Elevation at CPA', 'Elevation at LoWC', 'Elevation at WSLOWC', 'Azimuth at Caution', 'Azimuth at CPA', 'Azimuth at LoWC', 'Azimuth at WSLOWC']


    df3_1 = pd.concat(hist_dist_data_set1, axis=1, keys=group_labels)
    df3_2 = pd.concat(hist_dist_data_set2, axis=1, keys=group_labels)
    df3_3 = pd.concat(hist_dist_data_set3, axis=1, keys=group_labels)

    df3_dist = pd.concat([df3_1, df3_2, df3_3], axis=0)
    caution_Set = pd.concat([caution_Set_1, caution_Set_2, caution_Set_3], axis=0)


    fResults.write("\tAt Caution, Time to CPA. \tMean: " + str(df3.iloc[:, 0].mean()) + " Stddev: " + str(df3.iloc[:, 0].std()) + "\n")
    fResults.write("\tCaution to LoWC. \tMean: " + str(df3.iloc[:, 1].mean()) + " Stddev: " + str(df3.iloc[:, 1].std()) + "\n")
    fResults.write("\tCaution duration. \tMean: " + str(df3.iloc[:, 2].mean()) + " Stddev: " + str(df3.iloc[:, 2].std()) + "\n")
    fResults.write("\tLoWC to WSLOWC. \tMean: " + str(df3.iloc[:, 3].mean()) + " Stddev: " + str(df3.iloc[:, 3].std()) + "\n")
    fResults.write("\tLoWC to CPA. \tMean: " + str(df3.iloc[:, 4].mean()) + " Stddev: " + str(df3.iloc[:, 4].std()) + "\n")
    fResults.write("\tAt Caution, Predicted time to LoWC. \tMean: " + str(df3.iloc[:, 5].mean()) + " Stddev: " + str(df3.iloc[:, 5].std()) + "\n")

    mycsv.write(str(df3.iloc[:, 0].mean()) + "," + str(df3.iloc[:, 0].std()) + ",")
    mycsv.write(str(df3.iloc[:, 1].mean()) + "," + str(df3.iloc[:, 1].std()) + ",")
    mycsv.write(str(df3.iloc[:, 2].mean()) + "," + str(df3.iloc[:, 2].std()) + ",")
    mycsv.write(str(df3.iloc[:, 3].mean()) + "," + str(df3.iloc[:, 3].std()) + ",")
    mycsv.write(str(df3.iloc[:, 4].mean()) + "," + str(df3.iloc[:, 4].std()) + ",")
    mycsv.write(str(df3.iloc[:, 5].mean()) + "," + str(df3.iloc[:, 5].std()) + ",")
    mycsvh.write("Caution: T to CPA: Mean" + "," + "Caution: T to CPA: Std" + ",")
    mycsvh.write("Caution to LoWC: Mean" + "," + "Caution to LoWC: Std" + ",")
    mycsvh.write("Caution duration: Mean" + "," + "Caution duration: Std" + ",")
    mycsvh.write("LoWC to WSLOWC: Mean" + "," + "LoWC to WSLOWC: Std" + ",")
    mycsvh.write("LoWC to CPA: Mean" + "," + "LoWC to CPA: Std" + ",")
    mycsvh.write("Caution: Pt LoWC: Mean" + "," + "Caution: Pt LoWC: Std" + ",")


    # SHOW FIGURE
    df3_plot = [df3['LoWC to CPA'], \
                df3['LoWC to WSLOWC'], \
                df3['Caution to LoWC'], \
                df3['Predicted time to LoWC'], \
                ]

    group_labels = ['LoWC to CPA', 'LoWC to WSLOWC', 'Caution to LoWC', 'Predicted time to LoWC']


    fig = ff.create_distplot(df3_plot, group_labels, bin_size=[5, 5, 5, 5], show_hist=False)
    fig.update_layout(margin_b=0, margin_l=0, margin_r=0, width=1000)
    fig.update_xaxes(title_text='Time(s)', range=[0, 120])
    fig.update_layout(title_text='LoWC - Active Caution Alert: Time (sec)')
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="right",
        x=0.99
    ))

    filename = folderPath + "LoWC-Caution_Time_Comparison_Histogram-A.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "LoWC-Caution_Time_Comparison_Histogram-A.png"
    fig.write_image(filename)

    mydoc.add_picture(folderPath + "LoWC-Caution_Time_Comparison_Histogram-A.png", width=docx.shared.Inches(7.5))


    df3_plot = pd.concat(df3_plot, axis=1, keys=group_labels)

    fig = px.ecdf(df3_plot, group_labels, ecdfmode="reversed", marginal="box")
    fig.update_layout(margin_b= 0, margin_l= 0, margin_r= 0, width= 1000)
    fig.update_xaxes(title_text='Time(s)', range=[0, 120])
    fig.update_yaxes(title_text='CDF')
    fig.update_layout(title_text='LoWC - Active Caution Alert')
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="right",
        x=0.99
    ))

    filename = folderPath + "LoWC-Caution_Time_Comparison_Histogram-CDF.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "LoWC-Caution_Time_Comparison_Histogram-CDF.png"
    fig.write_image(filename)

    mydoc.add_picture(folderPath + "LoWC-Caution_Time_Comparison_Histogram-CDF.png", width=docx.shared.Inches(7.5))


    df3_dist_plot = [units.m_to_nm(df3_dist['Range at Caution']), \
                     units.m_to_nm(df3_dist['Range at LoWC']), \
                     units.m_to_nm(df3_dist['Range at WSLOWC']), \
                     units.m_to_nm(df3_dist['Range at CPA'])]


    group_labels = ['Range at Caution', 'Range at LoWC', 'Range at WSLoWC', 'Range at CPA']

    fig = ff.create_distplot(df3_dist_plot, group_labels, bin_size=[0.5, 0.5, 0.5, 0.5], histnorm='probability', show_hist= False)
    fig.update_layout(margin_b= 0, margin_l= 0, margin_r= 0, width= 1000)
    fig.update_layout(title_text='LoWC - Caution Alert: Horizontal Range (NM)')
    fig.update_xaxes(title_text='Range (NM)', range=[0, 4])
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="right",
        x=0.99
    ))

    filename = folderPath + "LoWC-Caution_Range_Comparison_Histogram-A.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "LoWC-Caution_Range_Comparison_Histogram-A.png"
    fig.write_image(filename)

    mydoc.add_picture(folderPath + "LoWC-Caution_Range_Comparison_Histogram-A.png", width=docx.shared.Inches(7.5))


    df3_plot = pd.concat(df3_dist_plot, axis=1, keys=group_labels)

    fig = px.ecdf(df3_plot, group_labels, ecdfmode="reversed", marginal="box")
    fig.update_layout(margin_b= 0, margin_l= 0, margin_r= 0, width= 1000)
    fig.update_xaxes(title_text='Range (NM)', range=[0, 4])
    fig.update_yaxes(title_text='CDF')
    fig.update_layout(title_text='LoWC - Caution Alert: Horizontal Range (NM)')
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="right",
        x=0.99
    ))

    filename = folderPath + "LoWC-Caution_Range_Comparison_Histogram-CDF.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "LoWC-Caution_Range_Comparison_Histogram-CDF.png"
    fig.write_image(filename)

    mydoc.add_picture(folderPath + "LoWC-Caution_Range_Comparison_Histogram-CDF.png", width=docx.shared.Inches(7.5))


    ############
    df3_dist_plot = [units.m_to_nm(df3_dist['Range at Caution'])]


    group_labels = ['Range at Caution']

    fig = ff.create_distplot(df3_dist_plot, group_labels, bin_size=[0.5], histnorm='probability', show_hist= False)
    fig.update_layout(margin_b= 0, margin_l= 0, margin_r= 0, width= 1000)
    fig.update_layout(title_text='LoWC - Caution Alert: Horizontal Range (NM)')
    fig.update_xaxes(title_text='Range (NM)', range=[0, 15])
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="right",
        x=0.99
    ))

    filename = folderPath + "LoWC-Caution_OnlyRange_Comparison_Histogram-A.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "LoWC-Caution_OnlyRange_Comparison_Histogram-A.png"
    fig.write_image(filename)

    mydoc.add_picture(folderPath + "LoWC-Caution_OnlyRange_Comparison_Histogram-A.png", width=docx.shared.Inches(7.5))


    df3_plot = pd.concat(df3_dist_plot, axis=1, keys=group_labels)

    fig = px.ecdf(df3_plot, group_labels, ecdfmode="reversed", marginal="box")
    fig.update_layout(margin_b= 0, margin_l= 0, margin_r= 0, width= 1000)
    fig.update_xaxes(title_text='Range (NM)', range=[0, 15])
    fig.update_yaxes(title_text='CDF')
    fig.update_layout(title_text='LoWC - Caution Alert: Horizontal Range (NM)')
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="right",
        x=0.99
    ))

    filename = folderPath + "LoWC-Caution_OnlyRange_Comparison_Histogram-CDF.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "LoWC-Caution_OnlyRange_Comparison_Histogram-CDF.png"
    fig.write_image(filename)

    mydoc.add_picture(folderPath + "LoWC-Caution_OnlyRange_Comparison_Histogram-CDF.png", width=docx.shared.Inches(7.5))



    ################
    hist_data = [abs(caution_Set['cautionFirstBeta'] - caution_Set['cautionFirstEndBeta']), \
                 abs(caution_Set['cautionBeta'] - caution_Set['cautionEndBeta']), \
                 abs(caution_Set['cautionFirstDz'] - caution_Set['cautionFirstEndDz'])/10, \
                 abs(caution_Set['cautionDz'] - caution_Set['cautionEndDz'])/10 \
                 ]
    group_labels = ['First Delta Beta', 'Delta Beta', 'First Delta Dz', 'Delta Dz']
    df3 = pd.concat(hist_data, axis=1, keys=group_labels)

    fig = ff.create_distplot(hist_data, group_labels, bin_size=[0.5, 0.5, 5, 5], histnorm='probability', show_hist= False)
    fig.update_layout(margin_b= 0, margin_l= 0, margin_r= 0, width= 1000)
    fig.update_xaxes(title_text='Beta variation (deg), Dz variation (100ft)') # , range=[0, 15]
    fig.update_layout(title_text='Spurious Caution Alert: Beta/ Dz variation')
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="right",
        x=0.99
    ))

    filename = folderPath + "LoWC-Caution_Delta_Comparison_Histogram-A.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "LoWC-Caution_Delta_Comparison_Histogram-A.png"
    fig.write_image(filename)

    mydoc.add_picture(folderPath + "LoWC-Caution_Delta_Comparison_Histogram-A.png", width=docx.shared.Inches(7.5))


    fig = px.ecdf(df3, group_labels, marginal="box")
    fig.update_layout(margin_b= 0, margin_l= 0, margin_r= 0, width= 1000)
    fig.update_xaxes(title_text='Beta variation (deg), Dz variation (10ft)') #, range=[0, 120]
    fig.update_yaxes(title_text='CDF')
    fig.update_layout(title_text='Spurious Caution Alert: Beta / Dz variation')
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="right",
        x=0.99
    ))

    filename = folderPath + "LoWC-Caution_Delta_Comparison_Histogram-CDF.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "LoWC-Caution_Delta_Comparison_Histogram-CDF.png"
    fig.write_image(filename)

    mydoc.add_picture(folderPath + "LoWC-Caution_Delta_Comparison_Histogram-CDF.png", width=docx.shared.Inches(7.5))



    ################
    fResults.write("\tRange at Caution: " + str(units.m_to_nm(df3_dist.iloc[:, 0].mean())) + " Stddev: " + str(
        units.m_to_nm(df3_dist.iloc[:, 0].std())) + "\n")
    fResults.write("\tRange at LoWC: " + str(units.m_to_nm(df3_dist.iloc[:, 2].mean())) + " Stddev: " + str(
        units.m_to_nm(df3_dist.iloc[:, 2].std())) + "\n")

    mycsv.write(str(units.m_to_nm(df3_dist.iloc[:, 0].mean())) + "," + str(units.m_to_nm(df3_dist.iloc[:, 0].std())) + ",")
    mycsvh.write("RangeCaution Mean:" + "," + "RangeCaution STD:" + ",")
    mycsv.write(str(units.m_to_nm(df3_dist.iloc[:, 2].mean())) + "," + str(units.m_to_nm(df3_dist.iloc[:, 2].std())) + ",")
    mycsvh.write("RangeLoWC Mean:" + "," + "RangeLoWC STD:" + ",")



    hist1 = np.histogram(df3_dist.iloc[:, 0], bins=30)
    dist_1 = scipy.stats.rv_histogram(hist1)
    p1 = dist_1.cdf(units.nm_to_m(8.0))

    hist2 = np.histogram(df3_dist.iloc[:, 1], bins=30)
    dist_2 = scipy.stats.rv_histogram(hist2)
    p2 = dist_2.cdf(units.nm_to_m(8.0))

    hist3 = np.histogram(df3_dist.iloc[:, 2], bins=30)
    dist_3 = scipy.stats.rv_histogram(hist3)
    p3 = dist_3.cdf(units.nm_to_m(8.0))

    hist4 = np.histogram(df3_dist.iloc[:, 3], bins=30)
    dist_4 = scipy.stats.rv_histogram(hist4)
    p4 = dist_4.cdf(units.nm_to_m(8.0))

    fResults.write("\tRange Caution. At caution activation: " + str(p1 * 100) + " At LoWC: " + str(p2 * 100) + " At WSLOWC: " + str(p3 * 100) + " At CPA: " + str(p4 * 100) + "\n")

    mycsv.write(str(p1 * 100) + "," + str(p2 * 100) + "," + str(p3 * 100) + "," + str(p4 * 100) + ",")
    mycsvh.write("RangeCautionActivation" + "," + "RangeAtLoWC" + "," + "RangeAtWSLoWC" + "," + "RangeAtCPA" + ",")


    hist1 = np.histogram(df3_dist.iloc[:, 4], bins=30)
    dist_1 = scipy.stats.rv_histogram(hist1)
    p1 = dist_1.cdf(15) - dist_1.cdf(-15)

    hist2 = np.histogram(df3_dist.iloc[:, 5], bins=80)
    dist_2 = scipy.stats.rv_histogram(hist2)
    p2 = dist_2.cdf(15) - dist_2.cdf(-15)

    hist3 = np.histogram(df3_dist.iloc[:, 6], bins=30)
    dist_3 = scipy.stats.rv_histogram(hist3)
    p3 = dist_3.cdf(15) - dist_3.cdf(-15)

    hist4 = np.histogram(df3_dist.iloc[:, 7], bins=80)
    dist_4 = scipy.stats.rv_histogram(hist4)
    p4 = dist_4.cdf(15) - dist_4.cdf(-15)

    fResults.write("\tElevation Caution. At caution activation: " + str(p1 * 100) + " At LoWC: " + str(p2 * 100) + " At WSLoWC: " + str(p3 * 100) + " At CPA: " + str(p4 * 100) + "\n")

    mycsv.write(str(p1 * 100) + "," + str(p2 * 100) + "," + str(p3 * 100) + "," + str(p4 * 100) + ",")
    mycsvh.write("ElevationCautionActivation" + "," + "ElevationAtLoWC" + "," + "ElevationAtWSLoWC" + "," + "ElevationAtCPA" + ",")


    hist1 = np.histogram(df3_dist.iloc[:, 8], bins=180)
    dist_1 = scipy.stats.rv_histogram(hist1)
    p1 = dist_1.cdf(110) - dist_1.cdf(-110)

    hist2 = np.histogram(df3_dist.iloc[:, 9], bins=90)
    dist_2 = scipy.stats.rv_histogram(hist2)
    p2 = dist_2.cdf(110) - dist_2.cdf(-110)

    hist3 = np.histogram(df3_dist.iloc[:, 10], bins=180)
    dist_3 = scipy.stats.rv_histogram(hist3)
    p3 = dist_3.cdf(110) - dist_3.cdf(-110)

    hist4 = np.histogram(df3_dist.iloc[:, 11], bins=90)
    dist_4 = scipy.stats.rv_histogram(hist4)
    p4 = dist_4.cdf(110) - dist_4.cdf(-110)

    fResults.write("\tAzimuth Caution. At caution activation: " + str(p1 * 100) + " At LoWC: " + str(p2 * 100) + " At WSLoWC: " + str(p3 * 100) + " At CPA: " + str(p4 * 100) + "\n")

    mycsv.write(str(p1 * 100) + "," + str(p2 * 100) + "," + str(p3 * 100) + "," + str(p4 * 100) + ",")
    mycsvh.write("AzimuthCautionActivation" + "," + "AzimuthAtLOWC" + "," + "AzimuthAtWSLoWC" + "," + "AzimuthAtCPA" + ",")

    inCoverageVector1 = (df3_dist.iloc[:, 0] <= units.nm_to_m(8.0))
    inCoverageVector2 = (df3_dist.iloc[:, 4] <= 15) & (df3_dist.iloc[:, 4] >= -15)
    inCoverageVector3 = (df3_dist.iloc[:, 8] <= 110) & (df3_dist.iloc[:, 8] >= -110)

    inCoverage1 = np.count_nonzero(inCoverageVector1 & inCoverageVector2 & inCoverageVector3)

    inCoverageVector1 = (df3_dist.iloc[:, 1] <= units.nm_to_m(8.0))
    inCoverageVector2 = (df3_dist.iloc[:, 5] <= 15) & (df3_dist.iloc[:, 5] >= -15)
    inCoverageVector3 = (df3_dist.iloc[:, 9] <= 110) & (df3_dist.iloc[:, 9] >= -110)

    inCoverage2 = np.count_nonzero(inCoverageVector1 & inCoverageVector2 & inCoverageVector3)

    inCoverageVector1 = (df3_dist.iloc[:, 2] <= units.nm_to_m(8.0))
    inCoverageVector2 = (df3_dist.iloc[:, 6] <= 15) & (df3_dist.iloc[:, 6] >= -15)
    inCoverageVector3 = (df3_dist.iloc[:, 10] <= 110) & (df3_dist.iloc[:, 10] >= -110)

    inCoverage3 = np.count_nonzero(inCoverageVector1 & inCoverageVector2 & inCoverageVector3)

    inCoverageVector1 = (df3_dist.iloc[:, 3] <= units.nm_to_m(8.0))
    inCoverageVector2 = (df3_dist.iloc[:, 7] <= 15) & (df3_dist.iloc[:, 7] >= -15)
    inCoverageVector3 = (df3_dist.iloc[:, 11] <= 110) & (df3_dist.iloc[:, 11] >= -110)

    inCoverage4 = np.count_nonzero(inCoverageVector1 & inCoverageVector2 & inCoverageVector3)

    fResults.write(
        "\tIn covegare. At caution activation: " + str(100 * inCoverage1 / len(caution_Set.index)) + " At LoWC: " + str(
            100 * inCoverage2 / len(caution_Set.index)) + " At WSLoWC: " + str(
            100 * inCoverage3 / len(caution_Set.index)) + " At CPA: " + str(
            100 * inCoverage4 / len(caution_Set.index)) + "\n")
    fResults.write("\n\n")

    mycsv.write(str(100 * inCoverage1 / len(caution_Set.index)) + "," + str(100 * inCoverage2 / len(caution_Set.index)) + "," + str(100 * inCoverage3 / len(caution_Set.index)) + "," + str(100 * inCoverage4 / len(caution_Set.index)) + ",")
    mycsvh.write("InCoverageCautionActivation" + "," + "InCoverageAtLoWC" + "," + "InCoverageAtWSLoWC" + "," + "InCoverageAtCPA" + ",")


    caution_Set_Filter = ((caution_Set['sepTime'] > 0) & (caution_Set['sepRelevant'] == True))
    caution_Set = caution_Set.loc[caution_Set_Filter]

    fResults.write("\tTotal number of spurious Caution activations with associated separation violation: " + str(
        len(caution_Set.index)) + "\n")

    mycsv.write(str(len(caution_Set.index)) + ",")
    mycsvh.write("SpuriousCautionwithSep" + ",")

    hist_data = [caution_Set['cautionFirstRange'],
                 caution_Set['sepTimeRange'],
                 abs(caution_Set['cautionFirstDz']),
                 abs(caution_Set['sepTimeDz']),
                 ]
    group_labels = ['Range at Caution activation', 'Range at Separation violation', 'Dz at Caution activation', 'Dz at Separation violation']

    df3 = pd.concat(hist_data, axis=1, keys=group_labels)

    fResults.write("\tRange at Caution activation. Mean: " + str(units.m_to_nm(df3.iloc[:, 0].mean())) + " Stddev: " + str(units.m_to_nm(df3.iloc[:, 0].std())) + "\n")
    fResults.write("\tRange at Separation violation. Mean: " + str(units.m_to_nm(df3.iloc[:, 1].mean())) + " Stddev: " + str(units.m_to_nm(df3.iloc[:, 1].std())) + "\n")
    fResults.write("\tDz at Caution activation. Mean: " + str(units.m_to_ft(df3.iloc[:, 2].mean())) + " Stddev: " + str(units.m_to_ft(df3.iloc[:, 2].std())) + "\n")
    fResults.write("\tDz at Separation violation. Mean: " + str(units.m_to_ft(df3.iloc[:, 3].mean())) + " Stddev: " + str(units.m_to_ft(df3.iloc[:, 3].std())) + "\n")

    mycsv.write(str(units.m_to_nm(df3.iloc[:, 0].mean())) + "," + str(units.m_to_nm(df3.iloc[:, 0].std())) + ",")
    mycsv.write(str(units.m_to_nm(df3.iloc[:, 1].mean())) + "," + str(units.m_to_nm(df3.iloc[:, 1].std())) + ",")
    mycsv.write(str(units.m_to_ft(df3.iloc[:, 2].mean())) + "," + str(units.m_to_ft(df3.iloc[:, 2].std())) + ",")
    mycsv.write(str(units.m_to_ft(df3.iloc[:, 3].mean())) + "," + str(units.m_to_ft(df3.iloc[:, 3].std())) + ",")
    mycsvh.write("Range at Caution: Mean" + "," + "Range at Caution: Std" + ",")
    mycsvh.write("Range at Sep: Mean" + "," + "Range at Sep: Std" + ",")
    mycsvh.write("Dz at Caution: Mean" + "," + "Dz at Caution: Std" + ",")
    mycsvh.write("Dz at Sep: Mean" + "," + "Dz at Sep: Std" + ",")


    #
    # Check for reversed coverage encounters
    caution_Set_Filter = ((caution_Set['cautionAzimuth'] <= 110)&(caution_Set['cautionAzimuth'] >= -110)&
                          ((caution_Set['hazFirstTimeAzimuth'] > 110)|(caution_Set['hazFirstTimeAzimuth'] < -110)))
    xx_caution_Set = caution_Set.loc[caution_Set_Filter]
    fResults.write("\tSome reversed azimuth Caution-LoWC occurs: " + str(len(xx_caution_Set.index)) + "\n")

    fResults.write("\tReverse azimuth Caution-LoWC encounters: \n\t")
    for index, row in xx_caution_Set.iterrows():
        fResults.write("(" + str(int(row["layer"])) + " - " + str(int(row["EncounterId"])) + " Caution: " + str(row["cautionTime"]) + "  LoWC: " +  str(row["hazFirstTime"]) + " )" + "\n")

    fResults.write("\n")



    # Caution activations with LoWC and LMV
    # We reuse the previous sets

    # LoWCV in which caution is activated and in which there a single caution activation or multiple activations

    fResults.write("\n\n")
    fResults.write("Active Caution - LMV - Timming Evaluation:" + "\n")


    caution_Set_Filter = (caution_Set_2['hazLmvViolatedTime'] <= 0)
    caution_Set = caution_Set_2.loc[caution_Set_Filter]

    fResults.write("\tIterating complex non-LMV LoWC: cautionTime -> hazFirstTime:\n")
    for index, row in caution_Set.iterrows():
        fResults.write("\t( Layer: " + str(int(row["layer"])) + " - EncounterId: " + str(int(row["EncounterId"])) + " : " + str(int(row["cautionFirstTime"])) + " -> " + str(int(row["hazFirstTime"])) + " )\n")
    fResults.write("\tDone.\n")


    cautionSetOrig_1 = caution_Set_1.copy(deep=True)
    cautionSetOrig_2 = caution_Set_2.copy(deep=True)
    cautionSetOrig_3 = caution_Set_3.copy(deep=True)


    caution_Set_Filter = ((caution_Set_1['hazLmvViolatedTime'] > 0))
    caution_Set_1 = caution_Set_1.loc[caution_Set_Filter]

    caution_Set_Filter = ((caution_Set_2['hazLmvViolatedTime'] > 0))
    caution_Set_2 = caution_Set_2.loc[caution_Set_Filter]

    caution_Set_Filter = ((caution_Set_3['hazLmvViolatedTime'] > 0))
    caution_Set_3 = caution_Set_3.loc[caution_Set_Filter]

    fResults.write(
        "\tTotal number of LoWC with LMV: Single interval: " + str(len(caution_Set_1.index)) + " Multiple caution divided in: Combined: " + str(len(caution_Set_2.index)) + " Single: " + str(len(caution_Set_3.index)) + "\n")


    mydoc.add_page_break()
    mydoc.add_heading("Active Caution with LMV Analysis", 2)
    mydoc.add_paragraph("Total number of LoWC with LMV: Single interval: " + str(len(caution_Set_1.index)) + " Multiple caution divided in: Combined: " + str(len(caution_Set_2.index)) + " Single: " + str(len(caution_Set_3.index)))



    hist_data_set1 = [\
        caution_Set_1['hazLmvViolatedTime'] - caution_Set_1['cautionTime'], \
        caution_Set_1['haz_SLoWC_time'] - caution_Set_1['hazLmvViolatedTime'], \
        caution_Set_1['haz_SLoWC_time'] - caution_Set_1['hazFirstTime']]

    hist_data_set2 = [\
        caution_Set_2['hazLmvViolatedTime'] - caution_Set_2['cautionTime'], \
        caution_Set_2['haz_SLoWC_time'] - caution_Set_2['hazLmvViolatedTime'], \
        caution_Set_2['haz_SLoWC_time'] - caution_Set_2['hazFirstTime']]

    hist_data_set3 = [\
        caution_Set_3['hazLmvViolatedTime'] - caution_Set_3['cautionTime'], \
        caution_Set_3['haz_SLoWC_time'] - caution_Set_3['hazLmvViolatedTime'], \
        caution_Set_3['haz_SLoWC_time'] - caution_Set_3['hazFirstTime']]


    hist_dist_data_set1 = [caution_Set_1['cautionRange'], \
                           caution_Set_1['hazLmvTimeRange'], \
                           caution_Set_1['hazFirstTimeRange'], \
                           caution_Set_1['hazSLoWCTimeRange'], \
                           caution_Set_1['cautionElevation'], \
                           caution_Set_1['hazLmvTimeElevation'], \
                           caution_Set_1['hazFirstTimeElevation'], \
                           caution_Set_1['hazSLoWCTimeElevation'], \
                           caution_Set_1['cautionAzimuth'], \
                           caution_Set_1['hazLmvTimeAzimuth'], \
                           caution_Set_1['hazFirstTimeAzimuth'], \
                           caution_Set_1['hazSLoWCTimeAzimuth'], \
                            caution_Set_1['cautionBeta'], \
                            caution_Set_1['hazLmvTimeBeta'], \
                            caution_Set_1['hazFirstTimeBeta'], \
                            caution_Set_1['hazSLoWCTimeBeta']]

    hist_dist_data_set2 = [caution_Set_2['cautionFirstRange'], \
                           caution_Set_2['hazLmvTimeRange'], \
                           caution_Set_2['hazFirstTimeRange'], \
                           caution_Set_2['hazSLoWCTimeRange'], \
                           caution_Set_2['cautionFirstElevation'], \
                           caution_Set_2['hazLmvTimeElevation'], \
                           caution_Set_2['hazFirstTimeElevation'], \
                           caution_Set_2['hazSLoWCTimeElevation'], \
                           caution_Set_2['cautionFirstAzimuth'], \
                           caution_Set_2['hazLmvTimeAzimuth'], \
                           caution_Set_2['hazFirstTimeAzimuth'], \
                           caution_Set_2['hazSLoWCTimeAzimuth'], \
                            caution_Set_2['cautionFirstBeta'], \
                            caution_Set_2['hazLmvTimeBeta'], \
                            caution_Set_2['hazFirstTimeBeta'], \
                            caution_Set_2['hazSLoWCTimeBeta']]

    hist_dist_data_set3 = [caution_Set_3['cautionRange'], \
                           caution_Set_3['hazLmvTimeRange'], \
                           caution_Set_3['hazFirstTimeRange'], \
                           caution_Set_3['hazSLoWCTimeRange'], \
                           caution_Set_3['cautionElevation'], \
                           caution_Set_3['hazLmvTimeElevation'], \
                           caution_Set_3['hazFirstTimeElevation'], \
                           caution_Set_3['hazSLoWCTimeElevation'], \
                           caution_Set_3['cautionAzimuth'], \
                           caution_Set_3['hazLmvTimeAzimuth'], \
                           caution_Set_3['hazFirstTimeAzimuth'], \
                           caution_Set_3['hazSLoWCTimeAzimuth'], \
                            caution_Set_3['cautionBeta'], \
                            caution_Set_3['hazLmvTimeBeta'], \
                            caution_Set_3['hazFirstTimeBeta'], \
                            caution_Set_3['hazSLoWCTimeBeta']]



    group_labels = ['Caution Time to LMV', 'LMV to WSLOWC', 'LoWC to WSLOWC']

    df3_1 = pd.concat(hist_data_set1, axis=1, keys=group_labels)
    df3_2 = pd.concat(hist_data_set2, axis=1, keys=group_labels)
    df3_3 = pd.concat(hist_data_set3, axis=1, keys=group_labels)


    df3 = pd.concat([df3_1, df3_2, df3_3], axis=0)


    fResults.write("\tAt Caution, Time to LMV. \tMean: " + str(df3.iloc[:, 0].mean()) + " Stddev: " + str(df3.iloc[:, 0].std()) + "\n")
    fResults.write("\tAt LMV, Time to LoWC. \tMean: " + str(df3.iloc[:, 1].mean()) + " Stddev: " + str(df3.iloc[:, 1].std()) + "\n")
    fResults.write("\tAt LoWC, Time to WSLOWC. \tMean: " + str(df3.iloc[:, 2].mean()) + " Stddev: " + str(df3.iloc[:, 2].std()) + "\n")


    fResults.write(
        "\tTotal number of LoWC with LMV: Single interval: " + str(len(caution_Set_1.index)) + " Multiple caution divided in: Combined: " + str(len(caution_Set_2.index)) + " Single: " + str(len(caution_Set_3.index)) + "\n")

    mycsv.write(str(len(caution_Set_1.index)) + "," + str(len(caution_Set_2.index)) + "," + str(len(caution_Set_3.index)) + ",")
    mycsvh.write("lmvSingle" + "," + "lmvMultiple" + "," + "lmvUnique" + ",")

    mycsv.write(str(df3.iloc[:, 0].mean()) + "," + str(df3.iloc[:, 0].std()) + ",")
    mycsvh.write("cautionToLMVMean" + "," + "cautionToLMVStd" + ",")

    mycsv.write(str(df3.iloc[:, 1].mean()) + "," + str(df3.iloc[:, 1].std()) + ",")
    mycsvh.write("LMVToLoWCMean" + "," + "LMVToLoWCStd" + ",")

    mycsv.write(str(df3.iloc[:, 2].mean()) + "," + str(df3.iloc[:, 2].std()) + ",")
    mycsvh.write("lowcToWSLoWCMean" + "," + "lowcToWSLoWCStd" + ",")


    if len(df3.index) > 1 and False:

        # SHOW FIGURE
        df3_plot = [df3['Caution Time to LMV'], \
                     df3['LMV to WSLOWC'], \
                     df3['LoWC to WSLOWC'], \
                    ]

        group_labels = ['Caution Time to LMV', 'LMV to WSLOWC', 'LoWC to WSLOWC']


        fig = ff.create_distplot(df3_plot, group_labels, bin_size=[5, 5, 5], show_hist=False)
        fig.update_layout(margin_b=0, margin_l=0, margin_r=0, width=1000)
        fig.update_xaxes(title_text='Time(s)', range=[0, 80])
        fig.update_layout(title_text='Caution Alert - LoWC: Time (sec)')
        fig.update_layout(legend=dict(
            yanchor="top",
            y=0.99,
            xanchor="right",
            x=0.99
        ))

        filename = folderPath + "LoWC-Caution_Time_Comparison_Histogram-A.html"
        fResults.write("\tGenerating figure: " + filename + "\n")
        fig.write_html(filename)
        filename = folderPath + "LoWC-Caution_Time_Comparison_Histogram-A.png"
        fig.write_image(filename)

        mydoc.add_picture(folderPath + "LoWC-Caution_Time_Comparison_Histogram-A.png", width=docx.shared.Inches(7.5))


        fig = px.ecdf(df3, group_labels, ecdfmode="reversed", marginal="box")
        fig.update_layout(margin_b= 0, margin_l= 0, margin_r= 0, width= 1000)
        fig.update_xaxes(title_text='Time(s)', range=[0, 80])
        fig.update_yaxes(title_text='CDF')
        fig.update_layout(title_text='Caution Alert - LoWC')
        fig.update_layout(legend=dict(
            yanchor="top",
            y=0.99,
            xanchor="right",
            x=0.99
        ))

        filename = folderPath + "LoWC-Caution_Time_Comparison_Histogram-CDF.html"
        fResults.write("\tGenerating figure: " + filename + "\n")
        fig.write_html(filename)
        filename = folderPath + "LoWC-Caution_Time_Comparison_Histogram-CDF.png"
        fig.write_image(filename)

        mydoc.add_picture(folderPath + "LoWC-Caution_Time_Comparison_Histogram-CDF.png", width=docx.shared.Inches(7.5))


        # SHOW FIGURE: RANGE

        caution_Set = pd.concat([caution_Set_1, caution_Set_2, caution_Set_3], axis=0)

        group_labels = ['Range at Caution', 'Range at LMV', 'Range at LoWC', 'Range at WSLOWC',
                        'Elevation at Caution', 'Elevation at LMV', 'Elevation at LoWC', 'Elevation at WSLOWC',
                        'Azimuth at Caution', 'Azimuth at LMV', 'Azimuth at LoWC', 'Azimuth at WSLOWC',
                        'Beta at Caution', 'Beta at LMV', 'Beta at LoWC', 'Beta at WSLOWC',]

        df3_1 = pd.concat(hist_dist_data_set1, axis=1, keys=group_labels)
        df3_2 = pd.concat(hist_dist_data_set2, axis=1, keys=group_labels)
        df3_3 = pd.concat(hist_dist_data_set3, axis=1, keys=group_labels)

        df3_dist = pd.concat([df3_1, df3_2, df3_3], axis=0)

        df3_dist_plot = [units.m_to_nm(df3_dist['Range at Caution']), \
                         units.m_to_nm(df3_dist['Range at LMV']), \
                         units.m_to_nm(df3_dist['Range at LoWC']), \
                         units.m_to_nm(df3_dist['Range at WSLOWC']), \
                         ]

        group_labels = ['Range at Caution', 'Range at LMV', 'Range at LoWC', 'Range at WSLOWC']


        fig = ff.create_distplot(df3_dist_plot, group_labels, bin_size=[0.5, 0.5, 0.5, 0.5], histnorm='probability', show_hist= False)
        fig.update_layout(margin_b= 0, margin_l= 0, margin_r= 0, width= 1000)
        fig.update_xaxes(title_text='Range (NM)', range=[0, 4])
        fig.update_layout(title_text='Caution Alert with LMV: Horizontal Range (NM)')
        fig.update_layout(legend=dict(
            yanchor="top",
            y=0.99,
            xanchor="right",
            x=0.99
        ))

        filename = folderPath + "LMV_LoWCCaution_Range_Comparison_Histogram-A.html"
        fResults.write("\tGenerating figure: " + filename + "\n")
        fig.write_html(filename)
        filename = folderPath + "LMV_LoWCCaution_Range_Comparison_Histogram-A.png"
        fig.write_image(filename)

        mydoc.add_picture(folderPath + "LMV_LoWCCaution_Range_Comparison_Histogram-A.png", width=docx.shared.Inches(7.5))



        df3 = pd.concat(df3_dist_plot, axis=1, keys=group_labels)

        fig = px.ecdf(df3, group_labels, ecdfmode="reversed", marginal="box")
        fig.update_layout(margin_b= 0, margin_l= 0, margin_r= 0, width= 1000)
        fig.update_xaxes(title_text='Range (NM)', range=[0, 4])
        fig.update_yaxes(title_text='CDF')
        fig.update_layout(title_text='Caution Alert with LMV: Horizontal Range (NM)')
        fig.update_layout(legend=dict(
            yanchor="top",
            y=0.99,
            xanchor="right",
            x=0.99
        ))

        filename = folderPath + "LMV_LoWCCaution_Range_Comparison_Histogram-CDF.html"
        fResults.write("\tGenerating figure: " + filename + "\n")
        fig.write_html(filename)
        filename = folderPath + "LMV_LoWCCaution_Range_Comparison_Histogram-CDF.png"
        fig.write_image(filename)

        mydoc.add_picture(folderPath + "LMV_LoWCCaution_Range_Comparison_Histogram-CDF.png", width=docx.shared.Inches(7.5))


        mycsv.write(str(units.m_to_nm(df3_dist['Range at Caution'].mean())) + "," + str(units.m_to_nm(df3_dist['Range at Caution'].std())) + ",")
        mycsv.write(str(units.m_to_nm(df3_dist['Range at LMV'].mean())) + "," + str(units.m_to_nm(df3_dist['Range at LMV'].std())) + ",")
        mycsvh.write("Range at Caution: Mean" + "," + "Range at Caution: Std" + ",")
        mycsvh.write("Range at LMV: Mean" + "," + "Range at LMV: Std" + ",")


        hist1 = np.histogram(df3_dist.iloc[:, 1], bins=30)
        dist_1 = scipy.stats.rv_histogram(hist1)
        p1 = dist_1.cdf(units.nm_to_m(8.0))

        fResults.write("\tRange Caution. At LMV: " + str(p1 * 100) + "\n")

        mycsv.write(str(p1 * 100) + ",")
        mycsvh.write("RangeCautionLMV" + ",")

        hist1 = np.histogram(df3_dist.iloc[:, 5], bins=30)
        dist_1 = scipy.stats.rv_histogram(hist1)
        p1 = dist_1.cdf(15) - dist_1.cdf(-15)

        fResults.write("\tElevation Caution. At LMV: " + str(p1 * 100) + "\n")

        mycsv.write(str(p1 * 100) + ",")
        mycsvh.write("ElevationCautionLMV" + ",")

        hist1 = np.histogram(df3_dist.iloc[:, 9], bins=180)
        dist_1 = scipy.stats.rv_histogram(hist1)
        p1 = dist_1.cdf(110) - dist_1.cdf(-110)

        fResults.write("\tAzimuth Caution. At caution LMV: " + str(p1 * 100) + "\n")

        mycsv.write(str(p1 * 100) + ",")
        mycsvh.write("AzimuthCautionLMV" + ",")

        inCoverageVector1 = (df3_dist.iloc[:, 1] <= units.nm_to_m(8.0))
        inCoverageVector2 = (df3_dist.iloc[:, 5] <= 15) & (df3_dist.iloc[:, 5] >= -15)
        inCoverageVector3 = (df3_dist.iloc[:, 9] <= 110) & (df3_dist.iloc[:, 9] >= -110)

        inCoverage1 = np.count_nonzero(inCoverageVector1 & inCoverageVector2 & inCoverageVector3)

        fResults.write(
            "\tIn covegare. At caution LMV: " + str(100 * inCoverage1 / len(caution_Set.index)) + "\n")
        fResults.write("\n\n")

        mycsv.write(str(100 * inCoverage1 / len(caution_Set.index)) + ",")
        mycsvh.write("InCoverageCautionLMV" + ",")

        #######################################################
        mydoc.add_page_break()
        mydoc.add_heading("Conflict Angle Analysis", 2)

        df3_dist_plot = [df3_dist['Beta at Caution'], \
                         df3_dist['Beta at LMV'], \
                         df3_dist['Beta at LoWC'], \
                         df3_dist['Beta at WSLOWC'], \
                         ]

        group_labels = ['Beta at Caution', 'Beta at LMV', 'Beta at LoWC', 'Beta at WSLOWC']

        fig = ff.create_distplot(df3_dist_plot, group_labels, bin_size=[5, 5, 5, 5])
        fig.update_traces(overwrite=True, marker={"opacity": 0.6})
        fig.update_layout(margin_b=0, margin_l=0, margin_r=0, width=1000)
        fig.update_xaxes(title_text='Beta (deg)')
        fig.update_layout(title_text='Conflict Beta Angle (deg)')
        fig.update_layout(legend=dict(
            yanchor="top",
            y=0.99,
            xanchor="right",
            x=0.99
        ))

        filename = folderPath + "LMV_LoWCCaution_Beta_Comparison_Histogram-A.html"
        fResults.write("\tGenerating figure: " + filename + "\n")
        fig.write_html(filename)
        filename = folderPath + "LMV_LoWCCaution_Beta_Comparison_Histogram-A.png"
        fig.write_image(filename)

        mydoc.add_picture(folderPath + "LMV_LoWCCaution_Beta_Comparison_Histogram-A.png", width=docx.shared.Inches(7.5))

        df3_dist_plot = [df3_dist['Azimuth at Caution'], \
                         df3_dist['Azimuth at LMV'], \
                         df3_dist['Azimuth at LoWC'], \
                         df3_dist['Azimuth at WSLOWC'], \
                         ]

        group_labels = ['Azimuth at Caution', 'Azimuth at LMV', 'Azimuth at LoWC', 'Azimuth at WSLOWC']

        fig = ff.create_distplot(df3_dist_plot, group_labels, bin_size=[5, 5, 5, 5])
        fig.update_traces(overwrite=True, marker={"opacity": 0.6})
        fig.update_layout(margin_b=0, margin_l=0, width=1000)
        fig.update_xaxes(title_text='Beta (deg)')
        fig.update_layout(title_text='Conflict Azimuth Angle (deg)')
        fig.update_layout(legend=dict(
            yanchor="top",
            y=0.99,
            xanchor="right",
            x=0.99
        ))

        filename = folderPath + "LMV_LoWCCaution_Azimuth_Comparison_Histogram-A.html"
        fResults.write("\tGenerating figure: " + filename + "\n")
        fig.write_html(filename)
        filename = folderPath + "LMV_LoWCCaution_Azimuth_Comparison_Histogram-A.png"
        fig.write_image(filename)

        mydoc.add_picture(folderPath + "LMV_LoWCCaution_Azimuth_Comparison_Histogram-A.png",
                          width=docx.shared.Inches(7.5))

        mydoc.add_page_break()
        mydoc.add_heading("Active Sensor Analysis", 2)

        fig = generate_dual_polar_plot(units.m_to_nm(df3_dist['Range at Caution']), df3_dist['Azimuth at Caution'],
                                       df3_dist['Elevation at Caution'], -180, 180, 20, -20,
                                       "Range, Azimuth and Elevation at Caution Alert")
        fig.update_layout(margin_b=0, margin_l=0, width=1000)
        # fig.update_xaxes(title_text='Beta (deg)')
        # fig.update_layout(title_text='Conflict Azimuth Angle (deg)')

        filename = folderPath + "LoWCCaution_AtCaution_Comparison_AzimuthElevation_PolarPlots.html"
        fResults.write("\tGenerating figure: " + filename + "\n")
        fig.write_html(filename)

        filename = folderPath + "LoWCCaution_AtCaution_Comparison_AzimuthElevation_PolarPlots.png"
        fig.write_image(filename)

        mydoc.add_picture(folderPath + "LoWCCaution_AtCaution_Comparison_AzimuthElevation_PolarPlots.png",
                          width=docx.shared.Inches(7.5))

        fig = generate_dual_polar_plot(units.m_to_nm(df3_dist['Range at LMV']), df3_dist['Azimuth at LMV'],
                                       df3_dist['Elevation at LMV'], -180, 180, 20, -20,
                                       "Range, Azimuth and Elevation at LMV")
        fig.update_layout(margin_b=0, margin_l=0, width=1000)
        # fig.update_xaxes(title_text='Beta (deg)')
        # fig.update_layout(title_text='Conflict Azimuth Angle (deg)')

        filename = folderPath + "LoWCCaution_AtLMV_Comparison_AzimuthElevation_PolarPlots.html"
        fResults.write("\tGenerating figure: " + filename + "\n")
        fig.write_html(filename)

        filename = folderPath + "LoWCCaution_AtLMV_Comparison_AzimuthElevation_PolarPlots.png"
        fig.write_image(filename)

        mydoc.add_picture(folderPath + "LoWCCaution_AtLMV_Comparison_AzimuthElevation_PolarPlots.png",
                          width=docx.shared.Inches(7.5))

        fig = generate_dual_polar_plot(units.m_to_nm(df3_dist['Range at LoWC']), df3_dist['Azimuth at LoWC'],
                                       df3_dist['Elevation at LoWC'], -180, 180, 20, -20,
                                       "Range, Azimuth and Elevation at LoWC")
        fig.update_layout(margin_b=0, margin_l=0, width=1000)
        # fig.update_xaxes(title_text='Beta (deg)')
        # fig.update_layout(title_text='Conflict Azimuth Angle (deg)')

        filename = folderPath + "LoWCCaution_AtLoWC_Comparison_AzimuthElevation_PolarPlots.html"
        fResults.write("\tGenerating figure: " + filename + "\n")
        fig.write_html(filename)

        filename = folderPath + "LoWCCaution_AtLoWC_Comparison_AzimuthElevation_PolarPlots.png"
        fig.write_image(filename)

        mydoc.add_picture(folderPath + "LoWCCaution_AtLoWC_Comparison_AzimuthElevation_PolarPlots.png",
                          width=docx.shared.Inches(7.5))

        fig = generate_dual_polar_plot(units.m_to_nm(df3_dist['Range at WSLOWC']), df3_dist['Azimuth at WSLOWC'],
                                       df3_dist['Elevation at WSLOWC'], -180, 180, 20, -20,
                                       "Range, Azimuth and Elevation at WSLoWC")
        fig.update_layout(margin_b=0, margin_l=0, width=1000)
        # fig.update_xaxes(title_text='Beta (deg)')
        # fig.update_layout(title_text='Conflict Azimuth Angle (deg)')

        filename = folderPath + "WSLoWCCaution_AtLoWC_Comparison_AzimuthElevation_PolarPlots.html"
        fResults.write("\tGenerating figure: " + filename + "\n")
        fig.write_html(filename)

        filename = folderPath + "WSLoWCCaution_AtLoWC_Comparison_AzimuthElevation_PolarPlots.png"
        fig.write_image(filename)

        mydoc.add_picture(folderPath + "WSLoWCCaution_AtLoWC_Comparison_AzimuthElevation_PolarPlots.png",
                          width=docx.shared.Inches(7.5))

        fResults.write("\tIterating complex LMV Values: cautionTime -> hazLmvViolatedTime -> hazFirstTime:\n")
        for index, row in caution_Set_2.iterrows():
            fResults.write("\t( Layer: " + str(int(row["layer"])) + " - EncounterId: " + str(
                int(row["EncounterId"])) + " : " + str(int(row["cautionFirstTime"])) + " -> " + str(
                int(row["hazLmvViolatedTime"])) + " -> " + str(int(row["hazFirstTime"])) + " )\n")
        fResults.write("\tDone.\n")




    # Caution activations with CA
    # We reuse the previous dataframes that have been deepcopied

    # LoWCV in which caution is activated and in which there a single caution activation or multiple activations

    fResults.write("\n\n")
    fResults.write("Active Caution - WARN - Timming Evaluation:" + "\n")


    # We recover from the previous copy
    caution_Set_1 = cautionSetOrig_1
    caution_Set_2 = cautionSetOrig_2
    caution_Set_3 = cautionSetOrig_3

    caution_Set_Filter = ((caution_Set_1['warnTime'] > 0))
    caution_Set_1 = caution_Set_1.loc[caution_Set_Filter]

    caution_Set_Filter = ((caution_Set_2['warnTime'] > 0))
    caution_Set_2 = caution_Set_2.loc[caution_Set_Filter]

    caution_Set_Filter = ((caution_Set_3['warnTime'] > 0))
    caution_Set_3 = caution_Set_3.loc[caution_Set_Filter]

    fResults.write(
        "\tTotal number of LoWC with Warning Alerts: Single interval: " + str(
            len(caution_Set_1.index)) + " Multiple caution divided in: Combined: " + str(
            len(caution_Set_2.index)) + " Single: " + str(len(caution_Set_3.index)) + "\n")


    mycsv.write(str(len(caution_Set_1.index) + len(caution_Set_2.index) + len(caution_Set_3.index)) + ",")
    mycsvh.write("TotalWarnCautionLoWC" + ",")

    mycsv.write(str(len(caution_Set_1.index)) + "," + str(len(caution_Set_2.index)) + "," + str(len(caution_Set_3.index)) + ",")
    mycsvh.write("singleWarnCautionLoWC" + "," + "multiWarnCautionLoWC" + "," + "uniqueWarnCautionLoWC" + ",")

    mydoc.add_page_break()
    mydoc.add_heading("Active Caution with Warn Analysis", 2)
    mydoc.add_paragraph("Total number of LoWC with Warn: Single interval: " + str(
        len(caution_Set_1.index)) + " Multiple caution divided in: Combined: " + str(
        len(caution_Set_2.index)) + " Single: " + str(len(caution_Set_3.index)))

    hist_data_set1 = [ \
        caution_Set_1['warnTime'] - caution_Set_1['cautionTime'], \
        caution_Set_1['hazFirstTime'] - caution_Set_1['cautionTime'], \
        caution_Set_1['hazFirstTime'] - caution_Set_1['warnTime'], \
        caution_Set_1['haz_SLoWC_time'] - caution_Set_1['warnTime'], \
        caution_Set_1['haz_SLoWC_time'] - caution_Set_1['hazFirstTime']]

    hist_data_set2 = [ \
        caution_Set_1['warnTime'] - caution_Set_1['cautionFirstTime'], \
        caution_Set_1['hazFirstTime'] - caution_Set_1['cautionFirstTime'], \
        caution_Set_1['hazFirstTime'] - caution_Set_1['warnTime'], \
        caution_Set_1['haz_SLoWC_time'] - caution_Set_1['warnTime'], \
        caution_Set_1['haz_SLoWC_time'] - caution_Set_1['hazFirstTime']]

    hist_data_set3 = [ \
        caution_Set_1['warnTime'] - caution_Set_1['cautionTime'], \
        caution_Set_1['hazFirstTime'] - caution_Set_1['cautionTime'], \
        caution_Set_1['hazFirstTime'] - caution_Set_1['warnTime'], \
        caution_Set_1['haz_SLoWC_time'] - caution_Set_1['warnTime'], \
        caution_Set_1['haz_SLoWC_time'] - caution_Set_1['hazFirstTime']]

    hist_dist_data_set1 = [caution_Set_1['cautionRange'], \
                           caution_Set_1['warnRange'], \
                           caution_Set_1['hazFirstTimeRange'], \
                           caution_Set_1['hazSLoWCTimeRange'], \
                           caution_Set_1['cautionElevation'], \
                           caution_Set_1['warnElevation'], \
                           caution_Set_1['hazFirstTimeElevation'], \
                           caution_Set_1['hazSLoWCTimeElevation'], \
                           caution_Set_1['cautionAzimuth'], \
                           caution_Set_1['warnAzimuth'], \
                           caution_Set_1['hazFirstTimeAzimuth'], \
                           caution_Set_1['hazSLoWCTimeAzimuth'], \
                           caution_Set_1['cautionBeta'], \
                           caution_Set_1['warnBeta'], \
                           caution_Set_1['hazFirstTimeBeta'], \
                           caution_Set_1['hazSLoWCTimeBeta']]

    hist_dist_data_set2 = [caution_Set_2['cautionFirstRange'], \
                           caution_Set_2['warnRange'], \
                           caution_Set_2['hazFirstTimeRange'], \
                           caution_Set_2['hazSLoWCTimeRange'], \
                           caution_Set_2['cautionFirstElevation'], \
                           caution_Set_2['warnElevation'], \
                           caution_Set_2['hazFirstTimeElevation'], \
                           caution_Set_2['hazSLoWCTimeElevation'], \
                           caution_Set_2['cautionFirstAzimuth'], \
                           caution_Set_2['warnAzimuth'], \
                           caution_Set_2['hazFirstTimeAzimuth'], \
                           caution_Set_2['hazSLoWCTimeAzimuth'], \
                           caution_Set_2['cautionFirstBeta'], \
                           caution_Set_2['warnBeta'], \
                           caution_Set_2['hazFirstTimeBeta'], \
                           caution_Set_2['hazSLoWCTimeBeta']]

    hist_dist_data_set3 = [caution_Set_3['cautionRange'], \
                           caution_Set_3['warnRange'], \
                           caution_Set_3['hazFirstTimeRange'], \
                           caution_Set_3['hazSLoWCTimeRange'], \
                           caution_Set_3['cautionElevation'], \
                           caution_Set_3['warnElevation'], \
                           caution_Set_3['hazFirstTimeElevation'], \
                           caution_Set_3['hazSLoWCTimeElevation'], \
                           caution_Set_3['cautionAzimuth'], \
                           caution_Set_3['warnAzimuth'], \
                           caution_Set_3['hazFirstTimeAzimuth'], \
                           caution_Set_3['hazSLoWCTimeAzimuth'], \
                           caution_Set_3['cautionBeta'], \
                           caution_Set_3['warnBeta'], \
                           caution_Set_3['hazFirstTimeBeta'], \
                           caution_Set_3['hazSLoWCTimeBeta']]

    group_labels = ['Caution Time to WARN', 'Caution Time to LoWC', 'WARN to LoWC', 'WARN to WSLOWC', 'LoWC to WSLOWC']

    df3_1 = pd.concat(hist_data_set1, axis=1, keys=group_labels)
    df3_2 = pd.concat(hist_data_set2, axis=1, keys=group_labels)
    df3_3 = pd.concat(hist_data_set3, axis=1, keys=group_labels)

    df3 = pd.concat([df3_1, df3_2, df3_3], axis=0)

    mycsv.write(str(df3.iloc[:, 0].mean()) + "," + str(df3.iloc[:, 0].std()) + ",")
    mycsvh.write("cautionToWarnMean" + "," + "cautionToWarnStd" + ",")

    mycsv.write(str(df3.iloc[:, 1].mean()) + "," + str(df3.iloc[:, 1].std()) + ",")
    mycsvh.write("cautionToLoWCMean" + "," + "cautionToLoWCStd" + ",")

    mycsv.write(str(df3.iloc[:, 2].mean()) + "," + str(df3.iloc[:, 2].std()) + ",")
    mycsvh.write("warnToLoWCMean" + "," + "warnToLoWCStd" + ",")

    mycsv.write(str(df3.iloc[:, 3].mean()) + "," + str(df3.iloc[:, 3].std()) + ",")
    mycsvh.write("warnToWSLoWCMean" + "," + "warnToWSLoWCStd" + ",")

    mycsv.write(str(df3.iloc[:, 4].mean()) + "," + str(df3.iloc[:, 4].std()) + ",")
    mycsvh.write("lowcToWSLoWCMean" + "," + "lowcToWSLoWCStd" + ",")

    if len(df3.index) > 1:

        fResults.write("\tAt Caution, Time to WARN. \tMean: " + str(df3.iloc[:, 0].mean()) + " Stddev: " + str(
            df3.iloc[:, 0].std()) + "\n")
        fResults.write("\tAt Caution, Time to LoWC. \tMean: " + str(df3.iloc[:, 1].mean()) + " Stddev: " + str(
            df3.iloc[:, 1].std()) + "\n")
        fResults.write("\tAt WARN, Time to LoWC. \tMean: " + str(df3.iloc[:, 2].mean()) + " Stddev: " + str(
            df3.iloc[:, 2].std()) + "\n")
        fResults.write("\tAt WARN, Time to WSLoWC. \tMean: " + str(df3.iloc[:, 3].mean()) + " Stddev: " + str(
            df3.iloc[:, 3].std()) + "\n")
        fResults.write("\tAt LoWC, Time to WSLOWC. \tMean: " + str(df3.iloc[:, 4].mean()) + " Stddev: " + str(
            df3.iloc[:, 4].std()) + "\n")


        # SHOW FIGURE
        df3_plot = [df3['Caution Time to WARN'], \
                    df3['Caution Time to LoWC'], \
                    df3['WARN to LoWC'], \
                    df3['WARN to WSLOWC'],
                    df3['LoWC to WSLOWC'], \
                    ]


        group_labels = ['Caution Time to WARN', 'Caution Time to LoWC', 'WARN to LoWC', 'WARN to WSLOWC', 'LoWC to WSLOWC']

        fig = ff.create_distplot(df3_plot, group_labels, bin_size=[5, 5, 5, 5, 5], show_hist=False)
        fig.update_layout(margin_b=0, margin_l=0, margin_r=0, width=1000)
        fig.update_xaxes(title_text='Time(s)', range=[0, 80])
        fig.update_layout(title_text='Caution Alert - WARN - LoWC: Time (sec)')
        fig.update_layout(legend=dict(
            yanchor="top",
            y=0.99,
            xanchor="right",
            x=0.99
        ))

        filename = folderPath + "WARN-LoWC-Caution_Time_Comparison_Histogram-A.html"
        fResults.write("\tGenerating figure: " + filename + "\n")
        fig.write_html(filename)
        filename = folderPath + "WARN-LoWC-Caution_Time_Comparison_Histogram-A.png"
        fig.write_image(filename)

        mydoc.add_picture(folderPath + "WARN-LoWC-Caution_Time_Comparison_Histogram-A.png", width=docx.shared.Inches(7.5))

        fig = px.ecdf(df3, group_labels, ecdfmode="reversed", marginal="box")
        fig.update_layout(margin_b=0, margin_l=0, margin_r=0, width=1000)
        fig.update_xaxes(title_text='Time(s)', range=[0, 80])
        fig.update_yaxes(title_text='CDF')
        fig.update_layout(title_text='Caution Alert - WARN - LoWC')
        fig.update_layout(legend=dict(
            yanchor="top",
            y=0.99,
            xanchor="right",
            x=0.99
        ))

        filename = folderPath + "WARN-LoWC-Caution_Time_Comparison_Histogram-CDF.html"
        fResults.write("\tGenerating figure: " + filename + "\n")
        fig.write_html(filename)
        filename = folderPath + "WARN-LoWC-Caution_Time_Comparison_Histogram-CDF.png"
        fig.write_image(filename)

        mydoc.add_picture(folderPath + "WARN-LoWC-Caution_Time_Comparison_Histogram-CDF.png", width=docx.shared.Inches(7.5))

        # SHOW FIGURE: RANGE

        caution_Set = pd.concat([caution_Set_1, caution_Set_2, caution_Set_3], axis=0)

        group_labels = ['Range at Caution', 'Range at WARN', 'Range at LoWC', 'Range at WSLOWC',
                        'Elevation at Caution', 'Elevation at WARN', 'Elevation at LoWC', 'Elevation at WSLOWC',
                        'Azimuth at Caution', 'Azimuth at WARN', 'Azimuth at LoWC', 'Azimuth at WSLOWC',
                        'Beta at Caution', 'Beta at WARN', 'Beta at LoWC', 'Beta at WSLOWC', ]

        df3_1 = pd.concat(hist_dist_data_set1, axis=1, keys=group_labels)
        df3_2 = pd.concat(hist_dist_data_set2, axis=1, keys=group_labels)
        df3_3 = pd.concat(hist_dist_data_set3, axis=1, keys=group_labels)

        df3_dist = pd.concat([df3_1, df3_2, df3_3], axis=0)

        df3_dist_plot = [units.m_to_nm(df3_dist['Range at Caution']), \
                         units.m_to_nm(df3_dist['Range at WARN']), \
                         units.m_to_nm(df3_dist['Range at LoWC']), \
                         units.m_to_nm(df3_dist['Range at WSLOWC']), \
                         ]

        group_labels = ['Range at Caution', 'Range at WARN', 'Range at LoWC', 'Range at WSLOWC']

        fig = ff.create_distplot(df3_dist_plot, group_labels, bin_size=[0.5, 0.5, 0.5, 0.5], histnorm='probability',
                                 show_hist=False)
        fig.update_layout(margin_b=0, margin_l=0, margin_r=0, width=1000)
        fig.update_xaxes(title_text='Range (NM)', range=[0, 4])
        fig.update_layout(title_text='Caution Alert with WARN: Horizontal Range (NM)')
        fig.update_layout(legend=dict(
            yanchor="top",
            y=0.99,
            xanchor="right",
            x=0.99
        ))

        filename = folderPath + "WARN_LoWCCaution_Range_Comparison_Histogram-A.html"
        fResults.write("\tGenerating figure: " + filename + "\n")
        fig.write_html(filename)
        filename = folderPath + "WARN_LoWCCaution_Range_Comparison_Histogram-A.png"
        fig.write_image(filename)

        mydoc.add_picture(folderPath + "WARN_LoWCCaution_Range_Comparison_Histogram-A.png", width=docx.shared.Inches(7.5))

        df3 = pd.concat(df3_dist_plot, axis=1, keys=group_labels)

        fig = px.ecdf(df3, group_labels, ecdfmode="reversed", marginal="box")
        fig.update_layout(margin_b=0, margin_l=0, margin_r=0, width=1000)
        fig.update_xaxes(title_text='Range (NM)', range=[0, 4])
        fig.update_yaxes(title_text='CDF')
        fig.update_layout(title_text='Caution Alert with WARN: Horizontal Range (NM)')
        fig.update_layout(legend=dict(
            yanchor="top",
            y=0.99,
            xanchor="right",
            x=0.99
        ))

        filename = folderPath + "WARN_LoWCCaution_Range_Comparison_Histogram-CDF.html"
        fResults.write("\tGenerating figure: " + filename + "\n")
        fig.write_html(filename)
        filename = folderPath + "WARN_LoWCCaution_Range_Comparison_Histogram-CDF.png"
        fig.write_image(filename)

        mydoc.add_picture(folderPath + "WARN_LoWCCaution_Range_Comparison_Histogram-CDF.png", width=docx.shared.Inches(7.5))



    #############################################################################
    fResults.write("\n\n")
    fResults.write("LoWCV without Caution activation - Timming Evaluation:" + "\n")

    caution_Set_Filter = ((df['hazFirstTime'] > 0))
    caution_Set = df.loc[caution_Set_Filter]

    total = len(caution_Set.index)

    caution_Set_Filter = ((caution_Set['cautionTime'] <= 0) | (caution_Set['hazFirstTime'] - caution_Set['cautionTime'] < minCautionInterval))
    caution_Set_1 = caution_Set.loc[caution_Set_Filter]

    caution_Set_Filter = ((caution_Set['cautionTime'] <= 0) | (caution_Set['hazFirstTime'] - caution_Set['cautionEndTime'] > minCautionInterval))
    caution_Set = caution_Set.loc[caution_Set_Filter]

    fResults.write("\tTotal number of LoWC: " + str(total) + " short Caution: " + str(len(caution_Set_1.index)) + " without Cautions: " + str(len(caution_Set.index)) + "\n")

    caution_Set = pd.concat([caution_Set, caution_Set_1], axis=0)

    if len(caution_Set.index):

        hist_data = [caution_Set['cpaTime'] - caution_Set['hazFirstTime'], \
                     caution_Set['haz_SLoWC_time'] - caution_Set['hazFirstTime'], \
                     caution_Set['hazFirstTcpa']]
        group_labels = ['Time to CPA', 'Time to SLoW', "Predicted Time to CPA"]


        df3 = pd.concat(hist_data, axis=1, keys=group_labels)

        fResults.write("\tAt WCV, Time to CPA. Mean: " + str(df3.iloc[:, 0].mean()) + " Stddev: " + str(df3.iloc[:, 0].std()) + "\n")
        fResults.write("\tAt WCV, Time to SLoWC. Mean: " + str(df3.iloc[:, 1].mean()) + " Stddev: " + str(df3.iloc[:, 1].std()) + "\n")
        fResults.write("\tAt WCV, predicted time to CPA. Mean: " + str(df3.iloc[:, 2].mean()) + " Stddev: " + str(df3.iloc[:, 2].std()) + "\n")
        fResults.write("\tSLoWC. Mean: " + str(caution_Set['hazSLoWC'].mean()) + " Stddev: " + str(caution_Set['hazSLoWC'].std()) + "\n")


        #####
        fResults.write("\tRange at LoWC: " + str(units.m_to_nm(caution_Set['hazFirstTimeRange'].mean())) + " Stddev: " + str(
            units.m_to_nm(caution_Set['hazFirstTimeRange'].std())) + "\n")
        fResults.write("\tRange at CPA: " + str(units.m_to_nm(caution_Set['cpaRange'].mean())) + " Stddev: " + str(
            units.m_to_nm(caution_Set['cpaRange'].std())) + "\n")
        fResults.write("\tRange at CPA: " + str(units.m_to_nm(caution_Set['hazSLoWCTimeRange'].mean())) + " Stddev: " + str(
            units.m_to_nm(caution_Set['hazSLoWCTimeRange'].std())) + "\n")

        hist1 = np.histogram(caution_Set['hazFirstTimeRange'], bins=30)
        dist_1 = scipy.stats.rv_histogram(hist1)
        p1 = dist_1.cdf(units.nm_to_m(8.0))

        hist2 = np.histogram(caution_Set['cpaRange'], bins=30)
        dist_2 = scipy.stats.rv_histogram(hist2)
        p2 = dist_2.cdf(units.nm_to_m(8.0))

        fResults.write(
            "\tRange. At LoWC: " + str(p1 * 100) + " At CPA: " + str(p2 * 100) + "\n")

        hist1 = np.histogram(caution_Set['hazFirstTimeElevation'], bins=30)
        dist_1 = scipy.stats.rv_histogram(hist1)
        p1 = dist_1.cdf(15) - dist_1.cdf(-15)

        hist2 = np.histogram(caution_Set['cpaElevation'], bins=80)
        dist_2 = scipy.stats.rv_histogram(hist2)
        p2 = dist_2.cdf(15) - dist_2.cdf(-15)

        fResults.write(
            "\tElevation Caution. At caution activation: " + str(p1 * 100) + " At caution end: " + str(p2 * 100) + "\n")

        hist1 = np.histogram(caution_Set['hazFirstTimeAzimuth'], bins=180)
        dist_1 = scipy.stats.rv_histogram(hist1)
        p1 = dist_1.cdf(110) - dist_1.cdf(-110)

        hist2 = np.histogram(caution_Set['cpaAzimuth'], bins=90)
        dist_2 = scipy.stats.rv_histogram(hist2)
        p2 = dist_2.cdf(110) - dist_2.cdf(-110)

        fResults.write(
            "\tAzimuth Caution. At caution activation: " + str(p1 * 100) + " At caution end: " + str(p2 * 100) + "\n")

        inCoverageVector1 = (caution_Set['hazFirstTimeRange'] <= units.nm_to_m(8.0))
        inCoverageVector2 = (caution_Set['hazFirstTimeElevation'] <= 15) & (caution_Set['hazFirstTimeElevation'] >= -15)
        inCoverageVector3 = (caution_Set['hazFirstTimeAzimuth'] <= 110) & (caution_Set['hazFirstTimeAzimuth'] >= -110)

        inCoverage1 = np.count_nonzero(inCoverageVector1 & inCoverageVector2 & inCoverageVector3)

        inCoverageVector1 = (caution_Set['cpaRange'] <= units.nm_to_m(8.0))
        inCoverageVector2 = (caution_Set['cpaElevation'] <= 15) & (caution_Set['cpaElevation'] >= -15)
        inCoverageVector3 = (caution_Set['cpaAzimuth'] <= 110) & (caution_Set['cpaAzimuth'] >= -110)

        inCoverage2 = np.count_nonzero(inCoverageVector1 & inCoverageVector2 & inCoverageVector3)

        if len(caution_Set.index) > 0:
            fResults.write(
                "\tIn covegare. At LoWC: " + str(
                    100 * inCoverage1 / len(caution_Set.index)) + " At CPA: " + str(
                    100 * inCoverage2 / len(caution_Set.index)) + "\n")
            fResults.write("\n\n")

        ####


        caution_Set_Filter = ((df['hazFirstTime'] > 0))
        caution_Set = df.loc[caution_Set_Filter]

        caution_Set_Filter = (caution_Set['cautionTime'] <= 0)
        caution_Set = caution_Set.loc[caution_Set_Filter]

        fResults.write("\tNo Caution LoWC encounters: \n")
        for index, row in caution_Set.iterrows():
            fResults.write("\t( Layer: " + str(int(row["layer"])) + " - EncounterId: " + str(int(row["EncounterId"])) + " )\n")

        fResults.write("\n")

        caution_Set_Filter = ((caution_Set['cautionTime'] <= 0) | (df['hazFirstTime'] - caution_Set['cautionEndTime'] > minCautionInterval))
        caution_Set = caution_Set.loc[caution_Set_Filter]

        fResults.write("\tLong Caution deactivation LoWC encounters: \n")
        for index, row in caution_Set.iterrows():
            fResults.write(
                "\t( Layer: " + str(int(row["layer"])) + " - EncounterId: " + str(int(row["EncounterId"])) + " )\n")

        fResults.write("\n")




def evaluateActivePreventiveTimming(fResults, mydoc,  mycsv, mycsvh, df, caption, folderPath):
    totalEncounters = len(df.index)

    # TIME RANGE FOR ACTIVE CAUTION ALERTS

    fResults.write("\n\n")
    fResults.write("Basic Preventive Timming Evaluation:" + "\n")

    mydoc.add_page_break()
    mydoc.add_heading("Preventive Timming Evaluation", 2)


    # STEP 1 ####################################################
    fResults.write("\n\n")
    fResults.write("Long Preventive - without Caution - General Timing Evaluation:" + "\n")

    prev_Set_Filter = ((df['cautionNumber'] == 0) & (df['prevNumber'] > 0))

    prev_Set = df.loc[prev_Set_Filter]

    fResults.write("\tTotal number of noCaution preventive activations: " + str(len(prev_Set.index)) + "\n")
    mydoc.add_paragraph("Total number of noCaution preventive activations: " + str(len(prev_Set.index)))

    mycsv.write(str(len(prev_Set.index)) + ",")
    mycsvh.write("noCautionPrev" + ",")

    prev_Set_Filter = (((prev_Set['prevEndTime'] - prev_Set['prevTime'] >= minPrevInterval)&(prev_Set['prevNumber'] == 1))|
                        ((prev_Set['prevTime'] - prev_Set['prevFirstEndTime'] <= minIniPrevInterval) & (prev_Set['prevEndTime'] - prev_Set['prevFirstTime'] >= minPrevInterval)&(prev_Set['prevNumber'] > 1)))

    prev_Set = prev_Set.loc[prev_Set_Filter]

    fResults.write("\tTotal number of noCaution long preventive activations: " + str(len(prev_Set.index)) + "\n")
    mydoc.add_paragraph("Total number of noCaution long preventive activations: " + str(len(prev_Set.index)))

    mycsv.write(str(len(prev_Set.index)) + ",")
    mycsvh.write("longNoCautionPrev" + ",")

    prev_Set_Filter = ((prev_Set['prevEndTime'] - prev_Set['prevTime'] >= minPrevDiscontinuity) & (prev_Set['prevNumber'] == 1))
    prev_Set_1 = prev_Set.loc[prev_Set_Filter]

    hist_dist_data_set1 = [prev_Set_1['prevRange'], \
                           prev_Set_1['prevElevation'], \
                           prev_Set_1['prevAzimuth'], \
                           prev_Set_1['prevEndRange'], \
                           prev_Set_1['prevEndElevation'], \
                           prev_Set_1['prevEndAzimuth'], \
                           prev_Set_1['cpaRange'], \
                           prev_Set_1['cpaElevation'], \
                           prev_Set_1['cpaAzimuth'], \
                           ]


    prev_Set_Filter = ((prev_Set['prevEndTime'] - prev_Set['prevTime'] >= longPrevInterval) & (prev_Set['prevNumber'] == 1))
    prev_Set_3 = prev_Set.loc[prev_Set_Filter]

    fResults.write("\tVery long preventive encounters: \n")
    for index, row in prev_Set_3.iterrows():
        fResults.write(
            "\t( Layer: " + str(int(row["layer"])) + " - EncounterId: " + str(int(row["EncounterId"])) + " )\n")

    fResults.write("\n")


    prev_Set_Filter = ((prev_Set['prevTime'] - prev_Set['prevFirstEndTime'] <= minIniCautionInterval) & (prev_Set['prevEndTime'] - prev_Set['prevFirstTime'] >= minPrevInterval)&(prev_Set['prevNumber'] > 1))
    prev_Set_2 = prev_Set.loc[prev_Set_Filter]

    hist_dist_data_set2 = [prev_Set_2['prevFirstRange'], \
                           prev_Set_2['prevFirstElevation'], \
                           prev_Set_2['prevFirstAzimuth'], \
                           prev_Set_2['prevEndRange'], \
                           prev_Set_2['prevEndElevation'], \
                           prev_Set_2['prevEndAzimuth'], \
                           prev_Set_2['cpaRange'], \
                           prev_Set_2['cpaElevation'], \
                           prev_Set_2['cpaAzimuth'], \
                           ]

    fResults.write("\tIntermitent preventive encounters: \n")
    for index, row in prev_Set_2.iterrows():
        fResults.write(
            "\t( Layer: " + str(int(row["layer"])) + " - EncounterId: " + str(int(row["EncounterId"])) + " )\n")


    fResults.write("\tSingle: " + str(len(prev_Set_1.index)) + "\tMultiple: " + str(len(prev_Set_2.index)) + "\n")
    mydoc.add_paragraph("\tSingle: " + str(len(prev_Set_1.index)) + "\tMultiple: " + str(len(prev_Set_2.index)))

    hist_data_set1 = [prev_Set_1['prevEndTime'] - prev_Set_1['prevTime'], \
                      prev_Set_1['prevTtHaz']]

    hist_data_set2 = [prev_Set_2['prevEndTime'] - prev_Set_2['prevFirstTime'], \
                      prev_Set_2['prevFirstTtHaz']]

    group_labels = ['Preventive Duration', 'Preventive to LoPWC']

    df3_1 = pd.concat(hist_data_set1, axis=1, keys=group_labels)
    df3_2 = pd.concat(hist_data_set2, axis=1, keys=group_labels)

    df3 = pd.concat([df3_1, df3_2], axis=0)


    if len(df3.index):

        mycsv.write(str(len(prev_Set_1.index)) + ",")
        mycsvh.write("longNoCautionPrevSingle" + ",")

        mycsv.write(str(len(prev_Set_2.index)) + ",")
        mycsvh.write("longNoCautionPrevMulti" + ",")

        mycsv.write(str(df3.iloc[:, 0].mean()) + "," + str(df3.iloc[:, 0].std()) + ",")
        mycsv.write(str(df3.iloc[:, 1].mean()) + "," + str(df3.iloc[:, 1].std()) + ",")
        mycsvh.write("prevDuration Avg" + "," + "prevDuration Std" + ",")
        mycsvh.write("timeToLoPWC Avg" + "," + "timeToLoPWC Std" + ",")


        # SHOW FIGURE
        df3_plot = [df3['Preventive Duration'], \
                     df3['Preventive to LoPWC']]

        fig = ff.create_distplot(df3_plot, group_labels, bin_size=[5, 5], show_hist=False)
        fig.update_layout(margin_b=0, margin_l=0, margin_r=0, width=1000)
        fig.update_xaxes(title_text='Time(s)', range=[0, 80])
        fig.update_layout(title_text='Preventive Caution Alert: Time (sec)')
        fig.update_layout(legend=dict(
            yanchor="top",
            y=0.99,
            xanchor="right",
            x=0.99
        ))

        filename = folderPath + "PreventiveCaution_Time_Comparison_Histogram-A.html"
        fResults.write("\tGenerating figure: " + filename + "\n")
        fig.write_html(filename)
        filename = folderPath + "PreventiveCaution_Time_Comparison_Histogram-A.png"
        fig.write_image(filename)

        mydoc.add_picture(folderPath + "PreventiveCaution_Time_Comparison_Histogram-A.png", width=docx.shared.Inches(7.5))


        df3 = pd.concat(df3_plot, axis=1, keys=group_labels)

        fig = px.ecdf(df3, group_labels, ecdfmode="reversed", marginal="box")
        fig.update_layout(margin_b= 0, margin_l= 0, margin_r= 0, width= 1000)
        fig.update_xaxes(title_text='Time(s)', range=[0, 80])
        fig.update_yaxes(title_text='CDF')
        fig.update_layout(title_text='Preventive Caution Alert: Time (sec)')
        fig.update_layout(legend=dict(
            yanchor="top",
            y=0.99,
            xanchor="right",
            x=0.99
        ))

        filename = folderPath + "PreventiveCaution_Time_Comparison-CDF.html"
        fResults.write("\tGenerating figure: " + filename + "\n")
        fig.write_html(filename)
        filename = folderPath + "PreventiveCaution_Time_Comparison-CDF.png"
        fig.write_image(filename)

        mydoc.add_picture(folderPath + "PreventiveCaution_Time_Comparison-CDF.png", width=docx.shared.Inches(7.5))


        group_labels = ['Range at Prev', 'Elevation at Prev', 'Azimuth at Prev', 'Range at End Prev', 'Elevation at End Prev', 'Azimuth at End Prev', 'Range at CPA', 'Elevation at CPA', 'Azimuth at CPA',]

        df3_1 = pd.concat(hist_dist_data_set1, axis=1, keys=group_labels)
        df3_2 = pd.concat(hist_dist_data_set2, axis=1, keys=group_labels)

        df3_dist = pd.concat([df3_1, df3_2], axis=0)


        fig = generate_dual_polar_plot(units.m_to_nm(df3_dist['Range at Prev']), df3_dist['Azimuth at Prev'], df3_dist['Elevation at Prev'], -180,180, 20, -20, "Range, Azimuth and Elevation at Preventive Alert")
        fig.update_layout(margin_b= 0, margin_l= 0, width= 1000)
        #fig.update_xaxes(title_text='Beta (deg)')
        #fig.update_layout(title_text='Conflict Azimuth Angle (deg)')


        filename = folderPath + "Preventive_Comparison_AzimuthElevation_PolarPlots.html"
        fResults.write("\tGenerating figure: " + filename + "\n")
        fig.write_html(filename)

        filename = folderPath + "Preventive_Comparison_AzimuthElevation_PolarPlots.png"
        fig.write_image(filename)

        mydoc.add_picture(folderPath + "Preventive_Comparison_AzimuthElevation_PolarPlots.png", width=docx.shared.Inches(7.5))


        fig = generate_dual_polar_plot(units.m_to_nm(df3_dist['Range at CPA']), df3_dist['Azimuth at CPA'], df3_dist['Elevation at CPA'], -180,180, 20, -20, "Range, Azimuth and Elevation at Preventive Alert")
        fig.update_layout(margin_b= 0, margin_l= 0, width= 1000)
        #fig.update_xaxes(title_text='Beta (deg)')
        #fig.update_layout(title_text='Conflict Azimuth Angle (deg)')


        filename = folderPath + "PreventiveCPA_Comparison_AzimuthElevation_PolarPlots.html"
        fResults.write("\tGenerating figure: " + filename + "\n")
        fig.write_html(filename)

        filename = folderPath + "PreventiveCPA_Comparison_AzimuthElevation_PolarPlots.png"
        fig.write_image(filename)

        mydoc.add_picture(folderPath + "PreventiveCPA_Comparison_AzimuthElevation_PolarPlots.png", width=docx.shared.Inches(7.5))



        hist1 = np.histogram(df3_dist.iloc[:, 0], bins=30)
        dist_1 = scipy.stats.rv_histogram(hist1)
        p1 = dist_1.cdf(units.nm_to_m(8.0))

        hist2 = np.histogram(df3_dist.iloc[:, 3], bins=30)
        dist_2 = scipy.stats.rv_histogram(hist2)
        p2 = dist_2.cdf(units.nm_to_m(8.0))

        fResults.write("\tRange Preventive. At preventive activation: " + str(p1 * 100) + " At preventive end: " + str(p2 * 100) + "\n")
        mycsv.write(str(p1 * 100) + "," + str(p2 * 100) + ",")
        mycsvh.write("RangePreventiveBegin" + "," + "RangeEndPreventive" + ",")

        hist1 = np.histogram(df3_dist.iloc[:, 1], bins=30)
        dist_1 = scipy.stats.rv_histogram(hist1)
        p1 = dist_1.cdf(15) - dist_1.cdf(-15)

        hist2 = np.histogram(df3_dist.iloc[:, 4], bins=80)
        dist_2 = scipy.stats.rv_histogram(hist2)
        p2 = dist_2.cdf(15) - dist_2.cdf(-15)

        fResults.write("\tElevation Preventive. At preventive activation: " + str(p1 * 100) + " At preventive end: " + str(p2 * 100) + "\n")
        mycsv.write(str(p1 * 100) + "," + str(p2 * 100) + ",")
        mycsvh.write("ElevationPreventiveBegin" + "," + "ElevationEndPreventive" + ",")

        hist1 = np.histogram(df3_dist.iloc[:, 2], bins=180)
        dist_1 = scipy.stats.rv_histogram(hist1)
        p1 = dist_1.cdf(110) - dist_1.cdf(-110)

        hist2 = np.histogram(df3_dist.iloc[:, 5], bins=90)
        dist_2 = scipy.stats.rv_histogram(hist2)
        p2 = dist_2.cdf(110) - dist_2.cdf(-110)

        fResults.write("\tAzimuth Preventive. At preventive activation: " + str(p1 * 100) + " At preventive end: " + str(p2 * 100) + "\n")
        mycsv.write(str(p1 * 100) + "," + str(p2 * 100) + ",")
        mycsvh.write("AzimuthPreventiveBegin" + "," + "AzimuthEndPreventive" + ",")

        inCoverageVector1 = (df3_dist.iloc[:, 0] <= units.nm_to_m(8.0))
        inCoverageVector2 = (df3_dist.iloc[:, 1] <= 15) & (df3_dist.iloc[:, 1] >= -15)
        inCoverageVector3 = (df3_dist.iloc[:, 2] <= 110) & (df3_dist.iloc[:, 2] >= -110)

        inCoverage1 = np.count_nonzero(inCoverageVector1 & inCoverageVector2 & inCoverageVector3)

        inCoverageVector1 = (df3_dist.iloc[:, 3] <= units.nm_to_m(8.0))
        inCoverageVector2 = (df3_dist.iloc[:, 4] <= 15) & (df3_dist.iloc[:, 4] >= -15)
        inCoverageVector3 = (df3_dist.iloc[:, 5] <= 110) & (df3_dist.iloc[:, 5] >= -110)

        inCoverage2 = np.count_nonzero(inCoverageVector1 & inCoverageVector2 & inCoverageVector3)

        fResults.write("\tIn covegare. At preventive activation: " + str(
                100 * inCoverage1 / len(df3_dist.index)) + " At caution end: " + str(
                100 * inCoverage2 / len(df3_dist.index)) + "\n")
        fResults.write("\n\n")

        mycsv.write(str(100 * inCoverage1 / len(df3_dist.index)) + "," + str(100 * inCoverage2 / len(df3_dist.index)) + ",")
        mycsvh.write("InCoveragePreventiveBegin" + "," + "InCoverageEndPreventive" + ",")



        fResults.write("\n\n")
        fResults.write("LoWC Preventive Timming Evaluation:" + "\n")


        # STEP 1 ####################################################
        fResults.write("\n\n")
        fResults.write("Preventive - Caution with LoWC - General Timing Evaluation:" + "\n")

        caution_Set_Filter = ((df['hazFirstTime'] > 0) & (((df['cautionEndTime'] - df['cautionTime'] < minCautionInterval)) & (df['cautionNumber'] > 0)))

        caution_Set_1 = df.loc[caution_Set_Filter]

        fResults.write("\tShort caution activation with LoWC: " + str(len(caution_Set_1.index)) + "\n")
        mydoc.add_paragraph("Short caution activation with LoWC: " + str(len(caution_Set_1.index)))

        mycsv.write(str(len(caution_Set_1.index)) + ",")
        mycsvh.write("shortCautionLoWC" + ",")

        prev_Set_Filter = (caution_Set_1['prevNumber'] > 0)
        prev_Set_1 = caution_Set_1.loc[prev_Set_Filter]
        mydoc.add_paragraph("With preventive: " + str(len(prev_Set_1.index)))

        mycsv.write(str(len(prev_Set_1.index)) + ",")
        mycsvh.write("prevShortCautionLoWC" + ",")

        #prev_Set_Filter = (caution_Set_1['prevEndTime'] - caution_Set_1['prevTime'] > 10)
        prev_Set_Filter = (((prev_Set_1['prevEndTime'] - prev_Set_1['prevTime'] >= minPrevInterval) & (prev_Set_1['prevNumber'] == 1)) |
                           ((prev_Set_1['prevTime'] - prev_Set_1['prevFirstEndTime'] <= minPrevDiscontinuity) & (
                                       prev_Set_1['prevEndTime'] - prev_Set_1['prevFirstTime'] >= minPrevInterval) & (prev_Set_1['prevNumber'] > 1)))
        prev_Set_11 = prev_Set_1.loc[prev_Set_Filter]

        fResults.write("\tWith long preventive before Caution: " + str(len(prev_Set_11.index)) + "\n")
        mydoc.add_paragraph("With long preventive before Caution: " + str(len(prev_Set_11.index)))

        mycsv.write(str(len(prev_Set_11.index)) + ",")
        mycsvh.write("longprevShortCautionLoWC" + ",")


        prev_Set_Filter = ((prev_Set_11['cautionTime'] - prev_Set_11['prevEndTime'] <= maxPrevCautionInterval))

        prev_Set_12 = prev_Set_11.loc[prev_Set_Filter]

        fResults.write("\tShort caution and LoWC with associated close preventive: " + str(len(prev_Set_12.index)) + "\n")
        mydoc.add_paragraph("Short caution and LoWC with associated close preventive: " + str(len(prev_Set_12.index)))

        mycsv.write(str(len(prev_Set_12.index)) + ",")
        mycsvh.write("closeprevShortCautionLoWC" + ",")


        fResults.write("\tShort Caution + Preventive LoWC encounters: \n")
        for index, row in prev_Set_11.iterrows():
            fResults.write(
                "\t( Layer: " + str(int(row["layer"])) + " - EncounterId: " + str(int(row["EncounterId"])) + " )\n")

        fResults.write("\n")



def evaluateNMACTimming(fResults, df, caption, folderPath):
    # TIME RANGE FOR NMAC VIOLATIONS

    #df.info(verbose=True)

    fResults.write("NMAC Timming Analysis:" + "\n")

    lmv_Set_Filter = ((df['hazFirstTime'] > 0) & (df['hazFirstTime'] > df['cpaTime']))
    xx_caution_Set = df.loc[lmv_Set_Filter]
    fResults.write("\tCPA occurs before WCV: " + str(len(xx_caution_Set.index)) + "\n")

    lmv_Set_Filter = ((df['hazFirstTime'] > 0))
    lmv_Set = df.loc[lmv_Set_Filter]
    fResults.write("\tTotal number of WCV violations: " + str(len(lmv_Set.index)) + "\n")

    lmv_Set_Filter = ((df['nmacTime'] > 0))
    lmv_Set = df.loc[lmv_Set_Filter]
    fResults.write("\tTotal number of NMAC violations: " + str(len(lmv_Set.index)) + "\n")

    lmv_Set_Filter = ((df['lmvFirstTime'] > 0))
    lmv_Set = df.loc[lmv_Set_Filter]
    fResults.write("\tTotal number of LMV violations: " + str(len(lmv_Set.index)) + "\n")

    lmv_Set_Filter = ((df['hazFirstTime'] > 0) & (df['lmvFirstTime'] > 0))
    lmv_Set = df.loc[lmv_Set_Filter]
    fResults.write("\tTotal number of WCV & LMV violations: " + str(len(lmv_Set.index)) + "\n")


    lmv_Set_Filter = ((lmv_Set['cautionTime'] > 0))
    lmv_Set = lmv_Set.loc[lmv_Set_Filter]

    fResults.write("\tTotal number of active Cautions with WCV and LMV: " + str(len(lmv_Set.index)) + "\n")

    lmv_Set_Filter = ((lmv_Set['cautionNumber'] == 1))
    lmv_Set = lmv_Set.loc[lmv_Set_Filter]

    fResults.write("\tTotal number of single active Cautions: " + str(len(lmv_Set.index)) + "\n")

    nmac_Set_Filter = ((df['hazFirstTime'] > 0) & (df['nmacTime'] > 0))
    nmac_Set = df.loc[nmac_Set_Filter]

    fResults.write("\tTotal number of LoWCV and NMAC: " + str(len(nmac_Set.index)) + "\n")

    hist_data = [nmac_Set['nmacTime'] - nmac_Set['hazFirstTime']]
    group_labels = ['LoWCV to NMAC']


    df3 = pd.concat(hist_data, axis=1, keys=group_labels)

    fResults.write("\tTime: LoWCV to NMAC . Mean: " + str(df3.iloc[:, 0].mean()) + " Stddev: " + str(df3.iloc[:, 0].std()) + "\n")

    fResults.write("\tNMAC encounters: \n")
    for index, row in nmac_Set.iterrows():
        fResults.write(
            "\t( Layer: " + str(int(row["layer"])) + " - EncounterId: " + str(int(row["EncounterId"])) + " )\n")

    fResults.write("\n")

    if len(lmv_Set.index) > 2:
        hist_data = [lmv_Set['lmvFirstTime'] - lmv_Set['hazFirstTime'],
                     lmv_Set['cpaTime'] - lmv_Set['lmvFirstTime'], lmv_Set['cpaTime'] - lmv_Set['nmacTime']]
        group_labels = ['LMV - WCV duration', 'LMV - CPA duration', 'NMAC - CPA duration']


        # Create distplot with custom bin_size
        fig = ff.create_distplot(hist_data, group_labels, bin_size=[10, 10, 10], histnorm='probability')
        filename = folderPath + "LMV_ActiveCaution_TimeRange_Histogram-A.html"
        fResults.write("\tGenerating figure: " + filename + "\n")
        fig.write_html(filename)
        #plot(fig, filename=filename)


        fig = generate_comparison_histogram3(lmv_Set['lmvFirstTime'] - lmv_Set['hazFirstTime'],
                     lmv_Set['cpaTime'] - lmv_Set['lmvFirstTime'], nmac_Set['cpaTime'] - nmac_Set['nmacTime'], 0, 0, 0,
                                             color1, color2, color3, 15, 'LMV - WCV duration', 'LMV - CPA duration',
                                             'NMAC - CPA duration', caption)
        filename = folderPath + "LMV_ActiveCaution_TimeRange_Histogram-B.html"
        fResults.write("\tGenerating figure: " + filename + "\n")
        fig.write_html(filename)
        #plot(fig, filename=filename)



def evaluateActiveCautionDistance(fResults, df, caption, folderPath):

    # DISTANCE FOR ACTIVE CAUTION ALERTS

    fResults.write("Active Caution Distance Evaluation:" + "\n")

    caution_Set_Filter = ((df['hazFirstTime'] > 0))
    caution_Set = df.loc[caution_Set_Filter]
    fResults.write("\tTotal number of WCV violations: " + str(len(caution_Set.index)) + "\n")

    caution_Set_Filter = ((caution_Set['cautionTime'] > 0) & (caution_Set['cautionEndTime'] > 0))
    caution_Set = caution_Set.loc[caution_Set_Filter]

    fResults.write("\tTotal number of active Cautions: " + str(len(caution_Set.index)) + "\n")

    if useNM:
        fResults.write("\tRange at First Caution: " + str(units.m_to_nm(caution_Set['cautionFirstRange'].mean())) + " Stddev: " + str(units.m_to_nm(caution_Set['cautionFirstRange'].std())) + "\n")
        fResults.write("\tRange at Caution: " + str(units.m_to_nm(caution_Set['cautionRange'].mean())) + " Stddev: " + str(units.m_to_nm(caution_Set['cautionRange'].std())) + "\n")
        fResults.write("\tRange at WCV: " + str(units.m_to_nm(caution_Set['hazFirstTimeRange'].mean())) + " Stddev: " + str(units.m_to_nm(caution_Set['hazFirstTimeRange'].std())) + "\n")
    else:
        fResults.write("\tRange at First Caution: " + str(caution_Set['cautionFirstRange'].mean()) + " Stddev: " + str(caution_Set['cautionFirstRange'].std()) + "\n")
        fResults.write("\tRange at Caution: " + str(caution_Set['cautionRange'].mean()) + " Stddev: " + str(caution_Set['cautionRange'].std()) + "\n")
        fResults.write("\tRange at WCV: " + str(caution_Set['hazFirstTimeRange'].mean()) + " Stddev: " + str(caution_Set['hazFirstTimeRange'].std()) + "\n")



    caution_Set_Filter = ((caution_Set['cautionNumber'] == 1))
    caution_Set = caution_Set.loc[caution_Set_Filter]

    fResults.write("\tTotal number of single active Cautions: " + str(len(caution_Set.index)) + "\n")

    hist_data = [caution_Set['cautionFirstRange'], caution_Set['hazFirstTimeRange']]
    group_labels = ['Distance at Caution', 'Distance at WCV']

    # Create distplot with custom bin_size
    fig = ff.create_distplot(hist_data, group_labels, bin_size=[10, 10], histnorm='probability')
    fig.update_layout(title_text="Active Caution Distance Analysis / " + caption, xaxis_title="Horizontal Distance (meters)")
    filename = folderPath + "ActiveCaution_Distance_Comparison_Histogram-A.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "ActiveCaution_Distance_Comparison_Histogram-A.png"
    fig.write_image(filename)
    #plot(fig, filename=filename)


    fig = generate_comparison_histogram2(caution_Set['cautionFirstRange'], caution_Set['hazFirstTimeRange'], 0, 0,
                                         color1, color2, 15, 'Distance at Caution', 'Distance at WCV', "Active Caution Distance Analysis / " + caption, "Horizontal Distance (meters)")
    filename = folderPath + "ActiveCaution_Distance_Comparison_Histogram-B.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "ActiveCaution_Distance_Comparison_Histogram-B.png"
    fig.write_image(filename)
    #plot(fig, filename=filename)



    caution_Set_Filter = ((df['sepTime'] > 0))
    caution_Set = caution_Set.loc[caution_Set_Filter]

    fResults.write("\tTotal number of Caution activations with associated separation violation: " + str(len(caution_Set.index)) + "\n")

    hist_data = [caution_Set['cautionFirstRange'], caution_Set['sepTimeRange']]
    group_labels = ['Distance at Caution', 'Distance at Separation']

    # Create distplot with custom bin_size
    fig = ff.create_distplot(hist_data, group_labels, bin_size=[15, 15], histnorm='probability')
    fig.update_layout(title_text="Active Separation Distance Analysis / " + caption, xaxis_title="Horizontal Distance (meters)")
    filename = folderPath + "ActiveSeparation_Distance_Comparison_Histogram-A.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "ActiveSeparation_Distance_Comparison_Histogram-A.png"
    fig.write_image(filename)


    fig = generate_comparison_histogram2(caution_Set['cautionFirstRange'], caution_Set['sepTimeRange'], 0, 0,
                                         color1, color2, 15, 'Distance at Caution', 'Distance at Separation', "Active Separation Distance Analysis / " + caption, "Horizontal Distance (meters)")
    filename = folderPath + "ActiveSeparation_Distance_Comparison_Histogram-B.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "ActiveSeparation_Distance_Comparison_Histogram-B.png"
    fig.write_image(filename)
    #plot(fig, filename=filename)




def evaluateSpuriousCautionDiscance(fResults, df, caption, folderPath):
    # TIME RANGE FOR SPURIOUS CAUTION ALERTS

    fResults.write("Spurious Caution Distance Evaluation:" + "\n")

    #df.info(verbose=True)
    caution_Set_Filter = ((((df['cautionEndTime'] - df['cautionTime'] < minCautionInterval) & (df['cautionNumber'] == 1)) |
                            ((((df['cautionTime'] - df['cautionFirstEndTime'] > minCautionInterval) & (df['cautionEndTime'] - df['cautionTime'] < minCautionInterval)) |
                            ((df['cautionTime'] - df['cautionFirstEndTime'] <= minCautionInterval) & (df['cautionEndTime'] - df['cautionFirstTime'] < minCautionInterval))) & (df['cautionNumber'] > 1))) &
                            (df['hazFirstTime'] == 0))

    caution_Set = df.loc[caution_Set_Filter]

    fResults.write("\tTotal number of spurious Caution activations: " + str(len(caution_Set.index)) + "\n")

    fResults.write("\tRange at First Caution: " + str(units.m_to_nm(caution_Set['cautionFirstRange'].mean())) + " Stddev: " + str(units.m_to_nm(caution_Set['cautionFirstRange'].std())) + "\n")
    fResults.write("\tRange at Caution: " + str(units.m_to_nm(caution_Set['cautionRange'].mean())) + " Stddev: " + str(units.m_to_nm(caution_Set['cautionRange'].std())) + "\n")

    hist_data = [caution_Set['cautionRange'], caution_Set['cautionEndRange']]
    group_labels = ['Distance at Caution', 'Distance at End Caution']

    # Create distplot with custom bin_size
    fig = ff.create_distplot(hist_data, group_labels, bin_size=[15, 15], histnorm='probability')
    fig.update_layout(title_text="Spurious Caution Distance Analysis / " + caption,
                      xaxis_title="Horizontal Distance (meters)")
    filename = folderPath + "SpuriousCaution_Distance_Histogram-A.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "SpuriousCaution_Distance_Histogram-B.png"
    fig.write_image(filename)
    #plot(fig, filename=filename)


    fig = generate_comparison_histogram2(caution_Set['cautionRange'], caution_Set['cautionEndRange'], 0, 0,
                                         color1, color2, 15, 'Distance at Caution', 'Distance at End Caution', "Spurious Caution Distance Analysis / " + caption, "Horizontal Distance (m)")
    filename = folderPath + "SpuriousCaution_Distance_Comparison_Histogram-B.html"
    fResults.write("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = folderPath + "SpuriousCaution_Distance_Comparison_Histogram-B.png"
    fig.write_image(filename)
    #plot(fig, filename=filename)




def generate_individual_histogram(df, lowerLimit, upperLimit, color, infoType, binsize, zone):
    fig = go.FigureWidget(
        [go.Histogram(
            x=df,
            histnorm='percent',
            name=infoType + ' at ' + zone,  # name used in legend and hover labels
            xbins=dict(
                start=lowerLimit,
                end=upperLimit,
                size=binsize
            ),
            marker_color=color,
            opacity=0.75,
            texttemplate="%{y}",
            textfont_size=8
        )],
        go.Layout(
            title=infoType + ' for ' + zone + ' Encounters',
            xaxis={'range': [lowerLimit, upperLimit],
                   'title': infoType},
            yaxis={'title': '% total encounters'},
            bargap=0.1))
    hist = fig.data[0]

    # Install xaxis zoom callback
    def handle_zoom(xaxis, xrange):
        filtered_x = df[np.logical_and(xrange[0] <= df, df <= xrange[1])]
        hist.x = filtered_x

    fig.layout.xaxis.on_change(handle_zoom, 'range')
    return fig




def generate_comparison_histogram1(df1, lowerLimit1, color1,  binsize, firstSet, caption, xaxis):
    trace1 = go.Histogram(
        x=df1,
        histnorm='percent',
        name=firstSet,  # name used in legend and hover labels
        xbins=dict(
            start=lowerLimit1,
#            end=upperLimit1,
            size=binsize
        ),
        marker_color=color1,
        opacity=0.75,
        texttemplate="%{y}",
        textfont_size=8
    )

    fig = go.FigureWidget(
        data=[trace1],
        layout=go.Layout(
            title=caption,
            xaxis={'title': xaxis},
            yaxis={'title': '% total encounters'},
            bargap=0.1
        )
    )

    hist1 = fig.data[0]

    # Install xaxis zoom callback
    def handle_zoom(xaxis, xrange):
        filtered_x1 = df1[np.logical_and(xrange[0] <= df1, df1 <= xrange[1])]
        hist1.x = filtered_x1

    fig.layout.xaxis.on_change(handle_zoom, 'range')

    return fig



def generate_comparison_histogram2(df1, df2, lowerLimit1, lowerLimit2, color1, color2,
                                   binsize, firstSet, secondSet, caption, xaxes):
    trace1 = go.Histogram(
        x=df1,
        histnorm='percent',
        cumulative_enabled=True,
        name=firstSet,  # name used in legend and hover labels
        xbins=dict(
            start=lowerLimit1,
#            end=upperLimit1,
            size=binsize
        ),
        marker_color=color1,
        opacity=0.75,
        texttemplate="%{y}",
        textfont_size=8
    )

    trace2 = go.Histogram(
        x=df2,
        histnorm='percent',
        cumulative_enabled=True,
        name=secondSet,
        xbins=dict(
            start=lowerLimit2,
#            end=upperLimit2,
            size=binsize
        ),
        marker_color=color2,
        opacity=0.75,
        texttemplate="%{y}",
        textfont_size=8
    )

    fig = go.FigureWidget(
        data=[trace1, trace2],
        layout=go.Layout(
            title=caption,
            xaxis={'title': xaxes},
            yaxis={'title': '% total encounters'},
            bargap=0.1
        )
    )

    hist1 = fig.data[0]
    hist2 = fig.data[1]

    # Install xaxis zoom callback
    def handle_zoom(xaxis, xrange):
        filtered_x1 = df1[np.logical_and(xrange[0] <= df1, df1 <= xrange[1])]
        hist1.x = filtered_x1

        filtered_x2 = df2[np.logical_and(xrange[0] <= df2, df2 <= xrange[1])]
        hist2.x = filtered_x2

    fig.layout.xaxis.on_change(handle_zoom, 'range')

    return fig



def generate_comparison_histogram3(df1, df2, df3, lowerLimit1, lowerLimit2, lowerLimit3, color1, color2, color3,
                                   binsize, firstSet, secondSet, thirdSet, caption):
    trace1 = go.Histogram(
        x=df1,
        histnorm='percent',
        name=firstSet,  # name used in legend and hover labels
        xbins=dict(
            start=lowerLimit1,
#            end=upperLimit1,
            size=binsize
        ),
        marker_color=color1,
        opacity=0.75,
        texttemplate="%{y}",
        textfont_size=8
    )

    trace2 = go.Histogram(
        x=df2,
        histnorm='percent',
        name=secondSet,
        xbins=dict(
            start=lowerLimit2,
#            end=upperLimit2,
            size=binsize
        ),
        marker_color=color2,
        opacity=0.75,
        texttemplate="%{y}",
        textfont_size=8
    )

    trace3 = go.Histogram(
        x=df3,
        histnorm='percent',
        name=thirdSet,
        xbins=dict(
            start=lowerLimit3,
            #            end=upperLimit2,
            size=binsize
        ),
        marker_color=color3,
        opacity=0.75,
        texttemplate="%{y}",
        textfont_size=8
    )

    fig = go.FigureWidget(
        data=[trace1, trace2, trace3],
        layout=go.Layout(
            title=caption,
            xaxis={'title': 'TimeRange (sec)'},
            yaxis={'title': '% total encounters'},
            bargap=0.1
        )
    )

    hist1 = fig.data[0]
    hist2 = fig.data[1]
    hist3 = fig.data[2]

    # Install xaxis zoom callback
    def handle_zoom(xaxis, xrange):
        filtered_x1 = df1[np.logical_and(xrange[0] <= df1, df1 <= xrange[1])]
        hist1.x = filtered_x1

        filtered_x2 = df2[np.logical_and(xrange[0] <= df2, df2 <= xrange[1])]
        hist2.x = filtered_x2

        filtered_x3 = df3[np.logical_and(xrange[0] <= df3, df3 <= xrange[1])]
        hist3.x = filtered_x3

    fig.layout.xaxis.on_change(handle_zoom, 'range')

    return fig


def generate_2D_Contour_Histogram(df1, df2, xaxisTitle, yaxisTitle, Title):
    fig = go.Figure()
    fig.add_trace(go.Histogram2dContour(
        x=df1,
        y=df2,
        colorscale='Hot',
        reversescale=True,
        xaxis='x',
        yaxis='y'
    ))
    fig.add_trace(go.Scatter(
        x=df1,
        y=df2,
        xaxis='x',
        yaxis='y',
        mode='markers',
        marker=dict(
            color='rgba(0,0,0,0.3)',
            size=3
        )
    ))
    fig.add_trace(go.Histogram(
        y=df2,
        histnorm='percent',
        xaxis='x2',
        marker=dict(
            color='rgba(0,0,0,1)'
        )
    ))
    fig.add_trace(go.Histogram(
        x=df1,
        histnorm='percent',
        yaxis='y2',
        marker=dict(
            color='rgba(0,0,0,1)'
        )
    ))

    fig.update_layout(
        autosize=False,
        xaxis=dict(
            title=xaxisTitle,
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        yaxis=dict(
            title=yaxisTitle,
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        xaxis2=dict(
            zeroline=False,
            domain=[0.85, 1],
            showgrid=True
        ),
        yaxis2=dict(
            zeroline=False,
            domain=[0.85, 1],
            showgrid=True
        ),
        height=1000,
        width=1000,
        bargap=0.1,
        hovermode='closest',
        showlegend=True,
        title=Title
    )

    return fig


def generate_dual_polar_plot(rang, secondType, thirdType, upperLimit1, lowerLimit1, upperLimit2, lowerLimit2, title):
    fig = make_subplots(rows=1, cols=2, specs=[[{'type': 'polar'}] * 2])

    fig.add_trace(go.Scatterpolar(r=rang, theta=secondType, name='Azimuth vs Range'), 1, 1)
    fig.add_trace(go.Scatterpolar(r=rang, theta=thirdType, name='Elevation vs Range'), 1, 2)

    # Same data for the two Scatterpolar plots, we will only change the sector in the layout
    fig.update_traces(mode="markers",
                      #line_color="magenta",
                      marker=dict(
                          #color="royalblue",
                          symbol="square",
                          size=4
                      ))

    # The sector is [0, 360] by default, we update it for the first plot only
    fig.update_layout(
        title= title,
        showlegend=True,
        polar=dict(  # setting parameters for the second plot would be polar2=dict(...)
            sector=[upperLimit1, lowerLimit1],
        ),
        polar2=dict(
            sector=[upperLimit2, lowerLimit2]
        ),
        legend = dict(
            yanchor="top",
            y=0.99,
            xanchor="right",
            x=0.99
        ))
    return fig
