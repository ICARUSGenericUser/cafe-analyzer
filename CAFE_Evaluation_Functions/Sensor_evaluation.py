import plotly.graph_objects as go
import numpy as np
from plotly.offline import plot
import plotly.figure_factory as ff
from plotly.subplots import make_subplots

color1 = '#EB89B5'
color2 = '#330C73'


def evaluateSensorCoverage(early, late, sensor, firstZone, secondZone, folderPath):
    # RANGE EARLY ZONE HISTOGRAM
    ####################################################################################################################
    fig = generate_individual_histogram(early["Range"], sensor.rangeMin, sensor.rangeMax, color1, "Range", 5, firstZone)

    filename = folderPath + "AircraftRange_" + firstZone + "_Histogram.html"
    print("Generating figure: " + filename)
    plot(fig, filename=filename)

    # RANGE LATE ZONE HISTOGRAM
    ####################################################################################################################

    fig = generate_individual_histogram(late["Range"], sensor.rangeMin, sensor.rangeMax, color2, "Range", 5, secondZone)

    filename = folderPath + "AircraftRange_" + secondZone + "_Histogram.html"
    print("Generating figure: " + filename)
    plot(fig, filename=filename)

    # RANGE EARLY AND LATE COMPARISON HISTOGRAM
    ####################################################################################################################

    hist_data = [early["Range"], late["Range"]]
    group_labels = ['Range at ' + firstZone, 'Range at ' + secondZone]

    # Create distplot with custom bin_size
    fig = ff.create_distplot(hist_data, group_labels, bin_size=[5, 5], histnorm='probability')
    plot(fig, filename=folderPath + "AircraftDensityComparison_Range_Histogram.html")

    fig = generate_comparison_histogram(early["Range"], late["Range"], sensor.rangeMin, sensor.rangeMax,
                                        sensor.rangeMin, sensor.rangeMax,
                                        color1, color2, "Range", 5, firstZone, secondZone)

    filename = folderPath + "AircraftRangeComparison_Histogram.html"
    print("Generating figure: " + filename)
    plot(fig, filename=filename)

    # RANGE AND AZIMUTH EARLY COMPARISON HISTOGRAM
    ####################################################################################################################

    fig = generate_2D_Contour_Histogram(early["Range"], early["Azimuth"], 'Range (m)', 'Azimuth (degrees)',
                                  "Range and Azimuth at "+firstZone+" Comparison")

    filename = folderPath + "AircraftAzimuthRange2DComparison_Histogram.html"
    print("Generating figure: " + filename)
    plot(fig, filename=filename)

    # AZIMUTH EARLY ZONE HISTOGRAM
    ####################################################################################################################

    fig = generate_individual_histogram(early["Azimuth"], -sensor.azimuthMax, sensor.azimuthMax, color1,
                                        "Azimuth", 2, firstZone)

    filename = folderPath + "AircraftAzimuth_" + firstZone + "_Histogram.html"
    print("Generating figure: " + filename)
    plot(fig, filename=filename)

    # AZIMUTH LATE ZONE HISTOGRAM
    ####################################################################################################################

    fig = generate_individual_histogram(late["Azimuth"], -sensor.azimuthMax, sensor.azimuthMax, color2,
                                        "Azimuth", 2, secondZone)

    filename = folderPath + "AircraftAzimuth_" + secondZone + "_Histogram.html"
    print("Generating figure: " + filename)
    plot(fig, filename=filename)

    # AZIMUTH EARLY AND LATE COMPARISON HISTOGRAM
    ####################################################################################################################

    hist_data = [early["Azimuth"], late["Azimuth"]]
    group_labels = ['Azimuth at ' + firstZone, 'Azimuth at ' + secondZone]

    # Create distplot with custom bin_size
    fig = ff.create_distplot(hist_data, group_labels, bin_size=[2, 2], histnorm='probability')
    plot(fig, filename=folderPath + "AircraftDensityComparison_Azimuth_Histogram.html")

    fig = generate_comparison_histogram(early["Azimuth"], late["Azimuth"], -sensor.azimuthMax,
                                        sensor.azimuthMax, -sensor.azimuthMax, sensor.azimuthMax, color1, color2,
                                        "Azimuth", 2, firstZone, secondZone)

    filename = folderPath + "AircraftAzimuthComparison_Histogram.html"
    print("Generating figure: " + filename)
    plot(fig, filename=filename)

    # ELEVATION EARLY ZONE HISTOGRAM
    ####################################################################################################################

    fig = generate_individual_histogram(early["Elevation"], -sensor.elevationDownMax, sensor.elevationUpMax, color1,
                                        "Elevation", .5, firstZone)

    filename = folderPath + "AircraftElevation_" + firstZone + "_Histogram.html"
    print("Generating figure: " + filename)
    plot(fig, filename=filename)

    # ELEVATION LATE NMAC HISTOGRAM
    ####################################################################################################################

    fig = generate_individual_histogram(late["Elevation"], -sensor.elevationDownMax, sensor.elevationUpMax, color2,
                                        "Elevation", .5, secondZone)

    filename = folderPath + "AircraftElevation_" + secondZone + "_Histogram.html"
    print("Generating figure: " + filename)
    plot(fig, filename=filename)

    # ELEVATION EARLY AND LATE COMPARISON HISTOGRAM
    ####################################################################################################################

    hist_data = [early["Elevation"], late["Elevation"]]
    group_labels = ['Azimuth at ' + firstZone, 'Azimuth at ' + secondZone]

    # Create distplot with custom bin_size
    fig = ff.create_distplot(hist_data, group_labels, bin_size=[0.5, 0.5], histnorm='probability')
    plot(fig, filename=folderPath + "AircraftDensityComparison_Elevation_Histogram.html")

    fig = generate_comparison_histogram(early["Elevation"], late["Elevation"], -sensor.elevationDownMax,
                                        sensor.elevationUpMax, -sensor.elevationDownMax, sensor.elevationUpMax, color1,
                                        color2, "Elevation", .5, firstZone, secondZone)

    filename = folderPath + "AircraftElevationComparison_Histogram.html"
    print("Generating figure: " + filename)
    plot(fig, filename=filename)

    # AZIMUTH AND ELEVATION POLAR PLOTS
    ####################################################################################################################

    fig = generate_dual_polar_plot(early["Range"], early["Azimuth"], early["Elevation"], -sensor.azimuthMax,
                                   sensor.azimuthMax, sensor.elevationUpMax*2, -sensor.elevationDownMax*2)

    filename = folderPath + "AircraftAzimuthElevation_PolarPlots.html"
    print("Generating figure: " + filename)
    plot(fig, filename=filename)

    return 0


def generate_individual_histogram(df, lowerLimit, upperLimit, color, infoType, binsize, zone):
    fig = go.FigureWidget(
        [go.Histogram(
            x=df,
            histnorm='percent',
            name=infoType + ' at ' + zone,  # name used in legend and hover labels
            xbins=dict(
                start=lowerLimit,
                end=upperLimit,
                size=binsize
            ),
            marker_color=color,
            opacity=0.75,
            texttemplate="%{y}",
            textfont_size=8
        )],
        go.Layout(
            title=infoType + ' for ' + zone + ' Encounters',
            xaxis={'range': [lowerLimit, upperLimit],
                   'title': infoType},
            yaxis={'title': '% total encounters'},
            bargap=0.1))
    hist = fig.data[0]

    # Install xaxis zoom callback
    def handle_zoom(xaxis, xrange):
        filtered_x = df[np.logical_and(xrange[0] <= df, df <= xrange[1])]
        hist.x = filtered_x

    fig.layout.xaxis.on_change(handle_zoom, 'range')
    return fig


def generate_comparison_histogram(df1, df2, lowerLimit1, upperLimit1, lowerLimit2, upperLimit2, color1, color2,
                                  infoType, binsize, firstZone, secondZone):
    trace1 = go.Histogram(
        x=df1,
        histnorm='percent',
        name=infoType + ' at ' + firstZone,  # name used in legend and hover labels
        xbins=dict(
            start=lowerLimit1,
            end=upperLimit1,
            size=binsize
        ),
        marker_color=color1,
        opacity=0.75,
        texttemplate="%{y}",
        textfont_size=8
    )

    trace2 = go.Histogram(
        x=df2,
        histnorm='percent',
        name=infoType + ' at ' + secondZone,
        xbins=dict(
            start=lowerLimit2,
            end=upperLimit2,
            size=binsize
        ),
        marker_color=color2,
        opacity=0.75,
        texttemplate="%{y}",
        textfont_size=8
    )

    fig = go.FigureWidget(
        data=[trace1, trace2],
        layout=go.Layout(
            title=infoType + ' Comparison for early and late zone Encounters',
            xaxis={'range': [lowerLimit1, upperLimit1], 'title': infoType},
            yaxis={'title': '% total encounters'},
            bargap=0.1
        )
    )

    hist1 = fig.data[0]
    hist2 = fig.data[1]

    # Install xaxis zoom callback
    def handle_zoom(xaxis, xrange):
        filtered_x1 = df1[np.logical_and(xrange[0] <= df1, df1 <= xrange[1])]
        hist1.x = filtered_x1

        filtered_x2 = df2[np.logical_and(xrange[0] <= df2, df2 <= xrange[1])]
        hist2.x = filtered_x2

    fig.layout.xaxis.on_change(handle_zoom, 'range')

    return fig


def generate_2D_Contour_Histogram(df1, df2, xaxisTitle, yaxisTitle, Title):
    fig = go.Figure()
    fig.add_trace(go.Histogram2dContour(
        x=df1,
        y=df2,
        colorscale='Hot',
        reversescale=True,
        xaxis='x',
        yaxis='y'
    ))
    fig.add_trace(go.Scatter(
        x=df1,
        y=df2,
        xaxis='x',
        yaxis='y',
        mode='markers',
        marker=dict(
            color='rgba(0,0,0,0.3)',
            size=3
        )
    ))
    fig.add_trace(go.Histogram(
        y=df2,
        histnorm='percent',
        xaxis='x2',
        marker=dict(
            color='rgba(0,0,0,1)'
        )
    ))
    fig.add_trace(go.Histogram(
        x=df1,
        histnorm='percent',
        yaxis='y2',
        marker=dict(
            color='rgba(0,0,0,1)'
        )
    ))

    fig.update_layout(
        autosize=False,
        xaxis=dict(
            title=xaxisTitle,
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        yaxis=dict(
            title=yaxisTitle,
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        xaxis2=dict(
            zeroline=False,
            domain=[0.85, 1],
            showgrid=True
        ),
        yaxis2=dict(
            zeroline=False,
            domain=[0.85, 1],
            showgrid=True
        ),
        height=1000,
        width=1000,
        bargap=0.1,
        hovermode='closest',
        showlegend=True,
        title=Title
    )

    return fig


def generate_dual_polar_plot(rang, secondType, thirdType, upperLimit1, lowerLimit1, upperLimit2, lowerLimit2):
    fig = make_subplots(rows=1, cols=2, specs=[[{'type': 'polar'}] * 2])

    fig.add_trace(go.Scatterpolar(r=rang, theta=secondType), 1, 1)
    fig.add_trace(go.Scatterpolar(r=rang, theta=thirdType), 1, 2)

    # Same data for the two Scatterpolar plots, we will only change the sector in the layout
    fig.update_traces(mode="markers",
                      line_color="magenta",
                      marker=dict(
                          color="royalblue",
                          symbol="square",
                          size=4
                      ))

    # The sector is [0, 360] by default, we update it for the first plot only
    fig.update_layout(
        title='',
        showlegend=True,
        polar=dict(  # setting parameters for the second plot would be polar2=dict(...)
            sector=[upperLimit1, lowerLimit1],
        ),
        polar2=dict(
            sector=[upperLimit2, lowerLimit2]
        ))

    return fig
