import numpy as np
import math as math
import Encounter.units as units

def normalizeAngle(angle):
    while angle < 0:
        angle += 360
    while angle > 360:
        angle -= 360
    return angle

# Normalize the angle within the 0 - 360 range
def normalize(angle):
    angle = (angle % 360)
    if angle <= 0:
        angle = angle + 360
    return angle % 360


# Normalize the angle within the - 180 < - 0 -> 180 range
def normalizeToAcute(angle):
    angle = normalize(angle)
    if angle > 180:
        angle = angle - 360
    return angle


# Returns absolute distance( in degrees) between two angles.
def absDistance(orig, dest, turnLeft):
    orig = normalize(orig)
    dest = normalize(dest)

    angle = 0

    if turnLeft:
        if dest <= orig:
            angle = orig - dest
        else:
            angle = 360 - (dest - orig)
    else:
        if dest >= orig:
            angle = dest - orig
        else:
            angle = 360 - (orig - dest)

    return angle


# Returns relative distance( in degrees) between two angles.
def distance(orig, dest):
    orig = normalize(orig)
    dest = normalize(dest)

    return normalize(dest - orig)


def getDifference(b1, b2):
    r = (b2 - b1) % 360.0
    # Python modulus has same sign as divisor, which is positive here,
    # so no need to consider negative case
    if r >= 180.0:
        r -= 360.0
    return normalizeAngle(r)


def computeBearingRelative(centre, point):
    bearing = (180 / math.pi) * math.atan2(centre[1] - point[1], centre[0] - point[0])
    return bearing


def computeBearingAbsolute(point):
    bearing = normalizeAngle(90 - (180 / math.pi) * math.atan2(point[1], point[0]))
    return bearing


def angularOffset(centre, point, trkDeg):
    trkRad = units.deg_to_rad(trkDeg)
    s = math.sin(trkRad)
    c = math.cos(trkRad)
    v2 = point - centre
    return np.array([v2[0] * c + v2[1] * s, -v2[0] * s + v2[1] * c])


def angularOffset(v2, trkDeg):
    trkRad = units.deg_to_rad(trkDeg)
    s = math.sin(trkRad)
    c = math.cos(trkRad)
    return np.array([v2[0] * c + v2[1] * s, -v2[0] * s + v2[1] * c])


# Returns 1 if otherAngle is to the right of sourceAngle,
#         sourceAngle and otherAngle should be in the range -180 to 180
#         0 if the angles are identical
#         -1 if otherAngle is to the left of sourceAngle
def compareAngles(sourceAngle, otherAngle):

    difference = otherAngle - sourceAngle

    if difference < -180.0:
        difference += 360.0
    if difference > 180.0:
        difference -= 360.0

    if difference > 0.0:
        return 1
    if difference < 0.0:
        return -1

    return 0


def getVelocityVector2D(speed, angle):
    radians = units.deg_to_rad(angle)
    return np.array([speed * math.sin(radians), speed * math.cos(radians)])
