
HAZ volume definition:
======================

Initially, the vertical size of the DWC volume was chosen to be 700 ft (see 2014 SARP presentation).
However, there were comments that in certain situations it was ok to pass at 500 ft separation, so the 700 ft would
result in undesired DAA alerts if traffic is predicted to pass with 500 or 600 ft separation.
So they reduced the vertical size of the HAZ to 450 ft and introduced the Preventive Alert. This alert is to be
declared if horizontally the HAZ will be penetrated but the predicted vertical separation remains between 450 and 700 ft.
The Preventive Alert is to inform the pilot not to maneuver vertically. E.g. in a head-on encounter with a predicted
vertical separation of 600 ft the pilot will see the traffic symbol change to the Preventive state, but there will be
no guidance bands intersection current track or altitude. There will be a so-called peripheral altitude band just below
or above current altitude indicating the proximity of the HAZ.

Note that the values of DMOD and tau were chosen based on the criterion that 1.5% of the unmitigated violations of the
HAZ would result in an NMAC (using the large encounter set in numerical simulations). Due to the reduction of 700 ft
to 450 ft, the 1.5% increased to 2.2% and this was deemed acceptable by the committee.


Encounter Color Code:
=====================

NMAC Violation:                     marker='o', markersize=4, color='red'
TCAS RA:                            marker='o', markersize=4, color='violet'
WCV Volume Violation:               marker='.', markersize=4, color='red'
HAZ RWC Warning Alert:              marker='.', markersize=4, color='orange'
HAZ RWC Corrective Alert:           marker='.', markersize=4, color='yellow'
HAZ RWC Preventive Alert:           marker='.', markersize=4, color='green'


Individual Encounter Data:
==========================
EncounterId, aircraft1Class, aircraft2Class, activeConflict,
hazViolated, isFirstHazViolation, timeHazViolation, HazSLoWC, HazRangePen, HazHMDPen, HazVertPen,
nmacViolated, timeNmacViolation, nmacWSR, nmacHMD, nmacVMD,
prevViolationType, prevViolationTime, prevThresholdTimeLower, prevThresholdTimeUpper, prevHazLevel??, prevRange, prevBearing prevElevation,
corrViolationType, corrViolationTime, corrThresholdTimeLower, corrThresholdTimeUpper, corrHazLevel??, corrRange, corrBearing corrElevation,
warnViolationType, warnViolationTime, warnThresholdTimeLower, warnThresholdTimeUpper, warnHazLevel??, warnRange, warnBearing warnElevation,
tcasViolated, timeTcasViolation, other



EncounterId,            Encounter identificator
Layer,
Approach Angle,
AC1 Mode A,
AC1 Altitude,
AC1 Vertical,
AC1 Speed,
AC2 Mode A,
AC2 Altitude,
AC2 Vertical,
AC2 Speed,

aircraft1Class,         Aircraft ownship class according to CAFE definition
aircraft2Class,         Aircraft intruder class according to CAFE definition
activeConflict,         Ownship experiences some type of conflict

cpaTime,                Time at CAFE first CPA
cpaBeta,                Beta at CAFE first CPA
cpaRange,               Range at CAFE first CPA
cpaAzimuth,             Azimuth at CAFE first CPA
cpaElevation,           Elevation at CAFE first CPA
cpaDz,                  Dz at CAFE first CPA

minHDistTime
minHDistBeta
minHDistRange
minHDistAzimuth
minHDistElevation
minHDistDz

nmacViolated,           Ownship violates the intruders NMAC.
nmacFirstTime,          Simulation time at which NMAC volume is violated for the first time
nmacTime,               Simulation time at which NMAC is violated with the minimum WSR
nmacWSR,                Minimum Weighted Slant Range (WSR) within the NMAC violation
nmacHMD,                HMD at last NMAC violation
nmacVMD,                VMD at last NMAC violation
nmacFirstTimeBeta,      Beta when NMAC is first violated
nmacFirstTimeRange,     Range when NMAC is first violated
nmacFirstTimeAzimuth,   Azimuth when NMAC is first violated
nmacFirstTimeElevation, Elevation when NMAC is first violated
nmacFirstTimeDz,        Dz when NMAC is first violated
nmacTimeBeta,           Beta when NMAC is violated with minimum WSR
nmacTimeRange,          Range when NMAC is violated with minimum WSR
nmacTimeAzimuth,        Azimuth when NMAC is violated with minimum WSR
nmacTimeElevation,      Elevation when NMAC is violated with minimum WSR
nmacTimeDz,             Dz when NMAC is violated with minimum WSR

hazViolated,            WCV also known as hazard volume is violated
hazFirstTime,           Time at which the hazard volume is violated for the first time
hazFirstTimeSLoWC
hazFirstTimeRangePen
hazFirstTimeHMDPen
hazFirstTimeVertPen
hazFirstTcpa,           TCPA when hazard volume is violated for the first time
hazFirstDcpa            DCPA when hazard volume is violated for the first time
haz_SLoWC_time          SLoWC when hazard volume is violated for the first time
hazSLoWC,               Maximum SLoWC with TCPA being positive
hazRangePen,            Maximum RangePen with TCPA being positive
hazHMDPen,              Maximum HMDPen with TCPA being positive
hazVertPen,             Maximum VertPen with TCPA being positive
hazTcpaTime,
hazTcpa,                TCPA when SLoWC is maximized with TCPA being positive
hazDcpa,                DCPA when SLoWC is maximized with TCPA being positive
minSlatTime,
minSlatRange

hazFirstTimeRange,      Range when hazard volume is violated for the first time
hazFirstTimeAzimuth,    Azimuth when hazard volume is violated for the first time
hazFirstTimeElevation,  Elevation when hazard volume is violated for the first time
hazFirstTimeDz,         Dz when hazard volume is violated for the first time
hazTimeRange,           Range when SLoWC is maximized with TCPA being positive
hazTimeAzimuth,         Azimuth when SLoWC is maximized with TCPA being positive
hazTimeElevation,       Elevation when SLoWC is maximized with TCPA being positive
hazTimeDz,              Dz when SLoWC is maximized with TCPA being positive

hazSLoWCTimeBeta
hazSLoWCTimeRange
hazSLoWCTimeAzimuth
hazSLoWCTimeElevation
hazSLoWCTimeDz
hazTcpaTimeBeta
hazTcpaTimeRange
hazTcpaTimeAzimuth
hazTcpaTimeElevation
hazTcpaTimeDz
hazSlatTimeBeta
hazSlatTimeRange
hazSlatTimeAzimuth
hazSlatTimeDz

cautionNumber",
cautionFirstTime",
cautionFirstTtHaz",
cautionFirstType",
cautionTime",
cautionTtHaz",
cautionType",
    "cautionFirstBeta",
    "cautionFirstRange",
    "cautionFirstAzimuth",
    "cautionFirstElevation",
    "cautionFirstDz",
    "cautionBeta",
    "cautionRange",
    "cautionAzimuth",
    "cautionElevation",
    "cautionDz",
    "cautionFirstEndTime",
    "cautionFirstEndBeta",
    "cautionFirstEndRange",
    "cautionFirstEndAzimuth",
    "cautionFirstEndElevation",
    "cautionFirstEndDz",
    "cautionEndTime",
    "cautionEndBeta",
    "cautionEndRange",
    "cautionEndAzimuth",
    "cautionEndElevation",
    "cautionEndDz",

sepViolated,            Separation volume is violated
sepTime,                Time at which the separation is violated for the first time
sepTimeRange,           Range when the separation is violated for the first time
sepTimeAzimuth,         Azimuth when the separation is violated for the first time
sepTimeElevation,       Elevation when the separation is violated for the first time
sepTimeDz,              Dz when the separation is violated for the first time


Encounter Resume Data:
======================

nConflicts:                     Number of conflicts
nNMAC:                          Number of NMAC violations
nHazard:                        Number of Hazard volume violations
nWarning:                       Number of Warning threshold violations
nCorrective:                    Number of Corrective threshold violations
nPreventive:                    Number of Preventive threshold violations

nEarlyPreventive:               Number of early preventives
EarlyPreventiveAvTime:          Average time for the early preventives
nAveragePreventive:             Number of average preventives
AveragePreventiveAvTime:        Average time for the average preventives
nLatePreventive:                Number of late preventives
LatePreventiveAvTime:           Average time for the late preventives
nOnlyPreventive:                Number of only preventive activations

nEarlyCorrective:               Number of early correctives
EarlyCorrectiveAvTime:          Average time for early correctives
nAverageCorrective:             Number of average correctives
AverageCorrectiveAvTime:        Average time for average correctives
nLateCorrective:                Number of late correctives
LateCorrectiveAvTime:           Average time for late correctives

nWarningNCorrective:                Number of warnings without corrective activation
WarningNCorrectiveAvTime:           Average time for warning activations without corrective activations
nEarlyWarningNCorrective:           Number of early warning activations without corrective activations
EarlyWarningNCorrectiveAvTime:      Average time for early warning activations without corrective activations
nAverageWarningNCorrective:         Number of average warning activations without corrective activations
AverageWarningNCorrectiveAvTime:    Average time for average warning activations without corrective activations

