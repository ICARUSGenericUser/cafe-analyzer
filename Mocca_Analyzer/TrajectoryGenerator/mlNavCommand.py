import math as math
from Util import angles as ang
from Mocca_Analyzer.TrajectoryGenerator import mlNavCommandType as mnct


class mlNavCommand():
  def __init__(self):
    self.command = mnct.mlNavCommandType.MAV_CMD_NAV_UNKNOWN
    self.param1 = 0.0
    self.param2 = 0.0
    self.param3 = 0.0
    self.param4 = 0.0
    self.param5 = 0.0
    self.param6 = 0.0
    self.param7 = 0.0


  def setWaypoint(self, lat, lon, alt, hold):
    self.command = mnct.mlNavCommandType.MAV_CMD_NAV_WAYPOINT
    self.param1 = hold
    self.param2 = 0.0
    self.param3 = 0.0
    self.param4 = 0.0
    self.param5 = lat
    self.param6 = lon
    self.param7 = alt


  def setWaypointExt(self, lat, lon, alt, hold, acceptRadius):
    self.command = mnct.mlNavCommandType.MAV_CMD_NAV_WAYPOINT
    self.param1 = hold
    self.param2 = acceptRadius
    self.param3 = 0.0
    self.param4 = 0.0
    self.param5 = lat
    self.param6 = lon
    self.param7 = alt


  def setRtl(self, lat, lon, alt, hold):
    self.command = mnct.mlNavCommandType.MAV_CMD_NAV_RETURN_TO_LAUNCH
    self.param1 = 0.0
    self.param2 = 0.0
    self.param3 = 0.0
    self.param4 = 0.0
    self.param5 = 0.0
    self.param6 = 0.0
    self.param7 = 0.0


  def setLand(self, lat, lon, alt):
    self.command = mnct.mlNavCommandType.MAV_CMD_NAV_LAND
    self.param1 = 0.0
    self.param2 = 0.0
    self.param3 = 0.0
    self.param4 = 0.0
    self.param5 = lat
    self.param6 = lon
    self.param7 = alt


  def setTakeoff(self, lat, lon, alt):
    self.command = mnct.mlNavCommandType.MAV_CMD_NAV_TAKEOFF
    self.param1 = 0.0
    self.param2 = 0.0
    self.param3 = 0.0
    self.param4 = 0.0
    self.param5 = lat
    self.param6 = lon
    self.param7 = alt


  def setLoiterToAlt(self, lat, lon, alt, heading):
    self.command = mnct.mlNavCommandType.MAV_CMD_NAV_LOITER_TO_ALT
    self.param1 = heading
    self.param2 = 0.0
    self.param3 = 0.0
    self.param4 = 0.0
    self.param5 = lat
    self.param6 = lon
    self.param7 = alt

  # 1: Angle target angle, 0 is north deg
  # 2: Angular Speed angular speed deg / s
  # 3: Direction direction: -1: counter clockwise, 1: clockwise  min: -1 max: 1 increment: 2
  # 4: Relative 0: absolute angle, 1: relative offset min: 0 max: 1 increment: 1

  def setSetConditionYaw(self, angle, angularSpeed, direction, mode):
    self.command = mnct.mlNavCommandType.MAV_CMD_CONDITION_YAW
    self.param1 = angle
    self.param2 = angularSpeed
    self.param3 = direction
    self.param4 = mode
    self.param5 = 0.0
    self.param6 = 0.0
    self.param7 = 0.0


  # 1: Speed Type Speed type(0 = Airspeed, 1 = Ground Speed, 2 = Climb Speed, 3 = Descent Speed)    min: 0 max: 3 increment: 1
  # 2: Speed Speed(-1 indicates no change)    min: -1 m / s
  # 3: Throttle Throttle(-1 indicates no change)    min: -1 %
  # 4: Relative 0: absolute, 1: relative min: 0 max: 1 increment: 1

  def setSetSpeed(self, type, speed):
    self.command = mnct.mlNavCommandType.MAV_CMD_DO_CHANGE_SPEED
    self.param1 = type
    self.param2 = speed
    self.param3 = 0.0
    self.param4 = 1.0
    self.param5 = 0.0
    self.param6 = 0.0
    self.param7 = 0.0


  def writeCommand(self, index):
    cmd = str(index) + "\t0\t3\t" + self.command + "\t" + self.param1 + "\t" + self.param2 + "\t" + self.param3 + "\t" + self.param4 + "\t" + self.param5 + "\t" + self.param6 + "\t" + self.param7 + "\t1"
    return cmd