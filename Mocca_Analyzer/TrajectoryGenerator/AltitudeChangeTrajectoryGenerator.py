import random
import matplotlib.pyplot as plt
import numpy as np

'''
Generates a flight plan with multiples(nOfSamples) altitude changes from hmin to hmax with altitude intevals from intervalmin to intervalmax
@param
    hmax: upper altitude boundary
    hmin: lower altitude boundary
    intervalmin: minimum altitude change
    intervalmax: maximum altitude change
    nOfSamples: number of altitude changes
    Plot: If True, plot the trajectory
'''
def VerticalClimbDescentTrajectoryGenerator(nOfFP2Generate, hTakeOff, hLanding, hmax, hmin, intervalmin, intervalmax, nOfSamples, plotPath, Plot):


    if hmax - hmin < intervalmin or nOfSamples <= 1:
        print("Wrong input parameters")

    totalHeightChanges = []
    totalHeightArray = []
    histoHeightChanges = []
    histogramInterval = 5 #HistogramPlotXInterval

    nOfFPGenetared = 0
    while nOfFPGenetared < nOfFP2Generate:

        #Array of a single FP
        HeightChanges = []
        HeightArray = []
        actualheight = hTakeOff
        HeightChanges.append(0)
        HeightArray.append(hTakeOff)
        accumulatedHeightChange = 0
        for index in range(0, nOfSamples - 1, 1):
            while (True):
                #generates a random altitude change +-[intervalmin, intervalmax]
                heightchange = random.choice([-1, 1]) * random.randint(intervalmin, intervalmax)
                # check if the altitude change surpass the hmax
                if heightchange + actualheight <= hmax and heightchange + actualheight >= hmin:
                    actualheight = actualheight + heightchange
                    HeightChanges.append(heightchange)
                    HeightArray.append(actualheight)
                    histoHeightChanges.append(heightchange-(heightchange)%histogramInterval)
                    accumulatedHeightChange = accumulatedHeightChange + abs(heightchange)

                    print("heightchange: " + str(heightchange))
                    print("actualheight: " + str(actualheight))
                    break
        #go to hLanding
        heightchange = actualheight - hLanding
        actualheight = hLanding
        HeightChanges.append(heightchange)
        HeightArray.append(actualheight)
        histoHeightChanges.append((heightchange)-(heightchange)%histogramInterval)
        accumulatedHeightChange = accumulatedHeightChange + abs(heightchange)

        print("Height array: " + str(HeightArray))
        print("Height changes: " + str(HeightChanges))
        print("accumulatedHeightChanges :" + str(accumulatedHeightChange))

        #Add the FP to the multiple FP array
        totalHeightChanges.append(HeightChanges)
        totalHeightArray.append(HeightArray)

        if Plot == True:
            figI = plt.figure()
            # ggplot style
            plt.style.use('ggplot')
            axI = figI.add_subplot(111)
            axI.set_xlabel('Steps')
            axI.set_ylabel('Height')

            steps = np.arange(0, len(HeightArray), 1)
            plt.plot(steps, HeightArray, marker='_', color='red')
            plt.title("Randomly trajectory generated")
            plt.show()
            figI.set_size_inches(14.0, 14.0)
            figI.savefig(plotPath + "\\" + "VerticalClimbDescentTrajectory" + str(nOfFPGenetared) + ".png")
            plt.close(figI)

        nOfFPGenetared = nOfFPGenetared + 1

    if Plot == True:
        #Plot histogram
        frequency = list(range(min(histoHeightChanges) - 5,max(histoHeightChanges) + 5, histogramInterval))
        plt.hist(x=histoHeightChanges, bins=frequency, color='#F2AB6D', rwidth=0.85, align='left')
        plt.title('Altitude Changes')
        plt.grid(True)
        plt.xlabel('Altitude Changes')
        plt.ylabel('Frequency')
        plt.xticks(frequency)
        plt.tick_params(labelsize=5)
        plt.gcf()
        plt.savefig(plotPath + "\\" + "AltitudeChangeHistogram.png")
        plt.show()

    return totalHeightArray, totalHeightChanges


'''
Generates a flight plan with multiples(nOfSamples) altitude changes from hmin to hmax with altitude intevals from intervalmin to intervalmax
@param

'''
def NonVerticalClimbDescentTrajectoryGenerator(HeightData, XData, nOfSamples, plotPath, Plot):

    HTakeOff, HLanding, Hmax, Hmin, Hintervalmin, Hintervalmax = HeightData
    XTakeOff, XLanding, Xmax, Xmin, Xintervalmin, Xintervalmax = XData

    if Hmax - Hmin < Hintervalmin or nOfSamples <= 1 or Xmax - Xmin < Xintervalmin :
        print("Wrong input parameters")

    HeightChanges = []
    HeightArray = []
    XHorizontalChanges = []
    XHorizontalArray = []
    actualheight = HTakeOff
    actualXposition = XTakeOff
    HeightChanges.append(0)
    XHorizontalChanges.append(0)
    HeightArray.append(HTakeOff)
    XHorizontalArray.append(XTakeOff)
    accumulatedHeightChange = 0
    accumulatedXChange = 0

    for index in range(0, nOfSamples - 1, 1):
        while (True):
            #generates a random altitude change +-[intervalmin, intervalmax]
            heightchange = random.choice([-1, 1]) * random.randint(Hintervalmin, Hintervalmax)
            # check if the altitude change surpass the hmax
            if heightchange + actualheight <= Hmax and heightchange + actualheight >= Hmin:
                Xchange = random.choice([-1, 1]) * random.randint(Xintervalmin, Xintervalmax)
                if Xchange + actualXposition <= Xmax and Xchange + actualXposition >= Xmin:
                    actualheight = actualheight + heightchange
                    actualXposition = actualXposition + Xchange
                    HeightChanges.append(heightchange)
                    HeightArray.append(actualheight)
                    XHorizontalChanges.append(Xchange)
                    XHorizontalArray.append(abs(actualXposition))
                    accumulatedHeightChange = accumulatedHeightChange + abs(heightchange)
                    accumulatedXChange = accumulatedXChange + abs(Xchange)

                    print("---Sample---")
                    print("heightchange: " + str(heightchange) + "; actualheight: " + str(actualheight))
                    print("Xchange: " + str(Xchange) + "; actualXposition: " + str(actualXposition))
                    break

    #go to Landing
    heightchange = actualheight - HLanding
    actualheight = HLanding
    Xchange = actualXposition - XLanding
    actualXposition = XLanding
    HeightChanges.append(heightchange)
    HeightArray.append(actualheight)
    XHorizontalChanges.append(Xchange)
    XHorizontalArray.append(abs(actualXposition))
    accumulatedHeightChange = accumulatedHeightChange + abs(heightchange)
    accumulatedXChange = accumulatedXChange + abs(Xchange)

    print("Height array: " + str(HeightArray))
    print("Height changes: " + str(HeightChanges))
    print("accumulatedHeightChanges :" + str(accumulatedHeightChange))

    if Plot == True:
        figI = plt.figure()
        # ggplot style
        plt.style.use('ggplot')
        axI = figI.add_subplot(111)
        axI.set_xlabel('Ax')
        axI.set_ylabel('Height')


        plt.plot(XHorizontalArray, HeightArray, marker='_', color='red')
        plt.title("Randomly trajectory generated")
        plt.show()
        figI.set_size_inches(14.0, 14.0)
        figI.savefig(plotPath + "\\" + "NonVerticalClimbDescentTrajectory.png")
        plt.close(figI)

    return HeightArray, HeightChanges, accumulatedHeightChange, XHorizontalArray, XHorizontalChanges, accumulatedXChange