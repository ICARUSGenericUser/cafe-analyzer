import random
import matplotlib.pyplot as plt
import numpy as np


class mlTrajectory():
    def __init__(self):
        self.commandList = []


    def addCommand(self, mlCommand):
        self.commandList.add(mlCommand)


    def writeTrajectory(self, folder, file):

        file_enc = open(folder + "\\" + file, "w")

        file_enc.write("QGC WPL 110");

        file_enc.write("0\t1\t0\t0\t0\t0\t0\t0\t" + wp.Latitude + "\t" + wp.Longitude + "\t0\t1");

        index = 0
        for command in self.commandList:
            file_enc.write(str(index) + "\t" + command.writeCommand() + "\n")
            index += 1

        file_enc.close()