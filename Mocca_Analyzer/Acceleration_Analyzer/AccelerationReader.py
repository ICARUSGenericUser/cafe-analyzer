import pandas as pd
import numpy as np
from Mocca_Analyzer.Acceleration_Analyzer import accelerationObject as accelObj

accel_colums = [
        "Name",
        "Timestamp",
        "HorizontalAcceleration",
        "LatitudeAcceleration",
        "LongitudeAcceleration"
]
def readAccelerationFile(full_acceleration_path):

    print("Reading Accelerations...")

    accel_df = pd.read_csv(full_acceleration_path, delimiter=',', index_col=False, header=None, names=accel_colums)

    sequence = []

    for index, row in accel_df.iterrows():
        msg = accelObj.accelerationObject()
        msg.decodeMsg(row)

        if msg != None:
            sequence.append(msg)

    return sequence