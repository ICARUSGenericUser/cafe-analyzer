class accelerationObject():
  def __init__(self):
    super().__init__()
    self.name = ""
    self.timestamp = 0.0
    self.acceleration = 0.0
    self.latitude = 0.0
    self.longitude = 0.0


  def decodeMsg(self, row):
    self.name = str(row["Name"])
    self.timestamp = float(row["Timestamp"])
    self.acceleration = float(row["HorizontalAcceleration"])
    self.latitude = float(row["LatitudeAcceleration"])
    self.longitude = float(row["LongitudeAcceleration"])