

class CircularBuffer(object):

    def __init__(self, max_size=20):
        """Initialize the CircularBuffer with a max_size if set, otherwise
        max_size will elementsdefault to 10"""
        self.buffer = [None] * max_size
        self.head = 0
        self.tail = 0
        self.max_size = max_size
        self.mysize = 0


    def __str__(self):
        """Return a formatted string representation of this CircularBuffer."""
        items = ['{!r}'.format(item) for item in self.buffer]
        return '[' + ', '.join(items) + ']'

    def size(self):
        """Return the size of the CircularBuffer
        Runtime: O(1) Space: O(1)"""
        if self.tail >= self.head:
            return self.tail - self.head
        #return self.max_size - self.head - self.tail
        #return self.max_size -(self.tail - self.head)
        #print ("Debug: Tail idx: ", self.tail, " Head idx: ", self.head, " Size: ", (self.tail - self.head) % self.max_size)
        return (self.tail - self.head) % self.max_size

    def get_mysize(self):
        return self.mysize

    def is_empty(self):
        """Return True if the head of the CircularBuffer is equal to the tail,
        otherwise return False
        Runtime: O(1) Space: O(1)"""
        return self.tail == self.head

    def is_full(self):
        """Return True if the tail of the CircularBuffer is one before the head,
        otherwise return False
        Runtime: O(1) Space: O(1)"""
        return self.tail == (self.head-1) % self.max_size

    def enqueue(self, item):
        """Insert an item at the back of the CircularBuffer
        Runtime: O(1) Space: O(1)"""
        if self.is_full():
            raise OverflowError(
                "CircularBuffer is full, unable to enqueue item")
        self.buffer[self.tail] = item
        self.tail = (self.tail + 1) % self.max_size
        self.mysize += 1

    def peek(self, position):
        p = self.actual_position(position)
        return self.buffer[p]

    def get_head(self):
        """Return the item at the front of the CircularBuffer
        Runtime: O(1) Space: O(1)"""
        return self.buffer[self.head]

    def get_prehead(self):
        p = self.get_prev_position(self.head)
        return self.buffer[p]

    def get_head_index(self):
        """Return the item at the front of the CircularBuffer
        Runtime: O(1) Space: O(1)"""
        return self.head

    def get_tail(self):
        """Return the item at the end of the CircularBuffer
        Runtime: O(1) Space: O(1)"""
        return self.buffer[(self.tail - 1) % self.max_size]

    def get_tail_index(self):
        """Return the item at the end of the CircularBuffer
        Runtime: O(1) Space: O(1)"""
        return (self.tail - 1) % self.max_size

    def get_center(self):
            p = self.center_position()
            return self.buffer[p]

    def dequeue(self):
        """Return the item at the front of the Circular Buffer and remove it
        Runtime: O(1) Space: O(1)"""
        if self.is_empty():
            raise IndexError("CircularBuffer is empty, unable to dequeue")
        item = self.buffer[self.head]
        self.buffer[self.head] = None
        self.head = (self.head + 1) % self.max_size
        self.mysize -= 1
        return item

    def actual_position(self, position):
        return position % self.max_size

    def next_position(self, position):
        return (position + 1) % self.max_size

    def prev_position(self, position):
        return (position + self.max_size - 1) % self.max_size

    def center_position(self):
        return (self.head + self.max_size - (self.size() / 2)) % self.max_size

