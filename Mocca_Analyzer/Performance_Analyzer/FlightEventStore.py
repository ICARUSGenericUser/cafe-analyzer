import sys
import math as math
from geographiclib import geodesic as geo
import numpy as np
from Mocca_Analyzer.Performance_Analyzer import Event_Type as et
from Mocca_Analyzer.Performance_Analyzer import Flight_Event as fe
from Mocca_Analyzer.Performance_Analyzer import TimedCircularBuffer as tcb
from Mocca_Analyzer.Mocca import Mocca_Object as mo
from Mocca_Analyzer.Mocca import mlGlobalPositionInt as gpi
from Mocca_Analyzer.Plots import dynamicAltitudePlot as dap, dynamicTimePlot as dtp, dynamicLocationPlot as dlp, dynamicDistancePlot as ddp
from Util import angles as agl

TIME_RANGE = 3500
MIN_TIME_RANGE = 2900

class FlightEventStore:
    def __init__(self, globalEventNumber, debug , analysis = 3, minTimeRange = MIN_TIME_RANGE, maxTimeDiff = TIME_RANGE):
        self.debug = debug
        self.analysisLevel = analysis
        self.currentIdx = 0
        self.eventList = []
        self.globalEventNumber = globalEventNumber
        self.telemetryList = []
        self.lastVSpeedEvent = None
        self.lastHSpeedEvent = None
        self.lastAltitudeEvent = None
        self.lastCourseEvent = None
        self.lastHoverEvent = None
        self.telemetryBuffer = tcb.TimedCircularBuffer(maxTimeDiff)
        self.init_turn_delta = 15.0
        self.end_turn_delta = 5.0
        self.speed_limit = 0.5
        self.startOfCourseRefineThreshold = 0.1
        self.endOfCourseRefineThreshold = 0.07
        self.startOfAltitudeChangeThreshold = 8.0
        self.endOfAltitudeChangeThreshold = 2.0
        self.startOfAltitudeChangeRefineThreshold = 0.01
        self.endOfAltitudeChangeRefineThreshold = 0.01
        self.minTimeRange = minTimeRange


        if self.debug >= 2:
            self.altitude_plot = dap.dynamicAltitudePlot('T (ms)', 'Altitude (m)', "Dynamic altitude plot")
            self.course_plot = dtp.dynamicTimePlot('T (ms)', 'Course (deg)', "Dynamic course plot")
            self.distance_plot = ddp.dynamicDistancePlot('T (ms)', 'Distance (m)', "Dynamic distance plot")
            self.location_plot = dlp.dynamicLocationPlot('Latitude (deg)', 'Longitude (deg)', "Dynamic location plot")
            self.hspeed_plot = dtp.dynamicTimePlot('T (ms)', 'HSpeed (m/s)', "Dynamic hspeed plot")



    def getHeadPositional(self):
        return self.telemetryBuffer.getHeadPositional()


    def getTailPositional(self):
        return self.telemetryBuffer.getTailPositional()


    def getFirstPositional(self):
        index = 0
        size = len(self.telemetryList)

        while size > 0:
            sample = self.telemetryList[index]

            if sample.type == mo.Object_Type.basic or mo.Object_Type.swGNSSNavigation or sample.type == mo.Object_Type.mlGlobalPositionInt or sample.type == mo.Object_Type.mlGPSRawInt:
                return sample

            index = index + 1
            size -= 1

        return None


    def getLastPositional(self):
        index = len(self.telemetryList) - 1
        size = len(self.telemetryList)

        while size > 0:
            sample = self.telemetryList[index]

            if sample.type == mo.Object_Type.basic or mo.Object_Type.swGNSSNavigation or sample.type == mo.Object_Type.mlGlobalPositionInt or sample.type == mo.Object_Type.mlGPSRawInt:
                return sample

            index = index - 1
            size -= 1

        return None


    def insertAnalyzing(self, sample, isEventStarted):
        if sample.type == mo.Object_Type.basic:
            self.addToList(sample)

            if self.telemetryBuffer.getTimeRange() >= self.minTimeRange and self.telemetryBuffer.getPositional() >= 2:
                isEventStarted = self.identifyAltitudeChange(sample, isEventStarted, 5.0)
                #self.identifyCourseChange(sample, self.course_delta)

            if self.telemetryBuffer.getTimeRange() >= self.minTimeRange and self.telemetryBuffer.getPositional() >= 5 and isEventStarted != 'None':
                self.identifyHover(3.0)
            return

        '''
        if sample.type == mo.Object_Type.swGNSSNavigation:
            self.addToList(sample)

            if self.debug >= 1:
                print("GNSSNavigation sample", "Time:", sample.timestamp)
                print("\tAltitude: ", sample.altitude, "Course:", sample.course, "HSpeed:", sample.hSpeed, "VSpeed:", sample.vSpeed)

            if self.debug >= 2:
                #dap.addSamplePlot(self.altitude_ax, sample.timestamp - self.telemetryBuffer.baseTime, sample.altitude, color='green')
                self.course_plot.addSamplePlot(sample.timestamp - self.telemetryBuffer.baseTime, sample.course)
                #self.distance_plot.addSamplePlot(sample.timestamp - self.telemetryBuffer.baseTime, sample)
                self.location_plot.addSamplePlot(sample.latitude, sample.longitude)

            if self.telemetryBuffer.getTimeRange() >= MIN_TIME_RANGE and self.telemetryBuffer.getPositional() >= 2:
                self.identifyAltitudeChange(sample, 3.0)
                #self.identifyCourseChange(sample, self.course_delta)
                self.identifyHSpeedChange(sample, 0.15, 0.05)

            return
        '''
        if sample.type == mo.Object_Type.mlGlobalPositionInt:
            self.addToList(sample)

            #if self.debug >= 1:
                #print("GlobalPositionInt sample", "Time:", sample.timestamp)
                #print("\tAltitude: ", sample.altitude, "Course:", sample.course, "HSpeed:", sample.hspeed, "VSpeed:", sample.vz)

            if self.debug >= 2:
                self.altitude_plot.addEventPlot(sample.timestamp - self.telemetryBuffer.baseTime, sample.altitude, "  ")
                #TODO delete course1plot
                horizontalDistance = self.telemetryList[0].getDistance(self.telemetryList[sample.idx])
                #self.course_plot.addEventPlot(horizontalDistance, sample.altitude, str(round(sample.vz,3)))
                self.course_plot.addSamplePlot(sample.timestamp - self.telemetryBuffer.baseTime, sample.course)
                v = np.sqrt(sample.vx * sample.vx + sample.vy * sample.vy)
                self.location_plot.addSamplePlot(sample.latitude, sample.longitude,color='purple', marker = 'd')
                self.hspeed_plot.addSamplePlot(sample.timestamp - self.telemetryBuffer.baseTime, sample.hspeed)
                self.altitude_plot.addSamplePlot(sample.timestamp - self.telemetryBuffer.baseTime, sample.altitude,
                                                 color='purple', marker = 'd')

                a = self.telemetryBuffer.getTimeRange()
                b = self.telemetryBuffer.getPositional()
            if self.telemetryBuffer.getTimeRange() >= self.minTimeRange and self.telemetryBuffer.getPositional() >= 2:

                    isEventStarted = self.identifyAltitudeChange(sample,isEventStarted, 3.0)

                    isEventStarted = self.identifyCourseChange(sample,isEventStarted,  self.init_turn_delta, self.end_turn_delta, self.speed_limit)
                    #self.identifyHSpeedChange(sample, 0.15, 0.05)
                    #self.location_plot.addSamplePlot(sample.latitude, sample.longitude)
                    #self.location_plot.addEventPlot(sample.latitude, sample.longitude, str(round(sample.timestamp,1)))



        #elif sample.type == mo.Object_Type.mlGPSRawInt:
            #self.addToList(sample)

            #if self.debug >= 1:
                #print("GPS Raw Int sample", "Time:", sample.timestamp)
                #print("\tAltitude: ", sample.altitude, "Course:", sample.course, "HSpeed:", sample.hspeed)

            #if self.debug >= 2:
                #self.altitude_plot.addSamplePlot(sample.timestamp - self.telemetryBuffer.baseTime, sample.altitude, color='blue')
                #self.course_plot.addSamplePlot(sample.timestamp - self.telemetryBuffer.baseTime, sample.course)
                #self.location_plot.addSamplePlot(sample.latitude, sample.longitude)

            #if self.telemetryBuffer.getTimeRange() >= MIN_TIME_RANGE and self.telemetryBuffer.getPositional() >= 2:
                #if self.analysisLevel == 1:
                    #self.identifyAltitudeChange(sample, 3.0)
                #elif self.analysisLevel == 2:
                    #self.identifyCourseChange(sample, self.course_delta, self.speed_limit)
                #else:
                    #self.identifyAltitudeChange(sample, 3.0)
                    #self.identifyCourseChange(sample, self.course_delta)
                    #self.identifyHSpeedChange(sample, 0.15, 0.05)

        #    return

        #elif sample.type == mo.Object_Type.mlAttitude:
        #    self.addToList(sample)
        #    return
#
        #elif sample.type == mo.Object_Type.mlVfrHud:
        #    #self.addToList(sample)
#
        #    if self.debug >= 1:
        #        print("VfrHud sample", "Time:", sample.timestamp)
        #        print("\tAltitude: ", sample.altitude, "Compass Heading:", sample.heading, "HSpeed:", sample.hSpeed, "VSpeed:", sample.vSpeed)
#
        #    #if self.debug >= 2:
        #        #self.altitude_plot.addSamplePlot(sample.timestamp - self.telemetryBuffer.baseTime, sample.altitude, color='grey')
#
        #    #if self.telemetryBuffer.getTimeRange() >= MIN_TIME_RANGE and self.telemetryBuffer.getPositional() >= 2:
        #    #    if self.analysisLevel == 2:
        #            #self.identifyCourseChange(sample, self.course_delta, self.speed_limit)  #mlVfrHud has attribute heading instead of course
#
        #    return

        #elif sample.type == mo.Object_Type.mlHighresIMU:
        #    self.addToList(sample)
#
        #    if self.debug >= 1:
        #        print("HighresIMU sample", "Time:", sample.timestamp)
        #        print("\tHAccel:", sample.hAcceleration, "VAccel:", sample.vAcceleration)
#
        #    return
#
        #elif sample.type == mo.Object_Type.mlScaledIMU:
        #    self.addToList(sample)
#
        #    if self.debug >= 1:
        #        print("ScaledIMU sample", "Time:", sample.timestamp)
        #        print("\tHAccel:", sample.hAcceleration, "VAccel:", sample.vAcceleration)

        #    return



    def addToList(self, sample):
        sample.idx = self.currentIdx
        self.telemetryList.append(sample)
        self.currentIdx += 1
        self.telemetryBuffer.insertSample(sample)

    def plotTurnDLP(self):
        for event in self.eventList:
            sample = self.telemetryList[event.idx]
            if event.type == et.Event_Type.StartCourseChange:
                self.location_plot.addEventPlot(sample.latitude, sample.longitude, "START-TURN")
            elif event.type == et.Event_Type.EndCourseChange:
                self.location_plot.addEventPlot(sample.latitude, sample.longitude, "END-TURN")

    def doneAnalyzing(self, path):
        #self.plotTurnDLP()
        if self.debug >= 2:
            self.altitude_plot.closeDynamicPlot(path + "-AltitudePlot.png")
            self.course_plot.closeDynamicPlot(path + "-CoursePlot.png")
            self.distance_plot.closeDynamicPlot(path + "-DistancePlot.png")
            self.location_plot.closeDynamicPlot(path + "-HorizontalPlot.png")
            self.hspeed_plot.closeDynamicPlot(path + "-HSpeedPlot.png")


    def GetNextPositionIntSample(self, initialIndex, finalIndex):
        index = initialIndex + 1
        while index < finalIndex:
            sample = self.telemetryList[index]
            if sample.type == mo.Object_Type.mlGlobalPositionInt or mo.Object_Type.basic:
                nextGlobalPosIntSample = sample
                nextSamplePosition = index
                return nextGlobalPosIntSample, nextSamplePosition
            index = index + 1
        return -1, -1


    def GetPreviousPositionIntSample(self, initialIndex, finalIndex):
        index = finalIndex - 1
        while index > initialIndex:
            sample = self.telemetryList[index]
            if sample.type == mo.Object_Type.mlGlobalPositionInt or mo.Object_Type.basic:
                previousGlobalPosIntSample = sample
                previousSamplePosition = index
                return previousGlobalPosIntSample, previousSamplePosition
            index = index - 1
        return -1, -1


    def identifyAltitudeChange(self, newer, isEventStarted, altitude_delta):

        older = self.telemetryBuffer.getHeadPositional()
        newer = self.telemetryBuffer.getTailPositional()

        #print("New sample altitude: ", newer.altitude, "old sample altitude:", older.altitude, " Altitude diference: ", newer.altitude - older.altitude)

        if self.lastAltitudeEvent == None or self.lastAltitudeEvent.type == et.Event_Type.EndAltChange:

            if (newer.altitude - older.altitude) > self.startOfAltitudeChangeThreshold:
                event = fe.Flight_Event()
                event.type = et.Event_Type.StartClimb
                event.latitude = older.latitude
                event.longitude = older.longitude
                event.altitude = older.altitude
                event.course = older.course
                event.timestamp = older.timestamp - self.telemetryBuffer.baseTime
                event.idx = older.idx
                event.tendency = +1
                self.globalEventNumber = self.globalEventNumber + 1
                event.tag = self.globalEventNumber

                self.eventList.append(event)
                self.lastAltitudeEvent = event

                isEventStarted = "AltChange"

                #print("Altitude flight event at: ", event.timestamp, "START-CLIMB", " Reference altitude: ", older.altitude)

                if self.debug >= 2:
                    self.altitude_plot.addEventPlot(older.timestamp - self.telemetryBuffer.baseTime, older.altitude, " ")
                    #-----------------------------------------
                    horizontalDistance = self.telemetryList[0].getDistance(self.telemetryList[older.idx])
                    #self.course_plot.addEventPlot(horizontalDistance, older.altitude, "START-CLIMB  " + str(self.globalEventNumber))

                return isEventStarted

            if (newer.altitude - older.altitude) < - self.startOfAltitudeChangeThreshold:
                event = fe.Flight_Event()
                event.type = et.Event_Type.StartDescent
                event.latitude = older.latitude
                event.longitude = older.longitude
                event.altitude = older.altitude
                event.course = older.course
                event.timestamp = older.timestamp - self.telemetryBuffer.baseTime
                event.idx = older.idx
                event.tendency = -1
                self.globalEventNumber = self.globalEventNumber + 1
                event.tag = self.globalEventNumber

                isEventStarted = "AltChange"

                self.eventList.append(event)
                self.lastAltitudeEvent = event
                #print("Altitude flight event at: ", event.timestamp, "START-DESCENT", " Reference altitude: ", older.altitude)

                if self.debug >= 2:
                    self.altitude_plot.addEventPlot(older.timestamp - self.telemetryBuffer.baseTime, older.altitude, " ")
                    # -----------------------------------------
                    horizontalDistance = self.telemetryList[0].getDistance(self.telemetryList[older.idx])
                    #self.course_plot.addEventPlot(horizontalDistance, older.altitude, "START-DESCENT  " + str(self.globalEventNumber))

                return isEventStarted

        if self.lastAltitudeEvent != None and (self.lastAltitudeEvent.type == et.Event_Type.StartClimb or self.lastAltitudeEvent.type == et.Event_Type.StartDescent):

            if abs(newer.altitude - older.altitude) < self.endOfAltitudeChangeThreshold:
                event = fe.Flight_Event()
                event.type = et.Event_Type.EndAltChange
                event.latitude = newer.latitude
                event.longitude = newer.longitude
                event.altitude = newer.altitude
                event.course = older.course
                event.timestamp = newer.timestamp - self.telemetryBuffer.baseTime
                event.idx = newer.idx
                self.globalEventNumber = self.globalEventNumber + 1
                event.tag = self.globalEventNumber

                isEventStarted = "Empty"

                self.eventList.append(event)
                self.lastAltitudeEvent = event
                #print("Altitude flight event at: ", event.timestamp, "LEVEL-OFF", " Reference altitude: ", newer.altitude)

                if self.debug >= 2:
                    self.altitude_plot.addEventPlot(older.timestamp - self.telemetryBuffer.baseTime, older.altitude, " ")

                    # -----------------------------------------
                    horizontalDistance = self.telemetryList[0].getDistance(self.telemetryList[older.idx])
                    #self.course_plot.addEventPlot(horizontalDistance, older.altitude, "LEVEL-OFF" + str(self.globalEventNumber))

                return isEventStarted

            '''if not self.telemetryBuffer.checkTendency(gpi.AltitudeTendency, self.lastAltitudeEvent.tendency):
                event = fe.Flight_Event()
                event.type = et.Event_Type.EndAltChange
                event.latitude = newer.latitude
                event.longitude = newer.longitude
                event.altitude = newer.altitude
                event.course = older.course
                event.timestamp = newer.timestamp - self.telemetryBuffer.baseTime
                event.idx = newer.idx
                self.globalEventNumber = self.globalEventNumber + 1
                event.tag = self.globalEventNumber

                isEventStarted = "Empty"

                self.eventList.append(event)
                self.lastAltitudeEvent = event
                #print("Altitude flight event at: ", event.timestamp, "REVERSE", " Reference altitude: ", newer.altitude)

                if self.debug >= 2:
                    self.altitude_plot.addEventPlot(older.timestamp - self.telemetryBuffer.baseTime, older.altitude, "REVERSE  " )

                return isEventStarted'''

        return isEventStarted
    # Guidelines for Turn / Hold events:
    # Events are a combination of CourseChanges or Hovers; both things cannot happen at the same time
    # After a hold ends, either a straight segment or a turn may start.
    # A Turn may end in a straight segment that starts or into a hold that starts.
    # Holds are processed with priority with respect turns due to the course uncertainty.
    # We will consider that the vehicle is holding if the horizontal speed is below a certain threshold.

    def identifyCourseChange(self, older, isEventStarted, init_turn_delta, end_turn_delta, speedLimit):
        if hasattr(older, 'course'):
            older = self.telemetryBuffer.getHeadPositional()
            newer = self.telemetryBuffer.getTailPositional()

            #To avoid the detection of hold when the drone is at ground
            #self.eventList[-1].type != et.Event_Type.StartClimb To avoid course change identification when the drone is climbing/descending
            if older.altitude > 3.0 and newer.altitude > 3.0 :
                if (self.lastCourseEvent == None or self.lastCourseEvent.type == et.Event_Type.EndCourseChange or self.lastCourseEvent.type == et.Event_Type.EndHover) :

                    a = agl.normalizeToAcute(newer.course - older.course)

                    if agl.normalizeToAcute(newer.course - older.course) < - init_turn_delta:

                        event = fe.Flight_Event()
                        event.type = et.Event_Type.StartCourseChange
                        event.latitude = older.latitude
                        event.longitude = older.longitude
                        event.altitude = older.altitude
                        event.course = older.course
                        event.timestamp = older.timestamp - self.telemetryBuffer.baseTime
                        event.idx = older.idx
                        self.globalEventNumber = self.globalEventNumber + 1
                        event.tag = self.globalEventNumber

                        isEventStarted = "CourseChange"

                        self.eventList.append(event)
                        self.lastCourseEvent = event
                        #print("Course event at: ", event.timestamp, "START-RIGHT-TURN", " Reference course: ", older.course)

                        if self.debug >= 2:
                            self.course_plot.addEventPlot(older.timestamp - self.telemetryBuffer.baseTime, older.course, " ")
                            #self.location_plot.addEventPlot(older.latitude, older.longitude, "START-RT")
                            #self.location_plot.addEventPlot(older.latitude, older.longitude, str(round(event.timestamp,3)))
                            self.hspeed_plot.addEventPlot(older.timestamp - self.telemetryBuffer.baseTime, older.hspeed, "START-RT")

                        return isEventStarted

                    if agl.normalizeToAcute(newer.course - older.course) > init_turn_delta:

                        event = fe.Flight_Event()
                        event.type = et.Event_Type.StartCourseChange
                        event.latitude = older.latitude
                        event.longitude = older.longitude
                        event.altitude = older.altitude
                        event.course = older.course
                        event.timestamp = older.timestamp - self.telemetryBuffer.baseTime
                        event.idx = older.idx
                        self.globalEventNumber = self.globalEventNumber + 1
                        event.tag = self.globalEventNumber

                        isEventStarted = "CourseChange"

                        self.eventList.append(event)
                        self.lastCourseEvent = event
                        #print("Course event at: ", event.timestamp, "START-LEFT-TURN", " Reference Course: ", older.course)

                        if self.debug >= 2:
                            self.course_plot.addEventPlot(older.timestamp - self.telemetryBuffer.baseTime, older.course, " ")
                            #self.location_plot.addEventPlot(older.latitude, older.longitude, "START-LT")
                            self.hspeed_plot.addEventPlot(older.timestamp - self.telemetryBuffer.baseTime, older.hspeed, "START-LT")

                        return isEventStarted

                    if self.telemetryBuffer.remainsAsHold(speedLimit):

                        event = fe.Flight_Event()
                        event.type = et.Event_Type.StartHover
                        event.latitude = older.latitude
                        event.longitude = older.longitude
                        event.altitude = older.altitude
                        event.course = older.course
                        event.timestamp = older.timestamp - self.telemetryBuffer.baseTime
                        event.idx = older.idx
                        self.globalEventNumber = self.globalEventNumber + 1
                        event.tag = self.globalEventNumber


                        isEventStarted = "CourseChange"

                        self.eventList.append(event)
                        self.lastCourseEvent = event
                        #print("Course event at: ", event.timestamp, "START-HOVER", " Reference Heading: None")

                        if self.debug >= 2:
                            self.course_plot.addEventPlot(older.timestamp - self.telemetryBuffer.baseTime, 0, "START-HOVER")
                            #self.location_plot.addEventPlot(older.latitude, older.longitude, "START-HV")
                            self.hspeed_plot.addEventPlot(older.timestamp - self.telemetryBuffer.baseTime, older.hspeed, "START-HV")

                        return isEventStarted


                if self.lastCourseEvent != None and (self.lastCourseEvent.type == et.Event_Type.StartCourseChange):
                    '''
                    if self.telemetryBuffer.remainsAsHold(speedLimit):

                        event = fe.Flight_Event()
                        event.type = et.Event_Type.StartHover
                        event.latitude = older.latitude
                        event.longitude = older.longitude
                        event.altitude = older.altitude
                        event.course = 0
                        event.timestamp = older.timestamp - self.telemetryBuffer.baseTime
                        event.idx = older.idx
                        self.globalEventNumber = self.globalEventNumber + 1
                        event.tag = self.globalEventNumber

                        isEventStarted = "CourseChange"

                        self.eventList.append(event)
                        self.lastCourseEvent = event
                        #print("Course event at: ", event.timestamp, "START-HOVER", " Reference Heading: None")

                        if self.debug >= 2:
                            self.course_plot.addEventPlot(older.timestamp - self.telemetryBuffer.baseTime, 0, "START-HOVER")
                            self.location_plot.addEventPlot(older.latitude, older.longitude, "START-HV")
                            self.hspeed_plot.addEventPlot(older.timestamp - self.telemetryBuffer.baseTime, older.hspeed, "START-HV")

                        return isEventStarted
                    '''

                    if agl.normalizeToAcute(newer.course - older.course) < end_turn_delta:
                        event = fe.Flight_Event()
                        event.type = et.Event_Type.EndCourseChange
                        event.latitude = newer.latitude
                        event.longitude = newer.longitude
                        event.altitude = newer.altitude
                        event.course = newer.course
                        event.timestamp = newer.timestamp - self.telemetryBuffer.baseTime
                        event.idx = newer.idx
                        self.globalEventNumber = self.globalEventNumber + 1
                        event.tag = self.globalEventNumber

                        isEventStarted = "Empty"

                        self.eventList.append(event)
                        self.lastCourseEvent = event
                        #print("Course event at: ", event.timestamp, "END-TURN", " Reference Course: ", newer.course)

                        if self.debug >= 2:
                            self.course_plot.addEventPlot(newer.timestamp - self.telemetryBuffer.baseTime, newer.course, "END-T" + str(event.tag))
                            #self.location_plot.addEventPlot(newer.latitude, newer.longitude, "END-T")
                            #self.location_plot.addEventPlot(newer.latitude, newer.longitude, str(round(event.timestamp,3)))
                            self.hspeed_plot.addEventPlot(newer.timestamp - self.telemetryBuffer.baseTime, newer.hspeed, "END-T")

                        return isEventStarted

                if self.lastCourseEvent != None and (self.lastCourseEvent.type == et.Event_Type.StartHover):

                    if self.telemetryBuffer.departsFromHold(speedLimit):

                        event = fe.Flight_Event()
                        event.type = et.Event_Type.EndCourseChange
                        event.latitude = newer.latitude
                        event.longitude = newer.longitude
                        event.altitude = newer.altitude
                        event.course = newer.course
                        event.timestamp = newer.timestamp - self.telemetryBuffer.baseTime
                        event.idx = newer.idx
                        self.globalEventNumber = self.globalEventNumber + 1
                        event.tag = self.globalEventNumber

                        isEventStarted = "Empty"

                        self.eventList.append(event)
                        self.lastCourseEvent = event
                        #print("Course event at: ", event.timestamp, "END-HOVER", " Reference Course: ", newer.course)

                        if self.debug >= 2:
                            self.course_plot.addEventPlot(older.timestamp - self.telemetryBuffer.baseTime, older.course, "END-HV")
                            #self.location_plot.addEventPlot(older.latitude, older.longitude, "END-HV")
                            self.hspeed_plot.addEventPlot(older.timestamp - self.telemetryBuffer.baseTime, older.hspeed, "END-HV")

                        return isEventStarted

        return isEventStarted


    def getTakeoffEvents(self):
        takeoffEvent = None
        endTakeoffEvent = None
        for event in self.eventList:
            if event.type == et.Event_Type.StartTakeOff:
                takeoffEvent = event
                continue
            if takeoffEvent != None and event.type == et.Event_Type.EndAltChange:
                endTakeoffEvent = event
                return takeoffEvent, endTakeoffEvent
        return None, None


    def getLandEvents(self):
        nEvent = 0
        for event in self.eventList:
            if event.type == et.Event_Type.StartLanding:
                return event, self.eventList[nEvent+1]
                break
            nEvent += 1
        return None, None


    def identifyTakeOffLanding(self):

        # Identify TakeOff
        nEvents = -1
        for event in self.eventList:
            nEvents += 1
            if event.type == et.Event_Type.StartClimb:
                self.eventList[nEvents].type = et.Event_Type.StartTakeOff
                break

        # Identify Landing
        eventListIndex = len(self.eventList) - 1
        toTime = 0
        ldTime = 0
        while (eventListIndex >= 0):
            if self.eventList[eventListIndex].type == et.Event_Type.StartDescent:
                self.eventList[eventListIndex].type = et.Event_Type.StartLanding

                if len(self.eventList) - 1 == eventListIndex: # means that Landing's level-off is not identified.So the last sample is the end.
                    event = fe.Flight_Event()
                    event.type = et.Event_Type.EndAltChange
                    event.latitude = self.telemetryList[-1].latitude
                    event.longitude = self.telemetryList[-1].longitude
                    event.altitude = self.telemetryList[-1].altitude
                    event.course = self.telemetryList[-1].course
                    event.timestamp = self.telemetryList[-1].timestamp - self.telemetryBuffer.baseTime
                    event.idx = self.telemetryList[-1].idx
                    self.eventList.append(event)
                break
            eventListIndex -= 1


    def identifyHSpeedChange(self, velocity_delta, end_delta):

        older = self.telemetryBuffer.getHeadPositional()
        newer = self.telemetryBuffer.getTailPositional()

        if self.lastHSpeedEvent == None or self.lastHSpeedEvent.type == et.Event_Type.EndHSpeedChange:

            if (nwSpeed - olSpeed) > velocity_delta:

                event = fe.Flight_Event()
                event.type = et.Event_Type.StartHAcceleration
                event.latitude = older.latitude
                event.longitude = older.longitude
                event.altitude = older.altitude

                if (SpeedType == "hSpeed"):
                    event.vSpeed = older.vSpeed
                elif (SpeedType == "vx" or SpeedType == "vy" or SpeedType == "vz"):
                    event.vx = older.vx
                    event.vy = older.vy
                    event.vz = older.vz

                event.timestamp = older.timestamp - self.telemetryBuffer.baseTime
                event.idx = older.idx

                self.eventList.append(event)
                self.lastHSpeedEvent = event
                print("Horizontal Speed change event at: ", event.timestamp, "Horizontal Acceleration", " Reference speed: ", olSpeed)
                #dap.addEventPlot(self.altitude_ax, older.timestamp - self.telemetryBuffer.baseTime, older.vSpeed, "START-CLIMB")

            if (nwSpeed - olSpeed) < - velocity_delta:
                event = fe.Flight_Event()
                event.type = et.Event_Type.StartHDeceleration
                event.latitude = older.latitude
                event.longitude = older.longitude
                event.altitude = older.altitude

                if (SpeedType == "hSpeed"):
                    event.vSpeed = older.vSpeed
                    event.hSpeed = older.hSpeed
                elif (SpeedType == "vx" or SpeedType == "vy" or SpeedType == "vz"):
                    event.vx = older.vx
                    event.vy = older.vy
                    event.vz = older.vz

                event.timestamp = older.timestamp - self.telemetryBuffer.baseTime
                event.idx = older.idx

                self.eventList.append(event)
                self.lastHSpeedEvent = event
                print("Horizontal Speed change event at: ", event.timestamp, "Horizontal Decceleration", " Reference speed: ", olSpeed)
                #dap.addEventPlot(self.altitude_ax, older.timestamp - self.telemetryBuffer.baseTime, older.altitude, "START-DESCENT")


        if self.lastHSpeedEvent != None and (self.lastHSpeedEvent.type == et.Event_Type.StartHAcceleration or self.lastHSpeedEvent.type == et.Event_Type.StartHDeceleration):


            if abs(nwSpeed - olSpeed) < end_delta:
                event = fe.Flight_Event()
                event.type = et.Event_Type.EndHSpeedChange
                event.latitude = newer.latitude
                event.longitude = newer.longitude
                event.altitude = newer.altitude

                if (SpeedType == "hSpeed"):
                    event.vSpeed = older.vSpeed
                    event.hSpeed = older.hSpeed
                elif (SpeedType == "vx" or SpeedType == "vy" or SpeedType == "vz"):
                    event.vx = older.vx
                    event.vy = older.vy
                    event.vz = older.vz

                event.timestamp = newer.timestamp - self.telemetryBuffer.baseTime
                event.idx = older.idx

                self.eventList.append(event)
                self.lastHSpeedEvent = event
                print("Horizontal Speed change event at: ", event.timestamp, "Horizontal Speed LEVEL-OFF", " Reference speed: ", nwSpeed)
                #dap.addEventPlot(self.altitude_ax, older.timestamp - self.telemetryBuffer.baseTime, older.altitude, "LEVEL-OFF")


    def identifyHover(self, radious_delta):

        older = self.telemetryBuffer.getHeadPositional()
        newer = self.telemetryBuffer.getTailPositional()

        distance = geo.Geodesic.WGS84.Inverse(older.latitude, older.longitude, newer.latitude, newer.longitude)

        if (self.lastHoverEvent == None or self.lastHoverEvent.type == et.Event_Type.EndHover) :

            #if (distance['s12'] < radious_delta) and (older.course < 0 or newer.course < 0):
            if (distance['s12'] < radious_delta):

                event = fe.Flight_Event()
                event.type = et.Event_Type.StartHover
                event.latitude = older.latitude
                event.longitude = older.longitude
                event.altitude = older.altitude
                event.course = older.course
                event.hspeed = older.hspeed
                event.vspeed = older.vspeed
                event.timestamp = older.timestamp - self.telemetryBuffer.baseTime

                self.eventList.append(event)
                self.lastHoverEvent = event
                print("Hover event at: ", event.timestamp, "START-HOVER", " Reference location, lat: ", older.latitude, older.longitude)
                #dlp.addEventPlot(self.location_ax, older.latitude, older.longitude, "START-HV")
                #dap.addEventPlot(self.altitude_ax, older.timestamp - self.telemetryBuffer.baseTime, older.altitude, "")

        if self.lastHoverEvent != None and (self.lastHoverEvent.type == et.Event_Type.StartHover):

            if distance['s12'] > radious_delta:
                event = fe.Flight_Event()
                event.type = et.Event_Type.EndHover
                event.latitude = newer.latitude
                event.longitude = newer.longitude
                event.altitude = newer.altitude
                event.course = newer.course
                event.hspeed = newer.hspeed
                event.vspeed = newer.vspeed
                event.timestamp = newer.timestamp - self.telemetryBuffer.baseTime

                self.eventList.append(event)
                self.lastHoverEvent = event
                print("Hover event at: ", event.timestamp, "END-HOVER", " Reference location, lat: ", newer.latitude, newer.longitude)
                #dap.addEventPlot(self.location_ax, newer.latitude, newer.longitude, "")
                #dap.addEventPlot(self.altitude_ax, older.timestamp - self.telemetryBuffer.baseTime, older.altitude, "")


    def refineEventsDetection(self):
        print("Refining Course Events ...")
        refinedCourseEvents = []
        newEventList = []

        event2Remove = []
        newList = self.eventList.copy()
        # check a StartOfCourse event is incorrect at the end
        eventListIndex = len(self.eventList) - 1
        while (eventListIndex >= 0):

            if self.eventList[eventListIndex].type == et.Event_Type.StartLanding:
                if self.eventList[eventListIndex-1].type == et.Event_Type.StartCourseChange:
                    event2Remove.append(self.eventList[eventListIndex-1])
                break
            if self.eventList[eventListIndex].type != et.Event_Type.EndAltChange:
                event2Remove.append(self.eventList[eventListIndex])

            eventListIndex -= 1

        for event in event2Remove:
            try:
                newList.remove(event)
            except:
                continue

        self.eventList = newList

        turnRefined = False
        previousEvent = ""
        nextLevelOffEventTimestamp = 0
        endTurnEventTimestamp = 0

        newList = self.eventList.copy()
        event2Remove = []
        previousEvent = None
        index = 0
        for index, event in enumerate(self.eventList):
            if event.type == et.Event_Type.EndCourseChange:
                endOfTurnEvent = event
                # identify Start Of courseChange
                eventListIndex = index - 1
                while (eventListIndex >= 0):
                    if self.eventList[eventListIndex].type == et.Event_Type.StartCourseChange or self.eventList[eventListIndex].type == et.Event_Type.StartHover:
                        startTurnEvent = self.eventList[eventListIndex]
                        break
                    elif self.eventList[eventListIndex].type == et.Event_Type.StartLanding or self.eventList[eventListIndex].type == et.Event_Type.StartClimb or self.eventList[eventListIndex].type == et.Event_Type.StartDescent or self.eventList[eventListIndex].type == et.Event_Type.EndAltChange or self.eventList[eventListIndex].type == et.Event_Type.StartTakeOff:
                        event2Remove.append(event)
                        eventListIndex = index - 1
                        while (eventListIndex >= 0):
                            if self.eventList[eventListIndex].type == et.Event_Type.StartCourseChange or self.eventList[eventListIndex].type == et.Event_Type.StartHover:
                                startTurnEvent = self.eventList[eventListIndex]
                                break
                            eventListIndex -= 1
                        event2Remove.append(self.eventList[eventListIndex])
                        break
                    eventListIndex -= 1


                #If there is a StartOfAltChange without EndOFAltChange
                endAltChangeFound = True
                while (eventListIndex >= 0):
                    if self.eventList[eventListIndex].type == et.Event_Type.EndAltChange:
                        endAltChangeFound = True
                        eventListIndex += 1
                        break
                    if self.eventList[eventListIndex].type == et.Event_Type.StartClimb or self.eventList[eventListIndex].type == et.Event_Type.StartDescent or self.eventList[eventListIndex].type == et.Event_Type.StartTakeOff or self.eventList[eventListIndex].type == et.Event_Type.StartLanding:
                        endAltChangeFound = False
                        eventListIndex += 1
                        break
                    eventListIndex -= 1
                if not endAltChangeFound:
                    if self.eventList[eventListIndex - 1].type == et.Event_Type.StartCourseChange:
                        event2Remove.append(self.eventList[eventListIndex])
                    while (eventListIndex <= index):
                        if self.eventList[eventListIndex].type == et.Event_Type.StartLanding or self.eventList[eventListIndex].type == et.Event_Type.StartClimb or self.eventList[eventListIndex].type == et.Event_Type.StartDescent or self.eventList[eventListIndex].type == et.Event_Type.EndAltChange or self.eventList[eventListIndex].type == et.Event_Type.StartTakeOff:
                            break
                        try:
                            event2Remove.remove(self.eventList[eventListIndex])
                        except:
                            eventAlreadyRemoved = True
                        event2Remove.append(self.eventList[eventListIndex])
                        eventListIndex += 1

        #Analyze each event
        CourseEvents = []

        for event in self.eventList:
            if event.type == et.Event_Type.StartHover or event.type == et.Event_Type.StartCourseChange or event.type == et.Event_Type.EndCourseChange or event.type == et.Event_Type.EndHover:
                CourseEvents.append(event)

        numberOfEvent = 0
        while numberOfEvent < len(CourseEvents):
            turnRefined = False
            #Get first event in CourseEventsArray.
            event2refine = CourseEvents[numberOfEvent]

            if event2refine.type == et.Event_Type.StartCourseChange:
                nextEvent = CourseEvents[numberOfEvent + 1] #Next event must be an end of turn or start of hover
                Angle2Turn = abs(nextEvent.course - event2refine.course)
                #Get next GlobalPositionInt sample
                nextGlobalPosIntSample, nextSamplePosition = self.GetNextPositionIntSample(event2refine.idx, nextEvent.idx)
                if nextSamplePosition == -1:
                    refinedCourseEvents.append(event2refine)
                    break
                previousSample = self.telemetryList[event2refine.idx]

                #Analyze the sample at the start of turn to the following to know if it contributes to the turn
                #If not, analyse the second one with the third and so on.
                index = event2refine.idx
                while index <= nextEvent.idx and turnRefined == False:

                    if abs(nextGlobalPosIntSample.heading - previousSample.heading) < self.startOfCourseRefineThreshold * Angle2Turn:
                        # This sample does not contribute to the turn
                        previousSample = nextGlobalPosIntSample
                        previousSamplePosition = nextSamplePosition
                        nextGlobalPosIntSample, nextSamplePosition = self.GetNextPositionIntSample(previousSamplePosition, nextEvent.idx)
                        if nextSamplePosition == -1:
                            break

                    else:
                        turnRefined = True

                        event = fe.Flight_Event()
                        event.type = et.Event_Type.StartCourseChange
                        event.latitude = nextGlobalPosIntSample.latitude
                        event.longitude = nextGlobalPosIntSample.longitude
                        event.altitude = nextGlobalPosIntSample.altitude
                        event.course = nextGlobalPosIntSample.course
                        event.timestamp = nextGlobalPosIntSample.timestamp - self.telemetryBuffer.baseTime
                        event.idx = nextGlobalPosIntSample.idx
                        event.tag = event2refine.tag

                        refinedCourseEvents.append(event)
                        #self.location_plot.addEventPlot(event.latitude, event.longitude, "Refined START")

                        #Replace the refined Event into self.eventList
                        a = self.eventList.index(event2refine)
                        self.eventList[self.eventList.index(event2refine)] = event

                    index = index + 1

                if not turnRefined:
                    #The turn is not refined.
                    #self.location_plot.addEventPlot(event2refine.latitude,event2refine.longitude, "NOT-Refined START")
                    refinedCourseEvents.append(event2refine)

            elif event2refine.type == et.Event_Type.EndCourseChange:
                previousEvent = CourseEvents[numberOfEvent - 1] #Previous event must be an start of turn or hover
                Angle2Turn = abs(previousEvent.course - event2refine.course)
                #Get previous GlobalPositionInt sample
                previousGlobalPosIntSample, previousSamplePosition = self.GetPreviousPositionIntSample(previousEvent.idx, event2refine.idx)
                if previousSamplePosition == -1:
                    refinedCourseEvents.append(event2refine)
                    break
                endOfEventSample = self.telemetryList[event2refine.idx]

                # Analyze the sample at the end of turn to the previous one to know if it contributes to the turn
                # If not, analyse the second one with the third and so on.
                index = event2refine.idx
                while index >= previousEvent.idx and turnRefined == False:

                    if abs(previousGlobalPosIntSample.heading - endOfEventSample.heading) < self.endOfCourseRefineThreshold * Angle2Turn:
                        # This sample does not contribute to the turn
                        endOfEventSample = previousGlobalPosIntSample
                        endSamplePosition = previousSamplePosition
                        previousGlobalPosIntSample, previousSamplePosition = self.GetPreviousPositionIntSample(previousEvent.idx, endSamplePosition)
                        if previousSamplePosition == -1:
                            break

                    else:
                        turnRefined = True

                        event = fe.Flight_Event()
                        event.type = et.Event_Type.EndCourseChange
                        event.latitude = endOfEventSample.latitude
                        event.longitude = endOfEventSample.longitude
                        event.altitude = endOfEventSample.altitude
                        event.course = endOfEventSample.course
                        event.timestamp = endOfEventSample.timestamp - self.telemetryBuffer.baseTime
                        event.idx = endOfEventSample.idx
                        event.tag = event2refine.tag

                        refinedCourseEvents.append(event)
                        #self.location_plot.addEventPlot(event.latitude, event.longitude, "Refined END")

                        # Replace the refined Event into self.eventList
                        self.eventList[self.eventList.index(event2refine)] = event

                    index = index - 1

                if not turnRefined:
                    #The turn is not refined.
                    #self.location_plot.addEventPlot(event2refine.latitude,event2refine.longitude, "NOT-Refined END")
                    refinedCourseEvents.append(event2refine)

            numberOfEvent = numberOfEvent + 1
        #remove duplicated events
        self.eventList = list(dict.fromkeys(self.eventList))
        #for event in self.eventList:
        #    self.location_plot.addEventPlot(event.latitude, event.longitude, "")


    def refineChangeAltitudeEvents(self):
        print("Refining Climb/Descent Events ...")
        print("\tTotal events: " + str(len(self.eventList)))
        refinedChangeAlltitudeEvents = []
        changeAltitudeEvents = []

        ischangeAltitudeRefined = False

        for event in self.eventList:
            if event.type == et.Event_Type.StartClimb or event.type == et.Event_Type.StartDescent or event.type == et.Event_Type.StartTakeOff or event.type == et.Event_Type.StartLanding or event.type == et.Event_Type.EndAltChange:
                changeAltitudeEvents.append(event)

        #Analyze each event
        numberOfEvent = 0

        while numberOfEvent < len(changeAltitudeEvents):
            ischangeAltitudeRefined = False
            #Get first event in CourseEventsArray.
            event2refine = changeAltitudeEvents[numberOfEvent]

            if event2refine.type == et.Event_Type.StartLanding or event2refine.type == et.Event_Type.StartClimb or event2refine.type == et.Event_Type.StartDescent:
                # Next event must be an end of altitude change
                nextEvent = changeAltitudeEvents[numberOfEvent + 1]
                altitudeVariation = nextEvent.altitude - event2refine.altitude
                typeOfEvent = "CLIMB" if altitudeVariation > 0 else "DESCENT"
                #Get next GlobalPositionInt sample
                nextGlobalPosIntSample, nextSamplePosition = self.GetNextPositionIntSample(event2refine.idx, nextEvent.idx)
                if nextSamplePosition == -1:
                    changeAltitudeEvents.append(event2refine)
                    break
                startOfEventSample = self.telemetryList[event2refine.idx]

                #Analyze the sample at the start of altitude change with the following to know if it contributes to the climb/descent
                #If not, analyse the second one with the third and so on.
                index = event2refine.idx
                samplesDeleted = 0
                while index <= nextEvent.idx and ischangeAltitudeRefined == False:

                    if (abs(nextEvent.altitude - startOfEventSample.altitude) >= 0.8*self.startOfAltitudeChangeThreshold) and (abs(nextGlobalPosIntSample.altitude - startOfEventSample.altitude) < self.startOfAltitudeChangeRefineThreshold * abs(altitudeVariation)):
                        samplesDeleted = + 1
                        # This sample does not contribute to the turn
                        startOfEventSample = nextGlobalPosIntSample
                        previousSamplePosition = nextSamplePosition
                        nextGlobalPosIntSample, nextSamplePosition = self.GetNextPositionIntSample(previousSamplePosition, nextEvent.idx)
                        if nextSamplePosition == -1:
                            break

                    else:
                        ischangeAltitudeRefined = True

                        event = fe.Flight_Event()
                        event.type = event2refine.type
                        event.latitude = startOfEventSample.latitude
                        event.longitude = startOfEventSample.longitude
                        event.altitude = startOfEventSample.altitude
                        event.course = startOfEventSample.course
                        event.timestamp = startOfEventSample.timestamp - self.telemetryBuffer.baseTime
                        event.idx = startOfEventSample.idx
                        self.globalEventNumber = self.globalEventNumber + 1
                        event.tag = self.globalEventNumber

                        refinedChangeAlltitudeEvents.append(event)

                        #Replace the refined Event into self.eventList
                        self.eventList[self.eventList.index(event2refine)] = event

                        print("Event: " + str(event.type) + " refined: height: " + str(event2refine.altitude) + " to: " + str(event.altitude))

                    index = index + 1
                if not ischangeAltitudeRefined:
                    refinedChangeAlltitudeEvents.append(event2refine)

            elif event2refine.type == et.Event_Type.EndAltChange:
                previousEvent = changeAltitudeEvents[numberOfEvent - 1] #Previous event must be an start of change altitude
                altitudeVariation = previousEvent.altitude - event2refine.altitude
                typeOfEvent = "CLIMB" if altitudeVariation < 0 else "DESCENT"
                #Get previous GlobalPositionInt sample
                previousGlobalPosIntSample, previousSamplePosition = self.GetPreviousPositionIntSample(previousEvent.idx, event2refine.idx)
                if previousSamplePosition == -1:
                    changeAltitudeEvents.append(event2refine)
                    break
                endOfEventSample = self.telemetryList[event2refine.idx]

                # Analyze the sample at the end of altitude change with the previous one to know if it contributes to the climb/descent
                # If not, analyse the second one with the third and so on.
                index = event2refine.idx
                samplesDeleted = 0
                while index >= previousEvent.idx and ischangeAltitudeRefined == False:

                    if (abs(previousEvent.altitude - endOfEventSample.altitude) >= 0.8*self.startOfAltitudeChangeThreshold) and (abs(previousGlobalPosIntSample.altitude - endOfEventSample.altitude) < self.endOfAltitudeChangeRefineThreshold * abs(altitudeVariation)):
                        samplesDeleted =+ 1
                        # This sample does not contribute to the turn
                        endOfEventSample = previousGlobalPosIntSample
                        endSamplePosition = previousSamplePosition
                        previousGlobalPosIntSample, previousSamplePosition = self.GetPreviousPositionIntSample(previousEvent.idx, endSamplePosition)
                        if previousSamplePosition == -1:
                            break

                    else:
                        ischangeAltitudeRefined = True

                        event = fe.Flight_Event()
                        event.type = event2refine.type
                        event.latitude = endOfEventSample.latitude
                        event.longitude = endOfEventSample.longitude
                        event.altitude = endOfEventSample.altitude
                        event.course = endOfEventSample.course
                        event.timestamp = endOfEventSample.timestamp - self.telemetryBuffer.baseTime
                        event.idx = endOfEventSample.idx
                        self.globalEventNumber = self.globalEventNumber + 1
                        event.tag = self.globalEventNumber

                        changeAltitudeEvents.append(event)

                        # Replace the refined Event into self.eventList
                        self.eventList[self.eventList.index(event2refine)] = event

                        print("Event: " + str(event.type) + " refined: height: " + str(event2refine.altitude) + " to: " + str(event.altitude))

                    index = index - 1
                #print("event" + str(event2refine.type) + "alt samples deleted : " + str(samplesDeleted))
                if not ischangeAltitudeRefined:
                    #The turn is not refined.
                    #self.altitude_plot.addEventPlot(event2refine.latitude,event2refine.longitude, "NOT-Refined END")
                    changeAltitudeEvents.append(event2refine)

            numberOfEvent = numberOfEvent + 1

        print("Done.")


    def getTimings(self):
        self.removeMissidentifiedCourseEvents()
        self.getHover()
        #self.printEvents()

        index = 0
        event2Remove = []
        timings = 0
        while index < len(self.eventList):
            event = self.eventList[index]
            if event.type == et.Event_Type.StartTakeOff:
                i = index + 1
                while i < len(self.eventList):
                    if self.eventList[i].type == et.Event_Type.EndAltChange:
                        break
                    else:
                        i += 1
                takeOffTime = (self.eventList[i].timestamp - event.timestamp) / 1000
                height = self.eventList[index + 1].altitude - event.altitude
                print("TO time: " + str(round(takeOffTime, 3)) + "s ; height: " + str(round(height, 3)) + " m")
                timings += takeOffTime
            elif event.type == et.Event_Type.StartHover:
                hoverTime = (self.eventList[index+1].timestamp - self.eventList[index].timestamp) / 1000
                print("HoverTime: " + str(hoverTime))
                timings += hoverTime
            if event.type == et.Event_Type.StartCourseChange or event.type == et.Event_Type.StartLanding or event.type == et.Event_Type.StartClimb or event.type == et.Event_Type.StartDescent or event.type == et.Event_Type.StartHover:
                SSTime = (event.timestamp - self.eventList[index - 1].timestamp) / 1000
                long = geo.Geodesic.WGS84.Inverse(self.eventList[index - 1].latitude, self.eventList[index - 1].longitude, event.latitude, event.longitude)['s12']
                if SSTime <= 3 and event.type != et.Event_Type.StartLanding:
                    #event2Remove.append(self.eventList[index])
                    #event2Remove.append(self.eventList[index + 1])
                    a=1
                if SSTime > 0 and event.type != et.Event_Type.StartLanding and long > 5:
                    timings += SSTime
                    print("SS time: " + str(round(SSTime, 3)) + "s ; longitude: " + str(round(abs(long), 3)) + " m")
                    if event.type == et.Event_Type.StartCourseChange:
                        turn = agl.normalizeToAcute(event.course - self.eventList[index + 1].course)
                        turnTime = (self.eventList[index + 1].timestamp - event.timestamp) / 1000
                        timings += turnTime
                        print("Turntime: " + str(round(turnTime, 3)) + " s" )
            if event.type == et.Event_Type.StartClimb or event.type == et.Event_Type.StartDescent:
                VCDTime = (self.eventList[index+1].timestamp - self.eventList[index].timestamp) / 1000
                VCDalt = self.eventList[index+1].altitude - self.eventList[index].altitude
                print("VCD: " + str(round(VCDalt,3)) + "m; " + str(VCDTime) + "s")
                timings += VCDTime
            if event.type == et.Event_Type.StartLanding:
                i = index + 1
                while i < len(self.eventList):
                    if self.eventList[i].type == et.Event_Type.EndAltChange:
                        break
                    else:
                        i += 1
                landingTime = (self.eventList[-1].timestamp - event.timestamp) / 1000
                height = self.eventList[-1].altitude - event.altitude
                print("LD time: " + str(round(landingTime, 3)) + "s ; height: " + str(round(abs(height), 3)) + " m")
                timings += landingTime


            index += 1
        for event in event2Remove:
            try:
                self.eventList.remove(event)
            except:
                eventAlreadyRemoved = True
        flightTime = (self.eventList[-1].timestamp - self.eventList[0].timestamp)/1000
        print("Total FLight time: " + str(round(flightTime, 3)) + "s ; __sum of timings: " +str(round(timings, 3)) + "s\n")
        print("FP timings computed--------------------------------------------------------------")


        #i = 0
        #t = 0
        #prevTimeStamp = self.eventList[0].timestamp
        #extraTime = 0
        #time2Subst = 0
        #while i<len(self.eventList)-1:
        #    ev = self.eventList[i]
        #    pos = self.eventList[i+1]
        #    time = (pos.timestamp - ev.timestamp) / 1000
        #    time2Subst = 0
#
        #    if prevTimeStamp>ev.timestamp:
        #        extraTime = extraTime + (prevTimeStamp-ev.timestamp)/1000
        #        time2Subst = (prevTimeStamp-ev.timestamp)/1000
        #        #print("Extratime: " + str(extraTime/1000))
#
        #    prevTimeStamp = ev.timestamp
#
        #    if time <= 0:
        #        i = i + 1
        #        continue
#
        #    time = time - time2Subst
        #    t = t + time
        #    if ev.type == et.Event_Type.StartHover and pos.type == et.Event_Type.EndCourseChange:
        #        print("Hover: " + str(time))
        #        i = i + 1
        #        continue
        #    if ev.type == et.Event_Type.StartTakeOff and pos.type == et.Event_Type.EndAltChange:
        #        alt = pos.altitude - ev.altitude
        #        print("TO: " + str(round(alt,3)) + "m; " + str(time))
        #        i = i + 1
        #        continue
        #    if ev.type == et.Event_Type.StartLanding and pos.type == et.Event_Type.EndAltChange:
        #        alt = pos.altitude - ev.altitude
        #        print("LD: " + str(round(alt,3)) + "m; " + str(time))
        #        i = i + 1
        #        continue
        #    if ev.type == et.Event_Type.StartCourseChange and pos.type == et.Event_Type.EndCourseChange:
        #        turn = pos.course - ev.course
        #        print("Turn: " +str(round(turn,3)) + "º; "+ str(time))
        #        i = i + 1
        #        continue
        #    if ev.type == et.Event_Type.StartClimb and pos.type == et.Event_Type.EndAltChange:
        #        alt = pos.altitude - ev.altitude
        #        print("Climb: " + str(round(alt,3)) + "m; " + str(time))
        #        i = i + 1
        #        continue
        #    if ev.type == et.Event_Type.StartDescent and pos.type == et.Event_Type.EndAltChange:
        #        alt = pos.altitude - ev.altitude
        #        print("Descent: " + str(round(alt,3)) + "m; " + str(time))
        #        i = i + 1
        #        continue
        #    long = geo.Geodesic.WGS84.Inverse(ev.latitude, ev.longitude,pos.latitude, pos.longitude)['s12']
        #    print("SS: " + str(round(long,3)) + "m; " + str(time))
        #    i = i + 1
        #print(t)
        #print(extraTime/1000)
        #sys.exit(0)


    def removeMissidentifiedCourseEvents(self):
        eventIndex = 0
        event2Remove = []
        '''while eventIndex < len(self.eventList) - 1:

            if self.eventList[eventIndex].type == et.Event_Type.StartCourseChange or self.eventList[eventIndex].type == et.Event_Type.StartHover:
                #print("--------------------------")
                #print("Turn:" + str(round(self.eventList[eventIndex + 1].course - self.eventList[eventIndex].course,3)))
                #index = self.eventList[eventIndex].idx
                #while index <= self.eventList[eventIndex + 1].idx:
                #    #print("course: " + str(round(self.telemetryList[index].course,3)))
                #    index += 1
                SSTime = (self.eventList[eventIndex].timestamp - self.eventList[eventIndex - 1].timestamp) / 1000
                if SSTime <= 3:
                    event2Remove.append(self.eventList[eventIndex])
                    event2Remove.append(self.eventList[eventIndex + 1])
                    eventIndex += 1
                    continue

                if abs(self.eventList[eventIndex].course - self.eventList[eventIndex + 1].course) <= 15:
                    event2Remove.append(self.eventList[eventIndex])
                    if self.eventList[eventIndex].type != et.Event_Type.StartHover:
                        event2Remove.append(self.eventList[eventIndex + 1])
            eventIndex += 1

        for event in event2Remove:
            self.eventList.remove(event)
        print("removedMissidentifiedCourseEvents")'''

        newEventList = []
        event2Remove = []
        newList = self.eventList.copy()
        # check a StartOfCourse event is incorrect at the end
        eventListIndex = len(self.eventList) - 1
        delete = False
        while (eventListIndex >= 0):

            if (self.eventList[eventListIndex].type == et.Event_Type.StartCourseChange or self.eventList[eventListIndex].type == et.Event_Type.EndCourseChange or self.eventList[eventListIndex].type == et.Event_Type.StartHover or self.eventList[eventListIndex].type == et.Event_Type.EndHover) and delete == True:
                event2Remove.append(self.eventList[eventListIndex])

            if self.eventList[eventListIndex].type == et.Event_Type.StartClimb or self.eventList[eventListIndex].type == et.Event_Type.StartLanding or self.eventList[eventListIndex].type == et.Event_Type.StartDescent:
                if self.eventList[eventListIndex-1].type == et.Event_Type.StartHover or self.eventList[eventListIndex-1].type == et.Event_Type.StartCourseChange:
                    event2Remove.append(self.eventList[eventListIndex - 1])
                delete = False
            if self.eventList[eventListIndex].type == et.Event_Type.EndAltChange:
                if eventListIndex + 1 <len(self.eventList) and self.eventList[eventListIndex+1].type == et.Event_Type.EndCourseChange:
                    event2Remove.append(self.eventList[eventListIndex+1])
                delete = True

            if self.eventList[eventListIndex].type == et.Event_Type.Takeoff:
                delete = True

            eventListIndex -= 1

        for event in event2Remove:
            try:
                newList.remove(event)
            except:
                continue

        self.eventList = newList

    def getHover(self):
        index = 0
        while index < len(self.eventList) - 1:
            ev = self.eventList[index]
            pos = self.eventList[index + 1]
            #long = geo.Geodesic.WGS84.Inverse(ev.latitude, ev.longitude, pos.latitude, pos.longitude)['s12']
            sampleIndex = ev.idx
            hoverStart = 0
            hoverLen = 0
            while sampleIndex < pos.idx:
                sample = self.telemetryList[sampleIndex]
                v = math.sqrt(sample.vx * sample.vx + sample.vy * sample.vy)
                if abs(sample.vz) < self.speed_limit and v < self.speed_limit:
                    #Hover
                    if hoverStart == 0:
                        hoverStart = sampleIndex
                        hoverLen += 1
                    else:
                        hoverLen += 1
                    sampleIndex += 1
                    continue
                if hoverStart != 0:
                    hoverEnd = sampleIndex - 1
                    if hoverLen > 10:
                        #min hover duration 10s
                        startHover = fe.Flight_Event()
                        startHover.type = et.Event_Type.StartHover
                        startHover.latitude = self.telemetryList[hoverStart].latitude
                        startHover.longitude = self.telemetryList[hoverStart].longitude
                        startHover.altitude = self.telemetryList[hoverStart].altitude
                        startHover.course = self.telemetryList[hoverStart].course
                        startHover.timestamp = self.telemetryList[hoverStart].timestamp - self.telemetryBuffer.baseTime
                        startHover.idx = self.telemetryList[hoverStart].idx
                        self.globalEventNumber = self.globalEventNumber + 1
                        startHover.tag = self.globalEventNumber

                        endHover = fe.Flight_Event()
                        endHover.type = et.Event_Type.EndHover
                        endHover.latitude = self.telemetryList[hoverEnd].latitude
                        endHover.longitude = self.telemetryList[hoverEnd].longitude
                        endHover.altitude = self.telemetryList[hoverEnd].altitude
                        endHover.course = self.telemetryList[hoverEnd].course
                        endHover.timestamp = self.telemetryList[hoverEnd].timestamp - self.telemetryBuffer.baseTime
                        endHover.idx = self.telemetryList[hoverEnd].idx
                        self.globalEventNumber = self.globalEventNumber + 1
                        endHover.tag = self.globalEventNumber

                        self.eventList.insert(index+1,endHover)
                        self.eventList.insert(index+1,startHover)
                        index += 2

                    break
                sampleIndex += 1
            index += 1

    '''def printEvents(self):
        lat = []
        lon = []
        alt1 = []
        time1 = []
        time0 = self.eventList[0].timestamp
        alt0 = self.eventList[-1].altitude
        plotEvents = dlp.dynamicLocationPlot('Latitude (deg)', 'Longitude (deg)', "Location Plot")
        plotEventsAlt = dap.dynamicAltitudePlot('T (s)', 'Altitude (m)', "Altitude plot")
        for event in self.eventList:
            if event.type == et.Event_Type.StartClimb:
                eventType = ' '
            elif event.type == et.Event_Type.StartDescent:
                eventType = ' '
            elif event.type == et.Event_Type.EndAltChange:
                eventType = ' '
            elif event.type == et.Event_Type.StartTakeOff:
                eventType = ' TakeOff'
            elif event.type == et.Event_Type.StartLanding:
                eventType = ' '
            elif event.type == et.Event_Type.StartCourseChange:
                eventType = '   '
            elif event.type == et.Event_Type.EndCourseChange:
                eventType = '   SS'
            elif event.type == et.Event_Type.StartHover:
                eventType = ' Hover'
            elif event.type == et.Event_Type.EndHover:
                eventType = ' '
            else:
                eventType = "   SS"

            if event.type == et.Event_Type.StartClimb or event.type == et.Event_Type.StartDescent or event.type == et.Event_Type.StartLanding or event.type == et.Event_Type.StartTakeOff or event.type == et.Event_Type.EndAltChange or event.type == et.Event_Type.StartHover or event.type == et.Event_Type.EndHover:
                alt1.append(event.altitude-alt0)
                time1.append((event.timestamp - time0)/1000)
                plotEventsAlt.addEventPlot(time1[-1], event.altitude-alt0, eventType)
            else:
                lat.append(event.latitude)
                lon.append(event.longitude)
                plotEvents.addEventPlot(event.latitude, event.longitude, eventType)

        plotEvents.addSamplePlot(lat, lon)
        plotEventsAlt.addSamplePlot(time1, alt1)

        eventList, timings = fpSIM.readFlightPlan(fpSIM.getFilepath())
        lat = []
        lon = []
        alt = []
        time = []
        time0 = eventList[0].timestamp
        for event in eventList:
            if event.type == et.Event_Type.StartClimb:
                eventType = ' '
            elif event.type == et.Event_Type.StartDescent:
                eventType = ' '
            elif event.type == et.Event_Type.EndAltChange:
                eventType = ' '
            elif event.type == et.Event_Type.StartTakeOff:
                eventType = ' TakeOff'
            elif event.type == et.Event_Type.StartLanding:
                eventType = ' '
            elif event.type == et.Event_Type.StartCourseChange:
                eventType = ' Turn'
            elif event.type == et.Event_Type.EndCourseChange:
                eventType = ' '
            elif event.type == et.Event_Type.StartHover:
                eventType = ' Hover'
            elif event.type == et.Event_Type.EndHover:
                eventType = ' '
            else:
                eventType = " SS"

            if event.type == et.Event_Type.StartClimb or event.type == et.Event_Type.StartDescent or event.type == et.Event_Type.StartLanding or event.type == et.Event_Type.StartTakeOff or event.type == et.Event_Type.EndAltChange or event.type == et.Event_Type.StartHover or event.type == et.Event_Type.EndHover:
                alt.append(event.altitude)
                time.append((event.timestamp - time0))
                plotEventsAlt.addEventPlot(time[-1], event.altitude, eventType,'b')
            else:
                lat.append(event.latitude)
                lon.append(event.longitude)
                plotEvents.addEventPlot(event.latitude, event.longitude, eventType,'b')

        plotEvents.addSamplePlot(lat, lon,color='b',marker='s',linestyle='dashed',linewidth=4)
        plotEventsAlt.addSamplePlot(time, alt,color='b',marker='s',linestyle='dashed',linewidth=4)

        #plotEventsAlt.addEventPlot(time[-1], alt[-1], " EndAltChange  " + str(round(time[-1],3)), 'b')
        #plotEventsAlt.addEventPlot(time1[-1], alt1[-1], " EndAltChange  " + str(round(time1[-1],3)), 'k')

        plotEvents.closeDynamicPlot("C:\\Users\Acer\Desktop\TFG\Drone-Performance-Analysis\Simulation\dlp\dlp.png")
        plotEventsAlt.closeDynamicPlot("C:\\Users\Acer\Desktop\TFG\Drone-Performance-Analysis\Simulation\dlp\dap.png")
        print("")

def CheckIfVerticalClimbDescent(flightEventStore, initialClimbDescent, levelOff, horizontalDistance2ConsiderVerticalClimbDescent):
    # Check if vertical climb/descent
    horizontalDistance = flightEventStore.telemetryList[initialClimbDescent.idx].getDistance(flightEventStore.telemetryList[levelOff.idx])
    if horizontalDistance < horizontalDistance2ConsiderVerticalClimbDescent:
        VerticalClimbDescent = True
    else:
        VerticalClimbDescent = False
    return VerticalClimbDescent,horizontalDistance
    '''

