from Mocca_Analyzer.Performance_Analyzer import Flight_Event as fe, Event_Type as et, TurnAnalysis as ta
#from Mocca_Analyzer.Plots import ClimbDescentInterpolationPlot as cdip, straightSegmentInterpolation as ssi, dynamicLocationPlot as dlp
from geographiclib import geodesic as geo
import matplotlib.pyplot as plt
import numpy as np
#import utm
import math
import sys

'''
#v1:
horizontalSpeed = 10.0

verticalSpeedUP = 2.5

verticalSpeedDN = 1.5
#v2:

horizontalSpeed = 12.5

verticalSpeedUP = 2.0

verticalSpeedDN = 2.0

#v3
horizontalSpeed = 15.0

verticalSpeedUP = 4.5

verticalSpeedDN = 3.5
'''

verticalClimbDescentThreshold = 3.0
horizontalClimbDescentThreshold = 5.0
file = "En4"
#file = "b8"
###file = "VCD_SS"
#file = "TestAllEvents"
#file = "NVCDFIN"
filepath = "C:\\Users\Acer\Desktop\TFG\Drone-Performance-Analysis\FP\\" + file + ".waypoints"

def readFlightPlan(filepath):

    #En1
    horizontalSpeed = 4.5
    verticalSpeedUP = 2.5
    verticalSpeedDN = 1.5

    #En2
    #horizontalSpeed = 4.5
    #verticalSpeedUP = 2.5
    #verticalSpeedDN = 1.0

    # En4
    #horizontalSpeed = 6.5
    #verticalSpeedUP = 2.6
    #verticalSpeedDN = 1.7

    #TestAllEvents v10
    #horizontalSpeed = 10.0
    #verticalSpeedUP = 2.5
    #verticalSpeedDN = 1.5

    # TestAllEvents v8
    #horizontalSpeed = 8.0
    #verticalSpeedUP = 2.5
    #verticalSpeedDN = 1.5

    print("File: " + filepath)
    verticalClimbDescentThreshold = 3.0
    horizontalClimbDescentThreshold = 5.0
    eventList = []
    waypoints = []
    timings = []
    file = open(filepath,"r")
    #read Lines into LineList
    lineList = file.readlines()
    #start line 1; 0 is header
    lineNumber = 1
    data = ''
    performTakeOff = False
    eventTag = 0
    previousCommand = 0
    while lineNumber < len(lineList):
        if previousCommand != 178:
            startEvent = data
        line = lineList[lineNumber]
        lineNumber += 1
        waypoints.append(line)
        data = line.split("\t")
        if len(data) < 2:
            break
        if len(data) > 8 and float(data[8]) == 0.0 and float(data[9]):
            print("Warning: Latitude and Longitude set to 0.0 in line: " + str(lineNumber))

        if (int(data[3]) == 16 or int(data[3]) == 0) and not performTakeOff:
            previousCommand = 16
            #TakeOff Command
            StartTO = fe.Flight_Event()
            StartTO.type = et.Event_Type.StartTakeOff
            StartTO.latitude = float(data[8])
            StartTO.longitude = float(data[9])
            StartTO.altitude = float(data[10])
            StartTO.course = ""
            StartTO.timestamp = 0
            StartTO.tag = eventTag
            eventTag = eventTag + 1
            eventList.append(StartTO)

            line = lineList[lineNumber]
            lineNumber += 1
            waypoints.append(line)
            data = line.split("\t")
            #next line must be 22 indicating the altitude to climb
            if int(data[3]) != 22:
                print("Error in FP 'TakeOff'")
                return -1

            deltaAltitude = float(data[10])
            takeOffCurve, takeOffOffset = cdip.getAltitudeChangeCurve("TakeOff",verticalSpeedUP)
            deltaTime = simmulateCurve(takeOffCurve, takeOffOffset, deltaAltitude)
            timings.append(deltaTime)

            event = fe.Flight_Event()
            event.type = et.Event_Type.EndAltChange
            event.latitude = StartTO.latitude
            event.longitude =StartTO.longitude
            event.altitude = StartTO.altitude + deltaAltitude
            event.course = ""
            event.timestamp = timings[-1]
            event.tag = eventTag
            eventTag = eventTag + 1
            eventList.append(event)
            data[8] = StartTO.latitude
            data[9] = StartTO.longitude
            startEvent = data

            performTakeOff =True
            print("TO: " + str(round(deltaAltitude,3)) + "m height; " + str(round(deltaTime,3)) + "s")

        elif int(data[3]) == 21:
            previousCommand = 21
            #Landing
            endEvent = data
            verticalDistance = float(startEvent[10])
            StartLanding = fe.Flight_Event()
            StartLanding.type = et.Event_Type.StartLanding
            StartLanding.latitude = float(startEvent[8])
            StartLanding.longitude = float(startEvent[9])
            StartLanding.altitude = float(startEvent[10])
            StartLanding.course = ""
            StartLanding.timestamp = timings[-1]
            StartLanding.tag = eventTag
            eventTag = eventTag + 1

            eventCurve, eventOffset = cdip.getAltitudeChangeCurve("Landing", verticalSpeedDN)
            deltaTime = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
            timings.append(deltaTime + timings[-1])

            EndLanding = fe.Flight_Event()
            EndLanding.type = et.Event_Type.EndAltChange
            EndLanding.latitude = float(startEvent[8])
            EndLanding.longitude = float(startEvent[9])
            EndLanding.altitude = float(data[10])
            EndLanding.course = ""
            EndLanding.timestamp = timings[-1]
            EndLanding.idx = ""
            EndLanding.tag = eventTag
            eventTag = eventTag + 1

            eventList.append(StartLanding)
            eventList.append(EndLanding)
            print("Landing: " + str(round(abs(verticalDistance), 3)) + "m height; " + str(round(deltaTime, 3)) + "s")


        elif int(data[3]) == 16:
            previousCommand = 16
            # waypoint command

            endEvent = data
            horizontalDistance = getDistance(float(startEvent[8]), float(startEvent[9]), float(endEvent[8]), float(endEvent[9]))
            verticalDistance = float(endEvent[10]) - float(startEvent[10])

            if abs(verticalDistance) > verticalClimbDescentThreshold:
                # case 1 ClImb/Descent
                # if altitude change(with respect to the next waypoint check horizontal distance
                previousEvent = eventList[-1]
                StartAltChange = fe.Flight_Event()

                if verticalDistance > 0:
                    StartAltChange.type = et.Event_Type.StartClimb
                else:
                    StartAltChange.type = et.Event_Type.StartDescent

                StartAltChange.latitude = float(startEvent[8])
                StartAltChange.longitude = float(startEvent[9])
                StartAltChange.altitude = float(startEvent[10])
                StartAltChange.course = ""
                StartAltChange.timestamp = timings[-1]
                StartAltChange.idx = ""
                StartAltChange.tag = eventTag
                eventTag = eventTag + 1

                EndAltChange = fe.Flight_Event()
                if horizontalDistance < horizontalClimbDescentThreshold:
                    #Vertical ClImb/Descent
                    EndAltChange.type = et.Event_Type.EndAltChange
                    if verticalDistance > 0:
                        #Climb
                        reachesVdef, At, Vdef = checkIfReachesVdef(abs(verticalDistance), verticalSpeedUP, 1.0)
                        if reachesVdef:
                            eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VC", verticalSpeedUP)
                            deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
                        else:
                            deltaTime1 = At
                            eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VC", Vdef)
                            deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
                    else:
                        reachesVdef, At, Vdef = checkIfReachesVdef(abs(verticalDistance), verticalSpeedDN, 1.0)
                        if reachesVdef:
                            eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VD", verticalSpeedDN)
                            deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
                        else:
                            deltaTime1 = At
                            eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VD", Vdef)
                            deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
                else:
                    #NON Vertical ClImb/Descent
                    EndAltChange.type = et.Event_Type.EndAltChangeNV
                    tx = horizontalDistance / horizontalSpeed
                    if verticalDistance > 0:
                        # NVC
                        tz = abs(verticalDistance) / verticalSpeedUP
                        if tz < 4 * tx:
                            # considered as a SS
                            followingEvent = getFollowingEvent(lineList, lineNumber)

                            reachesVdef, At, Vdef = checkIfReachesVdef(horizontalDistance, horizontalSpeed, 1.5)
                            if reachesVdef:
                                eventCurve, eventOffset = ssi.getSSCurve(previousEvent, followingEvent, horizontalSpeed)
                                deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, horizontalDistance)
                            else:
                                deltaTime1 = At
                                ssCurve, ssOffset = ssi.getSSCurve(previousEvent, followingEvent, Vdef)
                                deltaTimeVCD = simmulateCurve(ssCurve, ssOffset, horizontalDistance)

                        elif tz > 8 * tx:
                            # considered as a VCD
                            if verticalDistance > 0:
                                # Climb
                                reachesVdef, At, Vdef = checkIfReachesVdef(abs(verticalDistance), verticalSpeedUP, 1.0)
                                if reachesVdef:
                                    eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VC", verticalSpeedUP)
                                    deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
                                else:
                                    deltaTime1 = At
                                    eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VC", Vdef)
                                    deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
                            else:
                                reachesVdef, At, Vdef = checkIfReachesVdef(abs(verticalDistance), verticalSpeedDN, 1.0)
                                if reachesVdef:
                                    eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VD", verticalSpeedDN)
                                    deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
                                else:
                                    deltaTime1 = At
                                    eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VD", Vdef)
                                    deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))

                        else:
                            eventCurve, eventOffset = cdip.getAltitudeChangeCurve("NVC", verticalSpeedUP)
                            deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))


                    else:
                        tz = abs(verticalDistance) / verticalSpeedDN
                        if tz < 4 * tx:
                            # considered as a SS
                            followingEvent = getFollowingEvent(lineList, lineNumber)

                            reachesVdef, At, Vdef = checkIfReachesVdef(horizontalDistance, horizontalSpeed, 1.5)
                            if reachesVdef:
                                eventCurve, eventOffset = ssi.getSSCurve(previousEvent, followingEvent, horizontalSpeed)
                                deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, horizontalDistance)
                            else:
                                deltaTime1 = At
                                ssCurve, ssOffset = ssi.getSSCurve(previousEvent, followingEvent, Vdef)
                                deltaTimeVCD = simmulateCurve(ssCurve, ssOffset, horizontalDistance)
                        elif tz > 8 * tx:
                            # considered as a VCD
                            if verticalDistance > 0:
                                # Climb
                                reachesVdef, At, Vdef = checkIfReachesVdef(abs(verticalDistance), verticalSpeedUP, 1.0)
                                if reachesVdef:
                                    eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VC", verticalSpeedUP)
                                    deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
                                else:
                                    deltaTime1 = At
                                    eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VC", Vdef)
                                    deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
                            else:
                                reachesVdef, At, Vdef = checkIfReachesVdef(abs(verticalDistance), verticalSpeedDN, 1.0)
                                if reachesVdef:
                                    eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VD", verticalSpeedDN)
                                    deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
                                else:
                                    deltaTime1 = At
                                    eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VD", Vdef)
                                    deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))

                        else:
                            eventCurve, eventOffset = cdip.getAltitudeChangeCurve("NVD", verticalSpeedUP)
                            deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))

                timings.append(deltaTimeVCD + timings[-1])

                EndAltChange.latitude = float(endEvent[8])
                EndAltChange.longitude = float(endEvent[9])
                EndAltChange.altitude = float(endEvent[10])
                EndAltChange.course = ""
                EndAltChange.timestamp = timings[-1]
                EndAltChange.idx = ""
                EndAltChange.tag = eventTag
                eventTag = eventTag + 1

                '''if eventList[-1].type == et.Event_Type.EndSS or eventList[-1].type == et.Event_Type.EndAltChangeNV:
                    turn = getAngleOfTurn(previousEvent, StartAltChange, StartAltChange, EndAltChange)
                    if turn > 22.0:
                        # TODO check getTurnCurve function
                        eventCurve, eventOffset = ta.getTurnCurve()
                        deltaTime = simmulateCurve(eventCurve, eventOffset, turn)
                        timings.append(deltaTime + timings[-1])
                        # TODO lat,lon start/end turn¿?¿?¿?¿?¿?
                        StartCourseChange = fe.Flight_Event()
                        StartCourseChange.type = et.Event_Type.StartCourseChange
                        StartCourseChange.latitude = eventList[-1].latitude
                        StartCourseChange.longitude = eventList[-1].longitude
                        StartCourseChange.altitude = eventList[-1].altitude
                        StartCourseChange.course = ""
                        StartCourseChange.timestamp = timings[-1]
                        StartCourseChange.idx = ""
                        StartCourseChange.tag = eventTag
                        eventTag = eventTag + 1

                        EndCourseChange = fe.Flight_Event()
                        EndCourseChange.type = et.Event_Type.EndCourseChange
                        EndCourseChange.latitude = eventList[-1].latitude
                        EndCourseChange.longitude = eventList[-1].longitude
                        EndCourseChange.altitude = eventList[-1].altitude
                        EndCourseChange.course = ""
                        EndCourseChange.timestamp = timings[-1]
                        EndCourseChange.idx = ""
                        EndCourseChange.tag = eventTag
                        eventTag = eventTag + 1

                        eventList.append(StartCourseChange)
                        eventList.append(EndCourseChange)
                        print("Turn: " + str(round(turn, 3)) + "º; " + str(round(deltaTime, 3)) + "s")'''

                if horizontalDistance < horizontalClimbDescentThreshold:
                    print("VCD: " + str(round(verticalDistance, 3)) + "m height; " + str(round(deltaTimeVCD, 3)) + "s")
                else:
                    print("NVCD: " + str(round(verticalDistance, 3)) + "m height; hdist" + str(round(horizontalDistance, 3)) + "m height; " + str(round(deltaTimeVCD, 3)) + "s")

                eventList.append(StartAltChange)
                eventList.append(EndAltChange)

            elif abs(verticalDistance) <= verticalClimbDescentThreshold and horizontalDistance > horizontalClimbDescentThreshold:
                previousEvent = eventList[-1]
                #Straight Segment
                #1. Compute SS timing

                StartSS = fe.Flight_Event()
                StartSS.type = et.Event_Type.StartSS
                StartSS.latitude = float(startEvent[8])
                StartSS.longitude = float(startEvent[9])
                StartSS.altitude = float(startEvent[10])
                StartSS.course = ""
                StartSS.timestamp = timings[-1]
                StartSS.tag = eventTag
                eventTag = eventTag + 1

                EndSS = fe.Flight_Event()
                EndSS.type = et.Event_Type.EndSS
                EndSS.latitude = float(endEvent[8])
                EndSS.longitude = float(endEvent[9])
                EndSS.altitude = float(endEvent[10])
                EndSS.course = ""
                EndSS.tag = eventTag
                eventTag = eventTag + 1

                #2. check if the previous event was a SS or NVC/D. If yes --> turn happened
                if eventList[-1].type == et.Event_Type.EndSS or eventList[-1].type == et.Event_Type.EndAltChangeNV:
                    turn = getAngleOfTurn(eventList[-2], StartSS, StartSS, EndSS)
                    if turn > 22.0:
                        eventCurve, eventOffset = ta.getTurnCurve()
                        deltaTime = simmulateCurve(eventCurve, eventOffset, turn)
                        timings.append(deltaTime + timings[-1])
                        StartCourseChange = fe.Flight_Event()
                        StartCourseChange.type = et.Event_Type.StartCourseChange
                        StartCourseChange.latitude = eventList[-1].latitude
                        StartCourseChange.longitude = eventList[-1].longitude
                        StartCourseChange.altitude = eventList[-1].altitude
                        StartCourseChange.course = ""
                        StartCourseChange.timestamp = timings[-1]
                        StartCourseChange.idx = ""
                        StartCourseChange.tag = eventTag
                        eventTag = eventTag + 1


                        EndCourseChange = fe.Flight_Event()
                        EndCourseChange.type = et.Event_Type.EndCourseChange
                        EndCourseChange.latitude = eventList[-1].latitude
                        EndCourseChange.longitude = eventList[-1].longitude
                        EndCourseChange.altitude = eventList[-1].altitude
                        EndCourseChange.course = ""
                        EndCourseChange.timestamp = timings[-1]
                        EndCourseChange.idx = ""
                        EndCourseChange.tag = eventTag
                        eventTag = eventTag + 1

                        eventList.append(StartCourseChange)
                        eventList.append(EndCourseChange)
                        previousEvent = EndCourseChange
                        print("Turn: " + str(round(turn, 3)) + "º; " + str(round(deltaTime, 3)) + "s")

                followingEvent = getFollowingEvent(lineList, lineNumber)

                reachesVdef, At, Vdef = checkIfReachesVdef(horizontalDistance, horizontalSpeed, 1.5)
                if reachesVdef:
                    ssCurve, ssOffset = ssi.getSSCurve(previousEvent, followingEvent, horizontalSpeed)
                    deltaTime = simmulateCurve(ssCurve, ssOffset, horizontalDistance)
                else :
                    deltaTime1 = At
                    ssCurve, ssOffset = ssi.getSSCurve(previousEvent, followingEvent, Vdef)
                    deltaTime = simmulateCurve(ssCurve, ssOffset, horizontalDistance)

                timings.append(deltaTime + timings[-1])
                EndSS.timestamp = timings[-1]

                print("SS: " + str(round(horizontalDistance, 3)) + "m; " + str(round(deltaTime, 3)) + "s")
                eventList.append(StartSS)
                eventList.append(EndSS)

        elif int(data[3]) == 19:
            previousCommand = 19
            #reach the waypoint and delay. 8lat, 9lon, 10alt
            # waypoint command

            endEvent = data
            horizontalDistance = getDistance(float(startEvent[8]), float(startEvent[9]), float(endEvent[8]),
                                             float(endEvent[9]))
            verticalDistance = float(endEvent[10]) - float(startEvent[10])

            if abs(verticalDistance) > verticalClimbDescentThreshold:
                # case 1 ClImb/Descent
                # if altitude change(with respect to the next waypoint check horizontal distance
                previousEvent = eventList[-1]
                StartAltChange = fe.Flight_Event()

                if verticalDistance > 0:
                    StartAltChange.type = et.Event_Type.StartClimb
                else:
                    StartAltChange.type = et.Event_Type.StartDescent

                StartAltChange.latitude = float(startEvent[8])
                StartAltChange.longitude = float(startEvent[9])
                StartAltChange.altitude = float(startEvent[10])
                StartAltChange.course = ""
                StartAltChange.timestamp = timings[-1]
                StartAltChange.idx = ""
                StartAltChange.tag = eventTag
                eventTag = eventTag + 1

                EndAltChange = fe.Flight_Event()
                if horizontalDistance < horizontalClimbDescentThreshold:
                    # Vertical ClImb/Descent
                    EndAltChange.type = et.Event_Type.EndAltChange
                    if verticalDistance > 0:
                        # Climb
                        reachesVdef, At, Vdef = checkIfReachesVdef(abs(verticalDistance), verticalSpeedUP, 1.0)
                        if reachesVdef:
                            eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VC", verticalSpeedUP)
                            deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
                        else:
                            deltaTime1 = At
                            eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VC", Vdef)
                            deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
                    else:
                        reachesVdef, At, Vdef = checkIfReachesVdef(abs(verticalDistance), verticalSpeedDN, 1.0)
                        if reachesVdef:
                            eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VD", verticalSpeedDN)
                            deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
                        else:
                            deltaTime1 = At
                            eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VD", Vdef)
                            deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
                else:
                    # NON Vertical ClImb/Descent
                    EndAltChange.type = et.Event_Type.EndAltChangeNV
                    tx = horizontalDistance / horizontalSpeed
                    if verticalDistance > 0:
                        #NVC
                        tz = abs(verticalDistance) / verticalSpeedUP
                        if tz < 4 * tx:
                            # considered as a SS
                            followingEvent = getFollowingEvent(lineList, lineNumber)

                            reachesVdef, At, Vdef = checkIfReachesVdef(horizontalDistance, horizontalSpeed, 1.5)
                            if reachesVdef:
                                eventCurve, eventOffset = ssi.getSSCurve(previousEvent, followingEvent, horizontalSpeed)
                                deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, horizontalDistance)
                            else:
                                deltaTime1 = At
                                ssCurve, ssOffset = ssi.getSSCurve(previousEvent, followingEvent, Vdef)
                                deltaTimeVCD = simmulateCurve(ssCurve, ssOffset, horizontalDistance)
                        elif tx < 8 * tz:
                            # considered as a VCD
                            if verticalDistance > 0:
                                # Climb
                                reachesVdef, At, Vdef = checkIfReachesVdef(abs(verticalDistance), verticalSpeedUP, 1.0)
                                if reachesVdef:
                                    eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VC", verticalSpeedUP)
                                    deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
                                else:
                                    deltaTime1 = At
                                    eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VC", Vdef)
                                    deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
                            else:
                                reachesVdef, At, Vdef = checkIfReachesVdef(abs(verticalDistance), verticalSpeedDN, 1.0)
                                if reachesVdef:
                                    eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VD", verticalSpeedDN)
                                    deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
                                else:
                                    deltaTime1 = At
                                    eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VD", Vdef)
                                    deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))

                        else:
                            eventCurve, eventOffset = cdip.getAltitudeChangeCurve("NVC", verticalSpeedUP)
                            deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
                    else:
                        tz = abs(verticalDistance) / verticalSpeedDN
                        if tz < 4 * tx:
                            # considered as a SS
                            followingEvent = getFollowingEvent(lineList, lineNumber)

                            reachesVdef, At, Vdef = checkIfReachesVdef(horizontalDistance, horizontalSpeed, 1.5)
                            if reachesVdef:
                                eventCurve, eventOffset = ssi.getSSCurve(previousEvent, followingEvent, horizontalSpeed)
                                deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, horizontalDistance)
                            else:
                                deltaTime1 = At
                                ssCurve, ssOffset = ssi.getSSCurve(previousEvent, followingEvent, Vdef)
                                deltaTimeVCD = simmulateCurve(ssCurve, ssOffset, horizontalDistance)
                        elif tx < 8 * tz:
                            # considered as a VCD
                            if verticalDistance > 0:
                                # Climb
                                reachesVdef, At, Vdef = checkIfReachesVdef(abs(verticalDistance), verticalSpeedUP, 1.0)
                                if reachesVdef:
                                    eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VC", verticalSpeedUP)
                                    deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
                                else:
                                    deltaTime1 = At
                                    eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VC", Vdef)
                                    deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
                            else:
                                reachesVdef, At, Vdef = checkIfReachesVdef(abs(verticalDistance), verticalSpeedDN, 1.0)
                                if reachesVdef:
                                    eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VD", verticalSpeedDN)
                                    deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))
                                else:
                                    deltaTime1 = At
                                    eventCurve, eventOffset = cdip.getAltitudeChangeCurve("VD", Vdef)
                                    deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))

                        else:
                            eventCurve, eventOffset = cdip.getAltitudeChangeCurve("NVD", verticalSpeedUP)
                            deltaTimeVCD = simmulateCurve(eventCurve, eventOffset, abs(verticalDistance))

                timings.append(deltaTimeVCD + timings[-1])

                EndAltChange.latitude = float(endEvent[8])
                EndAltChange.longitude = float(endEvent[9])
                EndAltChange.altitude = float(endEvent[10])
                EndAltChange.course = ""
                EndAltChange.timestamp = timings[-1]
                EndAltChange.idx = ""
                EndAltChange.tag = eventTag
                eventTag = eventTag + 1

                if eventList[-1].type == et.Event_Type.EndSS or eventList[-1].type == et.Event_Type.EndAltChangeNV:
                    turn = getAngleOfTurn(previousEvent, StartAltChange, StartAltChange, EndAltChange)
                    if turn > 22.0:

                        eventCurve, eventOffset = ta.getTurnCurve()
                        deltaTime = simmulateCurve(eventCurve, eventOffset, turn)
                        timings.append(deltaTime + timings[-1])

                        StartCourseChange = fe.Flight_Event()
                        StartCourseChange.type = et.Event_Type.StartCourseChange
                        StartCourseChange.latitude = eventList[-1].latitude
                        StartCourseChange.longitude = eventList[-1].longitude
                        StartCourseChange.altitude = eventList[-1].altitude
                        StartCourseChange.course = ""
                        StartCourseChange.timestamp = timings[-1]
                        StartCourseChange.idx = ""
                        StartCourseChange.tag = eventTag
                        eventTag = eventTag + 1

                        EndCourseChange = fe.Flight_Event()
                        EndCourseChange.type = et.Event_Type.EndCourseChange
                        EndCourseChange.latitude = eventList[-1].latitude
                        EndCourseChange.longitude = eventList[-1].longitude
                        EndCourseChange.altitude = eventList[-1].altitude
                        EndCourseChange.course = ""
                        EndCourseChange.timestamp = timings[-1]
                        EndCourseChange.idx = ""
                        EndCourseChange.tag = eventTag
                        eventTag = eventTag + 1

                        eventList.append(StartCourseChange)
                        eventList.append(EndCourseChange)
                        print("Turn: " + str(round(turn, 3)) + "º; " + str(round(deltaTime, 3)) + "s")

                if horizontalDistance < horizontalClimbDescentThreshold:
                    print("VCD: " + str(round(verticalDistance, 3)) + "m height; " + str(round(deltaTimeVCD, 3)) + "s")
                else:
                    print("NVCD: " + str(round(verticalDistance, 3)) + "m height; hdist"+ str(round(horizontalDistance, 3)) + "m height; " + str(round(deltaTimeVCD, 3)) + "s")

                eventList.append(StartAltChange)
                eventList.append(EndAltChange)

            elif abs(verticalDistance) <= verticalClimbDescentThreshold and horizontalDistance > horizontalClimbDescentThreshold:
                previousEvent = eventList[-1]
                # Straight Segment
                # 1. Compute SS timing

                StartSS = fe.Flight_Event()
                StartSS.type = et.Event_Type.StartSS
                StartSS.latitude = float(startEvent[8])
                StartSS.longitude = float(startEvent[9])
                StartSS.altitude = float(startEvent[10])
                StartSS.course = ""
                StartSS.timestamp = timings[-1]
                StartSS.tag = eventTag
                eventTag = eventTag + 1

                EndSS = fe.Flight_Event()
                EndSS.type = et.Event_Type.EndSS
                EndSS.latitude = float(endEvent[8])
                EndSS.longitude = float(endEvent[9])
                EndSS.altitude = float(endEvent[10])
                EndSS.course = ""
                EndSS.tag = eventTag
                eventTag = eventTag + 1

                # 2. check if the previous event was a SS or NVC/D. If yes --> turn happened
                if eventList[-1].type == et.Event_Type.EndSS or eventList[-1].type == et.Event_Type.EndAltChangeNV:
                    turn = getAngleOfTurn(eventList[-2], StartSS, StartSS, EndSS)
                    if turn > 22.0:

                        eventCurve, eventOffset = ta.getTurnCurve()
                        deltaTime = simmulateCurve(eventCurve, eventOffset, turn)
                        timings.append(deltaTime + timings[-1])

                        StartCourseChange = fe.Flight_Event()
                        StartCourseChange.type = et.Event_Type.StartCourseChange
                        StartCourseChange.latitude = eventList[-1].latitude
                        StartCourseChange.longitude = eventList[-1].longitude
                        StartCourseChange.altitude = eventList[-1].altitude
                        StartCourseChange.course = ""
                        StartCourseChange.timestamp = timings[-1]
                        StartCourseChange.idx = ""
                        StartCourseChange.tag = eventTag
                        eventTag = eventTag + 1

                        EndCourseChange = fe.Flight_Event()
                        EndCourseChange.type = et.Event_Type.EndCourseChange
                        EndCourseChange.latitude = eventList[-1].latitude
                        EndCourseChange.longitude = eventList[-1].longitude
                        EndCourseChange.altitude = eventList[-1].altitude
                        EndCourseChange.course = ""
                        EndCourseChange.timestamp = timings[-1]
                        EndCourseChange.idx = ""
                        EndCourseChange.tag = eventTag
                        eventTag = eventTag + 1

                        eventList.append(StartCourseChange)
                        eventList.append(EndCourseChange)
                        previousEvent = EndCourseChange
                        print("Turn: " + str(round(turn, 3)) + "º; " + str(round(deltaTime, 3)) + "s")

                followingEvent = getFollowingEvent(lineList, lineNumber)

                reachesVdef, At, Vdef = checkIfReachesVdef(horizontalDistance, horizontalSpeed, 1.5)
                if reachesVdef:
                    ssCurve, ssOffset = ssi.getSSCurve(previousEvent, followingEvent, horizontalSpeed)
                    deltaTime = simmulateCurve(ssCurve, ssOffset, horizontalDistance)
                else:
                    deltaTime1 = At
                    ssCurve, ssOffset = ssi.getSSCurve(previousEvent, followingEvent, Vdef)
                    deltaTime = simmulateCurve(ssCurve, ssOffset, horizontalDistance)

                timings.append(deltaTime + timings[-1])
                EndSS.timestamp = timings[-1]

                print("SS: " + str(round(horizontalDistance, 3)) + "m; " + str(round(deltaTime, 3)) + "s")
                eventList.append(StartSS)
                eventList.append(EndSS)

            #waypoint reached
            # now hover
            # Delay. Hover
            previousEvent = eventList[-1]
            StartHover = fe.Flight_Event()
            StartHover.type = et.Event_Type.StartHover
            StartHover.latitude = previousEvent.latitude
            StartHover.longitude = previousEvent.longitude
            StartHover.altitude = previousEvent.altitude
            StartHover.course = ""
            StartHover.timestamp = timings[-1]
            StartHover.idx = ""
            StartHover.tag = eventTag
            eventTag = eventTag + 1

            deltaTime = float(data[4])
            timings.append(deltaTime + timings[-1])

            EndHover = fe.Flight_Event()
            EndHover.type = et.Event_Type.EndHover
            EndHover.latitude = previousEvent.latitude
            EndHover.longitude = previousEvent.longitude
            EndHover.altitude = previousEvent.altitude
            EndHover.course = ""
            EndHover.timestamp = timings[-1]
            EndHover.idx = ""
            EndHover.tag = eventTag
            eventTag = eventTag + 1

            print("Hover c19: " + str(deltaTime) + "s")
            eventList.append(StartHover)
            eventList.append(EndHover)

        elif int(data[3]) == 93:
            previousCommand = 93
            #Delay. Hover
            previousEvent = eventList[-1]
            StartHover = fe.Flight_Event()
            StartHover.type = et.Event_Type.StartHover
            StartHover.latitude = previousEvent.latitude
            StartHover.longitude = previousEvent.longitude
            StartHover.altitude = previousEvent.altitude
            StartHover.course = ""
            StartHover.timestamp = timings[-1]
            StartHover.idx = ""
            StartHover.tag = eventTag
            eventTag = eventTag + 1

            deltaTime = float(data[4])
            timings.append(deltaTime + timings[-1])

            EndHover = fe.Flight_Event()
            EndHover.type = et.Event_Type.EndHover
            EndHover.latitude = previousEvent.latitude
            EndHover.longitude = previousEvent.longitude
            EndHover.altitude = previousEvent.altitude
            EndHover.course = ""
            EndHover.timestamp = timings[-1]
            EndHover.idx = ""
            EndHover.tag = eventTag
            eventTag = eventTag + 1

            print("Hover: " + str(EndHover.timestamp - StartHover.timestamp) + "s")
            eventList.append(StartHover)
            eventList.append(EndHover)

        elif int(data[3]) == 178:
            previousCommand = 178
            #Change speed
            if int(float(data[4])) == 1.0:
                #Airspeed
                horizontalSpeed = float(data[5])
                print("Horizontal Speed changed to :" + str(horizontalSpeed))
            elif int(float(data[4])) < 2.1:
                #Climb Speed
                verticalSpeedUP = float(data[5])
                print("verticalSpeedUP Speed changed to :" + str(verticalSpeedUP))
            elif int(float(data[4])) < 3.1:
                # Descent Speed
                verticalSpeedDN = float(data[5])
                print("verticalSpeedDN Speed changed to :" + str(verticalSpeedDN))
            else:
                print("Speed change error")

    return eventList, timings

def simmulateCurve(curve, Offset, data):
    deltaTime = 0

    if curve[0] == '1':
        a = curve[1]
        b = curve[2] + Offset
        deltaTime = a * data + b
    elif curve[0] == '2':
        a = curve[1]
        b = curve[2]
        c = curve[3] + Offset
        deltaTime = a * data + b * data * data + c
    elif curve[0] == '3':
        a = curve[1]
        b = curve[2]
        c = curve[3]
        d = curve[4] + Offset
        deltaTime = a * data + b * data * data + c * data * data * data + d

    return deltaTime

def getDistance(latA,lonA,latB,lonB):
    distance = geo.Geodesic.WGS84.Inverse(latA,lonA,latB,lonB)
    return distance['s12']

def getAngleOfTurn(startSS1, endSS1, startSS2, endSS2):
    coordinatesI1= utm.from_latlon(startSS1.latitude, startSS1.longitude)
    coordinatesF1 = utm.from_latlon(endSS1.latitude, endSS1.longitude)

    coordinatesI2 = utm.from_latlon(startSS2.latitude, startSS2.longitude)
    coordinatesF2 = utm.from_latlon(endSS2.latitude, endSS2.longitude)

    dx1 = coordinatesF1[0] - coordinatesI1[0]
    dy1 = coordinatesF1[1] - coordinatesI1[1]
    dx2 = coordinatesF2[0] - coordinatesI2[0]
    dy2 = coordinatesF2[1] - coordinatesI2[1]
    angle1 = math.atan2(dy1, dx1)
    angle1 = int(angle1 * 180 / math.pi)
    angle2 = math.atan2(dy2, dx2)
    angle2 = int(angle2 * 180 / math.pi)
    if angle1 * angle2 >= 0:
        insideAngle = abs(angle1 - angle2)
    else:
        insideAngle = abs(angle1) + abs(angle2)
        if insideAngle > 180:
            insideAngle = 360 - insideAngle
    insideAngle = insideAngle % 180
    return insideAngle

def getFollowingEvent(lineList, lineNumber):
    line = lineList[lineNumber]
    data = line.split("\t")
    if int(data[3]) == 178:
        line = lineList[lineNumber+1]
        data = line.split("\t")
    if int(data[3]) == 21:
        # LandingCommand
        return "VCD"
    if int(data[3]) == 93:
        # hover
        return "Hover"

    elif int(data[3]) == 16:
        # waypoint command
        startEvent = data
        line = lineList[lineNumber + 1]
        data = line.split("\t")
        # next line must be 16
        endEvent = data
        horizontalDistance = getDistance(float(startEvent[8]), float(startEvent[9]), float(endEvent[8]),
                                         float(endEvent[9]))
        verticalDistance = float(endEvent[10]) - float(startEvent[10])

        if abs(verticalDistance) > verticalClimbDescentThreshold:
            # case 1 ClImb/Descent
            # if altitude change(with respect to the next waypoint check horizontal distance

            if horizontalDistance < horizontalClimbDescentThreshold:
                # Vertical ClImb/Descent
                return "VCD"
            else:
                # NON Vertical ClImb/Descent
                return "Turn"

        elif abs(verticalDistance) <= verticalClimbDescentThreshold and horizontalDistance > horizontalClimbDescentThreshold:
            # Straight Segment
            return "Turn"

        elif abs(verticalDistance) <= verticalClimbDescentThreshold and horizontalDistance <= horizontalClimbDescentThreshold:
            return "Hover"
    else:
        return "Error"

def checkIfReachesVdef(segmentLength, Vdef, acc):
    Ax = (Vdef*Vdef)/(2*acc)
    if Ax >segmentLength/2:
        #drone cannot reach Vdef
        #vmax that it can reach in segment with acc
        Vf = math.sqrt(acc * segmentLength)
        At = (2*segmentLength/2)/Vf
        reachesVdef = False
    else:
        reachesVdef = True
        At = -1
        Vf = 0
    return reachesVdef, At, Vf

def getFilepath():
    return filepath

if __name__ == '__main__':

    eventList, timings = readFlightPlan(filepath)
    print("Total flight time: " + str(round(timings[-1], 3)) + " s\n")

    figTO = plt.figure()
    # ggplot style
    plt.style.use('ggplot')
    axTO = figTO.add_subplot(111)
    axTO.set_xlabel('Longitude (deg)')
    axTO.set_ylabel('Latitude (deg)')

    lon = []
    lat = []
    SScounter = 0
    for event in eventList:
        lon.append(event.longitude)
        lat.append(event.latitude)

        axTO.text(event.longitude, event.latitude, str(event.type), size=15, zorder=1, color='k')
        if event.type == et.Event_Type.StartSS:
            SScounter += 1
    #print("Number of SS: " + str(SScounter))

    axTO.plot(lon, lat, color='red')

    plt.title(" Altitude variation[Ah] vs Time variation[At]")
    plt.show()
    figTO.set_size_inches(14.0, 14.0)
    figTO.savefig("C:\\Users\Acer\Desktop\TFG\Drone-Performance-Analysis\FP\Plots\dlp" + file + ".png")
    plt.close(figTO)

    print("Done.\n")
    sys.exit(0)