import enum

class Event_Type(enum.Enum):
  Unknown = 0
  Takeoff = 1
  Land = 2
  StartClimb = 3
  StartDescent = 4
  EndAltChange = 5
  StartCourseChange = 6
  EndCourseChange = 7
  StartVAcceleration = 8
  StartVDeceleration = 9
  EndVSpeedChange = 10
  StartHover = 11
  EndHover = 12
  StartHAcceleration = 13
  StartHDeceleration = 14
  EndHSpeedChange = 15
  StartTakeOff = 16
  StartLanding = 17
  StartSS = 18
  EndSS = 19
  EndAltChangeNV = 20

