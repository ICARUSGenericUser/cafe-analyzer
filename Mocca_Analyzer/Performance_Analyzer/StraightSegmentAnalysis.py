import math as math
from Mocca_Analyzer.Performance_Analyzer import Event_Type as et
from Mocca_Analyzer.Mocca import Mocca_Object as mo
from geographiclib import geodesic as geo
from Mocca_Analyzer.Plots import StraightSegmentPlot as ssp, dynamicLocationPlot as dlp

def analyzeStraightSegmentCourseEvents(flightEventStore, path, axsspGlobal, SSLentghVariance, SSDeltaTime, SSTag, SSPrePostEvents):
    initialStraightSegment= None
    endStraightSegment = None
    straightSegmentxParam = []
    straightSegmentyParam = []
    previousEvents = SSPrePostEvents[0]
    postEvents = SSPrePostEvents[1]
    print("Analysing Straight Segment Events")
    nOfStraightSegments = 0

    axssp, figssp = ssp.getDynamicStraightSegmentPlot()  # Straight Segment plot. One x file

    for event in flightEventStore.eventList:

        if hasattr(event,'course') and event.type == et.Event_Type.EndCourseChange or event.type == et.Event_Type.EndAltChange or event.type == et.Event_Type.EndHover:
            #Check that event has attriute course
            #End of turn or End of climb/descent or Enf of hover can be a start of StraightSegment
            initialStraightSegment = event
            continue

        if hasattr(event,'course') and event.type == et.Event_Type.StartCourseChange or event.type == et.Event_Type.StartClimb or event.type == et.Event_Type.StartDescent or event.type == et.Event_Type.StartLanding or event.type == et.Event_Type.StartHover:
            # Check that event has attriute course
            # Start of turn or Start of climb/descent or Start of Hover can be a start of StraightSegment
            endStraightSegment = event


            if initialStraightSegment != None:
                isStraightSegment, segmentLength = CheckIfStraightSegment(initialStraightSegment,endStraightSegment)

                # Check that is a Straight Segment(No Altitude Change) and its larger than 5 m  && check continuity  && check that difference of course is lower of 5 degrees
                if isStraightSegment and segmentLength > 5 and initialStraightSegment.timestamp < endStraightSegment.timestamp:
                    #print("Initial Alt: ", initialStraightSegment.altitude, " InLon: ", initialStraightSegment.longitude, " InLat: ", initialStraightSegment.latitude)
                    #print("Final Alt: ", endStraightSegment.altitude, " FnLon: ",endStraightSegment.longitude, " FinLat: ", endStraightSegment.latitude)
                    #print("Straight Segment distance: " + str(segmentLength), " m")
                    #print("Straight Segment duration: " + str((endStraightSegment.timestamp - initialStraightSegment.timestamp) / 1000) + " sec")

                    segmentSpeedArray, segmentLengthArray, nOfSample = getSegmentSpeedArray(flightEventStore, initialStraightSegment, endStraightSegment)
                    #Segment Speed: Speed programmed to reach in the segment ≈ Max(Speed)
                    SegmentSpeed = max(segmentSpeedArray)
                    #Normalize the speed array by the Segment Speed
                    segmentSpeedArrayNormalized = []
                    for sample in segmentSpeedArray:
                        segmentSpeedArrayNormalized.append(sample/SegmentSpeed)

                    #Tag the segment in dynamiclocationPlot
                    flightEventStore.location_plot.addStraightSegmentSampleText(initialStraightSegment, endStraightSegment,nOfStraightSegments,SegmentSpeed, flightEventStore)

                    #Global StraightSegment Plot: One plot x folder
                    #ssp.addGlobalPlot(axsspGlobal, segmentLengthArray, segmentSpeedArrayNormalized)
                    #Individual StraightSegment Plot: One plot x file
                    ssp.addEventPlot(axssp,segmentLengthArray,segmentSpeedArrayNormalized,round(segmentLength,3), initialStraightSegment, endStraightSegment, nOfSample, flightEventStore, SegmentSpeed, nOfStraightSegments)

                    deltaLength = getDistance(flightEventStore.telemetryList[initialStraightSegment.idx], flightEventStore.telemetryList[endStraightSegment.idx])
                    deltaTime = (flightEventStore.telemetryList[endStraightSegment.idx].timestamp - flightEventStore.telemetryList[initialStraightSegment.idx].timestamp) / 1000
                    SSLentghVariance.append(deltaLength)
                    SSDeltaTime.append(deltaTime)
                    previousEvents.append(initialStraightSegment)
                    postEvents.append(endStraightSegment)
                    altPlot = endStraightSegment.altitude - initialStraightSegment.altitude
                    ssp.addEventPlot2DeltaLenghtDeltaTime(axsspGlobal, deltaLength, deltaTime, altPlot)
                    if initialStraightSegment.tag == 101 or initialStraightSegment.tag== 89 or initialStraightSegment.tag == 992 or initialStraightSegment.tag == 994 or initialStraightSegment.tag == 85:
                        print("TAG : " + str(initialStraightSegment.tag) + "   " + path)
                        print("initial : " + str(initialStraightSegment.type) + ";  final  : " + str(endStraightSegment.type))
                        print("---------------------------------------------")
                    SSTag.append(initialStraightSegment.tag)

                    nOfStraightSegments = nOfStraightSegments + 1

                initialStraightSegment = None
                endStraightSegment = None

                continue

    SSPrePostEvents = []
    SSPrePostEvents.append(previousEvents)
    SSPrePostEvents.append(postEvents)
    ssp.closeDynamicPlot(figssp,path + "--StraightSegment-LengthVelocity.png")


def CheckIfStraightSegment(initialStraightSegment, endStraightSegment):
    if(abs(initialStraightSegment.altitude - endStraightSegment.altitude) < 15):
        isStraightSegment = True
        segmentLength = geo.Geodesic.WGS84.Inverse(initialStraightSegment.latitude, initialStraightSegment.longitude, endStraightSegment.latitude, endStraightSegment.longitude)['s12']
    else:
        isStraightSegment = False
        segmentLength = 0
    return isStraightSegment, segmentLength

def getSegmentSpeedArray(flightEventStore, initialEvent, finalEvent):

    segmentLength = geo.Geodesic.WGS84.Inverse(flightEventStore.telemetryList[initialEvent.idx].latitude, flightEventStore.telemetryList[initialEvent.idx].longitude, finalEvent.latitude, finalEvent.longitude)['s12']
    velocityArray = []
    lengthArray = []
    nOfSample = 0
    index = initialEvent.idx
    while index <= finalEvent.idx:
        sample = flightEventStore.telemetryList[index]
        if sample.type == mo.Object_Type.mlGlobalPositionInt:
            velocity = sample.hspeed  #cm/s
            velocityArray.append(velocity)
            distance2EndOfSegment = geo.Geodesic.WGS84.Inverse(sample.latitude, sample.longitude, finalEvent.latitude, finalEvent.longitude)['s12']
            lengthArray.append(distance2EndOfSegment / segmentLength)
            nOfSample += 1
        #if sample.type == mo.Object_Type.mlGPSRawInt:
        #    velocity = sample.hspeed  # m/s
        #    velocityArray.append(velocity)
        #    distance2EndOfSegment =geo.Geodesic.WGS84.Inverse(sample.latitude, sample.longitude, finalEvent.latitude, finalEvent.longitude)['s12']
        #    lengthArray.append(distance2EndOfSegment / segmentLength)
        #elif sample.type == mo.Object_Type.mlVfrHud:
        #    velocity = sample.airspeed #Airpeed m/s

        index += 1
    return velocityArray,lengthArray, nOfSample

def getDistance(sampleA, sampleB):
    distance = geo.Geodesic.WGS84.Inverse(sampleA.latitude, sampleA.longitude, sampleB.latitude, sampleB.longitude)
    return distance['s12']