from Mocca_Analyzer.Performance_Analyzer import Event_Type as et


class Flight_Event:
  def __init__(self):
    self.type = et.Event_Type.Unknown
    self.idx = -1
    self.timestamp = 0
    self.date = None
    self.latitude = 0
    self.longitude = 0
    self.altitude = 0
    self.course = 0
    self.hspeed = 0
    self.vspeed = 0
    self.tendency = 0
    self.tag = 0

