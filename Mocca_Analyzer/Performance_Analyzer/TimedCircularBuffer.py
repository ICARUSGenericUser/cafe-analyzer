from Mocca_Analyzer.Mocca import Mocca_Object as mo
from Mocca_Analyzer.Performance_Analyzer import CircularBuffer as cb


class TimedCircularBuffer:

    def __init__(self, maxTimeDiff=3000):
        self.headTime = 0
        self.tailTime = 0
        self.baseTime = 0
        self.positionalSamples = 0
        self.maxTimeDiff = maxTimeDiff
        self.nPosSample = 0
        self.buffer = cb.CircularBuffer(max_size=40)


    def insertSample(self, sample):

        #print("Inserting sample with timeStamp: ", sample.timestamp)

        if self.baseTime == 0:
            self.baseTime = sample.timestamp

        while not self.buffer.is_empty() and self.buffer.get_head().timestamp + self.maxTimeDiff < sample.timestamp:
            discarded = self.buffer.dequeue()
            #print("Discarding sample with timeStamp: ", discarded.timestamp)

            if discarded.type == mo.Object_Type.basic or discarded.type == mo.Object_Type.swGNSSNavigation or discarded.type == mo.Object_Type.mlGlobalPositionInt or discarded.type == mo.Object_Type.mlGPSRawInt:
                self.positionalSamples -= 1

        self.buffer.enqueue(sample)
        #print("Queue contains: ", self.buffer.size(), " samples.  Mysize: ", self.buffer.get_mysize())
        head = self.buffer.get_head()
        tail = self.buffer.get_tail()
        #print("Head timeStamp: ", head.timestamp, " Tail timestamp: ", tail.timestamp, " Differential time: ", tail.timestamp - head.timestamp)

        if sample.type == mo.Object_Type.basic or sample.type == mo.Object_Type.swGNSSNavigation or sample.type == mo.Object_Type.mlGlobalPositionInt or sample.type == mo.Object_Type.mlGPSRawInt:
            self.positionalSamples += 1

        #print("Total positional samples: ", self.positionalSamples)


    def getTimeRange(self):
        head = self.buffer.get_head()
        tail = self.buffer.get_tail()
        timerange = tail.timestamp - head.timestamp
        return timerange


    def getPositional(self):
        return self.positionalSamples


    def getHeadPositional(self):
        index = self.buffer.get_head_index()
        size = self.buffer.size()

        while size > 0:
            sample = self.buffer.peek(index)

            if sample.type == mo.Object_Type.basic or mo.Object_Type.swGNSSNavigation or sample.type == mo.Object_Type.mlGlobalPositionInt or sample.type == mo.Object_Type.mlGPSRawInt:
                return sample

            index = self.buffer.next_position(index)
            size -= 1

        return None


    def getTailPositional(self):
        index = self.buffer.get_tail_index()
        size = self.buffer.size()

        while size > 0:
            sample = self.buffer.peek(index)

            if sample.type == mo.Object_Type.basic or mo.Object_Type.swGNSSNavigation or sample.type == mo.Object_Type.mlGlobalPositionInt or sample.type == mo.Object_Type.mlGPSRawInt:
                return sample

            index = self.buffer.prev_position(index)
            size -= 1

        return None


    def getPreTailPositional(self):
        index = self.buffer.get_tail_index() - 1
        size = self.buffer.size()

        while size > 0:
            sample = self.buffer.peek(index)

            if sample.type == mo.Object_Type.basic or mo.Object_Type.swGNSSNavigation or sample.type == mo.Object_Type.mlGlobalPositionInt or sample.type == mo.Object_Type.mlGPSRawInt:
                return sample

            index = self.buffer.prev_position(index)
            size -= 1

        return None


    def remainsAsHold(self, speedLimit):
        index = self.buffer.get_head_index()
        size = self.buffer.size()

        while size > 0:
            sample = self.buffer.peek(index)

            if sample.type == (mo.Object_Type.basic or mo.Object_Type.mlGlobalPositionInt) and (abs(sample.hspeed) > speedLimit or abs(sample.vz) > speedLimit):
                return False

            index = self.buffer.next_position(index)
            size -= 1

        return True


    def departsFromHold(self, speedLimit):
        index = self.buffer.get_head_index()
        size = self.buffer.size()

        while size > 0:
            sample = self.buffer.peek(index)

            if sample.type == (mo.Object_Type.basic or mo.Object_Type.mlGlobalPositionInt) and sample.hspeed < speedLimit:
                return False

            index = self.buffer.next_position(index)
            size -= 1

        return True


    def checkTendency(self, tendencyFunction, desiredTendency):
        index = self.buffer.get_head_index()
        size = self.buffer.size()

        currentSample = None
        prevSample = None
        while size > 0:
            currentSample = self.buffer.peek(index)

            if currentSample.type == mo.Object_Type.mlGlobalPositionInt:

                if (prevSample != None) and tendencyFunction(prevSample, currentSample) == desiredTendency:
                    return True

                prevSample = currentSample

            index = self.buffer.next_position(index)
            size -= 1

        return False

