import math as math
import numpy as np
import matplotlib.pyplot as plt
from Mocca_Analyzer.Performance_Analyzer import Event_Type as et
from Mocca_Analyzer.Mocca import Mocca_Object as mo
from geographiclib import geodesic as geo
from scipy.optimize import curve_fit
import warnings
from Mocca_Analyzer.Plots import TurnPlot as tp, dynamicLocationPlot as dlp

# y = 2.72958E-02 * x + -4.41052E-05 * xx  + -6.36423E-01
turnCurve = ['2',2.72958E-02, -4.41052E-05, -6.36423E-01]
turnOffset = 0

def analyzeTurnEvents(flightEventStore, axtp, path):

    computeAndPlotTimeAngle(path)

    startOfTurn= None
    endOfTurn = None
    nOfTurns = 0

    for event in flightEventStore.eventList:

        if hasattr(event,'course') and event.type == et.Event_Type.StartCourseChange:
            # Check that event has attriute course
            #End of turn or End of climb/descent or Enf of hover can be a start of StraightSegment
            startOfTurn = event
            continue

        if hasattr(event,'course') and event.type == et.Event_Type.EndCourseChange:
            # Check that event has attriute course
            # Start of turn or Start of climb/descent or Start of Hover can be a start of StraightSegment
            endOfTurn = event

            if startOfTurn != None:


                speedArray,lengthArray, angleArray, Angle2Turn, xArray, yArray = getTurnAngleArray(flightEventStore,startOfTurn,endOfTurn)
                # Fly-over
                isDrifting = CheckIfDrift(angleArray)

                vMeanArray = []
                index = startOfTurn.idx
                while index <= endOfTurn.idx:
                    v = np.sqrt(flightEventStore.telemetryList[index].vx * flightEventStore.telemetryList[index].vx + flightEventStore.telemetryList[index].vy * flightEventStore.telemetryList[index].vy + flightEventStore.telemetryList[index].vz * flightEventStore.telemetryList[index].vz)
                    vMeanArray.append(round(v,2))
                    index = index + 1

                vMean = 0 if len(vMeanArray) == 0 else sum(vMeanArray) / len(vMeanArray)
                if isDrifting:
                    color = 'r'
                else:
                    color = 'black'
                #previousEvent = getEvent(flightEventStore, startOfTurn, endOfTurn, requiredEvent="Pre")
                #followingEvent = getEvent(flightEventStore, startOfTurn, endOfTurn, requiredEvent="Post")
                #prevSegment = flightEventStore.telemetryList[startOfTurn.idx].getDistance(flightEventStore.telemetryList[previousEvent.idx])
                #followingSegment = flightEventStore.telemetryList[endOfTurn.idx].getDistance(flightEventStore.telemetryList[followingEvent.idx])
                #tp.addEventPlot(axtp, angleArray, speedArray, isDrifting)
                #tp.getIndividualTurnPlot(speedArray, angleArray, xArray, yArray,Angle2Turn, path + "--" + str(nOfTurns) + "--Turn.png")
                #tp.addArcPlot(axtp, startOfTurn, endOfTurn,vMean,prevSegment, followingSegment, color)
                #Number the turns in dynamic location plot
                #dlp.addTurnSampleText(flightEventStore.location_ax,startOfTurn, endOfTurn, nOfTurns)

                nOfTurns = nOfTurns + 1
                startOfTurn = None
                endOfTurn = None
                continue

def CheckIfDrift(angleArray):
    isDrift = False
    for position in angleArray:
        #Drift if turns 10% or more of Angle2Turn to the other side
        if position < -0.07:
            isDrift = True
            break

    return isDrift


def getTurnAngleArray(flightEventStore, initialEvent, finalEvent):

    Angle2Turn = finalEvent.course - initialEvent.course
    segmentLength = geo.Geodesic.WGS84.Inverse(flightEventStore.telemetryList[initialEvent.idx].latitude, flightEventStore.telemetryList[initialEvent.idx].longitude, finalEvent.latitude, finalEvent.longitude)['s12']
    speedArray = []
    lengthArray = []
    angleArray = []
    xArray = []
    yArray = []
    index = initialEvent.idx
    while index <= finalEvent.idx:
        sample = flightEventStore.telemetryList[index]
        if sample.type == mo.Object_Type.mlGlobalPositionInt:
            speed = sample.hspeed  #m/s
            speedArray.append(speed)
            distance2EndOfSegment =geo.Geodesic.WGS84.Inverse(sample.latitude, sample.longitude, finalEvent.latitude, finalEvent.longitude)['s12']
            lenght = 0.0 if segmentLength == 0 else distance2EndOfSegment / segmentLength
            lengthArray.append(lenght)
            angle2EndTurn = finalEvent.course - sample.course

            #Used to plot X vs. Y
            x, y, z = lla_to_ecef(sample.latitude, sample.longitude, sample.altitude)
            xArray.append(x)
            yArray.append(y)
            #------------------------------------------------------
            if(Angle2Turn == 0):
                Angle2Turn = 1
            angleArray.append(angle2EndTurn / Angle2Turn)
        #if sample.type == mo.Object_Type.mlGPSRawInt:
        #    speed = sample.hspeed  #Ground Speed m/s
        #    speed = sample.hspeed  # m/s
        #    speedArray.append(speed)
        #    distance2EndOfSegment = geo.Geodesic.WGS84.Inverse(sample.latitude, sample.longitude, finalEvent.latitude, finalEvent.longitude)['s12']
        #    lengthArray.append(distance2EndOfSegment / segmentLength)
        #    angle2EndTurn = finalEvent.course - sample.course
#
        #    # Used to plot X vs. Y
        #    x, y, z = lla_to_ecef(sample.latitude, sample.longitude, sample.altitude)
        #    xArray.append(x)
        #    yArray.append(y)
        #    # ------------------------------------------------------
        #    if (Angle2Turn == 0):
        #        Angle2Turn = 1
        #    angleArray.append(angle2EndTurn / Angle2Turn)
        #elif sample.type == mo.Object_Type.mlVfrHud:
        #    speed = sample.airspeed #Airpeed m/s

        index += 1

    # Normalized arrays
    return speedArray,lengthArray, angleArray, Angle2Turn, xArray, yArray

def lla_to_ecef(lat, lon, alt):
    rad = np.float64(6378137.0)        # Radius of the Earth (in meters)
    f = np.float64(1.0/298.257223563)  # Flattening factor WGS84 Model
    cosLat = np.cos(np.radians(lat))
    sinLat = np.sin(np.radians(lat))
    FF     = (1.0-f)**2
    C      = 1/np.sqrt(cosLat**2 + FF * sinLat**2)
    S      = C * FF

    x = (rad * C + alt)*cosLat * np.cos(np.radians(lon))
    y = (rad * C + alt)*cosLat * np.sin(np.radians(lon))
    z = (rad * S + alt)*sinLat
    return x, y, z


def getMode(dataset):
    frequency = {}

    for value in dataset:
        frequency[value] = frequency.get(value, 0) + 1

    most_frequent = max(frequency.values())

    modes = [key for key, value in frequency.items()
             if value == most_frequent]

    return modes

def getEvent(fea, startOfTurn, endOfTurn, requiredEvent):
    index = 0
    while index < len(fea.eventList):
        if fea.eventList[index].tag == endOfTurn.tag and requiredEvent == "Post" and (index + 1) > len(fea.eventList):
            return fea.eventList[index + 1]
        if fea.eventList[index].tag == startOfTurn.tag and requiredEvent == "Pre":
            return fea.eventList[index - 1]
        index = index + 1

    return None

def computeAndPlotTimeAngle(filepath):

    deca = [120.8,119.5,120.6,122.6,121.8,121,122.1,121.4,121.3,121]
    nona = [59.3,60.4,60.1,60.9,60.7,58.1,59.3,61,59.3]
    octa = [56.9,55.5,55.8,55.5,56.9,57.2,55.4,55.3]
    hepta = [103,97.5,97.9,101.7,99.2,99.8,105.8]
    hexa = [89.8,89.2,89.8,89,90.2,89.8]
    penta = [83.8,91.4,82.5,81.1,74.9]
    tetra = [50.4,50.3,50.1,50.4]
    tri = [130.4,127.9,129.2]
    reverse = [167.1, 167.6]

    # y = 9.70229E-02 * x + 1.42715
    SSCurve = ['1', 9.70229E-02, 1.42715]


    a = SSCurve[1]
    b = SSCurve[2]

    yDeca = []
    for SS in deca:
        y = a * SS + b
        yDeca.append(y)

    yNona = []
    for SS in nona:
        y = a * SS + b
        yNona.append(y)

    yOcta = []
    for SS in octa:
        y = a * SS + b
        yOcta.append(y)

    yHepta = []
    for SS in hepta:
        y = a * SS + b
        yHepta.append(y)

    yHexa = []
    for SS in hexa:
        y = a * SS + b
        yHexa.append(y)

    yPenta = []
    for SS in penta:
        y = a * SS + b
        yPenta.append(y)

    yTetra = []
    for SS in tetra:
        y = a * SS + b
        yTetra.append(y)

    yTri = []
    for SS in tri:
        y = a * SS + b
        yTri.append(y)

    yRev = []
    for SS in reverse:
        y = a * SS + b
        yRev.append(y)

    '''y80 = []
    ss80 = [63.7,63.7,63.6,63.4,64,63.2,63.2,64]
    for SS in ss80:
        y = a * SS + b
        y80.append(y)

    y100 = []
    ss100 = [100, 96.5]
    for SS in ss100:
        y = a * SS + b
        y100.append(y)

    y130 = []
    ss130 = [96.5, 100.3]
    for SS in ss130:
        y = a * SS + b
        y130.append(y)

    y140 = []
    ss140 = [100.3, 95.7]
    for SS in ss140:
        y = a * SS + b
        y140.append(y)

    y160 = []
    ss160 = [95.7, 92.0]
    for SS in ss160:
        y = a * SS + b
        y160.append(y)'''

    #Real time spent to do the poligon - Time that would need to do the poligon if it does not lose time at turning
    # v = 10 m/s

    timeTurnDeca = 137 - sum(yDeca)
    timeTurnNona = 67 - sum(yNona)
    timeTurnOcta = 59 - sum(yOcta)
    timeTurnHepta = 82 - sum(yHepta)
    timeTurnHexa = 66 - sum(yHexa)
    timeTurnPenta = 53 - sum(yPenta)
    timeTurnTetra = 31 - sum(yTetra)
    timeTurnTri = 48 - sum(yTri)
    timeTurnRev = 41 - sum(yRev)
    '''

    # v = 8 m/s

    timeTurnNona = 72 - sum(yNona)
    timeTurnOcta = 60 - sum(yOcta)
    timeTurnHexa = 75 - sum(yHexa)
    timeTurnPenta = 86 - sum(yPenta)
    timeTurnTetra = 32 - sum(yTetra)
    timeTurnTri = 56 - sum(yTri)
    '''

    #As the turns have the same angle the time spent to do a turn is Tr-Tt/NOfTurns
    timeAngleDeca = timeTurnDeca / 10
    timeAngleNona = timeTurnNona / 9
    timeAngleOcta = timeTurnOcta / 8
    timeAngleHepta = timeTurnHepta / 7
    timeAngleHexa = timeTurnHexa / 6
    timeAnglePenta = timeTurnPenta / 5
    timeAngleTetra = timeTurnTetra / 4
    timeAngleTri = timeTurnTri / 3
    timeAngleRev = timeTurnRev / 2
    '''timeAngle80 = (59 - sum(y80)) / 8
    timeAngle100 = 26 - sum(y100)
    timeAngle130 = 25 - sum(y130)
    timeAngle140 = 26 - sum(y140)
    timeAngle160 = 27 - sum(y160)'''

    #Time to turn 1 degree
    '''timeDegreeDeca = timeAngleDeca / (360/len(yDeca))
    timeDegreeNona = timeAngleNona / (360/len(yNona))
    timeDegreeOcta = timeAngleOcta / (360/len(yOcta))
    timeDegreeHepta = timeAngleHepta/ (360/len(yHepta))
    timeDegreeHexa = timeAngleHexa / (360/len(yHexa))
    timeDegreePenta =timeAnglePenta/ (360/len(yPenta))
    timeDegreeTetra =timeAngleTetra/ (360/len(yTetra))
    timeDegreeTri = timeAngleTri / (360 / len(yTri))
    timeDegreeRev = timeAngleRev / (360 / len(yRev))'''

    figI = plt.figure()
    # ggplot style
    plt.style.use('ggplot')
    axI = figI.add_subplot(111)
    axI.set_xlabel('Aº (degrees)',fontsize=18)
    axI.set_ylabel('Time (s)',fontsize=18)

    axI.scatter(360/len(yDeca), timeAngleDeca, marker='o', color='black')
    axI.scatter(360/len(yNona), timeAngleNona, marker='o', color='black')
    axI.scatter(360/len(yOcta), timeAngleOcta, marker='o', color='black')
    axI.scatter(360/len(yHepta), timeAngleHepta, marker='o', color='black')
    axI.scatter(360/len(yHexa), timeAngleHexa, marker='o', color='black')
    axI.scatter(360/len(yPenta), timeAnglePenta, marker='o', color='black')
    axI.scatter(360/len(yTetra), timeAngleTetra, marker='o', color='black')
    axI.scatter(360/len(yTri), timeAngleTri, marker='o', color='black')
    axI.scatter(360 / len(yRev), timeAngleRev, marker='o', color='black')

    TurnVariance = [ 360 / len(yNona), 360 / len(yOcta),  360 / len(yHexa),
                    360 / len(yPenta), 360 / len(yTetra), 360 / len(yTri), 360 / len(yRev)]
    DeltaTime = [timeAngleNona, timeAngleOcta, timeAngleHexa, timeAnglePenta,
                 timeAngleTetra, timeAngleTri, timeAngleRev]

    x_line = np.arange(min(TurnVariance), max(TurnVariance), 0.25)
    curveOrder = turnCurve[0]
    if curveOrder == '1':
        a = turnCurve[1]
        b = turnCurve[2] + turnOffset
        y_line = a * x_line + b
        equation = "Curve Fitting: v = 10,0 m/s ; y = " + str(a) + " · x + " + str(b)
        axI.plot(x_line, y_line, marker='_', color='orange', label=equation)
    elif curveOrder == '2':
        # y = 2.72958E-02 * x + -4.41052E-05 * xx  + -6.36423E-01
        a = turnCurve[1]
        b = turnCurve[2]
        c = turnCurve[3]
        y_line = a * x_line + b * x_line * x_line + c
        equation = "Curve Fitting:  ; y = " + str(format(a, '.5E')) + " * x + " + str(format(b, '.5E')) + " * x^2 + " + str(format(c, '.5E'))
        axI.plot(x_line, y_line, marker='_', color='orange', label=equation)
    elif curveOrder == '3':
        a = turnCurve[1]
        b = turnCurve[2]
        c = turnCurve[3]
        d = turnCurve[4]
        y_line = a * x_line + b * x_line * x_line + c * x_line * x_line * x_line + d
        equation = "Curve Fitting:  ; y = " + str(format(a, '.5E')) + " * x + " + str(format(b, '.5E')) + " * x^2 + " + str(format(c, '.5E')) +" * x^3 + " + str(format(d, '.5E'))
        axI.plot(x_line, y_line, marker='_', color='orange', label=equation)

    '''axI.scatter(80, timeAngle80, marker='o', color='r')
    axI.scatter(100, timeAngle100, marker='o', color='r')
    axI.scatter(130, timeAngle130, marker='o', color='r')
    axI.scatter(140, timeAngle140, marker='o', color='r')
    axI.scatter(160, timeAngle160, marker='o', color='r')'''

    axI.legend(fontsize=18)
    plt.title("Turn variation[Aº] vs Time variation[At]", fontsize=20)
    plt.show()
    figI.set_size_inches(14.0, 14.0)
    figI.savefig(filepath + "TurnVariation_TimeVariation.png")
    plt.close(figI)

    TurnVariance = [360 / len(yDeca), 360 / len(yNona), 360 / len(yOcta), 360 / len(yHepta), 360 / len(yHexa),
                    360 / len(yPenta), 360 / len(yTetra), 360 / len(yTri), 360 / len(yRev)]
    DeltaTime = [timeAngleDeca, timeAngleNona, timeAngleOcta, timeAngleHepta, timeAngleHexa, timeAnglePenta,
                 timeAngleTetra, timeAngleTri, timeAngleRev]

    figI = plt.figure()
    # seaborn-darkgrid style
    plt.style.use('ggplot')
    axI = figI.add_subplot(111)
    axI.set_xlabel('Aº (m)',fontsize=18)
    axI.set_ylabel('ATime (s)',fontsize=18)
    axI.scatter(TurnVariance, DeltaTime, marker='o', color='black')


    x_line = np.arange(min(TurnVariance), max(TurnVariance), 0.25)
    warnings.filterwarnings('ignore', message='Covariance of the parameters could not be estimated')
    #Curve order one: y = a * x + b
    popt, _ = curve_fit(curveObjectiveOrder1, TurnVariance, DeltaTime)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder1(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(format(a,'.5E')) + " * x + " + str(format(b,'.5E'))
    axI.plot(x_line, curve, marker='_', color='red', label=equation)

    #Curve order two: y = (a * x) + (b * x**2) + c
    popt, _ = curve_fit(curveObjectiveOrder2, TurnVariance, DeltaTime)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder2(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(format(a,'.5E')) + " * x + " + str(format(b,'.5E')) + " * x^2 + " + str(format(c,'.5E'))
    axI.plot(x_line, curve, marker='_', color='blue', label=equation)

    #Curve order three: y = (a * x) + (b * x**2) + (c * x**3) + d
    popt, _ = curve_fit(curveObjectiveOrder3, TurnVariance, DeltaTime)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder3(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(format(a,'.5E')) + " * x + " + str(format(b,'.5E')) + " * x^2 + " + str(format(c,'.5E')) + " * x^3 + " + str(format(d,'.5E'))
    axI.plot(x_line, curve, marker='_', color='purple', label=equation)

    axI.legend(fontsize=18)
    plt.title("Turn Curve Fitting\nTurn variation[Aº] vs Time variation[At]",fontsize=20)
    plt.show()
    figI.set_size_inches(14.0, 14.0)
    figI.savefig(filepath + "TurnVariation_TimeVariation_curveFitting.png")
    plt.close(figI)

# curves objectives to fit
def curveObjectiveOrder1(x, a, b, c, d, e): return a * x + b  # straight line


def curveObjectiveOrder2(x, a, b, c, d, e): return (a * x) + (b * x ** 2) + c  # polynomial regression order 2


def curveObjectiveOrder3(x, a, b, c, d, e): return (a * x) + (b * x ** 2) + (
            c * x ** 3) + d  # polynomial regression order 3


def curveObjectiveOrder4(x, a, b, c, d, e): return (a * x) + (b * x ** 2) + (c * x ** 3) + (
            c * x ** 4) + e  # polynomial regression order 4


def curveObjectiveOrder4(x, a, b, c, d, e, f): return (a * x) + (b * x ** 2) + (c * x ** 3) + (d * x ** 4) + (
            e * x ** 5) + f  # polynomial regression order 5


def curveObjectiveGaussian(x, a, b, c, d, e): return a * np.exp(-np.power(x - b, 2) / (2 * np.power(c, 2)))  # Gaussian


def curveObjectiveSine(x, a, b, c, d, e): return a * np.sin(b - x) + c * x ** 2 + d  # Sine curve


def curveObjective3D(X, a, b, c, d):
    x, y = X
    return a * x + b * y + c

def getTurnCurve():
    return turnCurve, turnOffset