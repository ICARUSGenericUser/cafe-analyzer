import math as math
from Mocca_Analyzer.Performance_Analyzer import Event_Type as et
from Mocca_Analyzer.Mocca import Mocca_Object as mo
from Mocca_Analyzer.Plots import dynamicVerticalAccelPlot as dvap
from Mocca_Analyzer.Plots import dynamicVerticalSpeedPlot as dvsp


def analyzeAltitudeEvents(flightEventStore, path, altitudeParams, curveFittingInfoCsv, trajectory_file,csvOverAllData,  interpolationVariable,interpolationVariable2,HorizontalSpeedClimb, HorizontalSpeedDescent, VerticalClimbEventsAltitudeVariance,
                          VerticalClimbEventsDeltaTime, VerticalDescentEventsAltitudeVariance, VerticalDescentEventsDeltaTime, NonVerticalClimbEventsAltitudeVariance,
                          NonVerticalClimbEventsDeltaTime, NonVerticalDescentEventsAltitudeVariance, NonVerticalDescentEventsDeltaTime, NonVerticalClimbEventsHorizontalVariance, NonVerticalDescentEventsHorizontalVariance, takeOffAltitudeVariance, takeoffDeltaTime, landingAltitudeVariance, landingDeltaTime):
    initialClimb = None
    initialDescent = None
    levelOff = None
    nDiagram = 0
    EventType = ""
    initialTimeStamp = 0
    horizontalDistance2ConsiderVerticalClimbDescent = 5.0

    for event in flightEventStore.eventList:
        xTimeValues = []
        yVSpeedValues = []

        if event.type == et.Event_Type.StartClimb:
            initialClimb = event
            EventType = "Climb"
            continue

        if event.type == et.Event_Type.StartTakeOff:
            initialClimb = event
            EventType = "Take Off"
            continue

        if event.type == et.Event_Type.StartDescent:
            initialDescent = event
            EventType = "Descent"
            continue

        if event.type == et.Event_Type.StartLanding:
            initialDescent = event
            EventType = "Landing"
            continue

        if event.type == et.Event_Type.EndAltChange:
            levelOff = event

            if initialClimb != None:
                # print("Climb from: ", initialClimb.altitude, " up to: ", levelOff.altitude)
                #print(EventType + " from: ", flightEventStore.telemetryList[initialClimb.idx].altitude, " up to: ",
                #      flightEventStore.telemetryList[levelOff.idx].altitude, " Total: ",
                #      levelOff.altitude - initialClimb.altitude, " m")
                #print(EventType + " duration: ", (levelOff.timestamp - initialClimb.timestamp) / 1000, " sec")
                #print("Average climb rate: ", 1000 * (levelOff.altitude - initialClimb.altitude) / (
                #            levelOff.timestamp - initialClimb.timestamp), " m/s")

                # Check if vertical climb
                isVerticalClimb, horizontalDistance = CheckIfVerticalClimbDescent(flightEventStore, initialClimb, levelOff, horizontalDistance2ConsiderVerticalClimbDescent)
                if isVerticalClimb and initialClimb.type != et.Event_Type.StartTakeOff:
                    axvsT, axvsA, figvs = dvsp.getDynamicVerticalSpeedPlot(
                        "[Vertical Climb]")  # Vertical Speed vs. Altitude vs. Time Plot
                    axva, figva = dvap.getDynamicVerticalAccelPlot("[Vertical Climb]")
                    #TODO: delete interpolationVariable
                    interpolationVariable.append(initialClimb.tag)
                    VerticalClimbEventsAltitudeVariance.append(levelOff.altitude - initialClimb.altitude)
                    VerticalClimbEventsDeltaTime.append((levelOff.timestamp - initialClimb.timestamp) / 1000)  # Time needed to climb: levelOff.altitude - initialClimb.altitude [sec]

                elif isVerticalClimb is False and initialClimb.type != et.Event_Type.StartTakeOff:
                    axvsT, axvsA, figvs = dvsp.getDynamicVerticalSpeedPlot("[Non-Vertical Climb]")  # Vertical Speed vs. Altitude vs. Time Plot
                    axva, figva = dvap.getDynamicVerticalAccelPlot("[Non-Vertical Climb]")
                    NonVerticalClimbEventsAltitudeVariance.append(levelOff.altitude - initialClimb.altitude)
                    NonVerticalClimbEventsDeltaTime.append((levelOff.timestamp - initialClimb.timestamp) / 1000)
                    NonVerticalClimbEventsHorizontalVariance.append(horizontalDistance)
                    HorizontalSpeedClimbVariation = abs(round(flightEventStore.telemetryList[levelOff.idx].hspeed,3) - round(flightEventStore.telemetryList[initialClimb.idx].hspeed,3))
                    HorizontalSpeedClimb.append(HorizontalSpeedClimbVariation)

                elif initialClimb.type == et.Event_Type.StartTakeOff:
                    takeOffAltitudeVariance.append(levelOff.altitude - initialClimb.altitude)
                    takeoffDeltaTime.append((levelOff.timestamp - initialClimb.timestamp) / 1000)  # Time needed to climb: levelOff.altitude - initialClimb.altitude [sec]
                    #TODO check continue for vAnalysis & *.csv
                    nDiagram += 1
                    initialClimb = None
                    levelOff = None

                    continue

                '''i = initialClimb.idx
                averageSpeed = []
                averageAccel = []
                while i < levelOff.idx:
                    sample = flightEventStore.telemetryList[i]

                    # if sample.type == mo.Object_Type.mlVfrHud:
                    #    vspeed = sample.vSpeed
                    #    averageSpeed.append(vspeed)
                    #    dvsp.addSamplePlot(axvs, sample.timestamp, vspeed)
                    if sample.type == mo.Object_Type.mlGlobalPositionInt:
                        if (initialTimeStamp == 0):
                            initialTimeStamp = sample.timestamp
                        vspeed = sample.vz
                        averageSpeed.append(vspeed)
                        dvsp.addSamplePlot(axvsT, sample.timestamp - initialTimeStamp, axvsA, sample.altitude,
                                           vspeed)  # Add Time & Altitude sample
                        xTimeValues.append(sample.timestamp - initialTimeStamp)
                        yVSpeedValues.append(vspeed)
                    elif sample.type == mo.Object_Type.mlHighresIMU:
                        vaccel = sample.z_lin_acc
                        averageAccel.append(vaccel)
                        dvap.addSamplePlot(axva, sample.timestamp, vaccel)
                    elif sample.type == mo.Object_Type.mlScaledIMU:
                        vaccel = sample.z_lin_acc
                        averageAccel.append(vaccel)
                        dvap.addSamplePlot(axva, sample.timestamp, vaccel)

                    i += 1
                

               # Calculate Average Vertical Speed not taking into account the "shift" first and last samples
                aveSpeed = CalculateAverageValue(averageSpeed, shift=3)
                dvsp.addAverageSpeedPlot(axvsT, aveSpeed)

                # Calculate Average Vertical Acceleration not taking into account the "shift" first and last samples
                aveAccel = CalculateAverageValue(averageAccel, shift=3)
                dvap.addAverageAccelPlot(axva, aveAccel)

                dvsp.closeDynamicPlot(figvs, path + "--" + str(nDiagram) + "-VerticalSpeed.png")
                dvap.closeDynamicPlot(figva, path + "--" + str(nDiagram) + "-VerticalAcceleration.png")
                #dvsp.interpolateValues(xTimeValues, yVSpeedValues, path + "--" + str(nDiagram))
                '''

                # Prepare data to csv
                Prepare_ALtitudeEvent_csvfile(trajectory_file, EventType, flightEventStore, initialClimb, levelOff, altitudeParams)

                nDiagram += 1
                initialClimb = None
                levelOff = None

                continue

            if initialDescent != None:
            #    # print("Descent from: ", initialDescent.altitude, " down to: ", levelOff.altitude)
            #    print(EventType + " from: ", flightEventStore.telemetryList[initialDescent.idx].altitude, " down to: ",
            #          flightEventStore.telemetryList[levelOff.idx].altitude, " Total: ",
            #          initialDescent.altitude - levelOff.altitude, " m")
            #    print(EventType + " duration: ", (levelOff.timestamp - initialDescent.timestamp) / 1000, " sec")
            #    print("Average descent rate: ", 1000 * (initialDescent.altitude - levelOff.altitude) / (
            #                levelOff.timestamp - initialDescent.timestamp), " m/s")


                # Check if vertical descent
                isVerticalDescent, horizontalDistance = CheckIfVerticalClimbDescent(flightEventStore, initialDescent, levelOff, horizontalDistance2ConsiderVerticalClimbDescent)
                if isVerticalDescent and initialDescent.type != et.Event_Type.StartLanding:
                    axvsT, axvsA, figvs = dvsp.getDynamicVerticalSpeedPlot(
                        "[Vertical Descent]")  # Vertical Speed vs. Altitude vs. Time Plot
                    axva, figva = dvap.getDynamicVerticalAccelPlot("[Vertical Descent]")
                    VerticalDescentEventsAltitudeVariance.append(initialDescent.altitude - levelOff.altitude)
                    VerticalDescentEventsDeltaTime.append((levelOff.timestamp - initialDescent.timestamp) / 1000)
                    #TODO: delete  TAG Variable
                    interpolationVariable2.append(initialDescent.tag)

                elif isVerticalDescent is False and initialDescent.type != et.Event_Type.StartLanding:
                    axvsT, axvsA, figvs = dvsp.getDynamicVerticalSpeedPlot(
                        "[Non-Vertical Descent]")  # Vertical Speed vs. Altitude vs. Time Plot
                    axva, figva = dvap.getDynamicVerticalAccelPlot("[Non-Vertical Descent]")
                    NonVerticalDescentEventsAltitudeVariance.append(initialDescent.altitude - levelOff.altitude)
                    NonVerticalDescentEventsDeltaTime.append((levelOff.timestamp - initialDescent.timestamp) / 1000)
                    NonVerticalDescentEventsHorizontalVariance.append(horizontalDistance)
                    HorizontalSpeedDescent.append(round(flightEventStore.telemetryList[initialDescent.idx].hspeed,3))

                elif initialDescent.type == et.Event_Type.StartLanding:
                    landingAltitudeVariance.append(initialDescent.altitude - levelOff.altitude)
                    landingDeltaTime.append((levelOff.timestamp - initialDescent.timestamp) / 1000)
                    # TODO check continue for vAnalysis & *.csv
                    nDiagram += 1
                    initialClimb = None
                    levelOff = None

                    continue

                '''i = initialDescent.idx
                averageSpeed = []
                averageAccel = []
                while i < levelOff.idx:
                    sample = flightEventStore.telemetryList[i]

                    # if sample.type == mo.Object_Type.mlVfrHud:
                    #    vspeed = sample.vSpeed
                    #    averageSpeed.append(vspeed)
                    #    dvsp.addSamplePlot(axvs, sample.timestamp, vspeed)
                    if sample.type == mo.Object_Type.mlGlobalPositionInt:
                        vspeed = sample.vz
                        averageSpeed.append(vspeed)
                        dvsp.addSamplePlot(axvsT, sample.timestamp, axvsA, sample.altitude,
                                           vspeed)  # Add Time & Altitude sample
                    elif sample.type == mo.Object_Type.mlHighresIMU:
                        vaccel = sample.z_lin_acc
                        averageAccel.append(vaccel)
                        dvap.addSamplePlot(axva, sample.timestamp, vaccel)
                    elif sample.type == mo.Object_Type.mlScaledIMU:
                        vaccel = sample.z_lin_acc
                        averageAccel.append(vaccel)
                        dvap.addSamplePlot(axva, sample.timestamp, vaccel)

                    i += 1

                # Calculate Average Vertical Speed not taking into account the "shift" first and last samples
                aveSpeed = CalculateAverageValue(averageSpeed, shift=3)
                dvsp.addAverageSpeedPlot(axvsT, aveSpeed)

                # Calculate Average Vertical Acceleration not taking into account the "shift" first and last samples
                aveAccel = CalculateAverageValue(averageAccel, shift=3)
                dvap.addAverageAccelPlot(axva, aveAccel)

                dvsp.closeDynamicPlot(figvs, path + "--" + str(nDiagram) + "-VerticalSpeedDescent.png")
                dvap.closeDynamicPlot(figva, path + "--" + str(nDiagram) + "-VerticalAccelerationDescent.png")'''

                # Prepare data to csv
   #             Prepare_ALtitudeEvent_csvfile(trajectory_file, EventType, flightEventStore, initialDescent, levelOff, altitudeParams)

                nDiagram += 1
                initialDescent = None
                levelOff = None

                continue

    curveFittingInfoCsv, csvOverAllData = Prepare_CurveFittingInfo_csvfile(flightEventStore, curveFittingInfoCsv, csvOverAllData)

    return altitudeParams, curveFittingInfoCsv, csvOverAllData


def analyzeVSpeedEvents(flightEventStore):
    initialVAcceleration = None
    initialVDeceleration = None
    levelOff = None

    for event in flightEventStore.eventList:

        if event.type == et.Event_Type.StartVAcceleration:
            initialVAcceleration = event

        if event.type == et.Event_Type.StartVDeceleration:
            initialVDeceleration = event

        if event.type == et.Event_Type.EndVSpeedChange:
            levelOff = event

            if initialVAcceleration != None:
                # print("Climb from: ", initialClimb.altitude, " up to: ", levelOff.altitude)
                #print("Accelerate from: ", flightEventStore.telemetryList[initialVAcceleration.idx].altitude,
                #      " up to: ", flightEventStore.telemetryList[levelOff.idx].altitude, " Total: ",
                #      levelOff.altitude - initialVAcceleration.altitude, " m")
                #print("Acceleration duration: ", (levelOff.timestamp - initialVAcceleration.timestamp) / 1000, " sec")
                #print("Average Acceleration rate: ", 1000 * (levelOff.vSpeed - initialVAcceleration.vSpeed) / (
                #            levelOff.timestamp - initialVAcceleration.timestamp), " m/s2")
                initialVAcceleration = None
                levelOff = None

            if initialVDeceleration != None:
                # print("Descent from: ", initialDescent.altitude, " down to: ", levelOff.altitude)
                #print("Descent from: ", flightEventStore.telemetryList[initialVDeceleration.idx].altitude, " down to: ",
                #      flightEventStore.telemetryList[levelOff.idx].altitude, " Total: ",
                #      abs(initialVDeceleration.altitude - levelOff.altitude), " m")
                #print("Decceleration duration: ", (levelOff.timestamp - initialVDeceleration.timestamp) / 1000, " sec")
                #print("Average Decceleration rate: ", 1000 * abs(levelOff.vSpeed - initialVDeceleration.vSpeed) / (
                #            levelOff.timestamp - initialVDeceleration.timestamp), " m/s2")
                initialVDeceleration = None
                levelOff = None


def CheckIfVerticalClimbDescent(flightEventStore, initialClimbDescent, levelOff, horizontalDistance2ConsiderVerticalClimbDescent):
    # Check if vertical climb/descent
    horizontalDistance = flightEventStore.telemetryList[initialClimbDescent.idx].getDistance(flightEventStore.telemetryList[levelOff.idx])
    if horizontalDistance < horizontalDistance2ConsiderVerticalClimbDescent:
        VerticalClimbDescent = True
    else:
        VerticalClimbDescent = False
    return VerticalClimbDescent,horizontalDistance


def Prepare_ALtitudeEvent_csvfile(trajectory_file, EventType, flightEventStore, initialClimbDescent, levelOff, altitudeParams):
    # Prepare data to csv
    eventParams = []
    eventParams.append(trajectory_file)
    eventParams.append(EventType)
    eventParams.append(str(flightEventStore.telemetryList[initialClimbDescent.idx].latitude))
    eventParams.append(str(flightEventStore.telemetryList[initialClimbDescent.idx].longitude))
    eventParams.append(str(flightEventStore.telemetryList[initialClimbDescent.idx].altitude))
    eventParams.append(str(flightEventStore.telemetryList[levelOff.idx].latitude))
    eventParams.append(str(flightEventStore.telemetryList[levelOff.idx].longitude))
    eventParams.append(str(flightEventStore.telemetryList[levelOff.idx].altitude))
    eventParams.append(str((levelOff.timestamp - initialClimbDescent.timestamp) / 1000))
    eventParams.append(str(1000 * (levelOff.altitude - initialClimbDescent.altitude) / (
                levelOff.timestamp - initialClimbDescent.timestamp)))
    altitudeParams.append(eventParams)


def CalculateAverageValue(listofValues, shift):  # Calculates the average value of a List shiftted
    index = shift
    aveVariable = 0
    # Check if there are enough samples
    if(len(listofValues) == 0):
        return 0
    elif (len(listofValues) <= 2 * shift):
        aveVariable = sum(listofValues) / len(listofValues)
    else:
        while index < len(listofValues) - shift:
            aveVariable += listofValues[index]
            index += 1
        aveVariable = aveVariable / (index - shift)
    return aveVariable

def Prepare_CurveFittingInfo_csvfile(flightEventStore, curveFittingInfoCsv, csvOverAllData):

    overAllSumTO, overAllSumC, overAllSumD, overAllSumL = csvOverAllData[0], csvOverAllData[1], csvOverAllData[2], csvOverAllData[3]
    overAlllenTO, overAlllenC, overAlllenD, overAlllenL = csvOverAllData[4], csvOverAllData[5], csvOverAllData[6], csvOverAllData[7]
    # Prepare data to csv
    eventIndex = 0
    overAllVelData = []

    flightSumTO, flightSumC, flightSumD, flightSumL = 0, 0, 0, 0
    flightlenTO, flightlenC, flightlenD, flightlenL = 0, 0, 0, 0,

    while eventIndex < len(flightEventStore.eventList):
        event = flightEventStore.eventList[eventIndex]
        eventParams = []
        if event.type == et.Event_Type.StartTakeOff or event.type == et.Event_Type.StartClimb or event.type == et.Event_Type.StartDescent or event.type == et.Event_Type.StartLanding:
            indexList = event.idx
            sumTO, sumC, sumD, sumL = 0, 0, 0, 0
            lenTO, lenC, lenD, lenL = 0, 0, 0, 0
            while indexList <= flightEventStore.eventList[eventIndex + 1].idx:
                eventParams.append(round(flightEventStore.telemetryList[indexList].vz, 5))
                overAllVelData.append(round(flightEventStore.telemetryList[indexList].vz, 5))
                if event.type == et.Event_Type.StartTakeOff:
                    sumTO = sumTO + abs(round(flightEventStore.telemetryList[indexList].vz, 5))
                    lenTO = lenTO + 1
                    flightlenTO = flightlenTO + 1
                    overAlllenTO = overAlllenTO + 1
                elif event.type == et.Event_Type.StartClimb:
                    sumC = sumC + abs(round(flightEventStore.telemetryList[indexList].vz, 5))
                    lenC = lenC + 1
                    flightlenC = flightlenC + 1
                    overAlllenC = overAlllenC + 1
                elif event.type == et.Event_Type.StartDescent:
                    sumD = sumD + abs(round(flightEventStore.telemetryList[indexList].vz, 5))
                    lenD = lenD + 1
                    flightlenD = flightlenD + 1
                    overAlllenD = overAlllenD + 1
                elif event.type == et.Event_Type.StartLanding:
                    sumL = sumL + abs(round(flightEventStore.telemetryList[indexList].vz, 5))
                    lenL = lenL + 1
                    flightlenL = flightlenL + 1
                    overAlllenL = overAlllenL + 1

                indexList = indexList + 1

            flightSumTO = flightSumTO + sumTO
            flightSumC = flightSumC + sumC
            flightSumD = flightSumD + sumD
            flightSumL = flightSumL + sumL

            overAllSumTO = overAllSumTO + sumTO
            overAllSumC = overAllSumC + sumC
            overAllSumD = overAllSumD + sumD
            overAllSumL = overAllSumL + sumL

            # Event calculations
            # AVOID DIVISION BY 0
            meanTO = 0 if lenTO == 0 else sumTO / lenTO
            meanC = 0 if lenC == 0 else sumC / lenC
            meanD = 0 if lenD == 0 else sumD / lenD
            meanL = 0 if lenL == 0 else sumL / lenL

            #mode = getMode(eventParams)
            eventParams.insert(0, "VEL")
            #eventParams.insert(0, mode[0])
            #eventParams.insert(0, "Mode:")
            eventParams.insert(0, round(meanL, 5))
            eventParams.insert(0, "MeanL:")
            eventParams.insert(0, round(meanD, 5))
            eventParams.insert(0, "MeanD:")
            eventParams.insert(0, round(meanC, 5))
            eventParams.insert(0, "MeanC:")
            eventParams.insert(0, round(meanTO, 5))
            eventParams.insert(0, "MeanTO:")
            eventParams.insert(0, event.type)
            eventParams.insert(0, "Event Type:")

        eventIndex = eventIndex + 1
        curveFittingInfoCsv.append(eventParams)

    # Flight calculations
    flightMeanTO = 0 if flightlenTO == 0 else flightSumTO / flightlenTO
    flightMeanC = 0 if flightlenC == 0 else flightSumC / flightlenC
    flightMeanD = 0 if flightlenD == 0 else flightSumTO / flightlenD
    flightMeanL = 0 if flightlenL == 0 else flightSumTO / flightlenL

    curveFittingInfoCsv.insert(0, "flightMeanL," + str(flightMeanL) + " , " + str(flightlenL))
    curveFittingInfoCsv.insert(0, "flightMeanD," + str(flightMeanD) + " , " + str(flightlenD))
    curveFittingInfoCsv.insert(0, "flightMeanC," + str(flightMeanC) + " , " + str(flightlenC))
    curveFittingInfoCsv.insert(0, "flightMeanTO," + str(flightMeanTO) + " , " + str(flightlenTO))

    #overallMode = getMode(overAllVelData)

    #curveFittingInfoCsv.insert(0, overallMode)
    #curveFittingInfoCsv.insert(0, "overallMode")

    csvOverAllData = []
    csvOverAllData = [overAllSumTO, overAllSumC, overAllSumD, overAllSumL, overAlllenTO, overAlllenC, overAlllenD,
                      overAlllenL]
    return curveFittingInfoCsv , csvOverAllData


def getMode(dataset):
    frequency = {}

    for value in dataset:
        frequency[value] = frequency.get(value, 0) + 1

    most_frequent = max(frequency.values())

    modes = [key for key, value in frequency.items()
             if value == most_frequent]

    return modes