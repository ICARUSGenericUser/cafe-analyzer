import matplotlib.pyplot as plt
from geographiclib import geodesic as geo
import matplotlib
matplotlib.rc('xtick', labelsize=20)
matplotlib.rc('ytick', labelsize=20)

class dynamicTimePlot:

    def __init__(self, xLabel, yLabel, title):
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111)
        self.ax.set_xlabel(xLabel, fontsize=18)
        self.ax.set_ylabel(yLabel, fontsize=18)
        plt.title(title, fontsize=20)
        plt.show()


    def addSamplePlot(self, time, course):
        self.ax.plot(time, course, 'gray', marker='.')


    def addEventPlot(self, time, course, event):
        self.ax.scatter(time, course, color='b')
        self.ax.text(time, course, event, size=10, zorder=1, color='k')


    def closeDynamicPlot(self, filepath):
        print("Generating figure: " + filepath)
        self.fig.set_size_inches(14.0, 14.0)
        self.fig.savefig(filepath)
        plt.close(self.fig)

