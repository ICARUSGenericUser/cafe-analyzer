import matplotlib.pyplot as plt, matplotlib.patches as mpatches
import mpl_toolkits.mplot3d.axes3d as p3
import math as math
import numpy as np
from DEG_Encounter import SegmentEnum as se
from Mocca_Analyzer.Mocca import Mocca_Object as mo
import matplotlib
matplotlib.rc('xtick', labelsize=20)
matplotlib.rc('ytick', labelsize=20)

class dynamicLocationPlot:
    def __init__(self, yLabel, xLabel, title):
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111)
        self.ax.set_xlabel(xLabel, fontsize=18)
        self.ax.set_ylabel(yLabel, fontsize=18)
        plt.title(title, fontsize=20)
        plt.show()


    def addSamplePlot(self, lat, lon, color='k', marker = '.',linestyle='-',linewidth=2):
        self.ax.plot(lon, lat, color=color, marker=marker, linestyle=linestyle,linewidth=linewidth)


    def addEventPlot(self, lat, lon, color='k', marker='.', linestyle='-',linewidth=2):
        self.ax.plot(lat, lon, marker=marker, color = color, linestyle=linestyle,linewidth=linewidth)


    def closeDynamicPlot(self, filepath):
        print("Generating figure: " + filepath)
        mlGPI_patch = mpatches.Patch(color='purple', label='mlGlobalPositionInt')
        mlGPSRI_patch = mpatches.Patch(color='blue', label='mlGPSRawInt')
        #mlVfrHud_patch = mpatches.Patch(color='grey', label='mlVfrHud')
        handles = [mlGPI_patch, mlGPSRI_patch]
        self.ax.legend(handles=handles, loc='upper left', fontsize=18)
        self.fig.set_size_inches(14.0, 14.0)
        self.fig.savefig(filepath)
        plt.close(self.fig)


    def addStraightSegmentSampleText(self, startOfSS, endOfSS, nOfSS, SegmentSpeed,fea):
        text ="·" + str(nOfSS) + "; VSeg: " + str(round(SegmentSpeed,3))
        self.ax.text(startOfSS.latitude, startOfSS.longitude, text, size=10, zorder=1, color='b')
        self.ax.text(endOfSS.latitude, endOfSS.longitude, str(nOfSS), size=10, zorder=1, color='r')
        index = startOfSS.idx + 1
        while index < endOfSS.idx:
            sample = fea.telemetryList[index]
            if sample.type == mo.Object_Type.mlGlobalPositionInt:
                self.ax.plot(sample.longitude, sample.latitude, 'red', marker='.')
            index = index + 1