import matplotlib.pyplot as plt
from geographiclib import geodesic as geo
from Mocca_Analyzer.Plots import dynamicTimePlot as dtp
import matplotlib
matplotlib.rc('xtick', labelsize=20)
matplotlib.rc('ytick', labelsize=20)

class dynamicDistancePlot(dtp.dynamicTimePlot):
    def __init__(self, xLabel, yLabel, title):
        super().__init__(xLabel, yLabel, title)
        self.lastSample = None


    def addSamplePlot(self, time, sample):

        if self.lastSample != None:
            distance = geo.Geodesic.WGS84.Inverse(sample.latitude, sample.longitude, self.lastSample.latitude, self.lastSample.longitude)
            self.ax.plot(time, distance['s12'], 'gray', marker='.')
        self.lastSample = sample


    def addEventPlot(self, time, sample, event):
        distance = geo.Geodesic.WGS84.Inverse(sample.latitude, sample.longitude, self.lastSample.latitude, self.lastSample.longitude)
        self.ax.scatter(time, distance['s12'], color='b')
        self.ax.text(time, distance['s12'], event, size=10, zorder=1, color='k')

