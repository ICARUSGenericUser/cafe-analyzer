import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import math as math
import numpy as np
from DEG_Encounter import SegmentEnum as se
import matplotlib
matplotlib.rc('xtick', labelsize=20)
matplotlib.rc('ytick', labelsize=20)

def getDynamicCoursePlot():
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('T (ms)', fontsize=18)
    ax.set_ylabel('Course (deg)', fontsize=18)
    plt.title("Dynamic altitude plot", fontsize=20)
    plt.show()
    return ax, fig


def addSamplePlot(ax, time, course):
    ax.plot(time, course, 'gray', marker='.')


def addEventPlot(ax, time, course, event):
    ax.scatter(time, course, color='b')
    ax.text(time, course, event, size=10, zorder=1, color='k')


def closeDynamicPlot(fig, filepath):
    print("Generating figure: " + filepath)
    fig.set_size_inches(14.0, 14.0)
    fig.savefig(filepath)
    plt.close(fig)

