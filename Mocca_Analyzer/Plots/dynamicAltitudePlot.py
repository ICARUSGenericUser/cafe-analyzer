import matplotlib
import matplotlib.pyplot as plt, matplotlib.patches as mpatches
import mpl_toolkits.mplot3d.axes3d as p3
import math as math
import numpy as np
from DEG_Encounter import SegmentEnum as se
import matplotlib
matplotlib.rc('xtick', labelsize=20)
matplotlib.rc('ytick', labelsize=20)

class dynamicAltitudePlot:
    def __init__(self, xLabel, yLabel, title):
        self.fig = plt.figure()
        plt.style.use('ggplot')
        self.ax = self.fig.add_subplot(111)
        self.ax.set_xlabel(xLabel, fontsize=18)
        self.ax.set_ylabel(yLabel, fontsize=18)
        plt.title(title, fontsize=20)
        plt.show()


    def addSamplePlot(self, time, altitude, color='k', marker='.', linestyle='-',linewidth=2):
        self.ax.plot(time, altitude, marker=marker, color = color, linestyle=linestyle,linewidth=linewidth)


    def addEventPlot(self, time, value, event, color = 'k'):
        self.ax.scatter(time, value, color='b')
        self.ax.text(time, value, event, size=18, zorder=1, color=color, rotation=60)


    def closeDynamicPlot(self, filepath):
        print("Generating figure: " + filepath)
        mlGPI_patch = mpatches.Patch(color='purple', label='mlGlobalPositionInt')
        mlGPSRI_patch = mpatches.Patch(color='blue', label='mlGPSRawInt')
        mlVfrHud_patch = mpatches.Patch(color='grey', label='mlVfrHud')
        handles = [mlGPI_patch, mlGPSRI_patch, mlVfrHud_patch]
        self.ax.legend(handles=handles, loc='upper left', fontsize=18)
        self.fig.set_size_inches(14.0, 14.0)
        self.fig.savefig(filepath)
        plt.close(self.fig)
