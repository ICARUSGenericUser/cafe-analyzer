import math

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from geographiclib import geodesic as geo
#from termcolor import colored
import numpy as np
import warnings

from scipy.optimize import curve_fit
from scipy import interpolate
import matplotlib

'''
matplotlib.rc('xtick', labelsize=20)
matplotlib.rc('ytick', labelsize=20)

horizontalSpeed = 10.0
verticalSpeedUP = 1.5
verticalSpeedDN = 2.5

#v100
#y = 0.375 * x + 0.779
climbCurve = ['1', 0.375, 0.779]
climbOffset = 0

climbCurvev08 = ['1', 0.483, 1.366]
climbCurvev15 = ['1', 0.259, 1.529]
climbCurvevmDrB = ['1', 0.189, 2.602]

#climbOffset = 2.5

# y = 0.641 * x + 1.8
descentCurve = ['1', 0.641, 1.8]
descentOffset = 0

descentCurvev08 = ['1', 0.802, 2.204]
descentCurvev15 = ['1', 0.423, 1.492]
descentCurvev20 = ['1', 0.322, 1.108]
descentCurvevmDrB = ['1', 0.195, 2.226]

#descentOffset = 2.5

# y = 0.408 * x + 4.852
takeOffCurve = ['1', 4.01400E-01, 5.22203]
takeOffOffset = 0

takeOffCurvev08 = ['1', 0.512, 4.555]
takeOffCurvev2 = ['1', 0.200, 7.999]
takeOffCurvevM = ['2', 2.57820E-01, -1.35423E-03, 6.63215]
takeOffCurvevmDr = ['1', 1.61084E-01, 8.16472]
takeOffCurvevmDrB = ['1', 1.99987E-01, 7.61655]

# y = 0.654 * x + 16.594
landingCurve = ['1', 6.5463E-01, 16.587]
landingOffset = 0

landingCurvev08 = ['1', 0.837, 14.017]
landingCurvev2 = ['1', 0.322, 21.34]
landingCurvevM = ['1', 3.58772E-01, 10.5497]
landingCurvevmDr = ['1', 2.07561E-01, 2.34492E+01] #5
landingCurvevmDrB = ['1', 2.56356E-01, 2.19616E+01] #8

# y = 0.081 * x + 2.037
SSCurve = ['2', 8.10609E-2, 1.05924E-4, 2.03705]
SSOffset = 0

# At =
NVClimbCurve = ['1', 3.70281E-01, 5.86275]
NVClimbOffset = 0

NVClimbCurvev08 = ['1', 5.48231E-01,1.9882]
NVClimbCurvev15 = ['1', 3.41571E-01, 3.1881]
NVClimbCurvevmDrB = ['1', 2.75861E-01,4.3657]

# At = 0.614 * x + 5.302
NVDescentCurve = ['1', 6.38704E-01,4.43658]
NVDescentOffset = 0
NVDescentCurve15 = ['1', 7.2449E-01,1.22079]
NVDescentCurve08 = ['1', 8.79298E-01,2.4998E-01]
NVDescentCurvevmDrB = ['1', 1.69775E-01,8.83554]


#Curves from Speed y = m·x + a
#Landing
mLD = ['2', -3.46288E-01, 2.95022E-02, 1.15048]
aLD = ['2', 5.28964, -4.62007E-01, 9.08541]
#TakeOff
mTO = ['2', -2.06576E-01, 1.50818E-02, 8.49821E-01]
aTO = ['2', 2.04065, -1.407E-01, 8.60358E-01]
#VC
mVC = ['2', -2.84535E-01,2.72637E-02, 9.32913E-01]
aVC = ['2',-1.67665, 3.08446E-01, 3.32033]
#VC
mVD = ['2', -5.09035E-01,5.77155E-02,1.30151]
aVD = ['2', -1.739891, 2.82408E-01, 3.85737]
#NVC
mNVC = ['2',-1.21201,1.90247E-01,2.21127]
aNVC = ['2',3.31775E+01,-5.65075,-4.17638E+01]
#NVC
mNVD = ['2',-4.62054E-02,-1.89867E-02,8.78215E-01]
aNVD = ['2',-1.79599, 5.80066E-01, 3.25858]

def InterpolateClimbDescent(filepath,curveFittingVariable,curveFittingVariable2, HorizontalSpeedClimb, HorizontalSpeedDescent, VerticalClimbEventsAltitudeVariance, VerticalClimbEventsDeltaTime, VerticalDescentEventsAltitudeVariance,VerticalDescentEventsDeltaTime,NonVerticalClimbEventsAltitudeVariance, NonVerticalClimbEventsDeltaTime, NonVerticalDescentEventsAltitudeVariance,NonVerticalDescentEventsDeltaTime, NonVerticalClimbEventsHorizontalVariance, NonVerticalDescentEventsHorizontalVariance, takeOffAltitudeVariance, takeoffDeltaTime, landingAltitudeVariance, landingDeltaTime):
    getCurveEvolution(filepath, "Landing")
    getCurveEvolution(filepath, "TakeOff")
    getCurveEvolution(filepath, "VC")
    getCurveEvolution(filepath, "VD")
    getCurveEvolution(filepath, "NVC")
    getCurveEvolution(filepath, "NVD")

    cL=getAltitudeChangeCurve("Landing", 1.2)
    cT=getAltitudeChangeCurve("TakeOff", 2.0)
    cV=getAltitudeChangeCurve("VC", 2.0)
    cD=getAltitudeChangeCurve("VD", 1.2)
    cNV=getAltitudeChangeCurve("NVC", 2.0)
    cND=getAltitudeChangeCurve("NVD", 1.2)

    # TakeOff & Landing
    getTakeOffLandingPlot(filepath + "TakeOff_TimeAltitudeCurveFitting-",
                                          takeOffAltitudeVariance, takeoffDeltaTime, event="TakeOff")
    #multipleTakeOffCurveFitting(filepath + "TakeOff_TimeAltitudeCurveFitting-", takeOffAltitudeVariance, takeoffDeltaTime)
    getTakeOffLandingPlot(filepath + "Landing_TimeAltitudeCurveFitting-",
                                          landingAltitudeVariance, landingDeltaTime, event="Landing")
    #multipleLandingCurveFitting(filepath + "Landing_TimeAltitudeCurveFitting-", landingAltitudeVariance, landingDeltaTime)

    # Vertical Climb
    multipleClimbCurveFitting(filepath + "MultipleClimb_TimeAltitudeCurveFitting-",VerticalClimbEventsAltitudeVariance, VerticalClimbEventsDeltaTime)
    getVerticalClimbAltitudeVariationPlot(filepath + "Climb_TimeAltitudeCurveFitting-",VerticalClimbEventsAltitudeVariance, VerticalClimbEventsDeltaTime)
    # Vertical Descent
    getVerticalDescentAltitudeVariationPlot(filepath + "Descent_TimeAltitudeCurveFitting-",VerticalDescentEventsAltitudeVariance, VerticalDescentEventsDeltaTime)
    multipleDescentCurveFitting(filepath + "MultipleDescent_TimeAltitudeCurveFitting-",VerticalDescentEventsAltitudeVariance, VerticalDescentEventsDeltaTime)

    # TAG EVENTS
    
    getVerticalClimbAltitudeVariationPlotTAG(filepath + "Climb2_TimeAltitudeCurveFitting-",
                                             VerticalClimbEventsAltitudeVariance, VerticalClimbEventsDeltaTime,
                                             curveFittingVariable)
    getVerticalDescentAltitudeVariationPlotTAG(filepath + "TAG_Descent_TimeAltitudeCurveFitting-",
                                               VerticalDescentEventsAltitudeVariance, VerticalDescentEventsDeltaTime,
                                               curveFittingVariable2)

    # Non-Vertical Climb
    getNonVerticalClimbAltitudeVariationPlot(filepath + "NVC_TimeAltitudeCurveFitting-",NonVerticalClimbEventsAltitudeVariance, NonVerticalClimbEventsDeltaTime, NonVerticalClimbEventsHorizontalVariance)
    #getNonVerticalClimbHorizontalVarianceSpeed(filepath + "NVC_TimeAltitudeCurveFitting-", HorizontalSpeedClimb,
    #                                         NonVerticalClimbEventsDeltaTime, NonVerticalClimbEventsHorizontalVariance, NonVerticalClimbEventsAltitudeVariance)

    # Non-Vertical Descent
    getNonVerticalDescentAltitudeVariationPlot(filepath + "NVD_TimeAltitudeCurveFitting-",
                                               NonVerticalDescentEventsAltitudeVariance,
                                               NonVerticalDescentEventsDeltaTime,
                                               NonVerticalDescentEventsHorizontalVariance)

def getTakeOffLandingPlot(filepath, AltitudeVariance, DeltaTime, event):
    if len(AltitudeVariance) < 5:
        print("Not enough TAKEOFF/LANDING's samples to interpolate")
        return 1

    figTO = plt.figure()
    #ggplot style
    plt.style.use('ggplot')
    axTO = figTO.add_subplot(111)
    axTO.set_xlabel('Ah (m)', fontsize=18)
    axTO.set_ylabel('Time (s)', fontsize=18)

    x_line = np.arange(0.0, 100.0, 0.25)

    # Time
    # define a sequence of inputs between the smallest and largest known inputs
    x_line = np.arange(min(AltitudeVariance), max(AltitudeVariance), 0.25)
    
    # Curve order one: y = a * x + b
    popt, _ = curve_fit(curveObjectiveOrder1, AltitudeVariance, DeltaTime)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder1(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(format(a, '.5E')) + " * x + " + str(format(b, '.5E'))
    axTO.plot(x_line, curve, marker='_', color='red', label=equation, linestyle='-')

    # Curve order two: y = (a * x) + (b * x**2) + c
    popt, _ = curve_fit(curveObjectiveOrder2, AltitudeVariance, DeltaTime)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder2(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(format(a, '.5E')) + " * x + " + str(format(b, '.5E')) + " * x^2 + " + str(format(c, '.5E'))
    axTO.plot(x_line, curve, marker='_', color='blue', label=equation)


    x_line = np.arange(0, 100, 1)
    if event == 'TakeOff':
        # Check curve order
        curveOrder = takeOffCurvev2[0]
        if curveOrder == '1':
            a = takeOffCurvev2[1]
            b = takeOffCurvev2[2] + takeOffOffset
            y_line = a * x_line + b
            equation = "Curve Fitting: v = 5 m/s ; y = " + str(round(a, 3)) + " * x + " + str(round(b, 3))
            axTO.plot(x_line, y_line, color='blue', label=equation,linestyle='dashdot',linewidth = 4)
        curveOrder = takeOffCurve[0]
        if curveOrder == '1':
            a = takeOffCurve[1]
            b = takeOffCurve[2] + takeOffOffset
            y_line = a * x_line + b
            equation = "Curve Fitting: v = 2.5 m/s ; y = " + str(round(a, 3)) + " * x + " + str(round(b, 3))
            axTO.plot(x_line, y_line, color='red', label=equation, linestyle='solid',linewidth = 4)
        curveOrder = takeOffCurvev08[0]
        if curveOrder == '1':
            a = takeOffCurvev08[1]
            b = takeOffCurvev08[2] + takeOffOffset
            y_line = a * x_line + b
            equation = "Curve Fitting: v = 1.25 m/s ; y = " + str(round(a, 3)) + " * x + " + str(round(b, 3))
            axTO.plot(x_line, y_line,  color='orange', label=equation, linestyle='dotted',linewidth = 4)
        curveOrder = takeOffCurvevM[0]
        if curveOrder == '1':
            a = takeOffCurvevM[1]
            b = takeOffCurvevM[2] + takeOffOffset
            y_line = a * x_line + b
            equation = "Curve Fitting: v = v max ; y = " + str(round(a, 3)) + " * x + " + str(round(b, 3))
            axTO.plot(x_line, y_line,  color='black', label=equation, linestyle='solid', linewidth = 4)


    elif event == 'Landing':
        # Check curve order
        curveOrder = landingCurvev2[0]
        if curveOrder == '1':
            a = landingCurvev2[1]
            b = landingCurvev2[2] + landingOffset
            y_line = a * x_line + b
            equation = "Curve Fitting: v = 3 m/s ; y = " + str(round(a, 3)) + " * x + " + str(round(b, 3))
            axTO.plot(x_line, y_line,  color='blue', label=equation,linestyle='dashdot',linewidth = 4)
        curveOrder = landingCurve[0]
        if curveOrder == '1':
            a = landingCurve[1]
            b = landingCurve[2] + landingOffset
            y_line = a * x_line + b
            equation = "Curve Fitting: v = 1.5 m/s ; y = " + str(round(a, 3)) + " * x + " + str(round(b, 3))
            axTO.plot(x_line, y_line, color='red', label=equation,linestyle='solid',linewidth = 4)
        curveOrder = landingCurvev08[0]
        if curveOrder == '1':
            a = landingCurvev08[1]
            b = landingCurvev08[2] + landingOffset
            y_line = a * x_line + b
            equation = "Curve Fitting: v = 0.75 m/s ; y = " + str(round(a, 3)) + " * x + " + str(round(b, 3))
            axTO.plot(x_line, y_line,  color='orange', label=equation, linestyle='dotted',linewidth = 4)



    axTO.scatter(AltitudeVariance, DeltaTime, marker='o', color='black')
    axTO.legend(fontsize=18)
    plt.title(event + " Altitude variation[Ah] vs Time variation[At]", fontsize=20)
    plt.show()
    figTO.set_size_inches(14.0, 14.0)
    figTO.savefig(filepath + "-curveFitting.png")
    plt.close(figTO)

def getVerticalClimbAltitudeVariationPlot(filepath, AltitudeVariance, DeltaTime):
    if len(AltitudeVariance) < 5:
        print("Not enough VerticalClimbAltitude's samples to interpolate")
        return 1

    #plot lower region(where we have more samples)
    #AltitudeVariance, DeltaTime = getAltitudChangeRegion(0.0, 10.0, AltitudeVariance, DeltaTime)

    figI = plt.figure()
    #ggplot style
    plt.style.use('ggplot')
    axI = figI.add_subplot(111)
    axI.set_xlabel('Ah (m)', fontsize=18)
    axI.set_ylabel('Time (s)', fontsize=18)

    # Time
    # define a sequence of inputs between the smallest and largest known inputs
    x_line = np.arange(0, 60, 1)

    # v100
    # Check curve order
    curveOrder = climbCurve[0]
    if curveOrder == '1':
        a = climbCurve[1]
        b = climbCurve[2] + climbOffset
        y_line = a * x_line + b
        equation = "Curve Fitting: v = 2.5 m/s; y = " + str(round(a, 3)) + " * x + " + str(round(b, 3))
        axI.plot(x_line, y_line,  color='red', label=equation,linestyle='solid',linewidth = 4)

    curveOrder = climbCurvev15[0]
    if curveOrder == '1':
        a = climbCurvev15[1]
        b = climbCurvev15[2] + climbOffset
        y_line = a * x_line + b
        equation = "Curve Fitting: v = 3.75 m/s ;y = " + str(round(a, 3)) + " * x + " + str(round(b, 3))
        axI.plot(x_line, y_line,  color='green', label=equation,linestyle='dashdot',linewidth = 4)
#
    # v50
    # Check curve order
    curveOrder = climbCurvev08[0]
    if curveOrder == '1':
        a = climbCurvev08[1]
        b = climbCurvev08[2]
        y_line = a * x_line + b
        equation = "Curve Fitting: v = 2.0 m/s ;y = " + str(round(a, 3)) + " * x + " + str(round(b, 3))
        axI.plot(x_line, y_line, color='orange', label=equation, linestyle='dotted',linewidth = 4)
    curve, offset = getAltitudeChangeCurve("VC", 5)
    curveOrder = curve[0]
    if curveOrder == '1':
        a = curve[1]
        b = curve[2] + offset
        y_line = a * x_line + b
        equation = "Curve Fitting: vmax = 5 m/s ; y = " + str(round(a, 3)) + " * x + " + str(round(b, 3))
        axI.plot(x_line, y_line,  color='black', label=equation,linestyle='solid',linewidth = 4)

    axI.scatter(AltitudeVariance, DeltaTime, marker='o', color='black')


    axI.legend(fontsize=18)
    plt.title("Vertical Climb Altitude variation[Ah] vs Time variation[At]", fontsize=20)
    plt.show()
    figI.set_size_inches(14.0, 14.0)
    figI.savefig(filepath + "-curveFitting.png")
    plt.close(figI)


def getVerticalClimbAltitudeVariationPlotTAG(filepath, AltitudeVariance, DeltaTime, TAG):
    if len(AltitudeVariance) < 5:
        print("Not enough VerticalClimbAltitude's samples to interpolate")
        return 1

    figI = plt.figure()
    # seaborn-darkgrid style
    plt.style.use('ggplot')
    axI = figI.add_subplot(111)
    axI.set_xlabel('Ah (m)', fontsize=18)
    axI.set_ylabel('Time (s)', fontsize=18)

    # Time
    # define a sequence of inputs between the smallest and largest known inputs
    x_line = np.arange(min(AltitudeVariance), max(AltitudeVariance), 1)

    # Using Scipy interpolate function
    # f = interpolate.interp1d(xTimeValues, yVSpeedValues, kind= 'cubic')
    # y_line = f(x_line)

    # Using Scipy curve fit function
    warnings.filterwarnings('ignore', message='Covariance of the parameters could not be estimated')
    popt, _ = curve_fit(curveObjectiveOrder1, AltitudeVariance, DeltaTime)
    # summarize the parameter values
    a, b, c, d, e = popt

    # calculate the output for the range
    y_line = curveObjectiveOrder1(x_line, a, b, c, d, e)

    axI.scatter(AltitudeVariance, DeltaTime, marker='o', color='black')

    #Tag samples to identify the weird ones
    index = 0
    while index < len(AltitudeVariance):
        axI.text(AltitudeVariance[index] + 0.025, DeltaTime[index] + 0.025, str(TAG[index]), size=7, zorder=1,
                 color='k')
        index = index + 1

    equation = "Curve Fitting: y = " + str(round(a,3)) + " * x + " + str(round(b,3))
    #axI.plot(x_line, y_line, marker='_', color='red', label=equation)
    axI.legend(fontsize=18)
    plt.title("Vertical Climb Curve Fitting\nAnalysing initial Vertical Speed(m/s)", fontsize=20)
    plt.show()
    figI.set_size_inches(14.0, 14.0)
    figI.savefig(filepath + "-curveFitting_CLIMB_TAG.png")
    plt.close(figI)


def getVerticalDescentAltitudeVariationPlot(filepath, AltitudeVariance, DeltaTime):
    if len(AltitudeVariance) < 5:
        print("Not enough VerticalDescent's samples to interpolate")
        return 1

    figI = plt.figure()
    #ggplot style
    plt.style.use('ggplot')
    axI = figI.add_subplot(111)
    axI.set_xlabel('Ah (m)', fontsize=18)
    axI.set_ylabel('Time (s)', fontsize=18)

    # Time
    # define a sequence of inputs between the smallest and largest known inputs
    x_line = np.arange(0, 60, 1)

    # Check curve order
    curveOrder = descentCurve[0]
    if curveOrder == '1':
        a = descentCurve[1]
        b = descentCurve[2] + descentOffset
        y_line = a * x_line + b
        equation = "Curve Fitting: v = 1.5 m/s ;y = " + str(round(a, 3)) + " * x + " + str(round(b, 3))
        axI.plot(x_line, y_line,  color='red', label=equation,linestyle='solid',linewidth = 4)

    curveOrder = descentCurvev15[0]
    if curveOrder == '1':
        a = descentCurvev15[1]
        b = descentCurvev15[2] + descentOffset
        y_line = a * x_line + b
        equation = "Curve Fitting: v = 2.25 m/s ;y = " + str(round(a, 3)) + " * x + " + str(round(b, 3))
        axI.plot(x_line, y_line,  color='green', label=equation,linestyle='dashdot',linewidth = 4)
#
    curveOrder = descentCurvev08[0]
    if curveOrder == '1':
        a = descentCurvev08[1]
        b = descentCurvev08[2] + descentOffset
        y_line = a * x_line + b
        equation = "Curve Fitting: v = 1.2 m/s ;y = " + str(round(a, 3)) + " * x + " + str(round(b, 3))
        axI.plot(x_line, y_line,  color='orange', label=equation,linestyle='dotted',linewidth = 4)
#
    curve, offset = getAltitudeChangeCurve("VD", 8)
    curveOrder = curve[0]
    if curveOrder == '1':
        a = curve[1]
        b = curve[2] + offset
        y_line = a * x_line + b
        equation = "Curve Fitting: vmax = 8 m/s ; y = " + str(round(a, 3)) + " * x + " + str(round(b, 3))
        axI.plot(x_line, y_line, marker='_', color='black', label=equation)

    axI.scatter(AltitudeVariance, DeltaTime, marker='o', color='black')
    axI.legend(fontsize=18)
    plt.title("Vertical Descent Altitude variation[Ah] vs Time variation[At]", fontsize=20)
    plt.show()
    figI.set_size_inches(14.0, 14.0)
    figI.savefig(filepath + "-curveFitting.png")
    plt.close(figI)


def getVerticalDescentAltitudeVariationPlotTAG(filepath, AltitudeVariance, DeltaTime, TAG):
    if len(AltitudeVariance) < 5:
        print("Not enough VerticalClimbAltitude's samples to interpolate")
        return 1

    #plot higher region(where we have more samples)
    #AltitudeVariance, DeltaTime = getAltitudChangeRegion(12.0, 21.0, AltitudeVariance, DeltaTime)

    figI = plt.figure()
    #ggplot style
    plt.style.use('ggplot')
    axI = figI.add_subplot(111)
    axI.set_xlabel('Ah (m)', fontsize=18)
    axI.set_ylabel('Time (s)', fontsize=18)

    # Time
    # define a sequence of inputs between the smallest and largest known inputs
    x_line = np.arange(min(AltitudeVariance), max(AltitudeVariance), 1)

    # Using Scipy interpolate function
    # f = interpolate.interp1d(xTimeValues, yVSpeedValues, kind= 'cubic')
    # y_line = f(x_line)

    # Using Scipy curve fit function
    warnings.filterwarnings('ignore', message='Covariance of the parameters could not be estimated')
    popt, _ = curve_fit(curveObjectiveOrder1, AltitudeVariance, DeltaTime)
    # summarize the parameter values
    a, b, c, d, e = popt

    # calculate the output for the range
    y_line = curveObjectiveOrder1(x_line, a, b, c, d, e)

    axI.scatter(AltitudeVariance, DeltaTime, marker='o', color='black')

    #Tag samples to identify the weird ones
    index = 0
    while index < len(AltitudeVariance):
        axI.text(AltitudeVariance[index] + 0.025, DeltaTime[index] + 0.025, str(TAG[index]), size=7, zorder=1,
                 color='k')
        index = index + 1

    equation = "Curve Fitting: y = " + str(round(a,3)) + " * x + " + str(round(b,3))
    #axI.plot(x_line, y_line, marker='_', color='red', label=equation)
    axI.legend(fontsize=18)
    plt.title("Vertical Descent Altitude variation[Ah] vs Time variation[At]", fontsize=20)
    plt.show()
    figI.set_size_inches(14.0, 14.0)
    figI.savefig(filepath + "-curveFitting_DESCENT_TAG.png")
    plt.close(figI)


def getNonVerticalClimbAltitudeVariationPlot(filepath, AltitudeVariance, DeltaTime, HorizontalVariance):

    if len(AltitudeVariance) <= 6:
        print("Not enough NonVerticalClimb's samples to interpolate")
        return 1

    #3d plot. Non-vertical climb
    fig = plt.figure()
    # ggplot style
    plt.style.use('ggplot')
    ax = fig.add_subplot(111, projection='3d')
    ax.set_xlabel('Ah (m)',fontsize=18)
    ax.set_ylabel('Horizontal Variance (m)',fontsize=18)
    ax.set_zlabel('Time (ms)',fontsize=18)
    plt.title("Dynamic altitude variance plot(NON VERTICAL CLIMB)",fontsize=20)
    ax.scatter(AltitudeVariance, DeltaTime, HorizontalVariance, marker='o', color='gray')

    #3d curve fitting
    x_line = np.arange(min(AltitudeVariance), max(AltitudeVariance), 1)
    y_line = np.linspace(min(DeltaTime), max(DeltaTime), num=len(x_line))
    a, b, c, d = 2.0, 3.0, 1.0, 1.0
    z_line = curveObjective3D((x_line, y_line), a, b, c, d)
    p0 = [0, 0, 0, 0]
    warnings.filterwarnings('ignore', message='Covariance of the parameters could not be estimated')
    popt, _ = curve_fit(curveObjective3D, (x_line, y_line), z_line, p0)
    a, b, c, d = popt
    z_line = curveObjective3D((x_line, y_line), a, b, c, d)

    equation = "Curve Fitting: z =" + str(round(a,3)) + " * x + " + str(round(b,3)) + " * y + " + str(round(c,3))
    ax.plot(x_line, y_line, z_line, marker='_', color='red', label=equation)


    ax.legend(fontsize=18)
    plt.show()
    fig.set_size_inches(14.0, 14.0)
    fig.savefig(filepath + "-NVC-curveFitting.png")
    plt.close(fig)

    #3d projection into 2d plots
    fig = plt.figure()
    ax1 = fig.add_subplot(311)
    plt.title("Dynamic altitude variance plot(NON VERTICAL CLIMB) 2D Projection")
    ax1.set_xlabel('Ah (m)',fontsize=18)
    ax1.set_ylabel('Time (s)',fontsize=18)
    ax2 = fig.add_subplot(312)
    ax2.set_xlabel('Horizontal Variance (m)',fontsize=18)
    ax2.set_ylabel('Time (s)',fontsize=18)
    ax2.plot(z_line, y_line, marker='_', color='red')
    ax3 = fig.add_subplot(313)
    ax3.set_xlabel('Horizontal Variance (m)',fontsize=18)
    ax3.set_ylabel('Ah (m)',fontsize=18)
    ax3.plot(z_line, x_line, marker='_', color='red')
    ax1.scatter(AltitudeVariance, DeltaTime, marker='o', color='gray')
    ax2.scatter(HorizontalVariance, DeltaTime, marker='o', color='gray')
    ax3.scatter(HorizontalVariance, AltitudeVariance, marker='o', color='gray')
    angle = np.arctan([i / j for i, j in zip(AltitudeVariance, HorizontalVariance)])  #Angle of climb/descent [rad] Arctan of the Division Position by Position of Ay/Ax

    #plot text (angle of Climb/Descent)
    index = 0
    print("________________________________________________________________________________")
    while index < len(HorizontalVariance):
        ax3.text(HorizontalVariance[index], AltitudeVariance[index]," α: " + str(round(angle[index] * 180 / np.pi ,3)), size=9, zorder=1,color='k', rotation=45)
        #ax1.text(AltitudeVariance[index]+ 0.025, DeltaTime[index] + 0.025, str(round(AltitudeVariance[index],3)) + " ; " + str(round(DeltaTime[index],3)) ,size=9, zorder=1, color='k', rotation=45)
        AxT = HorizontalVariance[index] / horizontalSpeed
        AzT = AltitudeVariance[index] / verticalSpeedUP
        print("NVC; AxT: " + colored(str(round(AxT, 3)),'red') + " s; Ax: " + str(round(HorizontalVariance[index], 3)) + " m; AzT; " + colored(str(round(AzT, 3)),'red') + " s; Ax: " + str(round(AltitudeVariance[index], 3)) + " m; RealTime: " + colored(str(round(DeltaTime[index], 3)),'red'))
        index += 1
    print("________________________________________________________________________________")

    x_line = np.arange(min(AltitudeVariance), max(AltitudeVariance), 0.25)

    # Curve order one: y = a * x + b
    popt, _ = curve_fit(curveObjectiveOrder1, AltitudeVariance, DeltaTime)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder1(x_line, a, b, c, d, e)

    #equation = "Curve Fitting: y = " + str(format(a,'.5E')) + " * x + " + str(format(b,'.5E'))
    #ax1.plot(x_line, curve, marker='_', color='blue', label=equation)

    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    plt.title("Dynamic altitude variance plot(NON VERTICAL CLIMB)", fontsize=20)
    ax1.set_xlabel('Ah (m)', fontsize=18)
    ax1.set_ylabel('Time (s)', fontsize=18)

    # Check curve order
    curveOrder = NVClimbCurve[0]
    if curveOrder == '1':
        a = NVClimbCurve[1]
        b = NVClimbCurve[2]
        y_line = a * x_line + b
        equation = "Curve Fitting: WP10;UP1.5;DN2.5 ;y = " + str(format(a,'.5E')) + " * x + " + str(format(b,'.5E'))
        ax1.plot(x_line, y_line, color='red', label=equation ,linestyle='solid',linewidth = 4)

    curveOrder = NVClimbCurvev15[0]
    if curveOrder == '1':
        a = NVClimbCurvev15[1]
        b = NVClimbCurvev15[2]
        y_line = a * x_line + b
        equation = "Curve Fitting: WP15;UP2.25;DN3.75 ;y = " + str(format(a, '.5E')) + " * x + " + str(format(b, '.5E'))
        ax1.plot(x_line, y_line, color='green', label=equation,linestyle='dashdot',linewidth = 4)

    curveOrder = NVClimbCurvev08[0]
    if curveOrder == '1':
        a = NVClimbCurvev08[1]
        b = NVClimbCurvev08[2]
        y_line = a * x_line + b
        equation = "Curve Fitting: WP8;UP1.2;DN2 ;y = " + str(format(a, '.5E')) + " * x + " + str(format(b, '.5E'))
        ax1.plot(x_line, y_line,  color='orange', label=equation,linestyle='dotted',linewidth = 4)


    # Check curve order
    curveOrder = NVClimbCurvev5[0]
    if curveOrder == '1':
        a = NVClimbCurvev5[1]
        b = NVClimbCurvev5[2]
        y_line = a * x_line + b
        equation = "Curve Fitting: v50 ;y = " + str(round(a, 3)) + " * x + " + str(round(b, 3))
        ax1.plot(x_line, y_line, marker='_', color='gren', label=equation)

    # Check curve order
    curveOrder = NVClimbCurvevx1vz15[0]
    if curveOrder == '1':
        a = NVClimbCurvevx1vz15[1]
        b = NVClimbCurvevx1vz15[2]
        y_line = a * x_line + b
        equation = "Curve Fitting: vx1vz15 ;y = " + str(round(a, 3)) + " * x + " + str(round(b, 3))
        ax1.plot(x_line, y_line, marker='_', color='orange', label=equation)



    ax1.legend(fontsize=18)
    plt.show()
    fig.set_size_inches(14.0, 14.0)
    fig.savefig(filepath + "-NVC-Projection-curveFitting.png")
    plt.close(fig)
    
    index = 0
    while index < len(HorizontalVariance):
        # y = 1.02688E-1 * x + 7.51618E-1
        SSCurve = ['1', 1.02688E-1, 7.51618E-1]

        a = SSCurve[1]
        b = SSCurve[2]
        DeltaTimeSS = a * HorizontalVariance[index] +  b


        a = climbCurve[1]
        b = climbCurve[2] + climbOffset
        DeltaTimeVC = a * AltitudeVariance[index] + b

        print("Real AT: " + str(DeltaTime[index]))
        print("SS AT: " + str(DeltaTimeSS) + "; SS dist: " + str(round(HorizontalVariance[index],3)))
        print("VC AT: " + str(DeltaTimeVC) + "; VC dist: " + str(round(AltitudeVariance[index],3)))
        print("Dif: " + str(DeltaTime[index] - (DeltaTimeVC + DeltaTimeSS)))
        print("---------------------------------------")

        index = index + 1




def getNonVerticalDescentAltitudeVariationPlot(filepath, AltitudeVariance, DeltaTime, HorizontalVariance):
    if len(AltitudeVariance) <= 6:
        print("Not enough NonVerticalDescent's samples to interpolate")
        return 1

    #3d plot. Non-vertical climb
    fig = plt.figure()
    # ggplot style
    plt.style.use('ggplot')
    ax = fig.add_subplot(111, projection='3d')
    ax.set_xlabel('Ah (m)')
    ax.set_ylabel('Horizontal Variance (m)')
    ax.set_zlabel('Time (ms)')
    plt.title("Dynamic altitude variance plot(NON VERTICAL DESCENT)")
    ax.scatter(AltitudeVariance, DeltaTime, HorizontalVariance, marker='o', color='gray')

    #3d curve fitting
    x_line = np.arange(min(AltitudeVariance), max(AltitudeVariance), 1)
    y_line = np.linspace(min(DeltaTime), max(DeltaTime), num=len(x_line))
    a, b, c, d = 2.0, 3.0, 1.0, 1.0
    z_line = curveObjective3D((x_line, y_line), a, b, c, d)
    p0 = [0, 0, 0, 0]
    warnings.filterwarnings('ignore', message='Covariance of the parameters could not be estimated')
    popt, _ = curve_fit(curveObjective3D, (x_line, y_line), z_line, p0)
    a, b, c, d = popt
    z_line = curveObjective3D((x_line, y_line), a, b, c, d)

    equation = "Curve Fitting: z =" + str(round(a,3)) + " * x + " + str(round(b,3)) + " * y + " + str(round(c,3))
    ax.plot(x_line, y_line, z_line, marker='_', color='red', label=equation)


    ax.legend(fontsize=18)
    plt.show()
    fig.set_size_inches(14.0, 14.0)
    fig.savefig(filepath + "-NVD-curveFitting.png")
    plt.close(fig)

    #3d projection into 2d plots
    fig = plt.figure()
    ax1 = fig.add_subplot(311)
    plt.title("Dynamic altitude variance plot(NON VERTICAL DESCENT) 2D Projection")
    ax1.set_xlabel('Ah (m)')
    ax1.set_ylabel('Time (s)')

    ax2 = fig.add_subplot(312)
    ax2.set_xlabel('Horizontal Variance (m)')
    ax2.set_ylabel('Time (s)')
    ax2.plot(z_line, y_line, marker='_', color='red')
    ax3 = fig.add_subplot(313)
    ax3.set_xlabel('Horizontal Variance (m)')
    ax3.set_ylabel('Ah (m)')
    ax3.plot(z_line, x_line, marker='_', color='red')
    ax1.scatter(AltitudeVariance, DeltaTime, marker='o', color='gray')
    ax2.scatter(HorizontalVariance, DeltaTime, marker='o', color='gray')
    ax3.scatter(HorizontalVariance, AltitudeVariance, marker='o', color='gray')
    angle = np.arctan([i / j for i, j in zip(AltitudeVariance, HorizontalVariance)])  #Angle of climb/descent [rad] Arctan of the Division Position by Position of Ay/Ax

    #plot text (angle of Climb/Descent)
    index = 0
    while index < len(HorizontalVariance):
        ax3.text(HorizontalVariance[index], AltitudeVariance[index]," α: " + str(round(angle[index] * 180 / np.pi ,3)), size=9, zorder=1,color='k', rotation=45)
        index += 1

    x_line = np.arange(min(AltitudeVariance), max(AltitudeVariance), 0.25)

    # Curve order one: y = a * x + b
    popt, _ = curve_fit(curveObjectiveOrder1, AltitudeVariance, DeltaTime)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder1(x_line, a, b, c, d, e)

    #equation = "Curve Fitting: y = " + str(format(a,'.5E')) + " * x + " + str(format(b,'.5E'))
    #ax1.plot(x_line, curve, marker='_', color='blue', label=equation)

    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    plt.title("Dynamic altitude variance plot(NON VERTICAL DESCENT)", fontsize=20)
    ax1.set_xlabel('Ah (m)', fontsize=18)
    ax1.set_ylabel('Time (s)', fontsize=18)
    x_line = np.arange(0, 100, 1)
    

    # Check curve order
    curveOrder = NVDescentCurve[0]
    if curveOrder == '1':
        a = NVDescentCurve[1]
        b = NVDescentCurve[2]
        y_line = a * x_line + b
        equation = "Curve Fitting: WP10;UP1.5;DN2.5 ;y = " + str(format(a,'.5E')) + " * x + " + str(format(b,'.5E'))
        ax1.plot(x_line, y_line, marker='_', color='red', label=equation,linestyle='solid',linewidth = 4)


   #urveOrder = NVDescentCurve15[0]
   #if curveOrder == '1':
   #    a = NVDescentCurve15[1]
   #    b = NVDescentCurve15[2]
   #    y_line = a * x_line + b
   #    equation = "Curve Fitting: WP15;UP2.25;DN3.75 ;y = " + str(format(a, '.5E')) + " * x + " + str(format(b, '.5E'))
   #    ax1.plot(x_line, y_line, marker='_', color='green', label=equation,linestyle='dashdot',linewidth = 4)

   #curveOrder = NVDescentCurve08[0]
   #if curveOrder == '1':
   #    a = NVDescentCurve08[1]
   #    b = NVDescentCurve08[2]
   #    y_line = a * x_line + b
   #    equation = "Curve Fitting: WP8;UP1.2;DN2 ;y = " + str(format(a, '.5E')) + " * x + " + str(format(b, '.5E'))
   #    ax1.plot(x_line, y_line, marker='_', color='orange', label=equation,linestyle='dotted',linewidth = 4)


    ax1.legend(fontsize=18)

    plt.show()
    fig.set_size_inches(14.0, 14.0)
    fig.savefig(filepath + "-NVD-Projection-curveFitting.png")
    plt.close(fig)

    
    index = 0
    while index < len(HorizontalVariance):
        # y = 1.02688E-1 * x + 7.51618E-1
        SSCurve = ['1', 1.02688E-1, 7.51618E-1]

        a = SSCurve[1]
        b = SSCurve[2]
        DeltaTimeSS = a * HorizontalVariance[index] +  b


        a = climbCurve[1]
        b = climbCurve[2] + climbOffset
        DeltaTimeVC = a * AltitudeVariance[index] + b

        print("Real AT: " + str(DeltaTime[index]))
        print("SS AT: " + str(DeltaTimeSS) + "; SS dist: " + str(round(HorizontalVariance[index],3)))
        print("VC AT: " + str(DeltaTimeVC) + "; VC dist: " + str(round(AltitudeVariance[index],3)))
        print("Dif: " + str(DeltaTime[index] - (DeltaTimeVC + DeltaTimeSS)))
        print("---------------------------------------")

        index = index + 1


def getNonVerticalClimbHorizontalVarianceSpeed(filepath, HorizontalSpeed, DeltaTime, HorizontalVariance, AltitudeVariance):
    #3d plot. Non-vertical climb
    fig = plt.figure()
    # ggplot style
    plt.style.use('ggplot')
    ax = fig.add_subplot(111, projection='3d')
    ax.set_xlabel('Horizontal Speed Variation(m/s)', fontsize=18)
    ax.set_ylabel('Horizontal Variance (m)')
    ax.set_zlabel('Time (ms)')
    plt.title("Dynamic altitude variance plot(NON VERTICAL CLIMB)", fontsize=20)




    #3d curve fitting
    x_line = np.arange(min(HorizontalSpeed), max(HorizontalSpeed), 1)
    y_line = np.linspace(min(DeltaTime), max(DeltaTime), num=len(x_line))
    a, b, c, d = 2.0, 3.0, 1.0, 1.0
    z_line = curveObjective3D((x_line, y_line), a, b, c, d)
    p0=[0,0,0,0]
    warnings.filterwarnings('ignore', message='Covariance of the parameters could not be estimated')
    popt , _ =curve_fit(curveObjective3D, (x_line, y_line), z_line, p0)
    a, b, c, d = popt
    z_line = curveObjective3D((x_line, y_line), a, b, c, d)

    #equation = "Curve Fitting: z = "
    #ax.plot(x_line, y_line, z_line, marker='_', color='red', label=equation)


    # 3d projection into 2d plots
    figProj = plt.figure()
    ax1 = figProj.add_subplot(311)
    plt.title("Dynamic altitude variance plot(NON VERTICAL CLIMB) 2D Projection", fontsize=20)
    ax1.set_xlabel('Horizontal Speed (m/s)', fontsize=18)
    ax1.set_ylabel('Time (s)', fontsize=18)
    #ax1.plot(x_line, y_line, marker='_', color='red')
    ax2 = figProj.add_subplot(312)
    ax2.set_xlabel('Horizontal Variance (m)', fontsize=18)
    ax2.set_ylabel('Time (s)', fontsize=18)
    #ax2.plot(z_line, y_line, marker='_', color='red')
    ax3 = figProj.add_subplot(313)
    ax3.set_xlabel('Horizontal Variance (m)', fontsize=18)
    ax3.set_ylabel('Horizontal Speed (m/s)', fontsize=18)
    #ax3.plot(z_line, x_line, marker='_', color='red')


    index = 0
    while index < len(AltitudeVariance):
        if AltitudeVariance[index] > 8 and AltitudeVariance[index] < 14:
            color = 'r'
        elif AltitudeVariance[index] > 15 and AltitudeVariance[index] < 24:
            color = 'green'
        elif AltitudeVariance[index] > 25 and AltitudeVariance[index] < 34:
            color = 'blue'
        else:
            color = 'black'

        position = "[" + str(round(HorizontalSpeed[index],3)) + ", " + str(round(HorizontalVariance[index],3)) + ", " + str(round(DeltaTime[index],3)) + "]"
        dist = np.sqrt(HorizontalVariance[index] * HorizontalVariance[index] + AltitudeVariance[index] * AltitudeVariance[index])
        ax.scatter(HorizontalSpeed[index], DeltaTime[index], HorizontalVariance[index], marker='o', color=color)
        ax.text(HorizontalSpeed[index], DeltaTime[index], HorizontalVariance[index], position, size=7, zorder=1, color= 'k', rotation=45)
        ax1.scatter(HorizontalSpeed[index], DeltaTime[index], marker='o', color=color)
        ax2.scatter(HorizontalVariance[index], DeltaTime[index], marker='o', color=color)
        ax2.text(HorizontalVariance[index], DeltaTime[index], str(round(AltitudeVariance[index],3)) + "  dist:" + str(round(dist,3)), size=7, zorder=1,color='k', rotation=45)
        ax3.scatter(HorizontalVariance[index], HorizontalSpeed[index], marker='o', color=color)

        index = index + 1

    hexa = [89.8,89.2,89.8,89,90.2,89.8]

    # y = 1.02688E-1 * x + 7.51618E-1
    SSCurve = ['1', 1.02688E-1, 7.51618E-1]

    a = SSCurve[1]
    b = SSCurve[2]
    yHexa = []
    for SS in hexa:
        y = a * SS + b
        yHexa.append(y)

    ax2.scatter(hexa, yHexa, color='purple')

    plt.show()
    figProj.set_size_inches(14.0, 14.0)
    figProj.savefig(filepath + "-NVCHorizontalSpeed-Projection-curveFitting.png")
    plt.close(figProj)

    ax.legend(fontsize=18)
    plt.show()
    fig.set_size_inches(14.0, 14.0)
    fig.savefig(filepath + "-NVCHorizontalSpeed-curveFitting.png")
    plt.close(fig)


Plot an altitude variation region and do the curveFitting
@param:
    lowerLimit: Altitude lowerLimit
    upperLimit: Altitude upperLimit
    AltitudeVariance array
    DeltaTime array

def getAltitudChangeRegion(lowerLimit, upperLimit, AltitudeVariance, DeltaTime):
    resultingAltitude = []
    resultingTime = []
    index = 0
    while index < len(AltitudeVariance):
        if AltitudeVariance[index] >= lowerLimit and AltitudeVariance[index] <= upperLimit:
            resultingAltitude.append(AltitudeVariance[index])
            resultingTime.append(DeltaTime[index])
        index = index + 1

    return resultingAltitude, resultingTime


def multipleClimbCurveFitting(filepath, AltitudeVariance, DeltaTime):
    if len(AltitudeVariance) < 5:
        print("Not enough VerticalClimbAltitude's samples to curve fitting")
        return 1

    # Using Scipy curve fit function
    warnings.filterwarnings('ignore', message='Covariance of the parameters could not be estimated')

    #lowerLimit = 6.0
    #upperLimit = 11.0
    #AltitudeVariance, DeltaTime = getAltitudChangeRegion(lowerLimit, upperLimit, AltitudeVariance, DeltaTime)

    if len(AltitudeVariance) < 5:
        print("Not enough VerticalClimbAltitude's samples in this region to interpolate")
        return 1

    figI = plt.figure()
    # seaborn-darkgrid style
    plt.style.use('ggplot')
    axI = figI.add_subplot(111)
    axI.set_xlabel('Ah (m)', fontsize=18)
    axI.set_ylabel('Time (s)', fontsize=18)
    axI.scatter(AltitudeVariance, DeltaTime, marker='o', color='black')

    x_line = np.arange(min(AltitudeVariance), max(AltitudeVariance), 0.25)

    #Curve order one: y = a * x + b
    popt, _ = curve_fit(curveObjectiveOrder1, AltitudeVariance, DeltaTime)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder1(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(round(a, 3)) + " * x + " + str(round(b, 3))
    axI.plot(x_line, curve, marker='_', color='red', label=equation)

    #Curve order two: y = (a * x) + (b * x**2) + c
    popt, _ = curve_fit(curveObjectiveOrder2, AltitudeVariance, DeltaTime)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder2(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(round(a, 3)) + " * x + " + str(round(b, 3)) + " * x^2 + " + str(round(c, 3))
    axI.plot(x_line, curve, marker='_', color='blue', label=equation)

    #Curve order three: y = (a * x) + (b * x**2) + (c * x**3) + d
    popt, _ = curve_fit(curveObjectiveOrder3, AltitudeVariance, DeltaTime)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder3(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(round(a, 3)) + " * x + " + str(round(b, 3)) + " * x^2 + " + str(round(c, 3)) + " * x^3 + " + str(round(d, 3))
    axI.plot(x_line, curve, marker='_', color='purple', label=equation)


    axI.legend(fontsize=18)
    plt.title("Vertical Climb Curve Fitting\nLower Altitude Change variation region", fontsize=20)
    plt.show()
    figI.set_size_inches(14.0, 14.0)
    figI.savefig(filepath + "-curveFitting.png")
    plt.close(figI)


def multipleDescentCurveFitting(filepath, AltitudeVariance, DeltaTime):
    if len(AltitudeVariance) < 5:
        print("Not enough DescentCurveFitting's samples to interpolate")
        return 1

    # Using Scipy curve fit function
    warnings.filterwarnings('ignore', message='Covariance of the parameters could not be estimated')

    #lowerLimit = 12.0
    #upperLimit = 21.0
    #AltitudeVariance, DeltaTime = getAltitudChangeRegion(lowerLimit, upperLimit, AltitudeVariance, DeltaTime)

    if len(AltitudeVariance) < 5:
        print("Not enough VerticalDescentAltitude's samples in this region to interpolate")
        return 1

    figI = plt.figure()
    # seaborn-darkgrid style
    plt.style.use('ggplot')
    axI = figI.add_subplot(111)
    axI.set_xlabel('Ah (m)', fontsize=18)
    axI.set_ylabel('Time (s)', fontsize=18)
    axI.scatter(AltitudeVariance, DeltaTime, marker='o', color='black')

    x_line = np.arange(min(AltitudeVariance), max(AltitudeVariance), 0.25)

    #Curve order one: y = a * x + b
    popt, _ = curve_fit(curveObjectiveOrder1, AltitudeVariance, DeltaTime)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder1(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(round(a, 3)) + " * x + " + str(round(b, 3))
    axI.plot(x_line, curve, marker='_', color='red', label=equation)

    #Curve order two: y = (a * x) + (b * x**2) + c
    popt, _ = curve_fit(curveObjectiveOrder2, AltitudeVariance, DeltaTime)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder2(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(round(a, 3)) + " * x + " + str(round(b, 3)) + " * x^2 + " + str(round(c, 3))
    axI.plot(x_line, curve, marker='_', color='blue', label=equation)

    #Curve order three: y = (a * x) + (b * x**2) + (c * x**3) + d
    popt, _ = curve_fit(curveObjectiveOrder3, AltitudeVariance, DeltaTime)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder3(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(round(a, 3)) + " * x + " + str(round(b, 3)) + " * x^2 + " + str(round(c, 3)) + " * x^3 + " + str(round(d, 3))
    axI.plot(x_line, curve, marker='_', color='purple', label=equation)


    axI.legend(fontsize=18)
    plt.title("Vertical Descent Curve Fitting\nMedium Altitude Change variation region", fontsize=20)
    plt.show()
    figI.set_size_inches(14.0, 14.0)
    figI.savefig(filepath + "-curveFitting.png")
    plt.close(figI)


def multipleTakeOffCurveFitting(filepath, AltitudeVariance, DeltaTime):
    if len(AltitudeVariance) < 5:
        print("Not enough VerticalClimbAltitude's samples to curve fitting")
        return 1

    # Using Scipy curve fit function
    warnings.filterwarnings('ignore', message='Covariance of the parameters could not be estimated')

    #lowerLimit = 6.0
    #upperLimit = 11.0
    #AltitudeVariance, DeltaTime = getAltitudChangeRegion(lowerLimit, upperLimit, AltitudeVariance, DeltaTime)

    if len(AltitudeVariance) < 5:
        print("Not enough VerticalClimbAltitude's samples in this region to interpolate")
        return 1

    figI = plt.figure()
    # seaborn-darkgrid style
    plt.style.use('ggplot')
    axI = figI.add_subplot(111)
    axI.set_xlabel('Ah (m)', fontsize=18)
    axI.set_ylabel('Time (s)', fontsize=18)
    axI.scatter(AltitudeVariance, DeltaTime, marker='o', color='black')

    x_line = np.arange(min(AltitudeVariance), max(AltitudeVariance), 0.25)

    #Curve order one: y = a * x + b
    popt, _ = curve_fit(curveObjectiveOrder1, AltitudeVariance, DeltaTime)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder1(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(round(a, 9)) + " * x + " + str(round(b, 9))
    axI.plot(x_line, curve, marker='_', color='red', label=equation)

    #Curve order two: y = (a * x) + (b * x**2) + c
    popt, _ = curve_fit(curveObjectiveOrder2, AltitudeVariance, DeltaTime)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder2(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(round(a, 9)) + " * x + " + str(round(b, 9)) + " * x^2 + " + str(round(c, 9))
    axI.plot(x_line, curve, marker='_', color='blue', label=equation)

    #Curve order three: y = (a * x) + (b * x**2) + (c * x**3) + d
    popt, _ = curve_fit(curveObjectiveOrder3, AltitudeVariance, DeltaTime)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder3(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(round(a, 9)) + " * x + " + str(round(b, 9)) + " * x^2 + " + str(round(c, 9)) + " * x^3 + " + str(round(d, 9))
    axI.plot(x_line, curve, marker='_', color='purple', label=equation)


    axI.legend(fontsize=18)
    plt.title("Vertical Take-Off Curve Fitting\nLower Altitude Change variation region", fontsize=20)
    plt.show()
    figI.set_size_inches(14.0, 14.0)
    figI.savefig(filepath + "-curveFitting.png")
    plt.close(figI)


def multipleLandingCurveFitting(filepath, AltitudeVariance, DeltaTime):
    if len(AltitudeVariance) < 5:
        print("Not enough VerticalClimbAltitude's samples to curve fitting")
        return 1

    # Using Scipy curve fit function
    warnings.filterwarnings('ignore', message='Covariance of the parameters could not be estimated')

    #lowerLimit = 6.0
    #upperLimit = 11.0
    #AltitudeVariance, DeltaTime = getAltitudChangeRegion(lowerLimit, upperLimit, AltitudeVariance, DeltaTime)

    if len(AltitudeVariance) < 5:
        print("Not enough VerticalClimbAltitude's samples in this region to interpolate")
        return 1

    figI = plt.figure()
    # seaborn-darkgrid style
    plt.style.use('ggplot')
    axI = figI.add_subplot(111)
    axI.set_xlabel('Ah (m)', fontsize=18)
    axI.set_ylabel('Time (s)', fontsize=18)
    axI.scatter(AltitudeVariance, DeltaTime, marker='o', color='black')

    x_line = np.arange(min(AltitudeVariance), max(AltitudeVariance), 0.25)

    #Curve order one: y = a * x + b
    popt, _ = curve_fit(curveObjectiveOrder1, AltitudeVariance, DeltaTime)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder1(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(round(a, 9)) + " * x + " + str(round(b, 9))
    axI.plot(x_line, curve, marker='_', color='red', label=equation)

    #Curve order two: y = (a * x) + (b * x**2) + c
    popt, _ = curve_fit(curveObjectiveOrder2, AltitudeVariance, DeltaTime)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder2(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(round(a, 9)) + " * x + " + str(round(b, 9)) + " * x^2 + " + str(round(c, 9))
    axI.plot(x_line, curve, marker='_', color='blue', label=equation)

    #Curve order three: y = (a * x) + (b * x**2) + (c * x**3) + d
    popt, _ = curve_fit(curveObjectiveOrder3, AltitudeVariance, DeltaTime)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder3(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(round(a, 9)) + " * x + " + str(round(b, 9)) + " * x^2 + " + str(round(c, 9)) + " * x^3 + " + str(round(d, 9))
    axI.plot(x_line, curve, marker='_', color='purple', label=equation)


    axI.legend(fontsize=18)
    plt.title("Vertical Landing Curve Fitting\nLower Altitude Change variation region", fontsize=20)
    plt.show()
    figI.set_size_inches(14.0, 14.0)
    figI.savefig(filepath + "-curveFitting.png")
    plt.close(figI)

def getCurveEvolution(filepath, eventType):
    figI = plt.figure()
    # seaborn-darkgrid style
    plt.style.use('ggplot')
    axI = figI.add_subplot(211)
    axI.set_xlabel('Speed (m/s)', fontsize=18)
    axI.set_ylabel('Curve Slope m', fontsize=18)
    plt.title("Curve evolution with velocity variation\n y = m·x + a", fontsize=20)
    axII = figI.add_subplot(212)
    axII.set_xlabel('Speed (m/s)', fontsize=18)
    axII.set_ylabel('Offset a', fontsize=18)
    plt.title("Curve evolution with velocity variation\n y = m·x + a", fontsize=20)

    if eventType == "Landing":
        axI.scatter(1.2, landingCurvev08[1], marker='o', color='black')
        axI.scatter(1.5, landingCurve[1], marker='o', color='black')
        axI.scatter(3.0, landingCurvev2[1], marker='o', color='black')
        axI.scatter(5.0, landingCurvevmDr[1], marker='o', color='black')
        axI.scatter(8.0, landingCurvevmDrB[1], marker='o', color='black')

        axII.scatter(1.2, landingCurvev08[2], marker='o', color='black')
        axII.scatter(1.5, landingCurve[2], marker='o', color='black')
        axII.scatter(3.0, landingCurvev2[2], marker='o', color='black')
        axII.scatter(5.0, landingCurvevmDr[2], marker='o', color='black')
        axII.scatter(8.0, landingCurvevmDrB[2], marker='o', color='black')

        x_line = np.arange(1.2, 8.0, 0.05)
        xvariance = [1.2,1.5,3.0,5.0,8.0,1.2,1.5,3.0,5.0,8.0]
        yvarianceI = [landingCurvev08[1], landingCurve[1], landingCurvev2[1], landingCurvevmDr[1],landingCurvevmDrB[1], landingCurvev08[1], landingCurve[1], landingCurvev2[1], landingCurvevmDr[1], landingCurvevmDrB[1]]
        yvarianceII = [landingCurvev08[2], landingCurve[2], landingCurvev2[2], landingCurvevmDr[2],landingCurvevmDrB[2], landingCurvev08[2], landingCurve[2], landingCurvev2[2], landingCurvevmDr[2], landingCurvevmDrB[2]]

    elif eventType == "TakeOff":
        axI.scatter(2.0, takeOffCurvev08[1], marker='o', color='black')
        axI.scatter(2.5, takeOffCurve[1], marker='o', color='black')
        axI.scatter(5.0, takeOffCurvevmDrB[1], marker='o', color='black')
        axI.scatter(8.0, takeOffCurvevmDr[1], marker='o', color='black')

        axII.scatter(2.0, takeOffCurvev08[2], marker='o', color='black')
        axII.scatter(2.5, takeOffCurve[2], marker='o', color='black')
        axII.scatter(5.0, takeOffCurvevmDrB[2], marker='o', color='black')
        axII.scatter(8.0, takeOffCurvevmDr[2], marker='o', color='black')

        x_line = np.arange(2.0, 8.0, 0.05)
        xvariance = [2.0,2.5,5.0,8.0, 2.0,2.5,5.0,8.0]
        yvarianceI = [takeOffCurvev08[1], takeOffCurve[1],takeOffCurvevmDrB[1], takeOffCurvevmDr[1], takeOffCurvev08[1], takeOffCurve[1],takeOffCurvevmDrB[1], takeOffCurvevmDr[1]]
        yvarianceII = [takeOffCurvev08[2], takeOffCurve[2],takeOffCurvevmDrB[2], takeOffCurvevmDr[2], takeOffCurvev08[2], takeOffCurve[2],takeOffCurvevmDrB[2], takeOffCurvevmDr[2]]

    elif eventType == "VC":
        axI.scatter(2.0, climbCurvev08[1], marker='o', color='black')
        axI.scatter(2.5, climbCurve[1], marker='o', color='black')
        axI.scatter(3.75, climbCurvev15[1], marker='o', color='black')
        axI.scatter(5.0, climbCurvevmDrB[1], marker='o', color='black')

        axII.scatter(2.0, climbCurvev08[2], marker='o', color='black')
        axII.scatter(2.5, climbCurve[2], marker='o', color='black')
        axII.scatter(3.75, climbCurvev15[2], marker='o', color='black')
        axII.scatter(5.0, climbCurvevmDrB[2], marker='o', color='black')

        x_line = np.arange(2.0, 5.0, 0.05)
        xvariance = [2.0,2.5,3.75,5.0, 2.0,2.5,3.75, 5.0]
        yvarianceI = [climbCurvev08[1], climbCurve[1], climbCurvev15[1],climbCurvevmDrB[1], climbCurvev08[1], climbCurve[1], climbCurvev15[1], climbCurvevmDrB[1]]
        yvarianceII = [climbCurvev08[2], climbCurve[2], climbCurvev15[2],climbCurvevmDrB[2], climbCurvev08[2], climbCurve[2], climbCurvev15[2], climbCurvevmDrB[2]]

    elif eventType == "VD":
        axI.scatter(1.2, descentCurvev08[1], marker='o', color='black')
        axI.scatter(1.5, descentCurve[1], marker='o', color='black')
        axI.scatter(2.25, descentCurvev15[1], marker='o', color='black')
        axI.scatter(3.0, descentCurvev20[1], marker='o', color='black')
        axI.scatter(5.0, descentCurvevmDrB[1], marker='o', color='black')

        axII.scatter(1.2, descentCurvev08[2], marker='o', color='black')
        axII.scatter(1.5, descentCurve[2], marker='o', color='black')
        axII.scatter(2.25, descentCurvev15[2], marker='o', color='black')
        axII.scatter(3.0, descentCurvev20[2], marker='o', color='black')
        axII.scatter(5.0, descentCurvevmDrB[2], marker='o', color='black')

        x_line = np.arange(1.2, 5.0, 0.05)
        xvariance = [1.2, 1.5, 2.25, 3.0,5.0, 1.2, 1.5, 2.25, 3.0,5.0]
        yvarianceI = [descentCurvev08[1], descentCurve[1], descentCurvev15[1], descentCurvev20[1],descentCurvevmDrB[1], descentCurvev08[1], descentCurve[1], descentCurvev15[1], descentCurvev20[1],descentCurvevmDrB[1]]
        yvarianceII = [descentCurvev08[2], descentCurve[2], descentCurvev15[2], descentCurvev20[2], descentCurvevmDrB[2],descentCurvev08[2], descentCurve[2], descentCurvev15[2], descentCurvev20[2],descentCurvevmDrB[2]]

    elif eventType == "NVC":
        axI.scatter(2.0, NVClimbCurvev08[1], marker='o', color='black')
        axI.scatter(2.5, NVClimbCurve[1], marker='o', color='black')
        axI.scatter(3.75, NVClimbCurvev15[1], marker='o', color='black')
        #axI.scatter(5.0, NVClimbCurvevmDrB[1], marker='o', color='black')

        axII.scatter(2.0, NVClimbCurvev08[2], marker='o', color='black')
        axII.scatter(2.5, NVClimbCurve[2], marker='o', color='black')
        axII.scatter(3.75, NVClimbCurvev15[2], marker='o', color='black')
        #axII.scatter(5.0, NVClimbCurvevmDrB[2], marker='o', color='black')

        x_line = np.arange(1.0, 5.0, 0.05)
        xvariance = [2.0,2.5,3.75,2.0,2.5,3.75]
        yvarianceI = [NVClimbCurvev08[1], NVClimbCurve[1],NVClimbCurvev15[1],NVClimbCurvev08[1], NVClimbCurve[1],NVClimbCurvev15[1]]
        yvarianceII = [NVClimbCurvev08[2], NVClimbCurve[2],NVClimbCurvev15[2],NVClimbCurvev08[2], NVClimbCurve[2],NVClimbCurvev15[2]]

    elif eventType == "NVD":
        axI.scatter(1.2, NVDescentCurve08[1], marker='o', color='black')
        axI.scatter(1.5, NVDescentCurve[1], marker='o', color='black')
        axI.scatter(2.25, NVDescentCurve15[1], marker='o', color='black')
        axI.scatter(5.0, NVDescentCurvevmDrB[1], marker='o', color='black')

        axII.scatter(1.2, NVDescentCurve08[2], marker='o', color='black')
        axII.scatter(1.5, NVDescentCurve[2], marker='o', color='black')
        axII.scatter(2.25, NVDescentCurve15[2], marker='o', color='black')
        axII.scatter(5.0, NVDescentCurvevmDrB[2], marker='o', color='black')

        x_line = np.arange(1.0, 5.0, 0.05)
        xvariance = [1.2,1.5,2.25,5.0,1.2,1.5,2.25,5.0]
        yvarianceI = [NVDescentCurve08[1], NVDescentCurve[1],NVDescentCurve15[1],NVDescentCurvevmDrB[1],NVDescentCurve08[1], NVDescentCurve[1],NVDescentCurve15[1],NVDescentCurvevmDrB[1]]
        yvarianceII = [NVDescentCurve08[2], NVDescentCurve[2],NVDescentCurve15[2],NVDescentCurvevmDrB[2],NVDescentCurve08[2], NVDescentCurve[2],NVDescentCurve15[2],NVDescentCurvevmDrB[2]]
    else:
        return -1

    # Curve order two: y = (a * x) + (b * x**2) + c
    popt, _ = curve_fit(curveObjectiveOrder2, xvariance, yvarianceI)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder2(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(format(a,'.5E')) + " * x + " + str(format(b,'.5E')) + " * x^2 + " + str(format(c,'.5E'))
    axI.plot(x_line, curve, marker='_', color='blue', label=equation)

    #-----------------------------------------------------------------------------------------------------------------------

    # Curve order two: y = (a * x) + (b * x**2) + c
    popt, _ = curve_fit(curveObjectiveOrder2, xvariance, yvarianceII)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder2(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(format(a,'.5E')) + " * x + " + str(format(b,'.5E')) + " * x^2 + " + str(format(c,'.5E'))
    axII.plot(x_line, curve, marker='_', color='orange', label=equation)

    axI.legend(fontsize=18)
    axII.legend(fontsize=18)
    plt.show()
    figI.set_size_inches(14.0, 14.0)
    figI.savefig(filepath + eventType + "CurveEvolution.png")
    plt.close(figI)


# curves objectives to fit
def curveObjectiveOrder1(x, a, b, c, d, e): return a * x + b  # straight line


def curveObjectiveOrder2(x, a, b, c, d, e): return (a * x) + (b * x ** 2) + c  # polynomial regression order 2


def curveObjectiveOrder3(x, a, b, c, d, e): return (a * x) + (b * x ** 2) + (
            c * x ** 3) + d  # polynomial regression order 3


def curveObjectiveOrder4(x, a, b, c, d, e): return (a * x) + (b * x ** 2) + (c * x ** 3) + (
            c * x ** 4) + e  # polynomial regression order 4


def curveObjectiveOrder4(x, a, b, c, d, e, f): return (a * x) + (b * x ** 2) + (c * x ** 3) + (d * x ** 4) + (
            e * x ** 5) + f  # polynomial regression order 5


def curveObjectiveGaussian(x, a, b, c, d, e): return a * np.exp(-np.power(x - b, 2) / (2 * np.power(c, 2)))  # Gaussian


def curveObjectiveSine(x, a, b, c, d, e): return a * np.sin(b - x) + c * x ** 2 + d  # Sine curve


def curveObjective3D(X, a, b, c, d):
    x, y = X
    return a * x + b * y + c

def getDistance(sampleA, sampleB):
    distance = geo.Geodesic.WGS84.Inverse(sampleA.latitude, sampleA.longitude, sampleB.latitude, sampleB.longitude)
    return distance['s12']

def getAltitudeChangeCurve(eventType, v):

    if eventType == "Landing":
        mCurve = mLD
        aCurve = aLD
    elif eventType == "TakeOff":
        mCurve = mTO
        aCurve = aTO
    elif eventType == "VC":
        mCurve = mVC
        aCurve = aVC
    elif eventType == "VD":
        mCurve = mVD
        aCurve = aVD
    elif eventType == "NVC":
        mCurve = mNVC
        aCurve = aNVC
    elif eventType == "NVD":
        mCurve = mNVD
        aCurve = aNVD
    else:
        return -1
    m = simmulateCurve(mCurve, 0, v)
    a = simmulateCurve(aCurve, 0, v)
    curve = ['1', m, a]
    return curve, 0

def simmulateCurve(curve, Offset, data):
    deltaTime = 0

    if curve[0] == '1':
        a = curve[1]
        b = curve[2] + Offset
        deltaTime = a * data + b
    elif curve[0] == '2':
        a = curve[1]
        b = curve[2]
        c = curve[3] + Offset
        deltaTime = a * data + b * data * data + c
    elif curve[0] == '3':
        a = curve[1]
        b = curve[2]
        c = curve[3]
        d = curve[4] + Offset
        deltaTime = a * data + b * data * data + c * data * data * data + d

    return deltaTime
'''