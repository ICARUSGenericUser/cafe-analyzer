import matplotlib.pyplot as plt
import numpy as np
import warnings
from Mocca_Analyzer.Performance_Analyzer import Event_Type as et

from scipy.optimize import curve_fit
from scipy import interpolate
import matplotlib
matplotlib.rc('xtick', labelsize=20)
matplotlib.rc('ytick', labelsize=20)

def addEventPlot(ax, length, speed,segmentLength, initialEvent, finalEvent, nOfSample, flightEventStore,SegmentSpeed, nOfStraightSegments):

    if initialEvent.type == et.Event_Type.EndCourseChange:
        if segmentLength > 15.0:
            color = 'grey'
        else:
            color = 'red'
    elif initialEvent.type == et.Event_Type.EndAltChange:
        if segmentLength > 15.0:
            color = 'blue'
        else:
            color = 'orange'
    else:
        color = 'grey'
    ax.scatter(length, speed, color=color)
    sampleI = flightEventStore.telemetryList[initialEvent.idx]
    sampleF = flightEventStore.telemetryList[finalEvent.idx]
    #text = "Length: " + str(segmentLength) + "; Samples: " + str(nOfSample) + "; vIni: " + str(round(sampleI.hspeed,3)) + "; vFin: " + str(round(sampleF.hspeed,3))
    text = "Length: " + str(segmentLength) + "; vSegm: " + str(round(SegmentSpeed,3)) + "; #" + str(nOfStraightSegments)
    ax.text(length[0], speed[0], text, size=7, zorder=1, color=color, rotation=45)
    textFin = "vSegm: " + str(round(SegmentSpeed,3)) + "; #" + str(nOfStraightSegments)
    ax.text(length[nOfSample-1], speed[nOfSample-1], textFin, size=7, zorder=1, color=color, rotation=45)



def addEventPlot2DeltaLenghtDeltaTime(ax, deltaLength, deltaTime, alt):
    ax.scatter(deltaLength, deltaTime, color='b')


def addGlobalPlot(ax, length, speed):
    ax.plot(length, speed)


def addSSPInterpolation(ax, length, speed):
    # Time
    # define a sequence of inputs between the smallest and largest known inputs
    x_line = np.arange(0, 1, 0.01)

    # Using Scipy interpolate function
    f = interpolate.interp1d(length, speed, kind= 'quadratic')
    y_line = f(x_line)
    ax.plot(x_line, y_line, color='blue')


def getDynamicStraightSegmentPlot():
    fig = plt.figure()
    plt.style.use('ggplot')
    ax = fig.add_subplot(111)
    ax.set_xlabel('deltaTime', fontsize=18)
    ax.set_ylabel('deltaLength', fontsize=18)
    plt.title("-----------", fontsize=20)
    plt.show()
    return ax, fig

def closeDynamicPlot(fig, filepath):
    print("Generating figure: " + filepath)
    fig.set_size_inches(14.0, 14.0)
    fig.savefig(filepath)
    plt.close(fig)

def getStraightSegmentInterpolationPlot(filepath, length, speed):
    fig = plt.figure()
    plt.style.use('ggplot')
    ax = fig.add_subplot(111)
    ax.set_xlabel('Length of the Segment remainig', fontsize=18)
    ax.set_ylabel('Velocity(m/s)', fontsize=18)
    plt.title("Dynamic Straight Segment Interpolation", fontsize=20)

    # Time
    # define a sequence of inputs between the smallest and largest known inputs
    x_line = np.arange(min(length), max(length), 1)

    # Using Scipy interpolate function
    # f = interpolate.interp1d(xTimeValues, yVSpeedValues, kind= 'cubic')
    # y_line = f(x_line)

    # Using Scipy curve fit function
    warnings.filterwarnings('ignore', message='Covariance of the parameters could not be estimated')
    popt, _ = curve_fit(curve_Objective_Descent, length, speed)
    # summarize the parameter values
    a, b, c, d, e = popt

    # calculate the output for the range
    y_line = curve_Objective_Descent(x_line, a, b, c, d, e)

    figI = plt.figure()
    axI = figI.add_subplot(111)
    axI.scatter(length, speed, marker='o', color='gray')
    axI.plot(x_line, y_line, marker='_', color='red')
    plt.show()
    figI.set_size_inches(14.0, 14.0)
    figI.savefig(filepath + "-Interpolation.png")
    plt.close(figI)




def curve_Objective_Descent(x, a, b, c, d, e):
    #return a * x + b #straight lin
    return (a * x) + (b * x**2) + c #polynomial regression order 2
    #return (a * x) + (b * x**2) + (c * x**3) + d #polynomial regression order 3
    #return (a * x) + (b * x**2) + (c * x**3) + (c * x**4) + e #polynomial regression order 4
    #return (a * x) + (b * x**2) + (c * x**3) + (d * x**4) + (e * x**5) + f
    #return a * np.exp(-np.power(x - b, 2) / (2 * np.power(c, 2)))  # Gaussian
    #return a * np.sin(b - x) + c * x ** 2 + d # Sine curve