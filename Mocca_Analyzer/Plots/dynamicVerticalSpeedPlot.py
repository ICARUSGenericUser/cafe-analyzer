import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import math as math
import numpy as np
from DEG_Encounter import SegmentEnum as se
from scipy.optimize import curve_fit
from scipy import interpolate
import matplotlib
matplotlib.rc('xtick', labelsize=20)
matplotlib.rc('ytick', labelsize=20)

def getDynamicVerticalSpeedPlot(type):
    fig = plt.figure()
    axT = fig.add_subplot(211)
    axT.set_xlabel('T (ms)', fontsize=18)
    axT.set_ylabel('VSpeed (m/s)', fontsize=18)
    axA = fig.add_subplot(212)
    axA.set_xlabel('Altitude (m)', fontsize=18)
    axA.set_ylabel('VSpeed (m/s)', fontsize=18)
    plt.title(type + " Dynamic Vertical Speed vs. Altitude vs. Time ", fontsize=20)
    plt.show()
    return axT, axA, fig

def addSamplePlot(axT, time, axA, altitude, vSpeed):
    axT.plot(time, vSpeed, 'gray', marker='.')
    axA.plot(altitude, vSpeed, 'gray', marker='.')

def addAverageSpeedPlot(ax, vSpeed):
    ax.axhline(vSpeed, xmin=0.1, xmax=0.9, color='y', linewidth=1)

def addEventPlot(ax, time, value, event):
    ax.scatter(time, value, color='b')
    ax.text(time, value, event, size=9, zorder=1, color='k', rotation=45)


def closeDynamicPlot(fig, filepath):
    print("Generating figure: " + filepath)
    fig.set_size_inches(14.0, 14.0)
    fig.savefig(filepath)
    plt.close(fig)

def interpolateValues(xTimeValues, yVSpeedValues, path):
    #Time
    #define a sequence of inputs between the smallest and largest known inputs
    x_line = np.arange(min(xTimeValues), max(xTimeValues), 500)

    #Using Scipy interpolate function
    f = interpolate.interp1d(xTimeValues, yVSpeedValues, kind= 'cubic')
    y_line = f(x_line)

    #Using Scipy curve fit function
    #popt, _ = curve_fit(curve_Objective, xTimeValues, yVSpeedValues)
    # summarize the parameter values
    #a, b, c, d, e, f = popt

    # calculate the output for the range
    #y_line = curve_Objective(x_line, a, b, c, d, e, f)

    figI = plt.figure()
    axI = figI.add_subplot(111)
    axI.plot(xTimeValues,yVSpeedValues,'gray', marker='.')
    axI.plot(x_line, y_line, '--', color='red')
    plt.show()
    figI.set_size_inches(14.0, 14.0)
    figI.savefig(path + "-Interpolation.png")
    plt.close(figI)

def curve_Objective(x, a, b, c, d, e, f):
    #return a * x + b #straight lin
    return (a * x) + (b * x**2) + c #polynomial regression order 2
    #return (a * x) + (b * x**2) + (c * x**3) + d #polynomial regression order 3
    #return (a * x) + (b * x**2) + (c * x**3) + (c * x**4) + e #polynomial regression order 4
    #return (a * x) + (b * x**2) + (c * x**3) + (d * x**4) + (e * x**5) + f
    #return a * np.exp(-np.power(x - b, 2) / (2 * np.power(c, 2)))  # Gaussian
    #return a * np.sin(b - x) + c * x ** 2 + d # Sine curve
