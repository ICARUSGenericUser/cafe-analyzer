import matplotlib.pyplot as plt
import numpy as np
from geographiclib import geodesic as geo
from Util import angles as agl
import matplotlib
matplotlib.rc('xtick', labelsize=20)
matplotlib.rc('ytick', labelsize=20)

def addEventPlot(ax, x_variable, y_variable,text):
    ax.plot(x_variable, y_variable, color='grey')
    ax.text(x_variable[0], y_variable[0], str(text), size=9, zorder=1, color='g', rotation=45)


def addArcPlot(ax, InitialSample, finalSample, vMean, prevSegment, followingEvent, c):
    distance = getDistance(InitialSample,finalSample)
    angleTurned = agl.normalizeToAcute((finalSample.course - InitialSample.course) * np.pi / 180)


    arcRadius = distance / (2 * np.sin(angleTurned / 2))
    arcLongitude = angleTurned * arcRadius
    #arcLongitude = abs(arcRadius / (angleTurned * 180 / np.pi))
    ax.scatter(arcLongitude, (finalSample.timestamp - InitialSample.timestamp)/1000, color=c, marker='.')

    if prevSegment > 60 and followingEvent > 60:
        c = 'blue'
    text = "angle: " + str(round(angleTurned * 180 / np.pi,3)) + "  Rad: " + str(round(arcRadius,3)) + " w: " + str(vMean/arcRadius)
    #text = "PrevSegm : " + str(round(prevSegment,3)) + " PostSeg: " + str(round(followingEvent,3)) + "  angle: " + str(round(angleTurned * 180 / np.pi,3))
    ax.text(arcLongitude, (finalSample.timestamp - InitialSample.timestamp)/1000, text, size=9, zorder=1, color=c, rotation=45)


def getDynamicTurnAnglePlot():
    fig = plt.figure()
    plt.style.use('ggplot')
    ax = fig.add_subplot(111)
    ax.set_xlabel('Delta Time', fontsize=18)
    ax.set_ylabel('arc Longitude', fontsize=18)
    plt.title("Delta arcLongitude vs. Delta Time", fontsize=20)
    plt.show()
    return ax, fig

def getDynamicTurnDistPlot():
    fig = plt.figure()
    plt.style.use('ggplot')
    ax = fig.add_subplot(111)
    ax.set_xlabel('Distance to turn normalized', fontsize=18)
    ax.set_ylabel('Speed(m/s)', fontsize=18)
    plt.title("Distance to turn vs. Speed", fontsize=20)
    plt.show()
    return ax, fig

def getIndividualTurnPlot(speedArray, angleArray, xArray, yArray, Angle2Turn,  path):
    #For each turn plots Speed vs. Course && Y vs. X
    fig = plt.figure()
    plt.style.use('ggplot')
    # Course  vs. Speed Plot
    axCS = fig.add_subplot(211)
    axCS.set_xlabel('Angle to Turn normalized(degrees)', fontsize=18)
    axCS.set_ylabel('Speed (m/s)', fontsize=18)
    axCS.plot(angleArray, speedArray, color='grey')
    # X vs. Y Plot
    axXY = fig.add_subplot(212)
    axXY.set_xlabel('X (m)', fontsize=18)
    axXY.set_ylabel('Y (m/s)', fontsize=18)
    plt.title("Turn", fontsize=20)
    axXY.plot(xArray, yArray, color='grey')
    textXY = "Angle2Turn: " + str(Angle2Turn)
    #axXY.text(xArray[0], yArray[0], textXY, size=9, zorder=1, color='g', rotation=45)
    plt.show()
    closeDynamicPlot(fig, path)



def closeDynamicPlot(fig, filepath):
    print("Generating figure: " + filepath)
    fig.set_size_inches(14.0, 14.0)
    fig.savefig(filepath)
    plt.close(fig)

def getDistance(sampleA, sampleB):
    distance = geo.Geodesic.WGS84.Inverse(sampleA.latitude, sampleA.longitude, sampleB.latitude, sampleB.longitude)
    return distance['s12']
