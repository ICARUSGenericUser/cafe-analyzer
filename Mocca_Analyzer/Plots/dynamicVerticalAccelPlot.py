import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import math as math
import numpy as np
from DEG_Encounter import SegmentEnum as se
import matplotlib
matplotlib.rc('xtick', labelsize=20)
matplotlib.rc('ytick', labelsize=20)

def getDynamicVerticalAccelPlot(type):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('T (ms)', fontsize=18)
    ax.set_ylabel('VAccel (g)', fontsize=18)
    plt.title(type + " Dynamic Vertical Acceleration plot", fontsize=20)
    plt.show()
    return ax, fig


def addSamplePlot(ax, time, vaccel):
    ax.plot(time, vaccel, 'gray', marker='.')

def addAverageAccelPlot(ax, vaccel):
    ax.axhline(vaccel, xmin=0.1, xmax=0.9, color='y', linewidth=1)

def addEventPlot(ax, time, value, event):
    ax.scatter(time, value, color='b')
    ax.text(time, value, event, size=9, zorder=1, color='k', rotation=45)


def closeDynamicPlot(fig, filepath):
    print("Generating figure: " + filepath)
    fig.set_size_inches(14.0, 14.0)
    fig.savefig(filepath)
    plt.close(fig)

