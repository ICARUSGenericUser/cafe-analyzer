from Mocca_Analyzer.Performance_Analyzer import Event_Type as et
import matplotlib.pyplot as plt
import numpy as np
import warnings
from scipy.optimize import curve_fit
import matplotlib
matplotlib.rc('xtick', labelsize=20)
matplotlib.rc('ytick', labelsize=20)

# y = 1.02688E-1 * x + 7.51618E-1
SSCurve = ['1', 1.02688E-1, 7.51618E-1]
SSOffset = 0

#v50:  y = 2.03538E-01 * x + -1.31059E-01
SSCurvev50 = ['1', 2.03538E-01, -1.31059E-01]
SSOffsetv50 = 0

#v25:  y = 4.01545E-01 * x + 1.24214E-02
SSCurvev25 = ['1', 4.01545E-01, 1.24214E-02]
SSOffsetv25 = 0

'''
    case 1: EndCourseChange + SS + StartCourseChange
    case 2: EndAltChange + SS + StartAltChange
    case 3: EndAltChange + SS + StartCourseChange
    case 4: EndCourseChange + SS + StartAltChange
    case 5: Otherwise(Hover)
    '''

#case 1: y = 9.70229E-02 * x + 1.42715
SSCurve1 = ['1', 9.70229E-02, 1.42715]
SSOffset1 = 0
SSCurve1v08 = ['1', 1.33036E-01, 3.14264E-01]
SSCurve1v025 = ['1', 4.28988E-01,6.08783E-02]

#case 2: y =
SSCurve2 = ['1', 1.00672E-01,4.48263]
SSOffset2 = 0
SSCurve2v50 = ['1', 1.92598E-01,4.4707]
SSCurve2v80 = ['1', 1.19493E-01,4.40892]

#case 3: y = 9.43429E-02 * x + 1.70687
#y = 1.14072E-01 * x + 2.72350E-01
SSCurve3 = ['1',1.14072E-01, 2.72350E-01]
SSOffset3 = 0
SSCurve3v08 = ['1', 1.288549E-01, 2.56176]
SSCurve3v50 = ['1', 2.15659E-01, 3.32426]

#case 4: y =
SSCurve4 = ['1', 9.67919E-02,2.45279]
SSOffset4 = 0

SSCurve4vmDrB = ['1', 1.07436E-01, 2.06325]
SSCurve4v08 = ['1', 1.28336E-01, 1.71926]
SSCurve4v50 = ['1', 2.27211E-01, -7.92456E-02]

#case 5: y = y case3
SSCurve5 = SSCurve3
SSOffset5 = 0

#Curves from Speed y = m·x + a
#SS Cases
mSS1 = ['2', -1.03934E-01,4.77372E-03, 6.58986E-01]
aSS1 = ['2', -6.68452E-01,6.80497E-02,1.3067]

mSS2 = ['2', -6.32587E-02, 2.99157E-03, 4.34102E-01]
aSS2 = ['2', -1.66959E-01,1.14897E-02,5.03325]

mSS3 = ['2', -8.49472E-02,4.30865E-03, 5.32679E-01]
aSS3 = ['2', 2.06123, -1.78108E-01, -2.52921]

mSS4 = ['2', -5.91895E-02, 2.17874E-03, 4.67213E-01]
aSS4 = ['2', 1.15117, -4.29025E-02, -4.75828]

mSS5 = mSS3
aSS5 = aSS3

def interpolateSS(filepath,lenghtVariance, DeltaTime, SSTag, SSPrePostEvents):

    getCurveEvolution(filepath, "Case1")
    getCurveEvolution(filepath, "Case2")
    getCurveEvolution(filepath, "Case3")
    getCurveEvolution(filepath, "Case4")

    c1 = getAltitudeChangeCurve("Case1", 10.0)
    c2 = getAltitudeChangeCurve("Case2", 10.0)
    c3 = getAltitudeChangeCurve("Case3", 10.0)
    c4 = getAltitudeChangeCurve("Case4", 10.0)
    c5 = getAltitudeChangeCurve("Case5", 10.0)

    multipleSSCurveFitting(filepath + "MultipleSS_TimeAltitudeCurveFitting-", lenghtVariance, DeltaTime, SSTag,
                           SSPrePostEvents)
    getSSLengthVariationPlot(filepath + "SS_TimeSSLenghtCurveFitting-", lenghtVariance, DeltaTime)

def multipleSSCurveFitting(filepath, lenghtVariance, DeltaTime, SSTag, SSPrePostEvents):
    '''if len(lenghtVariance) < 5:
        print("Not enough Straight Segments events to curve fitting")
        return 1'''

    # Using Scipy curve fit function
    warnings.filterwarnings('ignore', message='Covariance of the parameters could not be estimated')

    '''
    case 1: EndCourseChange + SS + StartCourseChange
    case 2: EndAltChange + SS + StartAltChange
    case 3: EndAltChange + SS + StartCourseChange
    case 4: EndCourseChange + SS + StartAltChange
    case 5: Otherwise(Hover)
    '''

    fig1 = plt.figure()
    # seaborn-darkgrid style
    plt.style.use('ggplot')
    ax1 = fig1.add_subplot(111)
    ax1.set_xlabel('AL (m)', fontsize=18)
    ax1.set_ylabel('Time (s)', fontsize=18)
    plt.title("Straight Segments Length variation[AL] vs Time variation[At]\n Case 1: EndCourseChange + SS + StartCourseChange", fontsize=20)

    fig2 = plt.figure()
    # seaborn-darkgrid style
    plt.style.use('ggplot')
    ax2 = fig2.add_subplot(111)
    ax2.set_xlabel('AL (m)', fontsize=18)
    ax2.set_ylabel('Time (s)', fontsize=18)
    plt.title("Straight Segments Length variation[AL] vs Time variation[At]\n Case 2: EndAltChange + SS + StartAltChange", fontsize=20)


    fig3 = plt.figure()
    # seaborn-darkgrid style
    plt.style.use('ggplot')
    ax3 = fig3.add_subplot(111)
    ax3.set_xlabel('AL (m)', fontsize=18)
    ax3.set_ylabel('Time (s)', fontsize=18)
    plt.title("Straight Segments Length variation[AL] vs Time variation[At]\n Case 3: EndAltChange + SS + StartCourseChange", fontsize=20)


    fig4 = plt.figure()
    # seaborn-darkgrid style
    plt.style.use('ggplot')
    ax4 = fig4.add_subplot(111)
    ax4.set_xlabel('AL (m)', fontsize=18)
    ax4.set_ylabel('Time (s)', fontsize=18)
    plt.title("Straight Segments Length variation[AL] vs Time variation[At]\n Case 4: EndCourseChange + SS + StartAltChange", fontsize=20)


    fig5 = plt.figure()
    # seaborn-darkgrid style
    plt.style.use('ggplot')
    ax5 = fig5.add_subplot(111)
    ax5.set_xlabel('AL (m)', fontsize=18)
    ax5.set_ylabel('Time (s)', fontsize=18)
    plt.title("Straight Segments Length variation[AL] vs Time variation[At]\n Case 5: Otherwise(Hover)", fontsize=20)


    previousEvents = SSPrePostEvents[0]
    postEvents = SSPrePostEvents[1]

    redLenghtVariance = []
    redDeltaTime = []
    greenLenghtVariance = []
    greenDeltaTime = []
    blueLenghtVariance = []
    blueDeltaTime = []
    blackLenghtVariance = []
    blackDeltaTime = []
    yellowLenghtVariance = []
    yellowDeltaTime = []
    index = 0
    while index < len(lenghtVariance):

        if previousEvents[index].type == et.Event_Type.EndCourseChange and postEvents[index].type == et.Event_Type.StartCourseChange:
            color = 'black'
            redLenghtVariance.append(lenghtVariance[index])
            redDeltaTime.append(DeltaTime[index])
            ax1.scatter(lenghtVariance[index], DeltaTime[index], marker='o', color=color)
            #ax1.text(lenghtVariance[index], DeltaTime[index], str(SSTag[index]), size=9, zorder=1, color='k',
            #         rotation=45)
        elif previousEvents[index].type == et.Event_Type.EndAltChange and (postEvents[index].type == et.Event_Type.StartClimb or postEvents[index].type == et.Event_Type.StartDescent):
            color = 'black'
            greenLenghtVariance.append(lenghtVariance[index])
            greenDeltaTime.append(DeltaTime[index])
            ax2.scatter(lenghtVariance[index], DeltaTime[index], marker='o', color=color)
            #ax2.text(lenghtVariance[index], DeltaTime[index], str(SSTag[index]), size=9, zorder=1, color='k',
            #         rotation=45)
        elif previousEvents[index].type == et.Event_Type.EndAltChange and postEvents[index].type == et.Event_Type.StartCourseChange:
            color = 'black'
            blueLenghtVariance.append(lenghtVariance[index])
            blueDeltaTime.append(DeltaTime[index])
            ax3.scatter(lenghtVariance[index], DeltaTime[index], marker='o', color=color)
            #ax3.text(lenghtVariance[index], DeltaTime[index], str(SSTag[index]), size=9, zorder=1, color='k',
           #          rotation=45)
        elif previousEvents[index].type == et.Event_Type.EndCourseChange and (postEvents[index].type == et.Event_Type.StartClimb or postEvents[index].type == et.Event_Type.StartDescent or postEvents[index].type == et.Event_Type.StartLanding):
            color = 'black'
            blackLenghtVariance.append(lenghtVariance[index])
            blackDeltaTime.append(DeltaTime[index])
            ax4.scatter(lenghtVariance[index], DeltaTime[index], marker='o', color=color)
            #ax4.text(lenghtVariance[index], DeltaTime[index], str(SSTag[index]), size=9, zorder=1, color='k',
            #         rotation=45)
        else:
            color = 'black'
            yellowLenghtVariance.append(lenghtVariance[index])
            yellowDeltaTime.append(DeltaTime[index])
            ax5.scatter(lenghtVariance[index], DeltaTime[index], marker='o', color=color)
            #ax5.text(lenghtVariance[index], DeltaTime[index], str(SSTag[index]), size=9, zorder=1, color='k',
            #         rotation=45)
        index = index + 1
    

    #Curve order one: y = a * x + b
    #red
    x_line = np.arange(0, 350, 1)
    if len(redLenghtVariance) > 5:
        x_line = np.arange(min(redLenghtVariance), max(redLenghtVariance), 0.25)
        popt, _ = curve_fit(curveObjectiveOrder1, redLenghtVariance, redDeltaTime)
        a, b, c, d, e = popt
        curve = curveObjectiveOrder1(x_line, a, b, c, d, e)
        equation = "Curve Fitting: y = " + str(format(a,'.5E')) + " * x + " + str(format(b,'.5E'))
        #ax1.plot(x_line, curve, marker='_', color='red', label=equation)
    #green
    if len(greenLenghtVariance) > 5:
        x_line = np.arange(min(greenLenghtVariance), max(greenLenghtVariance), 0.25)
        popt, _ = curve_fit(curveObjectiveOrder1, greenLenghtVariance, greenDeltaTime)
        a, b, c, d, e = popt
        curve = curveObjectiveOrder1(x_line, a, b, c, d, e)
        #equation = "Curve Fitting: y = " + str(format(a, '.5E')) + " * x + " + str(format(b, '.5E'))
        #ax2.plot(x_line, curve, marker='_', color='green', label=equation)
    #
    if len(blueLenghtVariance) > 5:
        x_line = np.arange(min(blueLenghtVariance), max(blueLenghtVariance), 0.25)
        popt, _ = curve_fit(curveObjectiveOrder1, blueLenghtVariance, blueDeltaTime)
        a, b, c, d, e = popt
        curve = curveObjectiveOrder1(x_line, a, b, c, d, e)
        #equation = "Curve Fitting: y = " + str(format(a, '.5E')) + " * x + " + str(format(b, '.5E'))
        #ax3.plot(x_line, curve, marker='_', color='blue', label=equation)
    #black
    if len(blackLenghtVariance) > 5:
        x_line = np.arange(min(blackLenghtVariance), max(blackLenghtVariance), 0.25)
        popt, _ = curve_fit(curveObjectiveOrder1, blackLenghtVariance, blackDeltaTime)
        a, b, c, d, e = popt
        curve = curveObjectiveOrder1(x_line, a, b, c, d, e)
        #equation = "Curve Fitting: y = " + str(format(a, '.5E')) + " * x + " + str(format(b, '.5E'))
        #ax4.plot(x_line, curve, marker='_', color='grey', label=equation)
    #yellow
    if len(yellowLenghtVariance) > 5:
        x_line = np.arange(min(lenghtVariance), max(lenghtVariance), 0.25)
        popt, _ = curve_fit(curveObjectiveOrder1, yellowLenghtVariance, yellowDeltaTime)
        a, b, c, d, e = popt
        curve = curveObjectiveOrder1(x_line, a, b, c, d, e)
        #equation = "Curve Fitting: y = " + str(format(a, '.5E')) + " * x + " + str(format(b, '.5E'))
        #ax5.plot(x_line, curve, marker='_', color='yellow', label=equation)


    ##Curve order two: y = (a * x) + (b * x**2) + c
    #popt, _ = curve_fit(curveObjectiveOrder2, lenghtVariance, DeltaTime)
    #a, b, c, d, e = popt
    ## calculate the output for the range
    #curve = curveObjectiveOrder2(x_line, a, b, c, d, e)

    #equation = "Curve Fitting: y = " + str(format(a,'.5E')) + " * x + " + str(format(b,'.5E')) + " * x^2 + " + str(format(c,'.5E'))
    #axI.plot(x_line, curve, marker='_', color='blue', label=equation)

    ##Curve order three: y = (a * x) + (b * x**2) + (c * x**3) + d
    #popt, _ = curve_fit(curveObjectiveOrder3, lenghtVariance, DeltaTime)
    #a, b, c, d, e = popt
    ## calculate the output for the range
    #curve = curveObjectiveOrder3(x_line, a, b, c, d, e)

    #equation = "Curve Fitting: y = " + str(format(a,'.5E')) + " * x + " + str(format(b,'.5E')) + " * x^2 + " + str(format(c,'.5E')) + " * x^3 + " + str(format(d,'.5E'))
    #axI.plot(x_line, curve, marker='_', color='purple', label=equation)

    x_line = np.arange(0, 350, 1.0)
    curveOrder = SSCurve1[0]
    if curveOrder == '1':
        a = SSCurve1[1]
        b = SSCurve1[2] + SSOffset1
        y_line = a * x_line + b
        equation = "Curve Fitting: v = 10,0 m/s ; y = " + str(a) + " · x + " + str(b)
        ax1.plot(x_line, y_line, color='black', label=equation, linestyle='dashdot',linewidth = 4)
    elif curveOrder == '2':
        a = SSCurve1[1]
        b = SSCurve1[2]
        c = SSCurve1[3] + SSOffset1
        y_line = a * x_line + b * x_line * x_line + c
        equation = "Curve Fitting: v = 10,0 m/s ; y = " + str(a) + " · x^2 + " + str(b) + " · x + " + str(c)
        ax1.plot(x_line, y_line, color='red', label=equation)

    '''curveOrder = SSCurve1v08[0]
    if curveOrder == '1':
        a = SSCurve1v08[1]
        b = SSCurve1v08[2] + SSOffset1
        y_line = a * x_line + b
        equation = "Curve Fitting: v = 8,0 m/s ; y = " + str(a) + " · x + " + str(b)
        ax1.plot(x_line, y_line, color='orange', label=equation, linestyle='dashed',linewidth = 4)
    elif curveOrder == '2':
        a = SSCurve1v08[1]
        b = SSCurve1v08[2]
        c = SSCurve1v08[3] + SSOffset1
        y_line = a * x_line + b * x_line * x_line + c
        equation = "Curve Fitting: v = 8,0 m/s ; y = " + str(a) + " · x^2 + " + str(b) + " · x + " + str(c)
        ax1.plot(x_line, y_line, color='orange', label=equation)

    curveOrder = SSCurve1v025[0]
    if curveOrder == '1':
        a = SSCurve1v025[1]
        b = SSCurve1v025[2] + SSOffset1
        y_line = a * x_line + b
        equation = "Curve Fitting: v = 2,5 m/s ; y = " + str(a) + " · x + " + str(b)
        ax1.plot(x_line, y_line, color='green', label=equation, linestyle='dotted',linewidth = 4)
    elif curveOrder == '2':
        a = SSCurve1v025[1]
        b = SSCurve1v025[2]
        c = SSCurve1v025[3] + SSOffset1
        y_line = a * x_line + b * x_line * x_line + c
        equation = "Curve Fitting: v = 2,5 m/s ; y = " + str(a) + " · x^2 + " + str(b) + " · x + " + str(c)
        ax1.plot(x_line, y_line, color='green', label=equation)'''

    curveOrder = SSCurve2[0]
    if curveOrder == '1':
        a = SSCurve2[1]
        b = SSCurve2[2] + SSOffset2
        y_line = a * x_line + b
        equation = "Curve Fitting: v = 10,0 m/s ; y = " + str(a) + " · x + " + str(b)
        ax2.plot(x_line, y_line, color='red', label=equation, linestyle='dashdot',linewidth = 4)
    elif curveOrder == '2':
        a = SSCurve2[1]
        b = SSCurve2[2]
        c = SSCurve2[3] + SSOffset2
        y_line = a * x_line + b * x_line * x_line + c
        equation = "Curve Fitting: v = 10,0 m/s ; y = " + str(a) + " · x^2 + " + str(b) + " · x + " + str(c)
        ax2.plot(x_line, y_line, color='red', label=equation)


    #urveOrder = SSCurve2v50[0]
    #if curveOrder == '1':
    #    a = SSCurve2v50[1]
    #    b = SSCurve2v50[2] + SSOffset2
    #    y_line = a * x_line + b
    #    equation = "Curve Fitting: v = 5,0 m/s ; y = " + str(a) + " · x + " + str(b)
    #    ax2.plot(x_line, y_line, color='purple', label=equation, linestyle='dotted',linewidth = 4)
    #elif curveOrder == '2':
    #    a = SSCurve2v50[1]
    #    b = SSCurve2v50[2]
    #    c = SSCurve2v50[3] + SSOffset2
    #    y_line = a * x_line + b * x_line * x_line + c
    #    equation = "Curve Fitting: v = 5,0 m/s ; y = " + str(a) + " · x^2 + " + str(b) + " · x + " + str(c)
    #    ax2.plot(x_line, y_line, color='purple', label=equation)

    #curveOrder = SSCurve2v80[0]
    #if curveOrder == '1':
    #    a = SSCurve2v80[1]
    #    b = SSCurve2v80[2] + SSOffset2
    #    y_line = a * x_line + b
    #    equation = "Curve Fitting: v = 8,0 m/s ; y = " + str(a) + " · x + " + str(b)
    #    ax2.plot(x_line, y_line, color='orange', label=equation, linestyle='dashed',linewidth = 4)
    #elif curveOrder == '2':
    #    a = SSCurve2v80[1]
    #    b = SSCurve2v80[2]
    #    c = SSCurve2v80[3] + SSOffset2
    #    y_line = a * x_line + b * x_line * x_line + c
    #    equation = "Curve Fitting: v = 8,0 m/s ; y = " + str(a) + " · x^2 + " + str(b) + " · x + " + str(c)
    #    ax2.plot(x_line, y_line, color='orange', label=equation)


    curveOrder = SSCurve3[0]
    if curveOrder == '1':
        a = SSCurve3[1]
        b = SSCurve3[2] + SSOffset3
        y_line = a * x_line + b
        equation = "Curve Fitting: v = 10,0 m/s ; y = " + str(a) + " · x + " + str(b)
        ax3.plot(x_line, y_line, color='blue', label=equation, linestyle='dashdot',linewidth = 4)
    elif curveOrder == '2':
        a = SSCurve3[1]
        b = SSCurve3[2]
        c = SSCurve3[3]
        y_line = a * x_line + b * x_line * x_line + c
        equation = "Curve Fitting: v = 10,0 m/s ; y = " + str(a) + " · x^2 + " + str(b) + " · x + " + str(c)
        ax3.plot(x_line, y_line, color='red', label=equation)


    #curveOrder = SSCurve3v08[0]
    #if curveOrder == '1':
    #    a = SSCurve3v08[1]
    #    b = SSCurve3v08[2] + SSOffset3
    #    y_line = a * x_line + b
    #    equation = "Curve Fitting: v = 8,0 m/s ; y = " + str(a) + " · x + " + str(b)
    #    ax3.plot(x_line, y_line, color='orange', label=equation, linestyle='dashed',linewidth = 4)
    #elif curveOrder == '2':
    #    a = SSCurve3v08[1]
    #    b = SSCurve3v08[2]
    #    c = SSCurve3v08[3]
    #    y_line = a * x_line + b * x_line * x_line + c
    #    equation = "Curve Fitting: v = 8,0 m/s ; y = " + str(a) + " · x^2 + " + str(b) + " · x + " + str(c)
    #    ax3.plot(x_line, y_line, color='orange', label=equation)
#
    #curveOrder = SSCurve3v50[0]
    #if curveOrder == '1':
    #    a = SSCurve3v50[1]
    #    b = SSCurve3v50[2] + SSOffset3
    #    y_line = a * x_line + b
    #    equation = "Curve Fitting: v = 5,0 m/s ; y = " + str(a) + " · x + " + str(b)
    #    ax3.plot(x_line, y_line, color='purple', label=equation, linestyle='dotted',linewidth = 4)
    #elif curveOrder == '2':
    #    a = SSCurve3v50[1]
    #    b = SSCurve3v50[2]
    #    c = SSCurve3v50[3]
    #    y_line = a * x_line + b * x_line * x_line + c
    #    equation = "Curve Fitting: v = 5,0 m/s ; y = " + str(a) + " · x^2 + " + str(b) + " · x + " + str(c)
    #    ax3.plot(x_line, y_line, color='purple', label=equation)
    #


    curveOrder = SSCurve4[0]
    if curveOrder == '1':
        a = SSCurve4[1]
        b = SSCurve4[2] + SSOffset4
        y_line = a * x_line + b
        equation = "Curve Fitting: v = 10,0 m/s ; y = " + str(a) + " · x + " + str(b)
        ax4.plot(x_line, y_line, color='blue', label=equation, linestyle='dashdot',linewidth = 4)
    elif curveOrder == '2':
        a = SSCurve4[1]
        b = SSCurve4[2]
        c = SSCurve4[3]
        y_line = a * x_line + b * x_line * x_line + c
        equation = "Curve Fitting: v = 10,0 m/s ; y = " + str(a) + " · x^2 + " + str(b) + " · x + " + str(c)
        ax4.plot(x_line, y_line, color='red', label=equation)


    #curveOrder = SSCurve4v08[0]
    #if curveOrder == '1':
    #    a = SSCurve4v08[1]
    #    b = SSCurve4v08[2] + SSOffset4
    #    y_line = a * x_line + b
    #    equation = "Curve Fitting: v = 8,0 m/s ; y = " + str(a) + " · x + " + str(b)
    #    ax4.plot(x_line, y_line, color='orange', label=equation, linestyle='dashed',linewidth = 4)
    #elif curveOrder == '2':
    #    a = SSCurve4v08[1]
    #    b = SSCurve4v08[2]
    #    c = SSCurve4v08[3]
    #    y_line = a * x_line + b * x_line * x_line + c
    #    equation = "Curve Fitting: v = 8,0 m/s ; y = " + str(a) + " · x^2 + " + str(b) + " · x + " + str(c)
    #    ax4.plot(x_line, y_line, color='orange', label=equation)
#
    #curveOrder = SSCurve4v50[0]
    #if curveOrder == '1':
    #    a = SSCurve4v50[1]
    #    b = SSCurve4v50[2]
    #    y_line = a * x_line + b
    #    equation = "Curve Fitting: v = 5,0 m/s ; y = " + str(a) + " · x + " + str(b)
    #    ax4.plot(x_line, y_line, color='purple', label=equation, linestyle='dotted',linewidth = 4)
    #elif curveOrder == '2':
    #    a = SSCurve4v50[1]
    #    b = SSCurve4v50[2]
    #    c = SSCurve4v50[3]
    #    y_line = a * x_line + b * x_line * x_line + c
    #    equation = "Curve Fitting: v = 5,0 m/s ; y = " + str(a) + " · x^2 + " + str(b) + " · x + " + str(c)
    #    ax4.plot(x_line, y_line, color='purple', label=equation)
    #


    ax1.legend(fontsize=18)
    plt.show()
    fig1.set_size_inches(14.0, 14.0)
    fig1.savefig(filepath + "-curveFitting_1.png")
    plt.close(fig1)

    ax2.legend(fontsize=18)
    plt.show()
    fig2.set_size_inches(14.0, 14.0)
    fig2.savefig(filepath + "-curveFitting_2.png")
    plt.close(fig1)

    ax3.legend(fontsize=18)
    plt.show()
    fig3.set_size_inches(14.0, 14.0)
    fig3.savefig(filepath + "-curveFitting_3.png")
    plt.close(fig1)

    ax4.legend(fontsize=18)
    plt.show()
    fig4.set_size_inches(14.0, 14.0)
    fig4.savefig(filepath + "-curveFitting_4.png")
    plt.close(fig1)

    ax5.legend(fontsize=18)
    plt.show()
    fig5.set_size_inches(14.0, 14.0)
    fig5.savefig(filepath + "-curveFitting_5.png")
    plt.close(fig1)


def getSSLengthVariationPlot(filepath, lenghtVariance, DeltaTime):
    if len(lenghtVariance) < 5:
        print("Not enough StraightSegment's events to interpolate")
        return 1

    figI = plt.figure()
    #ggplot style
    plt.style.use('ggplot')
    axI = figI.add_subplot(111)
    axI.set_xlabel('AL (m)', fontsize=18)
    axI.set_ylabel('Time (s)', fontsize=18)

    x_line = np.arange(min(lenghtVariance), 350, 1)

    # Curve order one: y = a * x + b
    popt, _ = curve_fit(curveObjectiveOrder1, lenghtVariance, DeltaTime)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder1(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(format(a, '.5E')) + " * x + " + str(format(b, '.5E'))
    axI.plot(x_line, curve, marker='_', color='red', label=equation)

    axI.scatter(lenghtVariance, DeltaTime, marker='o', color='black')
    axI.legend(fontsize=18)
    plt.title("Straight Segments Length variation[AL] vs Time variation[At]", fontsize=20)
    plt.show()
    figI.set_size_inches(14.0, 14.0)
    figI.savefig(filepath + "-curveFitting.png")
    plt.close(figI)


# curves objectives to fit
def curveObjectiveOrder1(x, a, b, c, d, e): return a * x + b  # straight line


def curveObjectiveOrder2(x, a, b, c, d, e): return (a * x) + (b * x ** 2) + c  # polynomial regression order 2


def curveObjectiveOrder3(x, a, b, c, d, e): return (a * x) + (b * x ** 2) + (
            c * x ** 3) + d  # polynomial regression order 3


def curveObjectiveOrder4(x, a, b, c, d, e): return (a * x) + (b * x ** 2) + (c * x ** 3) + (
            c * x ** 4) + e  # polynomial regression order 4


def curveObjectiveOrder4(x, a, b, c, d, e, f): return (a * x) + (b * x ** 2) + (c * x ** 3) + (d * x ** 4) + (
            e * x ** 5) + f  # polynomial regression order 5


def curveObjectiveGaussian(x, a, b, c, d, e): return a * np.exp(-np.power(x - b, 2) / (2 * np.power(c, 2)))  # Gaussian


def curveObjectiveSine(x, a, b, c, d, e): return a * np.sin(b - x) + c * x ** 2 + d  # Sine curve


def curveObjective3D(X, a, b, c, d):
    x, y = X
    return a * x + b * y + c

def getSSCurve(previousEvent, followingEvent,v):
    aOffset = 0
    if (previousEvent.type == et.Event_Type.EndCourseChange or previousEvent.type == et.Event_Type.EndSS) and followingEvent == "Turn":
        mCurve = mSS1
        aCurve = aSS1
        aOffset = 0
    elif (previousEvent.type == et.Event_Type.EndAltChange or previousEvent.type == et.Event_Type.EndHover or previousEvent.type == et.Event_Type.EndAltChangeNV) and (followingEvent == "VCD" or followingEvent == "NVCD" or followingEvent == "Hover"):
        mCurve = mSS2
        aCurve = aSS2
    elif (previousEvent.type == et.Event_Type.EndAltChange or previousEvent.type == et.Event_Type.EndHover or previousEvent.type == et.Event_Type.EndAltChangeNV ) and followingEvent == "Turn":
        mCurve = mSS3
        aCurve = aSS3
    elif (previousEvent.type == et.Event_Type.EndCourseChange or previousEvent.type == et.Event_Type.EndSS) and (followingEvent == "VCD" or followingEvent == "NVCD" or followingEvent == "Hover"):
        mCurve = mSS4
        aCurve = aSS4
    else:
        print("Error in SS get curve")

    m = simmulateCurve(mCurve, 0, v)
    a = simmulateCurve(aCurve, aOffset, v)
    curve = ['1', m, a]
    return curve, 0

def getCurveEvolution(filepath, eventType):
    figI = plt.figure()
    # seaborn-darkgrid style
    plt.style.use('ggplot')
    axI = figI.add_subplot(211)
    axI.set_xlabel('Speed (m/s)', fontsize=18)
    axI.set_ylabel('Curve Slope m', fontsize=18)
    plt.title("Curve evolution with velocity variation\n y = m·x + a", fontsize=20)
    axII = figI.add_subplot(212)
    axII.set_xlabel('Speed (m/s)', fontsize=18)
    axII.set_ylabel('Offset a', fontsize=18)
    plt.title("Curve evolution with velocity variation\n y = m·x + a", fontsize=20)

    if eventType == "Case1":
        axI.scatter(2.5, SSCurve1v025[1], marker='o', color='black')
        axI.scatter(8.0, SSCurve1v08[1], marker='o', color='black')
        axI.scatter(10.0, SSCurve1[1], marker='o', color='black')

        axII.scatter(2.5, SSCurve1v025[2], marker='o', color='black')
        axII.scatter(8.0, SSCurve1v08[2], marker='o', color='black')
        axII.scatter(10.0, SSCurve1[2], marker='o', color='black')

        x_line = np.arange(2.5, 10.0, 0.05)
        xvariance = [2.5,8.0,10.0,2.5,8.0,10.0]
        yvarianceI = [SSCurve1v025[1], SSCurve1v08[1], SSCurve1[1],SSCurve1v025[1], SSCurve1v08[1], SSCurve1[1]]
        yvarianceII = [SSCurve1v025[2], SSCurve1v08[2], SSCurve1[2],SSCurve1v025[2], SSCurve1v08[2], SSCurve1[2]]

    elif eventType == "Case2":
        axI.scatter(5.0, SSCurve2v50[1], marker='o', color='black')
        axI.scatter(8.0, SSCurve2v80[1], marker='o', color='black')
        axI.scatter(10.0, SSCurve2[1], marker='o', color='black')

        axII.scatter(5.0, SSCurve2v50[2], marker='o', color='black')
        axII.scatter(8.0, SSCurve2v80[2], marker='o', color='black')
        axII.scatter(10.0, SSCurve2[2], marker='o', color='black')

        x_line = np.arange(5.0, 10.0, 0.05)
        xvariance = [5.0,8.0,10.0,5.0,8.0,10.0]
        yvarianceI = [SSCurve2v50[1], SSCurve2v80[1],SSCurve2[1], SSCurve2v50[1], SSCurve2v80[1],SSCurve2[1] ]
        yvarianceII = [SSCurve2v50[2], SSCurve2v80[2],SSCurve2[2], SSCurve2v50[2], SSCurve2v80[2],SSCurve2[2]]

    elif eventType == "Case3":
        axI.scatter(5.0, SSCurve3v50[1], marker='o', color='black')
        axI.scatter(8.0, SSCurve3v08[1], marker='o', color='black')
        axI.scatter(10.0, SSCurve3[1], marker='o', color='black')

        axII.scatter(5.0, SSCurve3v50[2], marker='o', color='black')
        axII.scatter(8.0, SSCurve3v08[2], marker='o', color='black')
        axII.scatter(10.0, SSCurve3[2], marker='o', color='black')

        x_line = np.arange(2.5, 10.0, 0.05)
        xvariance = [5.0, 8.0, 10.0, 5.0, 8.0, 10.0]
        yvarianceI = [SSCurve3v50[1], SSCurve3v08[1], SSCurve3[1], SSCurve3v50[1], SSCurve3v08[1], SSCurve3[1]]
        yvarianceII = [SSCurve3v50[2], SSCurve3v08[2], SSCurve3[2], SSCurve3v50[2], SSCurve3v08[2], SSCurve3[2]]

    elif eventType == "Case4":
        axI.scatter(5.0, SSCurve4v50[1], marker='o', color='black')
        axI.scatter(8.0, SSCurve4v08[1], marker='o', color='black')
        axI.scatter(10.0, SSCurve4[1], marker='o', color='black')
        axI.scatter(18.0, SSCurve4vmDrB[1], marker='o', color='black')

        axII.scatter(5.0, SSCurve4v50[2], marker='o', color='black')
        axII.scatter(8.0, SSCurve4v08[2], marker='o', color='black')
        axII.scatter(10.0, SSCurve4[2], marker='o', color='black')
        axII.scatter(18.0, SSCurve4vmDrB[2], marker='o', color='black')

        x_line = np.arange(1.2, 18.0, 0.05)
        xvariance = [5.0,8.0,10.0,18.0,5.0,8.0,10.0,18.0]
        yvarianceI = [SSCurve4v50[1],SSCurve4v08[1], SSCurve4[1], SSCurve4vmDrB[1],SSCurve4v50[1],SSCurve4v08[1], SSCurve4[1], SSCurve4vmDrB[1]]
        yvarianceII = [SSCurve4v50[2],SSCurve4v08[2], SSCurve4[2], SSCurve4vmDrB[2],SSCurve4v50[2],SSCurve4v08[2], SSCurve4[2], SSCurve4vmDrB[2]]
    else:
        return -1



    # Curve order one: y = a * x + b
    '''popt, _ = curve_fit(curveObjectiveOrder1, xvariance, yvarianceI)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder1(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(format(a,'.5E')) + " * x + " + str(format(b,'.5E'))
    axI.plot(x_line, curve, marker='_', color='red', label=equation)'''

    # Curve order two: y = (a * x) + (b * x**2) + c
    popt, _ = curve_fit(curveObjectiveOrder2, xvariance, yvarianceI)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder2(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(format(a,'.5E')) + " * x + " + str(format(b,'.5E')) + " * x^2 + " + str(format(c,'.5E'))
    axI.plot(x_line, curve, marker='_', color='blue', label=equation)

    #-----------------------------------------------------------------------------------------------------------------------
    '''# Curve order one: y = a * x + b
    popt, _ = curve_fit(curveObjectiveOrder1, xvariance, yvarianceII)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder1(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(format(a,'.5E')) + " * x + " + str(format(b,'.5E'))
    axII.plot(x_line, curve, marker='_', color='green', label=equation)'''

    # Curve order two: y = (a * x) + (b * x**2) + c
    popt, _ = curve_fit(curveObjectiveOrder2, xvariance, yvarianceII)
    a, b, c, d, e = popt
    # calculate the output for the range
    curve = curveObjectiveOrder2(x_line, a, b, c, d, e)

    equation = "Curve Fitting: y = " + str(format(a,'.5E')) + " * x + " + str(format(b,'.5E')) + " * x^2 + " + str(format(c,'.5E'))
    axII.plot(x_line, curve, marker='_', color='orange', label=equation)

    axI.legend(fontsize=18)
    axII.legend(fontsize=18)
    plt.show()
    figI.set_size_inches(14.0, 14.0)
    figI.savefig(filepath + "SS_" + eventType + "CurveEvolution.png")
    plt.close(figI)

def getAltitudeChangeCurve(eventType, v):
    aOffset = 0
    if eventType == "Case1":
        mCurve = mSS1
        aCurve = aSS1
        aOffset = 0
    elif eventType == "Case2":
        mCurve = mSS2
        aCurve = aSS2
    elif eventType == "Case3":
        mCurve = mSS3
        aCurve = aSS3
    elif eventType == "Case4":
        mCurve = mSS4
        aCurve = aSS4
    elif eventType == "Case5":
        mCurve = mSS5
        aCurve = aSS5
    else:
        return -1
    m = simmulateCurve(mCurve, 0, v)
    a = simmulateCurve(aCurve, aOffset, v)
    curve = ['1', m, a]
    return curve, 0

def simmulateCurve(curve, Offset, data):
    deltaTime = 0

    if curve[0] == '1':
        a = curve[1]
        b = curve[2] + Offset
        deltaTime = a * data + b
    elif curve[0] == '2':
        a = curve[1]
        b = curve[2]
        c = curve[3] + Offset
        deltaTime = a * data + b * data * data + c
    elif curve[0] == '3':
        a = curve[1]
        b = curve[2]
        c = curve[3]
        d = curve[4] + Offset
        deltaTime = a * data + b * data * data + c * data * data * data + d

    return deltaTime