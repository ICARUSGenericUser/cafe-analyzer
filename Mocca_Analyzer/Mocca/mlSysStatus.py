from Mocca_Analyzer.Mocca import Mocca_Object as mo
from Mocca_Analyzer.Mocca.Mocca_Object import Object_Type


class mlSysStatus(mo.Mocca_Object):
  def __init__(self):
    super().__init__()
    self.type = mo.Object_Type.mlSysStatus
    self.onboard_control_sensors_present = 0
    self.onboard_control_sensors_enabled = 0
    self.onboard_control_sensors_health = 0
    self.load = 0
    self.voltage_battery = 0
    self.current_battery = 0
    self.battery_remaining = 0
    self.drop_rate_comm = 0
    self.errors_comm = 0
    self.errors_count1 = 0

  def decodeMsg(self, row):
    self.timestamp = int(row["timeStamp"])
    self.date = int(row["date"])
    self.onboard_control_sensors_present = int(row["P01"])
    self.onboard_control_sensors_enabled = int(row["P02"])
    self.onboard_control_sensors_health = int(row["P03"])
    self.load = int(row["P04"])
    self.voltage_battery = int(row["P05"])
    self.current_battery = int(row["P06"])
    self.battery_remaining = int(row["P07"])
    self.drop_rate_comm = int(row["P08"])
    self.errors_comm = int(row["P09"])
    self.errors_count1 = int(row["P10"])