from Mocca_Analyzer.Mocca import Mocca_Object as mo
from Mocca_Analyzer.Mocca.Mocca_Object import Object_Type


class mlNavControllerOut(mo.Mocca_Object):
  def __init__(self):
    super().__init__()
    self.type = mo.Object_Type.mlNavControllerOut
    self.navRoll = 0.0
    self.navPitch = 0.0
    self.navBearing = 0.0
    self.targetBearing = 0.0
    self.wpDistance = 0.0
    self.altitudeError = 0.0
    self.airspeedError = 0.0
    self.crosstrackError = 0.0

  def decodeMsg(self, row):
    self.timestamp = int(row["timeStamp"])
    self.date = int(row["date"])
    self.navRoll = float(row["P01"])
    self.navPitch = float(row["P02"])
    self.navBearing = float(row["P03"])
    self.targetBearing = float(row["P04"])
    self.wpDistance = float(row["P05"])
    self.altitudeError = float(row["P06"])
    self.airspeedError = float(row["P07"])
    self.crosstrackError = float(row["P08"])