from Mocca_Analyzer.Mocca import Mocca_Object as mo


class swGNSSNavigation(mo.Mocca_Object):
  def __init__(self):
    super().__init__()
    self.type = mo.Object_Type.swGNSSNavigation
    self.fixState = mo.swFixState.Unknown
    self.latitude = 0.0
    self.longitude = 0.0
    self.hAccuracy = 0.0
    self.altitude = 0.0
    self.vAccuracy = 0.0
    self.altitudeOnWgs84 = 0.0
    self.timeAccuracy = 0.0
    self.hSpeed = 0.0
    self.hSpeedAccuracy = 0.0
    self.vSpeed = 0.0
    self.vSpeedAccuracy = 0.0
    self.course = 0.0
    self.cAccuracy = 0.0
    self.heading = 0.0
    self.hdAccuracy = 0.0
    self.magDeviation = 0.0
    self.overflow = 0
    self.result = 0


  def decodeMsg(self, row):
    self.timestamp = int(row["timeStamp"])
    self.fixState = row["P01"]
    self.latitude = float(row["P02"])
    self.longitude = float(row["P03"])
    self.hAccuracy = float(row["P04"])
    self.altitude = float(row["P05"])
    self.vAccuracy = float(row["P06"])
    self.altitudeOnWgs84 = float(row["P07"])
    self.timeAccuracy = float(row["P08"])
    self.hSpeed = float(row["P09"])
    self.hSpeedAccuracy = float(row["P10"])
    self.vSpeed = float(row["P11"])
    self.vSpeedAccuracy = float(row["P12"])
    self.course = float(row["P13"])
    self.cAccuracy = float(row["P14"])
    self.heading = float(row["P15"])
    self.hdAccuracy = float(row["P16"])
    self.magDeviation = float(row["P17"])
    self.overflow = int(row["P18"])
    self.result = int(row["P19"])

