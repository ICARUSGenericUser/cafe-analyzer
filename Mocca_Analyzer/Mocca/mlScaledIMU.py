import math as math
from Mocca_Analyzer.Mocca import Mocca_Object as mo
from Mocca_Analyzer.Mocca.Mocca_Object import Object_Type
from Util import angles as agl


class mlScaledIMU(mo.Mocca_Object):
  def __init__(self):
    super().__init__()
    self.type = mo.Object_Type.mlScaledIMU
    self.sensor = 0
    self.x_lin_acc = 0.0
    self.y_lin_acc = 0.0
    self.z_lin_acc = 0.0
    self.x_ang_acc = 0.0
    self.y_ang_acc = 0.0
    self.z_ang_acc = 0.0
    self.x_mag = 0.0
    self.y_mag = 0.0
    self.z_mag = 0.0


  def decodeMsg(self, row):
    self.timestamp = int(row["timeStamp"])
    self.date = int(row["date"])
    self.sensor = int(row["P01"])
    self.x_lin_acc = float(row["P02"])
    self.y_lin_acc = float(row["P03"])
    self.z_lin_acc = float(row["P04"])
    self.x_ang_acc = float(row["P05"])
    self.y_ang_acc = float(row["P06"])
    self.z_ang_acc = float(row["P07"])
    self.x_mag = float(row["P08"])
    self.y_mag = float(row["P09"])
    self.z_mag = float(row["P10"])
    self.hAcceleration = math.sqrt(self.x_lin_acc**2 + self.y_lin_acc**2)
    self.vAcceleration = self.z_lin_acc

