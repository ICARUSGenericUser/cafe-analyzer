from Mocca_Analyzer.Mocca import Mocca_Object as mo
from Mocca_Analyzer.Mocca.Mocca_Object import Object_Type


class mlScaledPressure(mo.Mocca_Object):
  def __init__(self):
    super().__init__()
    self.type = mo.Object_Type.mlScaledPressure
    self.sensor = 0
    self.pressure_abs = 0.0
    self.pressure_diff = 0.0
    self.temperature = 0.0

  def decodeMsg(self, row):
    self.timestamp = int(row["timeStamp"])
    self.date = int(row["date"])
    self.sensor = int(row["P01"])
    self.pressure_abs = float(row["P02"])
    self.pressure_diff = float(row["P03"])
    self.temperature = float(row["P04"])