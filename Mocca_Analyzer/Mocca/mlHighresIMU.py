import math as math
from Mocca_Analyzer.Mocca import Mocca_Object as mo
from Util import angles as agl


class mlHighresIMU(mo.Mocca_Object):
  def __init__(self):
    super().__init__()
    self.type = mo.Object_Type.mlHighresIMU
    self.x_lin_acc = 0.0
    self.y_lin_acc = 0.0
    self.z_lin_acc = 0.0
    self.x_ang_acc = 0.0
    self.y_ang_acc = 0.0
    self.z_ang_acc = 0.0
    self.x_mag = 0.0
    self.y_mag = 0.0
    self.z_mag = 0.0
    self.pressure_abs = 0.0
    self.pressure_diff = 0.0
    self.pressure_alt = 0.0
    self.temperature = 0.0
    self.fields_updated = 0
    self.hAcceleration = 0.0
    self.vAcceleration = 0.0


  def decodeMsg(self, row):
    self.timestamp = int(row["timeStamp"])
    self.date = int(row["date"])
    self.x_lin_acc = float(row["P01"])
    self.y_lin_acc = float(row["P02"])
    self.z_lin_acc = float(row["P03"])
    self.x_ang_acc = float(row["P04"])
    self.y_ang_acc = float(row["P05"])
    self.z_ang_acc = float(row["P06"])
    self.x_mag = float(row["P07"])
    self.y_mag = float(row["P08"])
    self.z_mag = float(row["P09"])
    self.pressure_abs = float(row["P10"])
    self.pressure_diff = float(row["P11"])
    self.pressure_alt = float(row["P12"])
    self.temperature = float(row["P13"])
    self.fields_updated = float(row["P14"])
    self.hAcceleration = math.sqrt(self.x_lin_acc**2 + self.y_lin_acc**2)
    self.vAcceleration = self.z_lin_acc

