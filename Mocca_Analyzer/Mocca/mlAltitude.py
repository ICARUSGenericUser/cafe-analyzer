from Mocca_Analyzer.Mocca import Mocca_Object as mo
from Mocca_Analyzer.Mocca.Mocca_Object import Object_Type


class mlAltitude(mo.Mocca_Object):
  def __init__(self):
    super().__init__()
    self.type = mo.Object_Type.mlAltitude
    self.altitude_mon = 0.0
    self.altitude_amsl = 0.0
    self.altitude_local = 0.0
    self.altitude_relative = 0.0
    self.altitude_terrain = 0.0
    self.bottom_clearance = 0.0

  def decodeMsg(self, row):
    self.timestamp = int(row["timeStamp"])
    self.date = int(row["date"])
    self.altitude_mon = float(row["P01"])
    self.altitude_amsl = float(row["P02"])
    self.altitude_local = float(row["P03"])
    self.altitude_relative = float(row["P04"])
    self.altitude_terrain = float(row["P05"])
    self.bottom_clearance = float(row["P06"])