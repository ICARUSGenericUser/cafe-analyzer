import math as math
from Util import angles as ang
from Mocca_Analyzer.Mocca import Mocca_Object as mo


class mlGlobalPositionInt(mo.Mocca_Object):
  def __init__(self):
    super().__init__()
    self.type = mo.Object_Type.mlGlobalPositionInt
    self.latitude = 0.0
    self.longitude = 0.0
    self.altitude = 0.0
    self.relativeAltitude = 0.0
    self.vx = 0.0
    self.vy = 0.0
    self.vz = 0.0
    self.heading = 0.0
    # Derived from vx, vy
    self.hspeed = 0.0
    self.course = 0.0


  def decodeMsg(self, row):
    self.timestamp = int(row["timeStamp"])
    self.date = int(row["date"])
    self.latitude = float(row["P01"])
    self.longitude = float(row["P02"])
    self.altitude = float(row["P03"])
    self.relativeAltitude = float(row["P04"])
    self.vx = float(row["P05"])
    self.vy = float(row["P06"])
    self.vz = float(row["P07"])
    self.heading = float(row["P08"]) / 100
    #self.course = ang.normalize(math.atan2(self.vy,self.vx) * 180 / math.pi) #Degrees
    self.course = ang.normalize(90 - (180 / math.pi) * math.atan2(self.vx, self.vy))
    self.hspeed = math.sqrt(self.vx**2 + self.vy**2)


  def decodeSIMMsg(self, row):
    self.timestamp = int(row[0].replace(".",","))
    self.date = int(row[1])
    self.latitude = float(row[2].replace(".",",")) / 10e6
    self.longitude = float(row[3].replace(".",",")) / 10e6
    self.altitude = float(row[4].replace(".",",")) / 1000.0
    self.relativeAltitude = float(row[5])
    self.vx = float(row[6].replace(".",",")) / 100
    self.vy = float(row[7].replace(".",",")) / 100
    self.vz = float(row[8].replace(".",",")) / 100
    self.heading = float(row[9].replace(".",",")) / 100.0
    #self.course = ang.normalize(math.atan2(self.vy,self.vx) * 180 / math.pi) #Degrees
    self.course = ang.normalize(90 - (180 / math.pi) * math.atan2(self.vx, self.vy))
    self.hspeed = math.sqrt(self.vx**2 + self.vy**2)


def AltitudeTendency(current, next):
  if next.altitude - current.altitude > 0:
    return 1
  else:
    return -1
