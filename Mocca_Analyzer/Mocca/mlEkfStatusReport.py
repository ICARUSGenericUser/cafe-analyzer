from Mocca_Analyzer.Mocca import Mocca_Object as mo
from Mocca_Analyzer.Mocca.Mocca_Object import Object_Type


class mlEkfStatusReport(mo.Mocca_Object):
  def __init__(self):
    super().__init__()
    self.type = mo.Object_Type.mlEkfStatusReport
    self.flags = 0
    self.velocityVariance = 0.0
    self.posHorizVariance = 0.0
    self.posVertVariance = 0.0
    self.compassVariance = 0.0
    self.terrainAltVariance = 0.0
    self.airspeedVariance = 0.0

  def decodeMsg(self, row):
    self.timestamp = int(row["timeStamp"])
    self.date = int(row["date"])
    self.flags = int(row["P01"])
    self.velocityVariance = float(row["P02"])
    self.posHorizVariance = float(row["P03"])
    self.posVertVariance = float(row["P04"])
    self.compassVariance = float(row["P05"])
    self.terrainAltVariance = float(row["P06"])
    self.airspeedVariance = float(row["P07"])