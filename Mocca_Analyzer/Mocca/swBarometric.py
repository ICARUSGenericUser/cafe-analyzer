from Mocca_Analyzer.Mocca import Mocca_Object as mo


class swBarometric(mo.Mocca_Object):
  def __init__(self):
    super().__init__()
    self.type = mo.Object_Type.swBarometric
    self.pressure = 0.0
    self.temperature = 0.0
    self.humidity = 0.0


  def decodeMsg(self, row):
    self.timestamp = int(row["timeStamp"])
    self.pressure = float(row["P01"])
    self.temperature = float(row["P02"])
    self.humidity = float(row["P03"])
