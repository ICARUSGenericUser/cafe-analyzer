import enum
from geographiclib import geodesic as geo


class Object_Type(enum.Enum):
  Unknown = 0
  swGNSSNavigation = 1
  swInertial = 2
  swBarometric = 3
  mlHeartbeat = 4
  mlGlobalPositionInt = 5
  mlGPSRawInt = 6
  mlScaledIMU = 7
  mlHighresIMU = 8
  mlVibration = 9
  mlScaledPressure = 10
  mlAltitude = 11
  mlAttitude = 12
  mlRangeFinder = 13
  mlDistanceSensor = 14
  mlNavControllerOut = 15
  mlVfrHud = 16
  mlEkfStatusReport = 17
  mlSysStatus = 18
  mlHomePosition = 19
  djiGNSSIMUNavigation = 20
  djiGPSDetails = 21
  djiBattery = 22
  basic = 23


class swFixState(enum.Enum):
  Unknown = 0
  LE_NoFix = 2
  LE_2D = 2
  LE_3D = 3

class djiFlightMode(enum.Enum):
  FLIGHT_MODE_UNKNOWN = 0
  FLIGHT_MODE_ATTI = 1
  FLIGHT_MODE_GPS = 2
  FLIGHT_MODE_NAVIGATION = 3
  FLIGHT_MODE_TAKEOFF = 4
  FLIGHT_MODE_LANDING = 5
  FLIGHT_MODE_RTL = 6


class djiFlightStatus(enum.Enum):
  FLIGHT_STATUS_UNKNOWN = 0
  FLIGHT_STATUS_STOPPED = 1
  FLIGHT_STATUS_ON_GROUND = 2
  FLIGHT_STATUS_ON_AIR = 3



class Mocca_Object:
  def __init__(self):
    self.type = Object_Type.Unknown
    self.idx = -1
    self.timestamp = 0
    self.date = None


  def getDistance(self, targetSample):
    distance = geo.Geodesic.WGS84.Inverse(self.latitude, self.longitude, targetSample.latitude, targetSample.longitude)
    return distance['s12']

