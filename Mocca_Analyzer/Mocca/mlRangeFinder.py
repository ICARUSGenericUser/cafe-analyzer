from Mocca_Analyzer.Mocca import Mocca_Object as mo
from Mocca_Analyzer.Mocca.Mocca_Object import Object_Type


class mlRangeFinder(mo.Mocca_Object):
  def __init__(self):
    super().__init__()
    self.type = mo.Object_Type.mlRangeFinder
    self.distance = 0.0
    self.voltage = 0.0

  def decodeMsg(self, row):
    self.timestamp = int(row["timeStamp"])
    self.date = int(row["date"])
    self.distance = float(row["P01"])
    self.voltage = float(row["P02"])