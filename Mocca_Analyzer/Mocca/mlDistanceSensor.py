from Mocca_Analyzer.Mocca import Mocca_Object as mo
from Mocca_Analyzer.Mocca.Mocca_Object import Object_Type


class mlDistanceSensor(mo.Mocca_Object):
  def __init__(self):
    super().__init__()
    self.type = mo.Object_Type.mlDistanceSensor
    self.min_distance = 0
    self.max_distance = 0
    self.current_distance = 0.0
    self.type = 0
    self.sensor_id = 0
    self.orientation = 0
    self.covariance = 0
    self.horizontal_fov = 0.0
    self.vertical_fov = 0.0

  def decodeMsg(self, row):
    self.timestamp = int(row["timeStamp"])
    self.date = int(row["date"])
    self.min_distance = int(row["P01"])
    self.max_distance = int(row["P02"])
    self.current_distance = float(row["P03"])
    self.type = int(row["P04"])
    self.sensor_id = int(row["P05"])
    self.orientation = int(row["P06"])
    self.covariance = int(row["P07"])
    self.horizontal_fov = float(row["P08"])
    self.vertical_fov = float(row["P09"])