from Mocca_Analyzer.Mocca import Mocca_Object as mo


class swInertial(mo.Mocca_Object):
  def __init__(self):
    super().__init__()
    self.type = mo.Object_Type.swInertial
    self.xLinealAcceleration = 0.0
    self.yLinealAcceleration = 0.0
    self.zLinealAcceleration = 0.0
    self.xAngularAcceleration = 0.0
    self.yAngularAcceleration = 0.0
    self.zAngularAcceleration = 0.0


  def decodeMsg(self, row):
    self.timestamp = int(row["timeStamp"])
    self.xLinealAcceleration = float(row["P01"])
    self.yLinealAcceleration = float(row["P02"])
    self.zLinealAcceleration = float(row["P03"])
    self.xAngularAcceleration = float(row["P04"])
    self.yAngularAcceleration = float(row["P05"])
    self.zAngularAcceleration = float(row["P06"])
