from Mocca_Analyzer.Mocca import Mocca_Object as mo
from Mocca_Analyzer.Mocca.Mocca_Object import Object_Type


class mlVibration(mo.Mocca_Object):
  def __init__(self):
    super().__init__()
    self.type = mo.Object_Type.mlVibration
    self.vibration_x = 0.0
    self.vibration_y = 0.0
    self.vibration_z = 0.0
    self.clipping_0 = 0
    self.clipping_1 = 0
    self.clipping_2 = 0

  def decodeMsg(self, row):
    self.timestamp = int(row["timeStamp"])
    self.date = int(row["date"])
    self.vibration_x = float(row["P01"])
    self.vibration_y = float(row["P02"])
    self.vibration_z = float(row["P03"])
    self.clipping_0 = int(row["P04"])
    self.clipping_1 = int(row["P05"])
    self.clipping_2 = int(row["P06"])