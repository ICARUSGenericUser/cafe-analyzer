from Mocca_Analyzer.Mocca import Mocca_Object as mo
from Mocca_Analyzer.Mocca.Mocca_Object import Object_Type


class mlVfrHud(mo.Mocca_Object):
  def __init__(self):
    super().__init__()
    self.type = mo.Object_Type.mlVfrHud
    self.airspeed = 0.0
    self.hSpeed = 0.0
    self.heading = 0.0
    self.throttle = 0.0
    self.altitude = 0.0
    self.vSpeed = 0.0

  def decodeMsg(self, row):
    self.timestamp = int(row["timeStamp"])
    self.date = int(row["date"])
    self.airspeed = float(row["P01"])
    self.hSpeed = float(row["P02"])
    self.heading = float(row["P03"])
    self.throttle = float(row["P04"])
    self.altitude = float(row["P05"])
    self.vSpeed = float(row["P06"])