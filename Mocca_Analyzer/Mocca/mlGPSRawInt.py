from Mocca_Analyzer.Mocca import Mocca_Object as mo
from Mocca_Analyzer.Mocca.Mocca_Object import Object_Type


class mlGPSRawInt(mo.Mocca_Object):
  def __init__(self):
    super().__init__()
    self.type = mo.Object_Type.mlGPSRawInt
    self.fixtype = ""
    self.latitude = 0.0
    self.longitude = 0.0
    self.altitude = 0.0
    self.hdop = 0.0
    self.vdop = 0.0
    self.hspeed = 0.0
    self.course = 0.0
    self.satsInView = 0
    self.altitudeOnWgs84 = 0.0
    self.hAccuracy = 0.0
    self.vAccuracy = 0.0
    self.hSpeedAccuracy = 0.0
    self.hdAccuracy = 0.0


  def decodeMsg(self, row):
    self.timestamp = int(row["timeStamp"])
    self.date = int(row["date"])
    self.fixtype = str(row["P01"])
    self.latitude = float(row["P02"])
    self.longitude = float(row["P03"])
    self.altitude = float(row["P04"])
    self.hdop = float(row["P05"])
    self.vdop = float(row["P06"])
    self.hspeed = float(row["P07"])
    self.course = float(row["P08"])
    self.satsInView = int(row["P09"])
    self.altitudeOnWgs84 = float(row["P10"])
    self.hAccuracy = float(row["P11"])
    self.vAccuracy = float(row["P12"])
    self.hSpeedAccuracy = float(row["P13"])
    self.hdAccuracy = float(row["P14"])