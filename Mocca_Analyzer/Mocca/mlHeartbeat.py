from Mocca_Analyzer.Mocca import Mocca_Object as mo
from Mocca_Analyzer.Mocca.Mocca_Object import Object_Type


class mlHeartbeat(mo.Mocca_Object):
  def __init__(self):
    super().__init__()
    self.type = mo.Object_Type.mlHeartbeat
    self.autopilot = ""
    self.base_mode = ""
    self.custom_mode = ""
    self.system_status = ""
    self.mavlink_version = 0

  def decodeMsg(self, row):
    self.timestamp = int(row["timeStamp"])
    self.date = int(row["date"])
    self.autopilot = str(row["P01"])
    self.base_mode = str(row["P02"])
    self.custom_mode = str(row["P03"])
    self.system_status = str(row["P04"])
    self.mavlink_version = int(row["P05"])