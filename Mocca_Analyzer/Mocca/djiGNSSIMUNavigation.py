from Mocca_Analyzer.Mocca import Mocca_Object as mo

'''
  djiGNSSIMUNavigation; timeStamp; latitude; longitude; altitude; altitudeASL; altitudeWGS84; fixState; heading; roll; pitch; yaw; speedX; speedY; speedZ; accX; accY; accZ; fMode; upQ; downQ
'''

class djiGNSSIMUNavigation(mo.Mocca_Object):
  def __init__(self):
    super().__init__()
    self.type = mo.Object_Type.djiGNSSIMUNavigation
    self.fixState = 0
    self.latitude = 0.0
    self.longitude = 0.0
    self.altitude = 0.0
    self.altitudeASL = 0.0
    self.altitudeWGS84 = 0.0
    self.heading = 0.0
    self.roll = 0.0
    self.pitch = 0.0
    self.yaw = 0.0
    self.speedX = 0.0
    self.speedY = 0.0
    self.speedZ = 0.0
    self.accX = 0.0
    self.accY = 0.0
    self.accZ = 0.0
    self.flightMode = mo.djiFlightMode.FLIGHT_MODE_UNKNOWN
    self.flightStatus = mo.djiFlightStatus.FLIGHT_STATUS_UNKNOWN
    self.uplinkQuality = -1
    self.downlinkQuality = -1
    self.primary = False
    self.tablet = False


  def decodeMsg(self, row):
    self.timestamp = int(row["timeStamp"])
    self.latitude = float(row["P01"])
    self.longitude = float(row["P02"])
    self.altitude = float(row["P03"])
    self.altitudeASL = float(row["P04"])
    self.altitudeWGS84 = float(row["P05"])
    self.fixState = int(row["P06"])
    self.heading = float(row["P07"])
    self.roll = float(row["P08"])
    self.pitch = float(row["P09"])
    self.yaw = float(row["P10"])
    self.speedX = float(row["P11"])
    self.speedY = float(row["P12"])
    self.speedZ = float(row["P13"])
    self.accX = float(row["P14"])
    self.accY = float(row["P15"])
    self.accZ = float(row["P16"])
    self.flightMode = row["P17"]
    self.flightStatus = row["P18"]
    self.uplinkQuality = int(row["P19"])
    self.downlinkQuality = int(row["P20"])
    self.primary = bool(row["P21"])
    self.tablet = bool(row["P22"])

