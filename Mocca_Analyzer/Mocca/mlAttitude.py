from Mocca_Analyzer.Mocca import Mocca_Object as mo
from Mocca_Analyzer.Mocca.Mocca_Object import Object_Type


class mlAttitude(mo.Mocca_Object):
  def __init__(self):
    super().__init__()
    self.type = mo.Object_Type.mlAttitude
    self.roll = 0.0
    self.pitch = 0.0
    self.yaw = 0.0
    self.rollSpeed = 0.0
    self.pitchSpeed = 0.0
    self.yawSpeed = 0.0


  def decodeMsg(self, row):
    self.timestamp = int(row["timeStamp"])
    self.date = int(row["date"])
    self.roll = float(row["P01"])
    self.pitch = float(row["P02"])
    self.yaw = float(row["P03"])
    self.rollSpeed = float(row["P04"])
    self.pitchSpeed = float(row["P05"])
    self.yawSpeed = float(row["P06"])
