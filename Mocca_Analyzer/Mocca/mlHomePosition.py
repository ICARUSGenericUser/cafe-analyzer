from Mocca_Analyzer.Mocca import Mocca_Object as mo
from Mocca_Analyzer.Mocca.Mocca_Object import Object_Type


class mlHomePosition(mo.Mocca_Object):
  def __init__(self):
    super().__init__()
    self.type = mo.Object_Type.mlHomePosition
    self.latitude = 0.0
    self.longitude = 0.0
    self.altitude = 0.0

  def decodeMsg(self, row):
    self.timestamp = int(row["timeStamp"])
    self.date = int(row["date"])
    self.latitude = float(row["P01"])
    self.longitude = float(row["P02"])
    self.altitude = float(row["P03"])