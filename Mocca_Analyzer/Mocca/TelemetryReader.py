import pandas as pd
import numpy as np
import csv
from Mocca_Analyzer.Mocca.Mocca_Object import Object_Type
from Mocca_Analyzer.Mocca import swGNSSNavigation as swg, swBarometric as swb, swInertial as swi
from Mocca_Analyzer.Mocca import mlHeartbeat as mlHeart, mlGlobalPositionInt as mlGlobalP, mlGPSRawInt as mlGPS, mlScaledIMU as mlScaledI, mlHighresIMU as mlHighI, mlVibration as mlVib, mlScaledPressure as mlScaledP, mlAltitude as mlAlt, mlAttitude as mlAtt, mlRangeFinder as mlRange, mlDistanceSensor as mlDist, mlHomePosition as mlHome, mlNavControllerOut as mlNav, mlVfrHud as mlVfr, mlEkfStatusReport as mlEkf, mlSysStatus as mlSys
from Mocca_Analyzer.Mocca import djiGNSSIMUNavigation as djiGNSSIMUNav

sw_columns = [
        "TelCmd",
        "timeStamp",
        "P01",
        "P02",
        "P03",
        "P04",
        "P05",
        "P06",
        "P07",
        "P08",
        "P09",
        "P10",
        "P11",
        "P12",
        "P13",
        "P14",
        "P15",
        "P16",
        "P17",
        "P18",
        "P19"
]


sw_dtype = {
        "TelCmd": str,
        "timeStamp": np.longlong,
}


ml_columns = [
        "TelCmd",
        "date",
        "timeStamp",
        "P01",
        "P02",
        "P03",
        "P04",
        "P05",
        "P06",
        "P07",
        "P08",
        "P09",
        "P10",
        "P11",
        "P12",
        "P13",
        "P14",
        "P15",
        "P16",
        "P17",
        "P18",
        "P19"
]


ml_dtype = {
        "TelCmd": str,
        "timeStamp": np.longlong,
        "date": np.longlong,
}


dji_columns = [
        "TelCmd",
        "timeStamp",
        "P01",
        "P02",
        "P03",
        "P04",
        "P05",
        "P06",
        "P07",
        "P08",
        "P09",
        "P10",
        "P11",
        "P12",
        "P13",
        "P14",
        "P15",
        "P16",
        "P17",
        "P18",
        "P19",
        "P20",
        "P21",
        "P22"
]


dji_dtype = {
        "TelCmd": str,
        "timeStamp": np.longlong,
}


'''
djiGNSSIMUNavigation; timeStamp; latitude; longitude; altitude; altitudeASL; altitudeWGS84; fixState; heading; roll; pitch; yaw; speedX; speedY; speedZ; accX; accY; accZ; fMode; upQ; downQ
djiBattery; timeStamp; capacity; voltage; current; percentage
djiGimbal; timeStamp; roll; pitch; yaw
djiGPSDetails; timeStamp; latitude; longitude; altitude; hdop; pdop; vacc; hacc; sacc; gpsspeed; satnum
'''



def readSWTelemetryFile(full_telemetry_path):

    print("Reading telemetry...")

    tel_df = pd.read_csv(full_telemetry_path, delimiter=';', skiprows=3, names=sw_columns, dtype=sw_dtype)

    sequence = []

    for index, row in tel_df.iterrows():
        msg = None
        if row['TelCmd'] == Object_Type.swGNSSNavigation.name:
            msg = swg.swGNSSNavigation()
            msg.decodeMsg(row)
        elif row['TelCmd'] == Object_Type.swInertial.name:
            msg = swi.swInertial()
            msg.decodeMsg(row)
        elif row['TelCmd'] == Object_Type.swBarometric.name:
            msg = swb.swBarometric()
            msg.decodeMsg(row)

        if msg != None:
            sequence.append(msg)

    return sequence


def readSIMTelemetryFile(full_telemetry_path):

    print("Reading SIM telemetry...")
    file = open(full_telemetry_path)

    sequence = []
    row = file.readline().split(',')
    while(len(row) > 1):

        #Discard bad telemetry
        if len(row) < 10:
            row = file.readline().split(',')
            continue
        msg = None

        if row[9] == 'mavlink_global_position_int_t' and row[13] != '0' and row[15] != '0': #lat and lon !=0. Before the drone takes off, lat,lon are reported as if they were 0
            data = [str(row[11]),"-1", str(row[13]), str(row[15]), str(row[19]), str(row[19]), str(row[21]), str(row[23]), str(row[25]), str(row[27])]
            msg = mlGlobalP.mlGlobalPositionInt()
            msg.decodeSIMMsg(data)

        if msg != None:
            sequence.append(msg)

        row = file.readline().split(',')

    file.close()
    return sequence


def readMLTelemetryFile(full_telemetry_path):

    print("Reading ML telemetry...")

    tel_df = pd.read_csv(full_telemetry_path, delimiter=';', skiprows=16, names=ml_columns, dtype=ml_dtype)

    sequence = []

    for index, row in tel_df.iterrows():
        msg = None
#        if row['TelCmd'] == Object_Type.mlHeartbeat.name:
#            msg = mlHeart.mlHeartbeat()
#            msg.decodeMsg(row)
        if row['TelCmd'] == Object_Type.mlGlobalPositionInt.name:
            msg = mlGlobalP.mlGlobalPositionInt()
            msg.decodeMsg(row)

        elif row['TelCmd'] == Object_Type.mlGPSRawInt.name:
            msg = mlGPS.mlGPSRawInt()
            msg.decodeMsg(row)

        elif row['TelCmd'] == Object_Type.mlVfrHud.name:
            msg = mlVfr.mlVfrHud()
            msg.decodeMsg(row)

        elif row['TelCmd'] == Object_Type.mlAttitude.name:
            msg = mlAtt.mlAttitude()
            msg.decodeMsg(row)

        elif row['TelCmd'] == Object_Type.mlScaledIMU.name:
            msg = mlScaledI.mlScaledIMU()
            msg.decodeMsg(row)

        elif row['TelCmd'] == Object_Type.mlHighresIMU.name:
            msg = mlHighI.mlHighresIMU()
            msg.decodeMsg(row)


        if msg != None:
            sequence.append(msg)

    return sequence


'''     elif row['TelCmd'] == Object_Type.mlGPSRawInt.name:
            msg = mlGPS.mlGPSRawInt()
            msg.decodeMsg(row)
        elif row['TelCmd'] == Object_Type.mlScaledIMU.name:
            msg = mlScaledI.mlScaledIMU()
            msg.decodeMsg(row)
        elif row['TelCmd'] == Object_Type.mlHighresIMU.name:
            msg = mlHighI.mlHighresIMU()
            msg.decodeMsg(row)
        elif row['TelCmd'] == Object_Type.mlVibration.name:
            msg = mlVib.mlVibration()
            msg.decodeMsg(row)
        elif row['TelCmd'] == Object_Type.mlScaledPressure.name:
            msg = mlScaledP.mlScaledPressure()
            msg.decodeMsg(row)
        elif row['TelCmd'] == Object_Type.mlAltitude.name:
            msg = mlAlt.mlAltitude()
            msg.decodeMsg(row)
        elif row['TelCmd'] == Object_Type.mlAttitude.name:
            msg = mlAtt.mlAttitude()
            msg.decodeMsg(row)
        elif row['TelCmd'] == Object_Type.mlRangeFinder.name:
            msg = mlRange.mlRangeFinder()
            msg.decodeMsg(row)
        elif row['TelCmd'] == Object_Type.mlDistanceSensor.name:
            msg = mlDist.mlDistanceSensor()
            msg.decodeMsg(row)
        elif row['TelCmd'] == Object_Type.mlHomePosition.name:
            msg = mlHome.mlHomePosition()
            msg.decodeMsg(row)
        elif row['TelCmd'] == Object_Type.mlNavControllerOut.name:
            msg = mlNav.mlNavControllerOut()
            msg.decodeMsg(row)
        elif row['TelCmd'] == Object_Type.mlVfrHud.name:
            msg = mlVfr.mlVfrHud()
            msg.decodeMsg(row)
        elif row['TelCmd'] == Object_Type.mlEkfStatusReport.name:
            msg = mlEkf.mlEkfStatusReport()
            msg.decodeMsg(row)
        elif row['TelCmd'] == Object_Type.mlSysStatus.name:
            msg = mlSys.mlSysStatus()
            msg.decodeMsg(row)'''


def readDJITelemetryFile(full_telemetry_path):

    print("Reading DJI telemetry...")

#    tel_df = pd.read_csv(full_telemetry_path, delimiter=',', skiprows=range(0, 4), names=dji_columns, dtype=dji_dtype)
    tel_df = pd.read_csv(full_telemetry_path, delimiter=',', skiprows=range(0, 5), index_col=False, header=None, names=dji_columns)

    sequence = []

    for index, row in tel_df.iterrows():
        msg = None
#        if row['TelCmd'] == Object_Type.mlHeartbeat.name:
#            msg = mlHeart.mlHeartbeat()
#            msg.decodeMsg(row)
        if row['TelCmd'] == Object_Type.djiGNSSIMUNavigation.name:
            msg = djiGNSSIMUNav.djiGNSSIMUNavigation()
            msg.decodeMsg(row)

        if msg != None:
            sequence.append(msg)

    return sequence