from Mocca_Analyzer.Mocca import Mocca_Object as mo
from datetime import datetime
from geographiclib import geodesic as geo
import numpy as np
from scipy.spatial import distance


class basicPosition(mo.Mocca_Object):
  def __init__(self):
    super().__init__()
    self.type = mo.Object_Type.basic
    self.latitude = 0.0
    self.longitude = 0.0
    self.altitude = 0.0
    self.altitudeOnWgs84 = 0.0
    self.hspeed = 0.0
    self.vspeed = 0.0
    self.course = 0.0
    self.distance = 0.0


  def decodeMsg(self, row):
    self.timestamp = int(row["Time"])/1000
    self.latitude = float(row["Lat"])
    self.longitude = float(row["Lon"])
    self.altitude = float(row["Alt"])


  def decodeCBMsg(self, row):
    dt_obj = datetime.strptime(row["DateTime"], '%Y-%m-%d %H:%M:%S')
    self.timestamp = int(dt_obj.timestamp())
    self.latitude = float(row["Lat"])
    self.longitude = float(row["Lon"])
    self.altitude = float(row["Alt"])



  def complementSpeeds(self, prevSample):
    if prevSample != None:
      if self.timestamp - prevSample.timestamp == 0:
        self.vspeed = prevSample.vspeed
        self.hspeed = prevSample.hspeed
      else:
        self.vspeed = (self.altitude - prevSample.altitude)/(self.timestamp - prevSample.timestamp)
        self.hspeed = self.getDistance(prevSample) / (self.timestamp - prevSample.timestamp)

      geoVector = geo.Geodesic.WGS84.Inverse(prevSample.latitude, prevSample.longitude, self.latitude, self.longitude)

      self.course = geoVector['azi1']
      self.distance = geoVector['s12']

