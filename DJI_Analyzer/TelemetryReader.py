import pandas as pd
import datetime
from datetime import timezone
import numpy as np
from Projection import FlatEarthProjection as proj
from Encounter import units as units
from Encounter import position as pos, encounter as enc


dji_columns = [
        "time",
        "datetime",
        "Lat",
        "Lon",
        "HAT",  # height_above_takeoff(meters)
        "HAG",  # height_above_ground_at_drone_location(meters)
        "DGE",  # ground_elevation_at_drone_location(meters)
        "ASL",  # altitude_above_seaLevel(meters)
        "HS",   # height_sonar
        "speed",  # speed(m/s)
        "distance",
        "satellites",
        "gpslevel",
        "voltage",
        "max_altitude",
        "max_ascent",
        "max_speed",
        "max_distance",
        "xSpeed",
        "ySpeed",
        "zSpeed",
        "heading",
        "pitch",
        "roll"
]


dji_dtype = {
        "time": int,
        "datetime": str,
}

def timestamp(dt):
    return int(dt.replace(tzinfo=timezone.utc).timestamp()*1000)


def readDJITelemetryFile(full_telemetry_path1, full_telemetry_path2):

    print("Reading DJI telemetry...")

    # We create the encounter and we fill it with the data from the data files
    # Note that a coordinate transformation is still necessary to a local coordinate system
    encounter = enc.Encounter(1, 0, 0, 0, 0, "dji")

    # In order to perform the coordinate system transformation we seek a centre position averaged
    # from all positions that occur along both own ship and intruder
    nSample = 0; meanLat = 0; meanLon = 0


    #tel_df = pd.read_csv(full_telemetry_path, delimiter=';', skiprows=1, names=dji_columns, dtype=dji_dtype)
    tel_df1 = pd.read_csv(full_telemetry_path1, delimiter=',', skiprows=1, index_col=False, header=None, names=dji_columns)

    first_row = tel_df1.iloc[0]
    #base_ms = first_row["time"]
    base_ms = 0
    base_date_time1 = datetime.datetime.strptime(first_row["datetime"], '%Y-%m-%d %H:%M:%S')
    base_date_time = base_date_time1

    for index, row in tel_df1.iterrows():
        date_time = datetime.datetime.strptime(row["datetime"], '%Y-%m-%d %H:%M:%S')

        if (date_time > base_date_time):
            base_ms = 0
            base_date_time = date_time
        else:
            base_ms += 100

        timestampms = timestamp(date_time) + base_ms
        position = pos.Position(index, timestampms, 0, 0, 0)
        position.setLocation(row['Lon'], row['Lat'], row['ASL'])
        position.assignDynamics(row['xSpeed'], row['ySpeed'], row['zSpeed'], row['heading'])
        encounter.addAircraft1Position(position)
        nSample += 1
        meanLat += row['Lat']
        meanLon += row['Lon']

    # tel_df = pd.read_csv(full_telemetry_path, delimiter=';', skiprows=1, names=dji_columns, dtype=dji_dtype)
    tel_df2 = pd.read_csv(full_telemetry_path2, delimiter=',', skiprows=1, index_col=False, header=None, names=dji_columns)

    first_row = tel_df2.iloc[0]
    #base_ms = first_row["time"]
    base_ms = 0
    base_date_time2 = datetime.datetime.strptime(first_row["datetime"], '%Y-%m-%d %H:%M:%S')
    base_date_time = base_date_time2

    for index, row in tel_df2.iterrows():
        date_time = datetime.datetime.strptime(row["datetime"], '%Y-%m-%d %H:%M:%S')

        if (date_time > base_date_time):
            base_ms = 0
            base_date_time = date_time
        else:
            base_ms += 100

        timestampms = timestamp(date_time) + base_ms
        position = pos.Position(index, timestampms, 0, 0, 0)
        position.setLocation(row['Lon'], row['Lat'], row['ASL'])
        position.assignDynamics(row['xSpeed'], row['ySpeed'], row['zSpeed'], row['heading'])
        encounter.addAircraft1Position(position)
        nSample += 1
        meanLat += row['Lat']
        meanLon += row['Lon']

    encounter.validateDynamicsLatLon()

    # Now we compute the average and set up the coordinate transformation
    meanLat = meanLat / nSample
    meanLon = meanLon / nSample

    projection = proj.FlatEarthProjection(meanLon, meanLat, 0)

    # print("Checking positions: ")
    for position in encounter.aircraft1:
        # print("Current position Lon: ", position.x_loc, " Lan: ", position.y_loc)
        position.updateCoordinateSystem(projection)
        # print("Updated position X: ", position.x_loc, " Y: ", position.y_loc)

    # print("Checking positions: ")
    for position in encounter.aircraft2:
        # print("Current position Lon: ", position.x_loc, " Lan: ", position.y_loc)
        position.updateCoordinateSystem(projection)
        # print("Updated position X: ", position.x_loc, " Y: ", position.y_loc)

    encounter.validateDynamicsXY()

    encounter.computeDynamicsFactor(0.1)

    if base_date_time1 < base_date_time2:
        idx = 0
        while idx < len(encounter.aircraft1) - 1:
            position = encounter.aircraft1[idx]
            next_position = encounter.aircraft1[idx + 1]

            if (position.violates <= encounter.aircraft2[10].violates) and (next_position.violates >= encounter.aircraft2[10].violates):
                gotIt = True
            idx += 1
    else:
        idx = 0
        while idx < len(encounter.aircraft2) - 1:
            position = encounter.aircraft2[idx]
            next_position = encounter.aircraft2[idx + 1]

            if (position.violates <= encounter.aircraft1[10].violates) and (next_position.violates >= encounter.aircraft1[10].violates):
                gotIt = True
            idx += 1


    return encounter


