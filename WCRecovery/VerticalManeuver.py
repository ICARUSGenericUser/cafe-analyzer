from RWC import RWC_thresholds
from Encounter import position
# min and max are minimum and maximum altitude, c is altitude step
def compute_vertical_maneuver(own_pos, int_pos, v_params, k, B, T, rwc_param, debug):
    new_altitude = -1
    v_cost = 1000
    i = 0
    while (i * v_params.c + v_params.min) <= v_params.max:
        new_pos = position.Position(0, own_pos.x_loc, own_pos.y_loc, own_pos.altitude)
        v_speed = vertical_speed_change(new_altitude, own_pos, T)
        new_pos.assignDynamics(own_pos.x_v, own_pos.y_v, v_speed, 0)
        interval = RWC_thresholds.WCV_interval(new_pos, int_pos, B, T, rwc_param, debug)
        if ((interval.up - interval.low)*k + abs(new_altitude - own_pos.altitude)*v_params.k) < v_cost:
            v_cost = (interval.up - interval.low)*k + abs(new_altitude - own_pos.altitude)*v_params.k
            new_altitude = i * v_params.c + v_params.min
        i = i + 1
    #print("new altitude = " + str(new_altitude) + ", v_cost = " + str(v_cost))
    return new_altitude, v_cost


def vertical_speed_change(new_altitude, own_pos, T):
    v_speed = (new_altitude - own_pos.altitude) / T
    return v_speed



