from RWC import RWC_thresholds
import math
from Encounter import position

def compute_speed_maneuver(own_pos, int_pos, s_params, k, B, T, rwc_param, debug):
    i = 0
    s_cost = 1000
    new_speed = - 1
    current_speed = speed_module(own_pos)
    while (i * s_params.c + s_params.min) <= s_params.max:
        new_pos = position.Position(0, own_pos.x_loc, own_pos.y_loc, own_pos.altitude)
        x_v, y_v, v_speed = speed_change(own_pos, i * s_params.c + s_params.min)
        new_pos.assignDynamics(x_v, y_v, v_speed, 0)
        interval = RWC_thresholds.WCV_interval(new_pos, int_pos, B, T, rwc_param, debug)
        if ((interval.up - interval.low)*k + abs(i * s_params.c + s_params.min - current_speed)*s_params.k) < abs(s_cost):
            s_cost = ((interval.up - interval.low)*k + abs(i * s_params.c + s_params.min - current_speed)*s_params.k)
            new_speed = i * s_params.c + s_params.min
        i = i + 1
    return new_speed, s_cost


def speed_module(own_pos):
    speed = math.sqrt(own_pos.x_v ** 2 + own_pos.y_v ** 2 + own_pos.v_speed ** 2)
    return speed


def speed_change(own_pos, new_speed):
    current_speed = speed_module(own_pos)
    x_v = own_pos.x_v * new_speed / current_speed
    y_v = own_pos.y_v * new_speed / current_speed
    v_speed = own_pos.v_speed * new_speed / current_speed
    return x_v, y_v, v_speed