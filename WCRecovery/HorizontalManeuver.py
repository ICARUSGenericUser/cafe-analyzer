from RWC import RWC_thresholds
from Encounter import position
import math


# min and max are minimum and maximum yaw angles deviation, c is yaw angle step
def compute_horizontal_maneuver(own_pos, int_pos, h_params, k, B, T, rwc_param, debug):
    i = 0
    yaw_change = 1000
    h_cost = 1000
    while (i * h_params.c + h_params.min) <= h_params.max:
        new_pos = position.Position(0, own_pos.x_loc, own_pos.y_loc, own_pos.altitude)
        x_v, y_v = direction_change(own_pos, i * h_params.c + h_params.min)
        new_pos.assignDynamics(x_v, y_v, own_pos.v_speed, 0)
        interval = RWC_thresholds.WCV_interval(new_pos, int_pos, B, T, rwc_param, debug)
        #print(str(interval.low) + " - " + str(interval.up))
        if ((interval.up - interval.low)*k + abs(i * h_params.c + h_params.min)*h_params.k) < h_cost:
            h_cost = (interval.up - interval.low)*k + abs(i * h_params.c + h_params.min)*h_params.k
            yaw_change = i * h_params.c + h_params.min
            #print("yaw_change = " + str(yaw_change) + ", cost = " + str(h_cost) + ", interval: " + str(interval.low) + " - " + str(interval.up))
        i = i + 1
    #print(str(own_pos.x_v))
    return yaw_change, h_cost


def direction_change(own_pos, yaw):
    yaw = yaw*math.pi/180
    x_v = own_pos.x_v*math.cos(yaw) + own_pos.y_v*math.sin(yaw)
    y_v = - own_pos.x_v*math.sin(yaw) + own_pos.y_v*math.cos(yaw)
    return x_v, y_v