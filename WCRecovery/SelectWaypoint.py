from WCRecovery import SelectManeuver, HorizontalManeuver, VelocityManeuver, VerticalManeuver
import math
from Encounter import position

# To recover the original track we will look for the closest waypoint from the original flight plan
def compute_closest_waypoint(waypoints, own_pos, WCV_time):
    lowest_distance = 1000
    closest_waypoint = 0
    i = 0
    while i < len(waypoints):
        distance = math.sqrt((waypoints[i][1] - own_pos.x_loc)**2 + (waypoints[i][2] - own_pos.y_loc)**2 + (waypoints[i][3] - own_pos.altitude)**2)
        if lowest_distance > distance and WCV_time < waypoints[i][0]:
            lowest_distance = distance
            closest_waypoint = i
        i = i + 1
    #print(str(lowest_distance))
    if lowest_distance <= 1:
        closest_waypoint += 1
    return waypoints[closest_waypoint]

#comptes next waypoint for a given maneuver
def compute_next_intern_waypoint(own_pos, maneuver_type, maneuver, time, actual_time, time_interval):
    new_pos = position.Position(0, own_pos.x_loc, own_pos.y_loc, own_pos.altitude)
    if maneuver_type == 0:
        x_v, y_v = HorizontalManeuver.direction_change(own_pos, maneuver)
        new_pos.assignDynamics(x_v, y_v, own_pos.v_speed, 0)
    elif maneuver_type == 1:
        v_speed = VerticalManeuver.vertical_speed_change(maneuver, own_pos, time)
        new_pos.assignDynamics(own_pos.x_v, own_pos.y_v, v_speed, 0)
    else:
        x_v, y_v, v_speed = VelocityManeuver.speed_change(own_pos, maneuver)
        new_pos.assignDynamics(x_v, y_v, v_speed, 0)
    return compute_next_position_with_constant_speed(new_pos, time_interval, actual_time)

# computes next position for  continuos speed and a given time interval T
def compute_next_position_with_constant_speed(own_pos, T, actual_time):
    x_loc = own_pos.x_loc + own_pos.x_v * T
    y_loc = own_pos.y_loc + own_pos.y_v * T
    altitude = own_pos.altitude + own_pos.v_speed * T
    time = T + actual_time
    position = [time, x_loc, y_loc, altitude, own_pos.x_v, own_pos.y_v, own_pos.v_speed]
    return position

def compute_next_data_with_constant_speed(own_data, T, actual_time):
    x_loc = own_data[1] + own_data[4] * T
    y_loc = own_data[2] + own_data[5] * T
    altitude = own_data[3] + own_data[6] * T
    time = T + actual_time
    position = [time, x_loc, y_loc, altitude, own_data[4], own_data[5], own_data[6]]
    return position

def compute_new_own_data(own_pos, wp, actual_time, time_interval):
    current_speed = VelocityManeuver.speed_module(own_pos)
    distance = math.sqrt((own_pos.x_loc - wp[1])**2 + (own_pos.y_loc - wp[2])**2 + (own_pos.altitude - wp[3])**2)
    time = distance / current_speed
    x_v = (wp[1] - own_pos.x_loc) / time
    y_v = (wp[2] - own_pos.y_loc) / time
    v_speed = (wp[3] - own_pos.altitude) / time
    new_own_data = [actual_time, own_pos.x_loc, own_pos.y_loc, own_pos.altitude, x_v, y_v, v_speed]
    return new_own_data