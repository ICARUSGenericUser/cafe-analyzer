from WCRecovery import HorizontalManeuver, VerticalManeuver, VelocityManeuver, ManeuverParameters
from Encounter import position

class maneuver():
    def __init__(self, maneuver_type, maneuver_parameter):
        self.maneuver_type = maneuver_type
        self.maneuver = maneuver_parameter


def compute_position(own_pos, maneuver, T):
    own_pos1 = position.Position(0, own_pos.x_loc, own_pos.y_loc, own_pos.altitude)
    if maneuver.maneuver_type == 0:
        x_v, y_v = HorizontalManeuver.direction_change(own_pos, maneuver.maneuver)
        own_pos1.assignDynamics(x_v, y_v, own_pos.v_speed, 0)
    elif maneuver.maneuver_type == 1:
        v_speed = VerticalManeuver.vertical_speed_change(maneuver.maneuver, own_pos, T)
        own_pos1.assignDynamics(own_pos.x_v, own_pos.y_v, v_speed, 0)
    else:
        x_v, y_v, v_speed = VelocityManeuver.speed_change(own_pos, maneuver.maneuver)
        own_pos1.assignDynamics(x_v, y_v, v_speed, 0)
    return own_pos1


# this function will return the lower cost maneuver, where 0 is horizontal, 1 is vertical and 2 is speed
def compute_maneuver(own_pos, int_pos, h_params, v_params, s_params, k, B, T, rwc_param, debug):
    # compute each from the thre maneuvers possible
    h_maneuver = HorizontalManeuver.compute_horizontal_maneuver(own_pos, int_pos, h_params, k, B, T, rwc_param, debug)
    v_maneuver = VerticalManeuver.compute_vertical_maneuver(own_pos, int_pos, v_params, k, B, T, rwc_param, debug)
    s_maneuver = VelocityManeuver.compute_speed_maneuver(own_pos, int_pos, s_params, k, B, T, rwc_param, debug)
    #print("h_cost = "+str(h_maneuver[1])+", v_cost = "+str(v_maneuver[1])+", s_cost = "+str(s_maneuver[1]))
    if h_maneuver[1] <= v_maneuver[1] and h_maneuver[1] <= s_maneuver[1]:
        return maneuver(0, h_maneuver[0])
    elif v_maneuver[1] <= s_maneuver[1]:
        return maneuver(1, v_maneuver[0])
    else:
        return maneuver(2, s_maneuver[0])


def compute_cost_maneuver(maneuver, params):
    cost = params.k * abs(maneuver)/(params.max - params.min)
    return cost


