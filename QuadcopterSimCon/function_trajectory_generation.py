# -*- coding: utf-8 -*-
"""
author: John Bass
email: john.bobzwik@gmail.com
license: MIT
Please feel free to use and modify this, but keep the above information. Thanks!
"""

import numpy as np
import time

from QuadcopterSimCon.trajectory import Trajectory
from QuadcopterSimCon.ctrl import Control
from QuadcopterSimCon.quadFiles.quad import Quadcopter
from QuadcopterSimCon.utils.windModel import Wind
import QuadcopterSimCon.utils as utils


def quad_sim(t, Ts, quad, ctrl, wind, traj, i, simulated_waypoints):
    
    # Dynamics (using last timestep's commands)
    # ---------------------------
    quad.update(t, Ts, ctrl.w_cmd, wind)
    t = round(t + Ts, 2)

    # Trajectory for Desired States 
    # ---------------------------
    sDes = traj.desiredState(t, Ts, quad, i, simulated_waypoints)

    # Generate Commands (for next iteration)
    # ---------------------------
    ctrl.controller(traj, quad, sDes, Ts)

    # Generated waypoints vs simulated waypoints
    # ---------------------------

    return t


def generateTrajectory(trajectory, figuresFilepath, droneParametersSet, droneName):

    start_time = time.time()

    # Simulation Setup
    # --------------------------- 
    Ti = 0
    Ts = 0.02
    Tf = trajectory.wpSequence[len(trajectory.wpSequence)-1, 3]


    ifsave = 0

    # Choose trajectory settings
    # --------------------------- 
    ctrlOptions = ["xyz_pos", "xy_vel_z_pos", "xyz_vel"]
    trajSelect = np.zeros(3)

    # Select Control Type             (0: xyz_pos,                  1: xy_vel_z_pos,            2: xyz_vel)
    ctrlType = ctrlOptions[0]
    # Select Position Trajectory Type (0: hover,                    1s: pos_waypoint_timed,      2: pos_waypoint_interp,
    #                                  3s: minimum velocity          4s: minimum accel,           5s: minimum jerk,           6s: minimum snap
    #                                  7s: minimum accel_stop        8: minimum jerk_stop        9s: minimum snap_stop
    #                                 10: minimum jerk_full_stop   11: minimum snap_full_stop
    #                                 12s: pos_waypoint_arrived
    trajSelect[0] = 2
    # Select Yaw Trajectory Type      (0: none                      1: yaw_waypoint_timed,      2: yaw_waypoint_interp     3: follow          4: zero)
    trajSelect[1] = 0
    # Select if waypoint time is used, or if average speed is used to calculate waypoint time   (0: waypoint time,   1: average speed)
    trajSelect[2] = 0
    print("Control type: {}".format(ctrlType))

    # Write WP matrices into file
    # ---------------------------

    n = len(trajectory.wpSequence)
    wp = trajectory.wpSequence[:, [0, 1, 2]]
    t = trajectory.wpSequence[:, 3]
    yaw = np.zeros(n, dtype=float)

    f = open(figuresFilepath + "\\waypointSequence.wp", "a")
    f.write("Waypoint generator Waypoints:\n")
    f.write(repr(np.round(wp, 5)))
    f.write("\n\nWaypoint generator Time:\n")
    f.write(repr(np.round(t, 3)))
    f.write("\n\nWaypoint generator Yaw:\n")
    f.write(repr(np.round(yaw, 3)))
    f.close()


    # Initialize Quadcopter, Controller, Wind, Result Matrixes
    # ---------------------------
    quad = Quadcopter(Ti, trajectory.wpSequence, droneParametersSet, droneName)
    traj = Trajectory(quad, ctrlType, trajSelect, trajectory.wpSequence)
    ctrl = Control(quad, traj.yawType,droneParametersSet, droneName)
    wind = Wind('None', 2.0, 90, -15)

    # Trajectory for First Desired States
    # ---------------------------
    i = 1
    # This matrix stores the index within the wps array, position, speed and time of each waypoint when it is considered as "arrived"
    trajectory.flownSequence = np.zeros((n, 5))
    sDes = traj.desiredState(0, Ts, quad, i, trajectory.flownSequence)

    # Generate First Commands
    # ---------------------------
    ctrl.controller(traj, quad, sDes, Ts)
    
    # Initialize Result Matrixes
    # ---------------------------
    numTimeStep = int(Tf/Ts+2)

    t_all          = np.zeros([numTimeStep, 1])
    s_all          = np.zeros([numTimeStep, len(quad.state)])
    pos_all        = np.zeros([numTimeStep, len(quad.pos)])
    vel_all        = np.zeros([numTimeStep, len(quad.vel)])
    quat_all       = np.zeros([numTimeStep, len(quad.quat)])
    omega_all      = np.zeros([numTimeStep, len(quad.omega)])
    euler_all      = np.zeros([numTimeStep, len(quad.euler)])
    sDes_traj_all  = np.zeros([numTimeStep, len(traj.sDes)])
    sDes_calc_all  = np.zeros([numTimeStep, len(ctrl.sDesCalc)])
    w_cmd_all      = np.zeros([numTimeStep, len(ctrl.w_cmd)])
    wMotor_all     = np.zeros([numTimeStep, len(quad.wMotor)])
    thr_all        = np.zeros([numTimeStep, len(quad.thr)])
    tor_all        = np.zeros([numTimeStep, len(quad.tor)])

    t_all[0,:]          = np.array([Ti])
    s_all[0,:]          = quad.state
    pos_all[0,:]        = quad.pos
    vel_all[0,:]        = quad.vel
    quat_all[0,:]       = quad.quat
    omega_all[0,:]      = quad.omega
    euler_all[0,:]      = quad.euler
    sDes_traj_all[0,:]  = traj.sDes
    sDes_calc_all[0,:]  = ctrl.sDesCalc
    w_cmd_all[0,:]      = ctrl.w_cmd
    wMotor_all[0,:]     = quad.wMotor
    thr_all[0,:]        = quad.thr
    tor_all[0,:]        = quad.tor

    # Run Simulation
    # ---------------------------
    t = round(Ti, 2)
    while round(t, 2) < Tf:
        
        t = quad_sim(t, Ts, quad, ctrl, wind, traj, i, trajectory.flownSequence)
        
        # print("{:.3f}".format(t))
        t_all[i,:]             = np.array([t])
        s_all[i,:]           = quad.state
        pos_all[i,:]         = quad.pos
        vel_all[i,:]         = quad.vel
        quat_all[i,:]        = quad.quat
        omega_all[i,:]       = quad.omega
        euler_all[i,:]       = quad.euler
        sDes_traj_all[i,:]   = traj.sDes
        sDes_calc_all[i,:]   = ctrl.sDesCalc
        w_cmd_all[i,:]       = ctrl.w_cmd
        wMotor_all[i,:]      = quad.wMotor
        thr_all[i,:]         = quad.thr
        tor_all[i,:]         = quad.tor
        
        i += 1
    
    end_time = time.time()
    print("Simulated {:.2f}s in {:.6f}s.".format(t, end_time - start_time))

    # Show in command line the simulated waypoints
    print("\nTrajectory Simulator idx,t,pos_x,pos_y,pos_z:\n" + repr(trajectory.flownSequence))

    trajectory.trajectorySequence = np.concatenate((t_all, pos_all, vel_all), axis=1)

    # View Results
    # ---------------------------

    # utils.fullprint(sDes_traj_all[:,3:6])
    #utils.makeFigures(quad.params, t_all, pos_all, vel_all, quat_all, omega_all, euler_all, w_cmd_all, wMotor_all, thr_all, tor_all, sDes_traj_all, sDes_calc_all)
    # ani = utils.sameAxisAnimation(t_all, traj.wps, pos_all, quat_all, sDes_traj_all, Ts, quad.params, traj.xyzType, traj.yawType, ifsave)
    #plt.show()

