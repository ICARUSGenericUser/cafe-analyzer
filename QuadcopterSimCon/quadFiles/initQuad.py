# -*- coding: utf-8 -*-
"""
author: John Bass
email: john.bobzwik@gmail.com
license: MIT
Please feel free to use and modify this, but keep the above information. Thanks!
"""

import numpy as np
from numpy import pi
from numpy.linalg import inv
import QuadcopterSimCon.utils as utils
import QuadcopterSimCon.config as config
import json, ast


def load_params_from_file(filename):

    print("Loading vehicle parameters from file: ", filename)
    # Opening JSON file
    f = open(filename, "r")
    # returns JSON object as a dictionary
    paramsTable = json.load(f)

    return paramsTable


def set_sys_params(paramsTable, droneName):

    params = {}

    drone = paramsTable[droneName]
    params["mB"] = float(drone['mB'])                                       # mass (kg)
    params["g"]  = float(drone['g'])                                        # gravity (m/s/s)
    params["dxm"] = float(drone['dxm'])                                     # arm length (m)
    params["dym"] = float(drone['dym'])                                     # arm length (m)
    params["dzm"]  = float(drone['dzm'])                                    # motor height (m)
    params["IB"]  = np.vstack(ast.literal_eval(drone['IB']))                # Inertial tensor (kg*m^2)
    params["invI"] = inv(params["IB"])
    params["IRzz"]  = float(drone['IRzz'])                                  # Rotor moment of inertia (kg*m^2)
    params["useIntergral"] = bool(False)                                    # Include integral gains in linear velocity control
    # params["interpYaw"] = bool(False)                                     # Interpolate Yaw setpoints in waypoint trajectory
    params["Cd"] = float(drone['Cd'])
    params["kTh"] = float(drone['kTh'])                                     # thrust coeff (N/(rad/s)^2)  (1.18e-7 N/RPM^2)
    params["kTo"] = float(drone['kTo'])                                     # torque coeff (Nm/(rad/s)^2)  (1.79e-9 Nm/RPM^2)
    params["mixerFM"] = makeMixerFM(params)                                 # Make mixer that calculated Thrust (F) and moments (M) as a function on motor speeds
    params["mixerFMinv"] = inv(params["mixerFM"])
    params["minThr"] = float(drone['minThr'])                               # Minimum total thrust
    params["maxThr"] = float(drone['maxThr'])                               # Maximum total thrust
    params["minWmotor"] = float(drone['minWmotor'])                         # Minimum motor rotation speed (rad/s)
    params["maxWmotor"] = float(drone['maxWmotor'])                         # Maximum motor rotation speed (rad/s)
    params["tau"] = float(drone['tau'])                                     # Value for second order system for Motor dynamics
    params["kp"] = float(drone['kp'])                                       # Value for second order system for Motor dynamics
    params["damp"] = float(drone['damp'])                                   # Value for second order system for Motor dynamics
    params["motorc1"] = float(drone['motorc1'])                             # w (rad/s) = cmd*c1 + c0 (cmd in %)
    params["motorc0"] = float(drone['motorc0'])
    params["motordeadband"] = float(drone['motordeadband'])

    return params


def makeMixerFM(params):
    dxm = params["dxm"]
    dym = params["dym"]
    kTh = params["kTh"]
    kTo = params["kTo"] 

    # Motor 1 is front left, then clockwise numbering.
    # A mixer like this one allows to find the exact RPM of each motor 
    # given a desired thrust and desired moments.
    # Inspiration for this mixer (or coefficient matrix) and how it is used : 
    # https://link.springer.com/article/10.1007/s13369-017-2433-2 (https://sci-hub.tw/10.1007/s13369-017-2433-2)
    if (config.orient == "NED"):
        mixerFM = np.array([[    kTh,      kTh,      kTh,      kTh],
                            [dym*kTh, -dym*kTh,  -dym*kTh, dym*kTh],
                            [dxm*kTh,  dxm*kTh, -dxm*kTh, -dxm*kTh],
                            [   -kTo,      kTo,     -kTo,      kTo]])
    elif (config.orient == "ENU"):
        mixerFM = np.array([[     kTh,      kTh,      kTh,     kTh],
                            [ dym*kTh, -dym*kTh, -dym*kTh, dym*kTh],
                            [-dxm*kTh, -dxm*kTh,  dxm*kTh, dxm*kTh],
                            [     kTo,     -kTo,      kTo,    -kTo]])
    
    
    return mixerFM

def init_cmd(params):
    mB = params["mB"]
    g = params["g"]
    kTh = params["kTh"]
    kTo = params["kTo"]
    c1 = params["motorc1"]
    c0 = params["motorc0"]
    
    # w = cmd*c1 + c0   and   m*g/4 = kTh*w^2   and   torque = kTo*w^2
    thr_hover = mB*g/4.0
    w_hover   = np.sqrt(thr_hover/kTh)
    tor_hover = kTo*w_hover*w_hover
    cmd_hover = (w_hover-c0)/c1
    return [cmd_hover, w_hover, thr_hover, tor_hover]

def init_state(params,wp_list):
    
    x0     = wp_list[0,0]  # m
    y0     = wp_list[0,1]  # m
    z0     = wp_list[0,2]  # m
    phi0   = 0.  # rad
    theta0 = 0.  # rad
    psi0   = 0.  # rad

    quat = utils.YPRToQuat(psi0, theta0, phi0)
    
    if (config.orient == "ENU"):
        z0 = -z0

    s = np.zeros(21)
    s[0]  = x0       # x
    s[1]  = y0       # y
    s[2]  = z0       # z
    s[3]  = quat[0]  # q0
    s[4]  = quat[1]  # q1
    s[5]  = quat[2]  # q2
    s[6]  = quat[3]  # q3
    s[7]  = 0.       # xdot
    s[8]  = 0.       # ydot
    s[9]  = 0.       # zdot
    s[10] = 0.       # p
    s[11] = 0.       # q
    s[12] = 0.       # r

    w_hover = params["w_hover"] # Hovering motor speed
    wdot_hover = 0.              # Hovering motor acc

    s[13] = w_hover
    s[14] = wdot_hover
    s[15] = w_hover
    s[16] = wdot_hover
    s[17] = w_hover
    s[18] = wdot_hover
    s[19] = w_hover
    s[20] = wdot_hover
    
    return s


def quadPerformances(params):
    # This function returns the maximum horizontal and vertical speed, climb and descent rate of a quadcopter

    # Maximum descent rate: free fall (D=W)
    # -------------------------------------
    # Terminal Velocity = maximum descent rate (m/s)
    rho = 1.225 # Sea Level air density (kg/m^3)
    max_descent_rate = np.sqrt((2*params['mB']*params['g'])/(params['Cd']*rho*params['dxm']*params['dym']))

    # Maximum climb rate: vertical climb at maximum thrust (Thrustmax - D - W = 0)
    # ---------------------------------------
    max_climb_rate = np.sqrt((2*params['maxThr'])/(rho*params['Cd']*params['dxm']*params['dym']*params['g']*params['mB']))

    return max_descent_rate, max_climb_rate