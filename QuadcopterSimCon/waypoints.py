# -*- coding: utf-8 -*-
"""
author: John Bass
email: john.bobzwik@gmail.com
license: MIT
Please feel free to use and modify this, but keep the above information. Thanks!
"""

import numpy as np
from numpy import pi
import QuadcopterSimCon.config as config

deg2rad = pi/180.0

def makeWaypoints(wp_list):
    t_ini = 0
    yaw_ini = 0
    v_average = 10 #1.6

    n = len(wp_list)

    wp = wp_list[:, [0, 1, 2]]
    t = wp_list[:, 3]
    yaw = np.zeros(n, dtype=float)
    wp_ini = wp[0]

    t = np.hstack((t_ini, t)).astype(float)
    wp = np.vstack((wp_ini, wp)).astype(float)
    yaw = np.hstack((yaw_ini, yaw)).astype(float)*deg2rad


    # Delete repeated value in the first place
    t = np.delete(t,(0), axis=0)
    wp = np.delete(wp, (0), axis=0)
    yaw = np.delete(yaw,(0), axis=0)


    return t, wp, yaw, v_average
