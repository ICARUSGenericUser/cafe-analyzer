# This is a sample Python script.
import os, shutil
import sys
import csv
from time import time
import pandas as pd
import numpy as np
from Encounter import units as units
from Encounter import position as pos, encounter as enc, encounter_task as et
from Encounter_Distribution import Encounter_Parameters as ep
from NoiseModel import Noise_parameters as npa, Noise_insertion as ni

#df_dtype = {
#        "Encounter_Distribution No.": int,
#        "Seed": int,
#        "Layer": int,
#        "VMD": np.float64,
#        "HMD": np.float64,
#        "Approach Angle": np.float64,
#        "Bearing from North": np.float64,
#        "Time before CPA": int,
#        "Time after CPA": int,
#        "AC1 Mode A": str,
#        "AC1 Callsign": str,
#        "AC1 Class": int,
#        "AC1 Controlled?": bool,
#        "AC1 Altitude": np.float64,
#        "AC1 Vertical": np.float64,
#        "AC1 Speed": np.float64,
#        "AC1 Acceleration": np.float64,
#        "AC1 Heading": np.float64,
#        "AC1 Turn": np.float64,
#        "AC2 Mode A": str,
#        "AC2 Callsign": str,
#        "AC2 Class": int,
#        "AC2 Controlled?": bool,
#        "AC2 Altitude": np.float64,
#        "AC2 Vertical": np.float64,
#        "AC2 Speed": np.float64,
#        "AC2 Acceleration": np.float64,
#        "AC2 Heading": np.float64,
#        "AC2 Turn": np.float64,
#}


df_dtype = {
        "Encounter No.": np.float64,
        "Seed": np.float64,
        "Layer": np.float64,
        "VMD": np.float64,
        "HMD": np.float64,
        "Approach Angle": np.float64,
        "Bearing from North": np.float64,
        "Time before CPA": np.float64,
        "Time after CPA": np.float64,
        "AC1 Mode A": str,
        "AC1 Callsign": str,
        "AC1 Class": np.float64,
        "AC1 Controlled?": bool,
        "AC1 Altitude": np.float64,
        "AC1 Vertical Rate": np.float64,
        "AC1 Speed": np.float64,
        "AC1 Acceleration": np.float64,
        "AC1 Heading": np.float64,
        "AC1 Turn": np.float64,
        "AC2 Mode A": str,
        "AC2 Callsign": str,
        "AC2 Class": np.float64,
        "AC2 Controlled?": bool,
        "AC2 Altitude": np.float64,
        "AC2 Vertical Rate": np.float64,
        "AC2 Speed": np.float64,
        "AC2 Acceleration": np.float64,
        "AC2 Heading": np.float64,
        "AC2 Turn": np.float64,
}


trj_eu_dtype = {
        "time": str,
        "aircraft":  np.int32,
        "Mode A": np.int32,
        "x-position": np.float64,
        "y-position": np.float64,
        "altitude": np.float64,
        "callsign": str,
}


trj_ftd_dtype = {
        "time": str,
        "aircraft":  np.int32,
        "x-position": np.float64,
        "y-position": np.float64,
        "altitude": np.float64,
        "callsign": str,
}


trj_eu_columns = [
        "time",
        "aircraft",
        "Mode A",
        "x-position",
        "y-position",
        "altitude",
        "callsign",
]


trj_ftd_columns = [
        "time",
        "aircraft",
        "x-position",
        "y-position",
        "altitude",
        "BDS-30",
]

df_columns = [
        "Encounter No.",
        "Seed",
        "Layer",
        "VMD",
        "HMD",
        "Approach Angle",
        "Bearing from North",
        "Time before CPA",
        "Time after CPA",
        "AC1 Mode A",
        "AC1 Callsign",
        "AC1 Class",
        "AC1 Controlled?",
        "AC1 Altitude",
        "AC1 Vertical",
        "AC1 Speed",
        "AC1 Acceleration",
        "AC1 Heading",
        "AC1 Turn",
        "AC2 Mode A",
        "AC2 Callsign",
        "AC2 Class",
        "AC2 Controlled?",
        "AC2 Altitude",
        "AC2 Vertical",
        "AC2 Speed",
        "AC2 Acceleration",
        "AC2 Heading",
        "AC2 Turn"
]



def writeTrajectoryFile(filepath, encTask, encounter, aircraftIdx):
    # eu_type =
    #    "time": str,
    #    "aircraft": np.int32,
    #    "Mode A": np.int32,
    #    "x-position": np.float64,
    #    "y-position": np.float64,
    #    "altitude": np.float64,
    #    "callsign": str,

    # ftd_type =
    #    "time": str,
    #    "aircraft": np.int32,
    #    "x-position": np.float64,
    #    "y-position": np.float64,
    #    "altitude": np.float64,
    #    "callsign": str,

    with open(filepath, "w", newline="") as f:
        result = []

        if aircraftIdx == 1:
            aircraft = encounter.aircraft1
        elif aircraftIdx == 2:
            aircraft = encounter.aircraft2

        for i in range(len(aircraft)):
            result.append((aircraft[i].violates, aircraft[i].id, round(units.m_to_nm(aircraft[i].x_loc), 7), round(units.m_to_nm(aircraft[i].y_loc), 7), round(units.m_to_ft(aircraft[i].altitude), 7)), aircraft[i].toa_offset)

        writer = csv.writer(f)
        writer.writerows(result)
        f.close()


def writeEncounterFile(filepath, encTask, encounter):
    # eu_type =
    #    "time": str,
    #    "aircraft": np.int32,
    #    "Mode A": np.int32,
    #    "x-position": np.float64,
    #    "y-position": np.float64,
    #    "altitude": np.float64,
    #    "callsign": str,

    # ftd_type =
    #    "time": str,
    #    "aircraft": np.int32,
    #    "x-position": np.float64,
    #    "y-position": np.float64,
    #    "altitude": np.float64,
    #    "callsign": str,

    with open(filepath, "w", newline="") as f:
        result = []

        f.write("HEADER\n")
        f.write("synthetic, 2\n")
        f.write(encTask.aircraft1Prefix + "\n")
        f.write(encTask.aircraft2Prefix + "\n")
        f.write("BODY\n")

        aircraft1 = encounter.aircraft1
        aircraft2 = encounter.aircraft2

        for i in range(len(encounter.aircraft1)):
            if encTask.enc_format == "eu":
                result.append((aircraft1[i].violates, aircraft1.id, encTask.aircraft1ModeA, units.m_to_nm(aircraft1[i].x_loc), units.m_to_nm(aircraft1[i].y_loc), units.m_to_ft(aircraft1[i].altitude), encTask.aircraft1Callsign))
                result.append((aircraft2[i].violates, aircraft2.id, encTask.aircraft2ModeA, units.m_to_nm(aircraft2[i].x_loc), units.m_to_nm(aircraft2[i].y_loc), units.m_to_ft(aircraft2[i].altitude), encTask.aircraft2Callsign))
            if encTask.enc_format == "ftd":
                result.append((aircraft1[i].violates, aircraft1[i].id, round(units.m_to_nm(aircraft1[i].x_loc), 7), round(units.m_to_nm(aircraft1[i].y_loc), 7), round(units.m_to_ft(aircraft1[i].altitude), 7)))
                result.append((aircraft2[i].violates, aircraft2[i].id, round(units.m_to_nm(aircraft2[i].x_loc), 7), round(units.m_to_nm(aircraft2[i].y_loc), 7), round(units.m_to_ft(aircraft2[i].altitude), 7)))

        writer = csv.writer(f)
        writer.writerows(result)
        f.close()


def processTrajectoryFile(full_encounter_path, encTask, encounter, noiseParameters):
    aircraftVector = None

    print("Reading a trajectory...")

    if encounter.enc_format == "eu":
        trj_df = pd.read_csv(full_encounter_path, delimiter='\t', names=trj_eu_columns, dtype=trj_eu_dtype)

        for index, row in trj_df.iterrows():
            if row['aircraft'] == 1:
                position = pos.Position(row['aircraft'], row['time'], row['x-position'], row['y-position'], row['altitude'])
                encounter.addAircraft1Position(position)
            elif row['aircraft'] == 2:
                position = pos.Position(row['aircraft'], row['time'], row['x-position'], row['y-position'], row['altitude'])
                encounter.addAircraft2Position(position)


    if encounter.enc_format == "ftd":
        skip = 0
        trj_df = pd.read_csv(full_encounter_path, header=None, sep='\n', engine='python')

        print("Splitting columns...")
        trj_df = trj_df[0].str.split('\s*,\s*', expand=True)

        # Assign column names
        # df.columns = df_columns

        val = trj_df[0][skip]
        if trj_df[0][skip] != "HEADER":
            return

        skip += 1
        if trj_df[0][skip] != "synthetic":
            return

        encounter.nAircraft = int(trj_df[1][skip])


        idx = 1
        while idx <= 2:
            if int(trj_df[0][skip + idx]) == 1:
                encTask.aircraft1Prefix = trj_df[0][skip + idx] + "," + trj_df[1][skip + idx] + "," + trj_df[2][skip + idx] + "," + trj_df[3][skip + idx] + "," + trj_df[4][skip + idx] + "," + trj_df[5][skip + idx]

            if int(trj_df[0][skip + idx]) == 2:
                encTask.aircraft2Prefix = trj_df[0][skip + idx] + "," + trj_df[1][skip + idx] + "," + trj_df[2][skip + idx] + "," + trj_df[3][skip + idx] + "," + trj_df[4][skip + idx] + "," + trj_df[5][skip + idx]

            idx += 1

        skip += encounter.nAircraft

        skip += 1
        v = trj_df[0][skip]
        if trj_df[0][skip] != "BODY":
            return

        skip += 1

        print("Renaming columns...")
        i = 0
        while i < len(trj_ftd_columns):
            trj_df.rename(columns={i: trj_ftd_columns[i]}, inplace=True)
            i += 1

        for index, row in trj_df.iterrows():
            if index < skip:
                continue

            if int(row['aircraft']) == 1:
                position = pos.Position(int(row['aircraft']), row['time'], float(row['x-position']), float(row['y-position']), float(row['altitude']))
                encounter.addAircraft1Position(position)
            elif int(row['aircraft']) == 2:
                position = pos.Position(int(row['aircraft']), row['time'], float(row['x-position']), float(row['y-position']), float(row['altitude']))
                encounter.addAircraft2Position(position)

            if int(row['aircraft']) == 1:
                aircraftVector = np.array([None] * encounter.nAircraft)

            position = pos.Position(int(row['aircraft']), row['time'], float(row['x-position']), float(row['y-position']), float(row['altitude']))
            aircraftVector[int(row['aircraft']) - 1] = position

            if int(row['aircraft']) == encounter.nAircraft:
                encounter.addAircraftVectorPosition(aircraftVector)


    if (encounter.numberAircraft1Position() == encounter.numberAircraft2Position()):

        noiseParameters.updateSeedForNextEncounter()

        #
        # Step 1: Store true trajectory
        suffix = "True"
        noise_encounter_path = encTask.task_enc_folder + "\\" + "Encs_" + str(encTask.enc_number).zfill(9) + "-" + "Own" + "." + suffix + ".ftd"
        writeTrajectoryFile(noise_encounter_path, encTask, encounter, 1)

        noise_encounter_path = encTask.task_enc_folder + "\\" + "Encs_" + str(encTask.enc_number).zfill(9) + "-" + "Int" + "." + suffix + ".ftd"
        writeTrajectoryFile(noise_encounter_path, encTask, encounter, 2)

        #
        # Step 2: Add GNSS and altimetry error
        suffix = "Gnss"

        gnssNoiseEncounter = encounter.duplicateEncounter()

        ni.insertGnssAltimetryError(gnssNoiseEncounter.aircraft1, noiseParameters)
        ni.insertGnssAltimetryError(gnssNoiseEncounter.aircraft2, noiseParameters)

        noise_encounter_path = encTask.task_enc_folder + "\\" + "Encs_" + str(encTask.enc_number).zfill(9) + "-" + "Own" + "." + suffix + ".ftd"
        writeTrajectoryFile(noise_encounter_path, encTask, gnssNoiseEncounter, 1)

        noise_encounter_path = encTask.task_enc_folder + "\\" + "Encs_" + str(encTask.enc_number).zfill(9) + "-" + "Int" + "." + suffix + ".ftd"
        writeTrajectoryFile(noise_encounter_path, encTask, gnssNoiseEncounter, 2)

        #
        # Step 3: Add ATAR error
        suffix = "Atar"

        atarErrorEncounter = encounter.duplicateEncounter()

        ni.insertAtarError(encounter.aircraft1, atarErrorEncounter.aircraft2, noiseParameters)
        ni.insertAtarError(encounter.aircraft2, atarErrorEncounter.aircraft1, noiseParameters)

        noise_encounter_path = encTask.task_enc_folder + "\\" + "Encs_" + str(encTask.enc_number).zfill(9) + "-" + "Own" + "." + suffix + ".ftd"
        writeTrajectoryFile(noise_encounter_path, encTask, encounter, 1)

        noise_encounter_path = encTask.task_enc_folder + "\\" + "Encs_" + str(encTask.enc_number).zfill(9) + "-" + "Int" + "." + suffix + ".ftd"
        writeTrajectoryFile(noise_encounter_path, encTask, encounter, 2)


        print("Done.")
        return

    else:
        print("Trajectory seems NOT consistent.")
        return



def processEncounterTrajectory(full_encounter_path, encTask, noiseParameters):

    start = time()
    print("Encounter Set file: ", full_encounter_path, "  Identified, processing...")

    encounter = enc.Encounter(encTask.enc_number, encTask.enc_layer, encTask.enc_type, encTask.aircraft1Class,
                              encTask.aircraft2Class, encTask.enc_format)

    encounter.aircraft1Callsign = encTask.aircraft1Callsign
    encounter.aircraft2Callsign = encTask.aircraft1Callsign

    processTrajectoryFile(full_encounter_path, encTask, encounter, noiseParameters)

    duration = (time() - start)
    print("Encounter_Distribution processed in: ", "{:.2f}".format(duration), " sec")



def processEncounterChunck(df, target_path, type, noiseParameters):

    encFound = 0
    encNotFound = 0
    for index, row in df.iterrows():

        found = False
        subfolder = 1
        while (subfolder <= MAX_ENCOUNTER_FOLDERS):

            full_encounter_path = target_path + type + "\\" + "FTD_" + str(subfolder) + "\\" + "Encs_" + str(row['Encounter No.']).zfill(9) + ".ftd"
            full_encounter_folder = target_path + type + "\\" + "FTD_" + str(subfolder)

            if os.path.exists(full_encounter_path):
                encFound += 1
                found = True

                enc_number = row['Encounter No.']
                aircraft1Class = row['AC1 Class']
                aircraft2Class = row['AC2 Class']
                enc_layer = row['Layer']

                print("\nProcessing encounter number: ", enc_number, " with Class1 aircraft: ", aircraft1Class,
                      " with Class2 aircraft: ", aircraft2Class)
                encTask = et.encounter_task(full_encounter_folder, None, enc_layer, type, enc_number, aircraft1Class, aircraft2Class, ENC_FORMAT)

                encTask.aircraft1Callsign = row['AC1 Callsign']
                encTask.aircraft2Callsign = row['AC2 Callsign']
                encTask.aircraft1ModeA = row['AC1 Mode A']
                encTask.aircraft2ModeA = row['AC2 Mode A']

                processEncounterTrajectory(full_encounter_path, encTask, noiseParameters)

                break

            subfolder += 1

        if not found:
            print("Cannot find encounter: ", str(row['Encounter No.']))
            encNotFound += 1

    print("Done.")
    return encFound, encNotFound, len(df.index)



def processEncounterSet(enc_folder, layer, type, subfolder_path, fullpath_file, noiseParameters):
    found = 0
    notFound = 0
    sourceTotal = 0

    df_chunk = pd.read_csv(fullpath_file, delimiter=',', chunksize=10000, dtype=df_dtype)

    for chunk in df_chunk:

        print("Got chuck of data: " + str(len(chunk.index)) + " for a total of: " + str(sourceTotal))

        chunk['Encounter No.'] = chunk['Encounter No.'].round().astype(int).astype(int)
        chunk['Seed'] = chunk['Seed'].round().astype(int).astype(int)
        chunk['Layer'] = chunk['Layer'].round().astype(np.int)
        chunk['AC1 Class'] = chunk['AC1 Class'].round().astype(np.int)
        chunk['AC2 Class'] = chunk['AC2 Class'].round().astype(np.int)

        target_path = enc_folder + subfolder_path

        partial1, partial2, partial3 = processEncounterChunck(chunk, target_path, type, noiseParameters)
        found += partial1
        notFound += partial2
        sourceTotal += partial3

        if partial1 > 0:
            print("Total encounters so far: " + str(found) + " missing encounters: " + str(notFound))

    return found, notFound, sourceTotal



# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    MAX_LAYER = 4
    MAX_ENCOUNTER_FOLDERS = 2
    ENCOUNTER_TYPE = ["EE", "EU", "UU"]
    ENC_FORMAT = "ftd"

    MAIN_PATH = "F:\Software\CAFE-Analyzer\\"
    enc_file_template = "Layer_%s_%s_100k.csv"
    enc_file = "Enc_Params.csv"

    layer_folder_template = "Layer_%s_%s_100k\\"
    enc_folder = "E:\CAFE-Encounters\RPAS_Generated_Encounters-REV5\RPAS_Other\\"

    noiseParameters = npa.Noise_parameters()

    noiseParameters.parseFromJson(MAIN_PATH + "NoiseModel\\URClearED-Noise-Parameters.json")

    noiseParameters.setInitialSeed(100)

    print("Analyzing encounters at folder: \n\t\t", enc_folder, "\n")


    layer = 1
    encounterReal = 0
    encounterFailed = 0
    encounterTotal = 0
    while (layer <= MAX_LAYER):
        i = 0
        print("Processing encounter layer: ", layer, "\n")

        while i < len(ENCOUNTER_TYPE):
            type = ENCOUNTER_TYPE[i]
            i += 1

            print("Processing encounter type: ", type, "\n")

            subfolder_path = layer_folder_template % (layer, type)
            #enc_file = enc_file_template % (layer, type)
            fullpath_file = enc_folder + subfolder_path + enc_file

            if not os.path.isfile(fullpath_file):
                print("Encounter_Distribution set: ", fullpath_file, "\n")
                print("Does not exists, skipping...\n")
            elif not os.access(fullpath_file, os.R_OK):
                print("Encounter_Distribution set: ", fullpath_file, "\n")
                print("Cannot be opened, skipping...\n")
            else:
                print("Opening encounter set: ", fullpath_file, "\n")
                partial1, partial2, total = processEncounterSet(enc_folder, layer, type, subfolder_path, fullpath_file, noiseParameters)
                encounterReal += partial1
                encounterFailed += partial2
                encounterTotal += total
                print("Encounter_Distribution set adds: ", partial1, "up to a total of: ", encounterReal, "\n")
        layer += 1


    print("Total number of encounters processed: " + str(encounterReal) + " missed: " + str(encounterFailed) + " from a total of: " + str(encounterTotal) + "\n")
    print("Done.\n")
    sys.exit(0)