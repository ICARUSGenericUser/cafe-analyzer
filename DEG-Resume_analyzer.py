# This is a sample Python script.
import os, shutil
import sys
from time import time
import pandas as pd
import numpy as np
import docx as docx
from docx.shared import Inches, Cm
from docx.enum.section import WD_SECTION_START
import argparse
import matplotlib.pyplot as plt
import Encounter.units as units
from NCSensor import NCS_parameters as ncsp
from CAFE_Evaluation_Functions import Sensor_evaluation as sensorEvaluation, Timming_evaluation as timmingEvaluation



df_columns = [
    "layer",
    "EncounterId",
    "Approach Angle",
    "AC1 Horizontal Mode",
    "AC1 Vertical Mode",
    "AC1 Speed Mode",
    "AC1 Altitude",
    "AC1 Speed",
    "AC1 Vertical Speed",
    "AC2 Horizontal Mode",
    "AC2 Vertical Mode",
    "AC2 Speed Mode",
    "AC2 Altitude",
    "AC2 Speed",
    "AC2 Vertical Speed",
    "aircraft1Class",
    "aircraft2Class",
    "activeConflict",
    "cpaTime",
    "cpaBeta",
    "cpaRange",
    "cpaAzimuth",
    "cpaElevation",
    "cpaDz",
    "minHDistTime",
    "minHDistBeta",
    "minHDistRange",
    "minHDistAzimuth",
    "minHDistElevation",
    "minHDistDz",
    "nmacViolated",
    "nmacFirstTime",
    "nmacTime",
    "nmacWSR",
    "nmacHMD",
    "nmacVMD",
    "nmacFirstTimeBeta",
    "nmacFirstTimeRange",
    "nmacFirstTimeAzimuth",
    "nmacFirstTimeElevation",
    "nmacFirstTimeDz",
    "nmacTimeBeta",
    "nmacTimeRange",
    "nmacTimeAzimuth",
    "nmacTimeElevation",
    "nmacTimeDz",

    "hazViolated",
    "hazFirstTime",
    "hazFirstTimeSLoWC",
    "hazFirstTimeRangePen",
    "hazFirstTimeHMDPen",
    "hazFirstTimeVertPen",
    "hazFirstTcpa",
    "hazFirstDcpa",
    "haz_SLoWC_time",
    "hazSLoWC",
    "hazRangePen",
    "hazHMDPen",
    "hazVertPen",
    "hazTcpaTime",
    "hazTcpa",
    "hazDcpa",
    "minSlatTime",
    "minSlatRange",

    "hazFirstTimeBeta",
    "hazFirstTimeRange",
    "hazFirstTimeAzimuth",
    "hazFirstTimeElevation",
    "hazFirstTimeDz",
    "hazSLoWCTimeBeta",
    "hazSLoWCTimeRange",
    "hazSLoWCTimeAzimuth",
    "hazSLoWCTimeElevation",
    "hazSLoWCTimeDz",
    "hazTcpaTimeBeta",
    "hazTcpaTimeRange",
    "hazTcpaTimeAzimuth",
    "hazTcpaTimeElevation",
    "hazTcpaTimeDz",
    "hazSlatTimeBeta",
    "hazSlatTimeRange",
    "hazSlatTimeAzimuth",
    "hazSlatTimeElevation",
    "hazSlatTimeDz",

    "cautionNumber",
    "cautionFirstTime",
    "cautionFirstTtHaz",
    "cautionFirstType",
    "cautionTime",
    "cautionTtHaz",
    "cautionType",
    "cautionFirstBeta",
    "cautionFirstRange",
    "cautionFirstAzimuth",
    "cautionFirstElevation",
    "cautionFirstDz",
    "cautionBeta",
    "cautionRange",
    "cautionAzimuth",
    "cautionElevation",
    "cautionDz",
    "cautionFirstEndTime",
    "cautionFirstEndBeta",
    "cautionFirstEndRange",
    "cautionFirstEndAzimuth",
    "cautionFirstEndElevation",
    "cautionFirstEndDz",
    "cautionEndTime",
    "cautionEndBeta",
    "cautionEndRange",
    "cautionEndAzimuth",
    "cautionEndElevation",
    "cautionEndDz",

    "prevNumber",
    "prevFirstTime",
    "prevFirstTtHaz",
    "prevFirstType",
    "prevTime",
    "prevTtHaz",
    "prevType",
    "prevFirstBeta",
    "prevFirstRange",
    "prevFirstAzimuth",
    "prevFirstElevation",
    "prevFirstDz",
    "prevBeta",
    "prevRange",
    "prevAzimuth",
    "prevElevation",
    "prevDz",
    "prevFirstEndTime",
    "prevFirstEndBeta",
    "prevFirstEndRange",
    "prevFirstEndAzimuth",
    "prevFirstEndElevation",
    "prevFirstEndDz",
    "prevEndTime",
    "prevEndBeta",
    "prevEndRange",
    "prevEndAzimuth",
    "prevEndElevation",
    "prevEndDz",

    "warnNumber",
    "warnFirstTime",
    "warnFirstTtHaz",
    "warnFirstType",
    "warnTime",
    "warnTtHaz",
    "warnType",
    "warnFirstBeta",
    "warnFirstRange",
    "warnFirstAzimuth",
    "warnFirstElevation",
    "warnFirstDz",
    "warnBeta",
    "warnRange",
    "warnAzimuth",
    "warnElevation",
    "warnDz",
    "warnFirstEndTime",
    "warnFirstEndBeta",
    "warnFirstEndRange",
    "warnFirstEndAzimuth",
    "warnFirstEndElevation",
    "warnFirstEndDz",
    "warnEndTime",
    "warnEndBeta",
    "warnEndRange",
    "warnEndAzimuth",
    "warnEndElevation",
    "warnEndDz",

    "tcasRa",
    "tcasRaTime",
    "tcasRaTimeWorst",
    "tcasRaTimeBeta",
    "tcasRaTimeRange",
    "tcasRaTimeAzimuth",
    "tcasRaTimeElevation",
    "tcasRaTimeDz",
    "tcasRaWorstBeta",
    "tcasRaWorstRange",
    "tcasRaWorstAzimuth",
    "tcasRaWorstElevation",
    "tcasRaWorstDz",
    "tcasRaTimeTimeCrit",
    "tcasRaTimeDistCrit",
    "tcasRaWorstTimeCrit",
    "tcasRaWorstDistCrit",

    "sepViolated",
    "sepRelevant",
    "sepTime",
    "sepTimeBeta",
    "sepTimeRange",
    "sepTimeAzimuth",
    "sepTimeElevation",
    "sepTimeDz",

    "stcaViolated",
    "stcaRelevant",
    "stcaTime",
    "stcaTimeBeta",
    "stcaTimeRange",
    "stcaTimeAzimuth",
    "stcaTimeElevation",
    "stcaTimeDz",

    "lmvActivated",
    "lmvViolated",
    "lmvFirstTime",
    "lmvWorstTime",
    "lmvFirstTimePrediction",
    "lmvWorstDistance",
    "lmvViolatedTime",
    "lmvTimeBeta",
    "lmvTimeRange",
    "lmvTimeAzimuth",
    "lmvTimeElevation",
    "lmvTimeDz",

    "hazLmvViolated",
    "hazLmvViolatedTime",
    "hazLmvTimeBeta",
    "hazLmvTimeRange",
    "hazLmvTimeAzimuth",
    "hazLmvTimeElevation",
    "hazLmvTimeDz"
]



df_dtype = {
    "layer": int,
    "EncounterId": int,
    "Approach Angle": np.float64,
    "AC1 Horizontal Mode": str,
    "AC1 Vertical Mode": str,
    "AC1 Speed Mode": str,
    "AC1 Altitude": np.float64,
    "AC1 Speed": np.float64,
    "AC1 Vertical Speed": np.float64,
    "AC2 Horizontal Mode": str,
    "AC2 Vertical Mode": str,
    "AC2 Speed Mode": str,
    "AC2 Altitude": np.float64,
    "AC2 Speed": np.float64,
    "AC2 Vertical Speed": np.float64,
    "aircraft1Class": int,
    "aircraft2Class": int,
    "activeConflict": bool,

    "cpaTime": int,
    "cpaBeta": np.float64,
    "cpaRange": np.float64,
    "cpaAzimuth": np.float64,
    "cpaElevation": np.float64,
    "cpaDz": np.float64,

    "minHDistTime": int,
    "minHDistBeta": np.float64,
    "minHDistRange": np.float64,
    "minHDistAzimuth": np.float64,
    "minHDistElevation": np.float64,
    "minHDistDz": np.float64,

    "nmacViolated": bool,
    "nmacFirstTime": int,
    "nmacTime": int,
    "nmacWSR": np.float64,
    "nmacHMD": np.float64,
    "nmacVMD": np.float64,
    "nmacFirstTimeBeta": np.float64,
    "nmacFirstTimeRange": np.float64,
    "nmacFirstTimeAzimuth": np.float64,
    "nmacFirstTimeElevation": np.float64,
    "nmacFirstTimeDz": np.float64,
    "nmacTimeBeta": np.float64,
    "nmacTimeRange": np.float64,
    "nmacTimeAzimuth": np.float64,
    "nmacTimeElevation": np.float64,
    "nmacTimeDz": np.float64,

    "hazViolated": bool,
    "hazFirstTime": int,
    "hazFirstTimeSLoWC": np.float64,
    "hazFirstTimeRangePen": np.float64,
    "hazFirstTimeHMDPen": np.float64,
    "hazFirstTimeVertPen": np.float64,
    "hazFirstTcpa": np.float64,
    "hazFirstDcpa": np.float64,
    "haz_SLoWC_time": int,
    "hazSLoWC": np.float64,
    "hazRangePen": np.float64,
    "hazHMDPen": np.float64,
    "hazVertPen": np.float64,
    "hazTcpaTime": np.float64,
    "hazTcpa": np.float64,
    "hazDcpa": np.float64,

    "minSlatTime": np.float64,
    "minSlatRange": np.float64,
    "hazFirstTimeBeta": np.float64,
    "hazFirstTimeRange": np.float64,
    "hazFirstTimeAzimuth": np.float64,
    "hazFirstTimeElevation": np.float64,
    "hazFirstTimeDz": np.float64,
    "hazSLoWCTimeBeta": np.float64,
    "hazSLoWCTimeRange": np.float64,
    "hazSLoWCTimeAzimuth": np.float64,
    "hazSLoWCTimeElevation": np.float64,
    "hazSLoWCTimeDz": np.float64,
    "hazTcpaTimeBeta": np.float64,
    "hazTcpaTimeRange": np.float64,
    "hazTcpaTimeAzimuth": np.float64,
    "hazTcpaTimeElevation": np.float64,
    "hazTcpaTimeDz": np.float64,
    "hazSlatTimeBeta": np.float64,
    "hazSlatTimeRange": np.float64,
    "hazSlatTimeAzimuth": np.float64,
    "hazSlatTimeElevation": np.float64,
    "hazSlatTimeDz": np.float64,

    "cautionNumber": int,
    "cautionFirstTime": int,
    "cautionFirstTtHaz": np.float64,
    "cautionFirstType": str,
    "cautionTime": int,
    "cautionTtHaz": np.float64,
    "cautionType": str,
    "cautionFirstBeta": np.float64,
    "cautionFirstRange": np.float64,
    "cautionFirstAzimuth": np.float64,
    "cautionFirstElevation": np.float64,
    "cautionFirstDz": np.float64,
    "cautionBeta": np.float64,
    "cautionRange": np.float64,
    "cautionAzimuth": np.float64,
    "cautionElevation": np.float64,
    "cautionDz": np.float64,
    "cautionFirstEndTime": int,
    "cautionFirstEndBeta": np.float64,
    "cautionFirstEndRange": np.float64,
    "cautionFirstEndAzimuth": np.float64,
    "cautionFirstEndElevation": np.float64,
    "cautionFirstEndDz": np.float64,
    "cautionEndTime": int,
    "cautionEndBeta": np.float64,
    "cautionEndRange": np.float64,
    "cautionEndAzimuth": np.float64,
    "cautionEndElevation": np.float64,
    "cautionEndDz": np.float64,

    "prevNumber": int,
    "prevFirstTime": int,
    "prevFirstTtHaz": np.float64,
    "prevFirstType": str,
    "prevTime": int,
    "prevTtHaz": np.float64,
    "prevType": str,
    "prevFirstBeta": np.float64,
    "prevFirstRange": np.float64,
    "prevFirstAzimuth": np.float64,
    "prevFirstElevation": np.float64,
    "prevFirstDz": np.float64,
    "prevBeta": np.float64,
    "prevRange": np.float64,
    "prevAzimuth": np.float64,
    "prevElevation": np.float64,
    "prevDz": np.float64,
    "prevFirstEndTime": int,
    "prevFirstEndBeta": np.float64,
    "prevFirstEndRange": np.float64,
    "prevFirstEndAzimuth": np.float64,
    "prevFirstEndElevation": np.float64,
    "prevFirstEndDz": np.float64,
    "prevEndTime": int,
    "prevEndBeta": np.float64,
    "prevEndRange": np.float64,
    "prevEndAzimuth": np.float64,
    "prevEndElevation": np.float64,
    "prevEndDz": np.float64,

    "warnNumber": int,
    "warnFirstTime": int,
    "warnFirstTtHaz": np.float64,
    "warnFirstType": str,
    "warnTime": int,
    "warnTtHaz": np.float64,
    "warnType": str,
    "warnFirstBeta": np.float64,
    "warnFirstRange": np.float64,
    "warnFirstAzimuth": np.float64,
    "warnFirstElevation": np.float64,
    "warnFirstDz": np.float64,
    "warnBeta": np.float64,
    "warnRange": np.float64,
    "warnAzimuth": np.float64,
    "warnElevation": np.float64,
    "warnDz": np.float64,
    "warnFirstEndTime": int,
    "warnFirstEndBeta": np.float64,
    "warnFirstEndRange": np.float64,
    "warnFirstEndAzimuth": np.float64,
    "warnFirstEndElevation": np.float64,
    "warnFirstEndDz": np.float64,
    "warnEndTime": int,
    "warnEndBeta": np.float64,
    "warnEndRange": np.float64,
    "warnEndAzimuth": np.float64,
    "warnEndElevation": np.float64,
    "warnEndDz": np.float64,

    "tcasRa": bool,
    "tcasRaTime": int,
    "tcasRaTimeWorst": int,
    "tcasRaTimeBeta": np.float64,
    "tcasRaTimeRange": np.float64,
    "tcasRaTimeAzimuth": np.float64,
    "tcasRaTimeElevation": np.float64,
    "tcasRaTimeDz": np.float64,
    "tcasRaWorstBeta": np.float64,
    "tcasRaWorstRange": np.float64,
    "tcasRaWorstAzimuth": np.float64,
    "tcasRaWorstElevation": np.float64,
    "tcasRaWorstDz": np.float64,
    "tcasRaTimeTimeCrit": np.float64,
    "tcasRaTimeDistCrit": np.float64,
    "tcasRaWorstTimeCrit": np.float64,
    "tcasRaWorstDistCrit": np.float64,

    "sepViolated": bool,
    "sepRelevant": bool,
    "sepTime": int,
    "sepTimeBeta": np.float64,
    "sepTimeRange": np.float64,
    "sepTimeAzimuth": np.float64,
    "sepTimeElevation": np.float64,
    "sepTimeDz": np.float64,

    "stcaViolated": bool,
    "stcaRelevant": bool,
    "stcaTime": int,
    "stcaTimeBeta": np.float64,
    "stcaTimeRange": np.float64,
    "stcaTimeAzimuth": np.float64,
    "stcaTimeElevation": np.float64,
    "stcaTimeDz": np.float64,

    "lmvActivated": bool,
    "lmvViolated": bool,
    "lmvFirstTime": int,
    "lmvWorstTime": int,
    "lmvFirstTimePrediction": np.float64,
    "lmvWorstDistance": np.float64,
    "lmvViolatedTime": int,
    "lmvTimeBeta": np.float64,
    "lmvTimeRange": np.float64,
    "lmvTimeAzimuth": np.float64,
    "lmvTimeElevation": np.float64,
    "lmvTimeDz": np.float64,

    "hazLmvViolated": bool,
    "hazLmvViolatedTime": int,
    "hazLmvTimeBeta": np.float64,
    "hazLmvTimeRange": np.float64,
    "hazLmvTimeAzimuth": np.float64,
    "hazLmvTimeElevation": np.float64,
    "hazLmvTimeDz": np.float64
}


# df_dtype = {
#     "EncounterId": int,
#     "aircraft1Class": int,
#     "aircraft2Class": int,
#     "activeConflict": bool,
#     "nmacViolated": bool,
#     "nmacFirstTime": int,
#     "nmacTime": int,
#     "nmacWSR": np.float64,
#     "nmacHMD": np.float64,
#     "nmacVMD": np.float64,
#     "nmacFirstTimeRange": np.float64,
#     "nmacFirstTimeAzimuth": np.float64,
#     "nmacFirstTimeElevation": np.float64,
#     "nmacTimeRange": np.float64,
#     "nmacTimeAzimuth": np.float64,
#     "nmacTimeElevation": np.float64,
#     "hazViolated": bool,
#     "hazFirstTime": np.float64,
#     "hazTime": np.float64,
#     "hazFirstTcpa": np.float64,
#     "hazTcpa": np.float64,
#     "hazSLoWC": np.float64,
#     "hazRangePen": np.float64,
#     "hazHMDPen": np.float64,
#     "hazVertPen": np.float64,
#     "hazFirstTimeRange": np.float64,
#     "hazFirstTimeAzimuth": np.float64,
#     "hazFirstTimeElevation": np.float64,
#     "hazTimeRange": np.float64,
#     "hazTimeAzimuth": np.float64,
#     "hazTimeElevation": np.float64,
# }


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # creating the name
    parser = argparse.ArgumentParser(prog='DEG_Encounter_analyzer',
                                     usage='%(prog)s [options]',
                                     description='Process DEG collision encounter subsets')

    parser.add_argument('-i', '--intruder',
                        type=str,
                        choices=['Other', 'RPAS'],
                        default='Other', required=False,
                        help='Intruder type: [Other|RPAS]')

    parser.add_argument('-a', '--altitude',
                        type=str,
                        choices=['LOWER', 'HIGHER'],
                        default='HIGHER', required=False,
                        help='Altitude subset with associated speed limitation: [LOWER|HIGHER]')

    parser.add_argument('-os', '--ownspeed',
                        type=str,
                        choices=['NONE', 'BELOW100', 'ABOVE100'],
                        default='NONE', required=False,
                        help='Speed of ownship : [NONE|BELOW100|ABOVE100]')

    parser.add_argument('-wcv', '--wcvSet',
                        type=str,
                        choices=['05NM', '22KFT', 'URCV', 'SESAR', 'DO365', '05NMShort', '22KFTShort', 'URCVShort'],
                        default='SESAR', required=False,
                        help='WCV volume definition: [05NM|22KFT|URCV|SESAR|DO365|05NMShort|22KFTShort|URCVShort]')

    parser.add_argument('-s', '--subset',
                        type=str,
                        default='', required=False,
                        help='Subset folder name for results')

    parser.add_argument('-bf', '--baseFolder',
                        type=str,
                        choices=['Laptop', 'Voyager'],
                        default='Laptop', required=False,
                        help='Base folder for execution.')

    parser.add_argument('-acas', '--acas', action='store_true')

    parser.add_argument('-cr', '--clearResults', action='store_true')

    # Read arguments from the command line
    args = parser.parse_args()

    # Start time of computing
    start = time()

    Prefix_Name = "DEG_Analysis_Results"

    if args.subset:
        Postfix_Name = args.subset
    else:
        Postfix_Name = "Analysis"


    result_file = "Encounter_Analysis_Results.csv"

    result_acas_file = "DEG_ACAS_Results.csv"
    prov_file = "DEG_Results.txt"
    results_folder = "ResultDiagrams"


    base_folder = "F:\\DroneEncounters\\EncounterSet\\wcvTestSet100-Set1\\"


    result_enc_folder = base_folder + Prefix_Name + "\\"
    result_enc_file = result_enc_folder + result_file

    resume_enc_folder = result_enc_folder + Postfix_Name + "\\"
    txt_resume_filename = resume_enc_folder + "resumeFile.txt"
    docx_resume_filename = resume_enc_folder + "resumeFigures.docx"
    csv_resume_filename = resume_enc_folder + "resumeTables.csv"
    csvh_resume_filename = resume_enc_folder + "resumeTablesHeader.csv"


    print("Analyzing...")

    if not os.path.exists(result_enc_file):
        print("Encounter results file: ", result_enc_file, "\n")
        print("Does not exists, stopping...\n")
        exit(-1)

    if os.path.exists(resume_enc_folder):
        print("Removing folder...\n")
        shutil.rmtree(resume_enc_folder)

    os.mkdir(resume_enc_folder)
    os.mkdir(resume_enc_folder + results_folder)

    fResults = open(txt_resume_filename, "w")


    if args.clearResults and os.path.exists(docx_resume_filename):
        os.remove(docx_resume_filename)
        os.remove(csv_resume_filename)
        os.remove(csvh_resume_filename)


    if not os.path.exists(docx_resume_filename):
        mydoc = docx.Document()
        mydoc.save(docx_resume_filename)


    mydoc = docx.Document(docx_resume_filename)
    mycsv = open(csv_resume_filename, "a")
    mycsvh = open(csvh_resume_filename, "w")

    current_section = mydoc.sections[-1]  # last section in document
    header = current_section.header
    header.is_linked_to_previous = False
    paragraph = header.paragraphs[0]

    paragraph.text = "Encounter Analysis Resume"
    paragraph.style = mydoc.styles["Header"]


    mydoc.add_heading("Heading values", 1)
    mycsvh.write("Headings")
    mycsv.write("Heading values")


    # , dtype=df_dtype
    df_results = pd.read_csv(result_enc_file, index_col=False, header=None, skiprows=1, sep=',', names=df_columns)

    #print(df_results['hazViolated'])
    df_results['nmacViolated'] = df_results['nmacViolated'].astype('bool')
    df_results['hazViolated'] = df_results['hazViolated'].astype('bool')
    df_results['sepViolated'] = df_results['sepViolated'].astype('bool')
    df_results['sepRelevant'] = df_results['sepRelevant'].astype('bool')
    df_results['stcaViolated'] = df_results['stcaViolated'].astype('bool')
    df_results['stcaRelevant'] = df_results['stcaRelevant'].astype('bool')


    #df_results.info(verbose=True)


    ##### SPURIOUS CAUTION TIMMING EVALUATION ################################################################################################

    # Baseline taken at CPA: "cpaTime"
    # NMAC as: "nmacTime"
    # LMV as: "lmvTime"
    # WCV Violation as: "wcvTime"
    # Caution activation as: "cautionTime"
    # End Caution activation as: "endCautionTime"
    # Separation violation as: "separationTime"

    cautionActivation = df_results[["EncounterId", \
                                    "minHDistTime", "nmacFirstTime", "lmvViolatedTime", "hazFirstTime", "hazFirstTcpa", "haz_SLoWC_time", \
                                    "hazSLoWC", "cautionFirstTime", "cautionFirstTtHaz", "cautionFirstEndTime", "cautionNumber", "sepTime", "AC1 Vertical Speed", "AC2 Vertical Speed"]].copy()

    #cautionActivation.info(verbose=True)

    cautionActivation.rename(columns={"minHDistTime": "cpaTime", 'nmacFirstTime': 'nmacTime', 'lmvViolatedTime': 'lmvTime', 'hazFirstTime': 'wcvTime', 'hazFirstTcpa': 'wcvTcpa', \
                                      "haz_SLoWC_time": 'wcvSLoWCTime',  "hazSLoWC": "wcvSLoWC", 'cautionFirstTime': 'cautionTime', \
                                      'cautionFirstTtHaz': 'cautionTtHaz', 'cautionFirstEndTime': 'endCautionTime', 'sepTime': 'separationTime'}, inplace=True)


    timmingEvaluation.evaluateGeneralCautionTimming(fResults, df_results, mycsv, mycsvh)

    Caption = "Spurious Caution activation analysis: "

    timmingEvaluation.evaluateSpuriousCautionTimming(fResults, mydoc, mycsv, mycsvh, df_results, Caption, resume_enc_folder + results_folder + "/")

    Caption = "Active Caution activation analysis: "

    timmingEvaluation.evaluateActiveCautionTimming(fResults, mydoc, mycsv, mycsvh, df_results, Caption, resume_enc_folder + results_folder + "/")

    timmingEvaluation.evaluateActivePreventiveTimming(fResults, mydoc, mycsv, mycsvh, df_results, Caption, resume_enc_folder + results_folder + "/")

    Caption = "NMAC violation analysis: "

    timmingEvaluation.evaluateNMACTimming(fResults, df_results, Caption, resume_enc_folder + results_folder + "/")


    mydoc.add_section(WD_SECTION_START.NEW_PAGE)
    sections = mydoc.sections
    for section in sections:
        section.top_margin = Cm(2)
        section.bottom_margin = Cm(2)
        section.left_margin = Cm(1)
        section.right_margin = Cm(1)

    mycsv.write("\n")
    mycsv.close()
    mycsvh.write("\n")
    mycsvh.close()
    mydoc.save(docx_resume_filename)

    fResults.close()
    exit(0)


    nmacSet = ((df_results['nmacViolated'].str.contains('True')))
    hazSet = ((df_results['hazViolated'].str.contains('True'))&(df_results['nmacViolated'].str.contains('False')))
    hazSet2 = ((df_results['hazViolated'].str.contains('True')))

    df_filtered_NMAC = df_results.loc[nmacSet]
    df_filtered_HAZ = df_results.loc[hazSet]


    #earlyNMAC = df_filtered_NMAC[["nmacFirstTimeBeta", "nmacFirstTimeRange", "nmacFirstTimeAzimuth", "nmacFirstTimeElevation", "nmacFirstTimeDz"]].copy()
    #lateNMAC = df_filtered_NMAC[["nmacTimeBeta", "nmacTimeRange", "nmacTimeAzimuth", "nmacTimeElevation", "nmacTimeDz"]].copy()

    #earlyNMAC.columns.values[0] = "Beta"
    #earlyNMAC.columns.values[1] = "Range"
    #earlyNMAC.columns.values[2] = "Azimuth"
    #earlyNMAC.columns.values[3] = "Elevation"

    #lateNMAC.columns.values[0] = "Beta"
    #lateNMAC.columns.values[1] = "Range"
    #lateNMAC.columns.values[2] = "Azimuth"
    #lateNMAC.columns.values[3] = "Elevation"


    #
    #sensor = NCS_parameters(rangeMax, rangeMin, azimuthMax, elevationUpMax, elevationDownMax)
    #sensor = NCS_parameters(5000, 0, 110, 15, 15)

    #evaluateSensorCoverage(earlyNMAC, lateNMAC, sensor)

    fResults.write("\nSensor coverage evaluation:")

    fResults.write("Encounters with NMAC violation: ", len(df_filtered_NMAC.index))
    fResults.write("Encounters with HAZ violation: ", len(df_filtered_HAZ.index))




##### SENSOR EVALUATION ################################################################################################

    earlyNMAC = df_filtered_NMAC[["nmacFirstTimeBeta", "nmacFirstTimeRange", "nmacFirstTimeAzimuth", "nmacFirstTimeElevation", "nmacFirstTimeDz"]].copy()
    lateNMAC = df_filtered_NMAC[["nmacTimeBeta", "nmacTimeRange", "nmacTimeAzimuth", "nmacTimeElevation", "nmacTimeDz"]].copy()

    earlyNMAC.columns.values[0] = "Beta"
    earlyNMAC.columns.values[1] = "Range"
    earlyNMAC.columns.values[2] = "Azimuth"
    earlyNMAC.columns.values[3] = "Elevation"
    earlyNMAC.columns.values[4] = "dZ"

    lateNMAC.columns.values[0] = "Beta"
    lateNMAC.columns.values[1] = "Range"
    lateNMAC.columns.values[2] = "Azimuth"
    lateNMAC.columns.values[3] = "Elevation"
    lateNMAC.columns.values[4] = "dZ"

    earlyHAZ = df_filtered_HAZ[["hazFirstTimeBeta", "hazFirstTimeRange", "hazFirstTimeAzimuth", "hazFirstTimeElevation", "hazFirstTimeDz"]].copy()
    lateHAZ = df_filtered_HAZ[["hazSlatTimeBeta", "hazSlatTimeRange", "hazSlatTimeAzimuth", "hazSlatTimeElevation", "hazSlatTimeDz"]].copy()

    earlyHAZ.columns.values[0] = "Beta"
    earlyHAZ.columns.values[1] = "Range"
    earlyHAZ.columns.values[2] = "Azimuth"
    earlyHAZ.columns.values[3] = "Elevation"
    earlyHAZ.columns.values[4] = "dZ"

    lateHAZ.columns.values[0] = "Beta"
    lateHAZ.columns.values[1] = "Range"
    lateHAZ.columns.values[2] = "Azimuth"
    lateHAZ.columns.values[3] = "Elevation"
    lateHAZ.columns.values[4] = "dZ"

    # sensor = ncsp.NCS_parameters(rangeMax, rangeMin, azimuthMax, elevationUpMax, elevationDownMax)
    sensor = ncsp.NCS_parameters(5000, 0, 110, 15, 15)

    sensorEvaluation.evaluateSensorCoverage(fResults, earlyHAZ, lateHAZ, sensor, "First Time Hazard Zone", "Slat Hazard Zone",
                                            resume_enc_folder + results_folder + "/")

########################################################################################################################

    from scipy.stats import rv_histogram
    import numpy as np

    df_filtered_HAZ['cpaTime']
    df_filtered_HAZ['cpaTime']


    hst_azimuth, bins_count = np.histogram(df_filtered_HAZ['hazFirstTimeAzimuth'], bins=361)
    r_azimuth = rv_histogram((hst_azimuth, bins_count))
    ang_dist = np.linspace(-180, 180, 361)
    pdf = r_azimuth.pdf(ang_dist)  # 0, 0.25, 0.5, 0.75, 1
    cdf = r_azimuth.cdf(ang_dist)

    fig = plt.figure()
    plt.plot(bins_count[1:], pdf, color="red", label="PDF")
    filepath = resume_enc_folder + results_folder + "/hazFirstTimeAzimuth_PDF.png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()

    fig = plt.figure()
    plt.plot(bins_count[1:], cdf, label="CDF")
    filepath = resume_enc_folder + results_folder + "/hazFirstTimeAzimuth_CDF.png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()

    print(r_azimuth.cdf(110) - r_azimuth.cdf(-110))

########################################################################################################################

# Polar plot
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='polar', xlim=(-90, 90))
    ax.set_theta_direction(-1)  # change direction to CCW
    ax.set_thetamin(0)  # set the limits
    ax.set_thetamax(180)
    ax.set_theta_offset(np.pi)  # point the origin towards the top
    ax.set_thetagrids(range(0, 180, 30))  # set the gridlines
    ax.set_title('Range versus beta at RWC violation', pad=-50)  # add title and relocate negative value lowers th
    ax.set_xlabel('range (m)')

    for index, row in df_filtered_HAZ.iterrows():
        radian = units.deg_to_rad(row['hazFirstTimeBeta'])
        ax.plot(radian, row['hazFirstTimeRange'], 'g.')

    for index, row in df_filtered_NMAC.iterrows():
        radian = units.deg_to_rad(row['hazFirstTimeBeta'])
        ax.plot(radian, row['hazFirstTimeRange'], 'r.')

    filepath = resume_enc_folder + results_folder + "/Range_Beta_Polar.png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()


    fig = plt.figure()
    ax = fig.add_subplot(111, projection='polar', xlim=(-90, 90))
    ax.set_theta_direction(-1)  # change direction to CCW
    ax.set_thetamin(-90)  # set the limits
    ax.set_thetamax(90)
    ax.set_theta_offset(0.5 * np.pi)  # point the origin towards the top
    ax.set_thetagrids(range(-90, 90, 30))  # set the gridlines
    ax.set_title('Range versus elevation at RWC violation', pad=-50)  # add title and relocate negative value lowers th
    ax.set_xlabel('range (m)')

    for index, row in df_filtered_HAZ.iterrows():
        radian = units.deg_to_rad(row['hazFirstTimeAzimuth'])
        ax.plot(radian, row['hazFirstTimeRange'], 'g.')

    for index, row in df_filtered_NMAC.iterrows():
        radian = units.deg_to_rad(row['hazFirstTimeAzimuth'])
        ax.plot(radian, row['hazFirstTimeRange'], 'r.')

    filepath = resume_enc_folder + results_folder + "/Range_Azimuth_Polar.png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()


    plt.figure()
    ax = df_filtered_HAZ.plot.scatter(x="hazFirstTimeRange", y="hazFirstTimeDz");
    df_filtered_NMAC.plot.scatter(x="hazFirstTimeRange", y="hazFirstTimeDz", ax = ax);
    plt.axis('equal')
    plt.title("Range versus Dz at RWC violation")
    plt.xlabel('H distance (m)')
    plt.ylabel('V distance (m)')
    fig = plt.gcf()
    filepath = resume_enc_folder + results_folder + "/Range_Dz_Hazard_First.png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()


    plt.figure()
    df_filtered_HAZ.plot.scatter(x="hazSLoWCTimeRange", y="hazSLoWCTimeDz");
    plt.axis('equal')
    plt.title("Range versus Dz at worst SLoW")
    plt.xlabel('H distance (m)')
    plt.ylabel('V distance (m)')
    fig = plt.gcf()
    filepath = resume_enc_folder + results_folder + "/Range_Dz_Hazard_SLoW.png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()


    plt.figure()
    df_filtered_HAZ.plot.scatter(x="hazFirstTimeAzimuth", y="hazFirstTimeElevation");
    plt.axis('equal')
    plt.title("Azimuth versus Elevation at RWC violation")
    plt.xlabel('Azimuth (deg)')
    plt.ylabel('Elevation (deg)')
    fig = plt.gcf()
    filepath = resume_enc_folder + results_folder + "/Azimuth_Elevation_Hazard_First.png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()


    plt.figure()
    df_filtered_HAZ.plot.scatter(x="hazSLoWCTimeAzimuth", y="hazSLoWCTimeElevation");
    plt.axis('equal')
    plt.title("Azimuth versus Elevation at worst SLoW")
    plt.xlabel('Azimuth (deg)')
    plt.ylabel('Elevation (deg)')
    fig = plt.gcf()
    filepath = resume_enc_folder + results_folder + "/Azimuth_Elevation_Hazard_SLoW.png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()


    plt.figure()
    df_filtered_HAZ.plot.scatter(x="hazFirstTimeHMDPen", y="hazFirstTimeVertPen");
    plt.axis('equal')
    plt.title("H Penetration versus V Penetration at RWC violation")
    plt.xlabel('Horizontal Penetration')
    plt.ylabel('Vertical Penetration')
    fig = plt.gcf()
    filepath = resume_enc_folder + results_folder + "/HPen_VPen_Hazard_First.png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()


    plt.figure()
    df_filtered_HAZ.plot.scatter(x="hazHMDPen", y="hazVertPen");
    plt.axis('equal')
    plt.title("H Penetration versus V Penetration at worst SLoW")
    plt.xlabel('Horizontal Penetration')
    plt.ylabel('Vertical Penetration')
    fig = plt.gcf()
    filepath = resume_enc_folder + results_folder + "/HPen_VPen_Hazard_SLoW.png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(12.0, 12.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()



    df = df_filtered_HAZ[['hazFirstTimeRange', 'hazSLoWCTimeRange']]
    df.plot(kind='hist',
            alpha=0.7,
            bins=30,
            title='Histogram Aircraft Ranges at Hazard Violation and SLoWC',
            rot=45,
            grid=True,
            figsize=(12, 8),
            fontsize=15,
            color=['#A0E8AF', '#FFCF56'])
    plt.xlabel('Range (m)')
    plt.ylabel("Number Of Encounters");
    fig=plt.gcf()
    filepath = resume_enc_folder + results_folder + "/AircraftRangeAtHazard_Histogram.png"
    print("Generating figure: " + filepath)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()

    df = df_filtered_HAZ[['hazFirstTimeSLoWC', 'hazSLoWC']]
    df.plot(kind='hist',
            alpha=0.7,
            bins=30,
            title='Severity of Loss of WC',
            rot=45,
            grid=True,
            figsize=(12, 8),
            fontsize=15,
            color=['#A0E8AF', '#FFCF56'])
    plt.xlabel('SLoWC Severity')
    plt.ylabel("Number Of Encounters");
    fig=plt.gcf()
    filepath = resume_enc_folder + results_folder + "/AircraftSLoWCAtHazard_Histogram.png"
    print("Generating figure: " + filepath)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()

    df = df_filtered_HAZ[['hazFirstTimeSLoWC', 'hazSLoWC']]
    df.plot(kind = 'kde',
            title='Severity of Loss of WC',
            rot=45,
            grid=True,
            figsize=(12, 8),
            fontsize=15,
            color=['#A0E8AF', '#FFCF56'])
    plt.xlabel('SLoWC Severity')
    plt.ylabel("Number Of Encounters");
    fig=plt.gcf()
    filepath = resume_enc_folder + results_folder + "/AircraftSLoWCAtHazard_KDE.png"
    print("Generating figure: " + filepath)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()



    ax1 = df_filtered_HAZ.plot(kind='scatter', x='hazFirstTimeRange', y='hazFirstTimeSLoWC', label="at Haz Violation", color='b')
    ax2 = df_filtered_HAZ.plot(kind='scatter', x='hazSLoWCTimeRange', y='hazSLoWC', color='r', label="at Worst Severity", ax=ax1)
    plt.xlabel('Hazard Violation Range (m)')
    plt.ylabel("SLoWC severity");
    fig=plt.gcf()
    filepath = resume_enc_folder + results_folder + "/AircraftRange_vs_SLoWC_AtHazard_Scatter.png"
    print("Generating figure: " + filepath)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()

    ax1 = df_filtered_HAZ.plot(kind='scatter', x='hazFirstTimeRange', y='hazFirstTimeAzimuth', label="at Haz Violation", color='b')
    ax2 = df_filtered_HAZ.plot(kind='scatter', x='hazSLoWCTimeRange', y='hazSLoWCTimeAzimuth', color='r', label="at Worst Severity", ax=ax1)
    plt.xlabel('Hazard Violation Range (m)')
    plt.ylabel("Intruder Azimuth");
    fig=plt.gcf()
    filepath = resume_enc_folder + results_folder + "/AircraftRange_vs_Azimuth_AtHazard_Scatter.png"
    print("Generating figure: " + filepath)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()



    df_filtered_HAZ['Time_Diff_SLoWC'] = df_filtered_HAZ['haz_SLoWC_time'] - df_filtered_HAZ['hazFirstTime']
    df_filtered_HAZ['Time_Diff_Cpa'] = df_filtered_HAZ['cpaTime'] - df_filtered_HAZ['hazFirstTime']

    df = df_filtered_HAZ[['Time_Diff_SLoWC', 'Time_Diff_Cpa']]
    df.plot(kind='hist',
            alpha=0.7,
            bins=30,
            title='Encounter duration',
            rot=45,
            grid=True,
            figsize=(12, 8),
            fontsize=15,
            color=['#A0E8AF', '#FFCF56'])
    plt.xlabel('Encounter duration from LoWC to CPA or worst SLoWC.')
    fig=plt.gcf()
    filepath = resume_enc_folder + results_folder + "/EnconterDuration_Histogram.png"
    print("Generating figure: " + filepath)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()


    sys.exit(0)
