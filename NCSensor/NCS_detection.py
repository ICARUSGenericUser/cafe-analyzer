import math as math
from Encounter import units as units, position as pos
from Util import angles as ang
from RoW import RoW_computation as row


class NCS_detection:

    def __init__(self):
        #self.detectable = False
        self.range = 0
        self.azimuth = 0
        self.elevation = 0
        self.dz = 0
        self.bearing = 0
        self.beta = 0


def detect_intruder(own_pos, int_pos):
    
    ## compute relative bearing
    # 1. Get bearing between two points
    dx = int_pos.x_loc - own_pos.x_loc
    dy = int_pos.y_loc - own_pos.y_loc

    bearing = math.pi / 2 - math.atan2(dy, dx)  # radians

    # 2. Compute relative bearing considering heading
    dvx = int_pos.x_v - own_pos.x_v
    dvy = int_pos.y_v - own_pos.y_v

    heading = math.pi / 2 - math.atan2(own_pos.y_v, own_pos.x_v)  # radians
    rel_bearing = bearing - heading

    # 3. Compute ownship heading
    # ownHeading = math.pi / 2 - math.atan2(own_pos.y_v, own_pos.x_v)  # radians

    ## compute range
    dz = int_pos.altitude - own_pos.altitude  # check units. should be all meters
    range = math.sqrt(dx * dx + dy * dy + dz * dz)

    ## compute elevation
    range_2d = math.sqrt(dx * dx + dy * dy)
    elevation = math.acos(range_2d / range)  # radians

    beta = abs(ang.normalizeToAcute(own_pos.bearing - int_pos.bearing))  # conflict angle

    if dz < 0:
        elevation = -1 * elevation

    # Compute relative parameters
    detection = NCS_detection()
    detection.range = range
    detection.azimuth = ang.normalizeToAcute(units.rad_to_deg(rel_bearing))
    detection.elevation = units.rad_to_deg(elevation)
    detection.dz = dz
    detection.bearing = units.rad_to_deg(bearing)
    detection.beta = beta

    # Determine if detection occurs according to ncs_param
    detection.detectable = False

    return detection


# Computer intruder position as seen from a noisy ATAR sensor that provides
# a non accurate range, elevation and azimuth
# The function receives the true position of both ownship and intruder and generates
# the noisy intruder position
def reverse_intruder(own_pos, int_pos, errDetection):

    intrPosition = pos.Position()

    # TODO

    return intrPosition


