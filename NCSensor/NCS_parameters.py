from Encounter import units as units


class NCS_parameters:

  def __init__(self, rangeMax, rangeMin, azimuthMax, elevationUpMax, elevationDownMax):
    self.rangeMax = units.ft_to_m(rangeMax)
    self.rangeMin = units.ft_to_m(rangeMin)
    self.azimuthMax = azimuthMax
    self.elevationUpMax = elevationUpMax
    self.elevationDownMax = elevationDownMax

