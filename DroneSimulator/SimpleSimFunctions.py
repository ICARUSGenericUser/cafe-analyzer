import math

def segmentAcc(d1, speed1, delta, a):
    delta_x = (speed1 * delta) + (0.5 * a * delta * delta) # Basic MRUA formula for distance

    new_dist = d1 + delta_x # Add distance

    new_speed = math.sqrt((speed1 * speed1) + (2 * a * delta_x)) # Basic v^2 velocity formula

    return new_dist, new_speed


def segmentDec(d1, speed1, delta, d):
    delta_x = (speed1 * delta) - (0.5 * d * delta * delta) # Basic MRUA formula for distance

    new_dist = d1 + delta_x

    new_speed = math.sqrt((speed1 * speed1) - (2 * d * delta_x)) # Basic v^2 velocity formula

    return new_dist, new_speed


def segmentConst(d1, speed1, delta):
    delta_x = speed1 * delta # Basic constant speed formula

    new_dist = d1 + delta_x

    return new_dist


def minDistAcc(speed1, speed2, a):
    delta_x = abs((speed2 * speed2) - (speed1 * speed1)) / (2 * a) # Min dist using basic v^2 formula
    delta_t = (2 * delta_x) / (speed2 + speed1)

    return delta_x


def minDistDec(speed1, speed2, d):
    delta_x = abs((speed2 * speed2) - (speed1 * speed1)) / (2 * d) # Min dist using basic v^2 formula
    delta_t = (2 * delta_x) / (speed2 + speed1)

    return delta_x

def minDistConst(speed1, delta):
    delta_x = speed1 * delta  # Basic constant speed formula

    return delta_x

def newObjSpeedAccDec(Vt, Vc, a, d, available_dist):
    # Vo = math.sqrt((Vc * Vc) + ((2 * available_dist) / ((1 / a) + (1 / d))))
    Vo = math.sqrt(((2 * a * d * available_dist) + (Vc * Vc * d) + (Vt * Vt * a)) / (a + d)) # Derived from v^2 formula, first from Vc to Vo', and then from Vo' to Vt.

    return Vo


def newObjSpeedDecAcc(Vt, Vc, a, d, available_dist):
    # Vo = math.sqrt((Vc * Vc) + ((2 * available_dist) / ((1 / d) + (1 / a))))
    Vo = math.sqrt(((-2 * a * d * available_dist) + (Vc * Vc * a) + (Vt * Vt * d)) / (a + d)) # Derived from v^2 formula, first from Vc to Vo', and then from Vo' to Vt.

    return Vo


def newSingleObjSpeedAcc(Vc, a, available_dist):
    Vo = math.sqrt((2 * a * available_dist) + (Vc * Vc)) # Basic v^2 formula

    return Vo


def newSingleObjSpeedDec(Vc, d, available_dist):
    Vo = math.sqrt((2 * -d * available_dist) + (Vc * Vc)) # Basic v^2 formula

    return Vo
