import random
import tkinter
from tkinter import filedialog
from tkinter import ttk
import customtkinter

from PIL import Image, ImageTk  # Importing PIL for handling image resizing

import folium
from tkhtmlview import HTMLLabel
import tkintermapview
from tkintermapview import map_widget

primary_color = "#003083"

window = customtkinter.CTk()
window.title("Drone Performance Simulator")
window.geometry("1300x800")

window.grid_columnconfigure(0, weight=1)
window.grid_columnconfigure(1, weight=4)
window.grid_rowconfigure(0, weight=0)
window.grid_rowconfigure(1, weight=1)
window.grid_rowconfigure(2, weight=1)
window.grid_rowconfigure(3, weight=1)
window.grid_rowconfigure(4, weight=1)
window.grid_rowconfigure(5, weight=1)
window.grid_rowconfigure(6, weight=1)
window.grid_rowconfigure(7, weight=4)

# Initial coordinates for the drone
latitude = 41.390205  # Starting latitude (example: Barcelona)
longitude = 2.154007  # Starting longitude (example: Barcelona)

# Initialize global variables for the timer
running = False  # To track if the timer is running
time_counter = 0  # Time in seconds
marker = None
dronePath = [(latitude, longitude)]
path = None



# Load a custom drone icon
drone_icon_image = Image.open("drone_icon.png")  # Replace with the path to your icon
drone_icon_resized = drone_icon_image.resize((20, 20), Image.LANCZOS)  # Resize to 20x20 pixels
drone_icon = ImageTk.PhotoImage(drone_icon_resized)


def start_timer():
    global running
    if not running:  # Only start if it's not already running
        running = True
        count_time()  # Start counting


def pause_timer():
    global running
    running = False  # Stop the timer


def reset_timer():
    global running, time_counter, marker, latitude, longitude, path, dronePath
    running = False  # Stop the timer if running
    time_counter = 0  # Reset the counter
    time_label.config(text="Time: 0")  # Update the label
    latitude, longitude = 41.390205, 2.154007  # Reset to initial coordinates
    if marker:
        marker.set_position(latitude, longitude)
    if path:
        path.delete()
        dronePath = [(latitude, longitude)]


def count_time():
    global time_counter, marker, latitude, longitude, path
    if running:
        time_counter += 0.1  # Increment time counter
        time_label.config(text=f"Time: {round(time_counter, 1)}")  # Update label text

        lat_offset = random.uniform(-0.1, 0.1)  # Small random change
        lon_offset = random.uniform(-0.1, 0.1)  # Small random change
        latitude += lat_offset
        longitude += lon_offset

        # Update or set the marker position
        if marker:
            marker.set_position(latitude, longitude)
        else:
            marker = map.set_marker(latitude, longitude, icon=drone_icon)

        dronePath.append((latitude, longitude))

        # Draw or update the path
        if len(dronePath) == 2:
            # Add a line connecting the last two positions
            path = map.set_path(dronePath)
        elif len(dronePath) > 2:
            path.set_position_list(dronePath)

        # Call count_time() again after 1 second
        time_label.after(100, count_time)


# Function to open the file dialog
def open_file_perfParams():
    # Open the file dialog and store the selected file path
    file_path_perfParams = filedialog.askopenfilename(title="Select a file", filetypes=[("All files", "*.*")])

    # Update the Entry widget with the selected file path
    if file_path_perfParams:
        file_entry.delete(0, tkinter.END)  # Clear any existing text
        file_entry.insert(0, file_path_perfParams)


def open_file_flightPlan():
    # Open the file dialog and store the selected file path
    file_path_flightPlan = filedialog.askopenfilename(title="Select a file", filetypes=[("All files", "*.*")])

    # Update the Entry widget with the selected file path
    if file_path_flightPlan:
        file_entry.delete(0, tkinter.END)  # Clear any existing text
        file_entry.insert(0, file_path_flightPlan)


map = tkintermapview.TkinterMapView(window, corner_radius=2)
map.set_position(41.390205, 2.154007)
map.set_zoom(10)
map.grid(row=1, column=1, rowspan=6, sticky="nsew")

label = tkinter.Label(window, text="Drone Performance Simulator",
                      bg=primary_color,
                      fg="white",
                      font=("Calibri", 16))

label.grid(row=0, column=0, sticky="nsew")

label = tkinter.Label(window, text="Map",
                      bg="white",
                      fg=primary_color,
                      font=("Calibri", 16))

label.grid(row=0, column=1, sticky="nsew")

################################ Performance Params File #######################################
framePerfParams = tkinter.Frame(window, bg="white")
framePerfParams.grid(row=1, column=0, sticky="nsew")

perfParamsLabel = tkinter.Label(framePerfParams, text="Performance Parameters File", font=("Calibri", 14),
                                fg=primary_color, bg="white")
perfParamsLabel.pack()

# Create an Entry widget to display the selected file path
file_entry = tkinter.Entry(framePerfParams, width=50)
file_entry.pack(pady=1)

# Create a button to open the file dialog
browse_button_perfParams = tkinter.Button(framePerfParams, text="Browse", command=open_file_perfParams)
browse_button_perfParams.pack(pady=10)

################################ Flight Plan File #######################################
frameFlightPlan = tkinter.Frame(window, bg="white")
frameFlightPlan.grid(row=2, column=0, sticky="nsew")

flightPlanLabel = tkinter.Label(frameFlightPlan, text="Flight Plan File", font=("Calibri", 14), fg=primary_color,
                                bg="white")
flightPlanLabel.pack()

# Create an Entry widget to display the selected file path
file_entry = tkinter.Entry(frameFlightPlan, width=50)
file_entry.pack(pady=1)

# Create a button to open the file dialog
browse_button_flightPlan = tkinter.Button(frameFlightPlan, text="Browse", command=open_file_flightPlan)
browse_button_flightPlan.pack(pady=10)

############################## Timer ################
frameTimer = tkinter.Frame(window, bg="white")
frameTimer.grid(row=3, column=0, sticky="nsew")
# Label to display the time
time_label = tkinter.Label(frameTimer, text="Time: 0", font=("Arial", 16), bg="white", fg=primary_color)
time_label.pack(pady=20)

# Configure style for ttk buttons
style = ttk.Style()
style.configure("TButton",
                font=("Helvetica", 12, "bold"),
                foreground="black",
                background="#4CAF50",
                padding=10)

style.map("TButton",
          foreground=[("active", "white")],
          background=[("active", "#45a049")])

# Load and resize icons
icon_size = (24, 24)
start_icon = ImageTk.PhotoImage(Image.open("start.png").resize(icon_size))
pause_icon = ImageTk.PhotoImage(Image.open("pause.png").resize(icon_size))
reset_icon = ImageTk.PhotoImage(Image.open("restart.png").resize(icon_size))

# Start, Pause, and Restart buttons with icons
start_button = ttk.Button(frameTimer, text=" Start", image=start_icon, compound="left", style="TButton",
                          command=start_timer)
start_button.pack(side=tkinter.LEFT, padx=15, pady=10)

pause_button = ttk.Button(frameTimer, text=" Pause", image=pause_icon, compound="left", style="TButton",
                          command=pause_timer)
pause_button.pack(side=tkinter.LEFT, padx=15, pady=10)

reset_button = ttk.Button(frameTimer, text=" Restart", image=reset_icon, compound="left", style="TButton",
                          command=reset_timer)
reset_button.pack(side=tkinter.LEFT, padx=15, pady=10)

window.mainloop()
