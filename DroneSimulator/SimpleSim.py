import time

import numpy as np

import SimpleSimFunctions

droneParams_file = open('simParams.txt', 'r')
dronePerformance_file = open('dronePerformance.txt', 'w')

dronePerformance_file.write("Timestamp;Distance;Current Speed;Objective Speed;Final Speed\n")

# Variables
a = 0.0  # Acceleration
d = 0.0  # Deceleration
Vc = 0.0  # Current velocity
Vo = 0.0  # Objective velocity
Vt = 0.0  # Terminal velocity
dist = 0.0  # Segment distance
delta = 0.0  # Delta time
current_dist = 0.0  # Current distance
timestamp = 0.0  # Timestamp of the point
achievedVo = False  # Boolean to control if Vo is achieved or not

# In this part of the code variables from the txt file are read.
while True:
    variable = droneParams_file.readline()
    if not variable:
        break
    else:
        variable_name = variable.split(":")[0]
        variable_value = float(variable.split(":")[1])
        match variable_name:
            case "a":
                a = variable_value
            case "d":
                d = variable_value
            case "Vc":
                Vc = variable_value
            case "Vo":
                Vo = variable_value
            case "Vt":
                Vt = variable_value
            case "dist":
                dist = variable_value
            case "delta":
                delta = variable_value
            case _:
                print("Variable name ", variable_name, " not recognized")

dronePerformance_file.write("%f;%f;%f;%f;%f\n" % (timestamp, current_dist, Vc, Vo, Vt))
startTime = time.time()

# Loop until the total distance is achieved
while current_dist < dist and abs(dist - current_dist) > dist * 0.0001:

    # If Vo is not achieved it means the aircraft is in Phase A: Before Vo
    if not achievedVo:

        print("Phase A")

        # ========================== CASES A, B AND C ==================================================================
        if Vc < Vo:  # Case in which the current speed is lower than the objective

            # ================================ CASE A ======================================================
            if Vo < Vt:  # Case in which the objective speed is lower than the termination. This means two increasing lines
                print("Case A")

                phaseA_dist = SimpleSimFunctions.minDistAcc(Vc, Vo, a)  # Compute min distance from Vc to Vo
                phaseB_dist = SimpleSimFunctions.minDistAcc(Vo, Vt, a)  # Compute min distance from Vo to Vt

                min_dist = phaseA_dist + phaseB_dist  # Compute min distances to achieve Vo and Vt

                available_dist = dist - current_dist  # Available distance

                # Check if the available distance is enough or not
                if available_dist >= min_dist:

                    # Available distance is enough so the aircraft can continue accelerating
                    current_dist, Vc = SimpleSimFunctions.segmentAcc(current_dist, Vc, delta, a)

                else:

                    # Available distance is not enough so the objective velocity is changed and equals the termination one.
                    Vo = SimpleSimFunctions.newSingleObjSpeedAcc(Vc, a, available_dist)
                    Vt = Vo

                    # There is the possibility that the new velocity is lower than the current one.
                    # Then, the aircraft needs to decelerate.
                    if Vc < Vo:

                        # In this case, the Vo is still higher than Vc so the aircraft stays in case A accelerating
                        current_dist, Vc = SimpleSimFunctions.segmentAcc(current_dist, Vc, delta, a)

                    else:

                        # In this case the aircraft must decelerate, and it is considered that Vo is achieved.
                        current_dist, Vc = SimpleSimFunctions.segmentDec(current_dist, Vc, delta, d)
                        achievedVo = True

                timestamp = timestamp + delta
                dronePerformance_file.write("%f;%f;%f;%f;%f\n" % (timestamp, current_dist, Vc, Vo, Vt))

            # ================================ CASE B ======================================================
            elif Vo == Vt:  # Case in which the Vo is the same as Vt. In this case, B, after accelerating to Vo, the velocity will be constant.

                print("Case B")

                phaseA_dist = SimpleSimFunctions.minDistAcc(Vc, Vo, a)  # Compute min distance from Vc to Vo

                min_dist = phaseA_dist  # Compute min distance

                available_dist = dist - current_dist  # Compute available distance

                # Check if the available distance is enough or not
                if available_dist >= min_dist:

                    # Available distance is enough so the aircraft can continue accelerating
                    current_dist, Vc = SimpleSimFunctions.segmentAcc(current_dist, Vc, delta, a)

                else:

                    # Available distance is not enough so the objective velocity is changed and equals the termination one.
                    Vo = SimpleSimFunctions.newSingleObjSpeedAcc(Vc, a, available_dist)
                    Vt = Vo

                    # There is the possibility that the new velocity is lower than the current one.
                    # Then, the aircraft needs to decelerate.
                    if Vc < Vo:

                        # In this case, the Vo is still higher than Vc so the aircraft stays in case B accelerating
                        current_dist, Vc = SimpleSimFunctions.segmentAcc(current_dist, Vc, delta, a)

                    else:

                        # In this case the aircraft must decelerate, and it is considered that Vo is achieved.
                        current_dist, Vc = SimpleSimFunctions.segmentDec(current_dist, Vc, delta, d)
                        achievedVo = True

                timestamp = timestamp + delta
                dronePerformance_file.write("%f;%f;%f;%f;%f\n" % (timestamp, current_dist, Vc, Vo, Vt))

            # ================================ CASE C ======================================================
            else:  # Case in which the Vt is lower than the Vo

                print("Case C")

                phaseA_dist = SimpleSimFunctions.minDistAcc(Vc, Vo, a)  # Compute min distance from Vc to Vo
                phaseB_dist = SimpleSimFunctions.minDistDec(Vo, Vt, d)  # Compute min distance from Vo to Vt

                min_dist = phaseA_dist + phaseB_dist  # Compute min distance

                available_dist = dist - current_dist  # Compute available distance

                # Check if the available distance is enough or not
                if available_dist >= min_dist:

                    # Available distance is enough so the aircraft can continue accelerating
                    current_dist, Vc = SimpleSimFunctions.segmentAcc(current_dist, Vc, delta, a)

                else:

                    # Available distance is not enough so the objective velocity is changed and equals the termination one.
                    Vo = SimpleSimFunctions.newObjSpeedAccDec(Vt, Vc, a, d, available_dist)

                    # There is the possibility that the new velocity is lower than the current one.
                    # Then, the aircraft needs to decelerate.
                    if Vc < Vo:

                        # In this case, the Vo is still higher than Vc so the aircraft stays in case C accelerating
                        current_dist, Vc = SimpleSimFunctions.segmentAcc(current_dist, Vc, delta, a)

                    else:

                        # In this case the aircraft must decelerate, and it is considered that Vo is achieved.
                        current_dist, Vc = SimpleSimFunctions.segmentDec(current_dist, Vc, delta, d)
                        achievedVo = True

                timestamp = timestamp + delta
                dronePerformance_file.write("%f;%f;%f;%f;%f\n" % (timestamp, current_dist, Vc, Vo, Vt))

            # ================================ Control if Vo is achieved ===================================
            if Vc >= Vo:  # Control if Vo is achieved or not
                achievedVo = True

        # ========================== CASES D, E AND F ==================================================================
        elif Vc == Vo:  # Case in which the current speed is the same as the objective

            # ================================ CASE D ======================================================
            if Vo < Vt:  # In this case, the VO is achieved as it is the same as the current

                print("Case D")

                achievedVo = True

            # ================================ CASE E ======================================================
            elif Vo == Vt:  # In this case, the speed is constant through all the segment

                print("Case E")

                current_dist = SimpleSimFunctions.segmentConst(current_dist, Vc, delta)  # Compute distance
                timestamp = timestamp + delta

                dronePerformance_file.write("%f;%f;%f;%f;%f\n" % (timestamp, current_dist, Vc, Vo, Vt))

            # ================================ CASE F ======================================================
            else:  # In this case, the VO is achieved as it is the same as the current, case F
                print("Case F")

                achievedVo = True

        # ========================== CASES G, H AND I ==================================================================
        else:  # Case in which the current speed is higher than the objective

            # ================================ CASE G ======================================================
            if Vo < Vt:  # In this case the aircraft will decelerate during phase A and accelerate during phase B

                print("Case G")

                phaseA_dist = SimpleSimFunctions.minDistDec(Vc, Vo, d)  # Compute min distance through deceleration
                phaseB_dist = SimpleSimFunctions.minDistAcc(Vo, Vt, a)  # Compute min distance through acceleration

                min_dist = phaseA_dist + phaseB_dist  # Compute min distance needed

                available_dist = dist - current_dist  # Compute available distance

                # Check if the available distance is enough or not
                if available_dist >= min_dist:

                    # In this case, the Vo is still lower than Vc so the aircraft stays in case G decelerating
                    current_dist, Vc = SimpleSimFunctions.segmentDec(current_dist, Vc, delta, d)

                else:

                    # Available distance is not enough so the objective velocity is changed and equals the termination one.
                    Vo = SimpleSimFunctions.newObjSpeedDecAcc(Vt, Vc, a, d, available_dist)

                    # There is the possibility that the new velocity is higher than the current one.
                    # Then, the aircraft needs to accelerate.
                    if Vc < Vo:

                        # In this case, the Vo is still higher than Vc so the aircraft stays in case G accelerating
                        current_dist, Vc = SimpleSimFunctions.segmentAcc(current_dist, Vc, delta, a)
                        achievedVo = True

                    else:

                        # In this case the aircraft must decelerate, and it is considered that Vo is achieved.
                        current_dist, Vc = SimpleSimFunctions.segmentDec(current_dist, Vc, delta, d)

                timestamp = timestamp + delta
                dronePerformance_file.write("%f;%f;%f;%f;%f\n" % (timestamp, current_dist, Vc, Vo, Vt))

            elif Vo == Vt:  # In this case the aircraft decelerates and remains constant
                print("Case H")

                phaseA_dist = SimpleSimFunctions.minDistDec(Vc, Vo, d)

                min_dist = phaseA_dist

                available_dist = dist - current_dist

                if available_dist >= min_dist:
                    current_dist, Vc = SimpleSimFunctions.segmentDec(current_dist, Vc, delta, d)

                else:
                    Vo = SimpleSimFunctions.newSingleObjSpeedDec(Vc, d, available_dist)
                    Vt = Vo
                    if Vc < Vo:
                        current_dist, Vc = SimpleSimFunctions.segmentAcc(current_dist, Vc, delta, a)
                        achievedVo = True
                    else:
                        current_dist, Vc = SimpleSimFunctions.segmentDec(current_dist, Vc, delta, d)

                timestamp = timestamp + delta
                dronePerformance_file.write("%f;%f;%f;%f;%f\n" % (timestamp, current_dist, Vc, Vo, Vt))

            else:  # In this case the aircraft continues decelerating
                print("Case I")

                phaseA_dist = SimpleSimFunctions.minDistDec(Vc, Vo, d)
                phaseB_dist = SimpleSimFunctions.minDistDec(Vo, Vt, d)

                min_dist = phaseA_dist + phaseB_dist

                available_dist = dist - current_dist

                if available_dist >= min_dist:
                    current_dist, Vc = SimpleSimFunctions.segmentDec(current_dist, Vc, delta, d)


                else:
                    Vo = SimpleSimFunctions.newSingleObjSpeedDec(Vc, a, available_dist)
                    Vt = Vo
                    if Vc < Vo:
                        current_dist, Vc = SimpleSimFunctions.segmentAcc(current_dist, Vc, delta, a)
                        achievedVo = True
                    else:
                        current_dist, Vc = SimpleSimFunctions.segmentDec(current_dist, Vc, delta, d)

                timestamp = timestamp + delta
                dronePerformance_file.write("%f;%f;%f;%f;%f\n" % (timestamp, current_dist, Vc, Vo, Vt))

            if Vc <= Vo:
                achievedVo = True

    else:
        print("Phase B")

        if abs(Vc - Vt) > (delta * a) and Vc < Vt:
            print("Case A, D or G")

            phaseB_dist = SimpleSimFunctions.minDistAcc(Vc, Vt, a)

            min_dist = phaseB_dist

            available_dist = dist - current_dist

            if available_dist >= min_dist:
                phaseB_constantDist = SimpleSimFunctions.minDistConst(Vc, delta)

                min_dist2 = min_dist + phaseB_constantDist

                if available_dist >= min_dist2:
                    current_dist = SimpleSimFunctions.segmentConst(current_dist, Vc, delta)
                else:
                    current_dist, Vc = SimpleSimFunctions.segmentAcc(current_dist, Vc, delta, a)

            else:
                Vt = SimpleSimFunctions.newSingleObjSpeedAcc(Vc, a, available_dist)
                if Vc < Vt:
                    current_dist, Vc = SimpleSimFunctions.segmentAcc(current_dist, Vc, delta, a)
                else:
                    current_dist, Vc = SimpleSimFunctions.segmentDec(current_dist, Vc, delta, d)

            timestamp = timestamp + delta
            dronePerformance_file.write("%f;%f;%f;%f;%f\n" % (timestamp, current_dist, Vc, Vo, Vt))
        elif abs(Vc - Vt) > (delta * d) and Vc > Vt:
            print("Case C, F or I")

            phaseB_dist = SimpleSimFunctions.minDistDec(Vc, Vt, d)

            min_dist = phaseB_dist

            available_dist = dist - current_dist

            if available_dist >= min_dist:

                phaseB_constantDist = SimpleSimFunctions.minDistConst(Vc, delta)

                min_dist2 = min_dist + phaseB_constantDist

                if available_dist >= min_dist2:
                    current_dist = SimpleSimFunctions.segmentConst(current_dist, Vc, delta)
                else:
                    current_dist, Vc = SimpleSimFunctions.segmentDec(current_dist, Vc, delta, d)

            else:
                Vt = SimpleSimFunctions.newSingleObjSpeedDec(Vc, d, available_dist)
                if Vc < Vt:
                    current_dist, Vc = SimpleSimFunctions.segmentAcc(current_dist, Vc, delta, a)
                else:
                    current_dist, Vc = SimpleSimFunctions.segmentDec(current_dist, Vc, delta, d)

            timestamp = timestamp + delta
            dronePerformance_file.write("%f;%f;%f;%f;%f\n" % (timestamp, current_dist, Vc, Vo, Vt))
        else:
            print("Case B, E or H")
            current_dist = SimpleSimFunctions.segmentConst(current_dist, Vc, delta)
            timestamp = timestamp + delta

            dronePerformance_file.write("%f;%f;%f;%f;%f\n" % (timestamp, current_dist, Vc, Vo, Vt))

endTime = time.time()

elapsedTime = endTime - startTime

print("Elapsed time: %f" % elapsedTime)

dronePerformance_file.write("============== Elapsed time: %f s =================" % elapsedTime)
