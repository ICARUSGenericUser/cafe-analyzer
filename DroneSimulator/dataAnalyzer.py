import numpy as np
import matplotlib.pyplot as plt

data = np.genfromtxt('dronePerformance.txt', delimiter=';', dtype=float, skip_header=1, skip_footer=1)

timestamp = data[:, 0]         # Timestamp
distance = data[:, 1]          # Distance
current_speed = data[:, 2]     # Current Speed
objective_speed = data[:, 3]   # Objective Speed
final_speed = data[:, 4]       # Final Speed

# Plot each parameter against Timestamp
plt.figure(figsize=(10, 12))

# Plot Distance
plt.subplot(5, 1, 1)
plt.plot(timestamp, distance, label="Distance", color="blue")
plt.ylabel("Distance")
plt.legend()

# Plot Current Speed
plt.subplot(5, 1, 2)
plt.plot(timestamp, current_speed, label="Current Speed", color="green")
plt.ylabel("Current Speed")
plt.legend()

# Plot Objective Speed
plt.subplot(5, 1, 3)
plt.plot(timestamp, objective_speed, label="Objective Speed", color="red")
plt.ylabel("Objective Speed")
plt.legend()

# Plot Final Speed
plt.subplot(5, 1, 4)
plt.plot(timestamp, final_speed, label="Final Speed", color="purple")
plt.ylabel("Final Speed")
plt.legend()

# Plot Current, Objective, and Final Speed together
plt.subplot(5, 1, 5)
plt.plot(timestamp, current_speed, label="Current Speed", color="green")
plt.plot(timestamp, objective_speed, label="Objective Speed", color="red", linestyle="--")
plt.plot(timestamp, final_speed, label="Final Speed", color="purple", linestyle="-.")
plt.xlabel("Timestamp")
plt.ylabel("Speed")
plt.legend()

# Show the plot
plt.tight_layout()
plt.show()