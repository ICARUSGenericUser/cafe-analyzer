# This is a sample Python script.
import os, shutil
import sys
from time import time
from datetime import datetime
import pandas as pd
import numpy as np
from Encounter import position as pos, encounter as enc, encounter_task as et
from Encounter_Distribution import Encounter_Parameters as ep
from Metrics import Enc_Alert_Measuring as mea, Alert_Measuring as am
from Projection import FlatEarthProjection as proj

#df_dtype = {
#        "Encounter_Distribution No.": int,
#        "Seed": int,
#        "Layer": int,
#        "VMD": np.float64,
#        "HMD": np.float64,
#        "Approach Angle": np.float64,
#        "Bearing from North": np.float64,
#        "Time before CPA": int,
#        "Time after CPA": int,
#        "AC1 Mode A": str,
#        "AC1 Callsign": str,
#        "AC1 Class": int,
#        "AC1 Controlled?": bool,
#        "AC1 Altitude": np.float64,
#        "AC1 Vertical": np.float64,
#        "AC1 Speed": np.float64,
#        "AC1 Acceleration": np.float64,
#        "AC1 Heading": np.float64,
#        "AC1 Turn": np.float64,
#        "AC2 Mode A": str,
#        "AC2 Callsign": str,
#        "AC2 Class": int,
#        "AC2 Controlled?": bool,
#        "AC2 Altitude": np.float64,
#        "AC2 Vertical": np.float64,
#        "AC2 Speed": np.float64,
#        "AC2 Acceleration": np.float64,
#        "AC2 Heading": np.float64,
#        "AC2 Turn": np.float64,
#}


df_dtype = {
        "Encounter No.": np.float64,
        "Seed": np.float64,
        "Layer": np.float64,
        "VMD": np.float64,
        "HMD": np.float64,
        "Approach Angle": np.float64,
        "Bearing from North": np.float64,
        "Time before CPA": np.float64,
        "Time after CPA": np.float64,
        "AC1 Mode A": str,
        "AC1 Callsign": str,
        "AC1 Class": np.float64,
        "AC1 Controlled?": bool,
        "AC1 Altitude": np.float64,
        "AC1 Vertical Rate": np.float64,
        "AC1 Speed": np.float64,
        "AC1 Acceleration": np.float64,
        "AC1 Heading": np.float64,
        "AC1 Turn": np.float64,
        "AC2 Mode A": str,
        "AC2 Callsign": str,
        "AC2 Class": np.float64,
        "AC2 Controlled?": bool,
        "AC2 Altitude": np.float64,
        "AC2 Vertical Rate": np.float64,
        "AC2 Speed": np.float64,
        "AC2 Acceleration": np.float64,
        "AC2 Heading": np.float64,
        "AC2 Turn": np.float64,
}


trj_eu_dtype = {
        "time": str,
        "aircraft":  np.int32,
        "Mode A": np.int32,
        "x-position": np.float64,
        "y-position": np.float64,
        "altitude": np.float64,
        "callsign": str,
}


trj_ftd_dtype = {
        "time": str,
        "aircraft":  np.int32,
        "x-position": np.float64,
        "y-position": np.float64,
        "altitude": np.float64,
        "callsign": str,
}


trj_eu_columns = [
        "time",
        "aircraft",
        "Mode A",
        "x-position",
        "y-position",
        "altitude",
        "callsign",
]


trj_ftd_columns = [
        "time",
        "aircraft",
        "x-position",
        "y-position",
        "altitude",
        "BDS-30",
]

df_columns = [
        "Encounter No.",
        "Seed",
        "Layer",
        "VMD",
        "HMD",
        "Approach Angle",
        "Bearing from North",
        "Time before CPA",
        "Time after CPA",
        "AC1 Mode A",
        "AC1 Callsign",
        "AC1 Class",
        "AC1 Controlled?",
        "AC1 Altitude",
        "AC1 Vertical",
        "AC1 Speed",
        "AC1 Acceleration",
        "AC1 Heading",
        "AC1 Turn",
        "AC2 Mode A",
        "AC2 Callsign",
        "AC2 Class",
        "AC2 Controlled?",
        "AC2 Altitude",
        "AC2 Vertical",
        "AC2 Speed",
        "AC2 Acceleration",
        "AC2 Heading",
        "AC2 Turn"
]

def processTrajectoryFile(full_encounter_path, full_diagram_path, enc_type, encounter, encounter_param):
    aircraftVector = None

    print("Reading a trajectory...")

    if encounter.enc_format == "eu":
        trj_df = pd.read_csv(full_encounter_path, delimiter='\t', names=trj_eu_columns, dtype=trj_eu_dtype)

        for index, row in trj_df.iterrows():
            if row['aircraft'] == 1:
                position = pos.Position(row['aircraft'], row['time'], row['x-position'], row['y-position'], row['altitude'])
                encounter.addAircraft1Position(position)
            elif row['aircraft'] == 2:
                position = pos.Position(row['aircraft'], row['time'], row['x-position'], row['y-position'], row['altitude'])
                encounter.addAircraft2Position(position)


    if encounter.enc_format == "ftd":
        skip = 0
        trj_df = pd.read_csv(full_encounter_path, header=None, sep='\n', engine='python')

        print("Splitting columns...")
        trj_df = trj_df[0].str.split('\s*,\s*', expand=True)

        # Assign column names
        # df.columns = df_columns

        val = trj_df[0][skip]
        if trj_df[0][skip] != "HEADER":
            return

        skip += 1
        if trj_df[0][skip] != "synthetic":
            return

        encounter.nAircraft = int(trj_df[1][skip])
        skip += encounter.nAircraft

        skip += 1
        v = trj_df[0][skip]
        if trj_df[0][skip] != "BODY":
            return

        skip += 1

        print("Renaming columns...")
        i = 0
        while i < len(trj_ftd_columns):
            trj_df.rename(columns={i: trj_ftd_columns[i]}, inplace=True)
            i += 1

        for index, row in trj_df.iterrows():
            if index < skip:
                continue

            if int(row['aircraft']) == 1:
                position = pos.Position(int(row['aircraft']), float(row['time']), float(row['x-position']), float(row['y-position']), float(row['altitude']))
                encounter.addAircraft1Position(position)
            elif int(row['aircraft']) == 2:
                position = pos.Position(int(row['aircraft']), float(row['time']), float(row['x-position']), float(row['y-position']), float(row['altitude']))
                encounter.addAircraft2Position(position)

            if int(row['aircraft']) == 1:
                aircraftVector = np.array([None] * encounter.nAircraft)

            position = pos.Position(int(row['aircraft']), float(row['time']), float(row['x-position']), float(row['y-position']), float(row['altitude']))
            aircraftVector[int(row['aircraft']) - 1] = position

            if int(row['aircraft']) == encounter.nAircraft:
                encounter.addAircraftVectorPosition(aircraftVector)


    if (encounter.numberAircraft1Position() == encounter.numberAircraft2Position()):
    #    print("Trajectory seems consistent with : " + str(encounter.numberAircraft1Position()) + " positions.\n")

        encounter.encAlertMeasuringAicraft1 = arcf1_eam = mea.Enc_Alert_Measuring()
        encounter.encAlertMeasuringAicraft2 = arcf2_eam = mea.Enc_Alert_Measuring()

        encounter.computeDynamics()
        encounter.computeConflictsCAFE(full_diagram_path, encounter_param, arcf1_eam, arcf2_eam)

        if encounter_param.debug and arcf1_eam.active_conflict:
            print("Reporting on aircraft 1:")
            arcf1_eam.report_Alert_Measuring()
        if encounter_param.debug and arcf2_eam.active_conflict:
            print("\nReporting on aircraft 2:")
            arcf2_eam.report_Alert_Measuring()

        if encounter_param.mustPlot:
            encounter.plotEncounterCAFE(full_diagram_path)

        print("Done.")
        return None
    else:
        print("Trajectory seems NOT consistent.")
        return None



def processEncounterSet(encTask, encounter_param):

    subfolder = 1
    while (subfolder <= MAX_ENCOUNTER_FOLDERS):

        if encTask.enc_format == "eu":
            full_encounter_path = encTask.task_enc_folder + "\\" + str(subfolder) + "\\" + "Encs_" + str(encTask.enc_number).zfill(9) + ".eu1"
        if encTask.enc_format == "ftd":
            full_encounter_path = encTask.task_enc_folder + "\\" + str(subfolder) + "\\" + "Encs_" + str(encTask.enc_number).zfill(9) + ".ftd"

        print("Checking Encounter_Distribution file: ", full_encounter_path)

        if os.path.exists(full_encounter_path):
            found = True
            #start = datetime.now()
            start = time()
            print("Encounter_Distribution file: ", full_encounter_path, "  Identified, processing...")
            encounter = enc.Encounter(encTask.enc_number, encTask.enc_layer, encTask.enc_type, encTask.aircraft1Class, encTask.aircraft2Class, encTask.enc_format)
            processTrajectoryFile(full_encounter_path, encTask.diagram_folder, enc_type, encounter, encounter_param)
            #duration = (datetime.now().microsecond - start.microsecond) / 1000**2
            duration = (time() - start)
            print("Encounter_Distribution processed in: ", "{:.2f}".format(duration) , " sec")
            return encounter

        subfolder += 1

    print("Encounter_Distribution number: ", enc_number, " cannot be found")

    return None


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # creating the name

    MAX_ENCOUNTER_FOLDERS = 2
    ENCOUNTER_TYPE = ["EE", "EU", "UU"]
    found = 0
    notFound = 0

    # Start time of computing
    start = time()

    # Options: Select a certain max number of encounters to debug
    maxProcess = 100
    # maxProcess = 10

    # Options: Select a certain encounter in order to debug
    #select_enc_number = None
    select_enc_number = None

    # Options: export in MOPS format
    writeMOPS = False

    # Options: location of encounters and parameters CSV file
    resume_enc_folder = "E:\CAFE-Encounters\CAFE-Test-Sets\CAFE_Stress_Set_2\\"
    resume_file = "CAFE_Stress_Set_2_for_ACASII_Enc_Params.csv"

    Prefix_Name = "CAFE_Stress_Set_2"

    result_file = Prefix_Name + "_Results.csv"
    prov_file = Prefix_Name + "_Results.txt"
    diagram_subfolder = Prefix_Name + "_Diagrams"
    MOPS_subfolder = Prefix_Name + "_MOPS"
    print("Analyzing " + Prefix_Name + " encounters...")


    resume_enc_file = resume_enc_folder + resume_file
    result_enc_filename = resume_enc_folder + result_file
    enc_type = "EE"

    print("Analyzing encounters selected in resume encounter file: \n\t\t", resume_enc_file, "\n")

    if not os.path.exists(resume_enc_file):
        print("Encounter_Distribution file: ", resume_enc_file, "\n")
        print("Does not exists, stopping...\n")
        exit(-1)

    # Options: set the encounter parameters
    print("Setting encounter parameters to DO365A")
    encounter_param = ep.Encounter_Parameters()
    encounter_param.set_DO365A_parameters()

    #print("Setting standard TCASII parameters")
    #encounter_param.set_TCASII_parameters()

    # Options: plot the encounters
    encounter_param.mustPlot = True
    # Options: activate debug
    encounter_param.debug = True
    # Options: plot contours for the conflict sequence
    encounter_param.plotContour = False


    print("Generating encounter results on file: \n\t\t", result_enc_filename, "\n")
    encounterResume = am.Alert_Measuring()


    diagram_folder = resume_enc_folder + "\\" + diagram_subfolder
    task_enc_folder = resume_enc_folder + "\\" + enc_type

    if os.path.exists(resume_enc_folder + "\\" + diagram_subfolder):
        print("Renaming folder...\n")
        if os.path.exists(resume_enc_folder + "\\" + diagram_subfolder + ".bak"):
            shutil.rmtree(resume_enc_folder + "\\" + diagram_subfolder + ".bak")
            # os.rmdir(resume_enc_folder + "\\" + diagram_subfolder + ".bak")
        os.rename(resume_enc_folder + "\\" + diagram_subfolder, resume_enc_folder + "\\" + diagram_subfolder + ".bak")
        os.mkdir(resume_enc_folder + "\\" + diagram_subfolder)
    else:
        os.mkdir(resume_enc_folder + "\\" + diagram_subfolder)

    if writeMOPS:
        if os.path.exists(resume_enc_folder + "\\" + MOPS_subfolder):
            shutil.rmtree(resume_enc_folder + "\\" + MOPS_subfolder)
        os.mkdir(resume_enc_folder + "\\" + MOPS_subfolder)
        projection = proj.FlatEarthProjection(1.9850292, 41.2755222, 0)

    result_enc_file = open(result_enc_filename, "w")

    df_chunk = pd.read_csv(resume_enc_file, delimiter=',', chunksize=10000, dtype=df_dtype)

    for chunk in df_chunk:

        # Limit the process to a certain number of encounters
        # Useful for debugging purposes
        if maxProcess > 0 and found == maxProcess:
            break

        print("\nProcessing new chunk...")

        chunk['Encounter_Distribution No.'] = chunk['Encounter_Distribution No.'].astype(int)
        chunk['Seed'] = chunk['Seed'].astype(int)
        chunk['Layer'] = chunk['Layer'].round().astype(np.int)
        chunk['AC1 Class'] = chunk['AC1 Class'].round().astype(np.int)
        chunk['AC2 Class'] = chunk['AC2 Class'].round().astype(np.int)

        print("Performing analysis...\n")

        for index, row in chunk.iterrows():

            # Limit the process to a certain number of encounters
            # Useful for debugging purposes
            if maxProcess > 0 and found == maxProcess:
                break

            #
            # Now, we should get an encounter line
            # We need to check is the associated file exists before starting any process

            enc_number = row['Encounter_Distribution No.']
            aircraft1Class = row['AC1 Class']
            aircraft2Class = row['AC2 Class']
            enc_layer = row['Layer']

            #
            # Filter configured to be able to debug a certain trajectory
            if (select_enc_number != None) and (select_enc_number != int(enc_number)):
                continue

            print("\nProcessing encounter number: ", enc_number, " with Class1 aircraft: ", aircraft1Class, " with Class2 aircraft: ", aircraft2Class)
            enc_task = et.encounter_task(task_enc_folder, diagram_folder, enc_layer, enc_type, enc_number, aircraft1Class, aircraft2Class, "ftd")
            encounter = processEncounterSet(enc_task, encounter_param)

            if encounter != None:
                encounter.writeEncounterResult(result_enc_file)
                encounterResume.addEncounter()
                encounterResume.addConflict(encounter.encAlertMeasuringAicraft1.active_conflict, encounter.encAlertMeasuringAicraft2.active_conflict)
                encounterResume.addNMAC(encounter.encAlertMeasuringAicraft1.nmac_violated, encounter.encAlertMeasuringAicraft2.nmac_violated)
                encounterResume.addHazard(encounter.encAlertMeasuringAicraft1.haz_violated, encounter.encAlertMeasuringAicraft2.haz_violated)
                encounterResume.addWarning(encounter.encAlertMeasuringAicraft1.warn_type, encounter.encAlertMeasuringAicraft2.warn_type)
                encounterResume.addCorrective(encounter.encAlertMeasuringAicraft1.caution_type, encounter.encAlertMeasuringAicraft2.caution_type)
                encounterResume.addPreventive(encounter.encAlertMeasuringAicraft1.prev_type, encounter.encAlertMeasuringAicraft2.prev_type)

                #encounterResume.addTimming(encounter.encAlertMeasuringAicraft1)
                encounterResume.addTimming(encounter, encounter.encAlertMeasuringAicraft2)
                found += 1

                if writeMOPS:
                    encounter.writeMOPSFormat(resume_enc_folder + "\\" + MOPS_subfolder, projection)

                duration = (time() - start)
                print("Total encounters processed: ", found, " in: ", "{:.2f}".format(duration), " sec")
            else:
                notFound += 1

    result_enc_file.close()
    print("Done.\n")

    duration = (time() - start)
    encounterResume.writeReport(resume_enc_folder + prov_file, duration)
    print("Found: " + str(found) + " Not found: " + str(notFound))
    sys.exit(0)
