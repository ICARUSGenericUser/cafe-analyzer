# This is a sample Python script.
import os, shutil
import sys
from time import time
from datetime import datetime
import pandas as pd
import numpy as np
from Encounter import position as pos, encounter as enc, encounter_task as et
from Encounter_Distribution import Encounter_Parameters as ep
from Metrics import Enc_Alert_Measuring as mea, Alert_Measuring as am
from TCAS import TCAS3D



# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # creating the name

    # Options: set the encounter parameters
    print("Setting encounter parameters to DO365A")
    encounter_param = ep.Encounter_Parameters()
    encounter_param.set_DO365A_parameters()

    print("Setting standard TCASII parameters")
    encounter_param.set_TCASII_parameters()

    # Options: plot the encounters
    encounter_param.mustPlot = True
    # Options: activate debug
    encounter_param.debug = True

    arcf1 = pos.Position(1, 0, 0, 10000)
    arcf1.assignDynamics(-100, 0, 0, 0)

    arcf2 = pos.Position(1, 1, 0, 10000)
    arcf2.assignDynamics(100, 0, 0, 0)

    # Set time detection range
    B = 0
    T = encounter_param.look_ahead_time

    if encounter_param.tcasii_actv:
        #
        # AIRCRAFT1 TCAS analysis
        #
        tcasInterval = TCAS3D.RA3D_interval(arcf1, arcf2, B, T, encounter_param.TCAS_Table)

        if tcasInterval.conflict():
            print("TCAS Aircraft1 -> Aircraft2")
            print("Interval Low: ", tcasInterval.interval.low, " Up: ", tcasInterval.interval.up)
            print("Time to cpa: ", tcasInterval.time_crit, " Distance at cpa: ", tcasInterval.dist_crit)

        #
        # AIRCRAFT2 TCAS analysis
        #
        tcasInterval = TCAS3D.RA3D_interval(arcf2, arcf1, B, T, encounter_param.TCAS_Table)

        if tcasInterval.conflict():
            print("TCAS Aircraft2 -> Aircraft1 at sequence Idx: ", time)
            print("Interval Low: ", tcasInterval.interval.low, " Up: ", tcasInterval.interval.up)
            print("Time to cpa: ", tcasInterval.time_crit, "Distance at cpa: ", tcasInterval.dist_crit)

    sys.exit(0)

