# This is a sample Python script.
import os, shutil
import sys
from STCA import STCA_core
from time import time
from datetime import datetime
import pandas as pd
import numpy as np
from Encounter import position as pos, encounter as enc, encounter_task as et
from Encounter_Distribution import Encounter_Parameters as ep
from Metrics import Enc_Alert_Measuring as mea, Alert_Measuring as am
from TCAS import TCAS3D



# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # creating the name
    stca = STCA_core.STCA_core(0)

    so = np.array([100, 0, 0])
    si = np.array([100, 0, 0])
    vo = np.array([10, 0, 0])
    vi = np.array([10, 0, 0])

    isHorizontalConflict = stca.compute_horizontalPrediction(so, si, vo, vi)
    if isHorizontalConflict:
        print("----------------------------------------------------------------------------------------------")
        print("STCA LINEAR HORIZONTAL PREDICTION")
        print("----------------------------------------------------------------------------------------------")
        print("Times:")
        print("TAU CPA: ", stca.tau_pca)
        print("TAU start: ", stca.tau_ths)
        print("TAU end: ", stca.tau_the)
        print("----------------------------------------------------------------------------------------------")
        print("Positions:")
        print("Ownship at CPA: X: ", stca.so_hor_pca[0], ", Y: ", stca.so_hor_pca[1], ", Z: ", stca.so_hor_pca[2])
        print("Intruder at CPA: X: ", stca.si_hor_pca[0], ", Y: ", stca.si_hor_pca[1], ", Z: ", stca.si_hor_pca[2])
        print("Ownship at conflict start: X: ", stca.so_hor_ths[0], ", Y: ", stca.so_hor_ths[1], ", Z: ", stca.so_hor_ths[2])
        print("Intruder at conflict start: X: ", stca.si_hor_ths[0], ", Y: ", stca.si_hor_ths[1], ", Z: ", stca.si_hor_ths[2])
        print("Ownship at conflict exit: X: ", stca.so_hor_the[0], ", Y: ", stca.so_hor_the[1], ", Z: ", stca.so_hor_the[2])
        print("Intruder at conflict exit: X: ", stca.si_hor_the[0], ", Y: ", stca.si_hor_the[1], ", Z: ", stca.si_hor_the[2])
    else:
        print("----------------------------------------------------------------------------------------------")
        print("STCA LINEAR HORIZONTAL PREDICTION")
        print("----------------------------------------------------------------------------------------------")
        print("HORIZONTAL CONFLICT NOT DETECTED")

    isVerticalConflict = stca.compute_verticalPrediction(so, si, vo, vi, 0)
    if isVerticalConflict and (stca.so_vert_pca[2] != 0 or stca.si_vert_pca[2] != 0):

        print("----------------------------------------------------------------------------------------------")
        print("STCA LINEAR VERTICAL PREDICTION")
        print("----------------------------------------------------------------------------------------------")
        print("Times:")
        print("TAU CPA: ", stca.tau_vert_cpa)
        print("TAU start: ", stca.tau_vert_entry)
        print("TAU end: ", stca.tau_vert_exit)
        print("----------------------------------------------------------------------------------------------")
        print("Positions:")
        print("Ownship at CPA: X: ", stca.so_vert_pca[0], ", Y: ", stca.so_vert_pca[1], ", Z: ", stca.so_vert_pca[2])
        print("Intruder at CPA: X: ", stca.si_vert_pca[0], ", Y: ", stca.si_vert_pca[1], ", Z: ", stca.si_vert_pca[2])
        print("Ownship at conflict start: X: ", stca.so_vert_ths[0], ", Y: ", stca.so_vert_ths[1], ", Z: ", stca.so_vert_ths[2])
        print("Intruder at conflict start: X: ", stca.si_vert_ths[0], ", Y: ", stca.si_vert_ths[1], ", Z: ", stca.si_vert_ths[2])
        print("Ownship at conflict exit: X: ", stca.so_vert_the[0], ", Y: ", stca.so_vert_the[1], ", Z: ", stca.so_vert_the[2])
        print("Intruder at conflict exit: X: ", stca.si_vert_the[0], ", Y: ", stca.si_vert_the[1], ", Z: ", stca.si_vert_the[2])
    elif isVerticalConflict:
        print("----------------------------------------------------------------------------------------------")
        print("STCA LINEAR VERTICAL PREDICTION")
        print("----------------------------------------------------------------------------------------------")
        print("VERTICAL CONFLICT DETECTED REGARDLESS TIME")

    else:
        print("----------------------------------------------------------------------------------------------")
        print("STCA LINEAR VERTICAL PREDICTION")
        print("----------------------------------------------------------------------------------------------")
        print("VERTICAL CONFLICT NOT DETECTED REGARDLESS TIME")

    if not isVerticalConflict:
        print("----------------------------------------------------------------------------------------------")
        print("STCA LINEAR OVERLAP PREDICTION")
        print("----------------------------------------------------------------------------------------------")
        print("NO OVERLAP SINCE NO VERTICAL CONFLICT")
    elif not isHorizontalConflict:
        print("----------------------------------------------------------------------------------------------")
        print("STCA LINEAR OVERLAP PREDICTION")
        print("----------------------------------------------------------------------------------------------")
        print("NO OVERLAP SINCE NO HORIZONTAL CONFLICT")
    else:
        if stca.compute_violationOverlap(so, si, vo, vi):
            print("----------------------------------------------------------------------------------------------")
            print("STCA LINEAR OVERLAP PREDICTION")
            print("----------------------------------------------------------------------------------------------")
            print("Times:")
            print("TAU start: ", stca.tau_conf_entry)
            print("TAU end: ", stca.tau_conf_exit)
            print("----------------------------------------------------------------------------------------------")
            print("Positions:")
            print("Ownship at conflict start: X: ", stca.si_conf_entry[0], ", Y: ", stca.si_conf_entry[1], ", Z: ", stca.si_conf_entry[2])
            print("Intruder at conflict start: X: ", stca.so_conf_entry[0], ", Y: ", stca.so_conf_entry[1], ", Z: ", stca.so_conf_entry[2])
            print("Ownship at conflict exit: X: ", stca.so_conf_entry[0], ", Y: ", stca.so_conf_entry[1], ", Z: ", stca.so_conf_entry[2])
            print("Intruder at conflict exit: X: ", stca.si_vert_the[0], ", Y: ", stca.si_vert_the[1], ", Z: ", stca.si_vert_the[2])
        else:
            print("----------------------------------------------------------------------------------------------")
            print("STCA LINEAR OVERLAP PREDICTION")
            print("----------------------------------------------------------------------------------------------")
            print("NO OVERLAP")

    stca.compute_linearPredictionAlertConfirmation()
    sys.exit(0)
