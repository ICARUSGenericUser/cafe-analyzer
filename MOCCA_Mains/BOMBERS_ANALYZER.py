import pandas as pd
import matplotlib.pyplot as plt
import folium
from folium.plugins import MarkerCluster
import seaborn as sns

df2023 = pd.read_excel("D:\Bombers\Vols 23-24-25\FlightOperationResume-BOMBERS-GROS-2023.xlsx")
df2024 = pd.read_excel("D:\Bombers\Vols 23-24-25\FlightOperationResume-BOMBERS-GROS-2024.xlsx")
df2025 = pd.read_excel("D:\Bombers\Vols 23-24-25\FlightOperationResume-BOMBERS-GROS-2025.xlsx")

df_combined = pd.concat([df2023, df2024, df2025], axis=0, ignore_index=True)

UPCPlates = ['BOMBERS-57.00', 'BOMBERS-57.02', 'BOMBERS-57.03']

filtered_data = []

i = 0
while i < len(df_combined):
    if df_combined.loc[i, 'Plate'] in UPCPlates:
        num_flights = int(df_combined.loc[i, 'NumFlights'])
        i += num_flights + 1  # Skip the operation row and its related flight rows
    else:
        num_flights = int(df_combined.loc[i, 'NumFlights'])

        if num_flights != 0:
            filtered_data.append(df_combined.loc[i])

        for j in range(1, num_flights + 1):
            flight_row = df_combined.loc[i + j].copy()
            flight_row['Plate'] = df_combined.loc[
                i, 'Plate']  # Associate flight row with the plate from the operation row
            flight_row['Date'] = df_combined.loc[
                i, 'Date']
            filtered_data.append(flight_row)
            flight_row['Model'] = df_combined.loc[
                i, 'Model']
        i += num_flights + 1

df_filtered = pd.DataFrame(filtered_data)

#============= Num operations per month =============

# Convert the 'Date' column to datetime format with the specified format
df_filtered['Date'] = pd.to_datetime(df_filtered['Date'], format='%d-%m-%Y-%H-%M')

# Extract the month and year from the 'Date' column
df_filtered['YearMonth'] = df_filtered['Date'].dt.to_period('M')

# Filter only the operation rows (where 'NumFlights' is not NaN)
operation_rows = df_filtered[df_filtered['NumFlights'].notna()]
operation_rows = operation_rows[operation_rows['Lat'].notna()]

# Count the number of operations each month
monthly_operations = operation_rows['YearMonth'].value_counts().sort_index()

# Create a complete index of all months in the range
all_months = pd.period_range(start=monthly_operations.index.min(), end=monthly_operations.index.max(), freq='M')

# Reindex the monthly_operations to include all months, filling missing values with 0
monthly_operations = monthly_operations.reindex(all_months, fill_value=0)

# Plot the simple number of operations each month
plt.figure(figsize=(10, 6))
monthly_operations.plot(kind='bar')
plt.title('Number of Operations Each Month')
plt.xlabel('Month')
plt.ylabel('Number of Operations')
plt.xticks(rotation=45)
plt.tight_layout()
plt.show()

# Plot the accumulated number of operations each month
plt.figure(figsize=(10, 6))
monthly_operations.cumsum().plot(kind='bar')
plt.title('Accumulated Number of Operations Each Month')
plt.xlabel('Month')
plt.ylabel('Accumulated Number of Operations')
plt.xticks(rotation=45)
plt.tight_layout()
plt.show()

#============= Num flights per month =============

# Sum the number of flights each month
monthly_flights = df_filtered.groupby('YearMonth')['NumFlights'].sum().sort_index()

# Create a complete index of all months in the range
all_months = pd.period_range(start=monthly_flights.index.min(), end=monthly_flights.index.max(), freq='M')

# Reindex the monthly_flights to include all months, filling missing values with 0
monthly_flights = monthly_flights.reindex(all_months, fill_value=0)

# Plot the simple number of flights each month
plt.figure(figsize=(10, 6))
monthly_flights.plot(kind='bar')
plt.title('Number of Flights Each Month')
plt.xlabel('Month')
plt.ylabel('Number of Flights')
plt.xticks(rotation=45)
plt.tight_layout()
plt.show()

# Plot the accumulated number of flights each month
plt.figure(figsize=(10, 6))
monthly_flights.cumsum().plot(kind='bar')
plt.title('Accumulated Number of Flights Each Month')
plt.xlabel('Month')
plt.ylabel('Accumulated Number of Flights')
plt.xticks(rotation=45)
plt.tight_layout()
plt.show()

#============ Num flights per plate =============
# Sum the number of flights per Plate
flights_per_plate_model = df_filtered.groupby(['Plate', 'Model'])['NumFlights'].sum().sort_values()

x_labels = [f"{plate}\n{model}" for plate, model in flights_per_plate_model.index]

# Plot the number of flights per Plate
plt.figure(figsize=(10, 6))
flights_per_plate_model.plot(kind='bar')
plt.title('Number of Flights per Plate')
plt.xlabel('Plate')
plt.ylabel('Number of Flights')
plt.xticks(ticks=range(len(x_labels)), labels=x_labels, rotation=45)
plt.tight_layout()
plt.show()

#============ Flight Time per plate =============
# Sum the number of flights per Plate
flights_per_plate_model = df_filtered.groupby(['Plate', 'Model'])['OperationTime'].sum().sort_values()



# Plot the number of flights per Plate
plt.figure(figsize=(10, 6))
flights_per_plate_model.plot(kind='bar')
plt.title('Flight Time per Plate')
plt.xlabel('Plate')
plt.ylabel('Total Flight Time (s)')
plt.xticks(ticks=range(len(x_labels)), labels=x_labels, rotation=45)
plt.tight_layout()
plt.show()

#=========== Distance per plate =============
# Sum the total distance for each plate
total_distance_per_plate_model = df_filtered.groupby(['Plate', 'Model'])['HDistance'].sum().sort_values()

# Plot the total distance per Plate
plt.figure(figsize=(10, 6))
total_distance_per_plate_model.plot(kind='bar')
plt.title('Total Distance per Plate')
plt.xlabel('Plate')
plt.ylabel('Total Distance (m)')
plt.xticks(ticks=range(len(x_labels)), labels=x_labels, rotation=45)
plt.tight_layout()
plt.show()

#============ Operations map =============
m = folium.Map(location=[41.82, 1.67], zoom_start=8, tiles='OpenStreetMap')
marker_cluster = MarkerCluster().add_to(m)

for idx, row in operation_rows.iterrows():
    folium.Marker(
        location=[row['Lat'], row['Lon']],
        popup=f"Plate: {row['Plate']}<br>NumFlights: {row['NumFlights']}",
    ).add_to(marker_cluster)

m.save('operations_map.html')

#=========== Time Capacity evolution =================
# Filter only the flight rows (where 'NumFlights' is NaN)
flight_rows = df_filtered[df_filtered['NumFlights'].isna()]

# Convert the 'Date' column to numeric values (days since a reference date)
flight_rows['DateNumeric'] = (flight_rows['Date'] - flight_rows['Date'].min()).dt.days

# Plot the time capacity for each Plate with a trend line
plt.figure(figsize=(10, 6))
for (plate, model), group in flight_rows.groupby(['Plate', 'Model']):
    label = f"{plate} ({model})"
    sns.regplot(x='DateNumeric', y='TimeCapacity', data=group, label=label, marker='o', scatter_kws={'s': 10}, line_kws={'label': f"Trend {plate}"})

# Update the x-axis labels to show the start of each month
months = flight_rows['Date'].dt.to_period('M').unique()
month_starts = [(pd.Period(month, freq='M').start_time - flight_rows['Date'].min()).days for month in months]
month_labels = [month.strftime('%Y-%m') for month in months]

plt.xticks(ticks=month_starts, labels=month_labels, rotation=45)
plt.title('Time Capacity of Each Plate for Each Flight')
plt.xlabel('Month')
plt.ylabel('Time Capacity (%/s)')
plt.legend(title='Plate')
plt.tight_layout()
plt.show()