import sys
import os, shutil
import math
import pathlib
import pandas as pd
import simplekml
from datetime import datetime

from os.path import isdir, isfile, join
from os import listdir
from Mocca_Analyzer.Performance_Analyzer import FlightEventStore as fes
from Mocca_Analyzer.Performance_Analyzer.Event_Type import Event_Type
from Mocca_Analyzer.Mocca import basicPosition as bp


from DJI_Analyzer import TelemetryReader as tr


df_columns = [
    "MsgType",
    "timeStamp",
    "lastUpdateDate",
    "currentDate",
    "Latitude",
    "Longitude",
    "Altitude",
    "hdop",
    "vdop",
    "hSpeed",
    "Course",
    "SatsInView",
    "Pressure",
    "Temperature",
    "RAT",
    "MNN",
    "cellId",
    "signalQuality",
    "rssi",
    "bErr",
    "airLatency",
    "state"
]

GlobalStateResume; timeStamp; lastUpdateDate; date; fixType; latitude; longitude; altitude; hdop; vdop; hSpeed; course; satsInView; pressure; temperature; RAT; MNN; cellId; signalQuality; rssi; bErr; airLatency; state
GlobalStateResume; 1646848621860; Wed Mar 09 18:57:02 CET 2022; Wed Mar 09 18:57:02 CET 2022; 0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0; 102.299515625; 38.36; RAT_LTE; Sierra Wireless; 20569640; 4; 0.0; 0; 0; { PRESSURE GSMS }




def GetFiles(traj_folder):
    trajectory_file_list = []
    onlyfiles = [f for f in listdir(traj_folder) if isfile(join(traj_folder, f))]
    for file in onlyfiles:
        splitter = file.split("-")
        ext = pathlib.Path(file).suffix
        if (splitter[0] == 'FP')and(ext == '.csv'):
            trajectory_file_list.append(file)
    return trajectory_file_list


def GetFoldersIn(traj_folder, opName):
    trajectory_folder_list = []
    onlyfolders = [f for f in listdir(traj_folder) if isdir(join(traj_folder, f))]
    for file in onlyfolders:
        if opName in file:
            trajectory_folder_list.extend(GetFoldersIn(traj_folder + "\\" + file, opName))
        else:
            trajectory_folder_list.append(file)
    return trajectory_folder_list


def GetCSVFiles(traj_folder):
    trajectory_file_list = []
    onlyfiles = [f for f in listdir(traj_folder) if isfile(join(traj_folder, f))]
    for file in onlyfiles:
        ext = pathlib.Path(file).suffix
        if ext == '.csv':
            trajectory_file_list.append(file)
    return trajectory_file_list


def createKMLfile (df, fea):

    if len(df.index) < 10:
        return None

    kml = simplekml.Kml()

    prevRow = None
    prevRowAssigned = False

    for index, row in df.iterrows():
        if math.isclose(row["Lat"], 0, abs_tol=0.001) and math.isclose(row["Lon"], 0, abs_tol=0.001):
            continue

        if prevRowAssigned:
            coordsA = (prevRow["Lon"], prevRow["Lat"], prevRow["Alt"])
            coordsB = (row["Lon"], row["Lat"], row["Alt"])

            point_sequence = []
            point_sequence.append(coordsA)
            point_sequence.append(coordsB)
            ls = kml.newlinestring()
            ls.coords = point_sequence
            ls.altitudemode = simplekml.AltitudeMode.absolute

            ts = int(prevRow["Time"])
            #date = datetime.utcfromtimestamp(ts/1000)
            #print(date.strftime("%Y-%m-%dT%H:%M:%SZ"))
            ls.timestamp.when = datetime.utcfromtimestamp(ts/1000).strftime("%Y-%m-%dT%H:%M:%SZ")
            ls.style.linestyle.width = 5
            ls.style.linestyle.color = simplekml.Color.green

        prevRow = row
        prevRowAssigned = True



    for event in fea.eventList:
        if event.type == Event_Type.StartClimb:
            description = "StartClimb" + "-" + str(event.altitude) + "m" + "-" + str(event.timestamp) + "s"
            pnt = kml.newpoint(description=description, coords=[(event.longitude, event.latitude, event.altitude)])
            pnt.style.labelstyle.color = simplekml.Color.red  # Make the text red
            pnt.style.labelstyle.scale = 2  # Make the text twice as big
            pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png'
            pnt.altitudemode = simplekml.AltitudeMode.absolute
            continue

        elif event.type == Event_Type.StartTakeOff:
            description = "StartTakeOff" + "-" + str(event.altitude) + "m" + "-" + str(event.timestamp) + "s"
            pnt = kml.newpoint(description=description, coords=[(event.longitude, event.latitude, event.altitude)])
            pnt.style.labelstyle.color = simplekml.Color.red  # Make the text red
            pnt.style.labelstyle.scale = 2  # Make the text twice as big
            pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png'
            pnt.altitudemode = simplekml.AltitudeMode.absolute
            continue

        elif event.type == Event_Type.StartDescent:
            description = "StartDescent" + "-" + str(event.altitude) + "m" + "-" + str(event.timestamp) + "s"
            pnt = kml.newpoint(description=description, coords=[(event.longitude, event.latitude, event.altitude)])
            pnt.style.labelstyle.color = simplekml.Color.red  # Make the text red
            pnt.style.labelstyle.scale = 2  # Make the text twice as big
            pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png'
            pnt.altitudemode = simplekml.AltitudeMode.absolute
            continue

        elif event.type == Event_Type.StartLanding:
            description = "StartLanding" + "-" + str(event.altitude) + "m" + "-" + str(event.timestamp) + "s"
            pnt = kml.newpoint(description=description, coords=[(event.longitude, event.latitude, event.altitude)])
            pnt.style.labelstyle.color = simplekml.Color.red  # Make the text red
            pnt.style.labelstyle.scale = 2  # Make the text twice as big
            pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png'
            pnt.altitudemode = simplekml.AltitudeMode.absolute
            continue

        elif event.type == Event_Type.EndAltChange:
            description = "EndAltChange" + "-" + str(event.altitude) + "m" + "-" + str(event.timestamp) + "s"
            pnt = kml.newpoint(description=description, coords=[(event.longitude, event.latitude, event.altitude)])
            pnt.style.labelstyle.color = simplekml.Color.red  # Make the text red
            pnt.style.labelstyle.scale = 2  # Make the text twice as big
            pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/shapes/placemark_square.png'
            pnt.altitudemode = simplekml.AltitudeMode.absolute
            continue


    #point_sequence = []
    #for index, row in df.iterrows():
    #    if math.isclose(row["Lon"], 0, abs_tol=0.001) and math.isclose(row["Lon"], 0, abs_tol=0.001):
    #        continue
    #    coords = (row["Lon"], row["Lat"], row["Alt"])
    #    point_sequence.append(coords)
    #
    #ls = kml.newlinestring(name='Flight Sequence')
    #ls.coords = point_sequence
    #ls.altitudemode = simplekml.AltitudeMode.absolute
    #ls.style.linestyle.width = 5
    #ls.style.linestyle.color = simplekml.Color.green

    return kml



if __name__ == '__main__':

    traj_folder = "F:\Documents\SVN-Projects\Project-CORUS-XUAM\VLD_Spain\Scenario-Data\CORUS-OPERATIONS\\UPC-Telem\\"
    trajectories_file = "RELEVANT-OPERATIONS.csv"


    df_trajectories = pd.read_csv(traj_folder + trajectories_file, index_col=False, header=None, sep=";", skipinitialspace=True)


    print("Analyzing trajectory files:\t", trajectories_file, "\n")

    #trajectory_file_list = GetCSVFiles(traj_folder)
    trajectory_folder_list = GetFolders(traj_folder)

    csvResume = open(traj_folder + "\\" + "Activation-Resume.csv", 'w')

    for trajectory_folder in trajectory_folder_list:
        print("Analyzing trajectory:\t", trajectory_file, "\n")

        # Only in case of Cristina's files
        '''tf = pathlib.Path(trajectory_file).stem.split("_")
        if len(tf) >= 5:
            callsign = tf[0] + "_" + tf[1] + "_" + tf[2]
            delivery = tf[3] + "_" + tf[4]
        elif len(tf) >= 3:
            callsign = tf[0] + "_" + tf[1] + "_" + tf[2]
            delivery = "Unknown"
            continue
        else:
            callsign = "Unknown"
            delivery = "Unknown"
            continue'''

        fea = fes.FlightEventStore(0, minTimeRange=8, maxTimeDiff=10, debug=0)

        df_results = pd.read_csv(traj_folder + "\\" + trajectory_file, index_col=False, header=None, skiprows=1, sep=';', names=df_columns)
        #df_results = pd.read_csv(traj_folder + "\\" + trajectory_file, index_col=False, header=None, skiprows=1, names=df_CBcolumns)

        if len(df_results.index) == 0:
            print("Skipping: " + trajectory_file + "\n")
            continue

        nSamples = 0

        for index, row in df_results.iterrows():
            if math.isclose(row["Lon"], 0, abs_tol=0.001) and math.isclose(row["Lon"], 0, abs_tol=0.001):
                continue

            sample = bp.basicPosition()
            #sample.decodeCBMsg(row)
            sample.decodeMsg(row)
            prevSample = fea.getHeadPositional()

            sample.complementSpeeds(prevSample)
            fea.insertAnalyzing(sample, isEventStarted = "Empty")

            nSamples += 1

        fea.identifyTakeOffLanding()
        fea.refineChangeAltitudeEvents()

        kml = createKMLfile(df_results, fea)

        if kml != None:
            kml.save(traj_folder + "\\" + trajectory_file + ".kml")

        firstPosition = fea.getFirstPositional()
        lastPosition = fea.getLastPositional()


        takeoff, endTakeoff = fea.getTakeoffEvents()
        startLand, land = fea.getLandEvents()

        #csvResume.write(callsign + "; " + delivery + "; ")
        #csvResume.write(pathlib.Path(trajectory_file).stem + "; ")
        if firstPosition != None and lastPosition != None:
            csvResume.write(pathlib.Path(trajectory_file).stem + "; " + str(int(firstPosition.timestamp)) + "; " + str(int(lastPosition.timestamp)) + "; ")

            if takeoff != None:
                csvResume.write(str(int(takeoff.timestamp)) + "; " + str(int(endTakeoff.timestamp)) + "; ")
            else:
                csvResume.write("-1" + "; " + "-1" + "; ")

            if startLand != None:
                csvResume.write(str(int(startLand.timestamp)) + "; " + str(int(land.timestamp)))
            else:
                csvResume.write("-1" + "; " + "-1")

            csvResume.write("; " + str(nSamples) + "\n")
        else:
            print("Rejecting: " + trajectory_file + "\n")

    csvResume.close()
    print("Done.\n")
    sys.exit(0)



