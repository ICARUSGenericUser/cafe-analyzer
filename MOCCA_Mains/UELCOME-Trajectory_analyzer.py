import math
import sys
import os, shutil
import pathlib

import chardet
import pandas as pd
import plotly.graph_objects as go
from plotly.offline import plot

from os.path import isfile, join
from os import listdir
import Mocca_Analyzer.Mocca.Mocca_Object as mo
from Mocca_Analyzer.Mocca import TelemetryReader as tr
from Mocca_Analyzer.Performance_Analyzer import FlightEventStore as fes
from Mocca_Analyzer.Performance_Analyzer import PerformanceAnalysis as pa, AltitudeAnalysis as aa, \
    StraightSegmentAnalysis as ssa, TurnAnalysis as ta
from Mocca_Analyzer.Plots import ClimbDescentInterpolationPlot as cdip, StraightSegmentPlot as ssp


#
# Reference: https://plotly.com/python/

def generateAltitudePlot(aglAlt, amslAlt, wgs84Alt, time, Title):
    fig = go.Figure()
    fig.add_trace(go.Scatter(
        x=time,
        y=aglAlt,
        xaxis='x',
        yaxis='y',
        hovertemplate='%{x:.10f}, %{y:.5f}',
        mode='markers',
        marker=dict(
            color='rgba(1.0,0,0,0.3)',
            size=4
        )
    ))
    fig.add_trace(go.Scatter(
        x=time,
        y=amslAlt,
        xaxis='x',
        yaxis='y',
        hovertemplate='%{x:.10f}, %{y:.5f}',
        mode='markers',
        marker=dict(
            color='rgba(0,1.0,0,0.3)',
            size=4
        )
    ))
    fig.add_trace(go.Scatter(
        x=time,
        y=wgs84Alt,
        xaxis='x',
        yaxis='y',
        hovertemplate='%{x:.10f}, %{y:.5f}',
        mode='markers',
        marker=dict(
            color='rgba(0,0,1.0,0.3)',
            size=4
        )
    ))

    fig.update_layout(
        autosize=False,
        xaxis=dict(
            title="Time (sec)",
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        yaxis=dict(
            title="AGL",
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        yaxis2=dict(
            title="AMSL",
            zeroline=False,
            domain=[0.85, 1],
            showgrid=True
        ),
        yaxis3=dict(
            title="WGS84",
            zeroline=False,
            domain=[0.85, 1],
            showgrid=True
        ),
        height=1000,
        width=3000,
        bargap=0.1,
        hovermode='closest',
        showlegend=True,
        title=Title
    )

    return fig


def generateSpeedPlot(speedX, speedY, speedZ, time, Title):
    fig = go.Figure()
    fig.add_trace(go.Scatter(
        x=time,
        y=speedX,
        xaxis='x',
        yaxis='y',
        mode='markers',
        marker=dict(
            color='rgba(1.0,0,0,0.3)',
            size=4
        )
    ))
    fig.add_trace(go.Scatter(
        x=time,
        y=speedY,
        xaxis='x',
        yaxis='y',
        mode='markers',
        marker=dict(
            color='rgba(0,1.0,0,0.3)',
            size=4
        )
    ))
    fig.add_trace(go.Scatter(
        x=time,
        y=speedZ,
        xaxis='x',
        yaxis='y',
        mode='markers',
        marker=dict(
            color='rgba(0,0,1.0,0.3)',
            size=4
        )
    ))

    fig.update_layout(
        autosize=False,
        xaxis=dict(
            title="Time (sec)",
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        yaxis=dict(
            title="Speed X",
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        yaxis2=dict(
            title="SpeedY",
            zeroline=False,
            domain=[0.85, 1],
            showgrid=True
        ),
        yaxis3=dict(
            title="SpeedZ",
            zeroline=False,
            domain=[0.85, 1],
            showgrid=True
        ),
        height=1000,
        width=3000,
        bargap=0.1,
        hovermode='closest',
        showlegend=True,
        title=Title
    )

    return fig


def generateSpeed2Plot(Vh, Vv, time, Title):
    fig = go.Figure()
    fig.add_trace(go.Scatter(
        x=time,
        y=Vh,
        xaxis='x',
        yaxis='y',
        hovertemplate='%{x:.10f}, %{y:.5f}',
        mode='markers',
        marker=dict(
            color='rgba(1.0,0,0,0.3)',
            size=18
        )
    ))

    fig.update_layout(
        autosize=False,
        xaxis=dict(
            title="Time (sec)",
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False,
        ),
        yaxis=dict(
            title="Vh (m/s)",
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False,
        ),
        height=1000,
        width=3000,
        bargap=0.1,
        hovermode='closest',
        showlegend=True,
        title=Title,
        font=dict(
            size=28,
        )

    )

    return fig


def generateAccelerationSpeedPlot(SpeedAcceleration, Vh, time, Title):
    fig = go.Figure()
    fig.add_trace(go.Scatter(
        x=time,
        y=Vh,
        xaxis='x',
        yaxis='y',
        hovertemplate='%{x:.10f}, %{y:.5f}',
        mode='markers',
        marker=dict(
            color='rgba(1.0,0,0,0.3)',
            size=4
        )
    ))
    '''fig.add_trace(go.Scatter(
        x=time,
        y=SpeedAcceleration,
        xaxis='x',
        yaxis='y',
        mode='markers',
        marker=dict(
            color='rgba(0,0,1.0,0.3)',
            size=4
        )
    ))'''

    fig.update_layout(
        autosize=False,
        xaxis=dict(
            title="Time (sec)",
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        yaxis=dict(
            title="Vh",
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        yaxis2=dict(
            title="Speed Acceleration",
            zeroline=False,
            domain=[0.85, 1],
            showgrid=True
        ),
        height=1000,
        width=3000,
        bargap=0.1,
        hovermode='closest',
        showlegend=True,
        title=Title
    )

    return fig


def generateAccelerationPlot(Acceleration, Vh, time, Title):
    fig = go.Figure()
    fig.add_trace(go.Scatter(
        x=time,
        y=Vh,
        xaxis='x',
        yaxis='y',
        mode='markers',
        marker=dict(
            color='rgba(1.0,0,0,0.3)',
            size=4
        )
    ))
    fig.add_trace(go.Scatter(
        x=time,
        y=Acceleration,
        xaxis='x',
        yaxis='y',
        mode='markers',
        marker=dict(
            color='rgba(0,0,1.0,0.3)',
            size=4
        )
    ))

    fig.update_layout(
        autosize=False,
        xaxis=dict(
            title="Time (sec)",
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        yaxis=dict(
            title="Vh",
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        yaxis2=dict(
            title="Acceleration",
            zeroline=False,
            domain=[0.85, 1],
            showgrid=True
        ),
        height=1000,
        width=3000,
        bargap=0.1,
        hovermode='closest',
        showlegend=True,
        title=Title
    )

    return fig


if __name__ == '__main__':

    def GetFiles(traj_folder):
        dji_trajectory_file_list = []
        onlyfilesList = []
        onlyfolders = listdir(traj_folder)

        for folder in onlyfolders:
            oppath = join(traj_folder, folder)
            onlyfiles = [f for f in listdir(oppath) if isfile(join(oppath, f))]

            for file in onlyfiles:
                pack = [oppath, file]
                onlyfilesList.append(pack)

        for pack in onlyfilesList:
            splitter = pack[1].split("-")
            if ((splitter[0] == 'DJI') and (splitter[1] == 'FLOG')):
                dji_trajectory_file_list.append(pack)

        return dji_trajectory_file_list


    # debugLevel variable
    # 1: print TimeStamp, Altitude, Heading/Course, HSpeed, VSpeed
    # 2: plot Altitude vs. Time, Heading/Course vs. Time, Latitude vs. Longitude

    # Line with folder in which measures are saved:
    traj_repo_folder = "D:\\DronePerformance\\Data\\16-09-2024"
    # traj_repo_folder = "D:\\U-ELCOME\\AlguaireEstiu2024\\17-08-2024-20240905T110402Z-001\\17-08-2024"
    operationId = "Telemetry"
    result_folder = "PerformanceDiagrams"

    debugLevel = 2

    traj_folder = traj_repo_folder + "\\" + operationId

    dji_trajectory_file_list = GetFiles(traj_folder)
    print("Number of DJI files to analyze: ", len(dji_trajectory_file_list), "\n")
    print("Analyzing trajectories at folder: \n\t\t", traj_folder, "\n")

    TrajAnalysis = pa.PerformanceAnalysis()

    TrajAnalysis.trajectoryFolder = traj_folder
    TrajAnalysis.trajectoryList = dji_trajectory_file_list

    if os.path.exists(traj_repo_folder + "\\" + result_folder):
        shutil.rmtree(traj_repo_folder + "\\" + result_folder)
    os.mkdir(traj_repo_folder + "\\" + result_folder)

    for trajectory_pack in TrajAnalysis.trajectoryList:

        print("Analyzing trajectory:\t", trajectory_pack[1], "\n")

        telSequence = tr.readDJITelemetryFile(trajectory_pack[0] + "\\" + trajectory_pack[1])

        dfAlt = pd.DataFrame(columns=['AGL', 'AMSL', 'WGS84', 'Time'])

        dfSpeed = pd.DataFrame(columns=['speedX', 'speedY', 'speedZ', 'Time'])

        # fea = fes.FlightEventStore()
        VhArray, VvArray, TimeSpeed2Array = [], [], []

        firstTimestamp = 0

        for sample in telSequence:
            if sample.type == mo.Object_Type.djiGNSSIMUNavigation:
                # sampleRow = [sample.altitude, sample.altitudeASL, sample.altitudeWGS84, sample.timestamp]
                if firstTimestamp == 0:
                    firstTimestamp = sample.timestamp / 1000
                newRow = pd.DataFrame(
                    {'AGL': [sample.altitude], 'AMSL': [sample.altitudeASL], 'WGS84': [sample.altitudeWGS84],
                     'Time': [(sample.timestamp / 1000) - firstTimestamp]})
                # sampleRow = [[sample.altitude], [sample.altitudeASL], [sample.altitudeWGS84], [sample.timestamp]]
                # df = pd.DataFrame(data=pd.array(sampleRow), columns=['AGL', 'AMSL', 'WGS84', 'Time'])
                dfAlt = dfAlt._append(newRow)
                newRowSpeed = pd.DataFrame(
                    {'speedX': [abs(sample.speedX)], 'speedY': [abs(sample.speedY)], 'speedZ': [abs(sample.speedZ)],
                     'Time': [(sample.timestamp / 1000) - firstTimestamp]})
                dfSpeed = dfSpeed._append(newRowSpeed)
                Vh = math.sqrt(sample.speedX * sample.speedX + sample.speedY * sample.speedY)
                Vv = sample.speedZ
                VhArray.append(Vh)
                VvArray.append(Vv)
                TimeSpeed2Array.append((sample.timestamp / 1000) - firstTimestamp)

        dfSpeed2 = pd.DataFrame({'Vh': VhArray, 'Vv': VvArray, 'Time': TimeSpeed2Array})

        dfAccelerationSpeeds = pd.DataFrame(columns=['Speed', 'Time'])
        initialAcc = -1
        timestampInitialAcc = 0
        finalAcc = -1
        timestampFinalAcc = 0
        initialDeAcc = -1
        timestampInitialDeAcc = 0
        finalDeAcc = -1
        timestampFinalDeAcc = 0

        for index, row in dfSpeed2.iterrows():
            if not (index == len(dfSpeed2.index) - 1):
                if initialAcc == -1 and dfSpeed2.iloc[index + 1]['Vh'] - dfSpeed2.iloc[index]['Vh'] > 2:
                    initialAcc = dfSpeed2.iloc[index]['Vh']
                    timestampInitialAcc = dfSpeed2.iloc[index]['Time']
                elif initialAcc >= 0 and finalAcc == -1 and dfSpeed2.iloc[index + 1]['Vh'] - dfSpeed2.iloc[index][
                    'Vh'] < 0:
                    finalAcc = dfSpeed2.iloc[index]['Vh']
                    timestampFinalAcc = dfSpeed2.iloc[index]['Time']
                elif initialAcc >= 0 and finalAcc >= 0:
                    if initialDeAcc == -1 and dfSpeed2.iloc[index + 1]['Vh'] - dfSpeed2.iloc[index]['Vh'] < 0:
                        initialDeAcc = dfSpeed2.iloc[index]['Vh']
                        timestampInitialDeAcc = dfSpeed2.iloc[index]['Time']
                    elif finalDeAcc == -1 and dfSpeed2.iloc[index - 1]['Vh'] - dfSpeed2.iloc[index]['Vh'] > 2:
                        finalDeAcc = dfSpeed2.iloc[index]['Vh']
                        timestampFinalDeAcc = dfSpeed2.iloc[index]['Time']
                    elif (initialDeAcc >= 0 and finalDeAcc >= 0):
                        newRowAcc1 = pd.DataFrame({'Speed': [initialAcc], 'Time': [timestampInitialAcc]})
                        dfAccelerationSpeeds = dfAccelerationSpeeds._append(newRowAcc1)
                        newRowAcc2 = pd.DataFrame({'Speed': [finalAcc], 'Time': [timestampFinalAcc]})
                        dfAccelerationSpeeds = dfAccelerationSpeeds._append(newRowAcc2)
                        newRowAcc3 = pd.DataFrame({'Speed': [initialDeAcc], 'Time': [timestampInitialDeAcc]})
                        dfAccelerationSpeeds = dfAccelerationSpeeds._append(newRowAcc3)
                        newRowAcc4 = pd.DataFrame({'Speed': [finalDeAcc], 'Time': [timestampFinalDeAcc]})
                        dfAccelerationSpeeds = dfAccelerationSpeeds._append(newRowAcc4)
                        initialAcc = -1
                        timestampInitialAcc = 0
                        finalAcc = -1
                        timestampFinalAcc = 0
                        initialDeAcc = -1
                        timestampInitialDeAcc = 0
                        finalDeAcc = -1
                        timestampFinalDeAcc = 0
                    else:
                        newRowNoAcc = pd.DataFrame({'Speed': [0], 'Time': [dfSpeed2.iloc[index]['Time']]})
                        dfAccelerationSpeeds = dfAccelerationSpeeds._append(newRowNoAcc)
                else:
                    newRowNoAcc = pd.DataFrame({'Speed': [0], 'Time': [dfSpeed2.iloc[index]['Time']]})
                    dfAccelerationSpeeds = dfAccelerationSpeeds._append(newRowNoAcc)
            else:
                newRowNoAcc = pd.DataFrame({'Speed': [0], 'Time': [dfSpeed2.iloc[index]['Time']]})
                dfAccelerationSpeeds = dfAccelerationSpeeds._append(newRowNoAcc)

        AccArray, TimeAccArray = [], []
        for index, row in dfSpeed2.iterrows():
            if not (index == len(dfSpeed2.index) - 1):
                acceleration = (dfSpeed2.iloc[index + 1]['Vh'] - dfSpeed2.iloc[index]['Vh']) / (
                            dfSpeed2.iloc[index + 1]['Time'] - dfSpeed2.iloc[index]['Time'])
                AccArray.append(acceleration)
                TimeAccArray.append(dfSpeed2.iloc[index]['Time'])
        dfAcceleration = pd.DataFrame({'Acceleration': AccArray, 'Time': TimeAccArray})

        fig = generateAltitudePlot(dfAlt['AGL'], dfAlt['AMSL'], dfAlt['WGS84'], dfAlt['Time'],
                                   "General Altitude Comparison")

        file_suffix = pathlib.Path(trajectory_pack[1]).stem

        filename = traj_repo_folder + "\\" + result_folder + "\\" + file_suffix + "-AltitudeComparison.html"
        print("Generating figure: " + filename)
        plot(fig, filename=filename)

        figSpeed = generateSpeedPlot(dfSpeed['speedX'], dfSpeed['speedY'], dfSpeed['speedZ'], dfSpeed['Time'],
                                     "General Speed Comparison")

        file_suffix = pathlib.Path(trajectory_pack[1]).stem

        filename = traj_repo_folder + "\\" + result_folder + "\\" + file_suffix + "-SpeedComparison.html"
        print("Generating figure: " + filename)
        plot(figSpeed, filename=filename)

        figSpeed2 = generateSpeed2Plot(dfSpeed2['Vh'], dfSpeed2['Vv'], dfSpeed2['Time'],
                                       "General Speed2 Comparison")

        file_suffix = pathlib.Path(trajectory_pack[1]).stem

        filename = traj_repo_folder + "\\" + result_folder + "\\" + file_suffix + "-Speed2Comparison.html"
        print("Generating figure: " + filename)
        plot(figSpeed2, filename=filename)

        figAccelerationSpeed = generateAccelerationSpeedPlot(dfAccelerationSpeeds['Speed'],
                                                             dfSpeed2['Vh'],
                                                             dfAccelerationSpeeds['Time'],
                                                             "General Acceleration Speed Comparison")

        file_suffix = pathlib.Path(trajectory_pack[1]).stem

        filename = traj_repo_folder + "\\" + result_folder + "\\" + file_suffix + "-AccelerationSpeedComparison.html"
        print("Generating figure: " + filename)
        plot(figAccelerationSpeed, filename=filename)

        figAcceleration = generateAccelerationPlot(dfAcceleration['Acceleration'],
                                                   dfSpeed2['Vh'],
                                                   dfAcceleration['Time'],
                                                   "General Acceleration Comparison")

        file_suffix = pathlib.Path(trajectory_pack[1]).stem

        filename = traj_repo_folder + "\\" + result_folder + "\\" + file_suffix + "-AccelerationComparison.html"
        print("Generating figure: " + filename)
        plot(figAcceleration, filename=filename)

        '''fea = fes.FlightEventStore()

        for sample in telSequence:
            fea.insertAnalyzing(sample)

        file_suffix = pathlib.Path(trajectory_pack[1]).stem

        fea.doneAnalyzing(traj_folder + "\\" + result_folder + "\\SW-" + file_suffix)
        TrajAnalysis.eventAnalysis.append(fea)

        aa.analyzeAltitudeEvents(fea)
        aa.analyzeVSpeedEvents(fea)'''

        print("Done.")

    print("DJI - Set completed.")

    print("Done.\n")
    sys.exit(0)
