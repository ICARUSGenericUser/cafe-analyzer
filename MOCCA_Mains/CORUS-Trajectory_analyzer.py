import sys
import os, shutil
import math
import pathlib

import numpy as np
import pandas as pd
import simplekml
from datetime import datetime

import matplotlib
import matplotlib.pyplot as plt

from os.path import isfile, join
from os import listdir
from Mocca_Analyzer.Performance_Analyzer import FlightEventStore as fes
from Mocca_Analyzer.Performance_Analyzer.Event_Type import Event_Type
from Mocca_Analyzer.Mocca import basicPosition as bp


from DJI_Analyzer import TelemetryReader as tr


df_columns = [
    "Time",
    "Lat",
    "Lon",
    "Alt"
]

df_CBcolumns = [
    "Time",
    "DateTime",
    "Lat",
    "Lon",
    "Alt"
]



def GetFiles(traj_folder):
    trajectory_file_list = []
    onlyfiles = [f for f in listdir(traj_folder) if isfile(join(traj_folder, f))]
    for file in onlyfiles:
        splitter = file.split("-")
        ext = pathlib.Path(file).suffix
        if (splitter[0] == 'FP')and(ext == '.csv'):
            trajectory_file_list.append(file)
    return trajectory_file_list


def GetCSVFiles(traj_folder):
    trajectory_file_list = []
    onlyfiles = [f for f in listdir(traj_folder) if isfile(join(traj_folder, f))]
    for file in onlyfiles:
        ext = pathlib.Path(file).suffix
        if ext == '.csv':
            trajectory_file_list.append(file)
    return trajectory_file_list



def createTrajectoryPlotFile (traj_folder, name, df, fea):

    if len(df.index) < 10:
        return None

    nTest = len(fea.telemetryList)

    time = [0] * (nTest)

    vh = [0] * (nTest)
    vz = [0] * (nTest)
    alt = [0] * (nTest)
    dist = [0] * (nTest)

    baseTime = fea.telemetryList[0].timestamp

    for idx, sample in enumerate(fea.telemetryList):
        time[idx] = sample.timestamp - baseTime
        vh[idx] = sample.hspeed
        vz[idx] = sample.vspeed
        alt[idx] = sample.altitude
        dist[idx] = sample.distance


    fig, axs = plt.subplots(3, 1, sharex=True)
    # Remove vertical space between axes
    fig.subplots_adjust(hspace=0.1)

    # Plot each graph, and manually set the y tick values
    axs[0].plot(time, vh)
    axs[0].set_yticks(np.arange(0, 12.0, 2.0))
    axs[0].set_ylim(0, 12.0)
    axs[0].set(ylabel='HS(m/s)')
    start, end = axs[0].get_xlim()
    axs[0].set_xticks(np.arange(0, end, 10))
    axs[0].grid(True)
    axs[0].yaxis.set_tick_params(labelsize=7)


    axs[1].plot(time, vz)
    axs[1].set_yticks(np.arange(-3.0, 4.0, 1.0))
    axs[1].set_ylim(-3.0, 4.0)
    axs[1].set(ylabel='VS(m/s)')
    start, end = axs[1].get_xlim()
    axs[1].set_xticks(np.arange(0, end, 10))
    axs[1].grid(True)
    axs[1].yaxis.set_tick_params(labelsize=7)


    axs[2].plot(time, alt)
    axs[2].set_yticks(np.arange(0, 100.0, 10.0))
    axs[2].set_ylim(0, 100.0)
    axs[2].set(ylabel='Alt(m)', xlabel='Time(s)')
    start, end = axs[2].get_xlim()
    axs[2].set_xticks(np.arange(0, end, 10))
    axs[2].grid(True)
    axs[2].yaxis.set_tick_params(labelsize=7)
    axs[2].xaxis.set_tick_params(labelsize=7)


    for event in fea.eventList:
        if event.type == Event_Type.StartTakeOff:
            axs[1].plot(event.timestamp, event.vspeed, marker='^', markersize=6, color='green')
            continue

        if event.type == Event_Type.StartLanding:
            axs[1].plot(event.timestamp, event.vspeed, marker='v', markersize=6, color='blue')
            continue

        if event.type == Event_Type.StartHover:
            axs[0].plot(event.timestamp, event.hspeed, marker='^', markersize=5, color='green')
            continue

        if event.type == Event_Type.EndHover:
            axs[0].plot(event.timestamp, event.hspeed, marker='>', markersize=5, color='green')
            continue

        if event.type == Event_Type.StartClimb:
            axs[2].plot(event.timestamp, event.altitude, marker='^', markersize=5, color='green')
            continue

        elif event.type == Event_Type.StartTakeOff:
            axs[2].plot(event.timestamp, event.altitude, marker='^', markersize=6, color='green')
            continue

        elif event.type == Event_Type.StartDescent:
            axs[2].plot(event.timestamp, event.altitude, marker='v', markersize=5, color='blue')
            continue

        elif event.type == Event_Type.StartLanding:
            axs[2].plot(event.timestamp, event.altitude, marker='v', markersize=6, color='blue')
            continue

        elif event.type == Event_Type.EndAltChange:
            axs[2].plot(event.timestamp, event.altitude, marker='D', markersize=5, color='blue')
            continue


    fig.suptitle('Trajectory analysis: ' + name + '. Base time: ' + str(baseTime), fontsize=10)
    plt.grid(True)
    fig=plt.gcf()
    filepath = traj_folder + "\\" + name + "_Analysis" + ".png"
    print("Generating figure: " + filepath)
    fig.set_size_inches(15.0, 15.0)
    fig.savefig(filepath)
    plt.close(fig)
    plt.clf()
    #plt.show()



def createKMLfile (df, fea):

    if len(df.index) < 10:
        return None

    kml = simplekml.Kml()

    prevRow = None
    prevRowAssigned = False

    for index, row in df.iterrows():
        if math.isclose(row["Lat"], 0, abs_tol=0.001) and math.isclose(row["Lon"], 0, abs_tol=0.001):
            continue

        if prevRowAssigned:
            coordsA = (prevRow["Lon"], prevRow["Lat"], prevRow["Alt"])
            coordsB = (row["Lon"], row["Lat"], row["Alt"])

            point_sequence = []
            point_sequence.append(coordsA)
            point_sequence.append(coordsB)
            ls = kml.newlinestring()
            ls.coords = point_sequence
            ls.altitudemode = simplekml.AltitudeMode.absolute

            ts = int(prevRow["Time"])
            #date = datetime.utcfromtimestamp(ts/1000)
            #print(date.strftime("%Y-%m-%dT%H:%M:%SZ"))
            ls.timestamp.when = datetime.utcfromtimestamp(ts/1000).strftime("%Y-%m-%dT%H:%M:%SZ")
            ls.style.linestyle.width = 5
            ls.style.linestyle.color = simplekml.Color.green

        prevRow = row
        prevRowAssigned = True



    for event in fea.eventList:
        if event.type == Event_Type.StartClimb:
            description = "StartClimb" + "-" + str(event.altitude) + "m" + "-" + str(event.timestamp) + "s"
            pnt = kml.newpoint(description=description, coords=[(event.longitude, event.latitude, event.altitude)])
            pnt.style.labelstyle.color = simplekml.Color.red  # Make the text red
            pnt.style.labelstyle.scale = 2  # Make the text twice as big
            pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png'
            pnt.altitudemode = simplekml.AltitudeMode.absolute
            continue

        elif event.type == Event_Type.StartTakeOff:
            description = "StartTakeOff" + "-" + str(event.altitude) + "m" + "-" + str(event.timestamp) + "s"
            pnt = kml.newpoint(description=description, coords=[(event.longitude, event.latitude, event.altitude)])
            pnt.style.labelstyle.color = simplekml.Color.red  # Make the text red
            pnt.style.labelstyle.scale = 2  # Make the text twice as big
            pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png'
            pnt.altitudemode = simplekml.AltitudeMode.absolute
            continue

        elif event.type == Event_Type.StartDescent:
            description = "StartDescent" + "-" + str(event.altitude) + "m" + "-" + str(event.timestamp) + "s"
            pnt = kml.newpoint(description=description, coords=[(event.longitude, event.latitude, event.altitude)])
            pnt.style.labelstyle.color = simplekml.Color.red  # Make the text red
            pnt.style.labelstyle.scale = 2  # Make the text twice as big
            pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png'
            pnt.altitudemode = simplekml.AltitudeMode.absolute
            continue

        elif event.type == Event_Type.StartLanding:
            description = "StartLanding" + "-" + str(event.altitude) + "m" + "-" + str(event.timestamp) + "s"
            pnt = kml.newpoint(description=description, coords=[(event.longitude, event.latitude, event.altitude)])
            pnt.style.labelstyle.color = simplekml.Color.red  # Make the text red
            pnt.style.labelstyle.scale = 2  # Make the text twice as big
            pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png'
            pnt.altitudemode = simplekml.AltitudeMode.absolute
            continue

        elif event.type == Event_Type.EndAltChange:
            description = "EndAltChange" + "-" + str(event.altitude) + "m" + "-" + str(event.timestamp) + "s"
            pnt = kml.newpoint(description=description, coords=[(event.longitude, event.latitude, event.altitude)])
            pnt.style.labelstyle.color = simplekml.Color.red  # Make the text red
            pnt.style.labelstyle.scale = 2  # Make the text twice as big
            pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/shapes/placemark_square.png'
            pnt.altitudemode = simplekml.AltitudeMode.absolute
            continue


    #point_sequence = []
    #for index, row in df.iterrows():
    #    if math.isclose(row["Lon"], 0, abs_tol=0.001) and math.isclose(row["Lon"], 0, abs_tol=0.001):
    #        continue
    #    coords = (row["Lon"], row["Lat"], row["Alt"])
    #    point_sequence.append(coords)
    #
    #ls = kml.newlinestring(name='Flight Sequence')
    #ls.coords = point_sequence
    #ls.altitudemode = simplekml.AltitudeMode.absolute
    #ls.style.linestyle.width = 5
    #ls.style.linestyle.color = simplekml.Color.green

    return kml



if __name__ == '__main__':

    #traj_folder = "E:\Documents\SVN-Projects\Project-CORUS-XUAM\VLD_Spain\Scenario-Data\CORUS-OPERATIONS\ASLOGIC-MISSIONS"
    traj_folder = "E:\Documents\SVN-Projects\Project-CORUS-XUAM\VLD_Spain\Scenario-Data\CORUS-OPERATIONS\\4DT-TELEMETRY-V3"

    print("Analyzing trajectory folder:\t", traj_folder, "\n")

    trajectory_file_list = GetCSVFiles(traj_folder)
    #trajectory_file_list = GetFiles(traj_folder)

    csvResume = open(traj_folder + "\\" + "Activation-Resume.csv", 'w')

    for trajectory_file in trajectory_file_list:
        print("Analyzing trajectory:\t", trajectory_file, "\n")

        # Only in case of using Cristina file format
        tf = pathlib.Path(trajectory_file).stem.split("_")
        if len(tf) >= 5:
            callsign = tf[0] + "_" + tf[1] + "_" + tf[2]
            delivery = tf[3] + "_" + tf[4]
        elif len(tf) >= 3:
            callsign = tf[0] + "_" + tf[1] + "_" + tf[2]
            delivery = "Unknown"
            continue
        else:
            callsign = "Unknown"
            delivery = "Unknown"
            continue

        fea = fes.FlightEventStore(0, minTimeRange=8, maxTimeDiff=10, debug=0)

        # For Cristina file format
        df_results = pd.read_csv(traj_folder + "\\" + trajectory_file, index_col=False, header=None, skiprows=1, names=df_CBcolumns)

        # For ASLOGIC file format
        # df_results = pd.read_csv(traj_folder + "\\" + trajectory_file, index_col=False, header=None, skiprows=1, sep=';', names=df_columns)


        if len(df_results.index) == 0:
            print("Skipping: " + trajectory_file + "\n")
            continue

        nSamples = 0

        for index, row in df_results.iterrows():
            if math.isclose(row["Lon"], 0, abs_tol=0.001) and math.isclose(row["Lon"], 0, abs_tol=0.001):
                continue

            sample = bp.basicPosition()
            sample.decodeCBMsg(row)  # In case we use Cristina's file format
            #sample.decodeMsg(row)  # In case we use the ASLOGIC file format
            prevSample = fea.getHeadPositional()

            sample.complementSpeeds(prevSample)
            fea.insertAnalyzing(sample, isEventStarted = "None")

            nSamples += 1

        fea.identifyTakeOffLanding()
        fea.refineChangeAltitudeEvents()

        kml = createKMLfile(df_results, fea)

        if kml != None:
            kml.save(traj_folder + "\\" + trajectory_file + ".kml")

        createTrajectoryPlotFile(traj_folder, os.path.splitext(trajectory_file)[0], df_results, fea)


        firstPosition = fea.getFirstPositional()
        lastPosition = fea.getLastPositional()


        takeoff, endTakeoff = fea.getTakeoffEvents()
        startLand, land = fea.getLandEvents()

        csvResume.write(callsign + "; " + delivery + "; ")
        #csvResume.write(pathlib.Path(trajectory_file).stem + "; ")
        if firstPosition != None and lastPosition != None:
            csvResume.write(pathlib.Path(trajectory_file).stem + "; " + str(int(firstPosition.timestamp)) + "; " + str(int(lastPosition.timestamp)) + "; ")

            if takeoff != None:
                csvResume.write(str(int(takeoff.timestamp)) + "; " + str(int(endTakeoff.timestamp)) + "; ")
            else:
                csvResume.write("-1" + "; " + "-1" + "; ")

            if startLand != None:
                csvResume.write(str(int(startLand.timestamp)) + "; " + str(int(land.timestamp)))
            else:
                csvResume.write("-1" + "; " + "-1")

            csvResume.write("; " + str(nSamples) + "\n")
        else:
            print("Rejecting: " + trajectory_file + "\n")

    csvResume.close()
    print("Done.\n")
    sys.exit(0)



