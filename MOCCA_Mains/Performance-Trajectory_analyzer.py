import math
import sys
import os, shutil
import pathlib

import chardet
import pandas as pd
import plotly.graph_objects as go
from plotly.offline import plot

from os.path import isfile, join
from os import listdir
import Mocca_Analyzer.Mocca.Mocca_Object as mo
from Mocca_Analyzer.Mocca import TelemetryReader as tr
from Mocca_Analyzer.Acceleration_Analyzer import AccelerationReader as ar
from Mocca_Analyzer.Performance_Analyzer import FlightEventStore as fes
from Mocca_Analyzer.Acceleration_Analyzer import AccelerationAnalysis as aa
from Mocca_Analyzer.Performance_Analyzer import PerformanceAnalysis as pa
from Mocca_Analyzer.Plots import ClimbDescentInterpolationPlot as cdip, StraightSegmentPlot as ssp

def calculate_centroid_from_series(xs: pd.Series, ys: pd.Series):
    """
    Calculate the centroid of a set of 2D points provided as two pandas Series.

    Parameters:
    xs (pd.Series): A pandas Series representing the x-coordinates.
    ys (pd.Series): A pandas Series representing the y-coordinates.

    Returns:
    tuple: The (x, y) coordinates of the centroid.
    """
    if len(xs) != len(ys):
        raise ValueError("The length of the x and y series must be the same.")

    # Calculate the centroid
    centroid_x = xs.mean()
    centroid_y = ys.mean()

    return centroid_x, centroid_y

def generateAccelerationTrajectoryPlotComplete(latitude, longitude, accelLongitude, accelName, Title):
    figure = go.Figure()
    figure.add_trace(
        go.Scattermap(
            lat=latitude,
            lon=longitude,
            mode='markers+lines',
            marker=go.scattermap.Marker(
                size=14,
                color='rgb(0,0,255)',
                opacity=0.6
            )
        )
    )
    figure.add_trace(
        go.Scattermap(
            lat=latitude,
            lon=accelLongitude,
            mode='markers+text',
            marker=go.scattermap.Marker(
                size=14,
                color='rgb(255,0,0)',
                opacity=1.0
            ),
            text=accelName,
            textfont=dict(size=18, color="white", weight=500)
        )
    )

    centroid = calculate_centroid_from_series(latitude,longitude)

    figure.update_layout(
        autosize=False,
        xaxis=dict(
            title="Latitude (ª)",
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        yaxis=dict(
            title="Longitude (ª)",
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        yaxis2=dict(
            title="Acceleration Longitude (ª)",
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        height=1000,
        width=1500,
        bargap=0.1,
        showlegend=True,
        title=Title,
        map=dict(
            center=go.layout.map.Center(
                lat=centroid[0],
                lon=centroid[1],
            ),
            zoom=15,
            style="satellite",
        )
    )

    return figure


def generateAccelerationTrajectoryPlot(latitude, longitude, Title):
    figure = go.Figure()
    figure.add_trace(go.Scatter(
        x=latitude,
        y=longitude,
        xaxis='x',
        yaxis='y',
        hovertemplate='%{x:.15f}, %{y:.15f}',
        mode='markers',
        marker=dict(
            color='rgba(1.0,0,0,0.3)',
            size=4
        )
    ))

    figure.update_layout(
        autosize=False,
        xaxis=dict(
            title="Latitude (ª)",
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        yaxis=dict(
            title="Longitude (ª)",
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        height=1000,
        width=3000,
        bargap=0.1,
        hovermode='closest',
        showlegend=True,
        title=Title
    )

    return figure


if __name__ == '__main__':

    def GetFiles(traj_folder):
        file_list = []
        onlyfilesList = []
        onlyfolders = listdir(traj_folder)

        for folder in onlyfolders:
            oppath = join(traj_folder, folder)
            onlyfiles = [f for f in listdir(oppath) if isfile(join(oppath, f))]

            for file in onlyfiles:
                pack = [oppath, file]
                onlyfilesList.append(pack)

        for pack in onlyfilesList:
            splitter = pack[1].split("-")
            if ((splitter[0] == 'DJI') and (splitter[1] == 'FLOG')):
                file_list.append(pack)
            if (splitter[0] == 'ACCEL'):
                file_list.append(pack)

        return file_list


    # debugLevel variable
    # 1: print TimeStamp, Altitude, Heading/Course, HSpeed, VSpeed
    # 2: plot Altitude vs. Time, Heading/Course vs. Time, Latitude vs. Longitude

    # Line with folder in which measures are saved:
    traj_repo_folder = "D:\\DronePerformance\\Data\\16-09-2024"
    # traj_repo_folder = "D:\\U-ELCOME\\AlguaireEstiu2024\\17-08-2024-20240905T110402Z-001\\17-08-2024"
    operationId = "Telemetry"
    result_folder = "PerformanceDiagrams"

    accel_repo_folder = "D:\\DronePerformance\\Data\\16-09-2024"
    accelId = "Accelerations"
    accel_result_folder = "AccelerationDiagrams"

    debugLevel = 2

    traj_folder = traj_repo_folder + "\\" + operationId

    accel_folder = accel_repo_folder + "\\" + accelId

    dji_trajectory_file_list = GetFiles(traj_folder)
    print("Number of DJI files to analyze: ", len(dji_trajectory_file_list), "\n")
    print("Analyzing trajectories at folder: \n\t\t", traj_folder, "\n")

    acceleration_trajectory_file_list = GetFiles(accel_folder)
    print("Number of acceleration files to analyze: ", len(acceleration_trajectory_file_list), "\n")
    print("Analyzing trajectories at folder: \n\t\t", accel_folder, "\n")

    TrajAnalysis = pa.PerformanceAnalysis()

    TrajAnalysis.trajectoryFolder = traj_folder
    TrajAnalysis.trajectoryList = dji_trajectory_file_list

    AccelAnalysis = aa.AccelerationAnalysis()

    AccelAnalysis.accelerationFolder = accel_folder
    AccelAnalysis.accelerationList = acceleration_trajectory_file_list

    if os.path.exists(accel_repo_folder + "\\" + accel_result_folder):
        shutil.rmtree(accel_repo_folder + "\\" + accel_result_folder)
    os.mkdir(accel_repo_folder + "\\" + accel_result_folder)

    for trajectory_pack in TrajAnalysis.trajectoryList:

        print("Analyzing trajectory:\t", trajectory_pack[1], "\n")

        telSequence = tr.readDJITelemetryFile(trajectory_pack[0] + "\\" + trajectory_pack[1])

        LatitudeArray, LongitudeArray, TimestampArray = [], [], []

        for sample in telSequence:
            if sample.type == mo.Object_Type.djiGNSSIMUNavigation:
                latitude = sample.latitude
                LatitudeArray.append(latitude)
                longitude = sample.longitude
                LongitudeArray.append(longitude)
                timestamp = [sample.timestamp / 1000]
                TimestampArray.append(timestamp)

        for acceleration_pack in AccelAnalysis.accelerationList:

            print("Analyzing acceleration file:\t", acceleration_pack[1], "\n")
            if acceleration_pack[0].split("\\")[-1].replace("ACCEL-", "").replace(".csv", "") == \
                    trajectory_pack[0].split("\\")[-1].replace(
                            "DJI-FLOG-", ""):
                accelSequence = ar.readAccelerationFile(acceleration_pack[0] + "\\" + acceleration_pack[1])

                AccelLongitudeArray = []
                AccelNameArray = []
                doubleMeasure = False
                for sample in telSequence:
                    foundSample = False
                    if not doubleMeasure:
                        for accelSample in accelSequence:
                            if (not foundSample) and (
                                    (round(sample.timestamp / 1000, 2) % accelSample.timestamp)) < 1.5:
                                AccelLongitudeArray.append(sample.longitude)
                                AccelNameArray.append(accelSample.name)
                                foundSample = True
                            elif (round(sample.timestamp / 1000, 2) % accelSample.timestamp) < 1.5:
                                AccelLongitudeArray.append(sample.longitude)
                                AccelNameArray.append(accelSample.name)
                                doubleMeasure = True

                        if not foundSample and not doubleMeasure:
                            AccelLongitudeArray.append(None)
                            AccelNameArray.append(None)
                    else:
                        doubleMeasure = False

                dfAccelerationCoordinates = pd.DataFrame(
                    {'Latitude': LatitudeArray, 'Longitude': LongitudeArray, 'AccelLongitude': AccelLongitudeArray,
                     'Timestamp': TimestampArray, 'AccelName': AccelNameArray})

                fig = generateAccelerationTrajectoryPlotComplete(dfAccelerationCoordinates['Latitude'],
                                                                 dfAccelerationCoordinates['Longitude'],
                                                                 dfAccelerationCoordinates['AccelLongitude'],
                                                                 dfAccelerationCoordinates['AccelName'],
                                                                 "Acceleration Coordinates Plot")

                file_suffix = pathlib.Path(trajectory_pack[1]).stem

                filename = accel_repo_folder + "\\" + accel_result_folder + "\\" + file_suffix + "-AccelerationCoordinatesComplete.html"
                print("Generating figure: " + filename)
                plot(fig, filename=filename, auto_open=False)

                print("Done.")
            else:
                dfAccelerationCoordinates = pd.DataFrame(
                    {'Latitude': LatitudeArray, 'Longitude': LongitudeArray,
                     'Timestamp': TimestampArray})

                fig = generateAccelerationTrajectoryPlot(dfAccelerationCoordinates['Latitude'],
                                                         dfAccelerationCoordinates['Longitude'],
                                                         "Acceleration Coordinates Plot")

                file_suffix = pathlib.Path(trajectory_pack[1]).stem

                filename = accel_repo_folder + "\\" + accel_result_folder + "\\" + file_suffix + "-AccelerationCoordinates.html"
                print("Generating figure: " + filename)
                plot(fig, filename=filename, auto_open=False)

                print("Done.")



    print("DJI - Set completed.")

    print("Done.\n")
    sys.exit(0)
