import sys
import os, shutil
import math
import pathlib
import pandas as pd
import docx as docx
from docx.shared import Inches, Cm, Pt
from docx.enum.section import WD_SECTION_START


def set_column_width(column, width):
    for cell in column.cells:
        cell.width = width

def set_text_size(table, size):
    for row in table.rows:
        for cell in row.cells:
            paragraphs = cell.paragraphs
            for paragraph in paragraphs:
                for run in paragraph.runs:
                    font = run.font
                    font.size = Pt(size)


if __name__ == '__main__':

    requirements_folder = "E:\Documents\\SVN-Projects\\Project-CORUS-XUAM\\Technical\\T5.3\\Tables\\"
    requirements_file = "CORUS-XUAM U-space requirements_Combined.xlsx"
    docx_requirements_file = "CORUS-XUAM U-space requirements.docx"

    print("Analyzing requirements file:\t", requirements_file, "\n")

    xl = pd.ExcelFile(requirements_folder + requirements_file)
    #df_requirenments = pd.read_excel(requirements_folder + requirements_file, usecols='A:C,AQ:DM', header=[0,1])
    df_requirenments = pd.read_excel(requirements_folder + requirements_file, header=[0,1])

    #print(df_requirenments.columns)

    df_requirenments = df_requirenments[['Identifier', 'Title', 'Requirement', 'Status', 'Rationale', 'CAT-OPE', 'CAT-SAF', 'CAT-SEC', "CAT-PER", "CAT-S&R", "U-space Service name", "U-space Capability name", 'SESAR Solution Identifier', 'Sub-operating environment']]

    print("This are the existing columns:")
    for col in df_requirenments.columns:
        print(col)

    if os.path.exists(docx_requirements_file):
        os.remove(docx_requirements_file)

    mydoc = docx.Document()

    from docx.shared import Pt

    style = mydoc.styles['Normal']
    font = style.font
    font.size = Pt(9)

    current_section = mydoc.sections[-1]  # last section in document
    header = current_section.header
    header.is_linked_to_previous = False
    #paragraph = header.paragraphs[0]
    #paragraph.style = mydoc.styles["Header"]
    mydoc.add_heading('Safety, Performance and Interoperability Requirements (SPR-INTEROP)', level=1)

    for index, row in df_requirenments.iterrows():
        if index < 1:
            continue

        Identifier = row[('Identifier', 'Unnamed: 0_level_1')]

        if pd.isnull(Identifier):
            continue

        Title = row[('Title', 'Unnamed: 1_level_1')]
        Requirement = row[('Requirement', 'Unnamed: 2_level_1')]
        Status = row[('Status', 'Unnamed: 42_level_1')]
        Rationale = row[('Rationale', slice(None))].values[0]

        CatOpe = row[('CAT-OPE', slice(None))].values[0]
        CatSafety = row[('CAT-SAF', slice(None))].values[0]
        CatSecurity = row[('CAT-SEC', slice(None))].values[0]
        CatPerformance = row[('CAT-PER', slice(None))].values[0]
        CatStandard = row[('CAT-S&R', slice(None))].values[0]

        Cat_Operational = (CatOpe == "X")
        Cat_Safety = (CatSafety == "X")
        Cat_Security = (CatSecurity == "X")
        Cat_Performance = (CatPerformance == "X")
        Cat_Standard = (CatStandard == "X")

        Solution = row[('SESAR Solution Identifier', slice(None))].values[0]
        OpEnvironment = row[('Sub-operating environment', slice(None))].values[0]


        paragraph = mydoc.add_paragraph('[REQ]')
        paragraph.paragraph_format.space_before = Inches(0.2)
        paragraph.paragraph_format.space_after = Inches(0.0)
        paragraph.style = mydoc.styles['Normal']

        table = mydoc.add_table(rows=6, cols=2)
        table.style = 'Table Grid'
        table.autofit = False

        set_column_width(table.columns[0], Cm(3.72))
        set_column_width(table.columns[1], Cm(12.58))

        column = table.columns[0]
        column.width=Inches(0.5)
        column.cells[0].text = 'Identifier'
        column.cells[1].text = 'Title'
        column.cells[2].text = 'Requirement'
        column.cells[3].text = 'Status'
        column.cells[4].text = 'Rationale'
        column.cells[5].text = 'Category'

        column = table.columns[1]
        column.width=Inches(10)
        column.cells[0].text = Identifier
        column.cells[1].text = Title
        column.cells[2].text = Requirement
        column.cells[3].text = Status
        if not pd.isna(Rationale):
            column.cells[4].text = Rationale

        category = ""
        if Cat_Operational:
            category += '<' + 'operational' + '>'
        if Cat_Safety:
            category += '<' + 'safety' + '>'
        if Cat_Security:
            category += '<' + 'security' + '>'
        if Cat_Performance:
            category += '<' + 'performance' + '>'
        if Cat_Standard:
            category += '<' + 'standard & regulatory' + '>'

        column.cells[5].text = category

        set_text_size(table, 9)


        df_services_row = row[('U-space Service name')]

        services = ""
        if df_services_row["<All Services>"] == "X":
            services = "<All Services>"
        else:
            first = True
            for (colname, colval) in df_services_row.iteritems():
                print(colname, colval)
                if colval == "X":
                    if first:
                        services += colname
                        first = False
                    else:
                        services += ", " + colname

        df_capability_row = row[('U-space Capability name')]

        capabilities = ""
        if df_capability_row["<All Capabilities>"] == "X":
            capabilities = "<All Capabilities>"
        else:
            first = True
            for (colname, colval) in df_capability_row.iteritems():
                print(colname, colval)
                if colval == "X":
                    if first:
                        capabilities += colname
                        first = False
                    else:
                        capabilities += ", " + colname


        paragraph = mydoc.add_paragraph('[REQ Trace]')
        paragraph.paragraph_format.space_before = Inches(0.1)
        paragraph.paragraph_format.space_after = Inches(0.0)
        paragraph.style = mydoc.styles['Normal']

        table = mydoc.add_table(rows=5, cols=3)
        table.style = 'Table Grid'
        table.autofit = False

        set_column_width(table.columns[0], Cm(4.71))
        set_column_width(table.columns[1], Cm(4.88))
        set_column_width(table.columns[2], Cm(6.71))

        rows = table.rows[0]
        rows.cells[0].text = "Relationship"
        rows.cells[1].text = "Linked Element Type"
        rows.cells[2].text = "Identifier"

        rows = table.rows[1]
        rows.cells[0].text = "<ALLOCATED_TO>"
        rows.cells[1].text = "<SESAR Solution>"
        if not pd.isna(Solution):
            rows.cells[2].text = Solution

        rows = table.rows[2]
        rows.cells[0].text = "<ALLOCATED_TO>"
        rows.cells[1].text = "<U-space Service>"
        rows.cells[2].text = services

        rows = table.rows[3]
        rows.cells[0].text = "<ALLOCATED_TO>"
        rows.cells[1].text = "<U-space Capability>"
        rows.cells[2].text = capabilities

        rows = table.rows[4]
        rows.cells[0].text = "<ALLOCATED_TO>"
        rows.cells[1].text = "<Operating Environment>"
        if not pd.isna(OpEnvironment):
            rows.cells[2].text = OpEnvironment

        set_text_size(table, 9)

    mydoc.save(requirements_folder + docx_requirements_file)
    print("Done.\n")
    #sys.exit(0)

