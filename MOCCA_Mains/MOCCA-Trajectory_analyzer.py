import sys
import os, shutil
import pathlib
import pandas as pd

from os.path import isfile, join
from os import listdir
from Mocca_Analyzer.Mocca import TelemetryReader as tr
from Mocca_Analyzer.Performance_Analyzer import FlightEventStore as fes
from Mocca_Analyzer.Performance_Analyzer import PerformanceAnalysis as pa, AltitudeAnalysis as aa, StraightSegmentAnalysis as ssa, TurnAnalysis as ta
from Mocca_Analyzer.Plots import ClimbDescentInterpolationPlot as cdip, StraightSegmentPlot as ssp

if __name__ == '__main__':

    def GetFiles(traj_folder):
        sw_trajectory_file_list = []
        ml_trajectory_file_list = []
        onlyfiles = [f for f in listdir(traj_folder) if isfile(join(traj_folder, f))]
        for file in onlyfiles:
            splitter = file.split("-")
            if ((splitter[0] == 'DYN') and (splitter[1] == 'ML')):
                ml_trajectory_file_list.append(file)
            elif ((splitter[0] == 'DYN') and (splitter[1] == 'FLOG')):
                sw_trajectory_file_list.append(file)
        return sw_trajectory_file_list, ml_trajectory_file_list

    # folder_selected variable
    # 2: "\Set-Flight2"
    # 3: "\Set-Flight3"
    # 4: "\Aire3-005A_ EC-RPAS-0011_16-09-2020"
    # 5: "\Aire3-005A_EC-RPAS-0011_02-07-2020"
    # 6: "\Aire3-005A_EC-RPAS-0011_29-01-2020"
    # 7: "\Aire3-005A_EC-RPAS-0011_31-01-2020"
    #-------------------------------------------
    # debugLevel variable
    # 1: print TimeStamp, Altitude, Heading/Course, HSpeed, VSpeed
    # 2: plot Altitude vs. Time, Heading/Course vs. Time, Latitude vs. Longitude

    #traj_folder = "C:\\Users\Acer\Desktop\TFG\Drone-Performance-Analysis"
    traj_repo_folder = "F:\Documents\SVN-TFC\Drone-Performance-Analysis"
    operationId = 4
    result_folder = "PerformanceDiagrams"

    debugLevel = 2

    doSWAnalysis = False #True: Analise SW files
    doFlightAnalsis = True #True: Analise Course change + 'altitude change' + Straight segment
    doAltitudeAnalysis = True #True: Analise altitude change
    doTurnAnalysis= False #True: Analise turn  events
    doStraightSegmentAnalysis= False #True:  Analise straight segments events



    traj_folder = traj_repo_folder + "\\" + operationId

    sw_trajectory_file_list, ml_trajectory_file_list = GetFiles(traj_folder)
    print("Number of ML files to analyze: \n\t\t", len(ml_trajectory_file_list), "\n")
    print("Analyzing trajectories at folder: \n\t\t", traj_folder, "\n")


    TrajAnalysis = pa.PerformanceAnalysis()

    TrajAnalysis.trajectoryFolder = traj_folder
    TrajAnalysis.trajectoryList = sw_trajectory_file_list

    if os.path.exists(traj_folder + "\\" + result_folder):
        shutil.rmtree(traj_folder + "\\" + result_folder)
    os.mkdir(traj_folder + "\\" + result_folder)

    if doSWAnalysis:
        for trajectory_file in TrajAnalysis.trajectoryList:

            print("Analyzing trajectory:\t", trajectory_file, "\n")

            telSequence = tr.readSWTelemetryFile(traj_folder + "\\" + trajectory_file)

            fea = fes.FlightEventStore()

            for sample in telSequence:
                fea.insertAnalyzing(sample)

            file_suffix = pathlib.Path(trajectory_file).stem

            fea.doneAnalyzing(traj_folder + "\\" + result_folder + "\\SW-" + file_suffix)
            TrajAnalysis.eventAnalysis.append(fea)

            aa.analyzeAltitudeEvents(fea)
            aa.analyzeVSpeedEvents(fea)

            print("Done.")

        print("SW - Set completed.")

    mlTrajAnalysis = pa.PerformanceAnalysis()

    mlTrajAnalysis.trajectoryFolder = traj_folder
    mlTrajAnalysis.trajectoryList = ml_trajectory_file_list

    globalEventNumber = 0
    altitudeParams = []
    straightSegmentxParam = []
    straightSegmentyParam = []
    VerticalClimbEventsAltitudeVariance = []  # Needed for Time vs. AltitudeVariance Plot
    VerticalClimbEventsDeltaTime = []
    VerticalDescentEventsAltitudeVariance = []
    VerticalDescentEventsDeltaTime = []
    NonVerticalClimbEventsAltitudeVariance = []  # Needed for Time vs. AltitudeVariance Plot
    NonVerticalClimbEventsDeltaTime = []
    NonVerticalDescentEventsAltitudeVariance = []
    NonVerticalDescentEventsDeltaTime = []
    NonVerticalClimbEventsHorizontalVariance = []
    NonVerticalDescentEventsHorizontalVariance = []
    HorizontalSpeedClimb = [] # Needed for 3d plot Non-VErtical Climbs/Descents. HorizontalVariance vs. Horizontal Speed vs. Time
    HorizontalSpeedDescent = []
    #TODO: delete interpolationVariable
    interpolationVariable = []
    interpolationVariable2 = []

    if not doFlightAnalsis:
    #Do not do flight analysis (Course change, altitude change, Straight segment)
        for trajectory_file in mlTrajAnalysis.trajectoryList:
            print("Analyzing trajectory:\t", trajectory_file, "\n")

            telSequence = tr.readMLTelemetryFile(traj_folder + "\\" + trajectory_file)

            fea = fes.FlightEventStore(globalEventNumber, debug = debugLevel)

            for sample in telSequence:
                fea.insertAnalyzing(sample)
            fea.identifyTakeOffLanding()
            fea.refineCourseEvents()

            file_suffix = pathlib.Path(trajectory_file).stem

            os.mkdir(traj_folder + "\\" + result_folder + "\\" + file_suffix)
            os.mkdir(traj_folder + "\\" + result_folder + "\\" + file_suffix + "\\" + "Flight_Analysis")

            TrajAnalysis.eventAnalysis.append(fea)

            globalEventNumber = fea.globalEventNumber
            fea.doneAnalyzing(traj_folder + "\\" + result_folder + "\\" + file_suffix + "\\ML-" + file_suffix)

    else:
    #Do flight analysis
        #ssp global plot. One x forder
        axsspGlobal, figsspGlobal = ssp.getDynamicStraightSegmentPlot()
        for trajectory_file in mlTrajAnalysis.trajectoryList:
            print("Analyzing trajectory:\t", trajectory_file, "\n")

            telSequence = tr.readMLTelemetryFile(traj_folder + "\\" + trajectory_file)

            fea = fes.FlightEventStore(globalEventNumber, debug = debugLevel)

            for sample in telSequence:
                fea.insertAnalyzing(sample)
            fea.identifyTakeOffLanding()
            # TODO: en /ALL line 205 index out of range
            # fea.refineCourseEvents()

            #fea.refineChangeAltitudeEvents()

            file_suffix = pathlib.Path(trajectory_file).stem

            os.mkdir(traj_folder + "\\" + result_folder + "\\" + file_suffix)
            os.mkdir(traj_folder + "\\" + result_folder + "\\" + file_suffix + "\\" + "Flight_Analysis")
            os.mkdir(traj_folder + "\\" + result_folder + "\\" + file_suffix + "\\" + "Altitude_Analysis")

            TrajAnalysis.eventAnalysis.append(fea)

            #Altitude[aa] + StraightSegment[ssa] + Turn[ta] Analysis
            if doAltitudeAnalysis:
                altitudeParams = aa.analyzeAltitudeEvents(fea, traj_folder + "\\" + result_folder + "\\" + file_suffix + "\\Altitude_Analysis\\ML-" + file_suffix, altitudeParams, trajectory_file, interpolationVariable,interpolationVariable2,HorizontalSpeedClimb, HorizontalSpeedDescent, VerticalClimbEventsAltitudeVariance, VerticalClimbEventsDeltaTime, VerticalDescentEventsAltitudeVariance,VerticalDescentEventsDeltaTime,NonVerticalClimbEventsAltitudeVariance, NonVerticalClimbEventsDeltaTime, NonVerticalDescentEventsAltitudeVariance,NonVerticalDescentEventsDeltaTime, NonVerticalClimbEventsHorizontalVariance, NonVerticalDescentEventsHorizontalVariance)
            if doStraightSegmentAnalysis:
                ssa.analyzeStraightSegmentCourseEvents(fea, traj_folder + "\\" + result_folder + "\\" + file_suffix + "\\" + "Flight_Analysis" + "\\" + "Straight_Segments", axsspGlobal)
            if doTurnAnalysis:
                ta.analyzeTurnEvents(fea,traj_folder + "\\" + result_folder + "\\" + file_suffix + "\\" + "Flight_Analysis" + "\\" + "Turns")

            if debugLevel > 1:
                fea.doneAnalyzing(traj_folder + "\\" + result_folder + "\\" + file_suffix + "\\ML-" + file_suffix)

            globalEventNumber = fea.globalEventNumber

        #ssp global plot
        ssp.closeDynamicPlot(figsspGlobal, traj_folder + "\\" + result_folder + "\\"  + "StraightSegment-LengthVelocity.png")

        if doAltitudeAnalysis:
            #Climb Descent Interpolation
            cdip.InterpolateClimbDescent(traj_folder + "\\" + result_folder + "\\" ,interpolationVariable,interpolationVariable2, HorizontalSpeedClimb, HorizontalSpeedDescent,VerticalClimbEventsAltitudeVariance, VerticalClimbEventsDeltaTime, VerticalDescentEventsAltitudeVariance,VerticalDescentEventsDeltaTime,NonVerticalClimbEventsAltitudeVariance, NonVerticalClimbEventsDeltaTime, NonVerticalDescentEventsAltitudeVariance,NonVerticalDescentEventsDeltaTime, NonVerticalClimbEventsHorizontalVariance, NonVerticalDescentEventsHorizontalVariance)
            #Altitude.csv file
            Altitude_csvfile = pd.DataFrame(altitudeParams)
            csv_header = ['File','eventType',  'LongInitial', 'LatInitial', 'AltInitial', 'LongFinal', 'LatFinal', 'AltFinal', 'Climb/Descent duration[s]', 'AverageClimb/DescentRate']
            Altitude_csvfile.to_csv('AltitudeChange.csv', header=csv_header, index=False)
            print("Altitude.csv generated \n")


    print("Done.\n")
    sys.exit(0)

