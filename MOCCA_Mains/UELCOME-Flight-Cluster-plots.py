import plotly.express as px
import pandas as pd

# Sample data representing drone flight operations in Catalonia
data = {
    'region': ['Barcelona', 'Barcelona', 'Barcelona', 'Tarragona', 'Tarragona', 'Lleida', 'Girona', 'Girona'],
    'lat': [41.38879, 41.38768, 41.38506, 41.11824, 41.11637, 41.6177, 41.9794, 41.9811],
    'long': [2.15899, 2.16253, 2.17357, 1.24449, 1.24031, 0.6220, 2.8252, 2.8278],
    'cnt': [10, 15, 5, 2, 3, 1, 2, 1]  # Number of operations in that area
}

# Create a DataFrame
#df = pd.DataFrame(data)

df2023 = pd.read_excel("C:\\Users\\alber\\Downloads\\FlightOperationResume-BOMBERS-2023.xlsx",  skipfooter=2)

filtered_df2023 = df2023[df2023['Number of flights'] >= 1]

df2024 = pd.read_excel("C:\\Users\\alber\\Downloads\\FlightOperationResume-BOMBERS-2024.xlsx", skipfooter=2)

filtered_df2024 = df2024[df2024['Number of flights'] >= 1]

df_combined = pd.concat([filtered_df2023, filtered_df2024], axis=0, ignore_index=True)

df_expanded = df_combined.loc[df_combined.index.repeat(df_combined['Number of flights'])].reset_index(drop=True)

df_expanded.loc[df_expanded['Good Duration'] > 1, 'Good Duration'] = df_expanded.loc[df_expanded['Number of flights'] > 1, 'Good Duration'] / df_expanded.loc[df_expanded['Number of flights'] > 1, 'Number of flights']

df_expanded.loc[df_expanded['Number of flights'] > 1, 'Number of flights'] = 1

# Create a scatter mapbox with clustering
fig = px.scatter_mapbox(
    df_expanded,
    lat="Lat",
    lon="Lon",
    size="Valid flight",  # Size based on the number of operations
    hover_name="Sub3",  # Display region name on hover
    size_max=20,  # Maximum marker size
    zoom=6,  # Adjust zoom level to fit Catalonia
    mapbox_style="open-street-map",  # Choose a Mapbox style
)

# Enable clustering (if applicable)
# For Plotly, clustering is usually handled automatically when size is specified
fig.update_traces(cluster=dict(enabled=True))

# Show the figure
fig.show()

monthly_activations = df_combined.groupby(['Year', 'Month']).agg(
    flight_count=('Number of flights', 'sum'),
    duration_count=('Good Duration', 'sum')
).reset_index()

monthly_activations['year-month'] = monthly_activations['Year'].astype(str) + '-' + monthly_activations['Month'].apply(lambda x: f'{x:02d}')

# Define the start and end date for filtering
start_date = '2023-06'
end_date = '2024-11'

# Filter the data based on the range
monthly_flights = monthly_activations[(monthly_activations['year-month'] >= start_date) & (monthly_activations['year-month'] <= end_date)]

# Create a complete list of all year-month combinations for the filtered range
years = range(2023, 2025)  # For 2023-2024
months = range(1, 13)  # Months from 1 to 12

# Create all combinations of years and months in the desired range
all_combinations = pd.MultiIndex.from_product([years, months], names=['Year', 'Month']).to_frame(index=False)

# Filter the all_combinations to match the range from 2023-06 to 2024-11
all_combinations['year-month'] = all_combinations['Year'].astype(str) + '-' + all_combinations['Month'].apply(lambda x: f'{x:02d}')
all_combinations = all_combinations[
    (all_combinations['year-month'] >= start_date) & (all_combinations['year-month'] <= end_date)
]

# Merge the complete list of year-month combinations with the filtered flight data
complete_monthly_activations = pd.merge(all_combinations, monthly_flights, on=['Year', 'Month'], how='left')

# Fill NaN flight counts with 0 for months without flights
complete_monthly_activations['flight_count'] = complete_monthly_activations['flight_count'].fillna(0)

# Fill NaN flight counts with 0 for months without flights
complete_monthly_activations['duration_count'] = complete_monthly_activations['duration_count'].fillna(0)

# Create 'year-month' column again with proper format
complete_monthly_activations['year-month'] = complete_monthly_activations['Year'].astype(str) + '-' + complete_monthly_activations['Month'].apply(lambda x: f'{x:02d}')

monthly_flights = df_expanded.groupby(['Year', 'Month']).agg(
    flight_count=('Number of flights', 'sum'),
    duration_count=('Good Duration', 'sum')
).reset_index()

monthly_flights['year-month'] = monthly_flights['Year'].astype(str) + '-' + monthly_flights['Month'].apply(lambda x: f'{x:02d}')

# Define the start and end date for filtering
start_date = '2023-06'
end_date = '2024-11'

# Filter the data based on the range
monthly_flights = monthly_flights[(monthly_flights['year-month'] >= start_date) & (monthly_flights['year-month'] <= end_date)]

# Create a complete list of all year-month combinations for the filtered range
years = range(2023, 2025)  # For 2023-2024
months = range(1, 13)  # Months from 1 to 12

# Create all combinations of years and months in the desired range
all_combinations = pd.MultiIndex.from_product([years, months], names=['Year', 'Month']).to_frame(index=False)

# Filter the all_combinations to match the range from 2023-06 to 2024-11
all_combinations['year-month'] = all_combinations['Year'].astype(str) + '-' + all_combinations['Month'].apply(lambda x: f'{x:02d}')
all_combinations = all_combinations[
    (all_combinations['year-month'] >= start_date) & (all_combinations['year-month'] <= end_date)
]

# Merge the complete list of year-month combinations with the filtered flight data
complete_monthly_flights = pd.merge(all_combinations, monthly_flights, on=['Year', 'Month'], how='left')

# Fill NaN flight counts with 0 for months without flights
complete_monthly_flights['flight_count'] = complete_monthly_flights['flight_count'].fillna(0)

# Fill NaN flight counts with 0 for months without flights
complete_monthly_flights['duration_count'] = complete_monthly_flights['duration_count'].fillna(0)

# Create 'year-month' column again with proper format
complete_monthly_flights['year-month'] = complete_monthly_flights['Year'].astype(str) + '-' + complete_monthly_flights['Month'].apply(lambda x: f'{x:02d}')

# Plot the histogram using Plotly Express
fig = px.bar(complete_monthly_flights, x='year-month', y='flight_count', title="Number of Flights per Month from June 2023 to November 2024")
fig.update_layout(xaxis_title="Year-Month", yaxis_title="Number of Flights", xaxis=dict(type='category'), font=dict(size=28))
fig.show()

complete_monthly_flights['Cumulative_Flight_Count'] = complete_monthly_flights['flight_count'].cumsum()

fig = px.bar(complete_monthly_flights, x='year-month', y='Cumulative_Flight_Count', title='Cumulative Number of Flights per Month from June 2023 to November 2024')
fig.update_layout(xaxis_title='Year-Month', yaxis_title='Cumulative Number of Flights', xaxis=dict(type='category'), font=dict(size=28))
fig.show()

complete_monthly_activations['Cumulative_Duration'] = complete_monthly_activations['duration_count'].cumsum()

fig = px.bar(complete_monthly_activations, x='year-month', y='Cumulative_Duration', title='Cumulative Duration of Activations per Month from June 2023 to November 2024')
fig.update_layout(xaxis_title='Year-Month', yaxis_title='Cumulative Duration of Activations [s]', xaxis=dict(type='category'), font=dict(size=28))
fig.show()
