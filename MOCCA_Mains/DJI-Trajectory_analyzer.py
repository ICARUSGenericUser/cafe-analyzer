import sys
import os, shutil
import pathlib
import pandas as pd


from DJI_Analyzer import TelemetryReader as tr


if __name__ == '__main__':

    traj_folder1 = "E:\XUAM-FlightData\Alguaire-21Oct-UPC"
    trajectory_file1 = "21-10-2021-14-31-Flight-Airdata.csv"

    traj_folder2 = "E:\XUAM-FlightData\Alguaire-21Oct-MARS"
    trajectory_file2 = "Oct-21st-2021-02-47PM-Flight-Airdata.csv"


    print("Analyzing trajectory:\t", trajectory_file1, "\n")

    telSequence = tr.readDJITelemetryFile(traj_folder1 + "\\" + trajectory_file1, traj_folder1 + "\\" + trajectory_file1)

    print("Done.\n")
    sys.exit(0)



