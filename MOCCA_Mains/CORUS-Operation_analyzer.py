import sys
import os, shutil
import math
import pathlib
import pandas as pd
import numpy as np
from datetime import datetime
import plotly.graph_objects as go
from plotly.subplots import make_subplots


from DJI_Analyzer import TelemetryReader as tr


df_columns = [
    "Operator",
    "OperationDate",
    "Callsign",
    "TimeActivation",
    "TimeCompletion",
    "SlotTime",
    "FlightTime",
    "UPCId",
    "Vertiport",
    "Delivery",
    "OperationId",
    "Status",
    "UPCDroneId",
    "INDRADroneId",
    "ASLOGICDroneId"
]

df_columns_flight = [
    "Callsign",
    "Delivery",
    'Filename',
    'APActivate',
    'APDeactivate',
    'APTakeoff',
    'APEndTakeoff',
    'APStartLanding',
    'APLanded',
    'nSamples',
    'Hover',
    'Model'
]

df_columns_flight2 = [
    'OperationId',
    'APActivate',
    'APDeactivate',
    'APTakeoff',
    'APEndTakeoff',
    'APStartLanding',
    'APLanded',
    'USMessages'
]

df_columns_intervals = [
    'Date',
    'Start',
    'End',
    'NumberOperations',
    'Density',
    'TUsage',
    'TDensity'
]

color1 = '#EB89B5'
color2 = '#330C73'
color3 = '#350C03'




def generate_individual_histogram(df, lowerLimit, upperLimit, color, text, binsize):
    fig = go.FigureWidget(
        [go.Histogram(
            x=df,
            histnorm='percent',
            name=text,  # name used in legend and hover labels
            xbins=dict(
                start=lowerLimit,
                end=upperLimit,
                size=binsize
            ),
            marker_color=color,
            opacity=0.75,
            texttemplate="%{y}",
            textfont_size=8
        )],
        go.Layout(
            title=text,
            xaxis={'range': [lowerLimit, upperLimit],
                   'title': text},
            yaxis={'title': 'Percentage of total operations'},
            bargap=0.1))
    hist = fig.data[0]

    # Install xaxis zoom callback
    def handle_zoom(xaxis, xrange):
        filtered_x = df[np.logical_and(xrange[0] <= df, df <= xrange[1])]
        hist.x = filtered_x

    fig.layout.xaxis.on_change(handle_zoom, 'range')
    return fig




def generate_comparison_histogram1(df1, lowerLimit1, color1,  binsize, firstSet, caption, xaxis):
    trace1 = go.Histogram(
        x=df1,
        histnorm='percent',
        name=firstSet,  # name used in legend and hover labels
        xbins=dict(
            start=lowerLimit1,
#            end=upperLimit1,
            size=binsize
        ),
        marker_color=color1,
        opacity=0.75,
        texttemplate="%{y}",
        textfont_size=8
    )

    fig = go.FigureWidget(
        data=[trace1],
        layout=go.Layout(
            title=caption,
            xaxis={'title': xaxis},
            yaxis={'title': '% total encounters'},
            bargap=0.1
        )
    )

    hist1 = fig.data[0]

    # Install xaxis zoom callback
    def handle_zoom(xaxis, xrange):
        filtered_x1 = df1[np.logical_and(xrange[0] <= df1, df1 <= xrange[1])]
        hist1.x = filtered_x1

    fig.layout.xaxis.on_change(handle_zoom, 'range')

    return fig



def generate_comparison_histogram2(df1, df2, lowerLimit1, lowerLimit2, color1, color2,
                                   binsize, firstSet, secondSet, caption, xaxes):
    trace1 = go.Histogram(
        x=df1,
        histnorm='percent',
        cumulative_enabled=True,
        name=firstSet,  # name used in legend and hover labels
        xbins=dict(
            start=lowerLimit1,
#            end=upperLimit1,
            size=binsize
        ),
        marker_color=color1,
        opacity=0.75,
        texttemplate="%{y}",
        textfont_size=8
    )

    trace2 = go.Histogram(
        x=df2,
        histnorm='percent',
        cumulative_enabled=True,
        name=secondSet,
        xbins=dict(
            start=lowerLimit2,
#            end=upperLimit2,
            size=binsize
        ),
        marker_color=color2,
        opacity=0.75,
        texttemplate="%{y}",
        textfont_size=8
    )

    fig = go.FigureWidget(
        data=[trace1, trace2],
        layout=go.Layout(
            title=caption,
            xaxis={'title': xaxes},
            yaxis={'title': '% total encounters'},
            bargap=0.1
        )
    )

    hist1 = fig.data[0]
    hist2 = fig.data[1]

    # Install xaxis zoom callback
    def handle_zoom(xaxis, xrange):
        filtered_x1 = df1[np.logical_and(xrange[0] <= df1, df1 <= xrange[1])]
        hist1.x = filtered_x1

        filtered_x2 = df2[np.logical_and(xrange[0] <= df2, df2 <= xrange[1])]
        hist2.x = filtered_x2

    fig.layout.xaxis.on_change(handle_zoom, 'range')

    return fig



def generate_comparison_histogram3(df1, df2, df3, lowerLimit1, lowerLimit2, lowerLimit3, color1, color2, color3,
                                   binsize, firstSet, secondSet, thirdSet, caption):
    trace1 = go.Histogram(
        x=df1,
        histnorm='percent',
        name=firstSet,  # name used in legend and hover labels
        xbins=dict(
            start=lowerLimit1,
#            end=upperLimit1,
            size=binsize
        ),
        marker_color=color1,
        opacity=0.75,
        texttemplate="%{y}",
        textfont_size=8
    )

    trace2 = go.Histogram(
        x=df2,
        histnorm='percent',
        name=secondSet,
        xbins=dict(
            start=lowerLimit2,
#            end=upperLimit2,
            size=binsize
        ),
        marker_color=color2,
        opacity=0.75,
        texttemplate="%{y}",
        textfont_size=8
    )

    trace3 = go.Histogram(
        x=df3,
        histnorm='percent',
        name=thirdSet,
        xbins=dict(
            start=lowerLimit3,
            #            end=upperLimit2,
            size=binsize
        ),
        marker_color=color3,
        opacity=0.75,
        texttemplate="%{y}",
        textfont_size=8
    )

    fig = go.FigureWidget(
        data=[trace1, trace2, trace3],
        layout=go.Layout(
            title=caption,
            xaxis={'title': 'TimeRange (sec)'},
            yaxis={'title': '% total encounters'},
            bargap=0.1
        )
    )

    hist1 = fig.data[0]
    hist2 = fig.data[1]
    hist3 = fig.data[2]

    # Install xaxis zoom callback
    def handle_zoom(xaxis, xrange):
        filtered_x1 = df1[np.logical_and(xrange[0] <= df1, df1 <= xrange[1])]
        hist1.x = filtered_x1

        filtered_x2 = df2[np.logical_and(xrange[0] <= df2, df2 <= xrange[1])]
        hist2.x = filtered_x2

        filtered_x3 = df3[np.logical_and(xrange[0] <= df3, df3 <= xrange[1])]
        hist3.x = filtered_x3

    fig.layout.xaxis.on_change(handle_zoom, 'range')

    return fig


def generate_2D_Contour_Histogram(df1, df2, xaxisTitle, yaxisTitle, Title):
    fig = go.Figure()
    fig.add_trace(go.Histogram2dContour(
        x=df1,
        y=df2,
        colorscale='Hot',
        reversescale=True,
        xaxis='x',
        yaxis='y'
    ))
    fig.add_trace(go.Scatter(
        x=df1,
        y=df2,
        xaxis='x',
        yaxis='y',
        mode='markers',
        marker=dict(
            color='rgba(0,0,0,0.3)',
            size=3
        )
    ))
    fig.add_trace(go.Histogram(
        y=df2,
        histnorm='percent',
        xaxis='x2',
        marker=dict(
            color='rgba(0,0,0,1)'
        )
    ))
    fig.add_trace(go.Histogram(
        x=df1,
        histnorm='percent',
        yaxis='y2',
        marker=dict(
            color='rgba(0,0,0,1)'
        )
    ))

    fig.update_layout(
        autosize=False,
        xaxis=dict(
            title=xaxisTitle,
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        yaxis=dict(
            title=yaxisTitle,
            zeroline=False,
            domain=[0, 0.85],
            showgrid=False
        ),
        xaxis2=dict(
            zeroline=False,
            domain=[0.85, 1],
            showgrid=True
        ),
        yaxis2=dict(
            zeroline=False,
            domain=[0.85, 1],
            showgrid=True
        ),
        height=1000,
        width=1000,
        bargap=0.1,
        hovermode='closest',
        showlegend=True,
        title=Title
    )

    return fig


def generate_dual_polar_plot(rang, secondType, thirdType, upperLimit1, lowerLimit1, upperLimit2, lowerLimit2, title):
    fig = make_subplots(rows=1, cols=2, specs=[[{'type': 'polar'}] * 2])

    fig.add_trace(go.Scatterpolar(r=rang, theta=secondType, name='Azimuth vs Range'), 1, 1)
    fig.add_trace(go.Scatterpolar(r=rang, theta=thirdType, name='Elevation vs Range'), 1, 2)

    # Same data for the two Scatterpolar plots, we will only change the sector in the layout
    fig.update_traces(mode="markers",
                      #line_color="magenta",
                      marker=dict(
                          #color="royalblue",
                          symbol="square",
                          size=4
                      ))

    # The sector is [0, 360] by default, we update it for the first plot only
    fig.update_layout(
        title= title,
        showlegend=True,
        polar=dict(  # setting parameters for the second plot would be polar2=dict(...)
            sector=[upperLimit1, lowerLimit1],
        ),
        polar2=dict(
            sector=[upperLimit2, lowerLimit2]
        ),
        legend = dict(
            yanchor="top",
            y=0.99,
            xanchor="right",
            x=0.99
        ))
    return fig



if __name__ == '__main__':

    operation_folder = "E:\Documents\SVN-Projects\Project-CORUS-XUAM\VLD_Spain\Scenario-Data\CORUS-OPERATIONS\\UPC-Telem\\"

    operation_file = "SCHOP-RESUME-ALL-2022-03-08-09-10.csv"
    #operation_file = "SCHOP-RESUME-ALL-2022-03-09-10.csv"
    #operation_file = "SCHOP-RESUME-ALL-2022-03-XX.csv"

    flight_folder = "E:\Documents\SVN-Projects\Project-CORUS-XUAM\VLD_Spain\Scenario-Data\CORUS-OPERATIONS\\4DT-TELEMETRY-V3\\"
    flight_file = "Activation-Resume.csv"

    aslogic_folder = "E:\Documents\SVN-Projects\Project-CORUS-XUAM\VLD_Spain\Scenario-Data\CORUS-OPERATIONS\ASLOGIC-MISSIONS\\"
    aslogic_file = "Activation-Resume.csv"

    interval_file = "ACTIVATION-INTERVALS.csv"


    print("Analyzing operation file:\t", operation_file, "\n")

    df_operations = pd.read_csv(operation_folder + operation_file, index_col=False, header=None, sep=";", keep_default_na=False, skipinitialspace=True, names=df_columns)

    #df_operations['TimeActivation'] = pd.to_numeric(df_operations['TimeActivation'])
    #df_operations['TimeCompletion'] = pd.to_numeric(df_operations['TimeCompletion'])
    #df_operations['TimeActivation'] = df_operations['TimeActivation'] / 1000
    #df_operations['TimeCompletion'] = df_operations['TimeCompletion'] / 1000

    # Add an empty column as necessary
    df_operations['APFlown'] = 0
    df_operations['APTakeoff'] = 0
    df_operations['APEndTakeoff'] = 0
    df_operations['APStartLanding'] = 0
    df_operations['APLanded'] = 0
    df_operations['APTotalDuration'] = 0
    df_operations['APUSDuration'] = 0
    df_operations['APUSLaunchVariation'] = 0
    df_operations['EstimatedDuration'] = 0
    df_operations['EstimationVariation'] = 0

    df_operations['ASLFlown'] = 0
    df_operations['ASLTakeoff'] = 0
    df_operations['ASLEndTakeoff'] = 0
    df_operations['ASLStartLanding'] = 0
    df_operations['ASLLanded'] = 0
    df_operations['ASLTotalDuration'] = 0
    df_operations['ASLUSDuration'] = 0
    df_operations['APUSLaunchVariation'] = 0

    df_operations['USMessages'] = 0


    df_flights = pd.read_csv(flight_folder + flight_file, index_col=False, header=None, sep=",", skipinitialspace=True, names=df_columns_flight)

    for index, row in df_flights.iterrows():
        print("Trying to inject data in flight: '" + row["Callsign"] + "'\n")

        idx = df_operations.loc[df_operations['Callsign'].isin([row['Callsign']])].index

        if len(idx) > 0:
            print("Flight " + row["Callsign"] + " found at index: " + str(idx[0]) + "\n")

            targetRow = df_operations.iloc[idx[0]]
            df_operations.loc[idx[0], 'APFlown'] = 1
            if row['APTakeoff'] != -1 and row['APEndTakeoff'] != -1:
                df_operations.loc[idx[0], 'APTakeoff'] = row['APActivate'] + row['APTakeoff'] + 3600
                df_operations.loc[idx[0], 'APEndTakeoff'] = row['APActivate'] + row['APEndTakeoff'] + 3600
            if row['APStartLanding'] != -1 and row['APLanded'] != -1:
                df_operations.loc[idx[0], 'APStartLanding'] = row['APActivate'] + row['APStartLanding'] + 3600
                df_operations.loc[idx[0], 'APLanded'] = row['APActivate'] + row['APLanded'] + 3600

            df_operations.loc[idx[0], 'APTotalDuration'] = row['APLanded'] - row['APTakeoff']
            df_operations.loc[idx[0], 'APUSDuration'] = row['APStartLanding'] - row['APEndTakeoff']
            df_operations.loc[idx[0], 'APUSLaunchVariation'] = row['APActivate'] + row['APTakeoff'] + 3600 - df_operations.loc[idx[0], "TimeActivation"]

    df_operations['EstimatedDuration'] = df_operations["TimeCompletion"] - df_operations["TimeActivation"]
    df_operations['EstimationVariation'] = df_operations['EstimatedDuration'] - df_operations['APTotalDuration']


    df_aslogic = pd.read_csv(aslogic_folder + aslogic_file, index_col=False, header=None, sep=";", skipinitialspace=True, names=df_columns_flight2)

    for index, row in df_aslogic.iterrows():
        print("Injecting data in flight: " + row["OperationId"] + "\n")

        idx = df_operations.loc[df_operations['OperationId'].isin([row['OperationId']])].index

        if len(idx) > 0:
            print("Flight " + row["OperationId"] + " found at index: " + str(idx[0]) + "\n")

            targetRow = df_operations.iloc[idx[0]]
            df_operations.loc[idx[0], 'ASLFlown'] = 1
            if row['APTakeoff'] != -1 and row['APEndTakeoff'] != -1:
                df_operations.loc[idx[0], 'ASLTakeoff'] = row['APActivate'] + row['APTakeoff']
                df_operations.loc[idx[0], 'ASLEndTakeoff'] = row['APActivate'] + row['APEndTakeoff']
            else:
                df_operations.loc[idx[0], 'ASLFlown'] = 2
            if row['APStartLanding'] != -1 and row['APLanded'] != -1:
                df_operations.loc[idx[0], 'ASLStartLanding'] = row['APActivate'] + row['APStartLanding']
                df_operations.loc[idx[0], 'ASLLanded'] = row['APActivate'] + row['APLanded']
            else:
                df_operations.loc[idx[0], 'ASLFlown'] = 2

            if row['APTakeoff'] == -1 and row['APEndTakeoff'] == -1 and row['APStartLanding'] == -1 and row['APLanded'] == -1:
                df_operations.loc[idx[0], 'ASLFlown'] = 3

            if df_operations.loc[idx[0], 'ASLFlown'] == 1:
                df_operations.loc[idx[0], 'ASLTotalDuration'] = row['APActivate'] + row['APLanded'] - row['APTakeoff']
                df_operations.loc[idx[0], 'ASLUSDuration'] = row['APActivate'] + row['APStartLanding'] - row['APEndTakeoff']
                df_operations.loc[idx[0], 'ASLUSLaunchVariation'] = row['APActivate'] + row['APLanded'] - row['APTakeoff'] + 3600 - df_operations.loc[idx[0], "TimeActivation"]

            df_operations.loc[idx[0], 'USMessages'] = row['USMessages']

    df_operations.sort_values("TimeActivation", inplace=True)

    df_intervals = pd.read_csv(operation_folder + interval_file, index_col=False, header=None, sep=";", skipinitialspace=True, names=df_columns_intervals)

    print("Total number of scheduled operations: " + str(len(df_operations.index)))

    operations_active = df_operations.loc[df_operations["APFlown"] == 1]
    operations_active_8 = operations_active.loc[operations_active["OperationDate"] == "2022-03-08"]
    operations_active_9 = operations_active.loc[operations_active["OperationDate"] == "2022-03-09"]
    operations_active_10 = operations_active.loc[operations_active["OperationDate"] == "2022-03-10"]

    print("Number of flown operations: " + str(len(operations_active.index)))
    print("On the 8th: " + str(len(operations_active_8.index)))
    print("On the 9th: " + str(len(operations_active_9.index)))
    print("On the 10th: " + str(len(operations_active_10.index)))

    dt_obj = datetime.strptime('2022-03-08', '%Y-%m-%d')
    startRange = dt_obj.timestamp()

    dt_obj = datetime.strptime('2022-03-11', '%Y-%m-%d')
    endRange = dt_obj.timestamp()

    step = 30 * 60
    nIntervals = math.ceil((endRange - startRange) / step)

    rollWindow = np.zeros(nIntervals)

    i = 0
    while i < nIntervals:
        startInterval = startRange + i * step
        endInterval = startRange + (i + 1) * step

        for index, row in operations_active.iterrows():
            takeoff = row['APTakeoff']
            landed = row['APLanded']

            if takeoff >= startInterval and takeoff < endInterval:
                rollWindow[i] += 1

            elif landed >= startInterval and landed < endInterval:
                rollWindow[i] += 1

            elif takeoff < startInterval and landed > endInterval:
                rollWindow[i] += 1

        i += 1



    print("\nChecking intervals... ")

    for index, row in df_intervals.iterrows():
        print("Checking interval: " + row["Date"] + "   From: " + str(row["Start"]) + " -> To: " + str(row["End"]) + "  Duration: " + str((row["End"] - row["Start"])/60) + " min")

        inInterval = operations_active.loc[(operations_active['APTakeoff'] >= row['Start']) & (operations_active['APLanded'] <= row['End'])]

        df_intervals.loc[index, 'NumberOperations'] = len(inInterval.index)
        df_intervals.loc[index, 'Density'] = len(inInterval.index) / ((row["End"] - row["Start"])/3600)
        df_intervals.loc[index, 'TUsage'] = (inInterval['APLanded'] - inInterval['APTakeoff']).sum() / 60
        df_intervals.loc[index, 'TDensity'] = ((inInterval['APLanded'] - inInterval['APTakeoff']).sum())/60 / ((row["End"] - row["Start"]) / 3600)

        print("Number of operations: " + str(df_intervals.loc[index, 'NumberOperations']) + "   Density: " + str(df_intervals.loc[index, 'Density']) + " op/h")
        print("TUsage: " + str(df_intervals.loc[index, 'TUsage']) + " min   TDensity: " + str(df_intervals.loc[index, 'TDensity']) + " min/h")
        print("\n")


    operations_active_ontime = operations_active.loc[(operations_active["APUSLaunchVariation"] <= 60) & (operations_active["APUSLaunchVariation"] >= -60)]
    operations_active_wellontime = operations_active.loc[
        (operations_active["APUSLaunchVariation"] <= 40) & (operations_active["APUSLaunchVariation"] >= -40)]


    print("Total operations: " + str(len(operations_active.index)))
    print("Full U-space operations: " + str(len(df_operations.loc[df_operations["ASLFlown"] == 1].index)))
    print("Partial U-space operations: " + str(len(df_operations.loc[df_operations["ASLFlown"] == 2].index)))
    print("Operations on 08: " + str(len(operations_active_8.index)) + "  Operations on 09: " + str(len(operations_active_9.index)) + "  perations on 10: " + str(len(operations_active_10.index)))
    print("Total operations on time: " + str(len(operations_active_ontime.index)) + " Well on time: " + str(len(operations_active_wellontime.index)))
    print("Average departure variation time: " + str(abs(operations_active["APUSLaunchVariation"].mean())))
    print("Average flight time: " + str(operations_active["APTotalDuration"].mean()))
    print("Max flight time: " + str(operations_active["APTotalDuration"].max()))

    print("Tracking US Messages: " + str(operations_active["USMessages"].sum()))

    totalFlightTime = operations_active['APTotalDuration'].sum() / 60
    totalUSTime = operations_active['APUSDuration'].sum() / 60

    totalFlightTime_08 = operations_active_8['APTotalDuration'].sum() / 60
    totalFlightTime_09 = operations_active_9['APTotalDuration'].sum() / 60
    totalFlightTime_10 = operations_active_10['APTotalDuration'].sum() / 60
    print("Flight time (min): " + str(totalFlightTime) + " Time on 08: " + str(totalFlightTime_08) + "  Time on 09: " + str(totalFlightTime_09) + "  Time on 10: " + str(totalFlightTime_10))
    print("U-space time (min): " + str(totalUSTime))



    TotalOp = 0
    UserAssigned = 0
    UserRequested = 0
    UserExecuted = 0

    for index, row in df_operations.iterrows():
        #print("Analyzing operation: " + row["Callsign"] + "\n")

        TotalOp += 1

        if row["Status"] == "Created" and row["UPCDroneId"] != "":
            UserAssigned += 1
        elif row["Status"] == "Requested":
            UserRequested += 1
        elif row["Status"] == "Terminated" or row["Status"] == "Approved" or row["Status"] == "Cancelled":
            UserExecuted += 1

    print("Total op: " + str(TotalOp) + " Assigned op: " + str(UserAssigned) + " Requested op: " + str(UserRequested) + " Executed op: " + str(UserExecuted))

    df_operations.to_csv(operation_folder + "SCHOP-ANALYSIS-ALL-2022-03-09-10.csv", index=False, header=None, sep=";")
    operations_active.to_csv(operation_folder + "SCHOP-ANALYSIS-ACTIVE-2022-03-09-10.csv", index=False, sep=";")



    fig = generate_individual_histogram(operations_active['APTotalDuration'], 100, 600, color1, 'Actual Operation Duration (sec)', 10)
    filename = operation_folder + "Operation-Duration.html"
    print("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = operation_folder + "Operation-Duration.png"
    fig.write_image(filename)


    fig = generate_individual_histogram(operations_active['EstimatedDuration'], 100, 700, color1, 'Estimated Operation Duration (sec)', 10)
    filename = operation_folder + "Estimated-Duration.html"
    print("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = operation_folder + "Estimated-Duration.png"
    fig.write_image(filename)


    fig = generate_individual_histogram(operations_active['APUSLaunchVariation'], -60, 80, color1, 'Departure Variation (sec)', 5)
    filename = operation_folder + "Departure-Variation.html"
    print("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = operation_folder + "Departure-Variation.png"
    fig.write_image(filename)


    fig = generate_individual_histogram(operations_active['EstimationVariation'], -100, 180, color1, 'Operation Estimation Error (sec))', 5)
    filename = operation_folder + "EstimationError-Variation.html"
    print("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = operation_folder + "EstimationError-Variation.png"
    fig.write_image(filename)

    operations_active_UPC_10 = operations_active.loc[operations_active["UPCDroneId"] == "ICARUS-UPC-010"]
    operations_active_UPC_11 = operations_active.loc[operations_active["UPCDroneId"] == "ICARUS-UPC-011"]

    print("\tOperations for: ICARUS-UPC-010 -> " + str(len(operations_active_UPC_10.index)) + "\n")
    print("\tOperations for: ICARUS-UPC-011 -> " + str(len(operations_active_UPC_11.index)) + "\n")


    fig = generate_individual_histogram(operations_active_UPC_10['EstimationVariation'], -100, 180, color1, 'Estimation Variation UPC-10', 5)
    filename = operation_folder + "Estimation-Variation-UPC-10.html"
    print("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = operation_folder + "Estimation-Variation-UPC-10.png"
    fig.write_image(filename)

    fig = generate_individual_histogram(operations_active_UPC_11['EstimationVariation'], -100, 180, color1, 'Estimation Variation UPC-11', 5)
    filename = operation_folder + "Estimation-Variation-UPC-11.html"
    print("\tGenerating figure: " + filename + "\n")
    fig.write_html(filename)
    filename = operation_folder + "Estimation-Variation-UPC-11.png"
    fig.write_image(filename)

    print("Done.\n")
    #sys.exit(0)

