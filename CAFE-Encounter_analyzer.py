# This is a sample Python script.
import os, shutil
import sys
from time import time
from datetime import datetime
import pandas as pd
import numpy as np
import argparse
from Encounter import position as pos, encounter as enc, encounter_task as et
from Encounter_Distribution import Encounter_Parameters as ep
from Encounter import aircraft_class as cls
from Metrics import Enc_Alert_Measuring as mea, Alert_Measuring as am
from Projection import FlatEarthProjection as proj

#df_dtype = {
#        "Encounter_Distribution No.": int,
#        "Seed": int,
#        "Layer": int,
#        "VMD": np.float64,
#        "HMD": np.float64,
#        "Approach Angle": np.float64,
#        "Bearing from North": np.float64,
#        "Time before CPA": int,
#        "Time after CPA": int,
#        "AC1 Mode A": str,
#        "AC1 Callsign": str,
#        "AC1 Class": int,
#        "AC1 Controlled?": bool,
#        "AC1 Altitude": np.float64,
#        "AC1 Vertical": np.float64,
#        "AC1 Speed": np.float64,
#        "AC1 Acceleration": np.float64,
#        "AC1 Heading": np.float64,
#        "AC1 Turn": np.float64,
#        "AC2 Mode A": str,
#        "AC2 Callsign": str,
#        "AC2 Class": int,
#        "AC2 Controlled?": bool,
#        "AC2 Altitude": np.float64,
#        "AC2 Vertical": np.float64,
#        "AC2 Speed": np.float64,
#        "AC2 Acceleration": np.float64,
#        "AC2 Heading": np.float64,
#        "AC2 Turn": np.float64,
#}

df_dtype = {
        "Encounter No.": int,
        "Seed": int,
        "Layer": int,
        "VMD": np.float64,
        "HMD": np.float64,
        "Approach Angle": np.float64,
        "Bearing from North": np.float64,
        "Time before CPA": int,
        "Time after CPA": int,
        "AC1 Mode A": str,
        "AC1 Callsign": str,
        "AC1 Class": int,
        "AC1 Controlled?": bool,
        "AC1 Altitude": np.float64,
        "AC1 Vertical": np.float64,
        "AC1 Speed": np.float64,
        "AC1 Acceleration": np.float64,
        "AC1 Heading": np.float64,
        "AC1 Turn": np.float64,
        "AC2 Mode A": str,
        "AC2 Callsign": str,
        "AC2 Class": int,
        "AC2 Controlled?": bool,
        "AC2 Altitude": np.float64,
        "AC2 Vertical": np.float64,
        "AC2 Speed": np.float64,
        "AC2 Acceleration": np.float64,
        "AC2 Heading": np.float64,
        "AC2 Turn": np.float64,
}

trj_eu_dtype = {
        "time": str,
        "aircraft":  np.int32,
        "Mode A": np.int32,
        "x-position": np.float64,
        "y-position": np.float64,
        "altitude": np.float64,
        "callsign": str,
}


trj_ftd_dtype = {
        "time": str,
        "aircraft":  np.int32,
        "x-position": np.float64,
        "y-position": np.float64,
        "altitude": np.float64,
        "callsign": str,
}


trj_eu_columns = [
        "time",
        "aircraft",
        "Mode A",
        "x-position",
        "y-position",
        "altitude",
        "callsign",
]


trj_ftd_columns = [
        "time",
        "aircraft",
        "x-position",
        "y-position",
        "altitude",
        "BDS-30",
]


df_columns = [
        "Encounter No.",
        "Seed",
        "Layer",
        "VMD",
        "HMD",
        "Approach Angle",
        "Bearing from North",
        "Time before CPA",
        "Time after CPA",
        "AC1 Mode A",
        "AC1 Callsign",
        "AC1 Class",
        "AC1 Controlled?",
        "AC1 Altitude",
        "AC1 Vertical",
        "AC1 Speed",
        "AC1 Acceleration",
        "AC1 Heading",
        "AC1 Turn",
        "AC2 Mode A",
        "AC2 Callsign",
        "AC2 Class",
        "AC2 Controlled?",
        "AC2 Altitude",
        "AC2 Vertical",
        "AC2 Speed",
        "AC2 Acceleration",
        "AC2 Heading",
        "AC2 Turn"
]

def processTrajectoryFile(full_encounter_path, full_diagram_path, enc_type, encounter, encounter_param):

    print("Reading a trajectory...")

    if encounter.enc_format == "eu":
        trj_df = pd.read_csv(full_encounter_path, delimiter='\t', names=trj_eu_columns, dtype=trj_eu_dtype)

        for index, row in trj_df.iterrows():
            if row['aircraft'] == 1:
                position = pos.Position(row['aircraft'], row['time'], row['x-position'], row['y-position'], row['altitude'])
                encounter.addAircraft1Position(position)
            elif row['aircraft'] == 2:
                position = pos.Position(row['aircraft'], row['time'], row['x-position'], row['y-position'], row['altitude'])
                encounter.addAircraft2Position(position)


    if encounter.enc_format == "ftd":
        skip = 0
        trj_df = pd.read_csv(full_encounter_path, header=None, sep='\n', engine='python')

        print("Splitting columns...")
        trj_df = trj_df[0].str.split('\s*,\s*', expand=True)

        # Assign column names
        # df.columns = df_columns

        val = trj_df[0][skip]
        if trj_df[0][skip] != "HEADER":
            return

        skip += 1
        if trj_df[0][skip] != "synthetic":
            return

        encounter.nAircraft = int(trj_df[1][skip])
        skip += encounter.nAircraft

        skip += 1
        v = trj_df[0][skip]
        if trj_df[0][skip] != "BODY":
            return

        skip += 1

        print("Renaming columns...")
        i = 0
        while i < len(trj_ftd_columns):
            trj_df.rename(columns={i: trj_ftd_columns[i]}, inplace=True)
            i += 1

        for index, row in trj_df.iterrows():
            if index < skip:
                continue

            if int(row['aircraft']) == 1:
                position = pos.Position(int(row['aircraft']), row['time'], float(row['x-position']), float(row['y-position']), float(row['altitude']))
                encounter.addAircraft1Position(position)
            elif int(row['aircraft']) == 2:
                position = pos.Position(int(row['aircraft']), row['time'], float(row['x-position']), float(row['y-position']), float(row['altitude']))
                encounter.addAircraft2Position(position)

            if int(row['aircraft']) == 1:
                aircraftVector = np.array([None] * encounter.nAircraft)

            position = pos.Position(int(row['aircraft']), row['time'], float(row['x-position']), float(row['y-position']), float(row['altitude']))
            aircraftVector[int(row['aircraft']) - 1] = position

            if int(row['aircraft']) == encounter.nAircraft:
                encounter.addAircraftVectorPosition(aircraftVector)

    if not computeConflicts:
        return None

    if (encounter.numberAircraft1Position() == encounter.numberAircraft2Position()):

        encounter.encAlertMeasuringAicraft1 = arcf1_eam = mea.Enc_Alert_Measuring()
        encounter.encAlertMeasuringAicraft2 = arcf2_eam = mea.Enc_Alert_Measuring()

        encounter.computeDynamics()
        encounter.computeConflictsCAFE(full_diagram_path, encounter_param, arcf1_eam, arcf2_eam)

        if encounter_param.debug and arcf1_eam.active_conflict:
            print("Reporting on aircraft 1:")
            arcf1_eam.report_Alert_Measuring()
        if encounter_param.debug and arcf2_eam.active_conflict:
            print("\nReporting on aircraft 2:")
            arcf2_eam.report_Alert_Measuring()

        if encounter_param.mustPlot and ((arcf1_eam.active_conflict or arcf2_eam.active_conflict) or (arcf1_eam.haz_lmv_violated or arcf2_eam.haz_lmv_violated or arcf1_eam.haz_violated or arcf2_eam.haz_violated or arcf1_eam.nmac_violated or arcf2_eam.nmac_violated)):
            encounter.plotEncounterCAFE(full_diagram_path)

        print("Done.")
        return None
    else:
        print("Trajectory seems NOT consistent.")
        return None



def processEncounterSet(encTask, encounter_param):

    subfolder = 1
    while (subfolder <= MAX_ENCOUNTER_FOLDERS):

        if encTask.enc_format == "eu":
            full_encounter_path = encTask.task_enc_folder + "\\" + "EU1_" + str(subfolder) + "\\" + "Encs_" + str(encTask.enc_number).zfill(9) + ".eu1"
        if encTask.enc_format == "ftd":
            full_encounter_path = encTask.task_enc_folder + "\\" + "FTD_" + str(subfolder) + "\\" + "Encs_" + str(encTask.enc_number).zfill(9) + ".ftd"

        print("Checking Encounter_Distribution file: ", full_encounter_path)

        if os.path.exists(full_encounter_path):
            found = True
            start = time()
            print("Encounter_Distribution file: ", full_encounter_path, "  Identified, processing...")
            encounter = enc.Encounter(encTask.enc_number, encTask.enc_layer, encTask.enc_type, encTask.aircraft1Class, encTask.aircraft2Class, encTask.enc_format)
            encounter.aircraft1Callsign = encTask.aircraft1Callsign
            encounter.aircraft2Callsign = encTask.aircraft2Callsign

            processTrajectoryFile(full_encounter_path, encTask.diagram_folder, enc_type, encounter, encounter_param)
            duration = (time() - start)
            print("Encounter_Distribution processed in: ", "{:.2f}".format(duration) , " sec")
            return encounter

        subfolder += 1

    print("Encounter_Distribution number: ", enc_number, " cannot be found")

    return None


# Main function.
if __name__ == '__main__':

    parser = argparse.ArgumentParser(prog='CAFE_Encounter_analyzer',
                                        usage='%(prog)s [options]',
                                        description='Process CAFE collision encounter subsets')

    parser.add_argument('-i', '--intruder',
                        type=str,
                        choices=['Other', 'RPAS'],
                        default='Other', required=False,
                        help='Intruder type: [Other|RPAS]')

    parser.add_argument('-is', '--intSubset',
                        type=str,
                        choices=['nonCoop', 'modeS', 'ACAS'],
                        default='nonCoop', required=False,
                        help='Intruder subset: [nonCoop|modeS|ACAS]')

    parser.add_argument('-r', '--rpas',
                        type=str,
                        choices=['URCLEARED', 'ACAS'],
                        default='URCLEARED', required=False,
                        help='Ownship type: [URCLEARED|ACAS]')

    parser.add_argument('-a', '--altitude',
                        type=str,
                        choices=['LOWER', 'HIGHER'],
                        default='LOWER', required=False,
                        help='Altitude subset with associated speed limitation: [LOWER|HIGHER]')

    parser.add_argument('-os', '--ownspeed',
                        type=str,
                        choices=['NONE', 'BELOW100', 'ABOVE100'],
                        default='NONE', required=False,
                        help='Speed of ownship : [NONE|BELOW100|ABOVE100]')

    parser.add_argument('-wcv', '--wcvSet',
                        type=str,
                        choices=['05NM', '22KFT', 'URCV', 'SESAR', 'DO365', '05NMShort', '22KFTShort', 'URCVShort'],
                        default='05NM', required=False,
                        help='WCV volume definition: [05NM|22KFT|URCV|SESAR|DO365|05NMShort|22KFTShort|URCVShort]')

    parser.add_argument('-s', '--subset',
                        type=str,
                        default='CAFE-Analysis', required=False,
                        help='Subset folder name for results')

    parser.add_argument('-rev', '--revision',
                        type=str,
                        choices=['REV6', 'REV7'],
                        default='REV6', required=False,
                        help='CAFE revision: [REV6|REV7]')

    parser.add_argument('-bf', '--baseFolder',
                        type=str,
                        choices=['Laptop', 'Voyager'],
                        default='Laptop', required=False,
                        help='Base folder for execution.')

    parser.add_argument('-acas', '--acas', action='store_true')

    parser.add_argument('-m', '--mops', action='store_true')

    parser.add_argument('-d', '--debug', action='store_true')

    parser.add_argument('-p', '--plot', action='store_true')

    parser.add_argument('-nconf', '--nconf', action='store_true')


    # Read arguments from the command line
    args = parser.parse_args()


    MAX_ENCOUNTER_FOLDERS = 2
    ENCOUNTER_TYPE = ["EE", "EU", "UU"]
    # CAFE format eu or ftd
    ENCOUNTER_FORMAT = "ftd"
    found = 0
    notFound = 0
    start = time()

    # Options: Select a certain max number of encounters to debug
    maxProcess = 0
    # maxProcess = 10

    Prefix_Name = "RPAS"

    if args.subset:
        Postfix_Name = args.subset
    else:
        Postfix_Name = "CAFE-Analysis"

    # Options: "Other" / "RPAS"
    Intruder_Type = "Other"
    if args.intruder:
        Intruder_Type = args.intruder

    # Options: "nonCoop" / "modeS" / "ACAS"
    Intruder_Subset = "nonCoop"
    if args.intSubset:
        Intruder_Subset = args.intSubset

    # Options: "URCLEARED" / "ACAS"
    RPAS_Subset = "URCLEARED"
    if args.rpas:
        RPAS_Subset = args.rpas

    # Options: "LOWER" / "HIGHER"
    Altitude_Subset = "LOWER"

    # Options: "SPEEDLIMIT" / "NONE"
    Speed_Subset = "SPEEDLIMIT"

    if args.ownspeed and args.ownspeed == "ABOVE100":
        Ownspeed_Subset = "ABOVE100"
        Speed_Subset = "NONE"
        Altitude_Subset = "NONE"
    elif args.ownspeed and args.ownspeed == "BELOW100":
        Ownspeed_Subset = "BELOW100"
        Speed_Subset = "NONE"
        Altitude_Subset = "LOWER"
    elif args.altitude and args.altitude == 'HIGHER':
        Ownspeed_Subset = "NONE"
        Speed_Subset = "NONE"
        Altitude_Subset = "HIGHER"
    elif args.altitude and args.altitude == 'LOWER':
        Ownspeed_Subset = "NONE"
        Speed_Subset = "SPEEDLIMIT"
        Altitude_Subset = "LOWER"

    # Options: "05NM" / "22KFT" / "URCV"
    WCV_Subset = "05NM"
    if args.wcvSet:
        WCV_Subset = args.wcvSet

    # Options: "REV6" / "REV7"
    CAFE_Revision = "REV7"
    if args.revision:
        CAFE_Revision = args.revision


    layer_folder_template = "Layer_%s_%s_100k\\"

    # Options: Select a certain encounter in order to debug
    select_enc_number = None
    #select_enc_number = 174638

    # Options: export in MOPS format
    writeMOPS = False
    if args.mops:
        writeMOPS = True

    storeACAS = False
    if args.acas:
        storeACAS = True

    computeConflicts = True
    if args.nconf:
        computeConflicts = False


    #resume_file = Prefix_Name + ("_%s_vs_%s_at_%s_%s_Resume_Enc_Params.csv" % (RPAS_Subset, Intruder_Subset, Altitude_Subset, Speed_Subset))
    #result_file = Postfix_Name + ("_%s_vs_%s_at_%s_%s_%s_Results.csv" % (RPAS_Subset, Intruder_Subset, Altitude_Subset, Speed_Subset, WCV_Subset))
    #prov_file = Postfix_Name + ("_%s_vs_%s_at_%s_%s_%s_Results.txt" % (RPAS_Subset, Intruder_Subset, Altitude_Subset, Speed_Subset, WCV_Subset))
    #diagram_subfolder = Postfix_Name + ("_%s_vs_%s_at_%s_%s_%s_Diagrams" % (RPAS_Subset, Intruder_Subset, Altitude_Subset, Speed_Subset, WCV_Subset))
    #MOPS_subfolder = Postfix_Name + ("_%s_vs_%s_at_%s_%s_%s_MOPS" % (RPAS_Subset, Intruder_Subset, Altitude_Subset, Speed_Subset, WCV_Subset))

    if Ownspeed_Subset == "NONE":
        resume_file = Prefix_Name + ("_%s_vs_%s_at_%s_%s_Resume_Enc_Params.csv" % (RPAS_Subset, Intruder_Subset, Altitude_Subset, Speed_Subset))
        result_file = Postfix_Name + ("_%s_vs_%s_at_%s_%s_%s_Results.csv" % (RPAS_Subset, Intruder_Subset, Altitude_Subset, Speed_Subset, WCV_Subset))
        acas_result_file = Postfix_Name + ("_%s_vs_%s_at_%s_%s_%s_ACAS_Results.csv" % (RPAS_Subset, Intruder_Subset, Altitude_Subset, Speed_Subset, WCV_Subset))
        prov_file = Postfix_Name + ("_%s_vs_%s_at_%s_%s_%s_Results.txt" % (RPAS_Subset, Intruder_Subset, Altitude_Subset, Speed_Subset, WCV_Subset))
        diagram_subfolder = Postfix_Name + ("_%s_vs_%s_at_%s_%s_%s_Diagrams" % (RPAS_Subset, Intruder_Subset, Altitude_Subset, Speed_Subset, WCV_Subset))
        MOPS_subfolder = Postfix_Name + ("_%s_vs_%s_at_%s_%s_%s_MOPS" % (RPAS_Subset, Intruder_Subset, Altitude_Subset, Speed_Subset, WCV_Subset))
    else:
        resume_file = Prefix_Name + ("_%s_vs_%s_at_%s_%s_Resume_Enc_Params.csv" % (RPAS_Subset, Intruder_Subset, Altitude_Subset, Ownspeed_Subset))
        result_file = Postfix_Name + ("_%s_vs_%s_at_%s_%s_%s_Results.csv" % (RPAS_Subset, Intruder_Subset, Altitude_Subset, Ownspeed_Subset, WCV_Subset))
        acas_result_file = Postfix_Name + ("_%s_vs_%s_at_%s_%s_%s_ACAS_Results.csv" % (RPAS_Subset, Intruder_Subset, Altitude_Subset, Ownspeed_Subset, WCV_Subset))
        prov_file = Postfix_Name + ("_%s_vs_%s_at_%s_%s_%s_Results.txt" % (RPAS_Subset, Intruder_Subset, Altitude_Subset, Ownspeed_Subset, WCV_Subset))
        diagram_subfolder = Postfix_Name + ("_%s_vs_%s_at_%s_%s_%s_Diagrams" % (RPAS_Subset, Intruder_Subset, Altitude_Subset, Ownspeed_Subset, WCV_Subset))
        MOPS_subfolder = Postfix_Name + ("_%s_vs_%s_at_%s_%s_%s_MOPS" % (RPAS_Subset, Intruder_Subset, Altitude_Subset, Ownspeed_Subset, WCV_Subset))


    if Ownspeed_Subset == "NONE":
        print("Analyzing " + Prefix_Name + (" %s  vs  %s  at  %s  %s  %s encounters..." % (RPAS_Subset, Intruder_Subset, Altitude_Subset, Speed_Subset, WCV_Subset)))
    else:
        print("Analyzing " + Prefix_Name + (" %s  vs  %s  at  %s  %s  %s encounters..." % (RPAS_Subset, Intruder_Subset, Altitude_Subset, Ownspeed_Subset, WCV_Subset)))


    # resume_enc_folder = "E:\Encounter-RPAS-Encounters\\RPAS_safety_generated_encounters-REV0\\RPAS_v_Non_RPAS\\"
    #resume_enc_folder = "E:\CAFE-Encounters\\URClearED - EGT 1k Test Encounters 11_6_2021\\"

    laptop_base_folder = "E:\CAFE-Encounters\\"
    voyager_base_folder = "D:\\URCLEARED\\CAFE-Encounters\\"

    if args.baseFolder and args.baseFolder == 'Laptop':
        base_folder = laptop_base_folder
    if args.baseFolder and args.baseFolder == 'Voyager':
        base_folder = voyager_base_folder


    enc_folder_template = "RPAS_Generated_Encounters-%s\\RPAS_%s\\"


    resume_enc_file = base_folder + enc_folder_template % (CAFE_Revision, Intruder_Type) + resume_file
    resume_enc_folder = base_folder + enc_folder_template % (CAFE_Revision, Intruder_Type) + "Analysis"
    result_enc_filename = base_folder + enc_folder_template % (CAFE_Revision, Intruder_Type) + "Analysis\\" + result_file
    acasResult_enc_filename = base_folder + enc_folder_template % (CAFE_Revision, Intruder_Type) + "Analysis\\" + acas_result_file

    print("Analyzing encounters selected in resume encounter file: \n\t\t", resume_enc_file, "\n")

    if not os.path.exists(resume_enc_file):
        print("Encounter_Distribution file: ", resume_enc_file, "\n")
        print("Does not exists, stopping...\n")
        exit(-1)


    if os.path.exists(resume_enc_folder + "\\" + diagram_subfolder):
        print("Renaming folder...\n")
        if os.path.exists(resume_enc_folder + "\\" + diagram_subfolder + ".bak"):
            shutil.rmtree(resume_enc_folder + "\\" + diagram_subfolder + ".bak")
            # os.rmdir(resume_enc_folder + "\\" + diagram_subfolder + ".bak")
        os.rename(resume_enc_folder + "\\" + diagram_subfolder, resume_enc_folder + "\\" + diagram_subfolder + ".bak")
        os.mkdir(resume_enc_folder + "\\" + diagram_subfolder)
    else:
        os.mkdir(resume_enc_folder + "\\" + diagram_subfolder)


    if os.path.exists(resume_enc_folder + "\\" + MOPS_subfolder):
        print("Renaming folder...\n")
        if os.path.exists(resume_enc_folder + "\\" + MOPS_subfolder + ".bak"):
            shutil.rmtree(resume_enc_folder + "\\" + MOPS_subfolder + ".bak")
            # os.rmdir(resume_enc_folder + "\\" + diagram_subfolder + ".bak")
        os.rename(resume_enc_folder + "\\" + MOPS_subfolder, resume_enc_folder + "\\" + MOPS_subfolder + ".bak")
        os.mkdir(resume_enc_folder + "\\" + MOPS_subfolder)
    else:
        os.mkdir(resume_enc_folder + "\\" + MOPS_subfolder)


    # Options: set the encounter parameters
    print("Setting encounter parameters to DO365A")
    encounter_param = ep.Encounter_Parameters()
    encounter_param.set_URCLEARED_parameters(False, WCV_Subset)
    encounter_param.set_Separation_parameters()

    # print("Setting standard TCASII parameters")
    encounter_param.set_TCASII_parameters()

    # Options: plot the encounters
    encounter_param.mustPlot = False
    if args.plot:
        encounter_param.mustPlot = True
    # Options: activate debug
    encounter_param.debug = False
    if args.debug:
        encounter_param.debug = True
    # Options: plot contours for the conflict sequence
    encounter_param.plotContour = False


    print("Generating encounter results on file: \n\t\t", result_enc_filename, "\n")
    encounterResume = am.Alert_Measuring()

    main_enc_folder = None
    enc_folder = None
    enc_type = None
    enc_file = None

    df_chunk = pd.read_csv(resume_enc_file, header=None, sep='\n', chunksize=10000, engine='python')

    for chunk in df_chunk:

        # Limit the process to a certain number of encounters
        # Useful for debugging purposes
        if maxProcess > 0 and found == maxProcess:
            break

        print("\nProcessing new chunk...")
        print("Splitting columns...")
        df = chunk[0].str.split('\s*,\s*', expand=True)

        # Assign column names
        # df.columns = df_columns

        print("Renaming columns...")
        i = 0
        while i < len(df_columns):
            df.rename(columns={i: df_columns[i]}, inplace=True)
            i += 1

        print("Performing analysis...\n")

        for index, row in df.iterrows():

            # Limit the process to a certain number of encounters
            # Useful for debugging purposes
            if maxProcess > 0 and found == maxProcess:
                break

            if row['Encounter No.'] == 'ROOT_FOLDER':
                main_enc_folder = base_folder + row["Seed"]
                print("Root folder directive identified for: ", main_enc_folder, "\n")

                if not os.path.exists(main_enc_folder):
                    print("Root folder: ", main_enc_folder, "\n")
                    print("Does not exists, stopping...\n")
                    exit(-1)

                result_enc_file_header = True
                result_enc_file = open(result_enc_filename, "w")

                if storeACAS:
                    acasResult_enc_file = open(acasResult_enc_filename, "w")
                else:
                    acasResult_enc_file = None

                #result_enc_file.write('ROOT_FOLDER; ' + main_enc_folder + "\n")
                continue

            if row['Encounter No.'] == 'ENCOUNTER_FOLDER':
                enc_folder = row["Seed"]
                enc_type = row["Layer"]
                enc_layer = row["VMD"]
                print("Encounter_Distribution folder directive identified for: ", enc_folder, " of type: ", enc_type, " at layer: ", enc_layer, "\n")

                if not os.path.exists(main_enc_folder + enc_folder):
                    print("Encounter_Distribution folder: ", main_enc_folder + enc_folder)
                    print("Does not exists, skipping the associated encounters...\n")
                    enc_folder = None
                    continue

                if not os.path.exists(main_enc_folder + enc_folder + enc_type):
                    print("Encounter_Distribution folder: ", main_enc_folder + enc_folder + enc_type)
                    print("Does not exists, skipping the associated encounters...\n")
                    enc_folder = None
                    continue

                #result_enc_file.write('ENCOUNTER_FOLDER; ' + enc_folder + '; ' + enc_type + "\n")

                task_enc_folder = main_enc_folder + enc_folder + "\\" + enc_type
                diagram_partial_folder = resume_enc_folder + "\\" + diagram_subfolder + "\\" + enc_folder
                diagram_folder = resume_enc_folder + "\\" + diagram_subfolder + "\\"  + enc_folder + "\\" + enc_type

                if writeMOPS:
                    if os.path.exists(resume_enc_folder + "\\" + MOPS_subfolder + "\\"  + enc_folder + "\\" + enc_type):
                        shutil.rmtree(resume_enc_folder + "\\" + MOPS_subfolder + "\\"  + enc_folder + "\\" + enc_type)
                    #os.mkdir(resume_enc_folder + "\\" + MOPS_subfolder + "\\" + enc_folder)
                    os.makedirs(resume_enc_folder + "\\" + MOPS_subfolder + "\\"  + enc_folder + "\\" + enc_type)
                    projection = proj.FlatEarthProjection(1.9850292, 41.2755222, 0)

                if not encounter_param.mustPlot:
                    continue

                if not os.path.exists(diagram_partial_folder):
                    os.mkdir(diagram_partial_folder)

                if not os.path.exists(diagram_folder):
                    os.mkdir(diagram_folder)

                #if os.path.exists(diagram_folder):
                #    print("Renaming folder...\n")
                #    if os.path.exists(diagram_folder + ".bak"):
                #        shutil.rmtree(diagram_folder + ".bak")
                #        #os.rmdir(resume_enc_folder + "\\" + diagram_subfolder + ".bak")
                #    os.rename(diagram_folder, diagram_folder + ".bak")
                #    os.mkdir(diagram_folder)
                #else:
                #    os.mkdir(diagram_partial_folder)
                #    os.mkdir(diagram_folder)

                continue

            #
            # Now, we should get an encounter line
            # We need to check is the associated file exists before starting any process

            enc_number = row['Encounter No.']
            aircraft1Class = row['AC1 Class']
            aircraft2Class = row['AC2 Class']
            aircraft1Callsign = row['AC1 Callsign']
            aircraft2Callsign = row['AC2 Callsign']

            if enc_folder == None:
                continue

            #
            # Filter configured to be able to debug a certain trajectory
            if (select_enc_number != None) and (select_enc_number != int(enc_number)):
                continue

            print("\nProcessing encounter number: ", enc_number, " with Class1 aircraft: ", aircraft1Class, " with Class2 aircraft: ", aircraft2Class)
            enc_task = et.encounter_task(task_enc_folder, diagram_folder, enc_layer, enc_type, enc_number, aircraft1Class, aircraft2Class, ENCOUNTER_FORMAT)
            enc_task.aircraft1Callsign = aircraft1Callsign
            enc_task.aircraft2Callsign = aircraft2Callsign

            encounter = processEncounterSet(enc_task, encounter_param)

            if encounter != None:

                if computeConflicts:

                    if result_enc_file_header == True:
                        encounter.writeEncounterHeader(result_enc_file)
                        if storeACAS:
                            encounter.writeEncounterHeader(acasResult_enc_file)
                        result_enc_file_header = False

                    if not storeACAS:
                        encounter.writeEncounterResult(row, result_enc_file)
                    else:
                        encounter.writeACASEncounterResult(row, result_enc_file, acasResult_enc_file)

                    encounterResume.addEncounter(encounter.aircraft1Class, encounter.aircraft2Class)
                    encounterResume.addConflict(encounter.encAlertMeasuringAicraft1.active_conflict, encounter.encAlertMeasuringAicraft2.active_conflict)
                    encounterResume.addNMAC(encounter.encAlertMeasuringAicraft1.nmac_violated, encounter.encAlertMeasuringAicraft2.nmac_violated)
                    encounterResume.addLMV(encounter.encAlertMeasuringAicraft1.lmv_activated, encounter.encAlertMeasuringAicraft1.lmv_violated,
                                            encounter.encAlertMeasuringAicraft2.lmv_activated, encounter.encAlertMeasuringAicraft2.lmv_violated)
                    encounterResume.addLMVnoNMAC(encounter.encAlertMeasuringAicraft1.lmv_activated and not encounter.encAlertMeasuringAicraft1.nmac_violated, encounter.encAlertMeasuringAicraft1.lmv_violated and not encounter.encAlertMeasuringAicraft1.nmac_violated,
                                            encounter.encAlertMeasuringAicraft2.lmv_activated and not encounter.encAlertMeasuringAicraft2.nmac_violated, encounter.encAlertMeasuringAicraft2.lmv_violated and not encounter.encAlertMeasuringAicraft1.nmac_violated)

                    encounterResume.addSeparation(encounter.encAlertMeasuringAicraft1.sep_violated, encounter.encAlertMeasuringAicraft1.sep_relevant, encounter.encAlertMeasuringAicraft2.sep_violated, encounter.encAlertMeasuringAicraft2.sep_relevant)
                    encounterResume.addHazard(encounter.encAlertMeasuringAicraft1.haz_violated, encounter.encAlertMeasuringAicraft2.haz_violated)
                    encounterResume.addWarning(encounter.encAlertMeasuringAicraft1.warn_type, encounter.encAlertMeasuringAicraft2.warn_type)
                    encounterResume.addCorrective(encounter.encAlertMeasuringAicraft1.caution_type, encounter.encAlertMeasuringAicraft2.caution_type)
                    encounterResume.addPreventive(encounter.encAlertMeasuringAicraft1.prev_type, encounter.encAlertMeasuringAicraft2.prev_type)

                    if cls.isOwnshipRPAS(encounterResume.aircraft1Class):
                        encounterResume.addTimming(encounter, encounter.encAlertMeasuringAicraft1)

                    if cls.isOwnshipRPAS(encounterResume.aircraft2Class):
                        encounterResume.addTimming(encounter, encounter.encAlertMeasuringAicraft2)

                found += 1

                if writeMOPS and (encounter.encAlertMeasuringAicraft1.active_conflict or encounter.encAlertMeasuringAicraft1.active_conflict):
                    encounter.writeMOPSFormat(resume_enc_folder + "\\" + MOPS_subfolder + "\\"  + enc_folder + "\\" + enc_type, projection)

                duration = (time() - start)
                print("Total encounters processed: ", found, " in: ", "{:.2f}".format(duration), " sec")
            else:
                notFound += 1

    result_enc_file.close()
    print("Done.\n")

    duration = (time() - start)
    encounterResume.writeReport(resume_enc_folder + prov_file, duration)
    print("Found: " + str(found) + " Not found: " + str(notFound))
    sys.exit(0)
