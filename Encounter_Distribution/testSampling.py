from Encounter import units as units
from Encounter_Distribution import Encounter_Distribution_Parameters as ep
from Encounter_Distribution import Encounter_distribution as ed
from matplotlib import style
#style.use('ggplot')

enc_parameters = ep.Encounter_Distribution_Parameters()
enc_distribution = ed.Encounter_distribution(enc_parameters)

enc_distribution.recordAltitude(450)
enc_distribution.recordAltitude(550)
enc_distribution.recordAltitude(1400)
enc_distribution.recordAltitude(21000)

enc_distribution.plotAltitudeDistribution(".")

enc_distribution.recordHMD(units.ft_to_nm(350))
enc_distribution.recordHMD(units.ft_to_nm(1500))

enc_distribution.plotHMDDistribution(".")

enc_distribution.recordAAngle(0)
enc_distribution.recordAAngle(92)
enc_distribution.recordAAngle(-92)
enc_distribution.recordAAngle(170)
enc_distribution.recordAAngle(-170)
enc_distribution.recordAAngle(179)
enc_distribution.recordAAngle(-179)
enc_distribution.plotAAngleDistribution(".")

enc_distribution.recordVMDHMD(500, units.ft_to_nm(1500))
enc_distribution.plotVMDHMDDistribution(".")
