from Encounter import units as units, aircraft_class as cls
from RWC import RWC_volume as rwct, RWC_thresholds as rwcthr, HAZ_parameters as rwc, RWC_alert_times as at, \
  NMAC_parameters as nmac
from NCSensor import NCS_detection as ncsd, NCS_parameters as ncsp
from TCAS import TCAS_Table as tcast
from STCA import SEP_parameters as sep
import json


class Encounter_Parameters:

    def __init__(self):
        self.name = None
        self.version = None
        self.description = None

        self.debug = False
        self.mustPlot = False
        self.plotContour = False
        self.tcasii_actv = False
        self.threshold_actv = False
        self.preventive_actv = False
        self.caution_actv = False
        self.warning_actv = False
        self.wcv_param_preventive = None
        self.wcv_param_caution = None
        self.wcv_param_warning = None

        self.preventive_t = None
        self.caution_t = None
        self.warning_t = None
        self.nmac_param = None
        self.ncs_actv = False
        self.ncs_param = None
        self.TCAS_Table = None
        self.track_step = units.deg_to_rad(1.0)
        self.look_ahead_time = 200
        self.separation_actv = False
        self.sep_param = None


    def set_TCASII_parameters(self):
        self.TCAS_Table = tcast.TCAS_Table()
        self.TCAS_Table.setDefaultRAThresholds(True)
        self.tcasii_actv = True


    def set_Separation_parameters(self):
        self.separation_actv = True
        self.sep_param = sep.SEP_parameters(1000, 5)

    def set_DEG_Separation_parameters(self, H, HMD):
        self.separation_actv = True
        self.sep_param = sep.SEP_parameters(0, 0)
        self.sep_param.setIMSParameters(H, HMD)

    def set_DO365A_parameters(self):

        self.preventive_actv = True
        self.caution_actv = True
        self.warning_actv = False

        # Set HAZ parameters
        self.wcv_param_preventive = rwc.HAZ_parameters(0, 0, 0, 0)
        self.wcv_param_preventive.setPreventiveDO365A()
        self.wcv_param_caution = rwc.HAZ_parameters(0, 0, 0, 0)
        self.wcv_param_caution.setCorrectiveDO365A()
        self.wcv_param_warning = rwc.HAZ_parameters(0, 0, 0, 0)
        self.wcv_param_warning.setWarningDO365A()

        # Set RWC time alerting parameters
        self.preventive_t = at.RWC_alert_times()
        self.preventive_t.setPreventiveDO365A()
        self.caution_t = at.RWC_alert_times()
        self.caution_t.setCautionDO365A()
        self.warning_t = at.RWC_alert_times()
        self.warning_t.setWarningDO365A()

        # Set NMAC parameters
        self.nmac_param = nmac.NMAC_parameters(0, 0)
        self.nmac_param.setStandard()

        # Set NCS parameters
        self.ncs_actv = False
        self.ncs_param = ncsp.NCS_parameters(0, 0, 0, 0, 0)


    def set_URCLEARED_parameters(self, activeTh, WCV_Subset):
        self.preventive_actv = True
        self.caution_actv = True
        self.warning_actv = False
        self.threshold_actv = True

        # Set default HAZ parameters
        self.wcv_param_preventive = rwc.HAZ_parameters(0, 0, 0, 0)
        self.wcv_param_preventive.setPreventiveDO365A()
        self.wcv_param_caution = rwc.HAZ_parameters(0, 0, 0, 0)
        self.wcv_param_caution.setCorrectiveDO365A()
        self.wcv_param_warning = rwc.HAZ_parameters(0, 0, 0, 0)
        self.wcv_param_warning.setWarningURCLEARED()

        #self.warning_t.setWarningDO365A()

        # Options: "05NM" / "22KFT" / "URCV"
        if WCV_Subset == "05NM" or WCV_Subset == "05NMShort":
            self.wcv_param_caution.setWCV_05NM()
            self.wcv_param_preventive.setPreventive_05NM()
            self.wcv_param_warning.setWCV_05NM()
        elif WCV_Subset == "22KFT" or WCV_Subset == "22KFTShort":
            self.wcv_param_caution.setWCV_22KFT()
            self.wcv_param_preventive.setPreventive_22KFT()
            self.wcv_param_warning.setWCV_22KFT()
        elif WCV_Subset == "URCV":
            self.wcv_param_caution.setWCV_URCV()
            self.wcv_param_preventive.setPreventive_URCV()
            self.wcv_param_warning.setWCV_URCV()
        elif WCV_Subset == "URCVShort":
            self.wcv_param_caution.setWCV_URCV_Short()
            self.wcv_param_preventive.setPreventive_URCV_Short()
            self.wcv_param_warning.setWCV_URCV_Short()
        elif WCV_Subset == "SESAR":
            self.wcv_param_caution.setWCV_SESAR()
            self.wcv_param_preventive.setPreventive_SESAR()
            self.wcv_param_warning.setWCV_SESAR()
        elif WCV_Subset == "DO365":
            self.wcv_param_caution.setWCV_DO365()
            self.wcv_param_preventive.setPreventive_DO365()
            self.wcv_param_warning.setWCV_DO365()


        # Set RWC time alerting parameters
        self.preventive_t = at.RWC_alert_times()
        self.warning_t = at.RWC_alert_times()

        self.caution_t = at.RWC_alert_times()
        if WCV_Subset == "05NM":
            self.caution_t.setCautionStd()
            self.preventive_t.setPreventiveStd()
            self.warning_t.setWarningStd()
        elif WCV_Subset == "22KFT":
            self.caution_t.setCautionStd()
            self.preventive_t.setPreventiveStd()
            self.warning_t.setWarningStd()
        elif WCV_Subset == "URCV":
            self.caution_t.setCautionStd()
            self.preventive_t.setPreventiveStd()
            self.warning_t.setWarningStd()
        elif WCV_Subset == "SESAR":
            self.caution_t.setCautionStd()
            self.preventive_t.setPreventiveStd()
            self.warning_t.setWarningStd()
        elif WCV_Subset == "DO365":
            self.caution_t.setCautionStd()
            self.preventive_t.setPreventiveStd()
            self.warning_t.setWarningStd()

        if WCV_Subset == "05NMShort":
            self.caution_t.setCautionShort()
            self.preventive_t.setPreventiveShort()
        elif WCV_Subset == "22KFTShort":
            self.caution_t.setCautionShort()
            self.preventive_t.setPreventiveShort()
        elif WCV_Subset == "URCVShort":
            self.caution_t.setCautionShort()
            self.preventive_t.setPreventiveShort()
            self.warning_t.setWarningShort()

        #self.correct_t.setCorrectiveDO365A()


        # Set NMAC parameters
        self.nmac_param = nmac.NMAC_parameters(0, 0)
        self.nmac_param.setStandard()

        # Set NCS parameters
        self.ncs_actv = False
        self.ncs_param = ncsp.NCS_parameters(0, 0, 0, 0, 0)


    def set_DEG_parameters(self):
        self.preventive_actv = True
        self.caution_actv = True
        self.warning_actv = True
        self.threshold_actv = True

        # Set HAZ parameters
        self.wcv_param_preventive = rwc.HAZ_parameters(0, 0, 0, 0)
        self.wcv_param_preventive.setPreventiveDEG()
        self.wcv_param_caution = rwc.HAZ_parameters(0, 0, 0, 0)
        self.wcv_param_caution.setCorrectiveDEG()
        self.wcv_param_warning = rwc.HAZ_parameters(0, 0, 0, 0)
        self.wcv_param_warning.setWarningDEG()

        # Set RWC time alerting parameters
        self.preventive_t = at.RWC_alert_times()
        self.preventive_t.setPreventiveDEG()
        self.caution_t = at.RWC_alert_times()
        self.caution_t.setCautionDEG()
        self.warning_t = at.RWC_alert_times()
        self.warning_t.setWarningDEG()

        # Set NMAC parameters
        self.nmac_param = nmac.NMAC_parameters(0, 0)
        self.nmac_param.setDEG()

        # Set NCS parameters
        self.ncs_actv = False
        self.ncs_param = ncsp.NCS_parameters(0, 0, 0, 0, 0)



    def parseFromJson(self, filename):
        complete = True

        print("Loading protection information from file: ", filename)
        f = open(filename, "r")
        protectionTable = json.load(f)

        if 'name' in protectionTable:
            self.name = protectionTable['name']

        if 'version' in protectionTable:
            self.version = protectionTable['version']

        if 'description' in protectionTable:
            self.description = protectionTable['description']

        if 'preventiveDistribution' in protectionTable:
            complete = complete and self.parseAircraftClassList(self.preventive_t, self.wcv_param_preventive, protectionTable['preventiveDistribution'])
        else:
            complete = False

        if 'initialDistribution' in distributionsTable:
            complete = complete and self.parseInitialDistribution(distributionsTable['initialDistribution'])
        else:
            complete = False

        #if 'transitionDistribution' in distributionsTable:
        #    self.Transition_Distribution = self.parseTransitionDistribution(distributionsTable['transitionDistribution'])
        #    complete = complete and (self.Transition_Distribution != None)
        #else:
        #    complete = False

        return complete
