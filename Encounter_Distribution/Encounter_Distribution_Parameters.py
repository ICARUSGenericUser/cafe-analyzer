
class Encounter_Distribution_Parameters:

  def __init__(self):
    self.VMD_Layers = 10
    self.VMD_Range = 4000
    self.HMD_Layers = 20
    self.HMD_Range = 20000
    self.HSpeed_Layers = 30
    self.HSpeed_Min_Range = 30
    self.HSpeed_Max_Range = 500
    self.VSpeed_Layers = 20
    self.VSpeed_Range = 5000
    self.AAngle_Layers = 19
    self.AAngle_Range = 180
    self.Altitude_Layers = 30
    self.Altitude_Min_Range = 500
    self.Altitude_Max_Range = 40500
    self.TR_Layers = 10
    self.TR_Range = 10
    self.Aircraft_Class_Range = 21
    # CAFE REV 6
    self.Aircraft_Names = [
        "Unclassified",
        "Piston",
        "TP, MTOM < 5,7 T",
        "TP, MTOM 5,7 - 15 T",
        "TP, MTOM > 15 T",
        "Jet",
        "TJ, MTOM 5,7 - 15 T",
        "TJ, MTOM 15 - 100 T",
        "TJ, MTOM > 100 T",
        "TJ, MTOM < 5,7 T",
        "RPAS P < FL180",
        "RPAS P < FL250",
        "RPAS P < FL300",
        "RPAS TP",
        "RPAS Jet",
        "HELI TH",
        "HELI P / UAM",
        "TILTROTOR",
        "RPAS HELI TACTICAL",
        "Unknown Turbojet",
        "Unknown Piston"
    ]

