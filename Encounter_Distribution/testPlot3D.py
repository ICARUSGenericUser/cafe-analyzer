from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
import numpy as np
from Encounter_Distribution import Encounter_Distribution_Parameters as ep
from Encounter_Distribution import Encounter_distribution as ed
from matplotlib import style
#style.use('ggplot')

enc_parameters = ep.Encounter_Distribution_Parameters()
enc_distribution = ed.Encounter_distribution(enc_parameters)

X_dim = (enc_distribution.VMD_Layers + 1)
Y_dim = (enc_distribution.HMD_Layers + 1)

namesVMD = enc_distribution.generateIndex1Range(enc_distribution.VMD_Layers, enc_distribution.VMD_Range)
namesHMD = enc_distribution.generateIndex1Range(enc_distribution.HMD_Layers, enc_distribution.HMD_Range)

npp = X_dim * Y_dim
np.random.seed(1234)
fig = plt.figure()
fig.set_size_inches(15, 10)
ax1 = fig.add_subplot(111, projection='3d')
A = np.random.randint(5, size=(X_dim, Y_dim))

x = np.array([[i] * Y_dim for i in range(X_dim)]).ravel() # x coordinates of each bar
y = np.array([i for i in range(Y_dim)] * X_dim) # y coordinates of each bar
z = np.zeros(npp) # z coordinates of each bar
dx = np.ones(npp) # length along x-axis of each bar
dy = np.ones(npp) # length along y-axis of each bar
dz = A.ravel() # length along z-axis of each bar (height)
#dz = enc_distribution.VMD_HMD_Samples.ravel()  # length along z-axis of each bar (height)

#ax1.w_xaxis.set_ticklabels(namesVMD)
#ax1.w_yaxis.set_ticklabels(namesHMD)

ax1.bar3d(x, y, z, dx, dy, dz)

ticksx = np.arange(0, enc_distribution.VMD_Layers + 1, 1)
plt.xticks(ticksx, namesVMD)

ticksy = np.arange(0, enc_distribution.HMD_Layers + 1, 1)
plt.yticks(ticksy, namesHMD)

plt.xticks(fontsize=7, rotation=45)
plt.yticks(fontsize=7)
plt.legend(prop={'size': 6})

ax1.set_xlabel('VMD', labelpad=15)
ax1.set_ylabel('HMD', labelpad=10)
ax1.set_zlabel('Occurrence')
#ax1.tick_params(axis='both', which='major', pad=5)
plt.title("VMD / HMD Distribution")

plt.show()
