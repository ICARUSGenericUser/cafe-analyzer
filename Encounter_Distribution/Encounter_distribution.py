import numpy as np
from Encounter import units as units
from RWC import RWC_volume as rwc
from Util import sign
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


class Encounter_distribution:

  def __init__(self, parameters):
    self.VMD_Layers = parameters.VMD_Layers
    self.VMD_Range = parameters.VMD_Range
    self.HMD_Layers = parameters.HMD_Layers
    self.HMD_Range = parameters.HMD_Range
    self.HSpeed_Layers = parameters.HSpeed_Layers
    self.HSpeed_Min_Range = parameters.HSpeed_Min_Range
    self.HSpeed_Max_Range = parameters.HSpeed_Max_Range
    self.VSpeed_Layers = parameters.VSpeed_Layers
    self.VSpeed_Range = parameters.VSpeed_Range
    self.AAngle_Layers = parameters.AAngle_Layers
    self.AAngle_Range = parameters.AAngle_Range
    self.Altitude_Layers = parameters.Altitude_Layers
    self.Altitude_Min_Range = parameters.Altitude_Min_Range
    self.Altitude_Max_Range = parameters.Altitude_Max_Range
    self.TR_Layers = parameters.TR_Layers
    self.TR_Range = parameters.TR_Range
    self.Aircraft_Class_Range = parameters.Aircraft_Class_Range
    self.namesAC = parameters.Aircraft_Names

    self.path = ""

    self.VMD_Samples = [0] * (self.VMD_Layers + 1)
    self.HMD_Samples = [0] * (self.HMD_Layers + 1)
    self.VMD_HMD_Samples = np.zeros((self.VMD_Layers + 1, self.HMD_Layers + 1))
    self.HSpeed_Samples = [0] * (self.HSpeed_Layers + 2)
    self.VSpeed_Samples_Climb = [0] * (self.VSpeed_Layers + 1)
    self.VSpeed_Samples_Descent = [0] * (self.VSpeed_Layers + 1)
    self.Altitude_Samples = [0] * (self.Altitude_Layers + 2)
    self.TR_Samples = [0] * (self.TR_Layers + 1)

    self.AC1_Samples = [0] * (self.Aircraft_Class_Range)
    self.AC2_Samples = [0] * (self.Aircraft_Class_Range)
    self.AC1_AC2_Samples = np.zeros((self.Aircraft_Class_Range, self.Aircraft_Class_Range))

    self.AAngle_Samples = [0] * (2*self.AAngle_Layers + 3)

    self.NMAC = 0
    self.NMAC_Total = 0
    self.NMAC_Samples = [0] * (self.Aircraft_Class_Range)
    self.AC1_AC2_NMAC_Samples = np.zeros((self.Aircraft_Class_Range, self.Aircraft_Class_Range))

    print("VMD: ", self.VMD_Layers, "HMD: ", self.HMD_Layers)
    print("VMD-HMD Size: ", np.shape(self.VMD_HMD_Samples))


  def findIndex1Range(self, value, layers, range):
    if value > range:
      return layers
    else:
      return int(value * layers / range)


  def findIndex1RangeSymm(self, value, layers, range):
    idx = 0

    if value > range:
      #print("Beyond max: ", value)
      idx = layers
    elif value < -1 * range:
      #print("Below min: ", value)
      idx = -layers
    elif abs(value) < range/(layers + 1):
      #print("At zero: ", value, " Limit: ", range/layers)
      idx = 0
    else:
      idx = sign.sign(value) * (int((abs(value) - range/(layers+1)) * (layers+1) / range) + 1)
      #print("In between: ", value, " Idx: ", idx)

    #print("Adding offset: ", idx + layers + 1)
    return idx + layers + 1


  def generateIndex1Range(self, layers, range):
    names = [None] * (layers + 1)

    idx = 0
    factor = range/layers
    while idx < layers:
      low = '{:05.0f}'.format(idx  *  factor)
      up = '{:05.0f}'.format((idx+1)  *  factor)
      names[idx] = "-" + up
      idx = idx + 1
    names[idx] = up + "-"
    return names


  def generateIndex1RangeSymm(self, layers, range):
    names = [None] * (2*layers + 3)

    names[0] = "<" + str(range)
    names[2*layers + 2] = ">" + str(range)
    names[layers + 1] = "0"

    idx = 1
    factor = range/(layers + 1)
    while idx <= layers:
      low = '{:05.0f}'.format(idx  *  factor)
      up = '{:05.0f}'.format((idx + 1)  *  factor)
      names[layers + 1 + idx] = low + "-"
      idx = idx + 1

    idx = 1
    while idx <= layers:
      low = '{:05.0f}'.format(idx * factor)
      up = '{:05.0f}'.format((idx + 1) * factor)
      names[layers + 1 - idx] = "-" + low
      idx = idx + 1

    return names


  def generateIndex2Range(self, layers, minrange, maxrange):
    names = [None] * (layers + 2)
    factor = (maxrange-minrange) / layers
    low = '{:05.0f}'.format(minrange)
    names[0] = "-" + low

    idx = 0
    while idx < layers:
      low = '{:05.0f}'.format(idx * factor + minrange)
      up = '{:05.0f}'.format((idx + 1) * factor + minrange)
      names[idx+1] = "-" + up
      idx = idx + 1
    names[idx+1] = up + "-"
    return names


  def findIndex2Range(self, value, layers, minrange, maxrange):
    #print("MinRange: ", minrange, " Maxrange: ", maxrange, " Layers: ", layers)
    if value > maxrange:
      #print("Return max idx: ", layers)
      return layers
    elif value < minrange:
      #print("Return min idx: ", layers)
      return 0
    else:
      #print("Top: ", value * layers, " Divisor: ", maxrange-minrange, " Ratio: ", (value - minrange) * layers / (maxrange-minrange))
      return int((value - minrange) * layers / (maxrange-minrange)) + 1


  def recordVMD(self, value):
    idx = self.findIndex1Range(value, self.VMD_Layers, self.VMD_Range)

    #print("VMD: ", value, " Index: ", idx, "\n")

    self.VMD_Samples[idx] += 1


  def recordHMD(self, value):
    idx = self.findIndex1Range(units.nm_to_ft(value), self.HMD_Layers, self.HMD_Range)

    #print("HMD: ", units.nm_to_ft(value), " Index: ", idx, "\n")

    self.HMD_Samples[idx] += 1


  def recordVMDHMD(self, vmd, hmd):
    idxHMD = self.findIndex1Range(units.nm_to_ft(hmd), self.HMD_Layers, self.HMD_Range)
    idxVMD = self.findIndex1Range(vmd, self.VMD_Layers, self.VMD_Range)

    self.VMD_HMD_Samples[idxVMD][idxHMD] += 1


  def recordAC1AC2(self, ac1, ac2):
    if ac1 < len(self.AC1_Samples) and ac2 < len(self.AC2_Samples):
      self.AC1_Samples[ac1] += 1
      self.AC2_Samples[ac2] += 1
      self.AC1_AC2_Samples[ac1][ac2] += 1
    else:
      print("Out of range AC index: ", ac1, " or ", ac2, "\n")


  def recordHSpeed(self, value):
    idx = self.findIndex2Range(value, self.HSpeed_Layers, self.HSpeed_Min_Range, self.HSpeed_Max_Range)

    #print("HSpeed: ", value, " Index: ", idx, "\n")

    self.HSpeed_Samples[idx] += 1


  def recordVSpeed(self, value):
    idx = self.findIndex1Range(abs(value), self.VSpeed_Layers, self.VSpeed_Range)

    #print("VSpeed: ", value, " Index: ", idx, "\n")

    if value >= 0:
      self.VSpeed_Samples_Climb[idx] += 1
    else:
      self.VSpeed_Samples_Descent[idx] += 1


  def recordAAngle(self, value):
    idx = self.findIndex1RangeSymm(value, self.AAngle_Layers, self.AAngle_Range)

    #print("AAngle: ", value, " Index: ", idx, "\n")

    self.AAngle_Samples[idx] += 1


  def recordAltitude(self, value):
    idx = self.findIndex2Range(value, self.Altitude_Layers, self.Altitude_Min_Range, self.Altitude_Max_Range)

    # print("HSpeed: ", value, " Index: ", idx, "\n")

    self.Altitude_Samples[idx] += 1


  def recordTurnrate(self, value):
    idx = self.findIndex1Range(value, self.TR_Layers, self.TR_Range)

    # print("HMD: ", units.nm_to_ft(value), " Index: ", idx, "\n")

    self.TR_Samples[idx] += 1


  def recordNMAC(self, vmd, hmd, ac1, ac2,  NMAC_Parameters):
    if rwc.check_vmdhmd(units.ft_to_m(vmd), units.nm_to_m(hmd), NMAC_Parameters):
      self.NMAC_Total += 1
      self.NMAC_Samples[ac1] += 1
      self.NMAC_Samples[ac2] += 1
      self.AC1_AC2_NMAC_Samples[ac1][ac2] += 1


  def plotVMDDistribution(self, path):
    names = self.generateIndex1Range(self.VMD_Layers, self.VMD_Range)
    label = "VMD (ft)"
    plt.bar(names, self.VMD_Samples, color='blue', label=label)
    plt.xticks(fontsize=6, rotation=45)
    plt.yticks(fontsize=6)
    plt.legend(prop={'size': 6})

    plt.title("VMD Distribution", fontsize=7)
    fig=plt.gcf()
    fig.set_size_inches(12.0, 12.0)
    filepath = path + "\\" + self.path + "\\VMD_distribution" + ".png"
    print("Generating figure: " + filepath)
    fig.savefig(filepath, dpi=300)
    plt.close(fig)


  def plotHMDDistribution(self, path):
    names = self.generateIndex1Range(self.HMD_Layers, self.HMD_Range)
    label = "HMD (ft)"
    plt.bar(names, self.HMD_Samples, color='blue', label=label)
    plt.xticks(fontsize=6, rotation=45)
    plt.yticks(fontsize=6)
    plt.legend(prop={'size': 6})

    plt.title("HMD Distribution", fontsize=7)
    fig = plt.gcf()
    fig.set_size_inches(12.0, 12.0)
    filepath = path + "\\" + self.path + "\\HMD_distribution" + ".png"
    print("Generating figure: " + filepath)
    fig.savefig(filepath, dpi=300)
    plt.close(fig)


  def plotVMDHMDDistribution(self, path):

    X_dim = (self.VMD_Layers + 1)
    Y_dim = (self.HMD_Layers + 1)

    namesVMD = self.generateIndex1Range(self.VMD_Layers, self.VMD_Range)
    namesHMD = self.generateIndex1Range(self.HMD_Layers, self.HMD_Range)

    npp = X_dim * Y_dim

    fig = plt.figure()
    fig.set_size_inches(20, 20)
    ax1 = fig.add_subplot(111, projection='3d')

    x = np.array([[i] * Y_dim for i in range(X_dim)]).ravel()  # x coordinates of each bar
    y = np.array([i for i in range(Y_dim)] * X_dim)  # y coordinates of each bar
    z = np.zeros(npp)  # z coordinates of each bar
    dx = np.ones(npp)  # length along x-axis of each bar
    dy = np.ones(npp)  # length along y-axis of each bar
    dz = self.VMD_HMD_Samples.ravel()  # length along z-axis of each bar (height)

    ax1.bar3d(x, y, z, dx, dy, dz)
    ax1.set_zlim3d(bottom=0)
    ax1.grid(True)

    ticksx = np.arange(0, self.VMD_Layers + 1, 1)
    plt.xticks(ticksx, namesVMD)

    ticksy = np.arange(0, self.HMD_Layers + 1, 1)
    plt.yticks(ticksy, namesHMD)

    plt.xticks(fontsize=6, rotation=45)
    plt.yticks(fontsize=6)
    plt.legend(prop={'size': 6})

    ax1.set_xlabel('VMD', labelpad=15)
    ax1.set_ylabel('HMD', labelpad=10)
    ax1.set_zlabel('Occurrence')
    # ax1.tick_params(axis='both', which='major', pad=5)
    plt.title("VMD / HMD Distribution", fontsize=7)
    #fig.set_size_inches(12.0, 12.0)

    plt.show()

    filepath = path + "\\" + self.path + "\\VMD_HMD_distribution" + ".png"
    print("Generating figure: " + filepath)
    fig.savefig(filepath, dpi=600)
    plt.close(fig)



  def plotNMACDistribution(self, path):
    label = "AC (Class)"
    plt.bar(self.namesAC, self.NMAC_Samples, color='blue', label=label)
    plt.xticks(fontsize=6, rotation=45)
    plt.yticks(fontsize=6)
    plt.legend(prop={'size': 6})

    plt.title("NMAC Distribution", fontsize=7)
    fig = plt.gcf()
    fig.set_size_inches(12.0, 12.0)
    filepath = path + "\\" + self.path + "\\NMAC_distribution" + ".png"
    print("Generating figure: " + filepath)
    fig.savefig(filepath, dpi=300)
    plt.close(fig)



  def plotAC1Distribution(self, path):
    label = "AC1 (Class)"
    plt.bar(self.namesAC, self.AC1_Samples, color='blue', label=label)
    plt.xticks(fontsize=6, rotation=45)
    plt.yticks(fontsize=6)
    plt.legend(prop={'size': 6})

    plt.title("AC1 Distribution", fontsize=7)
    fig = plt.gcf()
    fig.set_size_inches(12.0, 12.0)
    filepath = path + "\\" + self.path + "\\AC1_distribution" + ".png"
    print("Generating figure: " + filepath)
    fig.savefig(filepath, dpi=300)
    plt.close(fig)


  def plotAC2Distribution(self, path):
    label = "AC2 (Class)"
    plt.bar(self.namesAC, self.AC2_Samples, color='blue', label=label)
    plt.xticks(fontsize=6, rotation=45)
    plt.yticks(fontsize=6)
    plt.legend(prop={'size': 6})

    plt.title("AC2 Distribution", fontsize=7)
    fig = plt.gcf()
    fig.set_size_inches(12.0, 12.0)
    filepath = path + "\\" + self.path + "\\AC2_distribution" + ".png"
    print("Generating figure: " + filepath)
    fig.savefig(filepath, dpi=300)
    plt.close(fig)


  def plotAC1AC2Distribution(self, path):
    X_dim = (self.Aircraft_Class_Range)
    Y_dim = (self.Aircraft_Class_Range)

    npp = X_dim * Y_dim

    fig = plt.figure()
    ax1 = fig.add_subplot(111, projection='3d')

    x = np.array([[i] * Y_dim for i in range(X_dim)]).ravel()  # x coordinates of each bar
    y = np.array([i for i in range(Y_dim)] * X_dim)  # y coordinates of each bar
    z = np.zeros(npp)  # z coordinates of each bar
    dx = np.ones(npp)  # length along x-axis of each bar
    dy = np.ones(npp)  # length along y-axis of each bar
    dz = self.AC1_AC2_Samples.ravel()  # length along z-axis of each bar (height)

    ax1.bar3d(x, y, z, dx, dy, dz)
    ax1.set_zlim3d(bottom=0)
    ax1.grid(True)

    ticksx = np.arange(0, self.Aircraft_Class_Range, 1)
    plt.xticks(ticksx, self.namesAC)

    ticksy = np.arange(0, self.Aircraft_Class_Range, 1)
    plt.yticks(ticksy, self.namesAC)

    plt.xticks(fontsize=6, rotation=45)
    plt.yticks(fontsize=6)
    plt.legend(prop={'size': 6})

    ax1.set_xlabel('AC1', labelpad=15)
    ax1.set_ylabel('AC2', labelpad=10)
    ax1.set_zlabel('Occurrence')
    # ax1.tick_params(axis='both', which='major', pad=5)
    plt.title("AC1 / AC2 Distribution", fontsize=7)
    fig.set_size_inches(20.0, 20.0)

    plt.show()

    filepath = path + "\\" + self.path + "\\AC1_AC2_distribution" + ".png"
    print("Generating figure: " + filepath)
    fig.savefig(filepath, dpi=600)
    # ax1.close(fig)



  def plotAC1AC2NMACDistribution(self, path):
    X_dim = (self.Aircraft_Class_Range)
    Y_dim = (self.Aircraft_Class_Range)

    npp = X_dim * Y_dim

    fig = plt.figure()
    ax1 = fig.add_subplot(111, projection='3d')

    x = np.array([[i] * Y_dim for i in range(X_dim)]).ravel()  # x coordinates of each bar
    y = np.array([i for i in range(Y_dim)] * X_dim)  # y coordinates of each bar
    z = np.zeros(npp)  # z coordinates of each bar
    dx = np.ones(npp)  # length along x-axis of each bar
    dy = np.ones(npp)  # length along y-axis of each bar
    dz = self.AC1_AC2_NMAC_Samples.ravel()  # length along z-axis of each bar (height)

    ax1.bar3d(x, y, z, dx, dy, dz)
    ax1.set_zlim3d(bottom=0)
    ax1.grid(True)

    ticksx = np.arange(0, self.Aircraft_Class_Range, 1)
    plt.xticks(ticksx, self.namesAC)

    ticksy = np.arange(0, self.Aircraft_Class_Range, 1)
    plt.yticks(ticksy, self.namesAC)

    plt.xticks(fontsize=6, rotation=45)
    plt.yticks(fontsize=6)
    plt.legend(prop={'size': 6})

    ax1.set_xlabel('AC1', labelpad=15)
    ax1.set_ylabel('AC2', labelpad=10)
    ax1.set_zlabel('Occurrence')
    # ax1.tick_params(axis='both', which='major', pad=5)
    plt.title("AC1 / AC2 NMAC Distribution", fontsize=7)
    fig.set_size_inches(20.0, 20.0)

    plt.show()

    filepath = path + "\\" + self.path + "\\AC1_AC2_NMAC_distribution" + ".png"
    print("Generating figure: " + filepath)
    fig.savefig(filepath, dpi=600)
    # ax1.close(fig)







  def plotVSpeedDistribution(self, path):
    names = names = self.generateIndex1Range(self.VSpeed_Layers, self.VSpeed_Range)
    label = "Climb V Speed (fpm)"
    plt.bar(names, self.VSpeed_Samples_Climb, color='blue', label=label)
    plt.xticks(fontsize=6, rotation=45)
    plt.yticks(fontsize=6)
    plt.legend(prop={'size': 6})

    plt.title("Climb Vertical Speed Distribution", fontsize=7)
    fig = plt.gcf()
    fig.set_size_inches(12.0, 12.0)
    filepath = path + "\\" + self.path + "\\VSpeed_Climb_distribution" + ".png"
    print("Generating figure: " + filepath)
    fig.savefig(filepath, dpi=300)
    plt.close(fig)

    label = "Descent V Speed (fpm)"
    plt.bar(names, self.VSpeed_Samples_Descent, color='blue', label=label)
    plt.xticks(fontsize=6, rotation=45)
    plt.yticks(fontsize=6)
    plt.legend(prop={'size': 6})

    plt.title("Descent Vertical Speed Distribution", fontsize=7)
    fig = plt.gcf()
    fig.set_size_inches(12.0, 12.0)
    filepath = path + "\\" + self.path + "\\VSpeed_Descent_distribution" + ".png"
    print("Generating figure: " + filepath)
    fig.savefig(filepath, dpi=300)
    plt.close(fig)


  def plotHSpeedDistribution(self, path):
    names = self.generateIndex2Range(self.HSpeed_Layers, self.HSpeed_Min_Range, self.HSpeed_Max_Range)
    label = "H Speed (kt)"
    plt.bar(names, self.HSpeed_Samples, color='blue', label=label)
    plt.xticks(fontsize=6, rotation=45)
    plt.yticks(fontsize=6)
    plt.legend(prop={'size': 6})

    plt.title("Horizontal Speed Distribution", fontsize=7)
    fig = plt.gcf()
    fig.set_size_inches(12.0, 12.0)
    filepath = path + "\\" + self.path + "\\HSpeed_distribution" + ".png"
    print("Generating figure: " + filepath)
    fig.savefig(filepath, dpi=300)
    plt.close(fig)


  def plotAAngleDistribution(self, path):
    names = self.generateIndex1RangeSymm(self.AAngle_Layers, self.AAngle_Range)
    label = "Approach Angle (o)"
    plt.bar(names, self.AAngle_Samples, color='blue', label=label)
    plt.xticks(fontsize=6, rotation=45)
    plt.yticks(fontsize=6)
    plt.legend(prop={'size': 6})

    plt.title("Approach Angle Distribution", fontsize=7)
    fig = plt.gcf()
    fig.set_size_inches(12.0, 12.0)
    filepath = path + "\\" + self.path + "\\AAngle_distribution" + ".png"
    print("Generating figure: " + filepath)
    fig.savefig(filepath, dpi=300)
    plt.close(fig)



  def plotAltitudeDistribution(self, path):
    names = self.generateIndex2Range(self.Altitude_Layers, self.Altitude_Min_Range, self.Altitude_Max_Range)
    label = "Altitude (ft)"
    plt.bar(names, self.Altitude_Samples, color='blue', label=label)
    plt.xticks(fontsize=6, rotation=45)
    plt.yticks(fontsize=6)
    plt.legend(prop={'size': 6})

    plt.title("Altitude Distribution", fontsize=7)
    fig = plt.gcf()
    fig.set_size_inches(12.0, 12.0)
    filepath = path + "\\" + self.path + "\\Altitude_distribution" + ".png"
    print("Generating figure: " + filepath)
    fig.savefig(filepath, dpi=300)
    plt.close(fig)