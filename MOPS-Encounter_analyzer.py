# This is a sample Python script.
import os
import sys
import numpy as np
import pandas as pd
from Encounter_Distribution import Encounter_Parameters as ep
from Projection import FlatEarthProjection as proj
from Encounter import units as units
from Encounter import position as pos, encounter as enc
from Metrics import Enc_Alert_Measuring as mea, Alert_Measuring as am


def processTrajectoryFile(ep, enc_number, TestFolder, TestVector, DiagramFolder):

    if (not os.path.exists(TestFolder + "/Ownship/")) or (not os.path.exists(TestFolder + "/Intruder/")):
        print("Trajectory folders does not exists, skipping the associated trajectories...\n")
        return

    print("Reading an encounter...")

    ownSequence = pd.read_csv(TestFolder + "/Ownship/" + TestVector + "_Truth_TVOwn.csv")
    intrSequence = pd.read_csv(TestFolder + "/Intruder/" + TestVector + "_Truth_TVInt1.csv")

    #testColnames = ['ATAR_E', 'Pre_AR', 'Corr_AR', 'Warn_AR', 'Alt_Lat_Pre', 'Alt_Lat_Corr', 'Alt_Lat_Warn', 'Pre_Early', 'Corr_Early', 'Warn_Early',
    #                'Avg_Pre_AE', 'Avg_Corr_AE', 'Avg_Warn_AE', 'HMD', 'VMD', 'Arpt_Lat', 'Arpt_Lon', 'Arpt_Alt']
    #testSequence = pd.read_csv(TestFolder + "/" + TestVector + "_encounter_characterization_file.csv")

    ownSequence = ownSequence.drop('ICAO', axis=1)
    intrSequence = intrSequence.drop('ICAO', axis=1)

    aircraft1Class = 10
    ownSequence = ownSequence.rename(
        columns={'ToA(s)': 'ToA', 'Lat(deg)': 'Lat', 'Lon(deg)': 'Lon', 'Alt(ft)': 'Alt', 'EWV(kts)': 'Vx',
                 'NSV(kts)': 'Vy', 'VR(ft/min)': 'Vv', 'HDG(deg)': 'HDG'})

    aircraft2Class = 1
    intrSequence = intrSequence.rename(
        columns={'ToA(s)': 'ToA', 'Lat(deg)': 'Lat', 'Lon(deg)': 'Lon', 'Alt(ft)': 'Alt', 'EWV(kts)': 'Vx',
                 'NSV(kts)': 'Vy', 'VR(ft/min)': 'Vv', 'HDG(deg)': 'HDG'})

    if not os.path.exists(DiagramFolder):
        os.makedirs(DiagramFolder)

    # We create the encounter and we fill it with the data from the data files
    # Note that a coordinate transformation is still necessary to a local coordinate system
    encounter = enc.Encounter(enc_number, aircraft1Class, aircraft2Class)

    # In order to perform the coordinate system transformation we seek a centre postion averaged
    # from all positions that occur along both own ship and intruder
    nSample = 0; meanLat = 0; meanLon = 0
    for index, row in ownSequence.iterrows():

        position = pos.Position(1, index, 0, 0, 0)
        position.setLocation(row['Lon'], row['Lat'],  units.ft_to_m(row['Alt']))
        position.assignDynamics(row['Vx'], row['Vy'], row['Vv'], row['HDG'])
        encounter.addAircraft1Position(position)
        nSample += 1; meanLat += row['Lat']; meanLon += row['Lon']

    for index, row in intrSequence.iterrows():

        position = pos.Position(2, index, 0, 0, 0)
        position.setLocation(row['Lon'], row['Lat'], units.ft_to_m(row['Alt']))
        position.assignDynamics(row['Vx'], row['Vy'], row['Vv'], row['HDG'])
        encounter.addAircraft2Position(position)
        nSample += 1; meanLat += row['Lat']; meanLon += row['Lon']

    # encounter.testDistances()

    # Now we compute the average and set up the coordinate transformation
    meanLat = meanLat/nSample; meanLon = meanLon/nSample

    projection = proj.FlatEarthProjection(meanLon, meanLat, 0)

    #print("Checking positions: ")
    for position in encounter.aircraft1:
        #print("Current position Lon: ", position.x_loc, " Lan: ", position.y_loc)
        position.updateCoordinateSystem(projection)
        #print("Updated position X: ", position.x_loc, " Y: ", position.y_loc)

    #print("Checking positions: ")
    for position in encounter.aircraft2:
        #print("Current position Lon: ", position.x_loc, " Lan: ", position.y_loc)
        position.updateCoordinateSystem(projection)
        #print("Updated position X: ", position.x_loc, " Y: ", position.y_loc)

    if (encounter.numberAircraft1Position() == encounter.numberAircraft2Position()):

        encounter.encAlertMeasuringAicraft1 = arcf1_eam = mea.Enc_Alert_Measuring()
        encounter.encAlertMeasuringAicraft2 = arcf2_eam = mea.Enc_Alert_Measuring()

        encounter.computeDynamics()
        encounter.computeConflictsCAFE(ep, arcf1_eam, arcf2_eam)

        if arcf1_eam.active_conflict:
            print("Reporting on aircraft 1:")
            arcf1_eam.report_Alert_Measuring()
        if arcf2_eam.active_conflict:
            print("\nReporting on aircraft 2:")
            arcf2_eam.report_Alert_Measuring()

        if ep.mustPlot:
            encounter.plotEncounterCAFE(DiagramFolder)
    else:
        print("Trajectory seems NOT consistent.")

    print("Done.")



# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # creating the name

    ##### INPUT DATA ######
    ENCOUNTER_FOLDER = "E:\CAFE-RPAS-Encounters\MOPS_Test_Vectors"
    ENCOUNTER_MATRIX = np.array([["converging", "C", 32],
                                 ["designer", "D", 17],
                                 ["dynamic", "LL", 180],
                                 ["headon", "H", 22],
                                 ["highspeed", "S", 8],
                                 ["maneuver", "M", 27],
                                 ["overtaking", "O", 23],])



    if not os.path.exists(ENCOUNTER_FOLDER):
        print("Root folder: ", ENCOUNTER_FOLDER, "\n")
        print("Does not exists, stopping...\n")
        exit(-1)

    print("Setting encounter parameters to DO365A")
    encounter_param = ep.Encounter_Parameters()
    encounter_param.set_DO365A_parameters()

    print("Setting standard TCASII parameters")
    encounter_param.set_TCASII_parameters()

    encounter_param.mustPlot = True
    encounter_param.debug = False

    print("Processing MOPS test vectors...\n")

    for row in ENCOUNTER_MATRIX:

        TestType = row[0]
        TestPrefix = row[1]
        MaxTestVectors = int(row[2])

        print("Processing test class: ", TestType, " with: ", MaxTestVectors, " test vectors.")

        DIAGRAM_FOLDER = ENCOUNTER_FOLDER + "/" + TestType + "/" + "Diagrams"

        i = 1
        while i < MaxTestVectors:

            # Read trajectory data
            TestVector = TestPrefix + str(i)
            TestFolder = ENCOUNTER_FOLDER + "/" + TestType + "/" + TestVector + "/"

            processTrajectoryFile(encounter_param, i, TestFolder, TestVector, DIAGRAM_FOLDER)
            i+=1

    print("Done.\n")

    sys.exit(0)
