
from WCRecovery import SelectWaypoint, SelectManeuver, ManeuverParameters, HorizontalManeuver, VelocityManeuver
from RWC import RWC_thresholds, HAZ_parameters
from Encounter import position
import math
import matplotlib.pyplot as plt


def read_waypoint_file(TRAJECTORY_FOLDER, enc_number):
    wp_reader = open(TRAJECTORY_FOLDER + "\\" + "Encs_" + str(enc_number).zfill(8) + "\\ownship\\WP.txt", "r")
    wp_array = []
    wp_line = wp_reader.readline().split(",")
    while wp_line[0] != "":
        wp = []
        for variable in wp_line:
            wp.append(round(float(variable), 2))
        wp_array.append(wp)
        wp_line = wp_reader.readline().split(",")
    return wp_array

def distance_to_waypoint(own_pos, wp):
    distance = math.sqrt((wp[1] - own_pos.x_loc) + (wp[2] - own_pos.y_loc) + (wp[3] - own_pos.altitude))
    return distance

if __name__ == '__main__':

    TRAJECTORY_FOLDER = "C:\\Users\\David\\Desktop\\TFG-Encounter-Analyzer\\Output_encounters\\testSet"

    enc_number = 3
    encounters = 4

    DMOD = HMD = 30
    TAUMOD = 0
    H = 10
    rwc_params = HAZ_parameters.HAZ_parameters(DMOD, HMD, TAUMOD, H)
    debug = False
    plots = True
    B = 0
    T = 5
    k = 1

    h_params = ManeuverParameters.Parameters(-90, 90, 1, 1)
    v_params = ManeuverParameters.Parameters(0, 150, 1, 1)
    s_params = ManeuverParameters.Parameters(1, 50, 1, 0.5)

    own_x_position = []
    own_y_position = []
    own_z_position = []

    recovery_x_position = []
    recovery_y_position = []
    recovery_z_position = []

    int_x_position = []
    int_y_position = []
    int_z_position = []

    time_array = []
    time_array1 = []

    time_after_WCV = 4
    time_interval = 0.02 # it may be 1 second later on


    while enc_number < encounters:
        # Read txt output file from DEG Generator
        own_reader = open(TRAJECTORY_FOLDER + "\\" + "Encs_" + str(enc_number).zfill(8) + "\\ownship\\TRJ_" + str(enc_number).zfill(
            8) + ".csv", "r")
        int_reader = open(TRAJECTORY_FOLDER + "\\" + "Encs_" + str(enc_number).zfill(8) + "\\intruder\\TRJ_" + str(enc_number).zfill(
                8) + ".csv", "r")
        # read ownship waypoint file
        wp_array = read_waypoint_file(TRAJECTORY_FOLDER, enc_number)
        own_line = own_reader.readline()
        int_line = int_reader.readline()
        new_own_data = []
        WCV_time = 0
        stop = False
        while own_line != "" and int_line != "" and not stop:
            own_data = []
            int_data = []
            if new_own_data == []:
                for own_parameter in own_line.split(","):
                    own_data.append(float(own_parameter))
            else:
                own_data = new_own_data
            for int_parameter in int_line.split(","):
                int_data.append(float(int_parameter))
            if round(own_data[0], 2) == round(int_data[0], 2):
                # create own and int positions (ID_own = 0 and ID_int = 1)
                own_pos = position.Position(0, own_data[1], own_data[2], own_data[3])
                own_pos.assignDynamics(own_data[4], own_data[5], own_data[6], 0)
                int_pos = position.Position(1, int_data[1], int_data[2], int_data[3])
                int_pos.assignDynamics(int_data[4], int_data[5], int_data[6], 0)

                # look for a WCV
                WCV_interval = RWC_thresholds.WCV_interval(own_pos, int_pos, B, T, rwc_params, debug)

                if WCV_interval.low < WCV_interval.up:

                    #print("There's a Well Clear Violation in the interval:"+str(WCV_interval.low)+"-"+str(WCV_interval.up))
                    # Select the maneuver to recover Well Clear Status
                    maneuver = SelectManeuver.compute_maneuver(own_pos, int_pos, h_params, v_params, s_params, k, B, T, rwc_params, debug)
                    new_own_data = SelectWaypoint.compute_next_intern_waypoint(own_pos, maneuver.maneuver_type, maneuver.maneuver, time_after_WCV, round(own_data[0], 2), time_interval)
                    #print("Time: " + str(int_data[0]) + ", Maneuver type: " + str(maneuver.maneuver_type) + ", Maneuver parameter: " + str(maneuver.maneuver))
                    if WCV_time == 0:
                        WCV_time = round(int_data[0], 2)
                elif WCV_interval.low > WCV_interval.up and new_own_data != []:
                    # look for the nearest waypoint from the original trajectory
                    closest_wp = SelectWaypoint.compute_closest_waypoint(wp_array, own_pos, int_data[0])
                    # compute new speeds to reach the closest waypoint
                    new_own_data = SelectWaypoint.compute_new_own_data(own_pos, closest_wp, int_data[0], time_interval)
                    new_own_pos = position.Position(0, own_data[1], own_data[2], own_data[3])
                    new_own_pos.assignDynamics(own_data[4], own_data[5], own_data[6], 0)
                    new_WCV_interval = RWC_thresholds.WCV_interval(new_own_pos, int_pos, B, T, rwc_params, debug)
                    if new_WCV_interval.low < new_WCV_interval.up:
                        maneuver = SelectManeuver.compute_maneuver(new_own_pos, int_pos, h_params, v_params, s_params, k, B, T, rwc_params, debug)
                        new_own_data = SelectWaypoint.compute_next_intern_waypoint(own_pos, maneuver.maneuver_type, maneuver.maneuver, time_after_WCV, round(own_data[0], 2), time_interval)
                    new_own_data = SelectWaypoint.compute_next_data_with_constant_speed(new_own_data, time_interval, int_data[0])
                if plots:
                    time_array.append(round(int_data[0], 2))
                    # Create an array position for own and int
                    own_x_position.append(own_pos.x_loc)
                    own_y_position.append(own_pos.y_loc)
                    own_z_position.append(own_pos.altitude)

                    int_x_position.append(int_pos.x_loc)
                    int_y_position.append(int_pos.y_loc)
                    int_z_position.append(int_pos.altitude)
                own_line = own_reader.readline()
                int_line = int_reader.readline()
            elif round(own_data[0], 2) < round(int_data[0], 2):
                own_line = own_reader.readline()
            elif round(own_data[0], 2) > round(int_data[0], 2):
                int_line = int_reader.readline()
        if plots:
            fig, ax = plt.subplots(2)
            ax[0].plot(own_x_position, own_y_position, label="ownship")
            ax[0].plot(int_x_position, own_y_position, label="intruder")
            # ax[0].plot(recovery_x_position, recovery_y_position, label = "ownship recovery")
            ax[0].legend()
            ax[1].plot(time_array, own_z_position, label="ownship")
            ax[1].plot(time_array, int_z_position, label="intruder")
            # ax[1].plot(time_array1, recovery_z_position, label = "ownship recovery")
            ax[1].legend()
            plt.show()

        enc_number += 1

