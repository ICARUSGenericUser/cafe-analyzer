import os, shutil
import numpy as np
import csv
import statistics
import matplotlib.pyplot as plt
from scipy.stats import norm

file = os.path.join("H:\DroneEncounters\EncounterSet\LowManTestSet-Set1-2000\\DEG_Analysis_Results-WCV1\\Encounter_Analysis_Results.csv")


# Dictionary to store column names and their corresponding indices
column_indices = {}

# Open the CSV file in read mode
with open(file, 'r', newline='', encoding='utf-8') as csvfile:
    # Create a CSV reader object
    csvreader = csv.reader(csvfile, delimiter=',')

    # Read the header row to get column names and indices
    header_row = next(csvreader)
    for index, column_name in enumerate(header_row):
        column_indices[column_name] = index

    # Lists to store the columns you want
    EncounterId = []
    activeConflict = []
    cpaTime = []
    cpaRange = []
    minHDistTime = []
    minHDistRange = []

    #NMAC
    nmacViolated = []
    nmacFirstTime = []
    nmacTime = []
    nmacHMD = []
    nmacVMD = []
    nmacTimeRange = []

    #WCV Violation (LoWC)
    hazViolated = []
    hazFirstTime = []
    hazFirstTimeSLoWC = []
    hazFirstTimeRangePen = []
    hazFirstTimeHMDPen = []
    hazFirstTimeVertPen = []
    hazFirstTcpa = []
    hazFirstDcpa = []
    haz_SLoWC_time = []
    hazSLoWC = []
    hazRangePen = []
    hazHMDPen = []
    hazVertPen = []
    hazTcpaTime = []
    hazTcpa = []
    hazDcpa = []
    hazFirstTimeRange = []
    hazSLoWCTimeRange = []

    #Caution
    cautionNumber = []
    cautionFirstTime = []
    cautionFirstTtHaz = []
    cautionFirstType = []
    cautionTime = []
    cautionTtHaz = []
    cautionType = []
    cautionFirstRange = []
    cautionRange = []
    cautionFirstEndTime = []
    cautionFirstEndRange = []
    cautionEndTime = []
    cautionEndRange = []

    #Preventive
    prevNumber = []
    prevFirstTime = []
    prevFirstTtHaz = []
    prevFirstType = []
    prevTime = []
    prevTtHaz = []
    prevType = []
    prevFirstRange = []
    prevRange = []
    prevFirstEndTime = []
    prevFirstEndRange = []
    prevEndTime = []
    prevEndRange = []

    #Warning
    warnNumber = []
    warnFirstTime = []
    warnFirstTtHaz = []
    warnFirstType = []
    warnTime = []
    warnTtHaz = []
    warnType = []
    warnFirstRange = []
    warnRange = []
    warnFirstEndTime = []
    warnFirstEndRange = []
    warnEndTime = []
    warnEndRange = []

    # Iterate through the rows and store values from even rows into vectors
    for index, row in enumerate(csvreader):
        # Check if the row index is even
        if index % 2 == 0:
            try:
                # Append the desired columns to the respective lists using column names
                EncounterId.append(row[column_indices[' EncounterId']])
                activeConflict.append(row[column_indices[' activeConflict']])
                cpaTime.append(row[column_indices[' cpaTime']])
                cpaRange.append(row[column_indices[' cpaRange']])
                minHDistTime.append(row[column_indices[' minHDistTime']])
                minHDistRange.append(row[column_indices[' minHDistRange']])

                #NMAC
                nmacViolated.append(row[column_indices[' nmacViolated']])
                nmacFirstTime.append(row[column_indices[' nmacFirstTime']])
                nmacTime.append(row[column_indices[' nmacTime']])
                nmacHMD.append(row[column_indices[' nmacHMD']])
                nmacVMD.append(row[column_indices[' nmacVMD']])
                nmacTimeRange.append(row[column_indices[' nmacTimeRange']])

                # WCV violation
                hazViolated.append(row[column_indices[' hazViolated']])
                hazFirstTime.append(row[column_indices[' hazFirstTime']])
                hazFirstTimeSLoWC.append(row[column_indices[' hazFirstTimeSLoWC']])
                hazFirstTimeRangePen.append(row[column_indices[' hazFirstTimeRangePen']])
                hazFirstTimeHMDPen.append(row[column_indices[' hazFirstTimeHMDPen']])
                hazFirstTimeVertPen.append(row[column_indices[' hazFirstTimeVertPen']])
                hazFirstTcpa.append(row[column_indices[' hazFirstTcpa']])
                hazFirstDcpa.append(row[column_indices[' hazFirstDcpa']])
                haz_SLoWC_time.append(row[column_indices[' haz_SLoWC_time']])
                hazSLoWC.append(row[column_indices[' hazSLoWC']])
                hazRangePen.append(row[column_indices[' hazRangePen']])
                hazHMDPen.append(row[column_indices[' hazHMDPen']])
                hazVertPen.append(row[column_indices[' hazVertPen']])
                hazTcpaTime.append(row[column_indices[' hazTcpaTime']])
                hazTcpa.append(row[column_indices[' hazTcpa']])
                hazDcpa.append(row[column_indices[' hazDcpa']])
                hazFirstTimeRange.append(row[column_indices[' hazFirstTimeRange']])
                hazSLoWCTimeRange.append(row[column_indices[' hazSLoWCTimeRange']])

                # Caution
                cautionNumber.append(row[column_indices[' cautionNumber']])
                cautionFirstTime.append(row[column_indices[' cautionFirstTime']])
                cautionFirstTtHaz.append(row[column_indices[' cautionFirstTtHaz']])
                cautionFirstType.append(row[column_indices[' cautionFirstType']])
                cautionTime.append(row[column_indices[' cautionTime']])
                cautionTtHaz.append(row[column_indices[' cautionTtHaz']])
                cautionType.append(row[column_indices[' cautionType']])
                cautionFirstRange.append(row[column_indices[' cautionFirstRange']])
                cautionRange.append(row[column_indices[' cautionRange']])
                cautionFirstEndTime.append(row[column_indices[' cautionFirstEndTime']])
                cautionFirstEndRange.append(row[column_indices[' cautionFirstEndRange']])
                cautionEndTime.append(row[column_indices[' cautionEndTime']])
                cautionEndRange.append(row[column_indices[' cautionEndRange']])

                # Preventive
                prevNumber.append(row[column_indices[' prevNumber']])
                prevFirstTime.append(row[column_indices[' prevFirstTime']])
                prevFirstTtHaz.append(row[column_indices[' prevFirstTtHaz']])
                prevFirstType.append(row[column_indices[' prevFirstType']])
                prevTime.append(row[column_indices[' prevTime']])
                prevTtHaz.append(row[column_indices[' prevTtHaz']])
                prevType.append(row[column_indices[' prevType']])
                prevFirstRange.append(row[column_indices[' prevFirstRange']])
                prevRange.append(row[column_indices[' prevRange']])
                prevFirstEndTime.append(row[column_indices[' prevFirstEndTime']])
                prevFirstEndRange.append(row[column_indices[' prevFirstEndRange']])
                prevEndTime.append(row[column_indices[' prevEndTime']])
                prevEndRange.append(row[column_indices[' prevEndRange']])

                # Warning
                warnNumber.append(row[column_indices[' warnNumber']])
                warnFirstTime.append(row[column_indices[' warnFirstTime']])
                warnFirstTtHaz.append(row[column_indices[' warnFirstTtHaz']])
                warnFirstType.append(row[column_indices[' warnFirstType']])
                warnTime.append(row[column_indices[' warnTime']])
                warnTtHaz.append(row[column_indices[' warnTtHaz']])
                warnType.append(row[column_indices[' warnType']])
                warnFirstRange.append(row[column_indices[' warnFirstRange']])
                warnRange.append(row[column_indices[' warnRange']])
                warnFirstEndTime.append(row[column_indices[' warnFirstEndTime']])
                warnFirstEndRange.append(row[column_indices[' warnFirstEndRange']])
                warnEndTime.append(row[column_indices[' warnEndTime']])
                warnEndRange.append(row[column_indices[' warnEndRange']])

            except KeyError:
                # Handle the case where the specified column name doesn't exist in the header row
                print("Specified column name not found in the CSV file.")


# 5.1 RWC NOMINAL PERFORMANCE--------------------------------
total_Conflicts=0;
for x in range(len(activeConflict)):
    if activeConflict[x] == " True":  # This condition checks if the value is True
        total_Conflicts += 1;

total_Prev=0;
for i in range(len(prevNumber)):
    # This condition checks if the value is True
    total_Prev=total_Prev+int(prevNumber[i]);

total_Caution=0;
for i in range(len(cautionNumber)):
    # This condition checks if the value is True
    total_Caution=total_Caution+int(cautionNumber[i]);

total_Warn=0;
for i in range(len(warnNumber)):
    # This condition checks if the value is True
    total_Warn=total_Warn+int(warnNumber[i]);

total_LoWC=0;
for x in range(len(hazViolated)):
    if hazViolated[x] == " True":  # This condition checks if the value is True
        total_LoWC += 1;

hazViolatedIdx = [i for i in range(len(hazViolated)) if hazViolated[i] == " True"]


total_NMAC=0;
for x in range(len(nmacViolated)):
    if nmacViolated[x] == " True":  # This condition checks if the value is True
        total_NMAC += 1;

print('Active Conflicts: ', total_Conflicts)
print('Total preventive: ', total_Prev)
print('Total caution: ', total_Caution)
print('Total warning: ', total_Warn)
print('Total alerts: ', total_Warn+total_Caution+total_Prev)
print('Total LoWC: ', total_LoWC)
print('Total NMAC: ', total_NMAC)

# 5.2 RWC NOMINAL PERFORMANCE--------------------------------

CPA_Time = [int(number.strip()) for number in cpaTime] #CPA time values as integers
CPA_Time=[value for value in CPA_Time if value != 0] #Eliminate the ones with no conflict
mean_CPA_Time=statistics.mean(CPA_Time);
STD_CPA_Time=statistics.stdev(CPA_Time);
print("Mean CPA Time: ", mean_CPA_Time)
print("STD CPA Time: ", STD_CPA_Time)

CPA_Range = [float(number.strip()) for number in cpaRange] #CPA time values as integers
mean_CPA_Range=statistics.mean(CPA_Range);
STD_CPA_Range=statistics.stdev(CPA_Range);
print("Mean CPA Range: ", mean_CPA_Range)
print("STD CPA Range: ", STD_CPA_Range)

minHD_Range= [float(number.strip()) for number in minHDistRange] #CPA time values as integers
mean_minHD_Range=statistics.mean(minHD_Range);
STD_minHD_Range=statistics.stdev(minHD_Range);
print("Mean minHD Range: ", mean_minHD_Range)
print("STD minHD Range: ", STD_minHD_Range)

#NMAC
NMACtime = [int(number.strip()) for number in nmacFirstTime] #CPA time values as integers
NMACtime=[value for value in NMACtime if value != 0] #Eliminate the ones with no conflict
mean_firstNMAC_Time=statistics.mean(NMACtime);
STD_firstNAMC_Time=statistics.stdev(NMACtime);
print("Mean NMAC Time: ", mean_firstNMAC_Time)
print("STD NMAC Time: ", STD_firstNAMC_Time)

NMACHMD = [float(number.strip()) for number in nmacHMD] #CPA time values as integers
NMACHMD=[value for value in NMACHMD if value != 0] #Eliminate the ones with no conflict
mean_NMAC_HMD=statistics.mean(NMACHMD);
STD_NAMC_HMD=statistics.stdev(NMACHMD);
print("Mean NMAC HMD: ", mean_NMAC_HMD)
print("STD NMAC HMD: ", STD_NAMC_HMD)

NMACVMD = [float(number.strip()) for number in nmacVMD] #CPA time values as integers
NMACVMD=[value for value in NMACVMD if value != 0] #Eliminate the ones with no conflict
mean_NMAC_VMD=statistics.mean(NMACVMD);
STD_NAMC_VMD=statistics.stdev(NMACVMD);
print("Mean NMAC VMD: ", mean_NMAC_VMD)
print("STD NMAC VMD: ", STD_NAMC_VMD)


# EVALUATION OF CAUTION ALERTS--------------------------------

caution_early=0;
caution_average=0;
caution_late=0;
num_caution = [float(number.strip()) for number in cautionNumber]

for i in range(len(cautionFirstType)):
    if num_caution[i]>1:
        if cautionType[i] == ' earlyAlert':
            caution_early = caution_early + 1;
            if cautionFirstType[i] == ' earlyAlert':
                caution_early = caution_early + 1;
            elif cautionFirstType[i] == ' averageAlert':
                caution_average = caution_average + 1;
            elif cautionFirstType[i] == ' lateAlert':
                caution_late = caution_late + 1;
        elif cautionType[i] == ' averageAlert':
            caution_average = caution_average + 1;
            if cautionFirstType[i] == ' earlyAlert':
                caution_early = caution_early + 1;
            elif cautionFirstType[i] == ' averageAlert':
                caution_average = caution_average + 1;
            elif cautionFirstType[i] == ' lateAlert':
                caution_late = caution_late + 1;
        elif cautionType[i] == ' lateAlert':
            caution_late = caution_late + 1;
            if cautionFirstType[i] == ' earlyAlert':
                caution_early = caution_early + 1;
            elif cautionFirstType[i] == ' averageAlert':
                caution_average = caution_average + 1;
            elif cautionFirstType[i] == ' lateAlert':
                caution_late = caution_late + 1;
    else:
        if cautionFirstType[i] == ' earlyAlert':
            caution_early = caution_early + 1;
        elif cautionFirstType[i] == ' averageAlert':
            caution_average = caution_average + 1;
        elif cautionFirstType[i] == ' lateAlert':
            caution_late = caution_late + 1;
        else:
            continue

print("Total early caution: ", caution_early)
print("Total average caution: ", caution_average)
print("Total late caution: ", caution_late)

first_Caution_Time = [int(number.strip()) for number in cautionFirstTime]
first_Caution_Time=[value for value in first_Caution_Time if value != -1]
mean_firstCaution_Time=statistics.mean(first_Caution_Time);
STD_firstCaution_Time=statistics.stdev(first_Caution_Time);
print("Mean first Caution Time: ", mean_firstCaution_Time)
print("STD first Caution Time: ", STD_firstCaution_Time)

first_Caution_ttHAZ = [float(number.strip()) for number in cautionFirstTtHaz]
first_Caution_ttHAZ = [value for value in first_Caution_ttHAZ if value != -1]
first_Caution_ttHAZ = [value for value in first_Caution_ttHAZ if value != 0]
mean_firstCaution_ttHAZ = statistics.mean(first_Caution_ttHAZ);
STD_firstCaution_ttHAZ = statistics.stdev(first_Caution_ttHAZ);
print("Mean first Caution ttHAZ: ", mean_firstCaution_ttHAZ)
print("STD first Caution ttHAZ: ", STD_firstCaution_ttHAZ)

first_Caution_EndTime = [float(number.strip()) for number in cautionFirstEndTime]
first_Caution_EndTime = [value for value in first_Caution_EndTime if value != 0]
first_Caution_duration = np.array(first_Caution_EndTime)-np.array(first_Caution_Time);
mean_firstCaution_duration = statistics.mean(first_Caution_duration);
STD_firstCaution_duration = statistics.stdev(first_Caution_duration);
print("Mean first Caution duration: ", mean_firstCaution_duration)
print("STD first Caution duration: ", STD_firstCaution_duration)


Caution_Time = [int(number.strip()) for number in cautionTime]
Caution_Time = [value for value in Caution_Time if value != -1]
Caution_EndTime = [float(number.strip()) for number in cautionEndTime]
Caution_EndTime = [value for value in Caution_EndTime if value != 0]
Caution_duration = np.array(Caution_EndTime)-np.array(Caution_Time);

mean_Caution_duration=statistics.mean(Caution_duration);
STD_Caution_duration=statistics.stdev(Caution_duration);
print("Mean Caution duration: ", mean_Caution_duration)
print("STD Caution duration: ", STD_Caution_duration)

Caution_ttHAZ = [float(number.strip()) for number in cautionTtHaz]
Caution_ttHAZ = [value for value in Caution_ttHAZ if value != -1]
Caution_ttHAZ = [value for value in Caution_ttHAZ if value != 0]
mean_Caution_ttHAZ = statistics.mean(Caution_ttHAZ);
STD_Caution_ttHAZ = statistics.stdev(Caution_ttHAZ);
print("Mean Caution ttHAZ: ", mean_Caution_ttHAZ)
print("STD Caution ttHAZ: ", STD_Caution_ttHAZ)

# Calculate the PDF
TCaution_Time = [int(number.strip()) for number in cautionFirstTime]
cpaTime = [int(number.strip()) for number in cpaTime]

Caution_to_CPA=[0]*len(cpaTime);
for i in range(len(cpaTime)):
    if TCaution_Time[i]!=-1 and cpaTime[i]!=0:
        Caution_to_CPA[i]= cpaTime[i]-TCaution_Time[i]
    else:
        continue

Caution_to_CPA = [value for value in Caution_to_CPA if value != 0]
Caution_to_CPA = [value for value in Caution_to_CPA if value != -1]

mean_Caution_to_CPA = statistics.mean(Caution_to_CPA);
STD_Caution_to_CPA = statistics.stdev(Caution_to_CPA);

Caution_to_CPA_sorted = sorted(Caution_to_CPA,reverse=True)
PDF_Caution_to_CPA = norm.pdf(Caution_to_CPA_sorted, mean_Caution_to_CPA, STD_Caution_to_CPA)
Caution_duration_sorted = sorted(Caution_duration,reverse=True)
PDF_caution_duration = norm.pdf(Caution_duration_sorted, mean_Caution_duration, STD_Caution_duration)
Caution_ttHAZ_sorted = sorted(Caution_ttHAZ,reverse=True)
PDF_caution_ttHAZ = norm.pdf(Caution_ttHAZ_sorted, mean_Caution_ttHAZ, STD_Caution_ttHAZ)
# Plot the Caution PDF
plt.figure(figsize=(8, 6))
plt.plot(Caution_duration_sorted, PDF_caution_duration, label=f'Caution Duration\nMean={mean_Caution_duration:.4f}, Std={STD_Caution_duration:.4f}')
plt.plot(Caution_ttHAZ_sorted, PDF_caution_ttHAZ, label=f'Predicted time to LoWC\nMean={mean_Caution_ttHAZ:.4f}, Std={STD_Caution_ttHAZ:.4f}')
plt.plot(Caution_to_CPA_sorted, PDF_Caution_to_CPA, label=f'Caution time to CPA\nMean={mean_Caution_to_CPA:.4f}, Std={STD_Caution_to_CPA:.4f}')
plt.title('WCV Time Analysis Probability Density Function (PDF) for Caution Alerts')
plt.xlabel('Time')
plt.ylabel('PDF')
plt.legend()
plt.grid(True)
plt.show()

# Calculate the CDF
CDF_Caution_to_CPA = norm.sf(Caution_to_CPA_sorted, mean_Caution_to_CPA, STD_Caution_to_CPA)
CDF_caution_duration = norm.sf(Caution_duration_sorted, mean_Caution_duration, STD_Caution_duration)
CDF_caution_ttHAZ = norm.sf(Caution_ttHAZ_sorted, mean_Caution_ttHAZ, STD_Caution_ttHAZ)
# Plot the Caution PDF
plt.figure(figsize=(8, 6))
plt.plot(Caution_duration_sorted, CDF_caution_duration, label=f'Caution Duration\nMean={mean_Caution_duration:.4f}, Std={STD_Caution_duration:.4f}')
plt.plot(Caution_ttHAZ_sorted, CDF_caution_ttHAZ, label=f'Predicted time to LoWC\nMean={mean_Caution_ttHAZ:.4f}, Std={STD_Caution_ttHAZ:.4f}')
plt.plot(Caution_to_CPA_sorted, CDF_Caution_to_CPA, label=f'Caution time to CPA\nMean={mean_Caution_to_CPA:.4f}, Std={STD_Caution_to_CPA:.4f}')
plt.title('WCV Time Analysis Reverse Cumulative Distribution Function (CDF) for Caution Alerts')
plt.xlabel('Time')
plt.ylabel('CDF')
plt.legend()
plt.grid(True)

# EVALUATION OF PREVENTIVE ALERTS--------------------------------

prev_early = 0;
prev_average = 0;
prev_late = 0;
num_prev = [float(number.strip()) for number in prevNumber]

for i in range(len(prevFirstType)):
    if num_prev[i]>1:
        if prevType[i] == ' earlyAlert':
            prev_early = prev_early + 1;
            if prevFirstType[i] == ' earlyAlert':
                prev_early = prev_early + 1;
            elif prevFirstType[i] == ' averageAlert':
                prev_average = prev_average + 1;
            elif prevFirstType[i] == ' lateAlert':
                prev_late = prev_late + 1;
        elif prevType[i] == ' averageAlert':
            prev_average = prev_average + 1;
            if prevFirstType[i] == ' earlyAlert':
                prev_early = prev_early + 1;
            elif prevFirstType[i] == ' averageAlert':
                prev_average = prev_average + 1;
            elif prevFirstType[i] == ' lateAlert':
                prev_late = prev_late + 1;
        elif prevType[i] == ' lateAlert':
            prev_late = prev_late + 1;
            if prevFirstType[i] == ' earlyAlert':
                prev_early = prev_early + 1;
            elif prevFirstType[i] == ' averageAlert':
                prev_average = prev_average + 1;
            elif prevFirstType[i] == ' lateAlert':
                prev_late = prev_late + 1;
    else:
        if prevFirstType[i] == ' earlyAlert':
            prev_early = prev_early + 1;
        elif prevFirstType[i] == ' averageAlert':
            prev_average = prev_average + 1;
        elif prevFirstType[i] == ' lateAlert':
            prev_late = prev_late + 1;
        else:
            continue

print("Total early prev: ", prev_early)
print("Total average prev: ", prev_average)
print("Total late prev: ", prev_late)

first_prev_Time = [int(number.strip()) for number in prevFirstTime]
first_prev_Time = [value for value in first_prev_Time if value != -1]
mean_firstprev_Time = statistics.mean(first_prev_Time);
STD_firstprev_Time = statistics.stdev(first_prev_Time);
print("Mean first prev Time: ", mean_firstprev_Time)
print("STD first prev Time: ", STD_firstprev_Time)

first_prev_ttHAZ = [float(number.strip()) for number in prevFirstTtHaz]
first_prev_ttHAZ = [value for value in first_prev_ttHAZ if value != -1]
first_prev_ttHAZ = [value for value in first_prev_ttHAZ if value != 0]
mean_firstprev_ttHAZ = statistics.mean(first_prev_ttHAZ);
STD_firstprev_ttHAZ = statistics.stdev(first_prev_ttHAZ);
print("Mean first prev ttHAZ: ", mean_firstprev_ttHAZ)
print("STD first prev ttHAZ: ", STD_firstprev_ttHAZ)

first_prev_EndTime = [float(number.strip()) for number in prevFirstEndTime]
first_prev_EndTime = [value for value in first_prev_EndTime if value != 0]
first_prev_duration = np.array(first_prev_EndTime)-np.array(first_prev_Time);
mean_firstprev_duration = statistics.mean(first_prev_duration);
STD_firstprev_duration = statistics.stdev(first_prev_duration);
print("Mean first prev duration: ", mean_firstprev_duration)
print("STD first prev duration: ", STD_firstprev_duration)


prev_Time = [int(number.strip()) for number in prevTime]
prev_Time = [value for value in prev_Time if value != -1]
prev_EndTime = [float(number.strip()) for number in prevEndTime]
prev_EndTime = [value for value in prev_EndTime if value != 0]
prev_duration = np.array(prev_EndTime)-np.array(prev_Time);
mean_prev_duration = statistics.mean(prev_duration);
STD_prev_duration = statistics.stdev(prev_duration);
print("Mean prev duration: ", mean_prev_duration)
print("STD prev duration: ", STD_prev_duration)

prev_ttHAZ = [float(number.strip()) for number in prevTtHaz]
prev_ttHAZ = [value for value in prev_ttHAZ if value != -1]
prev_ttHAZ = [value for value in prev_ttHAZ if value != 0]
mean_prev_ttHAZ = statistics.mean(prev_ttHAZ);
STD_prev_ttHAZ = statistics.stdev(prev_ttHAZ);
print("Mean prev ttHAZ: ", mean_prev_ttHAZ)
print("STD prev ttHAZ: ", STD_prev_ttHAZ)

# Calculate the PDF
TPrev_Time = [int(number.strip()) for number in prevFirstTime]
Prev_to_CPA=[0]*len(cpaTime);
for i in range(len(cpaTime)):
    if TPrev_Time[i]!=-1 and cpaTime[i]!=0:
        Prev_to_CPA[i] = cpaTime[i]-TPrev_Time[i]
    else:
        continue
Prev_to_CPA = [abs(number) for number in Prev_to_CPA]
Prev_to_CPA = [value for value in Prev_to_CPA if value != 0]
Prev_to_CPA = [value for value in Prev_to_CPA if value != -1]
mean_Prev_to_CPA = statistics.mean(Prev_to_CPA);
STD_Prev_to_CPA = statistics.stdev(Prev_to_CPA);
print(prev_ttHAZ)
Prev_to_CPA_sorted = sorted(Prev_to_CPA, reverse=True)
PDF_Prev_to_CPA = norm.pdf(Prev_to_CPA_sorted, mean_Prev_to_CPA, STD_Prev_to_CPA)
prev_duration_sorted = sorted(prev_duration, reverse=True)
PDF_prev_duration = norm.pdf(prev_duration_sorted, mean_prev_duration, STD_prev_duration)
prev_ttHAZ_sorted = sorted(prev_ttHAZ, reverse=True)
PDF_prev_ttHAZ = norm.pdf(prev_ttHAZ_sorted, mean_prev_ttHAZ, STD_prev_ttHAZ)
# Plot the Preventive PDF
plt.figure(figsize=(8, 6))
plt.plot(prev_duration_sorted, PDF_prev_duration, label=f'Preventive Duration\nMean={mean_prev_duration:.4f}, Std={STD_prev_duration:.4f}')
plt.plot(prev_ttHAZ_sorted, PDF_prev_ttHAZ, label=f'Predicted time to LoWC\nMean={mean_prev_ttHAZ:.4f}, Std={STD_prev_ttHAZ:.4f}')
plt.plot(Prev_to_CPA_sorted, PDF_Prev_to_CPA, label=f'Preventive time to CPA\nMean={mean_Prev_to_CPA:.4f}, Std={STD_Prev_to_CPA:.4f}')
plt.title('WCV Time Analysis Probability Density Function (PDF) for Preventive Alerts')
plt.xlabel('Time')
plt.ylabel('PDF')
plt.legend()
plt.grid(True)
plt.show()

# Calculate the CDF
CDF_Prev_to_CPA = norm.sf(Prev_to_CPA_sorted, mean_Prev_to_CPA, STD_Prev_to_CPA)
CDF_prev_duration = norm.sf(prev_duration_sorted, mean_prev_duration, STD_prev_duration)
CDF_prev_ttHAZ = norm.sf(prev_ttHAZ_sorted, mean_prev_ttHAZ, STD_prev_ttHAZ)
# Plot the CDF
plt.figure(figsize=(8, 6))
plt.plot(prev_duration_sorted, CDF_prev_duration, label=f'Preventive Duration\nMean={mean_prev_duration:.4f}, Std={STD_prev_duration:.4f}')
plt.plot(prev_ttHAZ_sorted, CDF_prev_ttHAZ, label=f'Predicted time to LoWC\nMean={mean_prev_ttHAZ:.4f}, Std={STD_prev_ttHAZ:.4f}')
plt.plot(Prev_to_CPA_sorted, CDF_Prev_to_CPA, label=f'Preventive time to CPA\nMean={mean_Prev_to_CPA:.4f}, Std={STD_Prev_to_CPA:.4f}')
plt.title('WCV Time Analysis Reverse Cumulative Distribution Function (CDF) for Preventive Alerts')
plt.xlabel('Time')
plt.ylabel('CDF')
plt.legend()
plt.grid(True)
plt.show()

# EVALUATION OF WARNING ALERTS--------------------------------

warn_early=0;
warn_average=0;
warn_late=0;
num_warn = [float(number.strip()) for number in warnNumber]

for i in range(len(warnFirstType)):
    if num_warn[i]>1:
        if warnType[i] == ' earlyAlert':
            warn_early = warn_early + 1;
            if warnFirstType[i] == ' earlyAlert':
                warn_early = warn_early + 1;
            elif warnFirstType[i] == ' averageAlert':
                warn_average = warn_average + 1;
            elif warnFirstType[i] == ' lateIn':
                warn_late = warn_late + 1;
        elif warnType[i] == ' averageAlert':
            warn_average = warn_average + 1;
            if warnFirstType[i] == ' earlyAlert':
                warn_early = warn_early + 1;
            elif warnFirstType[i] == ' averageAlert':
                warn_average = warn_average + 1;
            elif warnFirstType[i] == ' lateIn':
                warn_late = warn_late + 1;
        elif warnType[i] == ' lateIn':
            warn_late = warn_late + 1;
            if warnFirstType[i] == ' earlyAlert':
                warn_early = warn_early + 1;
            elif warnFirstType[i] == ' averageAlert':
                warn_average = warn_average + 1;
            elif warnFirstType[i] == ' lateIn':
                warn_late = warn_late + 1;
    else:
        if warnFirstType[i] == ' earlyAlert':
            warn_early = warn_early + 1;
        elif warnFirstType[i] == ' averageAlert':
            warn_average = warn_average + 1;
        elif warnFirstType[i] == ' lateIn':
            warn_late = warn_late + 1;
        else:
            continue

print("Total early warn: ", warn_early)
print("Total average warn: ", warn_average)
print("Total late warn: ", warn_late)

first_warn_Time = [int(number.strip()) for number in warnFirstTime]
first_warn_Time = [value for value in first_warn_Time if value != -1]
mean_firstwarn_Time = statistics.mean(first_warn_Time);
STD_firstwarn_Time = statistics.stdev(first_warn_Time);
print("Mean first warn Time: ", mean_firstwarn_Time)
print("STD first warn Time: ", STD_firstwarn_Time)

first_warn_ttHAZ = [float(number.strip()) for number in warnFirstTtHaz]
first_warn_ttHAZ = [value for value in first_warn_ttHAZ if value != -1]
first_warn_ttHAZ = [value for value in first_warn_ttHAZ if value != 0]
mean_firstwarn_ttHAZ = statistics.mean(first_warn_ttHAZ);
STD_firstwarn_ttHAZ = statistics.stdev(first_warn_ttHAZ);
print("Mean first warn ttHAZ: ", mean_firstwarn_ttHAZ)
print("STD first warn ttHAZ: ", STD_firstwarn_ttHAZ)

first_warn_EndTime = [float(number.strip()) for number in warnFirstEndTime]
first_warn_EndTime = [value for value in first_warn_EndTime if value != 0]
first_warn_duration = np.array(first_warn_EndTime)-np.array(first_warn_Time);
mean_firstwarn_duration = statistics.mean(first_warn_duration);
STD_firstwarn_duration = statistics.stdev(first_warn_duration);
print("Mean first warn duration: ", mean_firstwarn_duration)
print("STD first warn duration: ", STD_firstwarn_duration)


warn_Time = [int(number.strip()) for number in warnTime]
warn_Time = [value for value in warn_Time if value != -1]
warn_EndTime = [float(number.strip()) for number in warnEndTime]
warn_EndTime = [value for value in warn_EndTime if value != 0]
warn_duration=abs(np.array(warn_EndTime)-np.array(warn_Time));
mean_warn_duration = statistics.mean(warn_duration);
STD_warn_duration = statistics.stdev(warn_duration);
print("Mean warn duration: ", mean_warn_duration)
print("STD warn duration: ", STD_warn_duration)

warn_ttHAZ = [float(number.strip()) for number in warnTtHaz]
warn_ttHAZ = [value for value in warn_ttHAZ if value != -1]
warn_ttHAZ = [value for value in warn_ttHAZ if value != 0]
mean_warn_ttHAZ = statistics.mean(warn_ttHAZ);
STD_warn_ttHAZ = statistics.stdev(warn_ttHAZ);
print("Mean warn ttHAZ: ", mean_warn_ttHAZ)
print("STD warn ttHAZ: ", STD_warn_ttHAZ)

# Calculate the PDF
TWarn_Time = [int(number.strip()) for number in warnFirstTime]
Warn_to_CPA=[0]*len(cpaTime);
for i in range(len(cpaTime)):
    if TWarn_Time[i]!=-1 and cpaTime[i]!=0:
        Warn_to_CPA[i]= cpaTime[i]-TWarn_Time[i]
    else:
        continue

Warn_to_CPA = [value for value in Warn_to_CPA if value != 0]
Warn_to_CPA = [value for value in Warn_to_CPA if value != -1]
mean_Warn_to_CPA = statistics.mean(Warn_to_CPA);
STD_Warn_to_CPA = statistics.stdev(Warn_to_CPA);

Warn_to_CPA_sorted = sorted(Warn_to_CPA,reverse=True)
PDF_Warn_to_CPA = norm.pdf(Warn_to_CPA_sorted, mean_Warn_to_CPA, STD_Warn_to_CPA)
warn_duration_sorted = sorted(warn_duration,reverse=True)
PDF_warn_duration = norm.pdf(warn_duration_sorted, mean_warn_duration, STD_warn_duration)
warn_ttHAZ_sorted = sorted(warn_ttHAZ,reverse=True)
PDF_warn_ttHAZ = norm.pdf(warn_ttHAZ_sorted, mean_warn_ttHAZ, STD_warn_ttHAZ)
# Plot the Warning PDF
plt.figure(figsize=(8, 6))
plt.plot(warn_duration_sorted, PDF_warn_duration, label=f'Warning Duration\nMean={mean_warn_duration:.4f}, Std={STD_warn_duration:.4f}')
plt.plot(warn_ttHAZ_sorted, PDF_warn_ttHAZ, label=f'Predicted time to LoWC\nMean={mean_warn_ttHAZ:.4f}, Std={STD_warn_ttHAZ:.4f}')
plt.plot(Warn_to_CPA_sorted, PDF_Warn_to_CPA, label=f'Warning time to CPA\nMean={mean_Warn_to_CPA:.4f}, Std={STD_Warn_to_CPA:.4f}')
plt.title('WCV Time Analysis Probability Density Function (PDF) for Warning Alerts')
plt.xlabel('Time')
plt.ylabel('PDF')
plt.legend()
plt.grid(True)
plt.show()

# Calculate the CDF
CDF_Warn_to_CPA = norm.sf(Warn_to_CPA_sorted, mean_Warn_to_CPA, STD_Warn_to_CPA)
CDF_warn_duration = norm.sf(warn_duration_sorted, mean_warn_duration, STD_warn_duration)
CDF_warn_ttHAZ = norm.sf(warn_ttHAZ_sorted, mean_warn_ttHAZ, STD_warn_ttHAZ)
# Plot the PDF
plt.figure(figsize=(8, 6))
plt.plot(warn_duration_sorted, CDF_warn_duration, label=f'Warning Duration\nMean={mean_warn_duration:.4f}, Std={STD_warn_duration:.4f}')
plt.plot(warn_ttHAZ_sorted, CDF_warn_ttHAZ, label=f'Predicted time to LoWC\nMean={mean_warn_ttHAZ:.4f}, Std={STD_warn_ttHAZ:.4f}')
plt.plot(Warn_to_CPA_sorted, CDF_Warn_to_CPA, label=f'Warning time to CPA\nMean={mean_Warn_to_CPA:.4f}, Std={STD_Warn_to_CPA:.4f}')
plt.title('WCV Time Analysis Reverse Cumulative Distribution Function (CDF) for Warning Alerts')
plt.xlabel('Time')
plt.ylabel('CDF')
plt.legend()
plt.grid(True)
plt.show()
print(warn_duration)

# EVALUATION OF ALERTS THAT LEAD TO A LoWC--------------------------------
print("Total LoWC", total_LoWC)


first_haz_Time = [int(hazFirstTime[idx].strip()) for idx in hazViolatedIdx]
mean_firsthaz_Time=statistics.mean(first_haz_Time);
STD_firsthaz_Time=statistics.stdev(first_haz_Time);
print("Mean haz first Time: ", mean_firsthaz_Time)
print("STD haz first Time: ", STD_firsthaz_Time)


worst_haz_Time = [int(haz_SLoWC_time[idx].strip()) for idx in hazViolatedIdx]
mean_worsthaz_Time=statistics.mean(worst_haz_Time);
STD_worsthaz_Time=statistics.stdev(worst_haz_Time);
print("Mean worst haz Time: ", mean_worsthaz_Time)
print("STD worst haz Time: ", STD_worsthaz_Time)


hazFirstTimeSLoWC = [float(hazFirstTimeSLoWC[idx].strip()) for idx in hazViolatedIdx]
mean_firstSLoWC=statistics.mean(hazFirstTimeSLoWC);
STD_firstSLoWC=statistics.stdev(hazFirstTimeSLoWC);
print("Mean first SLoWC: ", mean_firstSLoWC)
print("STD first SLoWC: ", STD_firstSLoWC)


hazSLoWC = [float(hazSLoWC[idx].strip()) for idx in hazViolatedIdx]
mean_SLoWC=statistics.mean(hazSLoWC);
STD_SLoWC=statistics.stdev(hazSLoWC);
print("Mean SLoWC: ", mean_SLoWC)
print("STD SLoWC: ", STD_SLoWC)


hazFirstTimeRangePen = [float(hazFirstTimeRangePen[idx].strip()) for idx in hazViolatedIdx]
mean_firsthazRangePen=statistics.mean(hazFirstTimeRangePen);
STD_firsthazRangePen=statistics.stdev(hazFirstTimeRangePen);
print("Mean first haz Range Pen: ", mean_firsthazRangePen)
print("STD first haz Range Pen: ", STD_firsthazRangePen)


hazRangePen = [float(hazRangePen[idx].strip()) for idx in hazViolatedIdx]
mean_hazRangePen=statistics.mean(hazRangePen);
STD_hazRangePen=statistics.stdev(hazRangePen);
print("Mean haz Range Pen: ", mean_hazRangePen)
print("STD haz Range Pen: ", STD_hazRangePen)


hazFirstTimeHMDPen = [float(hazFirstTimeHMDPen[idx].strip()) for idx in hazViolatedIdx]
mean_firsthazHMDPen=statistics.mean(hazFirstTimeHMDPen);
STD_firsthazHMDPen=statistics.stdev(hazFirstTimeHMDPen);
print("Mean first haz HMDPen: ", mean_firsthazHMDPen)
print("STD first haz HMDPen: ", STD_firsthazHMDPen)


hazFirstTimeVertPen = [float(hazFirstTimeVertPen[idx].strip()) for idx in hazViolatedIdx]
mean_firsthazVertPen=statistics.mean(hazFirstTimeVertPen);
STD_firsthazVertPen=statistics.stdev(hazFirstTimeVertPen);
print("Mean first haz VertPen: ", mean_firsthazVertPen)
print("STD first haz VertPen: ", STD_firsthazVertPen)


hazFirstTimeTcpa = [float(hazFirstTcpa[idx].strip()) for idx in hazViolatedIdx]
mean_firsthazTcpa=statistics.mean(hazFirstTimeTcpa);
STD_firsthazTcpa=statistics.stdev(hazFirstTimeTcpa);
print("Mean first haz Tcpa: ", mean_firsthazTcpa)
print("STD first haz Tcpa: ", STD_firsthazTcpa)


hazFirstTimeDcpa = [float(hazFirstDcpa[idx].strip()) for idx in hazViolatedIdx]
mean_firsthazDcpa=statistics.mean(hazFirstTimeDcpa);
STD_firsthazDcpa=statistics.stdev(hazFirstTimeDcpa);
print("Mean first haz Dcpa: ", mean_firsthazDcpa)
print("STD first haz Dcpa: ", STD_firsthazDcpa)


hazHMDPen = [float(hazHMDPen[idx].strip()) for idx in hazViolatedIdx]
mean_hazHMDPen=statistics.mean(hazHMDPen);
STD_hazHMDPen=statistics.stdev(hazHMDPen);
print("Mean haz HMDPen: ", mean_hazHMDPen)
print("STD haz HMDPen: ", STD_hazHMDPen)


hazVertPen = [float(hazVertPen[idx].strip()) for idx in hazViolatedIdx]
mean_hazVertPen=statistics.mean(hazVertPen);
STD_hazVertPen=statistics.stdev(hazVertPen);
print("Mean haz VertPen: ", mean_hazVertPen)
print("STD haz VertPen: ", STD_hazVertPen)


hazTcpa = [float(hazTcpa[idx].strip()) for idx in hazViolatedIdx]
mean_hazTcpa=statistics.mean(hazTcpa);
STD_hazTcpa=statistics.stdev(hazTcpa);
print(hazTcpa)
print("Mean haz Tcpa: ", mean_hazTcpa)
print("STD haz Tcpa: ", STD_hazTcpa)


hazDcpa = [float(hazDcpa[idx].strip()) for idx in hazViolatedIdx]
mean_hazDcpa=statistics.mean(hazDcpa);
STD_hazDcpa=statistics.stdev(hazDcpa);
print("Mean haz Dcpa: ", mean_hazDcpa)
print("STD haz Dcpa: ", STD_hazDcpa)


hazFirstTimeRange = [float(hazFirstTimeRange[idx].strip()) for idx in hazViolatedIdx]
mean_firsthazRange=statistics.mean(hazFirstTimeRange);
STD_firsthazRange=statistics.stdev(hazFirstTimeRange);
print("Mean first haz Range: ", mean_firsthazRange)
print("STD first haz Range: ", STD_firsthazRange)


hazSLoWCTimeRange = [float(hazSLoWCTimeRange[idx].strip()) for idx in hazViolatedIdx]
mean_hazRange=statistics.mean(hazSLoWCTimeRange);
STD_hazRange=statistics.stdev(hazSLoWCTimeRange);
print("Mean haz Range: ", mean_hazRange)
print("STD haz Range: ", STD_hazRange)

# Calculate the PDF
LoWC_to_WSLoW_time = abs(np.array(worst_haz_Time)-np.array(first_haz_Time));
LoWC_to_WSLoW_time = sorted(prev_duration,reverse=True)
mean_LoWC_to_WSLoW_time = statistics.mean(LoWC_to_WSLoW_time);
STD_LoWC_to_WSLoW_time = statistics.stdev(LoWC_to_WSLoW_time);
print("Mean predicted LoWC to WSLoW time: ", mean_LoWC_to_WSLoW_time)
print("STD predicted LoWC to WSLoW time: ", STD_LoWC_to_WSLoW_time)
worst_haz_Time_sorted=sorted(worst_haz_Time,reverse=True)
haz_tcpa=sorted(hazFirstTimeTcpa,reverse=True)
PDF_hazSLoWtime = norm.pdf(worst_haz_Time_sorted, mean_worsthaz_Time, STD_worsthaz_Time)
PDF_LoWC_to_WSLoW_time = norm.pdf(prev_duration_sorted, mean_prev_duration, STD_prev_duration)
PDF_haz_tcpa = norm.pdf(haz_tcpa, mean_firsthazTcpa, STD_firsthazTcpa)
# Plot the Preventive PDF
plt.figure(figsize=(8, 6))
plt.plot(LoWC_to_WSLoW_time, PDF_LoWC_to_WSLoW_time, label=f'Predicted time LoWC to SLoWC\nMean={mean_LoWC_to_WSLoW_time:.4f}, Std={STD_LoWC_to_WSLoW_time:.4f}')
plt.plot(haz_tcpa, PDF_haz_tcpa, label=f'Predicted time LoWC to CPA\nMean={mean_firsthazTcpa:.4f}, Std={STD_firsthazTcpa:.4f}')
plt.plot(worst_haz_Time_sorted, PDF_hazSLoWtime, label=f'Predicted time LoWC \nMean={mean_worsthaz_Time:.4f}, Std={STD_worsthaz_Time:.4f}')
plt.title('WCV Time Analysis Probability Density Function (PDF) for LoWC')
plt.xlabel('Time')
plt.ylabel('PDF')
plt.legend()
plt.grid(True)
plt.show()

# Calculate the CDF
CDF_LoWC_to_WSLoW_time = norm.sf(LoWC_to_WSLoW_time, mean_LoWC_to_WSLoW_time, STD_LoWC_to_WSLoW_time)
CDF_haz_tcpa = norm.sf(haz_tcpa, mean_firsthazTcpa, STD_firsthazTcpa)
CDF_hazSLoWtime = norm.sf(worst_haz_Time_sorted, mean_worsthaz_Time, STD_worsthaz_Time)
# Plot the Caution PDF
plt.figure(figsize=(8, 6))
plt.plot(LoWC_to_WSLoW_time, CDF_LoWC_to_WSLoW_time, label=f'Predicted time LoWC to WLoWC\nMean={mean_LoWC_to_WSLoW_time:.4f}, Std={STD_LoWC_to_WSLoW_time:.4f}')
plt.plot(haz_tcpa, CDF_haz_tcpa, label=f'Predicted time LoWC to CPA\nMean={mean_firsthazTcpa:.4f}, Std={STD_firsthazTcpa:.4f}')
plt.plot(worst_haz_Time_sorted, CDF_hazSLoWtime, label=f'Predicted time LoWC \nMean={mean_worsthaz_Time:.4f}, Std={STD_worsthaz_Time:.4f}')
plt.title('WCV Time Analysis Reverse Cumulative Distribution Function (CDF) for LoWC')
plt.xlabel('Time')
plt.ylabel('CDF')
plt.legend()
plt.grid(True)
plt.show()




