import os, shutil
import sys
import numpy as np
import pandas as pd
from time import time
import argparse
import csv
from DEG_Encounter import Parameters as p
from pathlib import Path
from RollingWindows import RollingWindow as rw
from Encounter_Distribution import Encounter_Parameters as ep
from Metrics import Enc_Alert_Measuring as mea, Alert_Measuring as am
from Encounter import encounter as enc
from Encounter import position as pos
from Encounter import aircraft_class as cls

#from DEG_Encounter import RightOfWay as rightofway


df_columns = [
        "Seed",
        "VMD",
        "HMD",
        "Approach Angle",
        "AC1 Class Name",
        "AC1 Class Idx",
        "AC1 Horizontal Mode",
        "AC1 Vertical Mode",
        "AC1 Speed Mode",
        "AC1 NMAC Idx",
        "AC1 Time Before NMAC",
        "AC1 Time After NMAC",
        "AC1 Heading",
        "AC1 Speed",
        "AC1 Altitude",
        "AC1 Vertical Speed",
        "AC2 Class Name",
        "AC2 Class Idx",
        "AC2 Horizontal Mode",
        "AC2 Vertical Mode",
        "AC2 Speed Mode",
        "AC2 NMAC Idx",
        "AC2 Time Before NMAC",
        "AC2 Time After NMAC",
        "AC2 Heading",
        "AC2 Speed",
        "AC2 Altitude",
        "AC2 Vertical Speed",
        "actualHMD",
        "Lowest CPA Idx",
        "lowestCPAtime"
]


df_dtype = {
    "Seed": np.int32,
    "VMD": np.float64,
    "HMD": np.float64,
    "Approach Angle": np.float64,
    "AC1 Class Name": str,
    "AC1 Class Idx": np.int32,
    "AC1 Horizontal Mode": str,
    "AC1 Vertical Mode": str,
    "AC1 Speed Mode": str,
    "AC1 NMAC Idx": np.int32,
    "AC1 Time Before NMAC": np.float64,
    "AC1 Time After NMAC": np.float64,
    "AC1 Heading": np.float64,
    "AC1 Speed": np.float64,
    "AC1 Altitude": np.float64,
    "AC1 Vertical Speed": np.float64,
    "AC2 Class Name": str,
    "AC2 Class Idx": np.int32,
    "AC2 Horizontal Mode": str,
    "AC2 Vertical Mode": str,
    "AC2 Speed Mode": str,
    "AC2 NMAC Idx": np.int32,
    "AC2 Time Before NMAC": np.float64,
    "AC2 Time After NMAC": np.float64,
    "AC2 Heading": np.float64,
    "AC2 Speed": np.float64,
    "AC2 Altitude": np.float64,
    "AC2 Vertical Speed": np.float64,
    "actualHMD": np.float64,
    "Lowest CPA Idx": np.int32,
    "lowestCPAtime": np.float64
}


trj_dtype = {
        "index":  np.int32,
        "time": np.float64,
        "aircraft":  np.int32,
        "x-position": np.float64,
        "y-position": np.float64,
        "altitude": np.float64,
        "x-speed": np.float64,
        "y-speed": np.float64,
        "z-speed": np.float64,
}


trj_columns = [
        "index",
        "aircraft",
        "time",
        "x-position",
        "y-position",
        "altitude",
        "x-speed",
        "y-speed",
        "z-speed",
]

def writeTotxt(result_enc_file,arcf1_eam,arcf2_eam):
    result_enc_file.write(str(enc_number))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf1_eam.HMDPen))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf1_eam.RangePen))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf1_eam.SLoWC))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf1_eam.VertPen))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf1_eam.WSR))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf1_eam.active_conflict))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf1_eam.haz_dcpa))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf1_eam.haz_tcpa))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf1_eam.haz_violated))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf1_eam.min_slat_range))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf1_eam.nmac_violated))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf1_eam.nmac_time))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf2_eam.HMDPen))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf2_eam.RangePen))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf2_eam.SLoWC))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf2_eam.VertPen))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf2_eam.WSR))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf2_eam.active_conflict))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf2_eam.haz_dcpa))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf2_eam.haz_tcpa))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf2_eam.haz_violated))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf2_eam.min_slat_range))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf2_eam.nmac_violated))
    result_enc_file.write(",")
    result_enc_file.write(str(arcf2_eam.nmac_time))
    result_enc_file.write('\n')

def getIntruderDataPath(rootFolder, enc):
    return os.path.join(rootFolder, "Encs_" + str(enc).zfill(8), "Intruder")


def getOwnshipDataPath(rootFolder, enc):
    return os.path.join(rootFolder, "Encs_" + str(enc).zfill(8), "Ownship")


def getCombinedDataPath(rootFolder, enc):
    return os.path.join(rootFolder, "Encs_" + str(enc).zfill(8))


def processEncounter(folder, encounter, enc_number, full_diagram_path):

    full_encounter_path = os.path.join(getCombinedDataPath(folder, enc_number), "DEG_TRJ_" + str(enc_number).zfill(8) + "_00.csv")

    if os.path.exists(full_encounter_path):

        start = time()
        print("Analyzing encounter: " + full_encounter_path)
        print("Reading a trajectory...")

        trj_df = pd.read_csv(full_encounter_path, delimiter=',', names=trj_columns, dtype=trj_dtype)

        prevRow1 = None # To test the heading generation proces
        prevRow2 = None
        rollWindow1 = rw.RollingWindow(150, [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1])
        rollWindow2 = rw.RollingWindow(150, [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1])

        for index, row in trj_df.iterrows():
            if row['aircraft'] == 1:
                position = pos.Position(row['aircraft'], row['time'], 0, 0, 0)
                position.setLocation(row['x-position'], row['y-position'], row['altitude'])
                position.assignDynamicsRaw(row['x-speed'], row['y-speed'], row['z-speed'])
#                encounter.addAircraft1Position(position)

                if rollWindow1.nSampled > 49:
                    # Resets the counter back to 0
                    conv_position = rollWindow1.combine2(position)
                    encounter.addAircraft1Position(conv_position)

                    if index > 2:
                        print("Aircraft 1 - Time: " + str(row['time']))
                        position.checkDynamics(prevRow1['x-position'], prevRow1['y-position'], prevRow1['altitude'])

                rollWindow1.enqueue(position)

                prevRow1 = row

            elif row['aircraft'] == 2:
                position = pos.Position(row['aircraft'], row['time'], 0, 0, 0)
                position.setLocation(row['x-position'], row['y-position'], row['altitude'])
                position.assignDynamicsRaw(row['x-speed'], row['y-speed'], row['z-speed'])
#                encounter.addAircraft2Position(position)

                if rollWindow2.nSampled > 49:
                    # Resets the counter back to 0
                    conv_position = rollWindow2.combine2(position)
                    encounter.addAircraft2Position(conv_position)

                    if index > 2:
                        print("Aircraft 2 - Time: " + str(row['time']))
                        position.checkDynamics(prevRow2['x-position'], prevRow2['y-position'], prevRow2['altitude'])

                rollWindow2.enqueue(position)

                prevRow2 = row


        encounter.encAlertMeasuringAicraft1 = arcf1_eam = mea.Enc_Alert_Measuring()
        encounter.encAlertMeasuringAicraft2 = arcf2_eam = mea.Enc_Alert_Measuring()

        #encounter.RoWDetermination(full_diagram_path, encounter_param, arcf1_eam, arcf2_eam,vdistlim,AreaRoW)
        #encounter.RoWFutur(full_diagram_path, encounter_param, arcf1_eam, arcf2_eam,AreaRoW,FutureTime,vdistlim)

        #encounter.computeDynamicsFactor()
        encounter.computeTurnDynamics()
        encounter.computeConflictsDEG(full_diagram_path, encounter_param, arcf1_eam, arcf2_eam)

        if encounter_param.debug and arcf1_eam.active_conflict:
            print("Reporting on aircraft 1:")
            arcf1_eam.report_Alert_Measuring()
        if encounter_param.debug and arcf2_eam.active_conflict:
            print("\nReporting on aircraft 2:")
            arcf2_eam.report_Alert_Measuring()

        if encounter_param.mustPlot:
            encounter.plotEncounterDEG(full_diagram_path)

        duration = (time() - start)
        print("Encounter_Distribution processed in: ", "{:.2f}".format(duration), " sec")

        return encounter
    else:
        return None



# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    parser = argparse.ArgumentParser(prog='DEG_Encounter_analyzer',
                                        usage='%(prog)s [options]',
                                        description='Process DEG collision encounter subsets')

    # Well-Clear volume definition
    # Standarized volumes that have priority over other parameters
    parser.add_argument('-wcv', '--wcvSet',
                        type=str,
                        choices=['05NM'],
                        default='05NM', required=False,
                        help='WCV volume definition: [05NM]')

    parser.add_argument('-s', '--subset',
                        type=str,
                        default='CAFE-Analysis', required=False,
                        help='Subset folder name for results')

    # Collision-Avoidance volume definition
    parser.add_argument('-cav', '--cavSet',
                        type=str,
                        choices=['05NM'],
                        default='05NM', required=False,
                        help='CAV volume definition: [05NM]')

    parser.add_argument('-d', '--debug', action='store_true')

    parser.add_argument('-p', '--plot', action='store_false')

    # Read arguments from the command line
    args = parser.parse_args()

    ##### INPUT DATA ######

    #ENCOUNTER_FOLDER = "F:\\DroneEncounters\\EncounterSet\\wcvTestSet100-Set1"
    #ENCOUNTER_FOLDER = "H:\\DroneEncounters\\EncounterSet\\sampleTestSet-Set2"
    ENCOUNTER_FOLDER = "E:\\Datasets\\DEG\\EncounterSet\\lowManTestSet"
    #ENCOUNTER_FOLDER = "E:\\Datasets\\DEG\\EncounterSet\\MAST-SET-H"

    #tests on arnau's comuputer
    #ENCOUNTER_FOLDER = "C:\\Users\\santi\\Desktop\\4B\\TFG\\TRAFFIC SIMULATOR\\DEG\\NEW GENERATED ENCOUNTERS\\Set2000-Set3-18102021-santi"
    #ENCOUNTER_FOLDER = "C:\\Users\\adri1\\OneDrive\\Desktop\\Grau Eng. Sist. Aeroespacials\\4B\\Tfg\\sampleTestSet"
    ENCOUNTER_FOLDER = os.path.join(ENCOUNTER_FOLDER)

    # Options: Select a certain encounter in order to debug
    select_enc_number = None
    # select_enc_number = 23

    Prefix_Name = "DEG_Analysis_Results"
    result_enc_filename = os.path.join(ENCOUNTER_FOLDER, Prefix_Name, "Encounter_Analysis_Results.csv")

    prov_file = Prefix_Name + "_Results.txt"
    diagram_subfolder = Prefix_Name + "_Diagrams"
    diagram_folder = os.path.join(ENCOUNTER_FOLDER, Prefix_Name)

    if not os.path.exists(ENCOUNTER_FOLDER):
        print("Root folder: ", ENCOUNTER_FOLDER, "\n")
        print("Does not exists, stopping...\n")
        exit(-1)


    # if os.path.exists(ENCOUNTER_FOLDER + "\\" + diagram_subfolder):
    #     print("Renaming folder...\n")
    #     if os.path.exists(ENCOUNTER_FOLDER + "\\" + diagram_subfolder + ".bak"):
    #         shutil.rmtree(ENCOUNTER_FOLDER + "\\" + diagram_subfolder + ".bak")
    #         # os.rmdir(resume_enc_folder + "\\" + diagram_subfolder + ".bak")
    #     os.rename(ENCOUNTER_FOLDER + "\\" + diagram_subfolder,
    #               ENCOUNTER_FOLDER + "\\" + diagram_subfolder + ".bak")
    #     os.mkdir(ENCOUNTER_FOLDER + "\\" + diagram_subfolder)
    # else:
    #     os.mkdir(ENCOUNTER_FOLDER + "\\" + diagram_subfolder)


    print("Setting encounter parameters to DEG")
    # Options: plot the encounters

    encounter_param = ep.Encounter_Parameters()
    encounter_param.set_DEG_parameters()
    encounter_param.set_DEG_Separation_parameters(60, 100)

    encounter_param.mustPlot = True
    if args.plot:
        encounter_param.mustPlot = True
    # Options: activate debug
    encounter_param.debug = True
    if args.debug:
        encounter_param.debug = True
    # Options: plot contours for the conflict sequence
    # encounter_param.plotContour = True
    encounter_param.plotContour = False


    resume_file = "Encounter_Summary_Output.DEG.csv"
    resume_enc_filename = os.path.join(ENCOUNTER_FOLDER, resume_file)
    encounterResume = am.Alert_Measuring()
    found = 0

    if os.path.exists(diagram_folder):
        print("Removing folder...\n")
        shutil.rmtree(diagram_folder)

    os.mkdir(diagram_folder)


    result_enc_file_header = True
    result_enc_file = open(result_enc_filename, "w")

    df = pd.read_csv(resume_enc_filename, delimiter=',', names=df_columns,header=None)

    for index, row in df.iterrows():
        enc_number = index
        #
        # Filter configured to be able to debug a certain trajectory
        if (select_enc_number != None) and (select_enc_number != int(enc_number)):
            continue

        aircraft1Class = 100 + row['AC1 Class Idx']
        aircraft2Class = 100 + row['AC2 Class Idx']
        print("\nProcessing encounter number: ", enc_number, " with Class1 aircraft: ", row['AC1 Class Name'], " with Class2 aircraft: ", row['AC2 Class Name'])
        encounter = enc.Encounter(enc_number, 0, "utm", aircraft1Class, aircraft2Class, "deg")
        encounter = processEncounter(ENCOUNTER_FOLDER, encounter, enc_number, diagram_folder)

        if result_enc_file_header == True:
            encounter.writeDEGEncounterHeader(result_enc_file)
            result_enc_file_header = False

        encounter.writeDEGEncounterResult(row, result_enc_file)

        encounterResume.addEncounter(encounter.aircraft1Class, encounter.aircraft2Class)
        encounterResume.addConflict(encounter.encAlertMeasuringAicraft1.active_conflict, encounter.encAlertMeasuringAicraft2.active_conflict)
        encounterResume.addNMAC(encounter.encAlertMeasuringAicraft1.nmac_violated, encounter.encAlertMeasuringAicraft2.nmac_violated)
        encounterResume.addLMV(encounter.encAlertMeasuringAicraft1.lmv_activated, encounter.encAlertMeasuringAicraft1.lmv_violated,
                               encounter.encAlertMeasuringAicraft2.lmv_activated, encounter.encAlertMeasuringAicraft2.lmv_violated)
        encounterResume.addLMVnoNMAC(
            encounter.encAlertMeasuringAicraft1.lmv_activated and not encounter.encAlertMeasuringAicraft1.nmac_violated,
            encounter.encAlertMeasuringAicraft1.lmv_violated and not encounter.encAlertMeasuringAicraft1.nmac_violated,
            encounter.encAlertMeasuringAicraft2.lmv_activated and not encounter.encAlertMeasuringAicraft2.nmac_violated,
            encounter.encAlertMeasuringAicraft2.lmv_violated and not encounter.encAlertMeasuringAicraft1.nmac_violated)

        encounterResume.addHazard(encounter.encAlertMeasuringAicraft1.haz_violated, encounter.encAlertMeasuringAicraft2.haz_violated)
        encounterResume.addWarning(encounter.encAlertMeasuringAicraft1.warn_type, encounter.encAlertMeasuringAicraft2.warn_type)
        encounterResume.addCorrective(encounter.encAlertMeasuringAicraft1.caution_type, encounter.encAlertMeasuringAicraft2.caution_type)
        encounterResume.addPreventive(encounter.encAlertMeasuringAicraft1.prev_type, encounter.encAlertMeasuringAicraft2.prev_type)

        if cls.isOwnshipRPAS(encounterResume.aircraft1Class):
            encounterResume.addTimming(encounter, encounter.encAlertMeasuringAicraft1)

        if cls.isOwnshipRPAS(encounterResume.aircraft2Class):
            encounterResume.addTimming(encounter, encounter.encAlertMeasuringAicraft2)

        found += 1
    print("Done.\n")

    sys.exit(0)
