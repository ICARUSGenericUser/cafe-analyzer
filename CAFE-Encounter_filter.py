# This is a sample Python script.
import os
import sys
import pandas as pd
import numpy as np
from datetime import datetime

from Encounter_Distribution import Encounter_Distribution_Parameters as ep
from Encounter_Distribution import Encounter_distribution as ed
from RWC import NMAC_parameters as nmac

#df_dtype = {
#        "Encounter_Distribution No.": int,
#        "Seed": int,
#        "Layer": int,
#        "VMD": np.float64,
#        "HMD": np.float64,
#        "Approach Angle": np.float64,
#        "Bearing from North": np.float64,
#        "Time before CPA": int,
#        "Time after CPA": int,
#        "AC1 Mode A": str,
#        "AC1 Callsign": str,
#        "AC1 Class": int,
#        "AC1 Controlled?": bool,
#        "AC1 Altitude": np.float64,
#        "AC1 Vertical": np.float64,
#        "AC1 Speed": np.float64,
#        "AC1 Acceleration": np.float64,
#        "AC1 Heading": np.float64,
#        "AC1 Turn": np.float64,
#        "AC2 Mode A": str,
#        "AC2 Callsign": str,
#        "AC2 Class": int,
#        "AC2 Controlled?": bool,
#        "AC2 Altitude": np.float64,
#        "AC2 Vertical": np.float64,
#        "AC2 Speed": np.float64,
#        "AC2 Acceleration": np.float64,
#        "AC2 Heading": np.float64,
#        "AC2 Turn": np.float64,
#}

df_dtype = {
        "Encounter No.": np.float64,
        "Seed": np.float64,
        "Layer": np.float64,
        "VMD": np.float64,
        "HMD": np.float64,
        "Approach Angle": np.float64,
        "Bearing from North": np.float64,
        "Time before CPA": np.float64,
        "Time after CPA": np.float64,
        "AC1 Mode A": str,
        "AC1 Callsign": str,
        "AC1 Class": np.float64,
        "AC1 Controlled?": bool,
        "AC1 Altitude": np.float64,
        "AC1 Vertical Rate": np.float64,
        "AC1 Speed": np.float64,
        "AC1 Acceleration": np.float64,
        "AC1 Heading": np.float64,
        "AC1 Turn": np.float64,
        "AC2 Mode A": str,
        "AC2 Callsign": str,
        "AC2 Class": np.float64,
        "AC2 Controlled?": bool,
        "AC2 Altitude": np.float64,
        "AC2 Vertical Rate": np.float64,
        "AC2 Speed": np.float64,
        "AC2 Acceleration": np.float64,
        "AC2 Heading": np.float64,
        "AC2 Turn": np.float64,
}

highSpeedLimit = 250
medSpeedLimit = 200
lowSpeedLimit = 100


def processEncounterChunck(df, resume_file, target_path, filterOptions, enc_distribution):

    NMACparameters = nmac.NMAC_parameters(0, 0)
    NMACparameters.setStandard()

    #highSet = ((df['AC1 Speed'] > medSpeedLimit) and (df['AC1 Speed'] <= highSpeedLimit)) or (
    #            (df['AC2 Speed'] > medSpeedLimit) and (df['AC2 Speed'] <= highSpeedLimit))
    # highSet = ((df['AC1 Speed'] > medSpeedLimit) & (df['AC1 Speed'] <= highSpeedLimit)) & \
    #           ((df['AC2 Speed'] > medSpeedLimit) & (df['AC2 Speed'] <= highSpeedLimit))
    #
    # midSet = ((df['AC1 Speed'] > lowSpeedLimit) & (df['AC1 Speed'] <= medSpeedLimit)) & \
    #          ((df['AC2 Speed'] > lowSpeedLimit) & (df['AC2 Speed'] <= medSpeedLimit))
    #
    # lowSet = ((df['AC1 Speed'] > 0) & (df['AC1 Speed'] <= lowSpeedLimit)) & \
    #         ((df['AC2 Speed'] > 0) & (df['AC2 Speed'] <= lowSpeedLimit))


    urclearedRPAS_Set = (((df['AC1 Class'] == 10) | (df['AC1 Class'] == 11) | (df['AC1 Class'] == 12) | (df['AC1 Class'] == 15)) | \
                ((df['AC2 Class'] == 10) | (df['AC2 Class'] == 11) | (df['AC2 Class'] == 12) | (df['AC2 Class'] == 15)))

    urclearedFastRPAS_Set = ((((df['AC1 Class'] == 10) | (df['AC1 Class'] == 11) | (df['AC1 Class'] == 12) | (df['AC1 Class'] == 15)) & (df['AC1 Speed'] > lowSpeedLimit)) | \
                (((df['AC2 Class'] == 10) | (df['AC2 Class'] == 11) | (df['AC2 Class'] == 12) | (df['AC2 Class'] == 15))  & (df['AC2 Speed'] > lowSpeedLimit)))

    urclearedSlowRPAS_Set = ((((df['AC1 Class'] == 10) | (df['AC1 Class'] == 11) | (df['AC1 Class'] == 12) | (df['AC1 Class'] == 15)) & (df['AC1 Speed'] <= lowSpeedLimit)) | \
                (((df['AC2 Class'] == 10) | (df['AC2 Class'] == 11) | (df['AC2 Class'] == 12) | (df['AC2 Class'] == 15))  & (df['AC2 Speed'] <= lowSpeedLimit)))

    urcleared_acas_RPAS_Set = ((((df['AC1 Class'] == 10) | (df['AC1 Class'] == 11) | (df['AC1 Class'] == 12) | (df['AC1 Class'] == 15)) & \
                                 ((df['AC2 Class'] == 12) | (df['AC2 Class'] == 13) | (df['AC2 Class'] == 14) | (df['AC2 Class'] == 15))) | \
                              (((df['AC2 Class'] == 10) | (df['AC2 Class'] == 11) | (df['AC2 Class'] == 12) | (df['AC2 Class'] == 15)) & \
                               ((df['AC1 Class'] == 12) | (df['AC1 Class'] == 13) | (df['AC1 Class'] == 14) | (df['AC2 Class'] == 15))))

    urcleared_modeS_RPAS_Set = ((((df['AC1 Class'] == 10) | (df['AC1 Class'] == 11) | (df['AC1 Class'] == 12) | (df['AC1 Class'] == 15)) & \
                                 ((df['AC2 Class'] == 10) | (df['AC2 Class'] == 11) | (df['AC2 Class'] == 12) | (df['AC2 Class'] == 15))) | \
                              (((df['AC2 Class'] == 10) | (df['AC2 Class'] == 11) | (df['AC2 Class'] == 12) | (df['AC2 Class'] == 15)) & \
                               ((df['AC1 Class'] == 10) | (df['AC1 Class'] == 11) | (df['AC1 Class'] == 12) | (df['AC2 Class'] == 15))))


    acasRPAS_Set = (((df['AC1 Class'] == 12) | (df['AC1 Class'] == 13) | (df['AC1 Class'] == 14) | (df['AC1 Class'] == 15)) | \
                ((df['AC2 Class'] == 12) | (df['AC2 Class'] == 13) | (df['AC2 Class'] == 14) | (df['AC2 Class'] == 15)))

    acasManned_Set = ((((df['AC1 Class'] >= 3) & (df['AC1 Class'] <= 8)) | (df['AC1 Class'] == 15) | (df['AC1 Class'] == 19)) | \
                    (((df['AC2 Class'] >= 3) & (df['AC2 Class'] <= 8)) | (df['AC2 Class'] == 15) | (df['AC2 Class'] == 19)))

    modeSManned_Set = (((df['AC1 Class'] == 2) | (df['AC1 Class'] == 9)) | \
                    ((df['AC2 Class'] == 2) | (df['AC2 Class'] == 9)))

    modeSRPAS_Set = (((df['AC1 Class'] == 10) | (df['AC1 Class'] == 11) | (df['AC1 Class'] == 12) | (df['AC1 Class'] == 15)) | \
                ((df['AC2 Class'] == 10) | (df['AC2 Class'] == 11) | (df['AC2 Class'] == 12) | (df['AC2 Class'] == 15)))

    nonCoopManned_Set = (((df['AC1 Class'] == 1) | (df['AC1 Class'] == 2) | (df['AC1 Class'] == 20)) | \
                    ((df['AC2 Class'] == 1) | (df['AC2 Class'] == 2) | (df['AC2 Class'] == 20)))

    lowerAirspace_Set = ((df['AC1 Altitude'] <= 10000) | (df['AC2 Altitude'] <= 10000))

    higherAirspace_Set = ((df['AC1 Altitude'] > 10000) & (df['AC2 Altitude'] > 10000))

    lowspeed_Set = ((df['AC1 Speed'] <= highSpeedLimit) & (df['AC2 Speed'] <= highSpeedLimit))


    if filterOptions.IntruderType == "Other":
        if filterOptions.RPASSubset == "URCLEARED":
            df_filtered_Set = df.loc[urclearedRPAS_Set]
            print("\tRPAS URCLEARED encounters: ", len(df_filtered_Set.index))
        elif filterOptions.RPASSubset == "ACAS":
            df_filtered_Set = df.loc[acasRPAS_Set]
            print("\tRPAS ACAS encounters: ", len(df_filtered_Set.index))
        else:
            df_filtered_Set = df


        # "nonCoop" / "modeS" / "ACAS"
        if filterOptions.IntruderSubset == "nonCoop":
            df_filtered_Set = df_filtered_Set.loc[nonCoopManned_Set]
            print("\tIntruder nonCoop encounters: ", len(df_filtered_Set.index))
        elif filterOptions.IntruderSubset == "modeS":
            df_filtered_Set = df_filtered_Set.loc[modeSManned_Set]
            print("\tIntruder Mode-S encounters: ", len(df_filtered_Set.index))
        elif filterOptions.IntruderSubset == "ACAS":
            df_filtered_Set = df_filtered_Set.loc[acasManned_Set] # Need work here
            print("\tIndruder ACAS encounters: ", len(df_filtered_Set.index))

    elif filterOptions.IntruderType == "RPAS":
        if (filterOptions.RPASSubset == "URCLEARED") and (filterOptions.IntruderSubset == "ACAS"):
            df_filtered_Set = df.loc[urcleared_acas_RPAS_Set]
            print("\tRPAS ACAS encounters: ", len(df_filtered_Set.index))
        elif (filterOptions.RPASSubset == "URCLEARED") and (filterOptions.IntruderSubset == "modeS"):
            df_filtered_Set = df.loc[urcleared_modeS_RPAS_Set]
            print("\tRPAS modeS encounters: ", len(df_filtered_Set.index))
        elif (filterOptions.RPASSubset == "URCLEARED") and (filterOptions.IntruderSubset == "nonCoop"):
            print("\tRPAS nonCoop encounters does not exist. Exiting. ")
            exit(-1)


    if filterOptions.AltitudeSubset == "LOWER":
        df_filtered_Set = df_filtered_Set.loc[lowerAirspace_Set]
        print("\tLower altitude encounters: ", len(df_filtered_Set.index))
    elif filterOptions.AltitudeSubset == "HIGHER":
        df_filtered_Set = df_filtered_Set.loc[higherAirspace_Set]
        print("\tHigher altitude encounters: ", len(df_filtered_Set.index))

    if filterOptions.SpeedSubset == "SPEEDLIMIT":
        df_filtered_Set = df_filtered_Set.loc[lowspeed_Set]
        print("\tLower speed limit encounters: ", len(df_filtered_Set.index))

    if filterOptions.OwnSpeedSubset == "BELOW100":
        df_filtered_Set = df_filtered_Set.loc[urclearedSlowRPAS_Set]
        print("\tOwnship lower speed limit encounters: ", len(df_filtered_Set.index))
    elif filterOptions.OwnSpeedSubset == "ABOVE100":
        df_filtered_Set = df_filtered_Set.loc[urclearedFastRPAS_Set]
        print("\tOwnship higher speed limit encounters: ", len(df_filtered_Set.index))


    if len(df_filtered_Set.index) > 0:
        print("Dumping to CSV resume file...")
        df_filtered_Set.to_csv(resume_file, header = False, index = False, mode='a')

        idx = 0
        for index, row in df_filtered_Set.iterrows():

            found = False
            subfolder = 1
            while (subfolder <= MAX_ENCOUNTER_FOLDERS):

                full_encounter_path = target_path + "\\" + "FTD_1" + "\\" + "Encs_" + str(row['Encounter No.']).zfill(9) + ".ftd"
                #full_encounter_path = target_path + "\\" + "EU1_" + str(subfolder) + "\\" + "Encs_" + str(row['Encounter No.']).zfill(9) + ".eu1"

                #print("Checking Encounter_Distribution file: ", full_encounter_path)
                if os.path.exists(full_encounter_path):
                    idx += 1
                    #row.to_csv(resume_file, header=False, index=False, mode='a')

                    found = True
                    enc_distribution.recordVMD(row['VMD'])
                    enc_distribution.recordHMD(row['HMD'])
                    enc_distribution.recordVMDHMD(row['VMD'], row['HMD'])
                    enc_distribution.recordHSpeed(row['AC1 Speed'])
                    enc_distribution.recordHSpeed(row['AC2 Speed'])
                    enc_distribution.recordVSpeed(row['AC1 Vertical Rate'])
                    enc_distribution.recordVSpeed(row['AC2 Vertical Rate'])
                    enc_distribution.recordAAngle(row['Approach Angle'])
                    enc_distribution.recordAltitude(row['AC1 Altitude'])
                    enc_distribution.recordAltitude(row['AC2 Altitude'])
                    enc_distribution.recordTurnrate(row['AC1 Turn'])
                    enc_distribution.recordTurnrate(row['AC2 Turn'])
                    enc_distribution.recordAC1AC2(row['AC1 Class'], row['AC2 Class'])
                    enc_distribution.recordNMAC(row['VMD'], row['HMD'], row['AC1 Class'], row['AC2 Class'], NMACparameters)

                    #print("Recording encounter: ", str(row['Encounter No.']), "with AC1 Class: " + str(row['AC1 Class']) + " Mode: " + str(row['AC1 Mode A']) + \
                    #      '  and   AC2 Class: ' + str(row['AC2 Class']) + " Mode: " + str(row['AC2 Mode A']))
                    break

                subfolder += 1

            if not found:
                print("Cannot find encounter: ", str(row['Encounter No.']))

        print("Done.")
        return idx, len(df_filtered_Set.index)
        #return len(df_filtered_Set.index)

    return 0, 0


def processEncounterSet(layer, enc_folder, enc_file_path, type, fullpath_file, resume_file, filterOptions, enc_distribution):
    total = 0
    sourceTotal = 0

    df_chunk = pd.read_csv(fullpath_file, delimiter=',', chunksize=10000, dtype=df_dtype)

    file_enc = open(resume_file, "a")
    file_enc.write("ENCOUNTER_FOLDER, " + enc_file_path + ", " + type + ", " + str(layer) + "\n")
    file_enc.close()

    for chunk in df_chunk:

        print("Got chuck of data: " + str(len(chunk.index)) + " for a total of: " + str(sourceTotal))

        intCols = ['Encounter No.', 'Layer', "AC1 Class", "AC2 Class"]

        #types_dict = dict(chunk)
        #del types_dict['Encounter_Distribution No.']
        #del types_dict['Layer']
        #del types_dict['AC1 Class']
        #del types_dict['AC2 Class']
        #chunk['Encounter_Distribution No.'] = pd.to_numeric(chunk['Encounter_Distribution No.'], downcast='unsigned', errors='coerce')
        #chunk['Layer'] = pd.to_numeric(chunk['Layer'], downcast='unsigned', errors='coerce')
        #chunk['AC1 Class'] = pd.to_numeric(chunk['AC1 Class'], downcast='unsigned', errors='coerce')
        #chunk['AC2 Class'] = pd.to_numeric(chunk['AC2 Class'], downcast='unsigned', errors='coerce')

        #chunk[intCols] = chunk[intCols].applymap(np.int64)

        #chunk['Encounter_Distribution No.'].astype(np.int32)
        chunk['Encounter No.'] = chunk['Encounter No.'].round().astype(int)
        chunk['Seed'] = chunk['Seed'].round().astype(int)
        chunk['Layer'] = chunk['Layer'].round().astype(int)
        chunk['AC1 Class'] = chunk['AC1 Class'].round().astype(int)
        chunk['AC2 Class'] = chunk['AC2 Class'].round().astype(int)
        chunk['Time before CPA'] = chunk['Time before CPA'].round().astype(int)
        chunk['Time after CPA'] = chunk['Time after CPA'].round().astype(int)



        target_path = enc_folder + enc_file_path + type

        partial1, partial2 = processEncounterChunck(chunk, resume_file, target_path, filterOptions, enc_distribution)
        total += partial1
        sourceTotal += partial2

        if partial1 > 0:
            print("Total encounters so far: " + str(total))

    return total, sourceTotal


class FilterOptions:
  def __init__(self):
    self.IntruderType = None
    self.IntruderSubset = None
    self.RPASSubset = None
    self.AltitudeSubset = None
    self.SpeedSubset = None
    self.OwnSpeedSubset = None



# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    MAX_LAYER = 4
    MAX_ENCOUNTER_FOLDERS = 2
    ENCOUNTER_TYPE = ["EE", "EU", "UU"]

    enc_parameters = ep.Encounter_Distribution_Parameters()
    enc_distribution = ed.Encounter_distribution(enc_parameters)

    enc_file_template = "Enc_Params_Layer_%s_%s.csv"
    #enc_file = "Enc_Params_Layer.csv"
    resume_file = "Resume_Enc_Params.csv"


    enc_folder_template = "RPAS_Generated_Encounters-REV7\\RPAS_%s\\"
    layer_folder_template = "Layer_%s_%s_100k\\"

    base_folder = "E:\CAFE-Encounters\\"
    #base_folder = "D:\\URCLEARED\\CAFE-Encounters\\"


    Prefix_Name = "RPAS"

    filterOptions = FilterOptions()

    # Options: "NONE" / "Other" / "RPAS"
    filterOptions.IntruderType = "Other"

    # Options: "NONE" / "nonCoop" / "modeS" / "ACAS"
    filterOptions.IntruderSubset = "nonCoop"

    # Options: "NONE" / "URCLEARED" / "ACAS"
    filterOptions.RPASSubset = "URCLEARED"

    # Options: "NONE" / "LOWER" / "HIGHER"
    filterOptions.AltitudeSubset = "LOWER"

    # Options: "NONE" / "SPEEDLIMIT"
    filterOptions.SpeedSubset = "NONE"

    # Options: "NONE" / "BELOW100" / "ABOVE100"
    filterOptions.OwnSpeedSubset = "BELOW100"


    enc_folder = base_folder + enc_folder_template % filterOptions.IntruderType
    enc_file = "%s_vs_%s_Resume" % (filterOptions.RPASSubset, filterOptions.IntruderSubset)
    enc_file ="Enc_Params.csv"

    if filterOptions.OwnSpeedSubset == "NONE":
        enc_distribution.path = Prefix_Name + ("_%s_vs_%s_at_%s_%s_Resume" % (filterOptions.RPASSubset, filterOptions.IntruderSubset, filterOptions.AltitudeSubset, filterOptions.SpeedSubset))
        resume_file = Prefix_Name + ("_%s_vs_%s_at_%s_%s_Resume_Enc_Params.csv" % (filterOptions.RPASSubset, filterOptions.IntruderSubset, filterOptions.AltitudeSubset, filterOptions.SpeedSubset))
    else:
        enc_distribution.path = Prefix_Name + ("_%s_vs_%s_at_%s_%s_Resume" % (filterOptions.RPASSubset, filterOptions.IntruderSubset, filterOptions.AltitudeSubset, filterOptions.OwnSpeedSubset))
        resume_file = Prefix_Name + ("_%s_vs_%s_at_%s_%s_Resume_Enc_Params.csv" % (filterOptions.RPASSubset, filterOptions.IntruderSubset, filterOptions.AltitudeSubset, filterOptions.OwnSpeedSubset))


    print("Analyzing encounters at folder: \n\t\t", enc_folder, "\n")

    file_enc = open(enc_folder + "\\" + resume_file, "w")
    file_enc.write("ROOT_FOLDER, " + enc_folder_template % filterOptions.IntruderType + "\n")
    file_enc.close()



    layer = 1
    encounterReal = 0
    encounterTotal = 0
    while (layer <= MAX_LAYER):
        i = 0
        print("Processing encounter layer: ", layer, "\n")

        while i < len(ENCOUNTER_TYPE):
            type = ENCOUNTER_TYPE[i]
            i += 1

            print("Processing encounter type: ", type, "\n")

            #subfolder_path = layer_folder % (layer, type)
            subfolder_path = layer_folder_template % (layer, type)
            #enc_file = enc_file_template % (layer, type)
            fullpath_file = enc_folder + subfolder_path + enc_file

            if not os.path.isfile(fullpath_file):
                print("Encounter_Distribution set: ", fullpath_file, "\n")
                print("Does not exists, skipping...\n")
            elif not os.access(fullpath_file, os.R_OK):
                print("Encounter_Distribution set: ", fullpath_file, "\n")
                print("Cannot be opened, skipping...\n")
            else:
                print("Opening encounter set: ", fullpath_file, "\n")
                partial, total = processEncounterSet(layer, enc_folder, subfolder_path, type, fullpath_file, enc_folder + resume_file, filterOptions, enc_distribution)
                encounterReal += partial
                encounterTotal += total
                print("Encounter_Distribution set adds: ", partial, "up to a total of: ", encounterReal, "\n")
        layer += 1

    if not os.path.exists(enc_folder + "\\" + enc_distribution.path):
        os.makedirs(enc_folder + "\\" + enc_distribution.path)

    enc_distribution.plotVMDDistribution(enc_folder)
    enc_distribution.plotHMDDistribution(enc_folder)
    enc_distribution.plotHSpeedDistribution(enc_folder)
    enc_distribution.plotVSpeedDistribution(enc_folder)
    enc_distribution.plotAAngleDistribution(enc_folder)
    enc_distribution.plotAltitudeDistribution(enc_folder)
    enc_distribution.plotVMDHMDDistribution(enc_folder)
    enc_distribution.plotAC1Distribution(enc_folder)
    enc_distribution.plotAC2Distribution(enc_folder)
    enc_distribution.plotAC1AC2Distribution(enc_folder)
    enc_distribution.plotNMACDistribution(enc_folder)
    enc_distribution.plotAC1AC2NMACDistribution(enc_folder)


    f = open(enc_folder + "\\" + enc_distribution.path + "\\Encounter_resume_report.txt", "w")
    now = datetime.now()
    date_time = now.strftime("%m/%d/%Y, %H:%M:%S")
    f.write("Encounter_Distribution filter set report generated on: " + date_time + "\n")
    f.write("Total number of encounters available: " + str(encounterReal) + " from a total of: " + str(encounterTotal) + "\n")
    f.write("Total number of NMAC: " + str(enc_distribution.NMAC_Total) + "\n")
    f.write("Done." + "\n")
    f.close()

    print("Total number of encounters filtered: " + str(encounterReal) + " from a total of: " + str(encounterTotal) + "\n")
    print("Done.\n")
    sys.exit(0)
