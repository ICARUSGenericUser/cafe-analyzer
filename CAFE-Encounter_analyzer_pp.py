# This is a sample Python script.
import os
import sys
from time import time, sleep
import queue
import pandas as pd
import numpy as np
from RWC import HAZ_parameters as rwc, NMAC_parameters as nmac
from Encounter import encounter_task as et, position as pos, encounter as enc
from Metrics import Enc_Alert_Measuring as mea, Alert_Measuring as am
from Encounter_Distribution import Encounter_Parameters as ep
import multiprocessing
from multiprocessing import Process, Value, Lock, current_process


#df_dtype = {
#        "Encounter_Distribution No.": int,
#        "Seed": int,
#        "Layer": int,
#        "VMD": np.float64,
#        "HMD": np.float64,
#        "Approach Angle": np.float64,
#        "Bearing from North": np.float64,
#        "Time before CPA": int,
#        "Time after CPA": int,
#        "AC1 Mode A": str,
#        "AC1 Callsign": str,
#        "AC1 Class": int,
#        "AC1 Controlled?": bool,
#        "AC1 Altitude": np.float64,
#        "AC1 Vertical": np.float64,
#        "AC1 Speed": np.float64,
#        "AC1 Acceleration": np.float64,
#        "AC1 Heading": np.float64,
#        "AC1 Turn": np.float64,
#        "AC2 Mode A": str,
#        "AC2 Callsign": str,
#        "AC2 Class": int,
#        "AC2 Controlled?": bool,
#        "AC2 Altitude": np.float64,
#        "AC2 Vertical": np.float64,
#        "AC2 Speed": np.float64,
#        "AC2 Acceleration": np.float64,
#        "AC2 Heading": np.float64,
#        "AC2 Turn": np.float64,
#}

df_dtype = {
        "Encounter_Distribution No.": np.int,
        "Seed": np.int,
        "Layer": np.int,
        "VMD": np.float64,
        "HMD": np.float64,
        "Approach Angle": np.float64,
        "Bearing from North": np.float64,
        "Time before CPA": np.float64,
        "Time after CPA": np.float64,
        "AC1 Mode A": str,
        "AC1 Callsign": str,
        "AC1 Class": np.int,
        "AC1 Controlled?": bool,
        "AC1 Altitude": np.float64,
        "AC1 Vertical": np.float64,
        "AC1 Speed": np.float64,
        "AC1 Acceleration": np.float64,
        "AC1 Heading": np.float64,
        "AC1 Turn": np.float64,
        "AC2 Mode A": str,
        "AC2 Callsign": str,
        "AC2 Class": np.int,
        "AC2 Controlled?": bool,
        "AC2 Altitude": np.float64,
        "AC2 Vertical": np.float64,
        "AC2 Speed": np.float64,
        "AC2 Acceleration": np.float64,
        "AC2 Heading": np.float64,
        "AC2 Turn": np.float64,
}

trj_dtype = {
        "time": str,
        "aircraft":  np.int32,
        "Mode A": np.int32,
        "x-position": np.float64,
        "y-position": np.float64,
        "altitude": np.float64,
        "callsign": str,
}

trj_columns = [
        "time",
        "aircraft",
        "Mode A",
        "x-position",
        "y-position",
        "altitude",
        "callsign",
]

df_columns = [
        "Encounter_Distribution No.",
        "Seed",
        "Layer",
        "VMD",
        "HMD",
        "Approach Angle",
        "Bearing from North",
        "Time before CPA",
        "Time after CPA",
        "AC1 Mode A",
        "AC1 Callsign",
        "AC1 Class",
        "AC1 Controlled?",
        "AC1 Altitude",
        "AC1 Vertical",
        "AC1 Speed",
        "AC1 Acceleration",
        "AC1 Heading",
        "AC1 Turn",
        "AC2 Mode A",
        "AC2 Callsign",
        "AC2 Class",
        "AC2 Controlled?",
        "AC2 Altitude",
        "AC2 Vertical",
        "AC2 Speed",
        "AC2 Acceleration",
        "AC2 Heading",
        "AC2 Turn"
]



def processTrajectoryFile(full_encounter_path, full_diagram_path, enc_type, encounter, encounter_param):

    print("Reading a trajectory...")
    trj_df = pd.read_csv(full_encounter_path, delimiter='\t', names=trj_columns, dtype=trj_dtype)
    #    trj_df = pd.read_csv(full_encounter_path, delimiter=',', names=trj_columns, dtype=trj_dtype)

    # print("Total number of points in the trajectory: " + str(len(trj_df.index)) + "\n")
    # print("Number of columns: " + str(len(trj_df.columns)) + "\n")

    for index, row in trj_df.iterrows():
        if row['aircraft'] == 1:
            position = pos.Position(row['aircraft'], row['time'], row['x-position'], row['y-position'], row['altitude'])
            encounter.addAircraft1Position(position)
        elif row['aircraft'] == 2:
            position = pos.Position(row['aircraft'], row['time'], row['x-position'], row['y-position'], row['altitude'])
            encounter.addAircraft2Position(position)

    if (encounter.numberAircraft1Position() == encounter.numberAircraft2Position()):
        #    print("Trajectory seems consistent with : " + str(encounter.numberAircraft1Position()) + " positions.\n")

        encounter.encAlertMeasuringAicraft1 = arcf1_eam = mea.Enc_Alert_Measuring()
        encounter.encAlertMeasuringAicraft2 = arcf2_eam = mea.Enc_Alert_Measuring()

        encounter.computeDynamics()
        encounter.computeConflictsCAFE(encounter_param, arcf1_eam, arcf2_eam)

        if encounter_param.debug and arcf1_eam.active_conflict:
            print("Reporting on aircraft 1:")
            arcf1_eam.report_Alert_Measuring()
        if encounter_param.debug and arcf2_eam.active_conflict:
            print("\nReporting on aircraft 2:")
            arcf2_eam.report_Alert_Measuring()

        if encounter_param.mustPlot:
            encounter.plotEncounterCAFE(full_diagram_path)

        print("Done.")
        return None
    else:
        print("Trajectory seems NOT consistent.")
        return None



def processResultCollector(tasksInQueue, outQueue, encounterResume, result_enc_filename, prov_enc_filename, processComputed, processDone, lock,):
    # Start time of the collection process
    start = time()

    name = current_process().name

    print('Starting result collector process: ', name)

    while True:
        try:
            encounter = outQueue.get(block=True, timeout=10)
        except queue.Empty:
            print("*** Collector process: reading has timed out.")
            if processDone.value:
                duration = (time() - start)
                encounterResume.writeReport(prov_enc_filename, duration)
                exit(0)
            else:
                print("*** Collector process: we continue through another iteration.")
                continue

        if not encounter.isVoid():

            print("\nTask {0} collecting encounter data with Id: {1}".format(os.getpid(), str(encounter.id).zfill(9) ), flush=True)
            result_enc_file = open(result_enc_filename, "a")
            encounter.writeEncounterResult(result_enc_file)
            result_enc_file.close()

            encounterResume.addEncounter()
            encounterResume.addConflict(encounter.encAlertMeasuringAicraft1.active_conflict, encounter.encAlertMeasuringAicraft2.active_conflict)
            encounterResume.addNMAC(encounter.encAlertMeasuringAicraft1.nmac_violated, encounter.encAlertMeasuringAicraft2.nmac_violated)
            encounterResume.addHazard(encounter.encAlertMeasuringAicraft1.haz_violated, encounter.encAlertMeasuringAicraft2.haz_violated)
            encounterResume.addWarning(encounter.encAlertMeasuringAicraft1.warn_type, encounter.encAlertMeasuringAicraft2.warn_type)
            encounterResume.addCorrective(encounter.encAlertMeasuringAicraft1.caution_type, encounter.encAlertMeasuringAicraft2.caution_type)
            encounterResume.addPreventive(encounter.encAlertMeasuringAicraft1.prev_type, encounter.encAlertMeasuringAicraft2.prev_type)

            # encounterResume.addTimming(encounter.encAlertMeasuringAicraft1)
            encounterResume.addTimming(encounter.encAlertMeasuringAicraft2)

            processComputed.value += 1

            duration = (time() - start)
            print("Total encounters processed: ", processComputed.value, " in: ", "{:.2f}".format(duration), " sec")
        else:
            print("Encounter_Distribution discarded: ", encounter.id)

        with lock:
            tasksInQueue.value -= 1

    print("WARNING: Collector process ended unexpectedly.")


# This is the consumer process
# It receives as parameter the input queue with the analysis parameters
# Inserts the results in the output queue
#def processEncounterSet(encfolder, diagram_folder, enc_number, aircraft1Class, aircraft2Class):
def processEncounterSet(inQueue, outQueue, encounter_param, lock):
    MAX_ENCOUNTER_FOLDERS = 2

    name = current_process().name
    print('Starting process: ', name)
    sleep(3)

    while True:
        found = False
        encTask = inQueue.get()
        print("\nTask {0} processing in folder: {1}  encounter: {2}".format(os.getpid(), encTask.task_enc_folder, str(encTask.enc_number).zfill(9)), flush=True)

        subfolder = 1
        while (subfolder <= MAX_ENCOUNTER_FOLDERS):

            full_encounter_path = encTask.task_enc_folder + "\\" + str(subfolder) + "\\" + "Encs_" + str(encTask.enc_number).zfill(9) + ".eu1"
            full_diagram_path = encTask.task_enc_folder + "\\" + encTask.diagram_folder

            print("Checking Encounter_Distribution file: ", full_encounter_path)

            if os.path.exists(full_encounter_path):
                print("Encounter_Distribution file: ", full_encounter_path, "  Identified, processing...")
                encounter = enc.Encounter(encTask.enc_number, encTask.aircraft1Class, encTask.aircraft2Class)
                processTrajectoryFile(full_encounter_path, full_diagram_path, encTask.enc_type, encounter, encounter_param)
                found = True
                outQueue.put(encounter)

            subfolder += 1

        if not found:
            encounter = enc.Encounter(encTask.enc_number, -1, -1)
            outQueue.put(encounter)
            print("Encounter_Distribution number: ", encTask.enc_number, " cannot be found\n")



# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # creating the name

    ENCOUNTER_TYPE = ["EE", "EU", "UU"]
    found = 0
    notFound = 0

    # Select a certain max number of encounters to debug
    maxProcess = 0
    # maxProcess = 10

    RPAS_Set = 12
    if RPAS_Set == 10:
        Prefix_Name = "Small_"
    elif RPAS_Set == 11:
        Prefix_Name = "Turboprop_"
    elif RPAS_Set == 12:
        Prefix_Name = "Jet_"

    # Options: "nonEquipped" / "equipped" / "class3"
    analysisSubset = "equipped"

    if analysisSubset == "nonEquipped":
        resume_file = Prefix_Name + "RPAS_vs_NonEquipped_Resume_Enc_Params.csv"
        result_file = Prefix_Name + "RPAS_vs_NonEquipped_Results.csv"
        prov_file = Prefix_Name + "RPAS_vs_NonEquipped_Results.txt"
        diagram_folder = Prefix_Name + "RPAS_vs_NonEquipped_Folder"
        print("Analyzing " + Prefix_Name + "RPAS vs non-equipped encounters...")
    elif analysisSubset == "equipped":
        resume_file = Prefix_Name + "RPAS_vs_Equipped_Resume_Enc_Params.csv"
        result_file = Prefix_Name + "RPAS_vs_Equipped_Results.csv"
        prov_file = Prefix_Name + "RPAS_vs_Equipped_Results.txt"
        diagram_folder = Prefix_Name + "RPAS_vs_Equipped_Folder"
        print("Analyzing " + Prefix_Name + "RPAS vs equipped encounters...")
    elif analysisSubset == "class3":
        resume_file = Prefix_Name + "RPAS_vs_Class3_Resume_Enc_Params.csv"
        result_file = Prefix_Name + "RPAS_vs_Class3_Results.csv"
        prov_file = Prefix_Name + "RPAS_vs_Class3_Results.txt"
        diagram_folder = Prefix_Name + "RPAS_vs_Class3_Folder"
        print("Analyzing " + Prefix_Name + "RPAS vs Class 3 encounters...")

    # layer_folder = "Layer_%s_%s_200k\\"
    # resume_enc_folder = "E:\Encounter-RPAS-Encounters\RPAS_safety_generated_encounters-REV0\RPAS_v_Non_RPAS\\"

    # layer_folder = "Layer_%s_%s_100k\\"
    # resume_enc_folder = "E:\Encounter-RPAS-Encounters\RPAS_safety_generated_encounters-REV3\RPAS_Other\\"

    # resume_enc_folder = "E:\Encounter-RPAS-Encounters\\RPAS_safety_generated_encounters-REV0\\RPAS_v_Non_RPAS\\"
    resume_enc_folder = "E:\CAFE-RPAS-Encounters\RPAS_safety_generated_encounters-REV3\RPAS_Other\\"

    resume_enc_file = resume_enc_folder + resume_file
    result_enc_filename = resume_enc_folder + result_file
    prov_enc_filename = resume_enc_folder + prov_file

    print("Analyzing encounters selected in resume encounter file: \n\t\t", resume_enc_file, "\n")

    if not os.path.exists(resume_enc_file):
        print("Encounter_Distribution file: ", resume_enc_file, "\n")
        print("Does not exists, stopping...\n")
        exit(-1)

    print("Setting encounter parameters to DO365A")
    encounter_param = ep.Encounter_Parameters()
    encounter_param.set_DO365A_parameters()

    #print("Setting standard TCASII parameters")
    #encounter_param.set_TCASII_parameters()

    encounter_param.mustPlot = False
    encounter_param.debug = False


    print("Generating encounter results on file: \n\t\t", result_enc_filename, "\n")
    encounterResume = am.Alert_Measuring()

    main_enc_folder = None
    enc_folder = None
    enc_type = None
    enc_file = None

    print("Setting multiprocessing framework...\n")
    print("Number of available cpu : ", multiprocessing.cpu_count())
    in_blocking_q = multiprocessing.Queue(maxsize= 2 * multiprocessing.cpu_count())
    out_blocking_q = multiprocessing.Queue(maxsize=2 * multiprocessing.cpu_count())

    tasksInQueue = Value('i', 0)
    processComputed = Value('i', 0)
    processDone = Value('b', False)
    lock = Lock()
    processInjected = 0

    # Instantiating process with arguments
    activeProcesses = []
    for i in range(0, multiprocessing.cpu_count()):
        # print(name)
        #proc = Process(name='Encoder-Analyzer-'+ str(i), target=processEncounterSet, args=(in_blocking_q, out_blocking_q))
        proc = Process(name='Encoder-Analyzer-' + str(i), target=processEncounterSet, args=(in_blocking_q, out_blocking_q, encounter_param, lock,))
        activeProcesses.append(proc)
        proc.start()


    df_chunk = pd.read_csv(resume_enc_file, header=None, sep='\n', chunksize=10000, engine='python')

    for chunk in df_chunk:

        # Limit the process to a certain number of encounters
        # Useful for debugging purposes
        if maxProcess > 0 and processComputed.value >= maxProcess:
            break

        print("Processing new chunk...")
        print("Splitting columns...")
        df = chunk[0].str.split('\s*,\s*', expand=True)

        # Assign column names
        # df.columns = df_columns

        print("Renaming columns...")
        i = 0
        while i < len(df_columns):
            df.rename(columns={i: df_columns[i]}, inplace=True)
            i += 1

        print("Performing analysis...")

        for index, row in df.iterrows():
            if row['Encounter_Distribution No.'] == 'ROOT_FOLDER':
                main_enc_folder = row["Seed"]
                print("Root folder directive identified for: ", main_enc_folder, "\n")

                if not os.path.exists(main_enc_folder):
                    print("Root folder: ", main_enc_folder)
                    print("Does not exists, stopping...")
                    exit(-1)

                result_enc_file = open(result_enc_filename, "w")
                result_enc_file.write('ROOT_FOLDER; ' + main_enc_folder + "\n")
                result_enc_file.close()

                proc = Process(name='Result-Collector', target=processResultCollector, args=(tasksInQueue, out_blocking_q, encounterResume, result_enc_filename, prov_enc_filename, processComputed, processDone, lock,))
                activeProcesses.append(proc)
                proc.start()
                continue


            if row['Encounter_Distribution No.'] == 'ENCOUNTER_FOLDER':
                enc_folder = row["Seed"]
                enc_type = row["Layer"]
                print("Encounter_Distribution folder directive identified for: ", enc_folder, " of type: ", enc_type, "\n")

                if not os.path.exists(main_enc_folder + enc_folder):
                    print("Encounter_Distribution folder: ", main_enc_folder + enc_folder)
                    print("Does not exists, skipping the associated encounters...\n")
                    enc_folder = None
                    continue

                if not os.path.exists(main_enc_folder + enc_folder + enc_type):
                    print("Encounter_Distribution folder: ", main_enc_folder + enc_folder + enc_type)
                    print("Does not exists, skipping the associated encounters...\n")
                    enc_folder = None
                    continue

                print("*** Injector process: new block of data. Waiting to empty queue: ", tasksInQueue.value)
                while (tasksInQueue.value != 0):
                    print("*** Injector process: new block of data. Waiting to empty queue: ", tasksInQueue.value)
                    sleep(5)

                print("*** Injector process: continues.")

                result_enc_file = open(result_enc_filename, "a")
                result_enc_file.write('ENCOUNTER_FOLDER; ' + enc_folder + '; ' + enc_type + "\n")
                result_enc_file.close()

                task_enc_folder = main_enc_folder + enc_folder + "\\" + enc_type

                if not encounter_param.mustPlot:
                    continue

                if os.path.exists(resume_enc_folder + "\\" + diagram_folder):
                    print("Renaming folder...\n")
                    if os.path.exists(resume_enc_folder + "\\" + diagram_folder + ".bak"):
                        os.rmdir(resume_enc_folder + "\\" + diagram_folder + ".bak")
                    os.rename(resume_enc_folder  + "\\" + diagram_folder, resume_enc_folder  + "\\" + diagram_folder + ".bak")
                    os.mkdir(resume_enc_folder  + "\\" + diagram_folder)
                else:
                    os.mkdir(resume_enc_folder  + "\\" + diagram_folder)

                continue

            # Limit the process to a certain number of encounters
            # Useful for debugging purposes
            if maxProcess > 0 and processComputed.value >= maxProcess:
                print("Stopping the insertion of tasks in the queue...\n")
                break

            # Now, we should get an encounter line
            # We need to check is the associated file exists before starting any process

            enc_number = row['Encounter_Distribution No.']
            aircraft1Class = row['AC1 Class']
            aircraft2Class = row['AC2 Class']

            print("Queuing encounter number: ", enc_number, " with Class1 aircraft: ", aircraft1Class, " with Class2 aircraft: ", aircraft2Class,  "\n")
            enc_task = et.encounter_task(task_enc_folder, diagram_folder, enc_type, enc_number, aircraft1Class, aircraft2Class)

            processInjected += 1
            with lock:
                tasksInQueue.value += 1

            in_blocking_q.put(enc_task)


    processDone.value = True
    while (tasksInQueue.value != 0):
        sleep(10)

    print("Done with a total of encounters: ", processInjected)

    sys.exit(0)
