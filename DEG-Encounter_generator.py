# This is a sample Python script.
import sys
import os
import numpy as np
from ProbabilityDist import Encounter_Distribution as ed
from DEG_Encounter import Parameters as ep, Encounter as enc, TrajectoryRotation as tr, storeTrajectory as st, CPAconditions as cpa
from QuadcopterSimCon.quadFiles.initQuad import load_params_from_file
from QuadcopterSimCon import function_trajectory_generation as tg
from DEG_Encounter import plotWaypointSequence as pw, plotEncounterVariables as pev


# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    # Set a fixed seed to try to reproduce the encounters
    seed = 869430  # Encounter Set MAST -A
    seed = 793419  # Encounter Set MAST -C
    seed = 367042  # Encounter Set MAST -D
    seed = 601897  # Encounter Set MAST -E
    seed = 131680  # Encounter Set MAST -F
    seed = 270356  # Encounter Set MAST -G
    seed = 925701  # Encounter Set MAST -H

    seed = 339472  # Encounter Set 3  / lowmanset
    #seed = 233423 # Encounter Set 2
    #seed = 146459 # Encounter Set 1
    np.random.seed(seed)
    #seed = np.random.get_state()[1][0]

    # Generate just for a given encounter, skip as much of the rest
    #SelectedEncounter = 44
    SelectedEncounter = None

    # If selected, draw several graphs to evaluate the correct operation of the DEG simulator
    TEST_DEBUG = 2

    # If selected, dynamic plots will be displayed during the generation process
    DYNAMIC_PLOTS = False

    # If selected, generate encounter summary file in CSV format
    ENCOUNTER_SUMMARY_OUTPUT = True

    # If selected, store console log into txt file
    STORE_LOG = True
    clear = lambda: os.system('CLS')  # on Windows System
    #os.system('clear')  # on Linux System

    MAIN_PATH = "E:\\Datasets\\DEG\\"
    #MAIN_PATH = "C:\\Users\\Voyager\\Desktop\\URCLEARED\\DroneEncounters\\"
    #MAIN_PATH = "D:\\URCLEARED\\DroneEncounters\\"

    DEG_Enconter_dist = ed.Encounter_Distribution()

    #DEG_Enconter_dist.parseFromJson(MAIN_PATH + "ProbabilityDistributions\\droneWCVTestDistribution.json")
    DEG_Enconter_dist.parseFromJson(MAIN_PATH + "ProbabilityDistributions\\droneSimpleTestDistribution.json")

    DEG_Enconter_dist.plotEncounterDistribution(MAIN_PATH + "ProbabilityDistributions\\Diagrams")

    generationParameters = ep.Parameters()
    generationParameters.totalNumberEncounters = 100

    generationParameters.destinationPath = MAIN_PATH + "EncounterSet\\"

    generationParameters.encounterSetName = "lowManTestSet"
    #generationParameters.encounterSetName = "MAST-SET-H"
    generationParameters.probDistributions = DEG_Enconter_dist

    # Insert desired drone
    # -----------------------------------------------------------------------
    # Options: DJIF450, DJIF450FAST
    droneName = "DJIF450FAST"
    quadJSONPath = MAIN_PATH + "VehicleParameters\\vehicleParams-v2.json"

    droneParametersSet = load_params_from_file(quadJSONPath)


    # Time duration of the simulation
    generationParameters.timeBeforeNMAC = 160
    generationParameters.timeAfterNMAC = 60

    generationParameters.createEncounterSetFolder()

    encounterNumber = 1

    while encounterNumber <= generationParameters.totalNumberEncounters:

        encounter = enc.Encounter()
        encounter.number = encounterNumber
        encounter.currentSeed = np.random.get_state()[1][0]

        encounter.createEncounterFolder(generationParameters)

        if STORE_LOG:
            # Store console log into a txt file
            sys.stdout = open(encounter.getEncounterPathSaveLog(), 'w')

        # Set initial conditions at CPA
        encounter.sampleNMAC(DEG_Enconter_dist)

        #
        # Generation of the ownship trajectory
        print("\nGENERATING OWNSHIP TRAJECTORY FOR VEHICLE TYPE: " + encounter.NMACValues.ClassOwnship.className, flush=True)

        # NMAC Segment generation
        encounter.sampleNMACSegment(encounter.ownshipTrajectory, DEG_Enconter_dist.Initial_Distribution, encounter.NMACValues.ClassOwnship)
        #encounter.NMACValues.ClassOwnship
        #encounter.NMACValues.ClassIntr

        while encounter.ownshipTrajectory.timeAfterNMAC < generationParameters.timeAfterNMAC:
            encounter.sampleForwardSegment(encounter.ownshipTrajectory, DEG_Enconter_dist.Initial_Distribution, encounter.NMACValues.ClassOwnship)

        while encounter.ownshipTrajectory.timeBeforeNMAC < generationParameters.timeBeforeNMAC:
            encounter.sampleBackwardSegment(encounter.ownshipTrajectory, DEG_Enconter_dist.Initial_Distribution, encounter.NMACValues.ClassOwnship)

        encounter.addLastBackwardSegment(encounter.ownshipTrajectory)

        # Generation of NMAC segment waypoints
        # Ownship trajectory generation
        if DYNAMIC_PLOTS:
            ax, fig = pw.getDynamicPlot("Ownship")

        encounter.positionNMACSegment(encounter.ownshipTrajectory, encounter.ownshipTrajectory.NMACSegment)
        idxNMCown = encounter.ownshipTrajectory.getNMACSegmentIdx()
        if DYNAMIC_PLOTS:
            pw.addSegmentDynamicPlot(ax, encounter.ownshipTrajectory.NMACSegment)


        idx = idxNMCown + 1
        prevSegment = encounter.ownshipTrajectory.NMACSegment
        while idx < len(encounter.ownshipTrajectory.segmentList):
            encounter.positionForwardSegment(encounter.ownshipTrajectory, encounter.ownshipTrajectory.segmentList[idx], prevSegment)
            prevSegment = encounter.ownshipTrajectory.segmentList[idx]
            if DYNAMIC_PLOTS:
                pw.addSegmentDynamicPlot(ax, encounter.ownshipTrajectory.segmentList[idx])
            idx += 1

        idx = idxNMCown - 1
        nextSegment = encounter.ownshipTrajectory.NMACSegment
        while idx >= 0:
            encounter.positionBackwardSegment(encounter.ownshipTrajectory, encounter.ownshipTrajectory.segmentList[idx], nextSegment)
            nextSegment = encounter.ownshipTrajectory.segmentList[idx]
            if DYNAMIC_PLOTS:
                pw.addSegmentDynamicPlot(ax, encounter.ownshipTrajectory.segmentList[idx])
            idx -= 1

        if DYNAMIC_PLOTS:
            pw.closeDynamicPlot(fig, encounter.getOwnshipHPath())

        # Generate waypoints from segments
        encounter.ownshipTrajectory.generateWaypoints()
        # Retrieve NMAC segment within Waypoint array
        encounter.ownshipTrajectory.identifyNMACIdx()

        print("\nPLOTTING OWNSHIP TRAJECTORY....", flush=True)

        # Plot ownship trajectory
        if TEST_DEBUG >= 2:
            pw.plotWaypoints(encounter.ownshipTrajectory.wpSequence, encounter.getOwnshipFigurePath(), encounter.ownshipTrajectory.indexNMAC, encounter.number, TEST_DEBUG)


        ######### TEST PLOT: check state variables from segments and waypoints over time ###################
        #if TEST_DEBUG:
        #    pev.plotSegmentVariables(encounter.ownshipTrajectory.segmentList)

        #
        # Generation of the intruder trajectory
        print("\nGENERATING INTRUDER TRAJECTORY FOR VEHICLE TYPE: " + encounter.NMACValues.ClassIntr.className, flush=True)

        # NMAC Segment generation
        encounter.sampleNMACSegment(encounter.intrTrajectory, DEG_Enconter_dist.Initial_Distribution, encounter.NMACValues.ClassIntr)

        while encounter.intrTrajectory.timeAfterNMAC < generationParameters.timeAfterNMAC:
            encounter.sampleForwardSegment(encounter.intrTrajectory, DEG_Enconter_dist.Initial_Distribution, encounter.NMACValues.ClassIntr)

        while encounter.intrTrajectory.timeBeforeNMAC < generationParameters.timeBeforeNMAC:
            encounter.sampleBackwardSegment(encounter.intrTrajectory, DEG_Enconter_dist.Initial_Distribution, encounter.NMACValues.ClassIntr)

        encounter.addLastBackwardSegment(encounter.intrTrajectory)

        # Intruder trajectory generation
        if DYNAMIC_PLOTS:
            ax, fig = pw.getDynamicPlot("Intruder")
        encounter.positionNMACSegment(encounter.intrTrajectory, encounter.intrTrajectory.NMACSegment)
        idxNMCintr = encounter.intrTrajectory.getNMACSegmentIdx()
        if DYNAMIC_PLOTS:
            pw.addSegmentDynamicPlot(ax, encounter.intrTrajectory.NMACSegment)

        idx = idxNMCintr + 1
        prevSegment = encounter.intrTrajectory.NMACSegment
        while idx < len(encounter.intrTrajectory.segmentList):
            encounter.positionForwardSegment(encounter.intrTrajectory, encounter.intrTrajectory.segmentList[idx], prevSegment)
            prevSegment = encounter.intrTrajectory.segmentList[idx]
            if DYNAMIC_PLOTS:
                pw.addSegmentDynamicPlot(ax, encounter.intrTrajectory.segmentList[idx])
            idx += 1

        idx = idxNMCintr - 1
        nextSegment = encounter.intrTrajectory.NMACSegment
        while idx >= 0:
            encounter.positionBackwardSegment(encounter.intrTrajectory, encounter.intrTrajectory.segmentList[idx], nextSegment)
            nextSegment = encounter.intrTrajectory.segmentList[idx]
            if DYNAMIC_PLOTS:
                pw.addSegmentDynamicPlot(ax, encounter.intrTrajectory.segmentList[idx])
            idx -= 1

        if DYNAMIC_PLOTS:
            pw.closeDynamicPlot(fig, encounter.getIntruderHPath())

        # Generate waypoints from segments
        encounter.intrTrajectory.generateWaypoints()
        # Retrieve NMAC segment within Waypoint array
        encounter.intrTrajectory.identifyNMACIdx()

        print("\nPLOTTING INTRUDER TRAJECTORY....", flush=True)

        # Plot intruder trajectory
        if TEST_DEBUG >= 2:
            pw.plotWaypoints(encounter.intrTrajectory.wpSequence, encounter.getIntruderFigurePath(), encounter.intrTrajectory.indexNMAC, encounter.number, TEST_DEBUG)

        ######### TEST PLOT: check state variables from segments and waypoints over time ###################
        #if TEST_DEBUG:
        #    pev.plotSegmentVariables(encounter.intrTrajectory.segmentList)

        #print("\nINTRUDER TRAJECTORY RESUME:")
        #encounter.intrTrajectory.printTrajectory()

        if SelectedEncounter != None and SelectedEncounter != encounter.number:
            print("\nSkipping trajectory generation")
            # Clear console log (in order to store one by one)
            clear()
            encounterNumber += 1
            continue

        print("\nGENERATING OWNSHIP EXECUTION TRAJECTORY:", flush=True)
        # Generate trajectory state variables using QuadcopterSimCon and store them in Encounter Format
        tg.generateTrajectory(encounter.ownshipTrajectory, encounter.getOwnshipFigurePath(), droneParametersSet, encounter.NMACValues.ClassOwnship.droneName)

        if TEST_DEBUG >= 2:
            print("\nPLOTTING AND STORING ALTITUDE VARIATION FIGURES FROM TRAJECTORY SIMULATION:")
            st.plotTrajectoryResults("TRJ_", encounter.ownshipTrajectory, encounter.getOwnshipFigurePath(), encounter.number, encounter.NMACValues.ClassOwnship.className)

        # Store simulated state variables prior to path rotations (DEG performance analysis only)
        if TEST_DEBUG >= 2 and ENCOUNTER_SUMMARY_OUTPUT:
            priorRotation = True
            st.storeTrajectory(encounter.ownshipTrajectory.trajectorySequence, encounter.getOwnshipDataPath(),encounter.number, priorRotation)

        rotation = tr.NMACRotationParameters()
        rotation.setParametersFromEncounter(encounter)

        rotation.fillOwnshipFromIdx()

        print("\nGENERATING INTRUDER EXECUTION TRAJECTORY:", flush=True)
        # Generate trajectory state variables using QuadcopterSimCon and store them in Encounter Format
        tg.generateTrajectory(encounter.intrTrajectory, encounter.getIntruderFigurePath(), droneParametersSet, encounter.NMACValues.ClassIntr.droneName)

        # Store simulated state variables prior to path rotations (DEG performance analysis only)
        if TEST_DEBUG >= 2 and ENCOUNTER_SUMMARY_OUTPUT:
            priorRotation = True
            st.storeTrajectory(encounter.intrTrajectory.trajectorySequence, encounter.getIntruderDataPath(), encounter.number, priorRotation)

        if TEST_DEBUG >= 2:
            print("\nPLOTTING AND STORING ALTITUDE VARIATION FIGURES FROM TRAJECTORY SIMULATION:")
            st.plotTrajectoryResults("TRJ_", encounter.intrTrajectory, encounter.getIntruderFigurePath(), encounter.number, encounter.NMACValues.ClassIntr.className)

        rotation.fillIntruderFromIdx()

        if TEST_DEBUG >= 2:
            st.plotEncounterResults("ENC_", encounter.ownshipTrajectory.trajectorySequence, encounter.ownshipTrajectory.flownSequence, rotation.ownshipIdx, encounter.intrTrajectory.trajectorySequence, encounter.intrTrajectory.flownSequence, rotation.intrIdx, encounter.getOwnshipFigurePath(), encounter, True)

        # Store Results in a txt. file according to what specified by EUROCONTROL's Encounter User Guide (sampling time 1s)
        print("\nSTORING ENCOUNTER OUTPUT: " + encounter.getEncounterPath(), flush=True)

        rotation.adjustTrajectories(TEST_DEBUG)

        # Initialize object CPA Conditions
        CPAconds = cpa.CPAconditions()
        CPAconds.lowestHMD(encounter, rotation)

        if ENCOUNTER_SUMMARY_OUTPUT:
            priorRotation = False
            st.storeEncounterSummaryOutput(encounter.getPathStoreSummaryOutput(), encounter, rotation, CPAconds)
            st.storeTrajectory(encounter.ownshipTrajectory.trajectorySequence, encounter.getOwnshipDataPath(), encounter.number, priorRotation)
            st.storeFlowSequence(encounter.ownshipTrajectory.flownSequence, encounter.getOwnshipDataPath(), encounter.number)
            st.storeTrajectory(encounter.intrTrajectory.trajectorySequence, encounter.getIntruderDataPath(), encounter.number, priorRotation)
            st.storeFlowSequence(encounter.intrTrajectory.flownSequence, encounter.getIntruderDataPath(), encounter.number)
            st.storeCombinedTrajectory(encounter.ownshipTrajectory.trajectorySequence, encounter.intrTrajectory.trajectorySequence, encounter.getCombinedDataPath(), encounter.number, 0)
            st.storeCombinedTrajectory(encounter.ownshipTrajectory.trajectorySequence, encounter.intrTrajectory.trajectorySequence, encounter.getCombinedDataPath(), encounter.number, 3)

        # Clear console log (in order to store one by one)
        clear()
        encounterNumber += 1
    print("Done.\n", flush=True)

    sys.exit(0)
