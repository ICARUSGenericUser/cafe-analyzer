from Encounter import units as units

def transformUnits(sample, dunits):
    if dunits == "kt":
        return units.kt_to_ms(sample)
    elif dunits == "fpm":
        return units.fpm_to_ms(sample)
    elif dunits == "nm":
        return units.nm_to_m(sample)
    elif dunits == "ft":
        return units.ft_to_m(sample)
    elif dunits == "min":
        return units.min_to_sec(sample)

    return sample

