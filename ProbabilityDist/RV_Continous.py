import scipy.stats
import math as math
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from ProbabilityDist import transformUnits as tu


def check_RVContinous(cells, freqs):
    if len(cells) != len(freqs):
        return False
    if freqs[0] != 0:
        return False
    if len(cells) < 3:
        return False
    i = 1
    while i < len(cells):
        if cells[i - 1] >= cells[i]:
            return False
        i += 1
    return True


def check_RVFull(cells, freqs):
    if len(cells) != len(freqs):
        return False
    if freqs[0] != 0:
        return False
    if len(cells) < 3:
        return False
    i = 0
    sum = 0
    while i < len(freqs):
        sum += freqs[i]
        i += 1
    if not math.isclose(sum, 1.0):
        return False
    return True


class RVContinuous(scipy.stats.rv_continuous):
    def __init__(self, cells, freqs, units):
        super().__init__(a=cells[0], b=cells[-1])
        if len(cells) != len(freqs):
            raise Exception('Cells and freqs should have the same number of entries.')
        if freqs[0] != 0:
            raise Exception('Entry 0 of freqs should have value 0.')
        if len(cells) < 3:
            raise Exception('At least three cells should be used.')

        i = 1
        while i < len(cells):
            if cells[i - 1] >= cells[i]:
                raise Exception('Cells contens should be monotonically incresing.')
            i += 1

        i = 0
        sum = 0
        while i < len(freqs):
            sum += freqs[i]
            i += 1
        if not math.isclose(sum, 1.0):
            raise Exception('Cummulative provabilities should be equal to 1.')

        self.Cells = cells
        self.Freqs = freqs
        self.gotMin = False
        self.gotMax = False
        self.Min = 0
        self.Max = 0
        self.Units = units


    def consistentMinMax(self):
        if self.gotMin and self.Cells[-1] <= self.Min:
            return False
        if self.gotMax and self.Cells[0] >= self.Max:
            return False
        if self.gotMin and self.gotMax and self.Max <= self.Min:
            return False
        return True


    def gotLimits(self):
        return self.gotMin or self.gotMax


    def setMaximum(self, max):
        self.Max = max
        self.gotMax = True


    def setMinimum(self, min):
        self.Min = min
        self.gotMin = True


    def getSample(self):
        sample = self.rvs()

        if self.gotMax and sample > self.Max:
            return self.Max

        if self.gotMin and sample < self.Min:
            return self.Min

        return sample


    def capToLimits(self, value):
        if self.gotMin and value < self.Min:
            return self.Min
        if value < self.Cells[0]:
            return self.Cells[0]
        if self.gotMax and value > self.Max:
            return self.Max
        if value > self.Cells[-1]:
            return self.Cells[-1]
        return value


    def getSampleUnits(self):
        sample = self.getSample()
        return tu.transformUnits(sample, self.getUnits())


    def getUnits(self):
        return self.Units


    def _cdf(self, x):
        return np.array([self.do_cdf(i) for i in x])


    def do_cdf(self, x):
        if x < self.Cells[0]: return 0.0
        if x >= self.Cells[-1]: return 1.0
        v = 0.0
        for i in range(len(self.Cells)-1):
            if x >= self.Cells[i+1]:
                v += self.Freqs[i+1]
            else:
                v += self.Freqs[i+1]*(x-self.Cells[i])/(self.Cells[i+1]-self.Cells[i])
                break
        return v


    def plot_distribution(self, name, path):
        data_normal = self.rvs(size=1000)

        # settings for seaborn plotting style
        sns.set(color_codes=True)
        # settings for seaborn plot sizes
        sns.set(rc={'figure.figsize': (5, 5)})
        ax = sns.distplot(data_normal,
                          bins=100,
                          kde=True,
                          color='skyblue',
                          hist_kws={"linewidth": 15, 'alpha': 1})
        ax.set(xlabel='Distribution', ylabel='Frequency')
        fig = plt.gcf()
        filepath = path + "\\" + name + "_Distribution.png"
        print("Generating figure: " + filepath)
        fig.savefig(filepath)
        plt.close(fig)

        x = np.linspace(self.Cells[0], self.Cells[-1], 1000)

        y_pdf = self.pdf(x)  # the normal pdf
        y_cdf = self.cdf(x)  # the normal cdf

        label = name + " - PDF"
        plt.plot(x, y_pdf, label = label)
        plt.legend(prop={'size': 6})
        plt.suptitle("NMAC " + name + " - PDF")
        plt.title("Probability Density Function")
        fig = plt.gcf()
        filepath = path + "\\" + name + "_PDF.png"
        print("Generating figure: " + filepath)
        fig.savefig(filepath)
        plt.close(fig)

        label = name + " - CDF"
        plt.plot(x, y_cdf, label = label)
        plt.legend(prop={'size': 6})
        plt.suptitle("NMAC " + name + " - CDF")
        plt.title("Cumulative distribution function")
        fig = plt.gcf()
        filepath = path + "\\" + name + "_CDF.png"
        print("Generating figure: " + filepath)
        fig.savefig(filepath)
        plt.close(fig)
