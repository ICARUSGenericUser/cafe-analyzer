import scipy.stats as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from ProbabilityDist import RV_Continous as rvc
from Encounter import units as units


class NMAC_aircraft_parameters:

    def __init__(self):
        self.Altitude = 0
        self.Speed = 0
        self.Vrate = 0
        self.Accel = 0
        self.Trate = 0

    def setAircraftParameters(self, altitude, speed, vrate, accel, trate):
        self.Altitude = units.ft_to_m(altitude)
        self.Speed = units.kt_to_ms(speed)
        self.Vrate = units.fpm_to_ms(vrate)
        self.Accel = accel
        self.Trate = trate


class NMAC_parameters:

    def __init__(self):
        self.VMD = 0
        self.HMD = 0
        self.Altitude = 0
        self.AppAngle = 0
        self.Aircraft_Ownship = None
        self.Aircraft_Intruder = None

    def setNMACParameters(self, vmd, hmd, altitude, appAngle):
        self.VMD = units.ft_to_m(vmd)
        self.HMD = units.ft_to_m(hmd)
        self.Altitude = units.ft_to_m(altitude)
        self.appAngle = units.deg_to_rad(appAngle)

    def setAircraftOwnship(self, aircraft):
        self.Aircraft_Ownship = aircraft

    def setAircraftIntruder(self, aircraft):
        self.Aircraft_Intruder = aircraft

