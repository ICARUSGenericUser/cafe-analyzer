import scipy.stats
import math as math
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from ProbabilityDist import transformUnits as tu


def checkIfDuplicates(listOfElems):
    ''' Check if given list contains any duplicates '''
    setOfElems = set()
    for elem in listOfElems:
        if elem in setOfElems:
            return True
        else:
            setOfElems.add(elem)
    return False


def check_RVDiscrete(cells, freqs):
    if len(cells) != len(freqs):
        return False
    if len(cells) < 2:
        return False
    if checkIfDuplicates(cells):
        return False
    i = 0
    sum = 0
    while i < len(freqs):
        sum += freqs[i]
        i += 1
    if not math.isclose(sum, 1.0):
        return False
    return True


#class RVDiscrete(rv_sample):
#    def __init__(self, cells, freqs, units):
    #        if len(cells) != len(freqs):
    #        raise Exception('Cells and freqs should have the same number of entries.')
    #    if len(cells) < 2:
    #        raise Exception('At least two cells should be used.')
    #
    #    if checkIfDuplicates(cells):
    #        raise Exception('No duplicates are allowed in cells.')
    #
    #    i = 0
    #    sum = 0
    #    while i < len(freqs):
    #        sum += freqs[i]
    #        i += 1
    #    if not math.isclose(sum, 1.0):
    #        raise Exception('Cummulative provabilities should be equal to 1.')
    #
    #    self.Cells = cells
    #    self.Freqs = freqs
    #    self.Units = units
    #
    #def __new__(cls, *args, **kwds):
    #    return super(RVDiscrete, cls).__new__(cls)

class RVDiscrete():
    def __init__(self, cells, freqs, names, units):
        if len(cells) != len(freqs):
            raise Exception('Cells and freqs should have the same number of entries.')
        if len(cells) < 2:
            raise Exception('At least two cells should be used.')

        if checkIfDuplicates(cells):
            raise Exception('No duplicates are allowed in cells.')

        i = 0
        sum = 0
        while i < len(freqs):
            sum += freqs[i]
            i += 1
        if not math.isclose(sum, 1.0):
            raise Exception('Cummulative provabilities should be equal to 1.')

        self.Cells = cells
        self.Freqs = freqs
        self.Names = names
        self.gotMin = False
        self.gotMax = False
        self.Min = 0
        self.Max = 0
        self.Units = units
        self.instance = scipy.stats.rv_discrete(values=(cells, freqs))

    def consistentMinMax(self):
        if self.gotMin and self.Cells[-1] <= self.Min:
            return False
        if self.gotMax and self.Cells[0] >= self.Max:
            return False
        if self.gotMin and self.gotMax and self.Max <= self.Min:
            return False
        return True


    def gotLimits(self):
        return self.gotMin or self.gotMax


    def setMaximum(self, max):
        self.Max = max
        self.gotMax = True


    def setMinimum(self, min):
        self.Min = min
        self.gotMin = True


    def getSample(self):
        sample = self.instance.rvs()

        if self.gotMax and sample > self.Max:
            return self.Max

        if self.gotMin and sample < self.Min:
            return self.Min

        return sample


    def getSampleUnits(self):
        sample = self.getSample()
        return tu.transformUnits(sample, self.getUnits())


    def getUnits(self):
        return self.Units

    def plot_distribution(self, name, path):
        data_normal = self.instance.rvs(size=1000)

        # settings for seaborn plotting style
        sns.set(color_codes=True)
        # settings for seaborn plot sizes
        sns.set(rc={'figure.figsize': (5, 5)})
        ax = sns.distplot(data_normal,
                          kde=True,
                          color='skyblue',
                          hist_kws={"linewidth": 15, 'alpha': 1})
        ax.set(xlabel='Distribution', ylabel='Frequency')
        fig = plt.gcf()
        filepath = path + "\\" + name + "_Distribution.png"
        print("Generating figure: " + filepath)
        fig.savefig(filepath)
        plt.close(fig)

        x = np.linspace(self.Cells[0], self.Cells[-1], 1000)

        #y_pdf = self.instance.pdf(x)  # the normal pdf
        y_cdf = self.instance.cdf(x)  # the normal cdf

        label = name + " - CDF"
        plt.plot(x, y_cdf, label = label)
        plt.legend(prop={'size': 6})
        plt.suptitle("NMAC " + name + " - CDF")
        plt.title("Cumulative distribution function")
        fig = plt.gcf()
        filepath = path + "\\" + name + "_CDF.png"
        print("Generating figure: " + filepath)
        fig.savefig(filepath)
        plt.close(fig)
