import scipy.stats as stats
import numpy as np
import json
from ProbabilityDist import RV_Continous as rvc, RV_Discrete as rvd
from Encounter import units as units


def getUnaryUniformFactor():
    return stats.uniform.rvs(loc=0, scale=1)


def scaleToUnaryFactor(range, reference, unaryFactor):
    lowRange = range * unaryFactor
    upRange = range * (1 - unaryFactor)
    return np.array([reference - lowRange, reference + upRange])


def newUnaryFactor(speedRange, speedRef):
    return (speedRef - speedRange[0])/(speedRange[1] - speedRange[0])


class Aircraft_Class:

    def __init__(self):
        self.classIdx = 0
        self.className = None
        self.droneName = None
        self.speedDistribution = None
        self.climbRateDistribution = None
        self.descentRateDistribution = None
        self.trateDistribution = None
        self.accelDistribution = None
        self.Transition_Distribution = None


class Initial_Distribution:

    def __init__(self):
        self.vmdDistribution = None
        self.hmdDistribution = None
        self.altitudeDistribution = None
        self.speedDistribution = None
        self.appAngleDistribution = None
        self.ownshipClassDistribution = None
        self.intruderClassDistribution = None
        self.ownshipDistributionList = []
        self.intruderDistributionList = []


class Transition_Distribution:

    def __init__(self):
        self.horizontalCmdProb = None
        self.verticalCmdProb = None
        self.speedCmdProb = None
        self.altitudeVariationDistribution = None
        self.speedVariationDistribution = None
        self.headingVariationDistribution = None
        self.segmentDurationDistribution = None
        self.holdDurationDistribution = None


class Initial_Aircraft_Distribution:

    def __init__(self):
        self.className = None
        self.classIdx = 0
        self.speedDistribution = None
        self.vrateDistribution = None
        self.accelDistribution = None
        self.trateDistribution = None


class Encounter_Distribution:

    def __init__(self):
        self.name = ""
        self.version = ""
        self.description = ""
        self.Aircraft_Class_List = None
        self.Initial_Distribution = None
        self.Transition_Distribution = None

    def getAircraftClass(self, idx):
        return self.Aircraft_Class_List[idx]


    def plotEncounterDistribution(self, path):
        self.Initial_Distribution.vmdDistribution.plot_distribution("VMD", path)
        self.Initial_Distribution.hmdDistribution.plot_distribution("HMD", path)
        self.Initial_Distribution.altitudeDistribution.plot_distribution("Altitude", path)
        self.Initial_Distribution.speedDistribution.plot_distribution("Speed", path)
        self.Initial_Distribution.appAngleDistribution.plot_distribution("appAngle", path)
        self.Initial_Distribution.ownshipClassDistribution.plot_distribution("ownShip", path)
        self.Initial_Distribution.intruderClassDistribution.plot_distribution("intruder", path)


    def parseFromJson(self, filename):
        complete = True

        print("Loading probability distributions from file: ", filename)
        f = open(filename, "r")
        distributionsTable = json.load(f)

        if 'name' in distributionsTable:
            self.name = distributionsTable['name']

        if 'version' in distributionsTable:
            self.version = distributionsTable['version']

        if 'description' in distributionsTable:
            self.description = distributionsTable['description']

        if 'aircraftClass' in distributionsTable:
            complete = complete and self.parseAircraftClassList(distributionsTable['aircraftClass'])
        else:
            complete = False

        if 'initialDistribution' in distributionsTable:
            complete = complete and self.parseInitialDistribution(distributionsTable['initialDistribution'])
        else:
            complete = False

        #if 'transitionDistribution' in distributionsTable:
        #    self.Transition_Distribution = self.parseTransitionDistribution(distributionsTable['transitionDistribution'])
        #    complete = complete and (self.Transition_Distribution != None)
        #else:
        #    complete = False

        return complete


    def parseAircraftClassList(self, aircraftClassTableList):
        complete = True

        self.Aircraft_Class_List = []

        for aircraftClassTable in aircraftClassTableList:

            aircraftClass = self.parseAircraftClass(aircraftClassTable)

            if aircraftClass != None:
                self.Aircraft_Class_List.append(aircraftClass)
            else:
                print("Failure to properly read an Aircraft Class distribution table.")
                complete = False

        return complete


    def parseAircraftClass(self, aircraftClassTable):
        complete = True

        aircraftClass = Aircraft_Class()

        self.Initial_Distribution = Initial_Distribution()

        if 'classIdx' in aircraftClassTable:
            aircraftClass.classIdx = int(aircraftClassTable['classIdx'])
        else:
            complete = False

        if 'className' in aircraftClassTable:
            aircraftClass.className = aircraftClassTable['className']
        else:
            complete = False

        if 'droneName' in aircraftClassTable:
            aircraftClass.droneName = aircraftClassTable['droneName']
        else:
            complete = False

        if 'speedDistribution' in aircraftClassTable:
            aircraftClass.speedDistribution = self.parseContinousDistribution(aircraftClassTable['speedDistribution'])

            if aircraftClass.speedDistribution == None:
                print("Failure to properly read an speedDistribution distribution table for aircraft: ", aircraftClass.className)
                complete = False
        else:
            complete = False

        if 'climbRateDistribution' in aircraftClassTable:
            aircraftClass.climbRateDistribution = self.parseContinousDistribution(aircraftClassTable['climbRateDistribution'])

            if aircraftClass.climbRateDistribution == None:
                print("Failure to properly read an climbRateDistribution distribution table for aircraft: ", aircraftClass.className)
                complete = False
        else:
            complete = False

        if 'descentRateDistribution' in aircraftClassTable:
            aircraftClass.descentRateDistribution = self.parseContinousDistribution(aircraftClassTable['descentRateDistribution'])

            if aircraftClass.descentRateDistribution == None:
                print("Failure to properly read an descentRateDistribution distribution table for aircraft: ", aircraftClass.className)
                complete = False
        else:
            complete = False

        if 'turnRateDistribution' in aircraftClassTable:
            aircraftClass.trateDistribution = self.parseContinousDistribution(aircraftClassTable['turnRateDistribution'])

            if aircraftClass.trateDistribution == None:
                print("Failure to properly read an turnRateDistribution distribution table for aircraft: ", aircraftClass.className)
                complete = False
        else:
            complete = False

        if 'accelDistribution' in aircraftClassTable:
            aircraftClass.accelDistribution = self.parseContinousDistribution(aircraftClassTable['accelDistribution'])

            if aircraftClass.accelDistribution == None:
                print("Failure to properly read an accelDistribution distribution table for aircraft: ", aircraftClass.className)
                complete = False
        else:
            complete = False

        if 'transitionDistribution' in aircraftClassTable:
            aircraftClass.Transition_Distribution = self.parseTransitionDistribution(aircraftClassTable['transitionDistribution'])

            if aircraftClass.Transition_Distribution == None:
                print("Failure to properly read an transitionDistribution distribution table for aircraft: ", aircraftClass.className)
                complete = False
        else:
            complete = False

        return aircraftClass



    def parseInitialDistribution(self, iniDistTable):
        complete = True
        self.Initial_Distribution = Initial_Distribution()

        if 'vmdDistribution' in iniDistTable:
            self.Initial_Distribution.vmdDistribution = self.parseContinousDistribution(iniDistTable['vmdDistribution'])

            if self.Initial_Distribution.vmdDistribution == None:
                print("Failure to properly read an vmdDistribution initial distribution table.")
                complete = False
        else:
            complete = False

        if 'hmdDistribution' in iniDistTable:
            self.Initial_Distribution.hmdDistribution = self.parseContinousDistribution(iniDistTable['hmdDistribution'])

            if self.Initial_Distribution.hmdDistribution == None:
                print("Failure to properly read an hmdDistribution initial distribution table.")
                complete = False
        else:
            complete = False

        if 'altitudeDistribution' in iniDistTable:
            self.Initial_Distribution.altitudeDistribution = self.parseContinousDistribution(iniDistTable['altitudeDistribution'])

            if self.Initial_Distribution.altitudeDistribution == None:
                print("Failure to properly read an altitudeDistribution initial distribution table.")
                complete = False
        else:
            complete = False

        if 'speedDistribution' in iniDistTable:
            self.Initial_Distribution.speedDistribution = self.parseContinousDistribution(iniDistTable['speedDistribution'])

            if self.Initial_Distribution.speedDistribution == None:
                print("Failure to properly read an speedDistribution initial distribution table.")
                complete = False
        else:
            complete = False

        if 'appAngleDistribution' in iniDistTable:
            self.Initial_Distribution.appAngleDistribution = self.parseContinousDistribution(iniDistTable['appAngleDistribution'])

            if self.Initial_Distribution.appAngleDistribution == None:
                print("Failure to properly read an appAngleDistribution initial distribution table.")
                complete = False
        else:
            complete = False

        if 'ownshipDistribution' in iniDistTable:
            self.Initial_Distribution.ownshipClassDistribution = self.parseDiscreteDistribution(iniDistTable['ownshipDistribution'])

            if self.Initial_Distribution.ownshipClassDistribution == None:
                print("Failure to properly read an ownshipDistribution initial distribution table.")
                complete = False
        else:
            complete = False

        if 'intruderDistribution' in iniDistTable:
            self.Initial_Distribution.intruderClassDistribution = self.parseDiscreteDistribution(iniDistTable['intruderDistribution'])

            if self.Initial_Distribution.intruderClassDistribution == None:
                print("Failure to properly read an intruderDistribution initial distribution table.")
                complete = False
        else:
            complete = False

        return complete



    def parseTransitionDistribution(self, transDistTable):
        complete = True
        transition_Distribution = Transition_Distribution()

        if 'horizontalCmdProb' in transDistTable:
            transition_Distribution.horizontalCmdProb = self.parseDiscreteDistribution(transDistTable['horizontalCmdProb'])

            if transition_Distribution.horizontalCmdProb == None:
                print("Failure to properly read an horizontalCmdProb transition distribution table.")
                complete = False
        else:
            complete = False

        if 'verticalCmdProb' in transDistTable:
            transition_Distribution.verticalCmdProb = self.parseDiscreteDistribution(transDistTable['verticalCmdProb'])

            if transition_Distribution.verticalCmdProb == None:
                print("Failure to properly read an verticalCmdProb transition distribution table.")
                complete = False
        else:
            complete = False

        if 'speedCmdProb' in transDistTable:
            transition_Distribution.speedCmdProb = self.parseDiscreteDistribution(transDistTable['speedCmdProb'])

            if transition_Distribution.speedCmdProb == None:
                print("Failure to properly read an speedCmdProb transition distribution table.")
                complete = False
        else:
            complete = False

        if 'altitudeVariationDistribution' in transDistTable:
            transition_Distribution.altitudeVariationDistribution = self.parseContinousDistribution(transDistTable['altitudeVariationDistribution'])

            if transition_Distribution.altitudeVariationDistribution == None:
                print("Failure to properly read an altitudeVariationDistribution transition distribution table.")
                complete = False
        else:
            complete = False

        if 'speedVariationDistribution' in transDistTable:
            transition_Distribution.speedVariationDistribution = self.parseContinousDistribution(transDistTable['speedVariationDistribution'])

            if transition_Distribution.speedVariationDistribution == None:
                print("Failure to properly read an speedVariationDistribution transition distribution table.")
                complete = False
        else:
            complete = False

        if 'headingVariationDistribution' in transDistTable:
            transition_Distribution.headingVariationDistribution = self.parseContinousDistribution(transDistTable['headingVariationDistribution'])

            if transition_Distribution.headingVariationDistribution == None:
                print("Failure to properly read an headingVariationDistribution transition distribution table.")
                complete = False
        else:
            complete = False

        if 'segmentDurationDistribution' in transDistTable:
            transition_Distribution.segmentDurationDistribution = self.parseContinousDistribution(transDistTable['segmentDurationDistribution'])

            if transition_Distribution.segmentDurationDistribution == None:
                print("Failure to properly read an segmentDurationDistribution transition distribution table.")
                complete = False
        else:
            complete = False

        if 'holdDurationDistribution' in transDistTable:
            transition_Distribution.holdDurationDistribution = self.parseContinousDistribution(transDistTable['holdDurationDistribution'])

            if transition_Distribution.holdDurationDistribution == None:
                print("Failure to properly read an holdDurationDistribution transition distribution table.")
                complete = False
        else:
            complete = False

        if complete:
            return transition_Distribution
        else:
            return None



    def parseContinousDistribution(self, distributionTable):
        complete = True

        if 'cellVector' in distributionTable:
            cellVector = distributionTable['cellVector']
        else:
            print("Error: No cellVector table found.")
            complete = False

        if 'freqsVector' in distributionTable:
            freqsVector = distributionTable['freqsVector']
        else:
            print("Error: No freqsVector table found.")
            complete = False


        if 'units' in distributionTable:
            units = distributionTable['units']
        else:
            print("Error: No units entry found.")
            complete = False

        if not complete:
            return None

        if not rvc.check_RVContinous(cellVector, freqsVector):
            print("Error: The distribution is not continous.")
            return None

        if not rvc.check_RVFull(cellVector, freqsVector):
            print("Error: The distribution probability does not add up to 1.")
            return None

        distribution = rvc.RVContinuous(cellVector, freqsVector, units)

        if 'max' in distributionTable:
            max = distributionTable['max']
            distribution.setMaximum(max)

        if 'min' in distributionTable:
            min = distributionTable['min']
            distribution.setMinimum(min)

        if distribution.gotLimits():
            distribution.consistentMinMax()

        return distribution


    def parseDiscreteDistribution(self, distributionTable):
        complete = True

        if 'cellVector' in distributionTable:
            cellVector = distributionTable['cellVector']
        else:
            complete = False

        if 'freqsVector' in distributionTable:
            freqsVector = distributionTable['freqsVector']
        else:
            complete = False

        if 'names' in distributionTable:
            names = distributionTable['names']
        else:
            names = []

        if 'units' in distributionTable:
            units = distributionTable['units']
        else:
            complete = False

        if complete and rvd.check_RVDiscrete(cellVector, freqsVector):
            distribution = rvd.RVDiscrete(cellVector, freqsVector, names, units)

            if 'max' in distributionTable:
                max = distributionTable['max']
                distribution.setMaximum(max)

            if 'min' in distributionTable:
                min = distributionTable['min']
                distribution.setMinimum(min)


            if distribution.gotLimits():
                distribution.consistentMinMax()

            return distribution

        return None
