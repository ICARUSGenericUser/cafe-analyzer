import math as math
import numpy as np
from Encounter import units as units

class FlatEarthProjection:
    def __init__(self, projLon, projLat, projAlt):
        self.projLat = units.deg_to_rad(projLat)
        self.projLon = units.deg_to_rad(projLon)
        self.projAlt = projAlt
        self.earthRadius = 6371229.0  # Canonical radius of the spherical earth
        self.radius = 6371229.0


    # Return a projection of a lat/lon/alt point in Euclidan 2-space
    def project(self, lon, lat):
        fromLat = units.deg_to_rad(lat)
        fromLon = units.deg_to_rad(lon)

        dy = self.radius * (fromLat - self.projLat)
        dx = self.radius * math.cos(fromLat) * (fromLon - self.projLon)
        return np.array([dx, dy])


    # Return a projection of a lat/lon/alt point in Euclidan 2-space
    def project2(self, v3):
        fromLon = units.deg_to_rad(v3(0))
        fromLat = units.deg_to_rad(v3(1))

        dy = self.radius * (fromLat - self.projLat)
        dx = self.radius * math.cos(fromLat) * (fromLon - self.projLon)
        return np.array([dx, dy])


    # Return a projection of a lat/lon/alt point in Eucliean 3-space
    def project3(self, v3):
        return np.concatenate((self.project2(v3), v3(2) - self.projAlt))


    # Return a Lat/Lon/Alt value corresponding to the given Euclidean position
    def inverse(self, xp, yp, alt):
        TOLERANCE = 1.0e-06;
        toLat = self.projLat + yp / self.radius
        cosl = math.cos (toLat)
        if (abs(cosl) < TOLERANCE):
            toLon = self.projLon
        else:
            toLon = self.projLon + xp / cosl / self.radius

        toLat = units.rad_to_deg(toLat)
        toLon = units.rad_to_deg(toLon)

        return np.array([toLon, toLat, alt])


    # Return a Lat/Lon/Alt value corresponding to the given Euclidean position
    def inverse3(self, v3):
        return self.inverse2(v3[0:1], v3(2))


    # Return a Lat/Lon/Alt value corresponding to the given Euclidean position
    def inverse2(self, v2, alt):
        TOLERANCE = 1.0e-06;
        xp = v2(0)
        yp = v2(1)
        toLat = self.projLat + yp / self.radius
        cosl = math.cos (toLat)
        if (abs(cosl) < TOLERANCE):
            toLon = self.projLon
        else:
            toLon = self.projLon + xp / cosl / self.radius

        return np.array([toLon, toLat, alt])

